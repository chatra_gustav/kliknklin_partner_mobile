package com.kliknklin.knklaundrymitra;

import com.RNFetchBlob.RNFetchBlobPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.bolan9999.SpringScrollViewPackage;
import com.devfd.RNGeocoder.RNGeocoderPackage;
import com.facebook.react.ReactPackage;
import com.facebook.soloader.SoLoader;
import com.heanoria.library.reactnative.locationenabler.RNAndroidLocationEnablerPackage;
import com.imagepicker.ImagePickerPackage;
import com.inprogress.reactnativeyoutube.ReactNativeYouTube;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.levelasquez.androidopensettings.AndroidOpenSettingsPackage;
import com.mustansirzia.fused.FusedLocationPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.reactnativenavigation.NavigationApplication;
import com.wheelpicker.WheelPickerPackage;
import android.support.multidex.MultiDex;
import android.content.Context;

import java.util.Arrays;
import java.util.List;

import br.com.classapp.RNSensitiveInfo.RNSensitiveInfoPackage;
import cn.jystudio.bluetooth.RNBluetoothEscposPrinterPackage;
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;

//import com.facebook.react.ReactApplication;
import com.reactnativecommunity.netinfo.NetInfoPackage;
//import com.facebook.react.ReactNativeHost;
//installed import

public class MainApplication extends NavigationApplication {

  @Override
  protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
    MultiDex.install(this);
  }

  @Override
  public void onCreate() { // <-- Check this block exists
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false); // <-- Check this line exists within the block
  }

  @Override
  public boolean isDebug() {
    // Make sure you are using BuildConfig from your own application
    return BuildConfig.DEBUG;
  }

  protected List<ReactPackage> getPackages() {
    // Add additional packages you require here
    // No need to add RnnPackage and MainReactPackage
    return Arrays.<ReactPackage>asList(
            new FusedLocationPackage(),
            new RNDeviceInfo(),
            new RNSensitiveInfoPackage(),
            new VectorIconsPackage(),
            new MapsPackage(),
            new AndroidOpenSettingsPackage(),
            new ImagePickerPackage(),
            new RNFirebasePackage(),
            new RNFirebaseMessagingPackage(),
            new RNFirebaseNotificationsPackage(),
            new RNAndroidLocationEnablerPackage(),
            new SpringScrollViewPackage(),
            new ReactNativeYouTube(),
            new WheelPickerPackage(),
            new RNGeocoderPackage(),
            new RNFetchBlobPackage(),
            new RNBluetoothEscposPrinterPackage(),
            new RNCWebViewPackage(),
            new NetInfoPackage()
    );
  }

  @Override
  public String getJSMainModuleName() {
    return "index";
  }

  @Override
  public List<ReactPackage> createAdditionalReactPackages() {
    return getPackages();
  }

}
