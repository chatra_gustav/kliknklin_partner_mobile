package com.kliknklin.knklaundrymitra;

import com.facebook.react.ReactActivity;
import com.imagepicker.ImagePickerPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.airbnb.android.react.maps.MapsPackage;
import com.reactnativenavigation.controllers.SplashActivity;
import com.imagepicker.permissions.OnImagePickerPermissionsCallback; // <- add this import
import com.facebook.react.modules.core.PermissionListener; // <- add this import
import android.widget.LinearLayout;
import android.content.Intent;
import android.graphics.Color;
import android.widget.TextView;
import android.view.Gravity;
import android.util.TypedValue;
import android.widget.ImageView;
import android.support.v4.content.ContextCompat;
import android.view.Display;
import android.graphics.Point;
import android.util.Log;

import com.reactnativenavigation.controllers.SplashActivity;

public class MainActivity extends SplashActivity implements OnImagePickerPermissionsCallback{
    private PermissionListener listener; // <- add this attribute

    // Your methods here

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }
    // Copy from here

    @Override
    public void setPermissionListener(PermissionListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        if (listener != null)
        {
            listener.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public LinearLayout createSplashLayout() {
        LinearLayout view = new LinearLayout(this);
        ImageView imageView = new ImageView(this);

        //view.setBackgroundColor(Color.parseColor("#6CCAF2"));
        //view.setGravity(Gravity.CENTER);

        // hard code the width and the height of the logo
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams.gravity = Gravity.FILL;
        imageView.setLayoutParams(layoutParams);
        //imageView.setAdjustViewBounds(true);
        imageView.setImageDrawable(ContextCompat.getDrawable(this.getApplicationContext(), R.drawable.splash));

        view.addView(imageView);
        return view;
    }

}