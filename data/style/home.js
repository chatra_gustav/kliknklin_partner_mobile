import { StyleSheet, PixelRatio, Platform } from 'react-native';

import {FontScale} from './../components/font_scale';

module.exports = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#FFF',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        top: 70
    },

    text: {
    	fontFamily:'Montserrat',
        fontSize:FontScale._getFontSize(8),
    },

    button: {
        height:40, 
        justifyContent:'center', 
        alignItems:'center',
        backgroundColor: '#66CEFD',
    },

    buttonPressed :{
        height:40, 
        justifyContent:'center', 
        alignItems:'center',
        backgroundColor: '#98defe',
    },

    paymentButton: {
        height:40, 
        justifyContent:'center', 
        alignItems:'center',
        backgroundColor: '#98defe',
    },

    paymentButtonPressed :{
        height:40, 
        justifyContent:'center', 
        alignItems:'center',
        backgroundColor: '#98defe',
        borderLeftWidth:6, borderLeftColor:'#1ab6fc',    
    },

    bankButton: {
        borderBottomWidth:2, borderColor:'#66CEFD',
    },

    bankButtonPressed:{
        borderBottomWidth:2, borderBottomColor:'#66CEFD',
        borderLeftWidth:6, borderLeftColor:'#1ab6fc',
    },

    separator: {
        borderBottomWidth: 1,
        borderColor:'lightgray',
        alignSelf:'stretch',
    },

    line: {
        flex: 1,
        height: 1,
        backgroundColor: 'lightgray',
    },
});
