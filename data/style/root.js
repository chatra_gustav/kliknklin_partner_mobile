import { StyleSheet,Platform } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    navBar: {
        backgroundColor: '#61CDFF',
        justifyContent:'center',
        alignItems:'center',
        height:Platform.OS == 'android' ? 60 : 70,
    },
    navBarTransparent:{
        backgroundColor: 'rgba(0,0,0,0)',
        justifyContent:'center',
        alignItems:'center',
    },
    navBarText: {
        color: 'white',
        marginVertical: 10,
    },
    navBarTitleText: {
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        alignSelf:'center',
    },
    navBarLeftButton: {
        flex:1,
        paddingLeft:10,
        alignItems:'center',
        justifyContent:'center',
        marginTop:Platform.OS == 'android' ? 5 : 0,
    },
    navBarRightButton: {
        
    },
    scene: {
        flex: 1,
        paddingTop: 63,
    }
});
