import { StyleSheet } from 'react-native';

module.exports = StyleSheet.create({
    listViewContainer: {
        //flex:1,
        backgroundColor:'#61CDFF',
    },

    viewContainer:{
        flex:1,
        //backgroundColor:'red'
    },

    menuItem: {
    	padding:10,
        paddingLeft:20,
        paddingRight:20,
    },

    headerItem:{
        //backgroundColor: '#61CDFF',
    	paddingTop:25,
        paddingLeft: 20,
        paddingRight: 20,
    },

    buttonItem:{
    	fontFamily:'Montserrat',
    	color:'#fff',
    	textAlign:'left',
    	fontWeight:'normal',
    	fontSize:21,
    	fontWeight:'800',
    },

    nameItem:{
    	fontFamily:'Montserrat',
    	color:'white',
    	textAlign:'left',
    	fontSize:21,
    	fontWeight:'800',
    },

    emailItem:{
    	fontFamily:'Montserrat',
    	color:'white',
    	textAlign:'left',
    	fontSize:12,
    },

    separator: {
    	paddingTop:15,
    	borderBottomWidth: 2,
    	borderColor:'white',
    	flex:1,
    },

    photoContainer: {
    	borderRadius: 30,
    	borderColor:'white',
    	borderWidth: 3,
    	width:60,
    	height:60,
    	justifyContent:'center',
    	alignItems:'center'
    },

    iconContainer: {
    	width:50,
    	height:50,
    	justifyContent:'center',
    	alignItems:'center'
    },

   	headerTextContainer: {
    	justifyContent:'center',
    	alignItems:'flex-start',
    	paddingLeft: 20,
    },

    buttonContainer:{
    	justifyContent:'center',
    	alignItems:'center',
    	paddingLeft: 20,
    }
});
