import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  View,
  ScrollView,
  TouchableOpacity,
  Text,
  TouchableWithoutFeedback
} from 'react-native';

import ResponsiveImage from 'react-native-responsive-image';
import Communications from 'react-native-communications';

import {TextWrapper} from "./../../../components";

import{verticalScale, moderateScale, scale} from 'react-native-size-matters';

export default class TypeA extends Component{
	constructor(props) {
	    super(props);
	    this.state = {
	    };
	}

	
	render(){
		return(
			<View 
			  //scrollEnabled={false}
			  style={[styleContainer.container, this.props.containerStyle]}
			>
				<ScrollView 
					contentContainerStyle={[styleContainer.containerContent, this.props.contentStyle]} 
				>
					{this.props.children}
				</ScrollView>
			</View>
		);
	}
}

const styleContainer = StyleSheet.create({
	container:{
		width:'100%',
	    backgroundColor:'white',
	   	//borderRadius:10,
	    minHeight:Dimensions.get('window').height * 0.7
	},
	containerContent:{
		padding:moderateScale(10),
	}
});