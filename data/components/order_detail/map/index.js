import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Dimensions,
  Linking,
  TouchableOpacity,
} from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import MapView from 'react-native-maps';
import ResponsiveImage from 'react-native-responsive-image';

import {TextWrapper} from "./../../../components";

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class Map extends Component {
	
	render(){
		return(
			<MapComponent 
				directionCoords={this.props.directionCoords}
				centerCoords={this.props.centerCoords}
			/>
		);
		if (this.props.directionCoords != null && this.props.centerCoords!= null && this.props.isMapAllowedToLoad) {
			return(
				<MapComponent 
					directionCoords={this.props.directionCoords}
					centerCoords={this.props.centerCoords}
				/>
			);
		}
		else{
			return(
				<LoadingMapComponent />
			);
		}
	}
}

class MapComponent extends Component{
	
	renderChild(){
		if (this.props.directionCoords.length != 0) {
			return(
				<View>
					<DestinationMarker 
						directionCoords={this.props.directionCoords}
					/>

					<DirectionMarker 
						directionCoords={this.props.directionCoords}
					/>

					<SelfMarker 
						directionCoords={this.props.directionCoords}
					/>
				</View>
			);
		}
	}
	render(){
		return(
			<View style={styles.container}>
				<View style={styles.containerMap}>
					<DetailMap 
						centerCoords={this.props.centerCoords}
					>
						
						{this.renderChild()}
						
					</DetailMap>

					<OpenMapsButton />
				</View>
			</View>
		);
	}
}

class LoadingMapComponent extends Component{
	render(){
		return(
			<View style={styles.container}>
				<View style={styles.containerMap}>
					<TextWrapper type={'base'} style={{color:'white'}}>
						Memuat peta...
					</TextWrapper>
				</View>
			</View>
		);
	}
}

class DetailMap extends Component{

	render(){
		return(
			<MapView
				initialRegion={{
			      latitude: -7.7956,
			      longitude: 110.3695,
			      latitudeDelta: 999999,
			      longitudeDelta: 999999,
			    }}
				region={this.props.centerCoords}
				style={styles.map}
				onPress={()=>{}}
			>
				{this.props.children}
			</MapView>
		);
	}
}

class DestinationMarker extends Component{
	render(){
		return(
			<MapView.Marker
				coordinate={this.props.directionCoords[this.props.directionCoords.length - 1]}
				pinColor="orange"
				title={'tujuan anda'}
			/>
		);
	}
}

class DirectionMarker extends Component{
	render(){
		return(
			<MapView.Polyline
				coordinates={this.props.directionCoords}
				strokeWidth={6}
				strokeColor="#23b1e9"
			/>
		);
	}
}

class SelfMarker extends Component{
	render(){
		return(
			<MapView.Marker
				coordinate={this.props.directionCoords[0]}
				pinColor={'red'}
				title={'lokasi anda'}
			/>
		);
	}
}

class OpenMapsButton extends Component{
	render(){
		return(
			<View style={styles.containerOpenMapsButton}>
				<TouchableOpacity 
					onPress={()=> {
						Linking.canOpenURL('http://maps.google.com/maps?daddr='+this._translateDestination().latitude+','+this._translateDestination().longitude).then(supported => {
							if (supported) {
							    Linking.openURL('http://maps.google.com/maps?daddr='+this._translateDestination().latitude+','+this._translateDestination().longitude);
							} else {
							    console.log('Don\'t know how to go');
							}
						}).catch(err => console.error('An error occurred', err));
					}} 
				>
				</TouchableOpacity> 
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container:{
		height:Dimensions.get('window').height * 0.42,
		width:Dimensions.get('window').width * 0.9,
		justifyContent: 'center',
		alignItems: 'center',
	},
	containerMap: {
		height:Dimensions.get('window').height * 0.4,
		width:Dimensions.get('window').width * 0.85,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 10,
		borderWidth:1,
		borderColor:'transparent',
		overflow: 'hidden',
		backgroundColor:'#69CAF0',
		elevation:3,
	},
	map: {
		...StyleSheet.absoluteFillObject,
	},
	containerOpenMapsButton:{
		flex:1,
		alignSelf:'stretch',
		height:200, 
		justifyContent:'flex-end', 
		alignItems:'flex-end',
		margin:5,
	}
});

function mapStateToProps(state, ownProps) {
	return {
	};
}

function mapDispatchToProps(dispatch) {
	return {
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Map);

