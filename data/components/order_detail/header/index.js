import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';

import {TextWrapper, OrderDetail, OrderStatus} from './../../../components';

export default class Header extends Component{
	constructor(props) {
		super(props);
	  
	    this.state = {
			buttonColor:this.props.buttonColor,
			buttonText:this.props.buttonText,
	    };
	  }

	render(){
		return(
			<View
				style={styles.headerContainer}
    		>
				<View style={styles.leftContentContainer}>
	    			<TextWrapper 
		    			type={'content'} 
		    			style={styles.orderIDText}
	    			>
	    			{this.props.orderID}
	    			</TextWrapper>
    			</View>

				<View style={styles.rightContentContainer}>
					<OrderStatus shipmentStatus={this.props.shipmentStatus}/>
				</View>
    		</View>
		);
	}
}

Header.propTypes = {
  shipmentStatus: PropTypes.oneOf(['AKTIF', 'TUNDA', 'BATAL', 'TELAT', 'SELESAI']),
  orderID:PropTypes.string,
}

Header.defaultProps = {
  	orderID:'Order ID',
  	shipmentStatus:'Unknown',
}

const styles = StyleSheet.create({
  	headerContainer: {
		flexDirection:'row',
		width:Dimensions.get('window').width * 0.8,
		height:Dimensions.get('window').height * 0.035,
		justifyContent:'flex-start',
		alignItems:'flex-end',
		marginBottom:Dimensions.get('window').height * 0.01,
	},
	rightContentContainer:{
		flex:0.3,
		alignItems:'flex-end',
	},
	leftContentContainer:{
		flex:0.7,
	},
	orderIDText:{
		color:'white',
	},
});