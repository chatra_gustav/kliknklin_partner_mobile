import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  View,
  TouchableOpacity,
  Text
} from 'react-native';

import ResponsiveImage from 'react-native-responsive-image';
import Communications from 'react-native-communications';

import {TextWrapper, CourierContent} from "./../../../components";
import * as CONSTANT from "./../../../constant";

export class Destination extends Component{

	render(){
		return(
			<View style={[styleContent.itemDetail, {borderBottomWidth: this.props.endContent ? 0 : 1} ]}>
	      		<View style={styleContent.titleRow}>
					<View style={styleContent.titleRowImage}>
						<ResponsiveImage
		                source={this.props.image.tujuan_icon.url}
		                initWidth={this.props.image.tujuan_icon.width * this.props.image.tujuan_icon.multiplierWidth}
		                initHeight={this.props.image.tujuan_icon.height * this.props.image.tujuan_icon.multiplierHeight}
		              />
					</View>

					<View style={styleContent.titleRowContent}>
						<TextWrapper type={'base'} style={{color:'#69CAF0'}}>
						  {'Tujuan'}
						</TextWrapper>
					</View>
				</View>

				<View style={styleContent.itemRow}>

					<View style={styleContent.itemRowTitle}>
						<TextWrapper type={'base'}>
						  Alamat
						</TextWrapper>
					</View>

					<View style={styleContent.itemRowColon}>
						<TextWrapper type={'base'}>
						  :
						</TextWrapper>
					</View>

					<View style={styleContent.itemRowContent}>
						<TextWrapper type={'base'}
							numberOfLines={5}
						>
						  {this.props.address}
						</TextWrapper>
					</View>
				</View>
						
				<View style={styleContent.itemRow}>
					<View style={styleContent.itemRowTitle}>
						<TextWrapper type={'base'}>
						  Info
						</TextWrapper>
					</View>

					<View style={styleContent.itemRowColon}>
						<TextWrapper type={'base'}>
						  :
						</TextWrapper>
					</View>

					<View style={styleContent.itemRowContent}>
						<TextWrapper type={'base'}
							numberOfLines={3}
						>
						  {this.props.addressInfo}
						</TextWrapper>
					</View>
				</View>
	   		 </View>
   		 );
	}
}

export class Schedule extends Component{

	render(){
		return(
			<View style={[styleContent.itemDetail, {borderBottomWidth: this.props.endContent ? 0 : 1} ]}>
				<View style={styleContent.titleRow}>
					<View style={styleContent.titleRowImage}>
						<ResponsiveImage
						source={this.props.image.jadwal_icon.url}
						initWidth={this.props.image.jadwal_icon.width * this.props.image.jadwal_icon.multiplierWidth}
						initHeight={this.props.image.jadwal_icon.height * this.props.image.jadwal_icon.multiplierHeight}
						/>
					</View>

					<View style={styleContent.titleRowContent}>
						<TextWrapper type={'base'} style={{color:'#69CAF0'}}>
						Jadwal
						</TextWrapper>
					</View>
				</View>

				<View style={styleContent.itemRow}>
					<View style={styleContent.itemRowTitle}>
						<TextWrapper type={'base'}>
						Tanggal
						</TextWrapper>
					</View>

					<View style={styleContent.itemRowColon}>
						<TextWrapper type={'base'}>
						:
						</TextWrapper>
					</View>

					<View style={styleContent.itemRowContent}>
						<TextWrapper type={'base'}>
						Senin, 20 Januari 2018
						</TextWrapper>
					</View>
				</View>

				<View style={styleContent.itemRow}>
					<View style={styleContent.itemRowTitle}>
						<TextWrapper type={'base'}>
						Waktu
						</TextWrapper>
					</View>

					<View style={styleContent.itemRowColon}>
						<TextWrapper type={'base'}>
						:
						</TextWrapper>
					</View>

					<View style={styleContent.itemRowContent}>
						<TextWrapper type={'base'}>
						10.00 - 11.00 WIB
						</TextWrapper>
					</View>
				</View>
			</View>
		)
	}
}

export class Customer extends Component{
	render(){
		return(
			<View style={[styleContent.itemDetail, {borderBottomWidth: this.props.endContent ? 0 : 1} ]}>
			
				<View style={styleContent.titleRow}>
					<View style={styleContent.titleRowImage}>
						<ResponsiveImage
						source={this.props.image.konsumen_icon.url}
						initWidth={this.props.image.konsumen_icon.width * this.props.image.konsumen_icon.multiplierWidth}
						initHeight={this.props.image.konsumen_icon.height * this.props.image.konsumen_icon.multiplierHeight}
						/>
					</View>

					<View style={styleContent.titleRowContent}>
						<TextWrapper type={'base'} style={{color:'#69CAF0'}}>
						Konsumen
						</TextWrapper>
					</View>
				</View>


				<View style={styleContent.itemRow}>
					<View style={styleContent.itemRowTitle}>
						<TextWrapper type={'base'}>
						Nama
						</TextWrapper>
					</View>

					<View style={styleContent.itemRowColon}>
						<TextWrapper type={'base'}>
						:
						</TextWrapper>
					</View>

					<View style={styleContent.itemRowContent}>
						<TextWrapper type={'base'}
							numberOfLines={1}
						>
						Dia yang namanya tidak boleh disebutkan
						</TextWrapper>
					</View>
				</View>

				<View style={[styleContent.itemRow, {alignItems:'center'}]}>

					<View style={styleContent.itemRowTitle}>
						<TextWrapper type={'base'}>
						Telepon
						</TextWrapper>
					</View>

					<View style={styleContent.itemRowColon}>
						<TextWrapper type={'base'}>
						:
						</TextWrapper>
					</View>

					<View style={styleContent.itemRowContent}>
						<View style={{flex:0.7}}>
							<TextWrapper type={'base'}>
							080989999
							</TextWrapper>
						</View>

						<View style={{flex:0.15, alignItems:'flex-end'}}>
							<TouchableOpacity onPress={() => Communications.phonecall('0123456789', true)}>
								<ResponsiveImage
								source={this.props.image.phone_icon.url}
								initWidth={this.props.image.phone_icon.width * this.props.image.phone_icon.multiplierWidth}
								initHeight={this.props.image.phone_icon.height * this.props.image.phone_icon.multiplierHeight}
								/>
							</TouchableOpacity>
						</View>

						<View style={{flex:0.15, alignItems:'flex-end'}}>
							<TouchableOpacity onPress={() => Communications.text('0123456789')}>
								<ResponsiveImage
								source={this.props.image.sms_icon.url}
								initWidth={this.props.image.sms_icon.width * this.props.image.sms_icon.multiplierWidth}
								initHeight={this.props.image.sms_icon.height * this.props.image.sms_icon.multiplierHeight}
								/>
							</TouchableOpacity>
						</View>
					</View>

				</View>

			</View>
		)
	}
}

export class Courier extends Component{

	_renderCourierSelector(){
		if (this.state.showCourierSelector) {
			return(
				<CourierContent/>
			);
		}
	}

	_onPressChangeCourierButton(){
		this.props.actionRoot.showBox('PartnerKliknKlin.BoxTypeC', {
	        itemList : ['asd'], 
	        titleText: 'Pilih Kurir', 
	        buttonPrimaryText: 'Konfirmasi', 
	        onPressButtonPrimary: () => {
		        // this.props.actionRoot.dismissBox();
		        // setTimeout(() => {
		        //     this.props.actionRoot.showBoxTypeA('Konfirmasi', 'Apakah anda yakin memilih kurir?', 'ya', 'tidak', () => {this.props.actionRoot.dismissBox();}, () => {this.props.actionRoot.dismissBox();})
		        // },500);
	        }
      });
	}

	_renderChangeCourierButton(){
		let {data} = this.props;
		let {orderStatus} = data;

		if (orderStatus == 'WaitingForPickup' || orderStatus == 'InLaundry'
			|| orderStatus == 'ReadyToBeDelivered') {
			return(
				<TouchableOpacity
					style={{
						borderRadius:10,
						backgroundColor:CONSTANT.COLOR.BLUE_A,
						width:Dimensions.get('window').width * 0.2,
						height:Dimensions.get('window').height * 0.035,
						alignItems:'center',
						justifyContent:'center',
					}}

					onPress={this._onPressChangeCourierButton.bind(this)}
				>
					<View>
						{this._renderCourierSelector.bind(this)}

						<TextWrapper fontWeight={'SemiBold'} type={'base'} style={{color:'white'}}>
							{'Ganti Kurir'}
						</TextWrapper>
					</View>
					
				</TouchableOpacity>
			);
		}
		
	}
	render(){
		let {type, data} = this.props;

		let {courierType, courierName} = data;

		return(
			<View style={[styleContent.itemDetail, {borderBottomWidth: this.props.endContent ? 0 : 1} ]}>
			
				<View style={styleContent.titleRow}>
					<View style={styleContent.titleRowImage}>
						<ResponsiveImage
							source={this.props.image.invoice_icon.url}
							initWidth={this.props.image.invoice_icon.width * this.props.image.invoice_icon.multiplierWidth}
							initHeight={this.props.image.invoice_icon.height * this.props.image.invoice_icon.multiplierHeight}
	                    />
					</View>

					<View style={styleContent.titleRowContent}>
						<TextWrapper type={'base'} style={{color:'#69CAF0'}}>
							{'Kurir ' + type}
						</TextWrapper>
					</View>

					<View style={styleContent.titleRowRightContent}>
						{this._renderChangeCourierButton()}
					</View>
				</View>


				<View style={[styleContent.itemRow, {alignItems:'center'}]}>

					<View style={styleContent.itemRowTitle}>
						<TextWrapper type={'base'}>
						Tipe
						</TextWrapper>
					</View>

					<View style={styleContent.itemRowColon}>
						<TextWrapper type={'base'}>
						:
						</TextWrapper>
					</View>

					<View style={styleContent.itemRowContent}>
						<TextWrapper type={'base'}>
						{courierType}
						</TextWrapper>
					</View>
				</View>

				<View style={[styleContent.itemRow, {alignItems:'center'}]}>

					<View style={styleContent.itemRowTitle}>
						<TextWrapper type={'base'}>
						Nama
						</TextWrapper>
					</View>

					<View style={styleContent.itemRowColon}>
						<TextWrapper type={'base'}>
						:
						</TextWrapper>
					</View>

					<View style={styleContent.itemRowContent}>
						<TextWrapper type={'base'}>
						{courierName}
						</TextWrapper>
					</View>
				</View>
			</View>
		);
	}
}

export class InvoiceSummary extends Component{

	render(){
		return(
			<View style={[styleContent.itemDetail, {borderBottomWidth: this.props.endContent ? 0 : 1} ]}>
			
				<View style={styleContent.titleRow}>
					<View style={styleContent.titleRowImage}>
						<ResponsiveImage
							source={this.props.image.invoice_icon.url}
							initWidth={this.props.image.invoice_icon.width * this.props.image.invoice_icon.multiplierWidth}
							initHeight={this.props.image.invoice_icon.height * this.props.image.invoice_icon.multiplierHeight}
	                    />
					</View>

					<View style={styleContent.titleRowContent}>
						<TextWrapper type={'base'} style={{color:'#69CAF0'}}>
							Pembayaran
						</TextWrapper>
					</View>

					<View style={styleContent.titleRowRightContent}>
						<TouchableOpacity
							style={{
								borderRadius:10,
								borderWidth:1.5,
								borderColor:CONSTANT.COLOR.BLUE_A,
								//elevation:2,
								width:Dimensions.get('window').width * 0.35,
								alignItems:'center',
							}}

							onPress={this.props.onPressInvoice}
						>
							<TextWrapper fontWeight={'SemiBold'} type={'base'} style={{color:CONSTANT.COLOR.BLUE_A}}>Lihat Detail Pesanan</TextWrapper>
						</TouchableOpacity>
					</View>
				</View>


				<View
					style={{
						alignItems:'center',
						marginVertical:Dimensions.get('window').height * 0.008,
					}}
				>
					<TextWrapper type={'bigtitle'} style={{fontWeight:'bold'}}>Rp 10.000</TextWrapper>
					<TextWrapper type={'base'} style={{marginTop:Dimensions.get('window').height * 0.005}}>Jumlah yang harus ditagih ke konsumen</TextWrapper>
				</View>
			</View>
		);
	}
}


const styleContent = StyleSheet.create({
	titleRow:{
		flexDirection:'row', 
		alignItems:'center',
	},
	titleRowImage:{
		flex:0.1,
	},
	titleRowContent:{
		flex:0.4,
	},
	titleRowRightContent:{
		flex:0.5,
		alignItems:'flex-end',
	},
	itemRow:{
		marginTop:Dimensions.get('window').height * 0.01,
		flexDirection:'row',
	},
	itemRowTitle:{
		flex:0.18,
	},
	itemRowColon:{
		flex:0.04,
	},
	itemRowContent:{
		flex:0.78,
		flexDirection:'row',
		alignItems:'center',
	},
	itemDetail:{
		paddingHorizontal:Dimensions.get('window').width * 0.025,
		paddingVertical:Dimensions.get('window').height * 0.01,
		borderColor:'#F0F0F0',
	},
});
