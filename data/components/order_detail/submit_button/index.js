import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';

import {TextWrapper, Button} from './../../../components';
import * as STRING from './../../../string';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class SubmitButton extends Component{
	constructor(props) {
		super(props);
	  
	    this.state = {
			buttonColor:this.props.buttonColor,
			buttonText:this.props.buttonText,
	    };
	  }

	componentWillMount(){
		this.configButton();
	}

	configButton(){
		if (this.props.buttonType == 'WaitingForPickup') {
			this.setState({
				buttonColor:'#61CDFF', 
				buttonText:STRING.ORDER_DETAIL.SUBMIT_BUTTON.GO_TO_PICKUP_POINT,
			});
		}
		else if (this.props.buttonType =='ReadyToBeDelivered') {
			this.setState({
				buttonColor:'#61CDFF', 
				buttonText:STRING.ORDER_DETAIL.SUBMIT_BUTTON.GO_TO_DELIVERY_POINT,
			});
		}
		else if (this.props.buttonType =='GoToPickupPoint') {
			this.setState({
				buttonColor:'#AEC93A', 
				buttonText:STRING.ORDER_DETAIL.SUBMIT_BUTTON.FINISH_PICKUP,
			});
		}
		else if (this.props.buttonType =='GoToDeliveryPoint') {
			this.setState({
				buttonColor:'#AEC93A', 
				buttonText:STRING.ORDER_DETAIL.SUBMIT_BUTTON.FINISH_DELIVERY,
			});
		}
		else if (this.props.buttonType =='TakeOrder') {
			this.setState({
				buttonColor:'#AEC93A', 
				buttonText:'Laundry Sudah Diterima',
			});
		}
		else if (this.props.buttonType =='InLaundry') {
			this.setState({
				buttonColor:'#AEC93A', 
				buttonText:'Laundry Telah Selesai',
			});
		}
	}

	onPress(){
		if (this.props.buttonType == 'WaitingForPickup') {
			this.props.actionOrderDetail.submitStartPickup(this.props.navigator);
		}
		else if (this.props.buttonType =='ReadyToBeDelivered') {
			this.props.actionOrderDetail.submitStartDelivery(this.props.navigator);
		}
		else if (this.props.buttonType =='GoToPickupPoint') {
			this.props.actionOrderDetail.submitFinishPickup(this.props.navigator);
		}
		else if (this.props.buttonType =='GoToDeliveryPoint') {
			this.props.actionOrderDetail.submitFinishDelivery(this.props.navigator);
		}
		else if (this.props.buttonType =='TakeOrder') {
			this.props.actionOrderDetail.submitLaundryAccepted(this.props.navigator);
		}
		else if (this.props.buttonType =='InLaundry') {
			this.props.actionOrderDetail.submitFinishDelivery(this.props.navigator);
		}
		else{
			this.props.onPress();
		}
	}

	render(){
		return(
			<View style={styles.buttonContainer}>
				<Button.TypeA
					onPress={this.onPress.bind(this)}
					buttonColor={this.state.buttonColor}
					buttonText={this.state.buttonText}
				/>
		    </View>
		);
	}
}

SubmitButton.propTypes = {
  buttonType: PropTypes.oneOf(['WaitingForPickup', 'GoToPickupPoint', 'GoToDeliveryPoint', 'ReadyToBeDelivered','TakeOrder', 'InLaundry']),
  buttonColor:PropTypes.string,
  buttonText:PropTypes.string,
  onPress:PropTypes.func
}

SubmitButton.defaultProps = {
  	buttonColor:'black',
  	buttonText:'button',
  	onPress: () => {},
}

const styles = StyleSheet.create({
  	buttonContainer: {
	    position:'absolute',
		bottom:0,
		height:Dimensions.get('window').height * 0.1,
		width:Dimensions.get('window').width,
		backgroundColor:'transparent',
		alignItems:'center',
		justifyContent:'center',
	},
});

function mapStateToProps(state, ownProps) {
	return {
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionOrderDetail: bindActionCreators(actions.orderDetail, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SubmitButton);