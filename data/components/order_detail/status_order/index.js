import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';

import {TextWrapper,} from './../../../components';

export default class StatusOrderLabel extends Component{
  constructor(props) {
    super(props);
    
      this.state = {
      buttonColor:this.props.buttonColor,
      buttonText:this.props.buttonText,
      };
    }

  labelColor(){
    var color = 'transparent';
    if(this.props.shipmentStatus == 'AKTIF') { color = '#69CAF0'; } // biru
    if(this.props.shipmentStatus == 'TUNDA') { color = '#FFC300'; } // yellow
    if(this.props.shipmentStatus == 'BATAL') { color = 'lightgray' ; } // gray
    if(this.props.shipmentStatus == 'TELAT') { color = '#BF2000'; } // red
    if(this.props.shipmentStatus == 'SELESAI') { color = '#AEC93A'; } // hijau

    return color;
  }

  render(){
    if (this.props.shipmentStatus != 'SELESAI') {
      return(
          <View style={[
                    styles.statusOrderContainer, 
                    {
                      backgroundColor:this.labelColor(),
                      borderColor:this.labelColor(),
                    }
              ]}
          >
            <TextWrapper type={'smallbase'} style={styles.statusOrderText}>
              {this.props.shipmentStatus}
            </TextWrapper>
        </View>
      );
    }
    return null;
  }
}

StatusOrderLabel.propTypes = {
  shipmentStatus: PropTypes.oneOf(['AKTIF', 'TUNDA', 'BATAL', 'TELAT', 'SELESAI']),
  statusColor:PropTypes.string,
}

StatusOrderLabel.defaultProps = {
    statusColor:'black',
}

const styles = StyleSheet.create({
  statusOrderContainer: {
    borderWidth:1,
    borderRadius:4,
    alignItems:'center',
    justifyContent:'center',
    width:Dimensions.get('window').width * 0.15,
    height:Dimensions.get('window').height * 0.04,
  },
  statusOrderText:{
    color: 'white', 
    fontWeight:'500'
  },
});