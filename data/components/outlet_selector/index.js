import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';
import {moderateScale} from 'react-native-size-matters';

import {TextWrapper, Label, Form, fontMaker, FontScale, Scaling} from 'app/data/components';
import * as CONSTANT from './../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../actions";


class OutletSelector extends Component{
  constructor(props) {
    super(props);
    
      this.state = {
      };
  }

  componentDidMount(){
  }

  _getSelectorData(){
    let result = [];
    try{
      let {userOutlet} = this.props;
      let {outletList} = userOutlet;

      for(let index in outletList){
        let obj = {}
        obj.id = outletList[index].id;
        obj.value = outletList[index].name;

        result.push(obj);
      }
    }
    catch(error){
      console.log(error, 'outlet_selector._getSelectorData')
    }
    return result;
  }

  _onChangeText(value, index, data){
    this.setState({value});
    this.loadOutletData(index);
  }

  loadOutletData(index = 0){
    let {actionUser, userOutlet, actionOrder, actionAuthentication} = this.props;
    actionUser.setUserOutlet(userOutlet.outletList, index);
    actionAuthentication.loadOutletData();
  }

  render(){
    try{
      let {userOutlet} = this.props;
      let {indexActiveOutlet} = userOutlet;
      let data = this._getSelectorData();
      let value = indexActiveOutlet != null ? this._getSelectorData()[userOutlet.indexActiveOutlet].value : null;

      return(
        <Form.DropdownField.MainDashboard
            {...this.props}
            data={data}
            value={value}
            label={'Tidak memiliki outlet'}
            onChangeText={this._onChangeText.bind(this)}
            width={'90%'}
        />
      );
    }
    catch(error){
      console.log(error, 'outlet_selector');
      return null;
    }
  }
}

OutletSelector.propTypes = {
}

OutletSelector.defaultProps = {
}

function mapStateToProps(state, ownProps) {
  return {
    userOutlet:state.rootReducer.user.outlet,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionUser: bindActionCreators(actions.user, dispatch),
    actionOrder: bindActionCreators(actions.order, dispatch),
    actionAuthentication: bindActionCreators(actions.authentication, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OutletSelector);

