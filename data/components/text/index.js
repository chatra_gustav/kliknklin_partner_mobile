
import React, { Component } from 'react';
import * as ReactNativeComponent from 'react-native';

import {fontCreator, Scaling} from './../../components';

import * as CONSTANT from './../../constant';

export default class Text extends Component{
  constructor(props){
    super(props);
    this.state = {
    };
  }

  render(){
      const styles = Scaling.ScaledSheet.create({
          fontStyle:fontCreator(this.props.fontOption),
      });
      
      return(
        <ReactNativeComponent.Text {...this.props} style={[styles.fontStyle, {color:CONSTANT.COLOR.BLACK}, this.props.textStyle]}>
            {this.props.children}
        </ReactNativeComponent.Text>
      );
  }
}



Text.defaultProps = {
  fontOption:{
    size:10, 
  },
}
