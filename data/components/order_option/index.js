import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions,
  Image
} from 'react-native';
import PropTypes from 'prop-types';
import {moderateScale} from 'react-native-size-matters';
import { TextField } from 'react-native-material-textfield';
import ResolveAssetSource from 'resolveAssetSource';

import {TextWrapper, Form, DelayOrder} from './../../components';
import * as STRING from './../../string';
import * as CONSTANT from './../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../actions";

class OrderOption extends Component{
	constructor(props) {
      super(props);
      this.state = {
        image:{
          option_icon:{
            width:0,
            height:0,
            multiplierWidth:0.3,
            multiplierHeight:0.3,
            url:require('./../../image/icon/option_icon.png'),
          },
        },
      }; 
  	}

	componentDidMount(){
	    this._getImageSize();
	}

 	_getImageSize(){
	    var image = this.state.image;

	    var image_option_icon = ResolveAssetSource(this.state.image.option_icon.url);

	    image.option_icon.width = image_option_icon.width * this.state.image.option_icon.multiplierWidth;;
	    image.option_icon.height = image_option_icon.height * this.state.image.option_icon.multiplierHeight;;
	    this.setState({image});
  	}

  	_getDataOption(){
  		let result = [
		   {value:'Hubungi CS'},
		];

  		try{
  			let {orderData, orderActiveType, shipmentActivePickup, shipmentActiveDelivery} = this.props;
			
			if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP) {
				let {orderlaundry} = orderData;
  				let {order_status_laundry_id} = orderlaundry;
  				let shipmentStatusPickup = shipmentActivePickup.shipment_status_id;
	  			let shipmentStatusDelivery = shipmentActiveDelivery.shipment_status_id;

				if (order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_PICKUP 
					//&& shipmentStatusPickup == CONSTANT.SHIPMENT_STATUS.ACTIVE
					&& shipmentStatusPickup != CONSTANT.SHIPMENT_STATUS.DELAY_CUSTOMER
					&& shipmentStatusPickup != CONSTANT.SHIPMENT_STATUS.DELAY_PARTNER
					){
					result = [
						{value:'Tunda'},
			            {value:'Hubungi CS'},
					];
				}
			}
			else if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY) {
				let {orderlaundry} = orderData;
	  			let {order_status_laundry_id} = orderlaundry;
	  			let shipmentStatusPickup = shipmentActivePickup.shipment_status_id;
	  			let shipmentStatusDelivery = shipmentActiveDelivery.shipment_status_id;

				if (order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_DELIVERY 
					//&& shipmentStatusDelivery == CONSTANT.SHIPMENT_STATUS.ACTIVE
					&& shipmentStatusDelivery != CONSTANT.SHIPMENT_STATUS.DELAY_CUSTOMER
					&& shipmentStatusDelivery != CONSTANT.SHIPMENT_STATUS.DELAY_PARTNER
				 ){
					result = [
						{value:'Tunda'},
			            {value:'Hubungi CS'},
					];
				}
			}
  		}
  		catch(error){
			console.log(error, 'order_option._getDataOption');
  		}

  		return result;
  	}

  	onChangeText(text){
  		if (text.toLowerCase() == 'hubungi cs') {
			this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.HelpContent', {helpType:'contact-us'})
  		}
  		else if (text.toLowerCase() == 'tunda') {
  			this._delayOrder();
  		}
  	}

  	_delayOrder(){
  		let {actionNavigator, navigator, actionOrder, delayOrderReasonList} = this.props;
		
		this.props.actionNavigator.showLightBox('PartnerKliknKlin.ItemSelection', {
          data:delayOrderReasonList,
          categoryToFind:'info',
          titleBar:true,
          title:'Pilih Alasan Penundaan',
          onPressButtonPrimary:(selectedItemIndex) => {
            actionNavigator.dismissLightBox();
              
            setTimeout(() => {
	          actionOrder.submitDelayShipment(navigator, delayOrderReasonList[selectedItemIndex].id, delayOrderReasonList[selectedItemIndex].info, '', this._delayOrder.bind(this));
	        }, 350)
          }
        });
  	}

	render(){
		return(
			 <Form.DropdownField.TypeB
			 	ref={(component) => {this.dropdown_field = component}}
		        label={''}
		        data={this._getDataOption()}
		        baseColor={'transparent'}
		        textColor={'transparent'}
		        disabledItemColor={'black'}
		        selectedItemColor={'black'}
		        itemColor={'black'}
		        labelHeight={0}
		        labelPadding={0}
		        value={''}
		        dropdownOffset={{
		        	top:moderateScale(32),
		        	left:moderateScale(-70),
		        }}
		        containerStyle={{
		            width:moderateScale(15),
		            height:moderateScale(30),
		            marginTop:moderateScale(10),
		            marginRight:moderateScale(7.5),
		        }}
		        pickerStyle={{
		        	width:moderateScale(125),
		        }}
		        inputContainerStyle={{
		        }}

		        onChangeText={this.onChangeText.bind(this)}

		        renderAccessory={() =>{
		            return(
			                <Image
			                    style={{width: this.state.image.option_icon.width, height: this.state.image.option_icon.height}}
			                  	source={this.state.image.option_icon.url}
			                />
		              );
		            }
		        }
		    />
		);
	}
}

OrderOption.propTypes = {
  buttonColor:PropTypes.string,
  buttonText:PropTypes.string,
  containerStyle:PropTypes.object,
  buttonStyle:PropTypes.object,
  buttonTextStyle:PropTypes.object,
  onPress:PropTypes.func
}

OrderOption.defaultProps = {
  	buttonColor:'black',
  	buttonText:'button',
  	containerStyle:{},
  	buttonStyle:{},
  	buttonTextStyle:{},
  	onPress: () => {},
}

const styles = StyleSheet.create({
  	buttonContainer: {
		height:Dimensions.get('window').height * 0.1,
		backgroundColor:'transparent',
		alignItems:'center',
		justifyContent:'center',
	},
	button:{
		borderRadius:50,
		width:Dimensions.get('window').width * 0.75,
		height:Dimensions.get('window').height * 0.06,
		alignItems:'center',
		justifyContent:'center',
	},
	buttonText:{
		color:'white',
	},
});


function mapStateToProps(state, ownProps) {
	return {
		userRole:state.rootReducer.user.role,
		orderData:state.rootReducer.order.active.data,
		orderActiveType:state.rootReducer.order.active.orderActiveType,
		delayOrderReasonList:state.rootReducer.masterData.delayOrderReasonList,
		shipmentActivePickup:state.rootReducer.order.shipmentActivePickup,
		shipmentActiveDelivery:state.rootReducer.order.shipmentActiveDelivery,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionNavigator: bindActionCreators(actions.navigator, dispatch),
		actionMasterData: bindActionCreators(actions.masterData, dispatch),
		actionOrder: bindActionCreators(actions.order, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderOption);

