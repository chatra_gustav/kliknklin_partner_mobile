import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity, Linking, ScrollView, Platform, BackHandler} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, DateFormat, ShipmentScheduleListCourier, OutletSelector, Label, Scaling} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class NoDataFound extends Component {
	static defaultProps = {
	  title:'',
	  message:'',
	}
	constructor(props) {
        super(props);
        this.state = {
        };  
    }

    componentWillMount(){
    }

	render(){
		try{
			return(
				<View
						style={{
							flex:1,
							width:'100%',
							height:'100%',
							paddingTop:Scaling.verticalScale(CONSTANT.STYLE.CONTAINER.BOX_DASHBOARD.PADDING_TOP),
							paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
						}}
					>
						<View
							style={{
								flex:0.25,
								alignItems:'center',
								justifyContent:'flex-end',
							}}
						>
							<Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>{this.props.title}</Text>
						</View>

						<View
							style={{
								flex:0.35,
								alignItems:'center',
								justifyContent:'center',
							}}
						>
							<Image
					            source={CONSTANT.IMAGE_URL.NO_OUTLET_ICON}
					            style={{
						            width:Scaling.scale(200),
						            height:Scaling.verticalScale(200),
						            resizeMode:'contain',
					            }}
					        /> 
						</View>

						<View
							style={{
								flex:0.4,
								alignItems:'center',
							}}
						>
							<Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>
								{this.props.message}
							</Text>
						</View>
						
						
					</View>
			);
		}
		catch(error){
			console.log(error, 'dashboard.summary_order');
			return null;
		}	
	}
}



function mapStateToProps(state, ownProps) {
	return {
		userRole:state.rootReducer.user.role,
		summaryOrder:state.rootReducer.order.summary,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(NoDataFound);

