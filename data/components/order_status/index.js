import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';

import {TextWrapper, Label} from './../../components';
import * as CONSTANT from './../../constant';
import * as STRING from './../../string';

export default class OrderStatus extends Component{
  static propTypes = {
    type: PropTypes.oneOf(['order', 'complaint']),
    statusColor:PropTypes.string,
  }

  static defaultProps = {
    containerStyle:{},
    type:'order'
  }

  constructor(props) {
      super(props);
      this.state = {
      };
    }

  _getData(){
    let result = {
        labelColor:'transparent',
        labelText:'label',
        labelTextColor:'transparent',
    };

    let {userRole, orderStatus, shipmentStatus, complaintStatus, onlineFlag} = this.props;

    if (userRole == CONSTANT.USER_ROLE.COURIER) {
        if(shipmentStatus == 'AKTIF') { 
          result.labelColor =  CONSTANT.COLOR.BLUE_STATUS_BACKGROUND; 
          result.labelTextColor =  CONSTANT.COLOR.BLUE_STATUS_TEXT; 
          result.labelText = 'AKTIF';
        } // biru
        else if(shipmentStatus == CONSTANT.SHIPMENT_STATUS.DELAY_CUSTOMER) {
          result.labelColor =  CONSTANT.COLOR.ORANGE_STATUS_BACKGROUND; 
          result.labelTextColor =  CONSTANT.COLOR.ORANGE_STATUS_TEXT;
          result.labelText = 'TUNDA - CUSTOMER';
        } // yellow
         else if(shipmentStatus == CONSTANT.SHIPMENT_STATUS.DELAY_PARTNER) {
          result.labelColor =  CONSTANT.COLOR.ORANGE_STATUS_BACKGROUND; 
          result.labelTextColor =  CONSTANT.COLOR.ORANGE_STATUS_TEXT;
          result.labelText = 'TUNDA - PARTNER';
        } // yellow
        else if(shipmentStatus == 'BATAL') { 
          result.labelColor =  CONSTANT.COLOR.GRAY_STATUS_BACKGROUND; 
          result.labelTextColor =  CONSTANT.COLOR.GRAY_STATUS_TEXT;
          result.labelText = 'BATAL';
        } // gray
        else if(shipmentStatus == CONSTANT.SHIPMENT_STATUS.LATE) { 
          if (orderStatus == CONSTANT.ORDER_STATUS.IN_PROGRESS_PICKUP) {
            result.labelColor =  CONSTANT.COLOR.RED_STATUS_BACKGROUND; 
            result.labelTextColor =  CONSTANT.COLOR.RED_STATUS_TEXT;
            result.labelText = 'SEDANG DIAMBIL - TELAT'; 
          }
          else if (orderStatus == CONSTANT.ORDER_STATUS.IN_PROGRESS_DELIVERY) {
            result.labelColor =  CONSTANT.COLOR.RED_STATUS_BACKGROUND; 
            result.labelTextColor =  CONSTANT.COLOR.RED_STATUS_TEXT;
            result.labelText = 'SEDANG DIANTAR - TELAT'; 
          }
          
        } // red
        else if(shipmentStatus == 'SELESAI') { 
          result.labelColor =  CONSTANT.COLOR.GREEN_STATUS_BACKGROUND; 
          result.labelTextColor =  CONSTANT.COLOR.GREEN_STATUS_TEXT;
          result.labelText = 'SELESAI';
        } // hijau
        // else if(shipmentStatus == CONSTANT.SHIPMENT_STATUS.IN_PROGRESS){
  }
  else{
      if (complaintStatus) {

        if (complaintStatus == CONSTANT.COMPLAINT_STATUS.IN_PROGRESS) {
          result.labelColor =  CONSTANT.COLOR.BLUE_STATUS_BACKGROUND; 
          result.labelTextColor =  CONSTANT.COLOR.BLUE_STATUS_TEXT; 
          result.labelText = STRING.COMPLAINT_STATUS.IN_PROGRESS.toUpperCase();
        }
        else if (complaintStatus == CONSTANT.COMPLAINT_STATUS.SOLVED) {
          result.labelColor =  CONSTANT.COLOR.GREEN_STATUS_BACKGROUND; 
          result.labelTextColor =  CONSTANT.COLOR.GREEN_STATUS_TEXT;
          result.labelText = STRING.COMPLAINT_STATUS.SOLVED.toUpperCase();
        }
        else if (complaintStatus == CONSTANT.COMPLAINT_STATUS.DONE) {
          result.labelColor =  CONSTANT.COLOR.GREEN_STATUS_BACKGROUND; 
          result.labelTextColor =  CONSTANT.COLOR.GREEN_STATUS_TEXT;
          result.labelText = STRING.COMPLAINT_STATUS.DONE.toUpperCase();
        }
        else if (complaintStatus == CONSTANT.COMPLAINT_STATUS.CANCEL) {
          result.labelColor =  CONSTANT.COLOR.GRAY_STATUS_BACKGROUND; 
          result.labelTextColor =  CONSTANT.COLOR.GRAY_STATUS_TEXT;
          result.labelText = STRING.COMPLAINT_STATUS.CANCEL.toUpperCase();
        }
      }
      else{
        if (orderStatus == CONSTANT.ORDER_STATUS.WAIT_PICKUP) {
            if(shipmentStatus == CONSTANT.SHIPMENT_STATUS.LATE) { 
              result.labelColor =  CONSTANT.COLOR.RED_STATUS_BACKGROUND; 
              result.labelTextColor =  CONSTANT.COLOR.RED_STATUS_TEXT;
              result.labelText = 'TELAT'; 
            }
            else if(shipmentStatus == CONSTANT.SHIPMENT_STATUS.ACTIVE) { 
              result.labelColor =  CONSTANT.COLOR.BLUE_STATUS_BACKGROUND; 
              result.labelTextColor =  CONSTANT.COLOR.BLUE_STATUS_TEXT;
              result.labelText = 'MENUNGGU DIAMBIL - AKTIF'; 
            } 
            else{
              result.labelColor =  CONSTANT.COLOR.BLUE_STATUS_BACKGROUND; 
              result.labelTextColor =  CONSTANT.COLOR.BLUE_STATUS_TEXT;
              result.labelText = 'MENUNGGU DIAMBIL';
            }
        }
        else if (orderStatus == CONSTANT.ORDER_STATUS.WAIT_OUTLET_CONFIRMATION) {
          result.labelColor =  CONSTANT.COLOR.BLUE_STATUS_BACKGROUND; 
          result.labelTextColor =  CONSTANT.COLOR.BLUE_STATUS_TEXT;
          result.labelText = 'MENUNGGU KONFIRMASI';
        }
        else if (orderStatus == CONSTANT.ORDER_STATUS.CANNOT_FIND_PARTNER) {
          result.labelColor =  CONSTANT.COLOR.GRAY_STATUS_BACKGROUND; 
          result.labelTextColor =  CONSTANT.COLOR.GRAY_STATUS_TEXT;
          result.labelText = 'DITOLAK';
        }
        else if (orderStatus == CONSTANT.ORDER_STATUS.IN_PROGRESS_PICKUP) {
            if(shipmentStatus == CONSTANT.SHIPMENT_STATUS.LATE) { 
              result.labelColor =  CONSTANT.COLOR.RED_STATUS_BACKGROUND; 
              result.labelTextColor =  CONSTANT.COLOR.RED_STATUS_TEXT;
              result.labelText = 'SEDANG DIAMBIL - TELAT'; 
            }
            else if(shipmentStatus == CONSTANT.SHIPMENT_STATUS.DELAY_CUSTOMER) {
              result.labelColor =  CONSTANT.COLOR.ORANGE_STATUS_BACKGROUND; 
              result.labelTextColor =  CONSTANT.COLOR.ORANGE_STATUS_TEXT;
              result.labelText = 'TUNDA - CUSTOMER';
            } // yellow
             else if(shipmentStatus == CONSTANT.SHIPMENT_STATUS.DELAY_PARTNER) {
              result.labelColor =  CONSTANT.COLOR.ORANGE_STATUS_BACKGROUND; 
              result.labelTextColor =  CONSTANT.COLOR.ORANGE_STATUS_TEXT;
              result.labelText = 'TUNDA - PARTNER';
            } // yellow
            else{
              result.labelColor =  CONSTANT.COLOR.BLUE_STATUS_BACKGROUND; 
              result.labelTextColor =  CONSTANT.COLOR.BLUE_STATUS_TEXT;
              result.labelText = 'SEDANG DIAMBIL';
            }
        }
        else if (orderStatus == CONSTANT.ORDER_STATUS.DONE_PICKUP) {
            result.labelColor =  CONSTANT.COLOR.BLUE_STATUS_BACKGROUND; 
            result.labelTextColor =  CONSTANT.COLOR.BLUE_STATUS_TEXT; 
            result.labelText = 'DIBAWA KURIR';
        }
        else if (orderStatus == CONSTANT.ORDER_STATUS.IN_PROGRESS_LAUNDRY) {
            result.labelColor =  CONSTANT.COLOR.BLUE_STATUS_BACKGROUND; 
            result.labelTextColor =  CONSTANT.COLOR.BLUE_STATUS_TEXT;
            result.labelText = 'DICUCI';

            //dicuci telat
        }
        else if (orderStatus == CONSTANT.ORDER_STATUS.DONE_LAUNDRY) {
            if (onlineFlag) {
                if(shipmentStatus == CONSTANT.SHIPMENT_STATUS.LATE) { 
                  result.labelColor =  CONSTANT.COLOR.RED_STATUS_BACKGROUND; 
                  result.labelTextColor =  CONSTANT.COLOR.RED_STATUS_TEXT;
                  result.labelText = 'SIAP DIANTAR - TELAT'; 
                }
                else if(shipmentStatus == CONSTANT.SHIPMENT_STATUS.ACTIVE) { 
                  result.labelColor =  CONSTANT.COLOR.BLUE_STATUS_BACKGROUND; 
                  result.labelTextColor =  CONSTANT.COLOR.BLUE_STATUS_TEXT;
                  result.labelText = 'SIAP DIANTAR - AKTIF'; 
                } 
                else{
                  result.labelColor =  CONSTANT.COLOR.BLUE_STATUS_BACKGROUND; 
                  result.labelTextColor =  CONSTANT.COLOR.BLUE_STATUS_TEXT;
                  result.labelText = 'SIAP DIANTAR'; 
                }
            }
            else{
              result.labelColor =  CONSTANT.COLOR.GREEN_STATUS_BACKGROUND; 
              result.labelTextColor =  CONSTANT.COLOR.GREEN_STATUS_TEXT;
              result.labelText = 'SUDAH DIAMBIL';
            }
            
        }
        else if (orderStatus == CONSTANT.ORDER_STATUS.AFTER_SUBMIT_POS) {
            result.labelColor =  CONSTANT.COLOR.BLUE_STATUS_BACKGROUND; 
            result.labelTextColor =  CONSTANT.COLOR.BLUE_STATUS_TEXT;
            result.labelText = 'BARU DIBUAT';
        }
        else if (orderStatus == CONSTANT.ORDER_STATUS.IN_PROGRESS_DELIVERY) {
            if(shipmentStatus == CONSTANT.SHIPMENT_STATUS.LATE) { 
              result.labelColor =  CONSTANT.COLOR.RED_STATUS_BACKGROUND; 
              result.labelTextColor =  CONSTANT.COLOR.RED_STATUS_TEXT;
              result.labelText = 'SEDANG DIANTAR - TELAT'; 
            }
            else if(shipmentStatus == CONSTANT.SHIPMENT_STATUS.DELAY_CUSTOMER) {
              result.labelColor =  CONSTANT.COLOR.ORANGE_STATUS_BACKGROUND; 
              result.labelTextColor =  CONSTANT.COLOR.ORANGE_STATUS_TEXT;
              result.labelText = 'TUNDA - CUSTOMER';
          } // yellow
           else if(shipmentStatus == CONSTANT.SHIPMENT_STATUS.DELAY_PARTNER) {
              result.labelColor =  CONSTANT.COLOR.ORANGE_STATUS_BACKGROUND; 
              result.labelTextColor =  CONSTANT.COLOR.ORANGE_STATUS_TEXT;
              result.labelText = 'TUNDA - PARTNER';
          } // yellow
            else{
              result.labelColor =  CONSTANT.COLOR.BLUE_STATUS_BACKGROUND; 
              result.labelTextColor =  CONSTANT.COLOR.BLUE_STATUS_TEXT;
              result.labelText = 'SEDANG DIANTAR';
            }
        }
        else if (orderStatus == CONSTANT.ORDER_STATUS.DONE_DELIVERY) {
            result.labelColor =  CONSTANT.COLOR.GREEN_STATUS_BACKGROUND; 
            result.labelTextColor =  CONSTANT.COLOR.GREEN_STATUS_TEXT;
            result.labelText = 'SELESAI DIANTAR';
        }
        else if (orderStatus == CONSTANT.ORDER_STATUS.APPLIED_COMPLAIN) {
            result.labelColor =  CONSTANT.COLOR.BLUE_STATUS_BACKGROUND; 
            result.labelTextColor =  CONSTANT.COLOR.BLUE_STATUS_TEXT; 
            result.labelText = 'DIAJUKAN KOMPLAIN';
        }
        else if (orderStatus == CONSTANT.ORDER_STATUS.DONE_ORDER) {
            result.labelColor =  CONSTANT.COLOR.GREEN_STATUS_BACKGROUND; 
            result.labelTextColor =  CONSTANT.COLOR.GREEN_STATUS_TEXT;
            result.labelText = 'PESANAN SELESAI';
        }
        else if(orderStatus == CONSTANT.ORDER_STATUS.CANCEL){
            result.labelColor =  CONSTANT.COLOR.GRAY_STATUS_BACKGROUND; 
            result.labelTextColor =  CONSTANT.COLOR.GRAY_STATUS_TEXT;
            result.labelText = 'BATAL';
        }
      }
  }
    
    return result;
  }

  render(){
    let {labelColor, labelText, labelTextColor} = this._getData();
    return(
        <Label.TypeA 
          labelText={labelText}
          labelColor={labelColor}
          labelTextColor={labelTextColor}
        />
    );
  }
}

OrderStatus.propTypes = {
  //shipmentStatus: PropTypes.oneOf(['AKTIF', 'TUNDA', 'BATAL', 'TELAT', 'SELESAI', 'NONE']),
  statusColor:PropTypes.string,
}

OrderStatus.defaultProps = {
  statusColor:'black',
}

