import React, { Component } from 'react';
import {
  NativeModules,
  Animated, 
  StyleSheet, 
  View, 
  Dimensions, 
  TouchableOpacity, 
  ActivityIndicator, 
  TouchableHighlight,
  Image
} from 'react-native';
import { LargeList } from "react-native-largelist-v3";
import {moderateScale} from 'react-native-size-matters';

import {Text, Scaling, Form, Button, Label} from 'app/data/components';
import * as CONSTANT from 'app/data/constant';


//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

const { UIManager } = NativeModules;
UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

class Cancel extends Component {
  static defaultProps = {
    onPress:()=>{},
    containerStyle:{},
    size:'normal',
  }
  constructor(props) {
    super(props);
  
    this.state = {
    };
   
  }

  render() {
    return(
      <TouchableOpacity
        style={[{
            width:Scaling.scale(50),
            height:Scaling.scale(50),
            justifyContent:'center',
            alignItems:'center',
            position:'absolute',
            right:'1.5%',
            top:'1.5%',
        },
        this.props.containerStyle
        ]}

        onPress={() => {
          this.props.onPress();

          this.props.actionNavigator.dismissLightBox();
          this.props.actionNavigator.dismissModal();
        }}
      >
        <Image
            source={CONSTANT.IMAGE_URL.CANCEL_ICON}
            style={{
              width:Scaling.scale(this.props.size == 'big' ? 40 : 35),
              height:Scaling.scale(this.props.size == 'big' ? 40 :35),
              resizeMode:'contain',
            }}
        /> 
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
});



function mapStateToProps(state, ownProps) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Cancel);
