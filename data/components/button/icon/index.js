import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';

import {Text, Scaling, fontCreator} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class Icon extends Component{
	static defaultProps = {
	    buttonColor:CONSTANT.COLOR.WHITE,
	  	buttonText:'button',
	  	buttonSize:CONSTANT.STYLE.BUTTON.MEDIUM,
	  	buttonBorderColor:CONSTANT.COLOR.LIGHT_GRAY,
	  	fontOption:CONSTANT.TEXT_OPTION.BUTTON,
	  	containerStyle:{},
	  	buttonStyle:{},
	  	buttonTextStyle:{},
	  	onPress: () => {},
	}

	render(){
		const width = this.props.buttonSize.WIDTH;
		const height = this.props.buttonSize.HEIGHT;
		const radius = this.props.buttonSize.RADIUS;

		const styles = Scaling.ScaledSheet.create({
	        button:{
	        	width:Scaling.scale(64.38),
				height:Scaling.scale(64.38),
				borderRadius:Scaling.scale(18),
				borderWidth:Scaling.moderateScale(2),
				borderColor:CONSTANT.COLOR.LIGHT_GRAY,
				backgroundColor:CONSTANT.COLOR.WHITE,
				alignItems:'center',
				justifyContent:'center',
			},
	    });

		return(
			<TouchableOpacity
				style={[styles.button, this.props.buttonStyle]}
				onPress={this.props.onPress}
		    >
		    	{this.props.children}
		    </TouchableOpacity>
		);
	}
}

function mapStateToProps(state, ownProps) {
	return {
	};
}

function mapDispatchToProps(dispatch) {
	return {
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Icon);


