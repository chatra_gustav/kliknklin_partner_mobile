import React from 'react';
import {StyleSheet, View, Dimensions, TouchableOpacity} from 'react-native';
import {moderateScale} from 'react-native-size-matters';

import {TextWrapper} from './../../../components';
import * as CONSTANT from './../../../constant';

export default class TypeB extends React.Component {

  render() {
    return (
      <TouchableOpacity 
          style={[styles.container, this.props.containerStyle]} 
          onPress={this.props.onPress}
      >
          <View style={[styles.containerContent, this.props.contentContainerStyle]}>
              {this.props.buttonImage}

              <TextWrapper type={this.props.buttonTextType} style={[{color:CONSTANT.COLOR.BLUE_A}, this.props.buttonTextStyle]}>{this.props.buttonText}</TextWrapper>
              

              <View style={[styles.containerIconRight]}>
                {this.props.iconRight}
              </View>
          </View>
      </TouchableOpacity>
    );
  }
}

TypeB.defaultProps = {
  buttonImage:null,
  buttonText:'text',
  iconRight:null,
  containerStyle:{},
  contentContainerStyle:{},
  buttonTextStyle:{},
  buttonTextType:'smallertitle',
}

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width * 0.85,
    height: Dimensions.get('window').height * 0.15,
    backgroundColor:'#FFF',
    elevation:3,
    borderRadius:5,
    margin:moderateScale(5),
  },
  containerContent:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row',
    margin:moderateScale(15),
  },
  containerIconRight:{
    position:'absolute',
    right:moderateScale(20),
  },
});

