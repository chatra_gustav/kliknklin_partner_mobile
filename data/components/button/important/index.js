import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';

import {Text, Label, toCurrency, Scaling} from 'app/data/components';
import * as CONSTANT from 'app/data/constant';
import * as STRING from 'app/data/string';

export default class Important extends Component{
  static propTypes = {
    shipmentStatus: PropTypes.oneOf(['AKTIF', 'TUNDA', 'BATAL', 'TELAT', 'SELESAI', 'NONE']),
    statusColor:PropTypes.string,
  }

  static defaultProps = {
    statusColor:'black',
    containerStyle:{},
    showImportantSign:false,
    onPress:()=>{},
  }

  constructor(props) {
      super(props);
      this.state = {

      };
  }
  
  componentWillMount(){
  }


  render(){
    let {showImportantSign, onPress} = this.props;

    if (showImportantSign) {
      return(
        <TouchableOpacity
          {...this.props}
          
          style={[{
            height:Scaling.scale(20),
            width:Scaling.verticalScale(75),
            borderRadius:Scaling.moderateScale(25),
            backgroundColor:CONSTANT.COLOR.RED_STATUS_TEXT,
            alignItems:'center',
            justifyContent:'center',
          },this.props.containerStyle]}

          onPress={onPress.bind(this)}
        >
          <Text
            fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}
            textStyle={{
              color:CONSTANT.COLOR.WHITE,
            }}
          >
            {'Penting!'}
          </Text>
        </TouchableOpacity>
      );
    }
    return null;
  }
}

