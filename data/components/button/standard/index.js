import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';

import {Text, Scaling, fontCreator} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class Standard extends Component{
	static defaultProps = {
	    buttonColor:CONSTANT.COLOR.GREEN,
	  	buttonText:'button',
	  	buttonSize:CONSTANT.STYLE.BUTTON.MEDIUM,
	  	buttonBorderColor:'transparent',
	  	fontOption:CONSTANT.TEXT_OPTION.BUTTON,
	  	buttonStyle:{},
	  	buttonTextStyle:{},
	  	disabled:false,
	  	onPress: () => {},
	}

	render(){
		const width = this.props.buttonSize.WIDTH;
		const height = this.props.buttonSize.HEIGHT;
		const radius = this.props.buttonSize.RADIUS;

		const styles = Scaling.ScaledSheet.create({
	        button:{
				borderRadius:Scaling.scale(radius),
				width:Scaling.scale(width),
				height:Scaling.verticalScale(height),
				alignItems:'center',
				justifyContent:'center',
			},
	    });

		return(
			<TouchableOpacity
				style={[styles.button,{
 					backgroundColor:this.props.buttonColor,
 					borderColor:this.props.buttonBorderColor,
 					borderWidth:Scaling.moderateScale(1),
				}, this.props.buttonStyle]}
				onPress={this.props.onPress}
				disabled={this.props.disabled}
		    >
		    	{this.props.children ? this.props.children : (
					
					<Text
			    		fontOption={this.props.fontOption}
			    		textStyle={[{color:CONSTANT.COLOR.WHITE}, this.props.buttonTextStyle]}
			    	>
						{this.props.buttonText}
			    	</Text>

		    	)}
		    </TouchableOpacity>
		);
	}
}

function mapStateToProps(state, ownProps) {
	return {
	};
}

function mapDispatchToProps(dispatch) {
	return {
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Standard);


