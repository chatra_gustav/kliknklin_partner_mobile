import React from 'react';
import {StyleSheet, View, Dimensions, TouchableOpacity} from 'react-native';

import {Scaling} from './../../../components';
import {moderateScale} from 'react-native-size-matters';

export default class TypeC extends React.Component {

  render() {
    return (
      <TouchableOpacity 
          style={[styles.container, this.props.containerStyle]} 
          onPress={this.props.onPress}
      >
          {this.props.children}
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    minHeight: Dimensions.get('window').height * 0.225,
    backgroundColor:'#FFF',
    elevation:3,
    padding:Scaling.moderateScale(5),
  },
});

