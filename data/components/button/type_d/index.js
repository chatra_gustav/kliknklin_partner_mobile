import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';

import {moderateScale} from 'react-native-size-matters';
import {TextWrapper,} from './../../../components';
import * as STRING from './../../../string';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as appActions from "./../../../actions";

export default class TypeD extends Component{

	render(){
		return(
			<View style={[styles.buttonContainer,this.props.containerStyle]}>
				<TouchableOpacity
					style={[styles.button, {backgroundColor:this.props.buttonColor}, this.props.buttonStyle]}
					onPress={this.props.onPress}
			    >
			    	<TextWrapper
			    		type={this.props.buttonTextType} 
			    		fontWeight={'SemiBold'}
						style={[styles.buttonText, this.props.buttonTextStyle]}
			    	>
						{this.props.buttonText}
			    	</TextWrapper>
			    </TouchableOpacity>
		    </View>
		);
	}
}

TypeD.propTypes = {
  buttonColor:PropTypes.string,
  buttonText:PropTypes.string,
  //containerStyle:PropTypes.object,
  buttonStyle:PropTypes.object,
  buttonTextStyle:PropTypes.array,
  onPress:PropTypes.func
}

TypeD.defaultProps = {
  	buttonColor:'black',
  	buttonText:'button',
  	buttonTextType:'smallcontent',
  	containerStyle:{},
  	buttonStyle:{},
  	buttonTextStyle:[],
  	onPress: () => {},
}

const styles = StyleSheet.create({
  	buttonContainer: {
		alignItems:'center',
		justifyContent:'center',
	},
	button:{
		borderRadius:50,
		width:Dimensions.get('window').width * 0.75,
		height:Dimensions.get('window').height * 0.06,
		alignItems:'center',
		justifyContent:'center',
	},
	buttonText:{
		color:'white',
		fontSize: moderateScale(10)
	},
});
