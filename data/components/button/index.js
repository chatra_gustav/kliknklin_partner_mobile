import TypeA from './type_a';
import TypeB from './type_b';
import TypeC from './type_c';
import TypeD from './type_d';
import Standard from './standard';
import Icon from './icon';
import Cancel from './cancel';
import Important from './important';

export {
	TypeA,
	TypeB,
	TypeC,
	TypeD,
	Standard,
	Icon,
	Cancel,
	Important,
}