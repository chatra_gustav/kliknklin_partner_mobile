export default function toCurrency (n, currency) {
    return currency + " " + Number(n).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
}