import React from 'react';
import {StyleSheet, View, Text, Dimensions, TouchableOpacity} from 'react-native';

export class UpdateBox extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          updateString:{
            newversionavailable :{
              title:'Versi baru telah tersedia',
              content: 'Ingin mengupdatenya sekarang?',
            },
            forceupdate :{
              title:'Versi baru telah tersedia',
              content: 'Anda harus update aplikasi untuk melanjutkan',
            }
          },
          buttonString:{
            newversionavailable: {
              button1:'Lain Kali',
              button2:'Update',
            },
            forceupdate:{
              button1:'Update',
            },
          },
      }; 
  }

  componentWillMount(){
    console.log('here');
  }

  _configUpdate(){
    var updateType = this.props.updateType ? this.props.updateType : 'newversionavailable';
    var result = updateType;

    for (key in this.state.updateString) {
      if (updateType == key) {
        var result = this.state.updateString[key];
      }
    }

    return result;
  }

  _renderButtonUpdate(){
    if (this.props.updateType == 'newversionavailable') {
      return(
        <View style={{flex: 2, flexDirection:'row',justifyContent:'space-around'}}>
            <TouchableOpacity
              onPress={() => this.props.navigator.dismissLightBox()}
            >
              <View>
                <Text>{this.state.buttonString.newversionavailable.button1}</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.props.navigator.dismissLightBox()}
            >
            <View>
                <Text>{this.state.buttonString.newversionavailable.button2}</Text>
              </View>
            </TouchableOpacity>
        </View>
      );
    }
    else{
      return(
        <View style={{flex: 2, flexDirection:'row',justifyContent:'space-around'}}>
            <TouchableOpacity
              onPress={() => this.props.navigator.dismissLightBox()}
            >
              <View>
                <Text>{this.state.buttonString.forceupdate.button1}</Text>
              </View>
            </TouchableOpacity>
        </View>
      );
    }
  }
  
  render() {
    var update = this._configUpdate();
    return (
      <View style={styles.container}>
        <View style={{flex: 8,alignItems:'center'}}>
          <Text style={styles.title}>{update.title}</Text>
          <Text style={styles.content}>{update.content}</Text>
        </View>
        
        {this._renderButtonUpdate()}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width * 0.7,
    height: Dimensions.get('window').height * 0.3,
    backgroundColor: '#ffffff',
    borderRadius: 5,
    borderColor: "#FAF",
    borderWidth:1,
    padding: 16,
  },
  title: {
    fontSize: 17,
    fontWeight: '700',
  },
  content: {
    marginTop: 8,
  },
});