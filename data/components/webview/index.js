import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        ScrollView
        } from 'react-native';

import { WebView } from 'react-native-webview';
import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Container, CustomView} from 'app/data/components';
import * as CONSTANT from 'app/data/constant';


import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class WebviewContent extends Component {
    static defaultProps = {
        title: '',
        url:'',
        needParameter:false,
        method:'GET',
        destination:null,
    }
    constructor(props){
        super(props);
        this.state = {
        };
    }

    render(){
        let {userData, userOutlet, needParameter, url, method, destination} = this.props;
        let source = {};
        if (needParameter) {
            source.uri = url;
            source.method = method;
            let body = 'access_token='+userData.accessToken+'&outlet_id='+userOutlet.outletList[userOutlet.indexActiveOutlet].id;
            if (destination) {
                body += '&destination='+destination;
            }
            source.body = body;
        }
        else{
            source.uri = url;
        }

        return(
            <LayoutPrimary
                showBackButton={true}
                showTabBar={false}
                showTitle={true}
                navigator={this.props.navigator}
                title={this.props.title}
            >
                <WebView
                    startInLoadingState={true}
                    renderLoading={() => {
                            return (
                                <LoadingContent/>
                            )
                        }
                    }
                    source={source}
                    style={{flex:1}}
                />
                
            </LayoutPrimary>
        );
    }
}

class LoadingContent extends Component{

    render(){
        return (
            <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>{'Memuat Konten...'}</Text>
            </View>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        userData:state.rootReducer.user.data,
        userOutlet:state.rootReducer.user.outlet,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actionNavigator: bindActionCreators(actions.navigator, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(WebviewContent);


