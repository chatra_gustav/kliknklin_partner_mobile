import TypeA from './type_a';
import TypeImportant from './type_important';
import SimpleInput from './simple_input';

export {
	TypeA,
	TypeImportant,
	SimpleInput,
}