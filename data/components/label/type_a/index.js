import React from 'react';
import {StyleSheet, View, Dimensions,} from 'react-native';
import {moderateScale} from 'react-native-size-matters';

import {Text, Scaling} from './../../../components';
import * as CONSTANT from './../../../constant';

export default class TypeA extends React.Component {

  render() {
    let {fontType} = this.props;
    return (
      <View
          style={[styles.container, this.props.containerStyle, {backgroundColor:this.props.labelColor}]} 
      >
          <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_CONTENT} textStyle={[styles.labelTextStyle, {color:this.props.labelTextColor}]}>
            {this.props.labelText}
            </Text>
      </View>
    );
  }
}

TypeA.defaultProps ={
  labelColor:CONSTANT.COLOR.BLUE_A,
  labelText:'Label',
}

const styles = StyleSheet.create({
  container: {
    width:Scaling.scale(205),
    height:Scaling.verticalScale(25),
    borderRadius:Scaling.moderateScale(7),
    padding:Scaling.moderateScale(5),
    alignItems:'center',
    justifyContent: 'center',
  },
  labelTextStyle:{
    color:CONSTANT.COLOR.WHITE,
  }
});

