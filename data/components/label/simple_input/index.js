import React from 'react';
import {StyleSheet, View, Dimensions,} from 'react-native';
import {moderateScale} from 'react-native-size-matters';

import {Text, Scaling} from './../../../components';
import * as CONSTANT from './../../../constant';

export default class SimpleInput extends React.Component {
  static defaultProps = {
    containerStyle : {},
    labelText:'Label',
    categoryText:'Category',
    fontOption:CONSTANT.TEXT_OPTION.SUB_HEADER,
  }
  render() {
    let {labelText, categoryText, fontOption} = this.props;
    return (
      <View
          style={[styles.container, this.props.containerStyle]} 
      >
        <View
          style={{
            flex:0.3,
          }}
        >
          <Text fontOption={fontOption}>{categoryText}</Text>
        </View>
        <View
          style={{
            flex:0.7,
          }}
        >
          <Text fontOption={fontOption}>{labelText}</Text>
        </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width:'100%',
    height:Scaling.verticalScale(40),
    flexDirection:'row',
    justifyContent:'center',
    backgroundColor:'red',
  },
  labelTextStyle:{
    color:'white',
  }
});

