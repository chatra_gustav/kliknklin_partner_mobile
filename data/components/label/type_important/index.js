import React from 'react';
import {StyleSheet, View, Dimensions,} from 'react-native';
import {moderateScale} from 'react-native-size-matters';

import {TextWrapper} from './../../../components';
import * as CONSTANT from './../../../constant';

export default class TypeImportant extends React.Component{


  render(){
      try{
          return (
             <View
                style={{
                  //position:'absolute',
                  right:moderateScale(10),
                  //top:-5,
                  width:moderateScale(25),
                  height:moderateScale(25),
                  borderRadius:moderateScale(25/2),
                  backgroundColor:CONSTANT.COLOR.YELLOW_A,
                  alignItems:'center',
                  justifyContent:'center'
                }}
              >
                <TextWrapper type={'content'} style={{color:'white'}} fontWeight={'SemiBold'}>!</TextWrapper>
              </View>
          );
      }
      catch(error){
        console.log(error);
        return null;
      }
      
  }
}

