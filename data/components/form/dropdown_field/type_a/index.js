
import React from 'react';
import {
  View, 
  StyleSheet, 
  TextInput,
  Text,
  Dimensions
} from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import {TextWrapper, FontScale, fontMaker} from './../../../../components';
import {moderateScale} from 'react-native-size-matters';

import Style from './../../style';

export default class TypeA extends React.Component{

  focus(){
    this.dropdown_field.focus();
  }

  blur(){
    this.dropdown_field.blur();
  }
  
  value(){
    this.dropdown_field.value();
  }

  selectedIndex(){
    this.dropdown_field.selectedIndex();
  }

  selectedItem(){
    this.dropdown_field.selectedItem();
  }

  isFocused(){
    this.dropdown_field.isFocused();
  }

  render(){

    return(
        <Dropdown
            ref={(component) => {this.dropdown_field = component}}
            lineWidth={0}
            dropdownPosition={0}
            labelPadding={moderateScale(0.5)}
            labelHeight={moderateScale(10)}
            labelFontSize={moderateScale(8)}
            fontSize={moderateScale(12)}
            titleFontSize={moderateScale(10)}
            baseColor={'white'}
            tintColor={'white'}
            labelTextStyle={[fontMaker({weight:'Bold'})]}
            style={[fontMaker({weight:'SemiBold'}),{padding:0, color:'white'}]}
            itemTextStyle={[fontMaker({weight:'Medium'})]}

            {...this.props}
            containerStyle={[Style.containerGeneralA, this.props.containerStyle]}
          />
    )
  }

}

