import TypeA from './type_a';
import TypeB from './type_b';
import MainDashboard from './main_dashboard';
import AttendanceDate from './attendance_date';
import SearchBy from './search_by';

export {
	TypeA,
	TypeB,
	MainDashboard,
	AttendanceDate,
	SearchBy
}