
import React from 'react';
import {
  View, 
  StyleSheet, 
  TextInput,
  Dimensions,
  Image
} from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import ResolveAssetSource from 'resolveAssetSource';

import {fontCreator, Scaling, Text, TextTicker} from 'app/data/components';
import * as CONSTANT from 'app/data/constant';
import {moderateScale} from 'react-native-size-matters';

import Style from './../../style';

export default class AttendanceDate extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      value:null,
      image:{
        arrow_down_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/iot_machine/arrow_down_icon.png'),
        }
      }
    };
  }

  componentDidMount(){
    this.getImageSize();
  }

  getImageSize(){
    var image = this.state.image;

    var arrow_down_icon = ResolveAssetSource(this.state.image.arrow_down_icon.url);

    image.arrow_down_icon.width = arrow_down_icon.width * this.state.image.arrow_down_icon.multiplierWidth;
    image.arrow_down_icon.height = arrow_down_icon.height * this.state.image.arrow_down_icon.multiplierHeight;

    this.setState({image});
  }

  focus(){
    this.dropdown_field.focus();
  }

  blur(){
    this.dropdown_field.blur();
  }
  
  setValue(value){
    this.setState({value:this.dropdown_field.value()});
  }

  selectedIndex(){
    this.dropdown_field.selectedIndex();
  }

  selectedItem(){
    this.dropdown_field.selectedItem();
  }

  isFocused(){
    this.dropdown_field.isFocused();
  }

  renderBase(props){

  }

  render(){
    const styles = Scaling.ScaledSheet.create({
          fontStyle:fontCreator(CONSTANT.TEXT_OPTION.ATTENDANCE_HEADER),
      });

    return(
        <Dropdown
            ref={(component) => {this.dropdown_field = component}}
            dropdownPosition={0}
            tickerDuration={7000}
            tickerLopp={true}
            tickerBounce={true}
            tickerRepeatSpacer={50}
            tickerMarqueeDelay={2000}
            containerStyle={{
              width:Scaling.scale(500 * 0.444),
              alignItems:'flex-end'
            }}
            inputContainerStyle={{
              justifyContent:'center',
            }}
            {...this.props}
            renderBase={(props) => {
              let {value, tickerDuration, tickerMarqueeDelay, tickerRepeatSpacer, tickerLoop, tickerBounce, inputContainerStyle, containerStyle, renderAccessory} = props;
              return (
                <View style={[{flex:1,flexDirection:'row', alignItems: 'center'}]}>
                  <View style={{width:Scaling.scale(400 * 0.444), alignItems: 'flex-end'}}>
                      <TextTicker
                        style={[styles.fontStyle, inputContainerStyle, {color: CONSTANT.COLOR.LIGHT_BLUE}]}
                        duration={tickerDuration}
                        loop={tickerLoop}
                        bounce={tickerBounce}
                        repeatSpacer={tickerRepeatSpacer}
                        marqueeDelay={tickerMarqueeDelay}
                      >
                        {value}
                      </TextTicker>
                  </View>
                </View>
              );
            }}
            renderAccessory={(props) => {
              return(
                <View 
                  style={{width:Scaling.scale(75 * 0.444), height:Scaling.verticalScale(75 * 0.444), justifyContent:'center'}}
                >
                  <Image
                    style={{width: this.state.image.arrow_down_icon.width, height: this.state.image.arrow_down_icon.height}}
                    source={this.state.image.arrow_down_icon.url}
                  />
                </View>
              );
            }}
            fontSize={moderateScale(14)}
          />
    )
  }

}

