import React from 'react';
import {
  View, 
  StyleSheet, 
  TextInput,
  Text,
  Dimensions
} from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import {TextWrapper, FontScale, fontMaker} from './../../../../components';
import * as CONSTANT from './../../../../constant';
import {moderateScale} from 'react-native-size-matters';

import Style from './../../style';

export default class TypeB extends React.Component{

  focus(){
    this.dropdown_field.focus();
  }

  blur(){
    this.dropdown_field.blur();
  }
  
  value(){
    this.dropdown_field.value();
  }

  selectedIndex(){
    this.dropdown_field.selectedIndex();
  }

  selectedItem(){
    this.dropdown_field.selectedItem();
  }

  isFocused(){
    this.dropdown_field.isFocused();
  }
  
  render(){

    return(
        <Dropdown
            lineWidth={0}
            dropdownPosition={0}
            labelPadding={moderateScale(0.5)}
            labelHeight={moderateScale(10)}
            labelFontSize={moderateScale(8)}
            fontSize={moderateScale(12)}
            titleFontSize={moderateScale(10)}
            baseColor={CONSTANT.COLOR.GRAY_B}
            tintColor={CONSTANT.COLOR.GRAY_B}
            labelTextStyle={[fontMaker({weight:'Bold'})]}
            style={[fontMaker({weight:'SemiBold'}),{padding:0}]}
            itemTextStyle={[fontMaker({weight:'Medium'})]}

            {...this.props}
            //containerStyle={[Style.containerGeneralA, this.props.containerStyle]}
          />
    )
  }

}

