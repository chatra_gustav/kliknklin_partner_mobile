
import React from 'react';
import {
  View, 
  StyleSheet, 
  TextInput,
  Dimensions,
  Image
} from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import ResolveAssetSource from 'resolveAssetSource';

import {fontCreator, Scaling, Text, TextTicker} from 'app/data/components';
import * as CONSTANT from 'app/data/constant';

import Style from './../../style';

export default class Dashboard extends React.Component{
  static defaultProps = {
    value:null,
    width:'100%',
    height:'100%',
    labelColor:CONSTANT.COLOR.DARK_GRAY,
    label:'',
    labelContainerStyle:{},
    leftIcon:null,
  }
  constructor(props) {
    super(props);
    this.state = {
      value:null,
      image:{
        arrow_down_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/iot_machine/arrow_down_icon.png'),
        }
      }
    };
  }

  componentDidMount(){
    this.getImageSize();
  }

  getImageSize(){
    var image = this.state.image;

    var arrow_down_icon = ResolveAssetSource(this.state.image.arrow_down_icon.url);

    image.arrow_down_icon.width = arrow_down_icon.width * this.state.image.arrow_down_icon.multiplierWidth;
    image.arrow_down_icon.height = arrow_down_icon.height * this.state.image.arrow_down_icon.multiplierHeight;

    this.setState({image});
  }

  focus(){
    this.dropdown_field.focus();
  }

  blur(){
    this.dropdown_field.blur();
  }
  
  setValue(value){
    this.setState({value:this.dropdown_field.value()});
  }

  selectedIndex(){
    this.dropdown_field.selectedIndex();
  }

  selectedItem(){
    this.dropdown_field.selectedItem();
  }

  isFocused(){
    this.dropdown_field.isFocused();
  }

  renderLabel(props){
    let {value, label, labelColor, labelContainerStyle} = props;

    if (value == null && label != null && label.trim() != '') {
      return(
        <View style={[{paddingLeft:this.props.leftIcon ? Scaling.scale(75 * 0.444) : 0}, labelContainerStyle]}>
          <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER} textStyle={{color:labelColor}}>{label}</Text>
        </View>
      );
    }
  }

  renderLeftIcon(props){
    let {leftIcon} = props;

    if (leftIcon) {
      return(
        <View 
          style={{paddingRight:Scaling.moderateScale(10),width:Scaling.scale(75 * 0.444), height:Scaling.verticalScale(75 * 0.444), justifyContent:'center'}}
        >
              {leftIcon()}
        </View>
      );
    }
    return null;
  }

  render(){
    const styles = Scaling.ScaledSheet.create({
          fontStyle:fontCreator(CONSTANT.TEXT_OPTION.DROPDOWN_DASHBOARD),
      });

    let isRenderLeftIcon = false;
    let isRenderLabel = false;

    return(
        <Dropdown
            ref={(component) => {this.dropdown_field = component}}
            itemTextStyle={styles.fontStyle}
            dropdownPosition={0}
            tickerDuration={6000}
            tickerLoop={true}
            tickerBounce={true}
            tickerRepeatSpacer={50}
            tickerMarqueeDelay={2000}
            disabled={this.props.disableOutletSelector}
            rippleInsets={{
              top:0,
              bottom:-1 * Scaling.moderateScale(20),
            }}
            rippleCentered={true}
            {...this.props}
            renderBase={(props) => {
              let {value, label, labelColor, tickerDuration, tickerMarqueeDelay, tickerRepeatSpacer, tickerLoop, tickerBounce, inputContainerStyle, containerStyle, renderAccessory} = props;
              return (
                <View style={[{width:this.props.width, height:this.props.height, justifyContent:'center'}]}>

                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        {this.renderLabel(props)}

                        {this.renderLeftIcon(props)}
                    
                        <TextTicker
                          style={[styles.fontStyle, inputContainerStyle]}
                          duration={tickerDuration}
                          loop={tickerLoop}
                          bounce={tickerBounce}
                          repeatSpacer={tickerRepeatSpacer}
                          marqueeDelay={tickerMarqueeDelay}
                        >
                          {value}
                        </TextTicker>

                        <Image
                          style={{
                            width: Scaling.scale(22.5), 
                            height: Scaling.verticalScale(22.5),
                            marginLeft:Scaling.moderateScale(7.5),
                            marginTop:Scaling.verticalScale(5),
                          }}
                          source={CONSTANT.IMAGE_URL.BOTTOM_ARROW_ICON}
                        />
                    </View>
                  </View>
              );
            }}
          />
    )
  }

}

