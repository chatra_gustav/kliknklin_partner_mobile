import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';
import {moderateScale} from 'react-native-size-matters';

import {TextWrapper,} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

export default class TypeA extends Component{

  render(){
    return(
      <View style={[styles.separator,this.props.separatorStyle]}/>
    );
  }
}

TypeA.propTypes = {
  separatorStyle:PropTypes.object,
}

TypeA.defaultProps = {
  separatorStyle:{},
}

const styles = StyleSheet.create({
  separator: {
    borderTopWidth:1,
    borderColor:CONSTANT.COLOR.GRAY_B,
    marginVertical:moderateScale(10),
  },
});
