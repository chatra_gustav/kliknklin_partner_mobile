import React from 'react';
import ReactNative from 'react-native';
import {TextWrapper} from './../../../../components';

const {View, StyleSheet, TextInput, Text, Platform, Dimensions, TouchableHighlight} = ReactNative;

export default class TypeA extends React.Component{
  constructor(props){
    super(props);

    this.state = {
    };

  }

  componentDidmount(){
  }

  render(){
    return(
      <TouchableHighlight
        {...this.props}
        underlayColor={'transparent'}
        style={[this.props.containerStyle,]}
      >
        <View>
          <TextWrapper
            fontWeight={'SemiBold'}
            type={'smallercontent'}
            {...this.props}
            style={[this.props.labelStyle,]}
            type={this.props.type}
          >
            {this.props.label}
          </TextWrapper>
        </View>
      </TouchableHighlight>
    )
  }

}

