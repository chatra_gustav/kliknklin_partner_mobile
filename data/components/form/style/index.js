import { StyleSheet, Platform } from 'react-native';
import {moderateScale} from 'react-native-size-matters';

export default StyleSheet.create({
	containerGeneralA: {
		borderRadius:5,
	    backgroundColor:'rgba(255,255,255,0.5)',
	    paddingHorizontal:moderateScale(10),
	},
});