import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';

import {TextWrapper,} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as appActions from "./../../../../actions";

export default class TypeA extends Component{

	render(){
		return(
			<View style={[styles.container,this.props.containerStyle]}>
		    	<TextWrapper
		    		type={this.props.errorTextType} 
					style={[styles.errorText, this.props.errorTextStyle]}
		    	>
					{this.props.errorText}
		    	</TextWrapper>
		    </View>
		);
	}
}

TypeA.propTypes = {
  errorText:PropTypes.string,
  containerStyle:PropTypes.object,
  errorTextStyle:PropTypes.object,
  errorTextType:PropTypes.string,
}

TypeA.defaultProps = {
	errorText:'',
	errorTextType:'smallercontent',
  	containerStyle:{},
  	errorTextStyle:{},
}

const styles = StyleSheet.create({
  	container: {
  		minHeight:Dimensions.get('window').height * 0.05,
		backgroundColor:'transparent',
		alignItems:'center',
		justifyContent:'center',
	},
	errorText:{
		color:CONSTANT.COLOR.RED_B,
	},
});
