
import React, {Component} from 'react';
import {
  View, 
  StyleSheet, 
  TextInput,
  Text,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';
import { TextField } from 'react-native-material-textfield';
import {TextWrapper, FontScale, fontMaker} from './../../../../components';
import {moderateScale} from 'react-native-size-matters';
import Style from './../../style';


export default class TypeC extends Component{
  state = {
    isTextFieldOnFocus:false,
    value:this.props.value,
  };

  componentDidMount(){
    if (this.props.autoFocus) {
      this.text_field.focus();
    }
  }

  onFocus() {
    this.setState({isTextFieldOnFocus:true});
    this.props.onFocus();
  }

  onBlur() {
    if (this.state.value == '') {
      this.setState({isTextFieldOnFocus:false});
    }

    this.props.onBlur();
  }

  onChangeText(value){
    this.setState({value}, () => {
      this.props.onChangeText(value);
    });
  }

  render() {
    return (
      <TextField
          ref={(component) => {this.text_field = component}}
          baseColor={'white'}
          tintColor={'white'}
          labelHeight={moderateScale(10)}
          labelPadding={moderateScale(0.5)}
          lineWidth={0}
          activeLineWidth={0}
          disabledLineWidth={0}
          labelFontSize={moderateScale(8)}
          fontSize={moderateScale(12)}
          titleFontSize={moderateScale(10)}
          keyboardType='default'
          labelTextStyle={fontMaker({weight:'Bold'})}
          style={[fontMaker({weight:'SemiBold'}), {padding:0, color:'white',}]}
          
          {...this.props}
          containerStyle={[Style.containerGeneralA, 
            {
              //paddingTop: moderateScale(5),
            },
            this.props.containerStyle]}

          onFocus={this.onFocus.bind(this)}
          onBlur={this.onBlur.bind(this)}
          onChangeText={this.onChangeText.bind(this)}
      />
    );

}
}

TypeC.propTypes = {
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onChangeText: PropTypes.func,
}

TypeC.defaultProps = {
   onFocus: () => {},
   onBlur: () => {},
   onChangeText: () => {},
}


