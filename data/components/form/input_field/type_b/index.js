
import React from 'react';
import ReactNative from 'react-native';
import {TextWrapper, FontScale} from './../../../../components';
import * as CONSTANT from './../../../../constant';

const {View, StyleSheet, TextInput, Text, Platform, Dimensions} = ReactNative;

export default class TypeB extends React.Component{
	constructor(props){
    super(props);

    this.state = {
    };

  }

  componentDidmount(){
  }
  render(){
    return(
          <View
            style={{
              flex:1,
              height:Dimensions.get('window').height * 0.1,
              alignSelf:'stretch',
            }}
          >
              {
                this.props.topLabel ? 

                <View>
                  <TextWrapper type={'base'} fontWeight={'SemiBold'}>{this.props.topLabelText}</TextWrapper>
                </View>

                :

                null
              }

              <View 
                style={[
                  this.props.containerStyle,
                  { 
                    flex:1,
                    borderRadius:5,
                    borderWidth:1,
                    borderColor:CONSTANT.COLOR.GRAY_B,
                    flexDirection:'row',
                  }
                ]}
              >
              
                <View style={{
                  flex:1,
                  margin:5
               }}>
                    <TextInput
                      {...this.props}
                      ref='inputBox'
                      keyboardType = {this.props.keyboardType}
                      editable ={this.state.editable}
                      multiline={true}
                      numberOfLines={2}
                      returnKeyType={'default'}
                      underlineColorAndroid='rgba(0,0,0,0)'
                      style={[
                          this.props.inputStyle,
                          { 
                            alignSelf:'stretch',
                            color:'#9B9C9B',
                            height:Dimensions.get('window').height * 0.15,
                            fontFamily:'Montserrat',
                            fontSize:FontScale._getFontSize(10),
                            fontWeight:'400',
                            textAlignVertical:'top',
                            padding:0,
                          }
                        ]}

                      placeholder={this.props.placeholder}
                      placeholderTextColor={this.props.placeholderTextColor ? this.props.placeholderTextColor : '#9B9C9B'}
                      value={this.state.value}
                    />
                </View>
            


                {(this.props.iconRight)
                  ? this.props.iconRight
                  : null
                }
                </View>
          </View>
    )
  }

}


