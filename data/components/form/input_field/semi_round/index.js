
import React from 'react';
import {
  View, 
  StyleSheet, 
  TextInput,
  Dimensions,
  Platform,
  TouchableWithoutFeedback,
} from 'react-native';

import * as CONSTANT from 'app/data/constant';

import {Scaling, fontCreator} from 'app/data/components';
import {moderateScale} from 'react-native-size-matters';

export default class SemiRound extends React.Component{
  static defaultProps = {
    fontOption:CONSTANT.TEXT_OPTION.SUB_HEADER,
    containerStyle:{},
    leftIcon: () => {
      return null;
    },
    editable:true,
  }
  focus(){
    if (this.text_field) {
      this.text_field.focus();
    }
  }

  blur(){
    if (this.text_field) {
      this.text_field.blur();
    }
  }

  render(){
    const styles = Scaling.ScaledSheet.create({
        fontStyle:fontCreator(this.props.fontOption),
    });

    return(
        <TouchableWithoutFeedback
            onPress={() => {
              if (this.props.editable) {
                this.focus(); 
              }
            }}
        >
          <View
            style={[{
              width:'100%',
              borderWidth:Scaling.moderateScale(1),
              borderColor:CONSTANT.COLOR.LIGHT_GRAY,
              borderRadius:Scaling.moderateScale(12),
              flexDirection:'row',
              alignItems:'center',
              justifyContent:'center',
              height:Scaling.verticalScale(55),
              paddingHorizontal:Scaling.scale(10),
            },
            this.props.containerStyle,
            ]}

          >   

              <View
                style={{
                  width:'100%',
                  height:'100%',
                  flexDirection:'row',
                  alignItems:'center',
                }}
              >
                {this.props.leftIcon()}

                <TextInput
                  ref={(component) => {this.text_field = component}}
                  // Inherit any props passed to it; e.g., multiline, numberOfLines below
                  style={[
                    {
                      margin:0,
                      padding:0,
                      marginTop:Scaling.verticalScale(2.5),
                      marginBottom:Scaling.verticalScale(1),
                      width:'90%',
                      color:CONSTANT.COLOR.BLACK,
                    },
                    styles.fontStyle,
                  ]}

                  {...this.props}
                />
              </View>
          </View>
        </TouchableWithoutFeedback>
    )
  }

}

