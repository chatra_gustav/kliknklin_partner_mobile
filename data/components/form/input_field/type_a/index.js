
import React from 'react';
import {
  View, 
  StyleSheet, 
  TextInput,
  Text,
  Dimensions,
  Platform,
} from 'react-native';
import {TextWrapper, fontCreator, Scaling} from './../../../../components';
import * as CONSTANT from './../../../../constant';

import { TextField } from 'react-native-material-textfield';
import {moderateScale} from 'react-native-size-matters';

export default class TypeA extends React.Component{
  static defaultProps = {
    textFontOption:CONSTANT.TEXT_OPTION.SUB_HEADER,
    labelFontOption:CONSTANT.TEXT_OPTION.SUB_HEADER,
  }
  focus(){
    if (this.text_field) {
      this.text_field.focus();
    }
  }

  blur(){
    if (this.text_field) {
      this.text_field.blur();
    }
  }

  render(){
    const textStyles = Scaling.ScaledSheet.create({
          fontStyle:fontCreator(this.props.textFontOption),
    }); 

    const labelStyles = Scaling.ScaledSheet.create({
          fontStyle:fontCreator(this.props.labelFontOption),
    }); 

    return(
          <TextField
            ref={(component) => {this.text_field = component}}
            baseColor={CONSTANT.COLOR.GRAY_C}
            tintColor={CONSTANT.COLOR.GRAY_C}
            labelHeight={moderateScale(7)}
            labelPadding={moderateScale(0.5)}
            lineWidth={0}
            activeLineWidth={0}
            disabledLineWidth={0}
            labelFontSize={moderateScale(10)}
            fontSize={moderateScale(12)}
            titleFontSize={moderateScale(10)}
            keyboardType='default'
            labelTextStyle={labelStyles.fontStyle}
            label={''}
            
            {...this.props}
            containerStyle={[
              {
                //alignItems:'center', 
                justifyContent:'flex-end', 
                width:'100%',
                //flexDirection:'row',
              },
              this.props.containerStyle
            ]}
            inputContainerStyle={{
              //alignItems:'center',
              //justifyContent:'flex-end',
              borderBottomWidth:0.75,
              borderColor:'lightgray',
            }}

            style={[textStyles.fontStyle, {padding:0, textAlign:'center', color:'black',}, this.props.inputTextStyle]}

            //onFocus={this.onFocus.bind(this)}
            //onBlur={this.onBlur.bind(this)}
            //onChangeText={this.onChangeText.bind(this)}
        />
    )
  }

}

