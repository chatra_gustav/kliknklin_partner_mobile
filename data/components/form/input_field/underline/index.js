
import React from 'react';
import {
  View, 
  StyleSheet, 
  TextInput,
  Dimensions,
  Platform,
} from 'react-native';

import * as CONSTANT from 'app/data/constant';

import {Scaling, fontCreator} from 'app/data/components';
import {moderateScale} from 'react-native-size-matters';

export default class Underline extends React.Component{
  static defaultProps = {
    fontOption:CONSTANT.TEXT_OPTION.SUB_HEADER,
    leftIcon: () => {
      return null;
    },
    width:'100%',
    containerStyle:{},
  }
  focus(){
    if (this.text_field) {
      this.text_field.focus();
    }
  }

  blur(){
    if (this.text_field) {
      this.text_field.blur();
    }
  }

  render(){
    const styles = Scaling.ScaledSheet.create({
        fontStyle:fontCreator(this.props.fontOption),
    });

    return(
          <View
            style={[
              {
                width:this.props.width,
                borderBottomWidth:Scaling.moderateScale(2),
                borderColor:CONSTANT.COLOR.LIGHT_GRAY,
                flexDirection:'row',
                alignItems:'center',
              },
              this.props.containerStyle,
            ]}
          >   

              <View
                style={{
                  width:'100%',
                  flexDirection:'row',
                }}
              >
                {this.props.leftIcon()}

                <TextInput
                  ref={(component) => {this.text_field = component}}
                  // Inherit any props passed to it; e.g., multiline, numberOfLines below
                  style={[
                    {
                      margin:0,
                      padding:0,
                      marginTop:Scaling.verticalScale(2.5),
                      marginBottom:Scaling.verticalScale(1),
                      width:'100%',
                      textAlignVertical:'center',
                    },
                    styles.fontStyle,
                  ]}

                  {...this.props}
                />
              </View>
          </View>
    )
  }

}

