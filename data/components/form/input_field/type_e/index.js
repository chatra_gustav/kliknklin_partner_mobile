
import React from 'react';
import {
  View, 
  StyleSheet, 
  TextInput,
  Text,
  Dimensions,
  Platform,
} from 'react-native';
import {TextWrapper, FontScale, fontMaker, Scaling} from './../../../../components';
import * as CONSTANT from './../../../../constant';

import { TextField } from 'react-native-material-textfield';
import {moderateScale} from 'react-native-size-matters';

export default class TypeE extends React.Component{

  focus(){
    this.text_field.focus();
  }

  blur(){
    if (this.text_field) {
      this.text_field.blur();
    }
  }

  clear(){
    this.text_field.clear();
  }

  render(){
    return(
      <View>

        {this.label()}

        <TextField
          ref={(component) => {this.text_field = component}}
          baseColor={CONSTANT.COLOR.GRAY_C}
          tintColor={CONSTANT.COLOR.GRAY_C}
          labelHeight={Scaling.moderateScale(7)}
          labelPadding={Scaling.moderateScale(0.5)}
          lineWidth={0}
          activeLineWidth={0}
          disabledLineWidth={0}
          labelFontSize={Scaling.moderateScale(10)}
          titleFontSize={Scaling.moderateScale(10)}
          keyboardType='default'
          containerStyle={[
            {
              alignItems:'center',
              justifyContent:'flex-end', 
              width:Dimensions.get('window').width * 0.88,
            }
          ]}
          inputContainerStyle={{
            alignItems:'center',
            justifyContent:'flex-end',
            backgroundColor: CONSTANT.COLOR.WHITE,
            elevation: 2,
            borderRadius: 10,
            paddingHorizontal: Scaling.moderateScale(10),
            paddingVertical: Scaling.moderateScale(5)
          }}
          style={[fontMaker({weight:'Normal'}), {padding:0, textAlign:'left', fontFamily: CONSTANT.TEXT_FAMILY.MAIN, color:CONSTANT.COLOR.LIGHT_BLACK, fontSize: Scaling.moderateScale(13)}, this.props.inputTextStyle]}
          {...this.props}
          label={''}
      />
      </View>
    )
  }

  label(){
    if(this.props.label !== undefined){
      return(
        <View style={{marginBottom: Scaling.moderateScale(12)}}>
          <Text style={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT, fontSize: Scaling.moderateScale(13), fontFamily: CONSTANT.TEXT_FAMILY.MAIN}}>{this.props.label}</Text>
        </View>
      )
    }else{
      return null
    }
  }

}

