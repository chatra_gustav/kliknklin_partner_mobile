
import React, {Component} from 'react';
import {
  View, 
  StyleSheet, 
  TextInput,
  Text,
  Dimensions,
  Platform
} from 'react-native';
import PropTypes from 'prop-types';
import { TextField } from 'react-native-material-textfield';
import {TextWrapper, FontScale, fontCreator} from './../../../../components';
import * as CONSTANT from './../../../../constant';
import {moderateScale} from 'react-native-size-matters';
import Style from './../../style';

const iosTextHeight = moderateScale(30);
const androidTextHeight = moderateScale(30);
const textHeight = Platform.OS === 'ios' ? iosTextHeight : androidTextHeight;

export default class TypeD extends Component{
  state = { 
    height: textHeight * 3, 
    minHeight: moderateScale(200),
    lines: 1,
    numberOfLines:this.props.numberOfLines ? this.props.numberOfLines : 1,
   }

  componentDidMount(){
    if (this.props.autoFocus) {
      this.text_field.focus();
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.value === '') {
      this.setState({ height: textHeight * 2, lines: 1 })
    }
  }

  onFocus() {
    this.setState({isTextFieldOnFocus:true});
    this.props.onFocus();
  }

  onBlur() {
    if (this.state.value == '') {
      this.setState({isTextFieldOnFocus:false});
    }

    this.props.onBlur();
  }

  onChangeText(value){
    this.setState({value}, () => {
      this.props.onChangeText(value);
    });
  }

  render() {
    return (
      <TextField
          ref={(component) => {this.text_field = component}}
          baseColor={CONSTANT.COLOR.GRAY}
          tintColor={CONSTANT.COLOR.GRAY}
          labelHeight={moderateScale(10)}
          labelPadding={moderateScale(2)}
          lineWidth={0}
          activeLineWidth={0}
          disabledLineWidth={0}
          labelFontSize={moderateScale(11)}
          fontSize={moderateScale(13)}
          titleFontSize={moderateScale(10)}
          keyboardType='default'
          labelTextStyle={fontCreator({
            weight:CONSTANT.TEXT_WEIGHT.SEMI_BOLD
          })}
          style={[
            fontCreator({
              weight : CONSTANT.TEXT_WEIGHT.MEDIUM,
              family : CONSTANT.TEXT_FAMILY.MAIN,
              style : CONSTANT.TEXT_STYLE.NONE,
            }), 
            {padding:0, color:'black',}, 
            this.props.inputTextStyle]}
          
          onContentSizeChange={(event) => {
            const height = Platform.OS === 'ios'
              ? event.nativeEvent.contentSize.height
              : event.nativeEvent.contentSize.height - androidTextHeight
            const lines = Math.round(height / textHeight)
            const visibleLines = lines < this.state.numberOfLines ? lines : this.state.numberOfLines
            const visibleHeight = textHeight * (visibleLines + 1)
            this.setState({ height: visibleHeight, lines: visibleLines })
          }}
          {...this.props}
          containerStyle={[{ height: this.state.height}, this.props.containerStyle]}
          inputContainerStyle={[{minHeight: this.state.minHeight}, this.props.inputContainerStyle]}
          maxHeight={this.state.minHeight > this.state.height ? this.state.minHeight : this.state.height}
          onFocus={this.onFocus.bind(this)}
          onBlur={this.onBlur.bind(this)}
          onChangeText={this.onChangeText.bind(this)}
      />
    );

}
}

TypeD.propTypes = {
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onChangeText: PropTypes.func,
}

TypeD.defaultProps = {
   onFocus: () => {},
   onBlur: () => {},
   onChangeText: () => {},
}


