import React, { Component } from 'react'
import { StyleSheet, View, Dimensions } from 'react-native'
import { Calendar, defaultStyle } from 'react-native-calendars'

import * as CONSTANT from './../../constant';
import {Button, Scaling} from './../../components';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../actions";

const XDate = require('xdate');

type Props = {
  onSuccess: React.PropTypes.func.isRequired,
};

class DateRangePickerComponent extends Component<Props> {

  state = {isFromDatePicked: false, isToDatePicked: false, markedDates: {}}

  componentDidMount() { 
    // this.setupInitialRange() 
  }

  onDayPress = (day) => {
    if (!this.state.isFromDatePicked || (this.state.isFromDatePicked && this.state.isToDatePicked)) {
      this.setupStartMarker(day)
    } else if (!this.state.isToDatePicked) {
      let markedDates = {...this.state.markedDates}
      let [mMarkedDates, range] = this.setupMarkedDates(this.state.fromDate, day.dateString, markedDates)
      if (range >= 0) {
        this.setState({isFromDatePicked: true, isToDatePicked: true, markedDates: mMarkedDates})
        this.props.onRangeSelected(this.state.fromDate, day.dateString);
      } else {
        this.setupStartMarker(day)
      }
    }
  }

  setupStartMarker = (day) => {
    let markedDates = {[day.dateString]: {startingDay: true, color: this.props.theme.markColor, textColor: this.props.theme.markTextColor}}
    this.setState({isFromDatePicked: true, isToDatePicked: false, fromDate: day.dateString, markedDates: markedDates})
  }

  setupMarkedDates = (fromDate, toDate, markedDates) => {
    let mFromDate = new XDate(fromDate)
    let mToDate = new XDate(toDate)
    let range = mFromDate.diffDays(mToDate)
    if (range >= 0) {
      if (range == 0) {
        markedDates = {[toDate]: {color: this.props.theme.markColor, textColor: this.props.theme.markTextColor}}
      } else {
        for (var i = 1; i <= range; i++) {
          let tempDate = mFromDate.addDays(1).toString('yyyy-MM-dd')
          if (i < range) {
            markedDates[tempDate] = {color: this.props.theme.markColor, textColor: this.props.theme.markTextColor}
          } else {
            markedDates[tempDate] = {endingDay: true, color: this.props.theme.markColor, textColor: this.props.theme.markTextColor}
          }
        }
      }
    }
    return [markedDates, range]
  }

  render() {
    let {width, height} = Dimensions.get('window');
    return (
      <View style={{ width: Scaling.moderateScale(width * 0.95), height: Scaling.moderateScale(height * 0.75), padding: Scaling.moderateScale(10), borderRadius: 10, backgroundColor: 'white'}}>
        <Calendar {...this.props}
          markingType={'period'}
          current={this.state.fromDate}
          markedDates={this.state.markedDates}
          onDayPress={(day) => {this.onDayPress(day)}}/>

          <View style={{
            width: '100%',
            position: 'absolute',
            bottom: Scaling.moderateScale(14),
            left: Scaling.moderateScale(10),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-around'
          }}>

            <Button.TypeD
              buttonText={'Cancel'}
              buttonColor={CONSTANT.COLOR.ORANGE}
              buttonTextStyle={[styles.buttonTextStyle]}
              buttonStyle={styles.buttonStyle}
              onPress={() => {this.props.onCancel()}}
            />

            <Button.TypeD
              buttonText={'Ok'}
              buttonColor={(this.state.isFromDatePicked && this.state.isToDatePicked ? CONSTANT.COLOR.LIGHT_BLUE: CONSTANT.COLOR.LIGHT_GRAY)}
              buttonTextStyle={[styles.buttonTextStyle]}
              buttonStyle={styles.buttonStyle}
              onPress={() => {
                if(this.state.isFromDatePicked && this.state.isToDatePicked)
                  this.props.onSubmit()}
              }
            />

          </View>
      </View>
    )
  }

}

DateRangePickerComponent.defaultProps = {
  theme: { 
    markColor: '#00adf5', markTextColor: '#ffffff'
  }
};

class DateRangePicker extends Component {

  constructor(props){
    super(props);
    this.state ={
      date: {
        startDate: '',
        endDate: ''
      }
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <DateRangePickerComponent
          onRangeSelected={this.onRangeSelected.bind(this)}
          onSubmit={this.onSubmit.bind(this)}
          onCancel={this.props.onPressButtonSecondary}
          theme={{ 
            markColor: CONSTANT.COLOR.LIGHT_BLUE, markTextColor: 'white' }}/>
      </View>
    );
  }

  onRangeSelected = (s,e) => {
    this.state.date.startDate = s;
    this.state.date.endDate = e;
  }

  onSubmit = () => {
    let startDate = this.state.date.startDate;
    let endDate = this.state.date.endDate;

    this.props.onPressButtonPrimary(startDate, endDate);
  }

  // formatTo_dd_mm_yyyy = (date) => {
  //   let date_arr = date.split("-");
  //   return date_arr[2]+'-'+date_arr[1]+'-'+date_arr[0];
  // }
}

function mapStateToProps(state, ownProps) {
  return {
    delayOrderReasonList:state.rootReducer.masterData.delayOrderReasonList,
    rejectOrderReasonList:state.rootReducer.masterData.rejectOrderReasonList,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionMasterData: bindActionCreators(actions.masterData, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DateRangePicker);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonTextStyle:{
    color: CONSTANT.COLOR.WHITE, 
    fontFamily: CONSTANT.TEXT_FAMILY.MAIN_SEMI_BOLD, 
    fontWeight: CONSTANT.TEXT_WEIGHT.MEDIUM.weight,
    fontSize: Scaling.moderateScale(13)
  },
  buttonStyle:{
    width:Dimensions.get('window').width * 0.7 * 0.4,
    borderRadius: Scaling.moderateScale(10),
    height:CONSTANT.STYLE.BUTTON.SMALL.HEIGHT
  }
});