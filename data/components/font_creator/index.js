import {
  Platform
} from 'react-native';

import * as CONSTANT from './../../constant';

// we define available font weight and styles for each font here

// generate styles for a font with given weight and style
export const fontCreator = (options = {}) => {
  let { weight, style, family, size } = Object.assign({
    weight: null,
    style: null,
    size:14,
  }, options)
  
  //bentuk return beda untuk android dan ios
  if (Platform.OS === 'android') {
    const suffix = weight ? weight.name + style : '';

    return {
      fontFamily: family + `-${suffix}`,
      fontSize:size,
    };
  } else {
    //ios belom bener
    weight = weights[weight] || weights.Normal
    style = styles[style] || 'normal'

    return {
      fontFamily: family,
      fontWeight: weight,
      fontStyle: style,
      fontSize: size,
    }
  }
}