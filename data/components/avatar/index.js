import React from 'react';
import {StyleSheet, View, Dimensions, TouchableOpacity, Image} from 'react-native';

export default class Avatar extends React.Component {
  static defaultProps = {
    containerStyle:{},
  }
  render() {
    let {
      size,
      img,
      containerStyle,
    } = this.props;

    if(img !== null){
      return (
        <View style={[styles.container, containerStyle]}>
          <Image
            style={{width: size, height: size, borderRadius: size * 0.5}}
            source={{uri: img}}
          />
        </View>
      );
    }else{
      return (
        <View style={[styles.container, containerStyle]}>
          <Image
            style={{width: size, height: size, borderRadius: size * 0.5}}
            source={require('../../image/signin/avatar_icon.png')}
          />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});

