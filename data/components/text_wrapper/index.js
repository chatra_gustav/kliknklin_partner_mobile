
import React, { Component } from 'react';
import { Text,View,PixelRatio, Platform} from 'react-native';

import {FontScale, fontMaker} from './../../components';

export class TextWrapper extends Component{
  constructor(props){
    super(props);
    this.state = {
       fontSize:this.props.fontSize,
       fontMaker:{
        weight:this.props.fontWeight,
        style:this.props.fontDecoration,
        family:this.props.fontFamily,
       }
    };
  }

  componentWillMount(){
    this._setFontSize();
  }

  _setFontSize(){
    var fontSize;

    if (this.props.type == 'gigatitle') {
      fontSize = 52;
    }
    else if (this.props.type == 'megatitle') {
      fontSize = 46;
    }
    else if (this.props.type == 'ultratitle') {
      fontSize = 40;
    }
    else if (this.props.type == 'supertitle') {
      fontSize = 36;
    }
    else if (this.props.type == 'biggertitle') {
      fontSize = 30;
    }
    else if (this.props.type == 'bigtitle') {
      fontSize = 26;
    }
    else if (this.props.type == 'title') {
      fontSize = 22;
    }
    else if (this.props.type == 'smalltitle') {
      fontSize = 20;
    }
    else if (this.props.type == 'smallertitle') {
      fontSize = 18;
    }
    else if (this.props.type == 'content') {
      fontSize = 16;
    }
    else if (this.props.type == 'smallcontent') {
      fontSize = 14;
    }
    else if (this.props.type == 'smallercontent') {
      fontSize = 12;
    }
    else if (this.props.type == 'smallestcontent') {
      fontSize = 11;
    }
    else if (this.props.type == 'base') {
      fontSize = 10;
    }
    else if (this.props.type == 'smallbase') {
      fontSize = 8
    }
    else if (this.props.type == 'smallerbase') {
      fontSize = 6
    }
    else{
      fontSize = this.state.fontSize;
    }

    this.setState({fontSize:FontScale._getFontSize(fontSize)});
  }

  render(){
      return(
        <Text {...this.props} style={[fontMaker(this.state.fontMaker), {fontSize:this.state.fontSize}, this.props.style]}>
            {this.props.children}
        </Text>
      );
  }
}

TextWrapper.defaultProps = {
  fontWeight:'',
  fontStyle:'Regular',
  fontFamily:'Montserrat',
}
