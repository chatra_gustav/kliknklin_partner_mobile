import React, { Component } from 'react';
import { Text,View, Dimensions, Platform, } from 'react-native';

import NetInfo from "@react-native-community/netinfo";

import {TextWrapper} from './../../components';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../actions";

class ConnectionStability extends Component{

  componentWillMount(){
    this.unsubscribe = NetInfo.addEventListener(this._updateConnectionChange.bind(this));
  }

  componentWillUnmount(){
    this.unsubscribe();
  }

  _updateConnectionChange (state){
    let result = 'none';
    
    if (state.type == 'none') {

    }
    else if (state.type == 'cellular') {
      result = state.details.cellularGeneration;
    }
    else{
      result = 'good';
    }

    this.props.actionRoot.setConnectionStatus(result);
  }

  render(){
      if (this.props.connectionStatus == 'none') {
        return this._renderConnectionOffline();
      }
      else if (this.props.connectionStatus == '2g') {
        return this._renderConnectionUnstable();
      }
      return null;
  }

  _renderConnectionOffline(){
      return(
        <View style={{position:'absolute', top:0, width:Dimensions.get('window').width, backgroundColor:'#4D4D4D', alignItems:'center'}}>
            <TextWrapper type='base' style={{color:'white'}}>Internet bermasalah ! tidak bisa mengambil data terkini</TextWrapper>
        </View>
      );
  }

  _renderConnectionUnstable(){
      return(
        <View style={{position:'absolute', top:0, width:Dimensions.get('window').width, backgroundColor:'#FFC300', alignItems:'center'}}>
            <TextWrapper type='base' style={{color:'white'}}>Internet anda tidak stabil! check koneksi anda</TextWrapper>
        </View>
      );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    connectionStatus:state.rootReducer.service.connection.status,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ConnectionStability);