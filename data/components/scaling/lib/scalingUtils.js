import { Dimensions, PixelRatio } from 'react-native';
const { width, height } = Dimensions.get('window');
let [shortDimension, longDimension] = width < height ? [width, height] : [height, width];

//shortDimension *= PixelRatio.get();
//longDimension *= PixelRatio.get();

//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 480;
const guidelineBaseHeight = 800;

const scale = size => shortDimension / guidelineBaseWidth * size;
const verticalScale = size => longDimension / guidelineBaseHeight * size;
const moderateScale = (size, factor = 0.5) => size + ( scale(size) - size ) * factor;

export {scale, verticalScale, moderateScale};