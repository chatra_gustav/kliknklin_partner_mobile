import React from 'react';
import {StyleSheet, View, Dimensions, TouchableOpacity, Image } from 'react-native';
import { WheelPicker } from 'react-native-wheel-picker-android'

import {Scaling, Text} from 'app/data/components';
import * as CONSTANT from 'app/data/constant';
export default class WheelPickerLightBox extends React.Component {

  static defaultProps = {
    data: [],
    title: ''
  }

  render() {

    return(
      <View style={styles.container}>

        <View style={{marginTop: Scaling.moderateScale(10)}}>
          <Text fontOption={CONSTANT.TEXT_OPTION.SEE_STATISTICS_HEADER} textStyle={{color: CONSTANT.COLOR.BLACK}}>{this.props.title}</Text>
        </View>

        <View style={{marginTop: Scaling.moderateScale(20)}}>
          <WheelPicker
            data={this.props.data}
            onItemSelected={(data)=>{this.props.onPressButtonPrimary(data)}}
            isCurved
            indicatorColor='#505050'
            selectedItemTextColor={CONSTANT.COLOR.LIGHT_BLUE}/>
          </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: CONSTANT.COLOR.WHITE,
    borderRadius: 10,
    marginHorizontal: Scaling.moderateScale(20)
  },
});

