import React, { Component } from 'react';
import { 	
        Platform,
        Modal, 
        View,
        Text,
        Dimensions,
        Alert,
        ActivityIndicator
  		} from 'react-native';

import {TextWrapper} from './../../components';

export class LoadingModal extends Component {
	constructor(props){
    	super(props);
    	this.state = {
            progressPercent:0,
	    };
    }

    _setProgressPercent(percent){
        this.setState({progressPercent:percent});
    }

    _renderContent(){
    	if (this.props.type == undefined || this.props.type == 'standard') {
    		return(
    			<View style={{flexDirection:'row'}}>
	            	<TextWrapper fontWeight={'Medium'} type={'smallercontent'} style={{color:'white'}}>
                        {'  '}{this.props.loadingText}
                    </TextWrapper>
    			</View>
    		);
    	}
        else if (this.props.type == 'uploading') {
            return(
                <View style={{alignItems:'center'}}>
                    <View style={{flexDirection:'row'}}>
                        <ActivityIndicator
                            animating={true}
                            size="small"
                        />
                        <TextWrapper style={{fontSize:20,fontWeight:'bold',color:'white'}}>{' ' + this.props.loadingString}</TextWrapper>
                    </View>
                    <TextWrapper style={{fontSize:20,fontWeight:'bold',color:'white'}}>{this.state.progressPercent + '%'}</TextWrapper>
                </View>
            );
        }
    }

    render(){
    	return(
                    <View
                      style={{flexDirection:'row', flex:1, backgroundColor:'rgba(0, 0, 0, 0.6)', justifyContent:'center', alignItems:'center'}}
                    >
                        {this._renderContent()}
                    </View>
    	);
    }
}