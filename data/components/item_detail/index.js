import React, { Component } from 'react';
import {View, Image, ScrollView, StyleSheet, Dimensions, TouchableOpacity, TouchableWithoutFeedback} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

//node js class import

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling, toCurrency, Form} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

//local
class ItemDetail extends Component{
    static defaultProps = {
      item: [],
      customerName: '',
      logOrderPrice:{},
    }

    constructor(props) {
      super(props);
    
      this.state = {
        extractedItem:[],
        totalItemPrice:0,
      };
    }

    componentWillMount(){
        this.configOrderItem();
    }

    configOrderItem() {
        let result = [];
        let totalItemPrice = 0;
        let duration = -1;
        
        try{
          let {item} = this.props;

          for(let i in item){
            let data = item[i];
            let obj = {};
            obj.quantity = data.quantity;
            obj.unitName = data.outlet.outletitemwashingtime.itemwashingtime.item.unit_measurement;
            obj.name = data.outlet.outletitemwashingtime.itemwashingtime.item.name;
            obj.selectedOutletItemWashingTime = {};
            obj.selectedOutletItemWashingTime.price = data.item_price;
            obj.outletItemWashingTimeID = data.outlet.outletitemwashingtime.id;
            result.push(obj);
            totalItemPrice += (data.item_price * data.quantity);

            let currentDuration = data.outlet.outletitemwashingtime.duration
            if (currentDuration > duration) {
                duration = currentDuration;
            }
          }


        }
        catch(error){
          console.log(error, 'components.item_detail.getOrderItem')
        }

        this.setState({extractedItem:result, totalItemPrice, duration});
      }

    renderTopBar(){
        let {customerName} = this.props;
        let {duration} = this.state;
        return(
            <Items
                {...this.props} 
                containerStyle={{
                    marginBottom:Scaling.verticalScale(5),
                    marginTop:Scaling.verticalScale(54 * 0.444),
                    paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                }}
                leftComponent= {
                    <View style={{width:'100%'}}>
                        <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>
                            {customerName}
                        </Text>
                    </View>
                }
                rightComponent={<Text fontOption={CONSTANT.TEXT_OPTION.CONTENT}>{'Durasi : ' + duration + ' Jam'}</Text>}
            />
        )
    }

    render(){
        try{
            let {item, customer, logOrderPrice, promo} = this.props;
            let {extractedItem, totalItemPrice} = this.state;
            let promoDetail = [];

            if (promo.length > 0) {
                for(let i in promo){
                    let promo_source = promo[i].promo.promosource.info;
                    let discount_price = promo[i].discount_price;
                    let promo_quantity = promo[i].promo_quantity;

                    let leftComponent = (
                        <View
                            style={{
                                flexDirection:'row',
                            }}
                        >
                            <View
                                style={{
                                    flex:0.175,
                                }}
                            >
                                <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>{promo_quantity +' pcs'}</Text>
                            </View>

                            <View
                                style={{
                                    flex:0.825,
                                    justifyContent:'center',
                                }}
                            >
                                <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>{promo_source}</Text>
                            </View>
                        </View>
                    );

                    let rightComponent = (
                        <View
                            style={{
                                flexDirection:'row',
                            }}
                        >
                            <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>{toCurrency(-1 * discount_price * promo_quantity, 'Rp' )}</Text>
                        </View>
                    );

                    let component = (
                        <Items
                            {...this.props} 
                            key={i} 
                            leftComponent={leftComponent}
                            rightComponent={rightComponent}
                        />
                    );

                    promoDetail.push(component);
                }
            }

            return(
                    <View
                        style={{
                            height:'100%',
                        }}
                    >   
                        <Button.Cancel
                            containerStyle={{
                                top:'-1%',
                                right:0,
                            }}
                            size={'big'}
                        />
                        <View
                            style={{
                                height:Scaling.verticalScale(260),
                            }}
                        >

                            {this.renderTopBar()}

                            <ScrollView
                                    contentContainerStyle={{
                                        paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                                    }}
                            >

                                {extractedItem.map((item) => {
                                    let leftComponent = (
                                        <View
                                            style={{
                                                flexDirection:'row',
                                            }}
                                        >
                                            <View
                                                style={{
                                                    flex:0.175,
                                                }}
                                            >
                                                <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>{item.quantity + ' ' + item.unitName}</Text>
                                            </View>

                                            <View
                                                style={{
                                                    flex:0.825,
                                                    justifyContent:'center',
                                                }}
                                            >
                                                <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>{item.name}</Text>
                                            </View>
                                        </View>
                                    );

                                    let rightComponent = (
                                        <View
                                            style={{
                                                flexDirection:'row',
                                            }}
                                        >
                                            <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>{toCurrency(item.selectedOutletItemWashingTime.price * item.quantity , 'Rp' )}</Text>
                                        </View>
                                    );

                                    return(
                                        <Items
                                            {...this.props} 
                                            key={item.outletItemWashingTimeID} 
                                            leftComponent={leftComponent}
                                            rightComponent={rightComponent}
                                        />
                                    )
                                })}

                                {
                                    promoDetail.map((item) => item)
                                }
                            </ScrollView>
                        </View>
                        
                        <View
                            style={{
                                position:'absolute',
                                bottom:0,
                                width:'100%',
                                height:Scaling.verticalScale(120),
                                paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                                marginVertical:Scaling.verticalScale(10),
                            }}
                        >
                            <View
                                style={{
                                    width:'100%',
                                    marginBottom:Scaling.verticalScale(10),
                                    borderBottomWidth:Scaling.moderateScale(1),
                                    borderColor:CONSTANT.COLOR.GRAY,
                                }}
                            />

                            <Items
                                {...this.props} 
                                leftComponent= {
                                    <View style={{width:'100%', flexDirection:'row'}}>
                                        <View style={{width:'35%'}} />
                                            
                                        <View
                                            style={{width:'65%'}}
                                        >
                                            <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>
                                                {'Total Biaya Cucian'}
                                            </Text>
                                        </View>
                                    </View>
                                }
                                rightComponent={<Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>{toCurrency(totalItemPrice , 'Rp' )}</Text>}
                            />

                            <Items
                                {...this.props} 
                                leftComponent= {
                                    <View style={{width:'100%', flexDirection:'row'}}>
                                        <View style={{width:'35%'}} />
                                            
                                        <View
                                            style={{width:'65%'}}
                                        >
                                            <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>
                                                {'Pengiriman'}
                                            </Text>
                                        </View>
                                    </View>
                                }
                                rightComponent={<Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>{toCurrency(logOrderPrice.shipment_fare , 'Rp' )}</Text>}
                            />

                            <Items
                                {...this.props} 
                                leftComponent= {
                                    <View style={{width:'100%', flexDirection:'row'}}>
                                        <View style={{width:'35%'}} />
                                            
                                        <View
                                            style={{width:'65%'}}
                                        >
                                            <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>
                                                {'Total Diskon'}
                                            </Text>
                                        </View>
                                    </View>
                                }
                                rightComponent={<Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>{toCurrency(-1 * (logOrderPrice.marketing_cost_kliknklin + logOrderPrice.marketing_cost_outlet) , 'Rp' )}</Text>}
                            />

                            <Items
                                {...this.props} 
                                leftComponent= {
                                    <View style={{width:'100%', flexDirection:'row'}}>
                                        <View style={{width:'35%'}} />
                                            
                                        <View
                                            style={{width:'65%'}}
                                        >
                                            <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>
                                                {'TOTAL BAYAR'}
                                            </Text>
                                        </View>
                                    </View>
                                }
                                rightComponent={<Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>{toCurrency(logOrderPrice.full_price , 'Rp' )}</Text>}
                            />
                        </View>
                </View>
            );
        }
        catch(error) {
            console.log(error);
            return null;
        }
    }
}

class Items extends Component{
    static defaultProps = {
      leftComponent: null,
      rightComponent: null,
      containerStyle: {},
    }
    render(){
        try{
            let {leftComponent, rightComponent, containerStyle} = this.props;
            return (
                <View
                    style={[{
                        minHeight:Scaling.verticalScale(50 * 0.444),
                        width:'100%',
                        marginTop:Scaling.verticalScale(5 * 0.444),
                        flexDirection:'row',
                    }, containerStyle]}
                >
                    <View
                        style={{
                            flex:0.725
                        }}
                    >
                        {leftComponent}
                    </View>

                    <View
                        style={{
                            flex:0.275
                        }}
                    >
                        {rightComponent}
                    </View>
                </View>
            );
        }
        catch(error){
            console.log(error);
            return null;
        }
    }
}


function mapStateToProps(state, ownProps) {
    return {
        userRole:state.rootReducer.user.role,
        posSelectedItem:state.rootReducer.pos.selectedItem,
        posAvailableDuration:state.rootReducer.pos.availableDuration,
        posTotalPrice:state.rootReducer.pos.totalPrice,
        posSelectedDuration:state.rootReducer.pos.selectedDuration,
        posSelectedCustomer:state.rootReducer.pos.selectedCustomer,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actionPOS: bindActionCreators(actions.pos, dispatch),
        actionRoot: bindActionCreators(actions.root, dispatch),
        actionNavigator: bindActionCreators(actions.navigator, dispatch),
    };
}

const styles = StyleSheet.create({
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemDetail);
