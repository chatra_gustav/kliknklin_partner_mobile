import React, { Component } from 'react';
import {Modal, StyleSheet, View, Dimensions, TouchableOpacity, FlatList, ActivityIndicator, TouchableHighlight} from 'react-native';
import { LargeList } from "react-native-largelist-v3";
import {moderateScale} from 'react-native-size-matters';

import {Text, Scaling} from './../../../components';
import * as CONSTANT from './../../../constant';


//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class ComplaintItemDetail extends Component {
  static defaultProps = {
    data: [],
  }
  constructor(props) {
    super(props);
  
    this.state = {
    };
  }

  componentWillMount(){
    setTimeout(() => {
      this.setState({isFinishLoad:true});
    },50)
  }

  renderContent(){
    let data = this.props.item;
    return(
      <View
        style={{
          margin:Scaling.scale(10),
        }}
      >
          <View
              style={[{
                  minHeight:Scaling.verticalScale(50 * 0.444),
                  width:'100%',
                  marginTop:Scaling.verticalScale(5 * 0.444),
                  flexDirection:'row',
              },]}
          >
              <View
                  style={{
                      flex:0.4
                  }}
              >
                <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>{'Penjelasan Keluhan : '}</Text>
              </View>

              <View
                  style={{
                      flex:0.6
                  }}
              >
                  <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>{this.props.problemDetails}</Text>
              </View>
          </View>
          
          {data.map((item, key) => {
              let {orderitem} = item;
              let {outlet} = orderitem;
              let {outletitemwashingtime} = outlet;
              let {itemwashingtime} = outletitemwashingtime;
              let detailItem = itemwashingtime.item;
              let leftComponent = (
                  <View
                      style={{
                          flexDirection:'row',
                      }}
                  > 
                      <View
                          style={{
                              flex:0.075,
                          }}
                      >
                          <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>{(key + 1) + '. '}</Text>
                      </View>

                      <View
                          style={{
                              flex:0.925,
                          }}
                      >
                          <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>{detailItem.name}</Text>
                      </View>
                  </View>
              );

              let rightComponent = (
                  <View
                      style={{
                          flexDirection:'row',
                      }}
                  >
                  </View>
              );

              return(
                  <Items
                      {...this.props} 
                      key={item.id} 
                      leftComponent={leftComponent}
                      rightComponent={rightComponent}
                  />
              )
          })}
      </View>
    )
  }

  render() {
    if (this.state.isFinishLoad) {
      return (
        <TouchableHighlight
          hitSlop={{
            bottom:Dimensions.get('window').height * 0.5,
          }}

          underlayColor={'transparent'}
          style={[styles.container, this.props.containerStyle]}
          onPress={() =>{
              this.props.actionNavigator.dismissLightBox();
              this.props.actionNavigator.dismissModal();
          }}
        >
          <TouchableHighlight underlayColor={'transparent'} style={styles.contentContainer}>
              {this.renderContent()}
          </TouchableHighlight>
        </TouchableHighlight>
      );
    }
    return null;
  }
}

class LoadingContent extends Component{
  render(){
    return(         
       <ActivityIndicator size={moderateScale(24)} color={CONSTANT.COLOR.GRAY_B} /> 
    );
  }
}

class Items extends Component{
    static defaultProps = {
      leftComponent: null,
      rightComponent: null,
      containerStyle: {},
    }
    render(){
        try{
            let {leftComponent, rightComponent, containerStyle} = this.props;
            return (
                <View
                    style={[{
                        minHeight:Scaling.verticalScale(50 * 0.444),
                        width:'100%',
                        marginTop:Scaling.verticalScale(5 * 0.444),
                        flexDirection:'row',
                    }, containerStyle]}
                >
                    <View
                        style={{
                            flex:0.725
                        }}
                    >
                        {leftComponent}
                    </View>

                    <View
                        style={{
                            flex:0.275
                        }}
                    >
                        {rightComponent}
                    </View>
                </View>
            );
        }
        catch(error){
            console.log(error);
            return null;
        }
    }
}

const styles = StyleSheet.create({
  container: {
    width:'100%',
    height:'100%',
    alignItems:'center',
    justifyContent:'flex-end',
  },
  contentContainer:{
    width:'100%',
    height:'50%',
    backgroundColor:'white',
    zIndex:10, 
  },
  containerTitle:{
    flex:0.225,
    alignItems:'center',
    justifyContent:'center',
    alignSelf:'stretch',
    backgroundColor: CONSTANT.COLOR.BLUE_A,
  },
  containerButton:{
    flex:0.3,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-around',
  },
});



function mapStateToProps(state, ownProps) {
  return {
    delayOrderReasonList:state.rootReducer.masterData.delayOrderReasonList,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionMasterData: bindActionCreators(actions.masterData, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ComplaintItemDetail);
