import React, { Component } from 'react';
import {Modal, StyleSheet, View, Dimensions, TouchableOpacity, FlatList, ActivityIndicator, TouchableHighlight} from 'react-native';
import { LargeList } from "react-native-largelist-v3";
import {moderateScale} from 'react-native-size-matters';

import {Text, Scaling, toCurrency} from './../../../components';
import * as CONSTANT from './../../../constant';


//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";


class PosOldItemDetail extends Component {
  static defaultProps = {
    data: [],
  }
  constructor(props) {
    super(props);
  
    this.state = {
    };
  }

  componentWillMount(){
    setTimeout(() => {
      this.setState({isFinishLoad:true});
    },50)
  }

  renderContent(){
    return(
      <ItemDetail {...this.props}/>
    )
  }

  render() {
    if (this.state.isFinishLoad) {
      return (
        <TouchableHighlight
          hitSlop={{
            bottom:Dimensions.get('window').height * 0.5,
          }}

          underlayColor={'transparent'}
          style={[styles.container, this.props.containerStyle]}
          onPress={() =>{
              this.props.actionNavigator.dismissLightBox();
              this.props.actionNavigator.dismissModal();
          }}
        >
          <View style={styles.contentContainer}>
            {this.renderContent()}
          </View>
        </TouchableHighlight>
      );
    }
    return null;
  }
}

class ItemDetail extends Component{

  render(){
    //{this.getDiscount()}
    return(         
       <View
          style={{
            backgroundColor: CONSTANT.COLOR.WHITE
          }}
        >
          <View>
            {this.getOrderItem()}
            {this.getDetailPayment()}
          </View>
        </View>
    );
  }

  getUnitMeasurement(itemtransaction){
    let is_kilo = itemtransaction[0].itemvariant.item.is_kilo;

    if(is_kilo == 1){
      return 'kg'
    }
    else return ''
  }

  getOrderItem(){
    let {itemtransaction} = this.props.item;

    return(
      <View style={styles.orderItem}>
        {itemtransaction.map((item, index) => (
          <View key={index}>
            <View style={styles.orderDetail}>
              <View style={styles.detailLeft}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
                  {item.total_item} {this.getUnitMeasurement(itemtransaction)}
                </Text>
              </View>

              <View style={styles.detailCenter}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>
                  {item.itemvariant.item.name}
                </Text>
              </View>

              <View style={styles.detailRight}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
                  {toCurrency((item.sub_total_price).toString(), 'Rp ')}
                </Text>
              </View>
            </View>

            {this.renderItemNotes(item.notes)}
          </View>
        ))}
      </View>
    )
  }

  renderItemNotes(notes){
    if(notes != ''){
      return(
        <View style={styles.orderDetail}>
          <View style={styles.detailLeft}>
          </View>

          <View style={styles.detailCenter}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK, fontStyle: 'italic'}}>
              {notes}
            </Text>
          </View>

          <View style={styles.detailRight}>
          </View>
        </View>
      )
    }
    return null
  }

  // getDiscount(){
    
  //   return(
  //     <View style={styles.discount}>
  //         <View style={styles.detailLeft}>
  //           <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
  //             {""}
  //           </Text>
  //         </View>

  //         <View style={styles.detailCenter}>
  //           <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>
  //             {'Promo Welcome'}
  //           </Text>
  //         </View>

  //         <View style={styles.detailRight}>
  //           <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
  //             {toCurrency('20000', '- Rp ')}
  //           </Text>
  //         </View>
  //       </View>
  //   )
  // }

  getTotalPrice(){
    let subTotalPrice = this.getSubtotalPrice();
    let expressCharge = this.getExpressCharge();

    return subTotalPrice + expressCharge;
  }

  getSubtotalPrice(){
    let subTotalPrice = 0;
    let {itemtransaction} = this.props.item;

    itemtransaction.map((item) => {
      subTotalPrice += this.getTotalItemPrice(item);
    })

    return subTotalPrice;
  }

  getTotalItemPrice(item){
    return item.sub_total_price - (item.sub_total_price * item.itemvariant.discount / 100);
  }

  getExpressCharge(){
    let expressCharge = 0;
    let {express_charge} = this.props.item;

    if(express_charge){
      expressCharge = express_charge;
    }

    return expressCharge;
  }

  getDetailPayment(){
    return(
      <View style={styles.detailPayment}>
        {this.subTotal()}
        {this.expressCharge()}
        {this.totalPrice()}
      </View>
    )
  }

  subTotal(){
    return(
      <View style={styles.orderDetail}>
          <View style={styles.detailLeft}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
              {""}
            </Text>
          </View>

          <View style={styles.detailPaymentCenter}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
              {'Subtotal'}
            </Text>
          </View>

          <View style={styles.detailPaymentRight}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
              {toCurrency(this.getSubtotalPrice().toString(), 'Rp ')}
            </Text>
          </View>
        </View>
    )
  }

  expressCharge(){

    return(
      <View style={styles.orderDetail}>
          <View style={styles.detailLeft}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
              {""}
            </Text>
          </View>

          <View style={styles.detailPaymentCenter}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
              {'Biaya Ekspres'}
            </Text>
          </View>

          <View style={styles.detailPaymentRight}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
              {toCurrency(this.getExpressCharge().toString(), 'Rp ')}
            </Text>
          </View>
        </View>
    )
  }

  totalPrice(){
    let {orderPrice} = this.props;
    return(
      <View style={[styles.orderDetail, {paddingVertical: Scaling.verticalScale(10)}]}>
        <View style={styles.detailLeft}>
          <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
            {""}
          </Text>
        </View>

        <View style={styles.detailPaymentCenter}>
          <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
            {'TOTAL BAYAR'}
          </Text>
        </View>

        <View style={styles.detailPaymentRight}>
          <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{fontSize: Scaling.scale(20), color: CONSTANT.COLOR.BLACK}}>
            {toCurrency(this.getTotalPrice().toString(), 'Rp ')}
          </Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width:'100%',
    height:'100%',
    alignItems:'center',
    justifyContent:'flex-end',
  },
  paymentSection:{
    flex: 1,
    backgroundColor: CONSTANT.COLOR.WHITE,
    elevation:0
  },
  rowTitle:{
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: Scaling.verticalScale(10)
  },
  rowContent:{
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: Scaling.verticalScale(5),
    marginBottom: Scaling.verticalScale(10),
    paddingVertical: Scaling.verticalScale(5),
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: CONSTANT.COLOR.LIGHTER_GRAY_FONT
  },
  column:{
    marginRight: Scaling.moderateScale(15)  
  },
  contentLeft: {
    flex: 0.5
  },
  contentRight:{
    flex: 0.5,
    alignItems: 'flex-end'
  },
  orderDetail:{
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: Scaling.verticalScale(5)
  },
  detailLeft:{
    flex: 0.15
  },
  detailCenter:{
    flex: 0.65
  },
  detailRight:{
    flex: 0.2,
    alignItems: 'flex-end'
  },
  detailPaymentCenter:{
    flex: 0.5,
    alignItems: 'flex-end'
  },
  detailPaymentRight:{
    flex: 0.35,
    alignItems: 'flex-end'
  },
  discount:{
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: Scaling.verticalScale(5),
    paddingBottom: Scaling.verticalScale(10),
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: CONSTANT.COLOR.LIGHTER_GRAY_FONT
  },
  buttonTextStyle:{
      color: CONSTANT.COLOR.BLACK,
      fontSize: Scaling.moderateScale(13), 
      fontFamily: CONSTANT.TEXT_FAMILY.MAIN, 
      fontWeight: CONSTANT.TEXT_WEIGHT.REGULAR.weight
  },
  buttonStyle:{
    flexDirection: 'row',
      borderRadius: Scaling.moderateScale(10),
      height:CONSTANT.STYLE.BUTTON.THICKER_EXTRA_MEDIUM.HEIGHT,
      width:'100%',
      alignItems:'center',
    justifyContent:'center',
    borderColor: CONSTANT.COLOR.LIGHT_BLUE,
    borderWidth: 1
  },
  button:{
    width: '100%',
    alignItems: 'center',
    paddingVertical: Scaling.verticalScale(10)
  },
  contentContainer:{
    width:'100%',
    height:'50%',
    paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
    paddingVertical:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
    backgroundColor:'white',
    zIndex:10, 
  },
  orderItem:{
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: CONSTANT.COLOR.LIGHTER_GRAY_FONT,
    paddingBottom: Scaling.verticalScale(10)
  },
  detailPayment:{
    paddingTop: Scaling.verticalScale(10)
  }
});



function mapStateToProps(state, ownProps) {
  return {
    delayOrderReasonList:state.rootReducer.masterData.delayOrderReasonList,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionMasterData: bindActionCreators(actions.masterData, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PosOldItemDetail);
