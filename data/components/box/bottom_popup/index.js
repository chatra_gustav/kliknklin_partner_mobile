import React, { Component } from 'react';
import {Modal, StyleSheet, View, Dimensions, TouchableOpacity, FlatList, ActivityIndicator, TouchableHighlight} from 'react-native';
import { LargeList } from "react-native-largelist-v3";
import {moderateScale} from 'react-native-size-matters';

import {Text, Scaling, Button} from './../../../components';
import * as CONSTANT from './../../../constant';


//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";


class BottomPopup extends Component {
  static defaultProps = {
    height:'50%',
    hasTitle:false,
    hasCloseButton:false,
    title:'',
  }
  constructor(props) {
    super(props);
  
    this.state = {
    };
  }

  componentDidMount(){
    setTimeout(() => {
      this.setState({isFinishLoad:true});
    },50)
  }

  topBar(){
    let {hasTitle, hasCloseButton, title} = this.props;
    let titleComponent = null;
    let closeButtonComponent = null;
    
    if (hasTitle) {
      titleComponent = (
        <View
          style={styles.titleContainer}
        >
          <Text
            fontOption={CONSTANT.TEXT_OPTION.HEADER}
          >
            {title}
          </Text>
        </View>
      );
    }

    if (hasCloseButton) {
      closeButtonComponent = (
        <Button.Cancel />
      );
    }
    

    let topBar = (
      <View>
        {titleComponent}

        {closeButtonComponent}
      </View>
    );

    return topBar;
  }

  render() {
    let {children} = this.props;

    if (this.state.isFinishLoad) {
      return (
        <TouchableHighlight
          hitSlop={{
            bottom:Dimensions.get('window').height * 0.5,
          }}
          disabled
          underlayColor={'transparent'}
          style={[styles.container, this.props.containerStyle]}
          onPress={() =>{
              this.props.actionNavigator.dismissLightBox();
              this.props.actionNavigator.dismissModal();
          }}
        >
          <View 
            underlayColor={'transparent'} 
            style={[styles.contentContainer,{
              height:this.props.height,
            }]}>
              {this.topBar()}

              {children}
          </View>
        </TouchableHighlight>
      );
    }
    return null;
  }
}

class LoadingContent extends Component{
  render(){
    return(         
       <ActivityIndicator size={moderateScale(24)} color={CONSTANT.COLOR.GRAY_B} /> 
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width:'100%',
    height:'100%',
    alignItems:'center',
    justifyContent:'flex-end',
  },
  contentContainer:{
    width:'100%',
    height:'50%',
    backgroundColor:'white',
  },
  containerTitle:{
    flex:0.225,
    alignItems:'center',
    justifyContent:'center',
    alignSelf:'stretch',
    backgroundColor: CONSTANT.COLOR.BLUE_A,
  },
  containerButton:{
    flex:0.3,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-around',
  },
  titleContainer:{
    height:Scaling.verticalScale(45),
    alignItems:'center',
    justifyContent:'center',
    borderBottomWidth:Scaling.moderateScale(1),
    borderColor:CONSTANT.COLOR.LIGHT_GRAY,
    marginHorizontal:Scaling.scale(5),
  }
});



function mapStateToProps(state, ownProps) {
  return {
    delayOrderReasonList:state.rootReducer.masterData.delayOrderReasonList,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionMasterData: bindActionCreators(actions.masterData, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BottomPopup);
