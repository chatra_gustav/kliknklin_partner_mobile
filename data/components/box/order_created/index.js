import React, { Component } from 'react';
import {Modal, Image, StyleSheet, View, Dimensions, TouchableOpacity, FlatList, ActivityIndicator, TouchableHighlight} from 'react-native';
import { LargeList } from "react-native-largelist-v3";
import {moderateScale} from 'react-native-size-matters';
import PropTypes from 'prop-types';

import {Text, Scaling, Button, toCurrency,} from './../../../components';
import * as CONSTANT from './../../../constant';


//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";


class OrderCreated extends Component {
  static propTypes = {
    type: PropTypes.oneOf([
      'order-created',
      'paid-order',
      'taken-order',
    ]),
  }

  static defaultProps = {
    data: [],
    change: -1,
    type:'order-created',
  }

  constructor(props) {
    super(props);
  
    this.state = {
      currentKey:null,
      currentInfo:'',
      show:false,
      extractedItem:[],
    };
  }

  componentDidMount(){

  }

  componentWillMount(){
    setTimeout(() => {
        this.setState({isFinishLoad:true});
    },50)
  }

  _onPressSubmit(key, info){
    if (key == null) {
      alert('Pilih data yang diperlukan');
    }
    else{
      this.props.data.onPressButtonPrimary(key, info);
    }
  }

  renderChange(){
    if (this.props.change >= 0) {
      return(
        <View
          style={{
          }}
        >
          <Text textStyle={{textAlign:'center'}} fontOption={CONSTANT.TEXT_OPTION.HEADER}>{'Jumlah Kembalian Pelanggan: '}</Text>
          <Text textStyle={{textAlign:'center'}} fontOption={CONSTANT.TEXT_OPTION.SUMMARY_NUMBER_BOX_DASHBOARD}>{toCurrency(this.props.change, 'Rp')}</Text>
        </View>
      );
    }
  }

  cetak(){
    this.props.actionBluetoothDevices.cetak(this.props.active.data)
  }

  _getContent(){
    let {type} = this.props;
    if (type == 'order-created') {
      return(
        <View
          style={{
            flex:1,
          }}
        >
          <View
            style={{
              flex:0.2,
              justifyContent:'flex-end',
              alignItems:'center',
            }}
          >
              <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>Pesanan Terbuat!</Text>
          </View>

          <View
            style={{
              flex:0.55,
              alignItems:'center',
            }}
          >
            <Image
                source={CONSTANT.IMAGE_URL.TROLLEY_ORDER}
                style={{
                  width:Scaling.scale(800 * 0.444),
                  height:Scaling.scale(800 * 0.444),
                  resizeMode:'contain',
                }}
            /> 
          </View>

          <View
            style={{
              flex:0.25,
              alignItems:'center',
            }}
          >
              {this.renderChange()}


              <View
                style={{
                  flexDirection:'row',
                  marginTop:Scaling.verticalScale(5),
                }}
              >
                <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>{'Tekan'}</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.props.actionNavigator.dismissLightBox();
                    this.onPressCancel();
                  }}
                >
                    <Text 
                      fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}
                      textStyle={{
                        color:CONSTANT.COLOR.LIGHT_BLUE,
                      }}
                    >{' disini '}</Text>
                </TouchableOpacity>

                <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>{'untuk melihat detail pesanan'}</Text> 
              </View>
              
          </View>
        </View>
      );
    }
    else if (type == 'paid-order') {
      return(
        <View
          style={{
            flex:1,
          }}
        >
          <View
            style={{
              flex:0.2,
              justifyContent:'flex-end',
              alignItems:'center',
            }}
          >
              <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>Pembayaran Berhasil!</Text>
          </View>

          <View
            style={{
              flex:0.55,
              alignItems:'center',
            }}
          >
            <Image
                source={CONSTANT.IMAGE_URL.PAID_ORDER}
                style={{
                  width:Scaling.scale(800 * 0.444),
                  height:Scaling.scale(800 * 0.444),
                  resizeMode:'contain',
                }}
            /> 
          </View>

          <View
            style={{
              flex:0.25,
              alignItems:'center',
            }}
          >
              {this.renderChange()}

              <View
                style={{
                  flexDirection:'row',
                  marginTop:Scaling.verticalScale(5),
                }}
              >
                <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>{'Tekan'}</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.props.actionNavigator.dismissLightBox();
                    this.onPressCancel();
                  }}
                >
                    <Text 
                      fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}
                      textStyle={{
                        color:CONSTANT.COLOR.LIGHT_BLUE,
                      }}
                    >{' disini '}</Text>
                </TouchableOpacity>

                <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>{'untuk melihat detail pesanan'}</Text> 
              </View>
          </View>
        </View>
      );
    }
    else if (type =='taken-order') {
      return(
        <View
          style={{
            flex:1,
          }}
        >
          <View
            style={{
              flex:0.2,
              justifyContent:'flex-end',
              alignItems:'center',
            }}
          >
              <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>Pesanan Telah Diambil!</Text>
          </View>

          <View
            style={{
              flex:0.55,
              alignItems:'center',
            }}
          >
            <Image
                source={CONSTANT.IMAGE_URL.TAKEN_ORDER}
                style={{
                  width:Scaling.scale(800 * 0.444),
                  height:Scaling.scale(800 * 0.444),
                  resizeMode:'contain',
                }}
            /> 
          </View>

          <View
            style={{
              flex:0.25,
              alignItems:'center',
            }}
          >
              {this.renderChange()}

              <View
                style={{
                  flexDirection:'row',
                  marginTop:Scaling.verticalScale(5),
                }}
              >
                <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>{'Tekan'}</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.props.actionNavigator.dismissLightBox();
                    this.onPressCancel();
                  }}
                >
                    <Text 
                      fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}
                      textStyle={{
                        color:CONSTANT.COLOR.LIGHT_BLUE,
                      }}
                    >{' disini '}</Text>
                </TouchableOpacity>

                <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>{'untuk melihat detail pesanan'}</Text> 
              </View>
          </View>
        </View>
      );
    }
  }

  onPressCancel(){
    this.props.actionPOS.resetPOSData();
    this.props.actionOrder.getSummaryOrder();
  }

  render() {
    if (this.state.isFinishLoad) {
      return (
        <View
          style={{
            flex:1,
            backgroundColor:CONSTANT.COLOR.WHITE,
            //justifyContent:'center',
            //alignItems:'center',
          }}
        >
          <Button.Cancel
            onPress={this.onPressCancel.bind(this)}
          />
          
          <View
            style={{
              flex:0.75,
            }}
          >
            {this._getContent()}
          </View>

          <View
            style={{
              flex:0.25,
              alignItems:'center',
            }}
          >
            <Button.Standard
              buttonSize={CONSTANT.STYLE.BUTTON.LARGE}
              buttonColor={CONSTANT.COLOR.LIGHT_BLUE}
              onPress={() => this.cetak()}
            >
              <View
                style={{
                  flexDirection:'row',
                  width:'100%',
                  alignItems:'center',
                }}
              >
                <View
                  style={{
                    flex:0.3,
                    alignItems:'flex-end',
                  }}
                >
                  <Image
                      source={CONSTANT.IMAGE_URL.PRINT_ICON}
                      style={{
                        width:Scaling.scale(70 * 0.444),
                        height:Scaling.verticalScale(70 * 0.444),
                        resizeMode:'contain',
                      }}
                  /> 
                </View> 

                <View
                  style={{
                    flex:0.7,
                    marginLeft:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                  }}
                >
                  <Text fontOption={CONSTANT.TEXT_OPTION.BUTTON} textStyle={{color:CONSTANT.COLOR.WHITE}}>{'Cetak Struk Pesanan'}</Text>
                </View> 
              </View>
            </Button.Standard>
          </View>
        </View>
      );
    }
    return null;
  }
}

class LoadingContent extends Component{
  render(){
    return(         
       <ActivityIndicator size={moderateScale(24)} color={CONSTANT.COLOR.GRAY_B} /> 
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width:'100%',
    height:'100%',
    alignItems:'center',
    justifyContent:'flex-end',
  },
  contentContainer:{
    width:'100%',
    height:'50%',
    backgroundColor:'white',
    zIndex:10, 
  },
  containerTitle:{
    flex:0.225,
    alignItems:'center',
    justifyContent:'center',
    alignSelf:'stretch',
    backgroundColor: CONSTANT.COLOR.BLUE_A,
  },
  containerButton:{
    flex:0.3,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-around',
  },
  containerMessage:{
    flex:0.7,
    justifyContent:'center',
  },
  textTitle: {
    color:'white',
  },
  textMessage: {
    textAlign:'center',
  },
  textButton: {
    color:'white'
  },
  buttonPrimary:{
    width:Dimensions.get('window').width * 0.45,
    height:Dimensions.get('window').height * 0.05,
    backgroundColor:'#61CDFF',
    borderRadius:100,
    alignItems:'center',
    justifyContent:'center',
  },
});



function mapStateToProps(state, ownProps) {
  return {
    delayOrderReasonList:state.rootReducer.masterData.delayOrderReasonList,
    orderCreated:state.rootReducer.pos.orderCreated,
    active:state.rootReducer.order.active,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionMasterData: bindActionCreators(actions.masterData, dispatch),
    actionPOS: bindActionCreators(actions.pos, dispatch),
    actionOrder: bindActionCreators(actions.order, dispatch),
    actionBluetoothDevices: bindActionCreators(actions.bluetoothDevice, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderCreated);
