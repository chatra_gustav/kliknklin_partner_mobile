import React, { Component } from 'react';
import {
  NativeModules,
  Animated, 
  StyleSheet, 
  View, 
  Dimensions, 
  TouchableOpacity, 
  ActivityIndicator, 
  TouchableHighlight,
  Image
} from 'react-native';
import { LargeList } from "react-native-largelist-v3";
import {moderateScale} from 'react-native-size-matters';

import {Text, Scaling, Form, Button, Label} from 'app/data/components';
import * as CONSTANT from 'app/data/constant';


//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

const { UIManager } = NativeModules;
UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

class SingleInput extends Component {
  static defaultProps = {
    title:'',
    onPressButtonPrimary:()=>{},
    buttonPrimaryText:'',
    placeholder:'',
    validation:()=>{},
    notification:''
  }
  constructor(props) {
    super(props);
  
    this.state = {
      height:new Animated.Value(Dimensions.get('window').height * 0.375),
      inputValue:'',
      notification:this.props.notification,
    };
   
  }

  componentWillMount() {
  }
  
  componentDidMount(){
    setTimeout(() => {
        this.setState({isFinishLoad:true}, () => {
        });
    },50);
  }

  _renderNotification(){
    if (this.state.notification && this.state.notification.trim() != '') {
      return(
        <View
          style={{
            height:'90%',
            borderRadius:Scaling.moderateScale(10),
            backgroundColor:CONSTANT.COLOR.LIGHT_GRAY,
            justifyContent:'center',
            paddingHorizontal:Scaling.scale(5),
            paddingVertical:Scaling.verticalScale(5),
          }}
        >
          <Text fontOption={CONSTANT.TEXT_OPTION.CONTENT} textStyle={CONSTANT.COLOR.GRAY}>{this.state.notification}</Text>
        </View>
      );
    }
  }

  async _onPressButtonPrimary(){
    let {actionNavigator} = this.props;

    this.setState({notification:await this.props.validation(this.state.inputValue)}, () => {
        if (this.state.notification == '') {
          actionNavigator.dismissModal();
          actionNavigator.dismissLightBox();

          setTimeout(() => {
            this.props.onPressButtonPrimary(this.state.inputValue);
          },350);
        }
    });
  }

  _onChangeText(text){
    this.setState({inputValue:text});
  }

  _renderButton(){
    return(
      <Button.Standard
        buttonSize={CONSTANT.STYLE.BUTTON.EXTRA_MEDIUM}
        buttonText={this.props.buttonPrimaryText}
        buttonColor={CONSTANT.COLOR.BLUE}
        onPress={this._onPressButtonPrimary.bind(this)}
      />
    );
  }

  renderContent(){
    return(
      <Animated.View
        style={[{
          height:this.state.height,
        }]}
      >
        <View
          style={{
            flex:1,
            marginHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
            marginVertical:Scaling.verticalScale(40 * 0.444),
          }}
        >
          <View
              style={{
                height:Scaling.verticalScale(100 * 0.444),
                borderColor:CONSTANT.COLOR.LIGHT_GRAY,
                borderBottomWidth:Scaling.moderateScale(2),
                alignItems:'center',
                justifyContent:'center',
              }}
            >
              <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>{this.props.title}</Text>


              <Button.Cancel />
            </View>
            
            <View
              style={{
                height:Scaling.verticalScale(110 * 0.444),
                justifyContent:'center',
              }}
            >
              {this._renderNotification()}
            </View>
            
            <View
              style={{
                flex:1,
              }}
            >
              <Form.InputField.SemiRound
                  keyboardType={'numeric'}
                  maxLength={16}
                  returnKeyType={'go'}
                  ref={(ref) => this.phone_number_text_field = ref}
                  editable={this.state.searchResult && (this.state.searchResult == 'register' || this.state.searchResult == 'done')? false : true}
                  leftIcon={() => {
                      return(
                          <Image
                              source={this.props.leftIcon}
                              style={{
                                width:Scaling.scale(60 * 0.444),
                                height:Scaling.verticalScale(60 * 0.444),
                                resizeMode:'contain',
                              }}
                          /> 
                      );
                  }}
                  containerStyle={{
                    marginTop:Scaling.verticalScale(10),
                  }}
                  placeholder={this.props.placeholder}
                  onChangeText={this._onChangeText.bind(this)}
                  value={this.state.inputValue}
              />
            </View>

            <View
              style={{
                height:Scaling.verticalScale(100 * 0.444),
                justifyContent:'center',
                alignItems:'center',
              }}
            >
              {this._renderButton()}
            </View>
        </View>
      </Animated.View>
    );
  }

  _onPressSubmit(key, info){
    if (key == null) {
      alert('Pilih data yang diperlukan');
    }
    else{
      this.props.data.onPressButtonPrimary(key, info);
    }
  }

  render() {
    if (this.state.isFinishLoad) {
      return (
        <TouchableHighlight
          hitSlop={{
            bottom:Dimensions.get('window').height * 0.5,
          }}
          disabled={this.props.isDismissAllowed ? false : true}
          underlayColor={'transparent'}
          style={[styles.container, this.props.containerStyle]}
          onPress={() =>{
              this.props.actionNavigator.dismissLightBox();
              this.props.actionNavigator.dismissModal();
          }}
        >
            <View underlayColor={'transparent'} style={styles.contentContainer}>
              {this.renderContent()}
            </View>
        </TouchableHighlight>
      );
    }
    return null;
  }
}

const styles = StyleSheet.create({
  container: {
    width:'100%',
    height:'100%',
    alignItems:'center',
  },
  contentContainer:{
    width:'90%',
    position:'absolute',
    top:'25%',
    borderRadius:Scaling.moderateScale(12),
    backgroundColor:'white',
  },
  containerTitle:{
    flex:0.225,
    alignItems:'center',
    justifyContent:'center',
    alignSelf:'stretch',
    backgroundColor: CONSTANT.COLOR.BLUE_A,
  },
  containerButton:{
    flex:0.3,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-around',
  },
  containerMessage:{
    flex:0.7,
    justifyContent:'center',
  },
  textTitle: {
    color:'white',
  },
  textMessage: {
    textAlign:'center',
  },
  textButton: {
    color:'white'
  },
  buttonPrimary:{
    width:Dimensions.get('window').width * 0.45,
    height:Dimensions.get('window').height * 0.05,
    backgroundColor:'#61CDFF',
    borderRadius:100,
    alignItems:'center',
    justifyContent:'center',
  },
});



function mapStateToProps(state, ownProps) {
  return {
    customerSearchData:state.rootReducer.customer.search,
    customerTempData:state.rootReducer.customer.temp,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionMasterData: bindActionCreators(actions.masterData, dispatch),
    actionCustomer: bindActionCreators(actions.customer, dispatch),
    actionValidation: bindActionCreators(actions.validation, dispatch),
    actionPOS: bindActionCreators(actions.pos, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleInput);
