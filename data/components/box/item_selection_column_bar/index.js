import React, { Component } from 'react';
import {ScrollView, StyleSheet, View, Dimensions, TouchableOpacity, FlatList, ActivityIndicator, TouchableHighlight} from 'react-native';
import { LargeList } from "react-native-largelist-v3";
import {moderateScale} from 'react-native-size-matters';

import {Text, Scaling, Button} from './../../../components';
import * as CONSTANT from './../../../constant';


//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";


class ItemSelectionColumnBar extends Component {
  static defaultProps = {
    data: [],
    onPressButtonPrimary: () => {},
    //isItemDisabled: () => false,
  }
  constructor(props) {
    super(props);
  
    this.state = {
      currentKey:null,
      currentInfo:'',
      show:false,
      extractedItem:[],
    };
  }

  componentWillMount(){
    this.setState({data:[...this.props.data]}, () => {
      setTimeout(() => {
        this.setState({isFinishLoad:true});
      },50)
    });
  }


  renderContent(){
    let data = this.state.data;
    let extractedData =[];
    let counter = 0;
    let eachData = [];
    for(let i in data){
      eachData.push(data[i]);
      counter += 1;

      if (counter == 2) {
        counter = 0;
        extractedData.push(eachData);
        eachData = [];
      }
    }

    if (counter == 1) {
      extractedData.push(eachData);
    }

    return(
      <View
        style={{
          flex:1,
          marginHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
          marginVertical:Scaling.verticalScale(40 * 0.444),
        }}
      >
          <View
            style={{
              height:Scaling.verticalScale(100 * 0.444),
              borderColor:CONSTANT.COLOR.LIGHT_GRAY,
              borderBottomWidth:Scaling.moderateScale(2),
              alignItems:'center',
              justifyContent:'center',
            }}
          >
            <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>{this.props.title}</Text>


            <Button.Cancel/>
          </View>

          <ScrollView
            style={{
              marginVertical:Scaling.verticalScale(40 * 0.444),
            }}
          >
              {extractedData.map((value, index) => {
                return (
                  <View
                    key={index}
                    style={{
                      flexDirection:'row',
                      marginBottom:Scaling.verticalScale(60 * 0.444),
                      justifyContent:'space-between',
                    }}
                  >
                    {value.map((value, index) => {
                      return (
                        <TouchableOpacity
                          key={index}
                          disabled={this.props.isItemDisabled(value.duration) ? true : false}
                          style={{
                            width:Scaling.scale(425 * 0.444),
                            height:Scaling.verticalScale(200 * 0.444),
                            borderRadius:Scaling.moderateScale(15),
                            borderColor:this.props.isItemDisabled(value.duration) ? CONSTANT.COLOR.LIGHT_GRAY : CONSTANT.COLOR.LIGHT_BLUE,
                            borderWidth:Scaling.moderateScale(1),
                            alignItems:'center',
                            justifyContent:'center',
                            backgroundColor:this.props.isItemDisabled(value.duration) ? CONSTANT.COLOR.LIGHT_GRAY : CONSTANT.COLOR.WHITE,
                          }}

                          onPress={this._onPressSubmit.bind(this,value)}
                        >
                          <Text 
                            fontOption={CONSTANT.TEXT_OPTION.ADD_SYMBOL_BOX_DASHBOARD} 
                            textStyle={{color:this.props.isItemDisabled(value.duration) ? CONSTANT.COLOR.GRAY : CONSTANT.COLOR.LIGHT_BLUE}}>
                            {value.duration + ' Jam'}
                          </Text>
                        </TouchableOpacity>
                      );
                    })}
                  </View>
                );
              })}
          </ScrollView>
      </View>
    );
  }

  _onPressSubmit(value){
    this.props.onPressButtonPrimary(value);
  }

  render() {
    if (this.state.isFinishLoad) {
      return (
        <TouchableHighlight
          hitSlop={{
            bottom:Dimensions.get('window').height * 0.65,
          }}

          underlayColor={'transparent'}
          style={[styles.container, this.props.containerStyle]}
          onPress={() =>{
              this.props.actionNavigator.dismissLightBox();
              this.props.actionNavigator.dismissModal();
          }}
        >
          <TouchableHighlight underlayColor={'transparent'} style={styles.contentContainer}>
              {this.renderContent()}
          </TouchableHighlight>
        </TouchableHighlight>
      );
    }
    return null;
  }
}


class LoadingContent extends Component{
  render(){
    return(         
       <ActivityIndicator size={moderateScale(24)} color={CONSTANT.COLOR.GRAY_B} /> 
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width:'100%',
    height:'100%',
    alignItems:'center',
    justifyContent:'flex-end',
  },
  contentContainer:{
    width:'100%',
    height:'65%',
    backgroundColor:'white',
    zIndex:10, 
  },
  containerTitle:{
    flex:0.225,
    alignItems:'center',
    justifyContent:'center',
    alignSelf:'stretch',
    backgroundColor: CONSTANT.COLOR.BLUE_A,
  },
  containerButton:{
    flex:0.3,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-around',
  },
  containerMessage:{
    flex:0.7,
    justifyContent:'center',
  },
  textTitle: {
    color:'white',
  },
  textMessage: {
    textAlign:'center',
  },
  textButton: {
    color:'white'
  },
  buttonPrimary:{
    width:Dimensions.get('window').width * 0.45,
    height:Dimensions.get('window').height * 0.05,
    backgroundColor:'#61CDFF',
    borderRadius:100,
    alignItems:'center',
    justifyContent:'center',
  },
});



function mapStateToProps(state, ownProps) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionMasterData: bindActionCreators(actions.masterData, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemSelectionColumnBar);
