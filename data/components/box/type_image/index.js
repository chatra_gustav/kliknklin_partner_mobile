import React from 'react';
import {StyleSheet, View, Dimensions, TouchableOpacity, FlatList,WebView, Image} from 'react-native';
import {moderateScale} from 'react-native-size-matters';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';

import {TextWrapper} from './../../../components';
import * as CONSTANT from './../../../constant';


//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";


class TypeImage extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {
      currentKey:0,
      image:{
        cancel_icon:{
          width:0,
          height:0,
          multiplierWidth:0.2,
          multiplierHeight:0.2,
          url:require('./../../../image/icon/cancel_icon.png'),
        },
        source_image:{
          width:Dimensions.get('window').width * 1,
          height:Dimensions.get('window').width * 1,
          multiplierWidth:0.11,
          multiplierHeight:0.11,
          url:this.props.sourceImage,
        }
      }
    };
  }

  componentWillMount(){
    this._getImageSize();
  }

  _getImageSize(){
    var image = this.state.image;

    var image_cancel_icon = ResolveAssetSource(this.state.image.cancel_icon.url);

    image.cancel_icon.width = moderateScale(image_cancel_icon.width);
    image.cancel_icon.height = moderateScale(image_cancel_icon.height);

    this.setState({image});
  }

  _onPressCancelButton(){
    let {actionNavigator} = this.props;

    actionNavigator.dismissLightBox();
    actionNavigator.dismissAllModals();
  }

  render() {
    let {sourceImage, title} = this.props;
    return (
      <View style={styles.container}>
        <View
          style={{
            flex:0.2,
          }}
        >
          <TouchableOpacity
            style={styles.cancelButton}
            onPress={this._onPressCancelButton.bind(this)}
          >
            <ResponsiveImage
              source={this.state.image.cancel_icon.url}
              initWidth={this.state.image.cancel_icon.width * this.state.image.cancel_icon.multiplierWidth}
              initHeight={this.state.image.cancel_icon.height * this.state.image.cancel_icon.multiplierHeight}
              style={{
                tintColor:CONSTANT.COLOR.GRAY_B,
              }}
            />
          </TouchableOpacity>
        
        </View>

        <View style={[styles.contentContainer, {
            //width:this.state.image.source_image.width + moderateScale(10),
            //height:this.state.image.source_image.height + moderateScale(10),
            //borderRadius:(this.state.image.source_image.height + moderateScale(10)) / 2,
        }]}>
          <Image
              style={{
                  width:this.state.image.source_image.width,
                  height:this.state.image.source_image.height,
                  //borderRadius:this.state.image.source_image.height / 2,
                  //borderWidth:1,
              }}
              source={this.props.sourceImage}
              resizeMode={'contain'}
          />
        </View>
      </View>
    );
  }
}

class LoadingContent extends React.Component{

    render(){
        return (
            <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                <TextWrapper type={'smallercontent'}>{'Memuat Konten...'}</TextWrapper>
            </View>
        )
    }
}

TypeImage.defaultProps = {
  sourceImage: '',
};

const styles = StyleSheet.create({
  container: {
    height:Dimensions.get('window').height * 0.7,
    width:Dimensions.get('window').width,
    backgroundColor:'rgba(0,0,0,0)',
  },
  contentContainer:{
    flex:0.8,
    alignItems:'center',
    justifyContent:'center',
  },
  topSection:{
    flex:0.2,
    borderBottomWidth:1,
    borderColor:CONSTANT.COLOR.GRAY_B,
    alignItems:'center',
    justifyContent:'center',
  },
  titleText:{
    color:'black',
  },
  mainSection:{
    flex:1,
  },
  cancelButton:{
    position:'absolute',
    right:moderateScale(10),
    top:moderateScale(10),
  },
});



function mapStateToProps(state, ownProps) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TypeImage);
