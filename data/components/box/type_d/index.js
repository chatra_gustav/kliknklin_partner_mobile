import React from 'react';
import {StyleSheet, View, Dimensions, TouchableOpacity, FlatList,WebView} from 'react-native';
import {moderateScale} from 'react-native-size-matters';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';

import {TextWrapper} from './../../../components';
import * as CONSTANT from './../../../constant';


//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";


class TypeD extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {
      currentKey:0,
      image:{
        cancel_icon:{
          width:0,
          height:0,
          multiplierWidth:0.11,
          multiplierHeight:0.11,
          url:require('./../../../image/icon/cancel_icon.png'),
        },
      }
    };
  }

  componentWillMount(){
    this._getImageSize();
  }

  _getImageSize(){
    var image = this.state.image;

    var image_cancel_icon = ResolveAssetSource(this.state.image.cancel_icon.url);

    image.cancel_icon.width = moderateScale(image_cancel_icon.width);
    image.cancel_icon.height = moderateScale(image_cancel_icon.height);

    this.setState({image});
  }

  _onPressCancelButton(){
    let {actionNavigator} = this.props;

    actionNavigator.dismissLightBox();
  }

  render() {
    let {source, title} = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.contentContainer}>
          <View style={styles.topSection}>

            <TextWrapper type={'smallcontent'} style={styles.titleText}>
              {title}
            </TextWrapper>

            <TouchableOpacity
              style={styles.cancelButton}
              onPress={this._onPressCancelButton.bind(this)}
            >
              <ResponsiveImage
                source={this.state.image.cancel_icon.url}
                initWidth={this.state.image.cancel_icon.width * this.state.image.cancel_icon.multiplierWidth}
                initHeight={this.state.image.cancel_icon.height * this.state.image.cancel_icon.multiplierHeight}
                style={{
                  tintColor:CONSTANT.COLOR.GRAY_B,
                }}
              />
            </TouchableOpacity>
          </View>

          <WebView
              startInLoadingState={true}
              renderLoading={() => {
                      return (
                          <LoadingContent/>
                      )
                  }
              }
              source={{uri: source}}
              style={styles.mainSection}
          />

        </View>
      </View>
    );
  }
}

class LoadingContent extends React.Component{

    render(){
        return (
            <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                <TextWrapper type={'smallercontent'}>{'Memuat Konten...'}</TextWrapper>
            </View>
        )
    }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems:'center',
    justifyContent:'center',
  },
  contentContainer:{
    width: Dimensions.get('window').width * 0.9,
    height: Dimensions.get('window').height * 0.75,
    backgroundColor: '#ffffff',
    borderRadius:5,
    //paddingHorizontal:Dimensions.get('window').width * 0.025,
    //paddingVertical:Dimensions.get('window').height * 0.015,
  },
  topSection:{
    flex:0.2,
    borderBottomWidth:1,
    borderColor:CONSTANT.COLOR.GRAY_B,
    alignItems:'center',
    justifyContent:'center',
  },
  titleText:{
    color:'black',
  },
  mainSection:{
    flex:1,
  },
  cancelButton:{
    position:'absolute',
    right:moderateScale(5),
    top:moderateScale(5),
  },
});



function mapStateToProps(state, ownProps) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TypeD);
