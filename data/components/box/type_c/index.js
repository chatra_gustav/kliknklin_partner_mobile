import React, { Component } from 'react';
import {StyleSheet, View, Dimensions, TouchableOpacity, FlatList,ActivityIndicator} from 'react-native';
import {moderateScale} from 'react-native-size-matters';

import {TextWrapper} from './../../../components';
import * as CONSTANT from './../../../constant';


//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";


class TypeC extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      currentKey:null,
      currentInfo:'',
    };
  }

  componentDidMount(){
    this._config();
  }

  _config(){
    let {data, actionMasterData} = this.props;
    let {itemList, type} = data;
    if (type == 'delay-reason') {
        //console.log(actionMasterData);
        actionMasterData.getDelayOrderReasonList();
    }
    else if (type == 'reject-reason') {
        actionMasterData.getRejectOrderReasonList();
    }
  }

  _getData(){
    let {data, delayOrderReasonList, rejectOrderReasonList} = this.props;
    let {itemList, type} = data;
    if (type == 'delay-reason') {
      //console.log(delayOrderReasonList);
      return delayOrderReasonList;
    }
    else if (type == 'reject-reason') {
      return rejectOrderReasonList;
    }
    else if (type == 'custom') {
      return itemList;
    }

    return [];
  }

  renderContent(){
    let data = this._getData();
    
    if (data == null) {
      return(
          <View
            style={{
              flex:1,
              alignItems:'center',
              justifyContent:'center',
            }}
          >
            <LoadingContent/>
          </View>
      );
    }

    return(
      <FlatList
        data={data}
        keyExtractor={(item, index) => index+''}
        contentContainerStyle={{
        }}
        style={{
          flex:0.6,
        }}
        extraData={this.state.currentKey}
        renderItem={(item) => { 
                return (
                  <TouchableOpacity
                    style={{
                      alignSelf:'stretch',
                      height:Dimensions.get('window').height * 0.075,
                      alignItems:'center',
                      justifyContent:'center',
                      marginHorizontal:moderateScale(5),
                    }}

                    onPress={() =>{
                        this.setState({currentKey:item.item.id, currentInfo:item.item.info});
                    }}
                  >
                      <TextWrapper type={'smallcontent'} style={{textAlign:'center',color:this.state.currentKey == item.item.id ? CONSTANT.COLOR.BLUE_A : 'gray'}}>{item.item.info}</TextWrapper>
                  </TouchableOpacity>
                );
            }}
      /> 
    );
  }

  _onPressSubmit(key, info){
    if (key == null) {
      alert('Pilih data yang diperlukan');
    }
    else{
      this.props.data.onPressButtonPrimary(key, info);
    }
  }

  render() {
    return (
      <View style={[styles.container, this.props.containerStyle]}>


        <View style={styles.containerTitle}>
            <TextWrapper fontWeight={'SemiBold'} type={'content'} style={styles.textTitle}>{this.props.data.titleText}</TextWrapper>
            

            <TouchableOpacity
              style={{
                position:'absolute',
                right:'5%',
                top:'30%',
                width:moderateScale(25),
                height:moderateScale(25),
                borderRadius:moderateScale(25/2),
                borderWidth:moderateScale(1),
                borderColor:'white',
                backgroundColor:'transparent',
                alignItems:'center',
                justifyContent:'center'
              }}

              onPress={() => {
                this.props.actionNavigator.dismissLightBox();
              }}
            >
            <TextWrapper fontWeight={'SemiBold'} type={'content'} style={{color:'white'}}>X</TextWrapper>
        </TouchableOpacity>
        </View>      

        <View style={styles.contentContainer}>
            {this.renderContent()}
        </View>

        <View style={styles.containerButton}>
          <TouchableOpacity
            style={styles.buttonPrimary}
            onPress={this._onPressSubmit.bind(this, this.state.currentKey, this.state.currentInfo)}
          >
            <TextWrapper type={'smallcontent'} style={styles.textButton}>
              {this.props.data.buttonPrimaryText}
            </TextWrapper>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

TypeC.defaultProps = {
    containerStyle: {},
    data:{
      titleText: 'title',
    },
}

class LoadingContent extends Component{
  render(){
    return(         
       <ActivityIndicator size={moderateScale(24)} color={CONSTANT.COLOR.GRAY_B} /> 
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width * 0.85,
    height: Dimensions.get('window').height * 0.4,
    backgroundColor: '#ffffff',
    borderRadius:5,
    marginHorizontal:Dimensions.get('window').width * 0.025,
    marginVertical:Dimensions.get('window').height * 0.015,
  },
  contentContainer:{
    flex:0.775, 
  },
  containerTitle:{
    flex:0.225,
    alignItems:'center',
    justifyContent:'center',
    alignSelf:'stretch',
    backgroundColor: CONSTANT.COLOR.BLUE_A,
  },
  containerButton:{
    flex:0.3,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-around',
  },
  containerMessage:{
    flex:0.7,
    justifyContent:'center',
  },
  textTitle: {
    color:'white',
  },
  textMessage: {
    textAlign:'center',
  },
  textButton: {
    color:'white'
  },
  buttonPrimary:{
    width:Dimensions.get('window').width * 0.45,
    height:Dimensions.get('window').height * 0.05,
    backgroundColor:'#61CDFF',
    borderRadius:100,
    alignItems:'center',
    justifyContent:'center',
  },
});



function mapStateToProps(state, ownProps) {
  return {
    delayOrderReasonList:state.rootReducer.masterData.delayOrderReasonList,
    rejectOrderReasonList:state.rootReducer.masterData.rejectOrderReasonList,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionMasterData: bindActionCreators(actions.masterData, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TypeC);
