import React from 'react';
import {StyleSheet, View, Dimensions, TouchableOpacity} from 'react-native';

import {TextWrapper, Scaling} from './../../../components';

export default class TypeB extends React.Component {

  render() {
    if(this.props.title == ''){
      return(
        <View style={styles.container}>
          <View style={styles.containerMessage}>
            <TextWrapper type={'smallercontent'} style={styles.textMessage}>{this.props.message}</TextWrapper>
          </View>
        </View>
      )
    }else{
      return (
        <View style={styles.container}>
          <View style={styles.containerTitle}>
            <TextWrapper type={'smallcontent'} style={styles.textTitle}>{this.props.title}</TextWrapper>
          </View>
          <View style={styles.containerMessage}>
            <TextWrapper type={'smallercontent'} style={styles.textMessage}>{this.props.message}</TextWrapper>
          </View>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width * 0.85,
    backgroundColor: '#ffffff',
    borderRadius:5,
    paddingHorizontal:Dimensions.get('window').width * 0.025,
    paddingVertical:Dimensions.get('window').height * 0.015,
    paddingBottom: Scaling.moderateScale(15)
  },
  containerContent:{
    alignItems:'center',
  },
  containerTitle:{
    justifyContent:'center',
    marginBottom: Scaling.moderateScale(25)
  },
  containerMessage:{
    justifyContent:'center',
  },
  textTitle: {
    fontWeight: '700',
    textAlign:'center',
  },
  textMessage: {
    textAlign:'center',
  }
});

