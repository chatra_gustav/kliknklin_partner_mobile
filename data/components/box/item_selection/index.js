import React, { Component } from 'react';
import {Modal, StyleSheet, View, Dimensions, TouchableOpacity, FlatList, ActivityIndicator, TouchableHighlight} from 'react-native';
import { LargeList } from "react-native-largelist-v3";
import {moderateScale} from 'react-native-size-matters';

import {Text, Scaling, Button, Form, Box} from './../../../components';
import * as CONSTANT from './../../../constant';


//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";


class ItemSelection extends Component {
  static defaultProps = {
    data: [],
    hasAutoComplete:false,
    onPressButtonPrimary:()=>{},
  }
  constructor(props) {
    super(props);
  
    this.state = {
      currentKey:null,
      currentInfo:'',
      extractedItem:[],
      sourceItem:[],
    };
  }

  componentWillMount(){
    let extractedData = this.extractData(this.props.data);
    this.setState({sourceItem:JSON.parse(JSON.stringify(extractedData)), extractedItem:JSON.parse(JSON.stringify(extractedData))}, () => {
      setTimeout(() => {
        this.setState({isFinishLoad:true});
      },50)
    });
  }

  extractData(list){
    let extractedItem = [];
    const sContent = { items: [] };
    for (let section = 0; section < list.length; ++section){
        sContent.items.push(list[section][this.props.categoryToFind]);
    }
    extractedItem.push(sContent);
    return extractedItem;
  }
  
  getAutoComplete(){
    let {hasAutoComplete} = this.props;

    if (hasAutoComplete) {
      return(
        <View
          style={{
            width:'100%',
            alignItems:'center',
            marginTop:Scaling.verticalScale(5),
          }}
        >
            <Form.InputField.Underline
              placeholder={'Cari..'}
              width={'90%'}
              containerStyle={{
                borderBottomWidth:Scaling.moderateScale(1),
              }}
              onChangeText={(itemToFind) => {
                let sourceItem = this.state.sourceItem[0].items.slice(0);
                let data = sourceItem.filter(function (str) { 
                  return str.toLowerCase().includes(itemToFind.toLowerCase()); 
                });
                let extractedItem = this.state.extractedItem;
                extractedItem[0].items = data;
                this.setState({extractedItem});
              }}
              {...this.props}
            />
        </View>
      );
    }
    return null;
  }

  renderContent(){
    let {data} = this.props;
    if (data == null) {
      return(
          <View
            style={{
              flex:1,
              alignItems:'center',
              justifyContent:'center',
            }}
          >
            <LoadingContent/>
          </View>
      );
    }
    return(
      <View
        style={{
          flex:1,
        }}
      >
        {this.getAutoComplete()}

        <LargeList
          data={this.state.extractedItem}
          heightForIndexPath={({section, row}) => {
              return Scaling.verticalScale(125 * 0.444);
          }}
          renderIndexPath={({section, row}) => { 
              return (
                <TouchableOpacity
                  style={{
                    alignSelf:'stretch',
                    height:Scaling.verticalScale(125 * 0.444),
                    justifyContent:'center',
                    marginHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                    borderBottomWidth:Scaling.verticalScale(1),
                    borderColor:CONSTANT.COLOR.LIGHT_GRAY
                  }}

                  onPress={() =>{
                      this.props.onPressButtonPrimary(row);
                      this.props.actionNavigator.dismissLightBox();
                      this.props.actionNavigator.dismissModal();
                  }}
                >
                    <Text fontOption={CONSTANT.TEXT_OPTION.BUTTON}>
                      {this.state.extractedItem[section].items[row]}
                    </Text>
                </TouchableOpacity>
              );
          }}
        /> 
      </View>
    );
  }

  _onPressSubmit(key, info){
    if (key == null) {
      alert('Pilih data yang diperlukan');
    }
    else{
      this.props.data.onPressButtonPrimary(key, info);
    }
  }

  render() {
    return(
      <Box.BottomPopup
        {...this.props}
      >
        {this.renderContent()}
      </Box.BottomPopup>
    );
  }
}

class LoadingContent extends Component{
  render(){
    return(         
       <ActivityIndicator size={moderateScale(24)} color={CONSTANT.COLOR.GRAY_B} /> 
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width:'100%',
    height:'100%',
    alignItems:'center',
    justifyContent:'flex-end',
  },
  contentContainer:{
    width:'100%',
    height:'55%',
    backgroundColor:'white',
    zIndex:10, 
  },
  containerTitle:{
    flex:0.225,
    alignItems:'center',
    justifyContent:'center',
    alignSelf:'stretch',
    backgroundColor: CONSTANT.COLOR.BLUE_A,
  },
  containerButton:{
    flex:0.3,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-around',
  },
  containerMessage:{
    flex:0.7,
    justifyContent:'center',
  },
  textTitle: {
    color:'white',
  },
  textMessage: {
    textAlign:'center',
  },
  textButton: {
    color:'white'
  },
  buttonPrimary:{
    width:Dimensions.get('window').width * 0.45,
    height:Dimensions.get('window').height * 0.05,
    backgroundColor:'#61CDFF',
    borderRadius:100,
    alignItems:'center',
    justifyContent:'center',
  },
});



function mapStateToProps(state, ownProps) {
  return {
    delayOrderReasonList:state.rootReducer.masterData.delayOrderReasonList,
    rejectOrderReasonList:state.rootReducer.masterData.rejectOrderReasonList,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionMasterData: bindActionCreators(actions.masterData, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemSelection);
