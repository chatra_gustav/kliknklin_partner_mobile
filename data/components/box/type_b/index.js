import React from 'react';
import {StyleSheet, View, Dimensions, TouchableOpacity} from 'react-native';

import {TextWrapper, Button} from './../../../components';
import * as CONSTANT from 'app/data/constant';

export default class TypeB extends React.Component {

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.containerContent}>
          <View style={styles.containerTitle}>
            <TextWrapper type={'smallcontent'} style={styles.textTitle}>{this.props.title}</TextWrapper>
          </View>
          <View style={styles.containerMessage}>
            <TextWrapper type={'smallercontent'} style={styles.textMessage}>{this.props.message}</TextWrapper>
          </View>
        </View>

        <View style={styles.containerButton}>
          <Button.Standard
            buttonSize={CONSTANT.STYLE.BUTTON.EXTRA_MEDIUM}
            buttonColor={CONSTANT.COLOR.LIGHT_BLUE}
            buttonText={this.props.buttonPrimaryText}
            onPress={this.props.onPressButtonPrimary}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width * 0.85,
    height: Dimensions.get('window').height * 0.25,
    backgroundColor: '#ffffff',
    borderRadius:5,
    paddingHorizontal:Dimensions.get('window').width * 0.025,
    paddingVertical:Dimensions.get('window').height * 0.015,
  },
  containerContent:{
    flex:0.65, 
    alignItems:'center',
  },
  containerButton:{
    flex:0.35,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-around',
  },
  containerTitle:{
    flex:0.3,
    justifyContent:'center',
  },
  containerMessage:{
    flex:0.7,
    justifyContent:'center',
  },
  textTitle: {
    fontWeight: '700',
    textAlign:'center',
  },
  textMessage: {
    textAlign:'center',
  },
  textButton: {
    color:'white'
  },
  buttonPrimary:{
    width:Dimensions.get('window').width * 0.45,
    height:Dimensions.get('window').height * 0.05,
    backgroundColor:CONSTANT.COLOR.BLUE,
    borderRadius:100,
    alignItems:'center',
    justifyContent:'center',
  },
});

