import React, { Component } from 'react';
import {Modal, StyleSheet, View, Dimensions, TouchableOpacity, FlatList, ActivityIndicator, TouchableHighlight, Image} from 'react-native';
import { LargeList } from "react-native-largelist-v3";
import {moderateScale} from 'react-native-size-matters';

import {Text, Scaling, toCurrency, Button} from './../../../components';
import * as CONSTANT from './../../../constant';


//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";


class ItemSelectionDetailed extends Component {
  static defaultProps = {
    data: [],
    title:'',
    itemTitle:'',
    itemDescription:'',
    rowDescriptionCategory:'',
    rowInfoCategory:'',
    rowPriceCategory:'',
    onPressButtonPrimary:() => {},
    onPressButtonSecondary:() => {},
    isShowConfirmation:true,
  }

  constructor(props) {
    super(props);
  
    this.state = {
      currentKey:null,
      currentInfo:'',
      show:false,
      extractedItem:[],
      isFinishLoad:false,
    };
  }

  componentWillMount(){
    this.setState({extractedItem:this.props.data}, () => {
      setTimeout(() => {
        this.setState({isFinishLoad:true});
      },50)
    });
  }

  componentDidMount(){
  }

  show(isShow){
    this.setState({
      show:isShow,
    });
  }

  renderContent(){
    let data = this.props.data;
    let source = CONSTANT.IMAGE_URL.KLIKNKLIN_ICON;
    let imageExist;
    let imageURL = data[0].items[0].item.item_image_url;

    if (imageURL == "") {
        source = CONSTANT.IMAGE_URL.KLIKNKLIN_ICON;
        imageExist = false;
    }
    else{
        source = {uri:imageURL};
        imageExist = true;
    }
    console.log(source);
    if (data == null) {
      return(
          <View
            style={{
              flex:1,
              alignItems:'center',
              justifyContent:'center',
            }}
          >
            <LoadingContent/>
          </View>
      );
    }
    return(
      <View
        style={{
          flex:1,
          paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
          paddingVertical:Scaling.scale(40 * 0.444),
        }}
      >
        <View
          style={{
            flex:0.085,
            borderColor:CONSTANT.COLOR.LIGHT_GRAY,
            borderBottomWidth:Scaling.moderateScale(2),
            alignItems:'center',
            justifyContent:'center',
          }}
        >
          <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>{this.props.title}</Text>


          <Button.Cancel/>
        </View>
      
        <View
          style={{
            flex:0.25,
            alignItems:'center',
            justifyContent:'center',
          }}
        >
          <Image
              source={source}
              style={[{
                width:Scaling.scale(120),
                height:Scaling.verticalScale(120),
                resizeMode:'contain',
              },
              imageExist == false ? {tintColor:CONSTANT.COLOR.BLUE}: {},
              ]}
          />
        </View>

        <View
          style={{
            flex:0.05,
            alignItems:'center',
            justifyContent:'center',
          }}
        >
          <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>{this.props.itemTitle}</Text>
        </View>

        <View
          style={{
            flex:0.115,
            alignItems:'center',
            justifyContent:'center',
          }}
        >
          <Text textStyle={{textAlign:'center'}} fontOption={CONSTANT.TEXT_OPTION.CONTENT}>{this.props.itemDescription}</Text>
        </View>

        <View
          style={{
            flex:0.6,
          }}
        >
            <LargeList
              data={this.state.extractedItem}
              heightForIndexPath={({section, row}) => {
                  return Scaling.verticalScale(180* 0.444);
              }}
              renderIndexPath={this._renderItem.bind(this)}
            /> 
        </View>
        
      </View>
    );
  }

  _renderItem({section, row}){
    let item = this.state.extractedItem[section].items[row];
    let itemInfo = item[this.props.rowInfoCategory];
    
    let description = this.state.extractedItem[section].items[row][this.props.rowDescriptionCategory].replace('\\n', '\n');

    return (
        <TouchableOpacity
          style={{
            alignSelf:'stretch',
            height:Scaling.verticalScale(125 * 0.444),
            justifyContent:'center',
            borderBottomWidth:Scaling.verticalScale(1),
            borderColor:CONSTANT.COLOR.LIGHT_GRAY
          }}

          onPress={() =>{
              this.props.actionNavigator.dismissLightBox();
              
              if (this.props.isShowConfirmation) {
                setTimeout(() => {
                  this.props.actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeA',{
                      title:'Konfirmasi', 
                      message: 'apakah anda yakin memilih ' + itemInfo, 
                      buttonPrimaryText: 'ya', 
                      buttonSecondaryText: 'batal', 
                      onPressButtonPrimary: () => {
                        this.props.actionNavigator.dismissLightBox();
                        this.props.onPressButtonPrimary(item);
                      }, 
                      onPressButtonSecondary: () =>{
                        this.props.actionNavigator.dismissLightBox();

                        setTimeout(() => {
                          this.props.onPressButtonSecondary();
                        },500);
                      },
                  });
                }, 500);
              }
              else{
                this.props.onPressButtonPrimary(item);
              }
          }}
        >
          <View
            style={{
              flex:1,
              flexDirection:'row',
            }}
          >
            <View 
              style={{
                flex:0.15,
                alignItems:'center',
                justifyContent:'center',
              }}
            >
                <View
                    style={{
                      width:Scaling.scale(60 * 0.444),
                      height:Scaling.scale(60 * 0.444),
                      borderRadius:Scaling.moderateScale(50),
                      borderColor:CONSTANT.COLOR.BLUE,
                      borderWidth:Scaling.moderateScale(1),
                    }}
                />
            </View>
            <View
              style={{
                flex:0.85,
                padding:Scaling.moderateScale(5),
              }}
            >
              <View
                style={{
                  flex:0.4,
                  flexDirection:'row',
                }}
              >
                <View
                  style={{
                    flex:0.7,
                    justifyContent:'center',
                  }}
                >
                  <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>
                    {this.state.extractedItem[section].items[row][this.props.rowInfoCategory]}
                  </Text>
                </View>
                <View
                  style={{
                    flex:0.3,
                    justifyContent:'center',
                    alignItems:'flex-end'
                  }}
                >
                  <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>
                    {toCurrency(this.state.extractedItem[section].items[row][this.props.rowPriceCategory], 'Rp')}
                  </Text>
                </View>
                
              </View>

              <View
                style={{
                  flex:0.6,
                }}
              >
                <Text numberOfLines={3} fontOption={CONSTANT.TEXT_OPTION.SUB_CONTENT} textStyle={{color:CONSTANT.COLOR.DARK_GRAY}}>
                  {description}
                </Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
    );
  }

  _onPressSubmit(key, info){
    if (key == null) {
      alert('Pilih data yang diperlukan');
    }
    else{
      this.props.data.onPressButtonPrimary(key, info);
    }
  }

  render() {
    if (this.state.isFinishLoad) {
      return (
        <TouchableHighlight
          hitSlop={{
            bottom:Dimensions.get('window').height * 0.5,
          }}

          underlayColor={'transparent'}
          style={[styles.container, this.props.containerStyle]}
          onPress={() =>{
              this.props.actionNavigator.dismissLightBox();
              this.props.actionNavigator.dismissModal();
          }}
        >
          <TouchableHighlight underlayColor={'transparent'} style={styles.contentContainer}>
              {this.renderContent()}
          </TouchableHighlight>
        </TouchableHighlight>
      );
    }
    return null;
  }
}

class LoadingContent extends Component{
  render(){
    return(         
       <ActivityIndicator size={moderateScale(24)} color={CONSTANT.COLOR.GRAY_B} /> 
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width:'100%',
    height:'100%',
    alignItems:'center',
    justifyContent:'flex-end',
    paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
  },
  contentContainer:{
    width:'100%',
    height:'85%',
    backgroundColor:'white',
    borderRadius:Scaling.moderateScale(7),
    zIndex:10, 
  },
  containerTitle:{
    flex:0.225,
    alignItems:'center',
    justifyContent:'center',
    alignSelf:'stretch',
    backgroundColor: CONSTANT.COLOR.BLUE_A,
  },
  containerButton:{
    flex:0.3,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-around',
  },
  containerMessage:{
    flex:0.7,
    justifyContent:'center',
  },
  textTitle: {
    color:'white',
  },
  textMessage: {
    textAlign:'center',
  },
  textButton: {
    color:'white'
  },
  buttonPrimary:{
    width:Dimensions.get('window').width * 0.45,
    height:Dimensions.get('window').height * 0.05,
    backgroundColor:'#61CDFF',
    borderRadius:100,
    alignItems:'center',
    justifyContent:'center',
  },
});



function mapStateToProps(state, ownProps) {
  return {
    delayOrderReasonList:state.rootReducer.masterData.delayOrderReasonList,
    rejectOrderReasonList:state.rootReducer.masterData.rejectOrderReasonList,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionMasterData: bindActionCreators(actions.masterData, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemSelectionDetailed);
