import React, { Component } from 'react';
import {
  NativeModules,
  Animated, 
  StyleSheet, 
  View, 
  Dimensions, 
  TouchableOpacity, 
  ActivityIndicator, 
  TouchableHighlight,
  Image
} from 'react-native';
import { LargeList } from "react-native-largelist-v3";
import {moderateScale} from 'react-native-size-matters';

import {Text, Scaling, Form, Button, Label} from 'app/data/components';
import * as CONSTANT from 'app/data/constant';


//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

const { UIManager } = NativeModules;
UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

class SearchCustomer extends Component {
  static defaultProps = {
    data: [],
    name:null,
    email:null,
    mobilePhone:null,
    extractedMobilePhone:null,
    isDismissAllowed:true,
    notification:null,
    customerData:null
  }

  constructor(props) {
    super(props);
  
    this.state = {
      height:new Animated.Value(Dimensions.get('window').height * 0.375),
      name:null,
      email:null,
      mobilePhone:null,
      extractedMobilePhone:null,
      notification:null,
      searchResult:null,
      customerData:null,
    };
   
  }

  componentWillMount() {
    this.config();
  }

  config(){
    let {name, email, mobilePhone, notification, extractedMobilePhone, searchResult, customerData} = this.props;

    this.setState({name, email, mobilePhone, extractedMobilePhone, customerData}, () => {
      this.setup(searchResult, notification);
    });
  }

  
  componentDidMount(){
    setTimeout(() => {
        this.setState({isFinishLoad:true}, () => {
        });
    },50);
  }

  setup(searchResult, notifications = null){
      let notification;

      if (searchResult == 'not-found') {
        notification = 'Pelanggan belum terdaftar, daftarkan terlebih dahulu.';
      }
      else if (searchResult == 'error-timeout') {
        notification = 'Koneksi anda bermasalah, coba lagi';
      }
      else if (searchResult == 'error-api') {
        notification = 'Server kami bermasalah, coba lagi';
      }
      else if (searchResult == 'found') {
        notification = 'Pelanggan telah terdaftar, pastikan data sesuai';
      }


      if(searchResult == 'found' || searchResult == 'not-found'){
        this.setState({searchResult, notification}, () => {
            Animated.spring(this.state.height, { 
              toValue: Dimensions.get('window').height * 0.525,
              tension:5,
            }).start();
        });
      }
      else{
        this.setState({notification});
      }
  }

  async _onSubmitEditingPhone(){
      let {actionCustomer, actionValidation} = this.props;
      let {mobilePhone, extractedMobilePhone} = this.state;
      if (await actionValidation.mobilePhone(mobilePhone) == true) {
        this.phone_number_text_field.blur();
        
        let searchResult = await actionCustomer.searchCustomerByPhone(mobilePhone);

        if (searchResult.result == 'found') {
          searchResult.customerData.extractedMobilePhone = extractedMobilePhone;
          let data = searchResult.customerData;
          this.setState({email:data.email, name:data.name, customerData:data}); 
        }
      
        this.setup(searchResult.result);
      }
      else{
          this.setState({notification:'Format nomer telepon salah, 08xxxxxxxxx, karakter 11 - 13'});
      }
  }

  _onSubmitEditingName(){
    this.name_text_field.blur();

    this.email_text_field.focus();
  }

  _onSubmitReset(){
    this.setState({
      name:null,
      email:null,
      notification:null,
      searchResult:null,
    }, () => {
        Animated.spring(this.state.height, { 
            toValue: Dimensions.get('window').height * 0.375,
            tension:5,
        }).start();
    });
  }

  _onSubmitDone(){
      let {actionPOS, actionNavigator} = this.props;
      let {customerData} = this.state;
      console.log(customerData);
      actionPOS.setSelectedCustomer(customerData);
      actionNavigator.dismissLightBox();
      actionNavigator.pop(this.props.navigator);
  }

  async _onSubmitEditingEmail(){
    let {email, name, mobilePhone, extractedMobilePhone, customerData} = this.state;
    let {actionValidation, actionCustomer, actionNavigator, actionPOS} = this.props;

    if (email != null && await actionValidation.email(email) == false) {
      this.setState({notification:'format email tidak sesuai, contoh : xxx@gmail.com'});
    }
    else if(await actionValidation.minChar(name, 4) == false){
      this.setState({notification:'nama lengkap minimal 4 karakter'});
    }
    else if(await actionValidation.letter(name) == false){
      this.setState({notification:'nama lengkap hanya boleh huruf'});
    }
    else if(email != null && email != '' && await actionCustomer.searchCustomerByEmail(email) != null){
      this.setState({notification:'email tidak tersedia, gunakan email yang lain'});
    }
    else{
        let obj = {};
        obj.email = email;
        obj.mobile_phone = mobilePhone;
        obj.name = name;
        obj.extractedMobilePhone = extractedMobilePhone;
        actionPOS.setSelectedCustomer(obj);
        console.log(obj, email);
        actionNavigator.dismissLightBox();
        actionNavigator.pop(this.props.navigator);
    }
  }

  _onChangeMobilePhoneText(newMobilePhone){
    let extractedMobilePhone = '';
    let {mobilePhone} = this.state;
    if (mobilePhone) {
      newMobilePhone = newMobilePhone.replace(/-/g, "");
      if (mobilePhone.length < newMobilePhone.length) {
        mobilePhone += newMobilePhone.charAt(newMobilePhone.length - 1);
      }
      else{
        mobilePhone = mobilePhone.substring(0, mobilePhone.length - 1);
      }
      
      let counter = 0;
      for(let i = 0; i < mobilePhone.length;i++){
        if (counter == 4) {
          extractedMobilePhone += '-';
          counter = 0;
        }

        extractedMobilePhone += mobilePhone.charAt(i);
        counter++;
      } 
    }
    else{
      mobilePhone = newMobilePhone;
      extractedMobilePhone = newMobilePhone;
    }

    this.setState({mobilePhone, extractedMobilePhone});
  }

  _onChangeEmailText(email){
    if (email == '') {
      email = null;
    }

    this.setState({email});
  }

  _onChangeNameText(name){
    this.setState({name});
  }

  _renderNotification(){
    if (this.state.notification && this.state.notification.trim() != '') {
      return(
        <View
          style={{
            height:'90%',
            borderRadius:Scaling.moderateScale(10),
            backgroundColor:CONSTANT.COLOR.LIGHT_GRAY,
            justifyContent:'center',
            paddingHorizontal:Scaling.scale(5),
            paddingVertical:Scaling.verticalScale(5),
          }}
        >
          <Text fontOption={CONSTANT.TEXT_OPTION.CONTENT} textStyle={CONSTANT.COLOR.GRAY}>{this.state.notification}</Text>
        </View>
      );
    }
  }

  _renderButton(){
    let {searchResult} = this.state;

    if (!searchResult) {
      return(
        <Button.Standard
          buttonSize={CONSTANT.STYLE.BUTTON.EXTRA_MEDIUM}
          buttonText={'Cari'}
          buttonColor={CONSTANT.COLOR.BLUE}
          onPress={this._onSubmitEditingPhone.bind(this)}
        />
      );
    }
    else if (searchResult == 'not-found') {
      return(
        <View
          style={{
            width:'100%',
            flexDirection:'row',
            justifyContent:'space-around',
          }}
        >
          <Button.Standard
            buttonSize={CONSTANT.STYLE.BUTTON.EXTRA_MEDIUM}
            buttonColor={CONSTANT.COLOR.GRAY}
            buttonText={'Ulang'}
            onPress={this._onSubmitReset.bind(this)}
          />
          <Button.Standard
            buttonSize={CONSTANT.STYLE.BUTTON.EXTRA_MEDIUM}
            buttonColor={CONSTANT.COLOR.ORANGE}
            buttonText={'Daftarkan'}
            onPress={this._onSubmitEditingEmail.bind(this)}
          />
        </View>
      );
    }
    else if(searchResult == 'found'){
      return(
        <View
          style={{
            width:'100%',
            flexDirection:'row',
            justifyContent:'space-around',
          }}
        >
          <Button.Standard
            buttonSize={CONSTANT.STYLE.BUTTON.EXTRA_MEDIUM}
            buttonColor={CONSTANT.COLOR.GRAY}
            buttonText={'Ulang'}
            onPress={this._onSubmitReset.bind(this)}
          />
          <Button.Standard
            buttonSize={CONSTANT.STYLE.BUTTON.EXTRA_MEDIUM}
            buttonColor={CONSTANT.COLOR.GREEN}
            buttonText={'Konfirmasi'}
            onPress={this._onSubmitDone.bind(this)}
          />
        </View>
      );
    }
  }

  _renderResult(){
      let {customerSearchData, customerTempData} = this.props;
      let {searchResult} = this.state;
      if (searchResult == 'not-found' || searchResult == 'found') {
        return(
          <View>
            <Form.InputField.SemiRound
                keyboardType={'default'}
                returnKeyType={'next'}
                ref={(ref) => this.name_text_field = ref}
                editable={this.state.searchResult && (this.state.searchResult == 'found' || this.state.searchResult == ' not-found') ? false : true}
                leftIcon={() => {
                    return(
                        <Image
                            source={CONSTANT.IMAGE_URL.CUSTOMER_ICON}
                            style={{
                              tintColor: CONSTANT.COLOR.BLUE,
                              width:Scaling.scale(60 * 0.444),
                              height:Scaling.verticalScale(60 * 0.444),
                              resizeMode:'contain',
                            }}
                        /> 
                    );
                }}
                containerStyle={{
                  marginTop:Scaling.verticalScale(15),
                }}
                placeholder={'nama lengkap'}
                onChangeText={this._onChangeNameText.bind(this)}
                onSubmitEditing={this._onSubmitEditingName.bind(this)}
                value={this.state.name}
            />

            <Form.InputField.SemiRound
                keyboardType={'email-address'}
                returnKeyType={'go'}
                ref={(ref) => this.email_text_field = ref}
                editable={this.state.searchResult && (this.state.searchResult == 'found' || this.state.searchResult == ' not-found') ? false : true}
                leftIcon={() => {
                    return(
                        <Image
                            source={CONSTANT.IMAGE_URL.EMAIL_ICON_2}
                            style={{
                              tintColor: CONSTANT.COLOR.BLUE,
                              width:Scaling.scale(60 * 0.444),
                              height:Scaling.verticalScale(60 * 0.444),
                              resizeMode:'contain',
                            }}
                        /> 
                    );
                }}
                containerStyle={{
                  marginTop:Scaling.verticalScale(15),
                }}
                placeholder={'alamat email'}
                onChangeText={this._onChangeEmailText.bind(this)}
                onSubmitEditing={this._onSubmitEditingEmail.bind(this)}
                value={this.state.email}
            />
          </View>
        );
      }
  }

  getTitle(){
    let {searchResult} = this.state;
    if (!searchResult) {
        return 'Cari Pelanggan';
    }
    else if(searchResult == 'found'){
        return 'Konfirmasi Pelanggan';
    }
    else if (searchResult == 'not-found') {
        return 'Daftarkan Pelanggan';
    }
  }

  renderContent(){
    return(
      <Animated.View
        style={[{
          height:this.state.height,
        }]}
      >
        <View
          style={{
            flex:1,
            marginHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
            marginVertical:Scaling.verticalScale(40 * 0.444),
          }}
        >
          <View
              style={{
                height:Scaling.verticalScale(100 * 0.444),
                borderColor:CONSTANT.COLOR.LIGHT_GRAY,
                borderBottomWidth:Scaling.moderateScale(2),
                alignItems:'center',
                justifyContent:'center',
              }}
            >
              <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>{this.getTitle()}</Text>


              <Button.Cancel/>
            </View>
            
            <View
              style={{
                height:Scaling.verticalScale(110 * 0.444),
                justifyContent:'center',
              }}
            >
              {this._renderNotification()}
            </View>
            
            <View
              style={{
                flex:1,
              }}
            >
              <Form.InputField.SemiRound
                  keyboardType={'numeric'}
                  maxLength={16}
                  returnKeyType={'go'}
                  ref={(ref) => this.phone_number_text_field = ref}
                  editable={this.state.searchResult && (this.state.searchResult == 'not-found' || this.state.searchResult == 'found')? false : true}
                  leftIcon={() => {
                      return(
                          <Image
                              source={CONSTANT.IMAGE_URL.PHONE_ICON_2}
                              style={{
                                tintColor: CONSTANT.COLOR.BLUE,
                                width:Scaling.scale(60 * 0.444),
                                height:Scaling.verticalScale(60 * 0.444),
                                resizeMode:'contain',
                              }}
                          /> 
                      );
                  }}
                  containerStyle={{
                    marginTop:Scaling.verticalScale(10),
                  }}
                  placeholder={'No Handphone, 08xxxxx'}
                  onChangeText={this._onChangeMobilePhoneText.bind(this)}
                  onSubmitEditing={this._onSubmitEditingPhone.bind(this)}
                  value={this.state.extractedMobilePhone}
              />

              {this._renderResult()}
            </View>

            <View
              style={{
                height:Scaling.verticalScale(100 * 0.444),
                justifyContent:'center',
                alignItems:'center',
              }}
            >
              {this._renderButton()}
            </View>
        </View>
      </Animated.View>
    );
  }

  _onPressSubmit(key, info){
    if (key == null) {
      alert('Pilih data yang diperlukan');
    }
    else{
      this.props.data.onPressButtonPrimary(key, info);
    }
  }

  render() {
    if (this.state.isFinishLoad) {
      return (
        <TouchableHighlight
          hitSlop={{
            bottom:Dimensions.get('window').height * 0.5,
          }}
          disabled={this.props.isDismissAllowed ? false : true}
          underlayColor={'transparent'}
          style={[styles.container, this.props.containerStyle]}
          onPress={() =>{
              this.props.actionNavigator.dismissLightBox();
              this.props.actionNavigator.dismissModal();
          }}
        >
            <View underlayColor={'transparent'} style={styles.contentContainer}>
              {this.renderContent()}
            </View>
        </TouchableHighlight>
      );
    }
    return null;
  }
}

class LoadingContent extends Component{
  render(){
    return(         
       <ActivityIndicator size={moderateScale(24)} color={CONSTANT.COLOR.GRAY_B} /> 
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width:'100%',
    height:'100%',
    alignItems:'center',
  },
  contentContainer:{
    width:'90%',
    position:'absolute',
    top:'25%',
    borderRadius:Scaling.moderateScale(12),
    backgroundColor:'white',
  },
  containerTitle:{
    flex:0.225,
    alignItems:'center',
    justifyContent:'center',
    alignSelf:'stretch',
    backgroundColor: CONSTANT.COLOR.BLUE_A,
  },
  containerButton:{
    flex:0.3,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-around',
  },
  containerMessage:{
    flex:0.7,
    justifyContent:'center',
  },
  textTitle: {
    color:'white',
  },
  textMessage: {
    textAlign:'center',
  },
  textButton: {
    color:'white'
  },
  buttonPrimary:{
    width:Dimensions.get('window').width * 0.45,
    height:Dimensions.get('window').height * 0.05,
    backgroundColor:'#61CDFF',
    borderRadius:100,
    alignItems:'center',
    justifyContent:'center',
  },
});



function mapStateToProps(state, ownProps) {
  return {
    customerSearchData:state.rootReducer.customer.search,
    customerTempData:state.rootReducer.customer.temp,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionMasterData: bindActionCreators(actions.masterData, dispatch),
    actionCustomer: bindActionCreators(actions.customer, dispatch),
    actionValidation: bindActionCreators(actions.validation, dispatch),
    actionPOS: bindActionCreators(actions.pos, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchCustomer);
