import React, { Component } from "react";
import { StyleSheet, View, Dimensions, TouchableOpacity, ScrollView, Image } from "react-native";
import debounce from "lodash.debounce";

import * as CONSTANT from "./../../../constant";
import { Text, Scaling, Form, Button } from "app/data/components";

//redux
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from "./../../../actions";

class MembershipTnC extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {}

    componentWillMount() {}

    renderContent() {
        let { termsAndConditions } = this.props;
        return (
            <View
                style={{
                    paddingVertical: Scaling.scale(25),
                    paddingHorizontal: Scaling.scale(17)
                }}
            >
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    style={{
                        marginBottom: Scaling.scale(20)
                    }}
                >
                    <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>syarat & ketentuan</Text>
                    <View style={{ marginTop: Scaling.scale(20) }}>{this.renderTermsndConditions(termsAndConditions)}</View>
                </ScrollView>
            </View>
        );
    }

    renderTermsndConditions(termsAndConditions) {
        let list_of_term_n_conditions = termsAndConditions.split("\n");

        return list_of_term_n_conditions.map((tnc, index) => (
            <View key={index} style={{ flexDirection: "row", marginBottom: 5 }}>
                <View style={{ flex: 1 }}>
                    <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>{tnc}</Text>
                </View>
            </View>
        ));
    }

    render() {
        return (
            <TouchableOpacity
                hitSlop={{
                    bottom: Dimensions.get("window").height * 0.5
                }}
                disabled
                underlayColor={"transparent"}
                style={[styles.container, this.props.containerStyle]}
                onPress={() => {
                    this.props.actionNavigator.dismissLightBox();
                    this.props.actionNavigator.dismissModal();
                }}
            >
                <View underlayColor={"transparent"} style={styles.contentContainer}>
                    {this.renderContent()}
                </View>
            </TouchableOpacity>
        );
    }
}

function mapStateToProps(state) {
    return {
        tagCategories: state.rootReducer.pos.tagCategories
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actionNavigator: bindActionCreators(actions.navigator, dispatch),
        actionPos: bindActionCreators(actions.pos, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MembershipTnC);

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "100%",
        alignItems: "center",
        justifyContent: "flex-end"
    },
    contentContainer: {
        width: "100%",
        height: "80%",
        backgroundColor: "white"
    }
});