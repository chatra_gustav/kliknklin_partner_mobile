import React, { Component,PureComponent } from 'react';
import {View, Keyboard, StyleSheet, Dimensions, ScrollView, Platform, TouchableOpacity, backHandler, Image} from 'react-native';

//node js class import
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import { LargeList } from "react-native-largelist-v3";

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling, Form, NoDataFound} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

//local
import ButtonAddCustomer from './button_add_customer';

class SelectCustomer extends Component {
	constructor(props) {
        super(props);
        this.state = {
            isKeyboardShown:false,
            customerList:[],
            customerLargeList:[],
            outletCustomer:this.props.outletCustomer,
        };
    }

    componentDidMount(){
        this.extractData(this.state.outletCustomer);
    }

    componentWillMount(){
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this,true));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidShow.bind(this, false));
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow (isShown) {
        this.setState({isKeyboardShown:isShown});
    }

    extractData(list){
        let extractedItem = [];

        let tempList = [];

        let currentAlphabet = '';
        
        for (let section = 0; section < list.length; ++section){
            let listNameFirstCharacter = list[section].user.name.charAt(0).toLowerCase();
            


            if (tempList.length == 0) {
                let data = {};
                data.firstAlphabet = listNameFirstCharacter;
                data.items = [];
                data.items.push(list[section]);
                tempList.push(data);
            }
            else{
                let hasSameAlphabet = false;

                for(let i in tempList){
                    if (tempList[i].firstAlphabet == listNameFirstCharacter) {
                        hasSameAlphabet = true;
                        tempList[i].items.push(list[section]);
                    }
                }

                if (!hasSameAlphabet) {
                    let data = {};
                    data.firstAlphabet = listNameFirstCharacter;
                    data.items = [];
                    data.items.push(list[section]);
                    tempList.push(data);   
                }
            }
        }

   
        for (let section = 0; section < tempList.length; ++section){
            let data = {};
            data.items = tempList[section].items
            extractedItem.push(data);
        }  
        
        this.setState({customerList:tempList, customerLargeList:extractedItem});
      }

	
	render(){
        try{
            let content = <NoDataFound
                    title={'Data pelanggan tidak dapat ditemukan'}
                    message={'Untuk membuat pelanggan, pilih tombol \n tambah pelanggan dikanan bawah layar anda'}
             />

            if (this.state.customerList.length != 0) {
                content = (
                    <LargeList
                        heightForIndexPath={({section, row}) => {
                            if (row == 0) {
                                return Scaling.verticalScale(115);
                            }
                            return Scaling.verticalScale(75);
                        }}

                        renderSection={(section) => 
                            <View
                                style={{
                                    height:Scaling.verticalScale(40),
                                    backgroundColor:CONSTANT.COLOR.WHITE,
                                    justifyContent:'center',
                                }}
                            >
                                <Text
                                    fontOption={CONSTANT.TEXT_OPTION.HEADER}
                                >
                                    {this.state.customerList[section].firstAlphabet.toUpperCase()}
                                </Text>
                            </View>
                        }

                        renderFooter={()=><View style={{height:Scaling.verticalScale(100)}}/>}

                        data={this.state.customerLargeList}

                        renderIndexPath={({section, row}) => {
                          return <Items
                            {...this.props}
                            {...this.state}
                            section={section}
                            row={row}
                          />;
                        }}
                    />
                );
            }

            return(
                    <LayoutPrimary
                        showTitle={true}
                        showTabBar={false}
                        showBackButton={true}
                        navigator={this.props.navigator}
                        title={'Pilih Pelanggan'}
                        contentContainerStyle={{
                            flex:1,
                            paddingHorizontal:Scaling.scale(20),
                            backgroundColor:CONSTANT.COLOR.WHITE
                        }}
                    >   
                        <Form.InputField.SemiRound
                            placeholder={'Cari nama / no handphone pelanggan'}
                            containerStyle={{
                                backgroundColor:CONSTANT.COLOR.GRAY_STATUS_BACKGROUND,
                                marginTop:Scaling.verticalScale(10),
                            }}
                            onChangeText={(itemToFind) => {
                                let sourceItem = this.props.outletCustomer;
                                let data = sourceItem.filter(function (str) { 

                                  return (str.user.name.toLowerCase().includes(itemToFind.toLowerCase()) ||
                                        str.user.mobile_phone.includes(itemToFind)
                                    ); 
                                });
                                this.extractData(data);
                            }}
                        />
                        
                        <View
                            style={{
                                flex:1,
                            }}
                        >
                            {content}
                        </View>

                        <ButtonAddCustomer 
                            {...this.props}
                        />
                    </LayoutPrimary>
            );
        }
        catch(error){
            console.log(error, 'pos.payment.index');
            return null;
        }
		
	}
}

class Items extends PureComponent{
    constructor(props) {
      super(props);
    
      this.state = {
        data:'',
      };
    }

    componentDidMount(){

    }

    onPress(data, shownData){
        this.props.actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeA',{
            title:shownData, 
            message: 'Apakah anda yakin untuk memilih pelanggan ini?', 
            buttonPrimaryText: 'ya', 
            buttonSecondaryText: 'batal',
            onPressButtonPrimary: () => {
                console.log(data);
                this.props.actionNavigator.dismissLightBox();
                this.props.actionPOS.setSelectedCustomer(data);
                this.props.actionNavigator.pop(this.props.navigator);
            }, 
            onPressButtonSecondary: () =>{
                this.props.actionNavigator.dismissLightBox();
            },
        });
    }

    toCapitalize(str){
        let result = str;
        try{
            result = str
            .replace(/\s+/g, " ")
            .trim()
            .toLowerCase()
            .split(' ')
            .map(function(word) {
                return word[0].toUpperCase() + word.substr(1);
            })
            .join(' ');
        }
        catch(error){

        }
        
        return result
    }

    render(){
        let data = this.props.customerList[this.props.section].items[this.props.row];
        let user = data.user;
        let name = user.name;
        name = this.toCapitalize(name);
        let phoneNumber = user.mobile_phone;
        let email = user.email;
        data.name = name;
        data.mobile_phone = phoneNumber;
        data.email = email;
        let shownData = name + ' - ' + phoneNumber;

        let listItem = 
        (
            <TouchableOpacity
                style={{
                    height:Scaling.verticalScale(75),
                    borderBottomWidth:Scaling.moderateScale(2),
                    borderColor:CONSTANT.COLOR.LIGHT_GRAY,
                    justifyContent:'center',
                    backgroundColor:CONSTANT.COLOR.WHITE
                }}

                onPress={this.onPress.bind(this, data, shownData)}
            >
                <Text
                    fontOption={CONSTANT.TEXT_OPTION.HEADER}
                >
                    {shownData}
                </Text>
            </TouchableOpacity>
        );

        let component = (
            listItem
        );

        if (this.props.row == 0) {
            component = (
            <View
                style={{
                    height:Scaling.verticalScale(115),
                    justifyContent:'flex-end',
                    backgroundColor:CONSTANT.COLOR.WHITE
                }}
            >
                {listItem}
            </View>
            );
        }

        return(
            component
        );
    }
}

function mapStateToProps(state, ownProps) {
	return {
        outletCustomer:state.rootReducer.customer.outlet,
	};
}

function mapDispatchToProps(dispatch) {
	return {
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
        actionPOS: bindActionCreators(actions.pos, dispatch),
	};
}

const styles = StyleSheet.create({
});

export default connect(mapStateToProps, mapDispatchToProps)(SelectCustomer);

