import React, { Component } from 'react';
import { Text,View,PixelRatio, Platform, Dimensions} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

class FontScale {
    _getFontSize(size){
      return moderateScale(size, 1);
      
      // if (PixelRatio.get() <= 1) {
      //   if (Dimensions.get('screen').height > 800 && Dimensions.get('screen').height <= 1024) {
      //     return size + 7;
      //   }
      //   else if (Dimensions.get('screen').height > 1024) {
      //     return size + 18;
      //   }

      //   return size + 1;
      // }
      // else if (PixelRatio.get() <= 1.5) {
      //   return size + 3;
      // }
      // else if (PixelRatio.get() <= 2) {
      //   if (Platform.OS == 'ios') {
      //     return size + 3.5;
      //   }
      //   return size + 5.5;
      // }
      // else if (PixelRatio.get() < 3) {
      //   return size + 6;
      // }
      // else if (PixelRatio.get() == 3) {
      //   return size + 5;
      // }
      // else if (PixelRatio.get() < 3.5) {
      //   return size + 6;
      // }
      // else if (PixelRatio.get() == 3.5) {
      //   return size + 6.5;
      // }
      // else{
      //   return size + 5.5;
      // }
    }
}

module.exports = {
  FontScale : new FontScale(),
}