import React, { Component } from 'react';
import { Animated, Platform, ScrollView, View, Dimensions } from 'react-native';

import{verticalScale, moderateScale, scale} from 'react-native-size-matters';


const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);

export default class Collapsible extends Component {
  scroll = new Animated.Value(0);
  offset = new Animated.Value(0);

  min = this.props.min ? this.props.min : moderateScale(25);
  max = this.props.max ? this.props.max : Dimensions.get('window').height * 0.45;

  position = this.scroll.interpolate({
    inputRange: [0, this.max],
    outputRange: [0, this.min - this.max],
    extrapolate: 'clamp'
  });

  opacity = this.scroll.interpolate({
    inputRange: [0, this.max],
    outputRange: [1, 0]
  });

  height = this.scroll.interpolate({
    inputRange: [-this.max, 0, this.max],
    outputRange: [this.max * 2, this.max, 0]
  });

  render() {

    const { backgroundColor, max, ...props } = this.props;

    let maxHeight = max != null || max != undefined ? max : this.max;

    return (
      <View style={{ flex: 1, width:'100%'}}>
        <AnimatedScrollView
          {...props}
          contentContainerStyle={[{width:'100%',paddingTop:maxHeight + moderateScale(10), paddingBottom:Dimensions.get('window').height * 0.1}, this.props.contentContainerStyle]}
          onScroll={Animated.event([{ 
              nativeEvent: { contentOffset: { y: this.scroll } } }
            ])}
          scrollEventThrottle={16}>
          {this.props.renderContent}
        </AnimatedScrollView>
        <Animated.View
          style={{
            backgroundColor,
            height: maxHeight,
            left: 0,
            position: 'absolute',
            right: 0,
            top: 0,
            transform: [{ translateY: this.position }]
          }}>
          <Animated.View style={{ opacity: this.opacity }}>
            {this.props.renderHeader}
          </Animated.View>
        </Animated.View>
      </View>
    );
  }
}