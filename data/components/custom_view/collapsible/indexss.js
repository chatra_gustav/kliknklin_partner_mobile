import React, { Component } from 'react';
import { Animated, Platform, ScrollView, View, Dimensions } from 'react-native';

import{verticalScale, moderateScale, scale} from 'react-native-size-matters';

import ExtraDimensions from 'react-native-extra-dimensions-android';

const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);

export default class Collapsible extends Component {
  scroll = new Animated.Value(0);
  offset = new Animated.Value(0);

  min = this.props.min === false ? 0 : Platform.select({ ios: 20, android: 0 });
  max = this.props.max ? this.props.max : 0;

  position = Animated.add(this.scroll, this.offset).interpolate({
    inputRange: [0, this.max],
    outputRange: [0, this.min - this.max],
    extrapolate: 'clamp'
  });

  opacity = this.scroll.interpolate({
    inputRange: [0, this.max],
    outputRange: [1, 0]
  });

  height = this.scroll.interpolate({
    inputRange: [-this.max, 0, this.max],
    outputRange: [this.max * 2, this.max, this.max]
  });

  render() {
    const { backgroundColor, ...props } = this.props;
    
    return (
      <View style={{ flex: 1, width:'100%'}}>
        <AnimatedScrollView
          {...props}
          contentContainerStyle={[{ flex:1, width:'100%',paddingTop:this.max + Dimensions.get('window').height * 0.005}, this.props.contentContainerStyle]}
          onScroll={Animated.event([{ 
              nativeEvent: { contentOffset: { y: this.scroll } } }
            ])}
          scrollEventThrottle={16}>
          {this.props.renderContent}
        </AnimatedScrollView>
        <Animated.View
          style={{
            backgroundColor,
            height: this.max,
            left: 0,
            position: 'absolute',
            right: 0,
            top: 0,
            transform: [{ translateY: this.position }]
          }}>
          <Animated.View style={{ opacity: this.opacity }}>
            {this.props.renderHeader}
          </Animated.View>
        </Animated.View>
      </View>
    );
  }
}