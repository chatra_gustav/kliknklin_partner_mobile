import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        WebView,
        } from 'react-native';

import {LayoutPrimary} from './../../../layout';
import {TextWrapper, Button, Container} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

export default class Webview extends Component {
    constructor(props){
        super(props);
        this.state = {
        };
    }

    render(){
        return(
            <Container.TypeA
                containerStyle={{
                    height:Dimensions.get('window').height * 0.8,
                }}
                contentStyle={{
                    flex:1,
                    padding:0,
                }}
            >
                <WebView
                    startInLoadingState={true}
                    renderLoading={() => {
                            return (
                                <LoadingContent/>
                            )
                        }
                    }
                    source={{uri: this.props.uri}}
                    style={{flex:1}}
                />
            </Container.TypeA>
        );
    }
}

class LoadingContent extends Component{

    render(){
        return (
            <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                <TextWrapper type={'smallercontent'}>{'Memuat Konten...'}</TextWrapper>
            </View>
        )
    }
}


