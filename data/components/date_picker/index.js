import React, { Component } from 'react'
import { StyleSheet, View, Dimensions } from 'react-native'
import { Calendar, defaultStyle } from 'react-native-calendars'

import * as CONSTANT from './../../constant';
import {Button, Scaling} from './../../components';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../actions";

const XDate = require('xdate');

type Props = {
  onSuccess: React.PropTypes.func.isRequired,
};

class DatePickerComponent extends Component<Props> {

  state = {isFromDatePicked: false, isToDatePicked: false, markedDates: {}}

  componentDidMount() { 
    // this.setupInitialRange() 
  }

  onDayPress = (day) => {
    this.props.onDateSelected(day.dateString);
  }

  setupStartMarker = (day) => {
    let markedDates = {[day.dateString]: {startingDay: true, color: this.props.theme.markColor, textColor: this.props.theme.markTextColor}}
    this.setState({isFromDatePicked: true, fromDate: day.dateString, markedDates: markedDates})
  }

  setupMarkedDates = (fromDate, toDate, markedDates) => {
    let mFromDate = new XDate(fromDate)
    let mToDate = new XDate(toDate)
    let range = mFromDate.diffDays(mToDate)
    if (range >= 0) {
      if (range == 0) {
        markedDates = {[toDate]: {color: this.props.theme.markColor, textColor: this.props.theme.markTextColor}}
      } else {
        for (var i = 1; i <= range; i++) {
          let tempDate = mFromDate.addDays(1).toString('yyyy-MM-dd')
          if (i < range) {
            markedDates[tempDate] = {color: this.props.theme.markColor, textColor: this.props.theme.markTextColor}
          } else {
            markedDates[tempDate] = {endingDay: true, color: this.props.theme.markColor, textColor: this.props.theme.markTextColor}
          }
        }
      }
    }
    return [markedDates, range]
  }

  render() {
    let {width, height} = Dimensions.get('window');
    return (
      <View style={{ width: Scaling.moderateScale(width * 0.95), height: Scaling.moderateScale(height * 0.68), padding: Scaling.moderateScale(10), borderRadius: 10, backgroundColor: 'white'}}>
        <Calendar {...this.props}
          markingType={'period'}
          current={this.state.fromDate}
          markedDates={this.state.markedDates}
          onDayPress={(day) => {this.onDayPress(day)}}/>
      </View>
    )
  }

}

DatePickerComponent.defaultProps = {
  theme: { 
    markColor: '#00adf5', markTextColor: '#ffffff'
  }
};

class DatePicker extends Component {

  constructor(props){
    super(props);
    this.state ={
      date: {
        startDate: ''
      }
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <DatePickerComponent
          onDateSelected={this.onDateSelected.bind(this)}
          theme={{ 
            markColor: CONSTANT.COLOR.LIGHT_BLUE, markTextColor: 'white' }}/>
      </View>
    );
  }

  onDateSelected = (s) => {
    let startDate = s;
    this.props.actionNavigator.dismissLightBox();
    this.props.onPressButtonPrimary(startDate);
  }
}

function mapStateToProps(state, ownProps) {
  return {
    delayOrderReasonList:state.rootReducer.masterData.delayOrderReasonList,
    rejectOrderReasonList:state.rootReducer.masterData.rejectOrderReasonList,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionMasterData: bindActionCreators(actions.masterData, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DatePicker);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonTextStyle:{
    color: CONSTANT.COLOR.WHITE, 
    fontFamily: CONSTANT.TEXT_FAMILY.MAIN,
    fontSize: Scaling.moderateScale(15)
  },
  buttonStyle:{
    borderRadius:Scaling.moderateScale(15),
    width:Dimensions.get('window').width * 0.7 * 0.4
  }
});