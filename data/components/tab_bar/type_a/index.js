import React, { Component } from 'react';
import {View, Dimensions, TouchableHighlight} from 'react-native';
import ResponsiveImage from 'react-native-responsive-image';

import {TextWrapper} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class TypeA extends Component{
  constructor(props) {
    super(props);
    this.state = {
      activeIndex:this.props.tabKey ? this.props.tabKey : 0,
    };
  }

  onPress(key){
    this.setState({activeIndex:key}, () => {
      var passProps = this.props.passProps ? this.props.passProps : {};
      passProps.tabKey = key;
      this.props.actionNavigator.resetTo(this.props.navigator, this.props.item[key].route, passProps);
    });
  }

  _getProportionSize(){
    return 1 / this.props.item.length;
  }

  render(){
    return (
      <View
        style={{
          flex:1,
          height:Dimensions.get('window').height * 0.085,
          backgroundColor:'#fff',
          borderTopWidth:1,
          borderColor:'lightgray',
          flexDirection:'row',
          justifyContent:'space-around',
        }}
      >
          {
            this.props.item.map((data, key) => {
              let isActive = false;
              if (key == this.state.activeIndex) {
                isActive = true;
              }
              return (
                  <TabBarItem 
                    {...data}
                    key={key}
                    isActive={isActive}
                    onPress={this.onPress.bind(this, key)}
                    proportionSize={this._getProportionSize()}
                  />
              );
            })
          }
      </View>
    );
  }
}

class TabBarItem extends Component{
  render(){
    return(
      <TouchableHighlight 
        onPress={this.props.onPress}
        underlayColor={'transparent'}
        style={{
            flex:1,
            alignItems:'center',
            justifyContent:'center',
        }}
      >
          <View style={{
            alignItems:'center',
            justifyContent:'center',
          }}>

            <ResponsiveImage
                style={{tintColor: this.props.isActive ? CONSTANT.COLOR.BLUE_A : 'gray'}}
                source={this.props.imageSource}
                initWidth={this.props.imageWidth}
                initHeight={this.props.imageHeight}
            />

            <TextWrapper type={'base'} style={{color: this.props.isActive ? CONSTANT.COLOR.BLUE_A : 'gray'}}>{this.props.itemText}</TextWrapper>
          </View>
      </TouchableHighlight>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TypeA);

