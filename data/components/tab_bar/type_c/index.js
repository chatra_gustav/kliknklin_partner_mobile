import React, { Component } from 'react';
import {View, Dimensions, TouchableOpacity} from 'react-native';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';

import {TextWrapper, DateFormat, Scaling} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class TypeC extends Component{
  constructor(props) {
    super(props);
    this.state = {
      activeIndex:this.props.tabKey ? this.props.tabKey : 0,
    };
  }

  onPress(key, data){
    this.props.onPress(key, data);
    this.setState({activeIndex:key});
  }

  _getProportionSize(){
    return 1 / this.props.item.length;
  }

  render(){
    return (
      <View
        style={[{
          height: Dimensions.get('window').height * 0.1,
          justifyContent:'center',
          flexDirection: 'row',
          backgroundColor: '#FFFFFF',
          borderRadius: 8,
          elevation: 1,
        },
        this.props.containerStyle]}
      >
        {
          this.props.item.map((item, key) => {
            let isActive = false;
            if (key == this.state.activeIndex) {
              isActive = true;
            }
            return (
                <TabBarItem
                  key={key}
                  date={item.date}
                  isImportant={item.isImportant}
                  proportionSize={this._getProportionSize()}
                  isActive={isActive}
                  {...this.props}
                  onPress={this.onPress.bind(this, key, item.date)}
                />
            );
          })
        }
      </View>
    );
  }
}

TypeC.defaultProps = {
  onPress: () => {}
}

class TabBarItem extends Component{
  render(){
    return(
      <TouchableOpacity
        onPress={this.props.onPress}
        style={{
          flex:this.props.proportionSize,
          justifyContent:'center',
          alignItems:'center',
          borderColor : CONSTANT.COLOR.BLUE,
          borderBottomWidth : this.props.isActive ? 2 : 0,
          margin:moderateScale(5),
        }}
      >
          {
            this.props.isImportant ?
             <View
                style={{
                  width:Scaling.scale(5),
                  height:Scaling.scale(5),
                  borderRadius:Scaling.moderateScale(10),
                  backgroundColor:CONSTANT.COLOR.RED,
                  position:'absolute',
                  top:'1.5%',
                }}
             /> 
            :
            null
          }
          <TextWrapper type={'base'} style={{color:this.props.isActive  ? CONSTANT.COLOR.BLUE  : CONSTANT.COLOR.GRAY}}>{DateFormat(this.props.date, 'dayNameShort')}</TextWrapper>
          <TextWrapper type={'smallercontent'} style={{color:this.props.isActive  ? CONSTANT.COLOR.BLUE : CONSTANT.COLOR.GRAY}}>{DateFormat(this.props.date, 'day')}</TextWrapper>
          <TextWrapper type={'smallbase'} style={{color:this.props.isActive  ? CONSTANT.COLOR.BLUE : CONSTANT.COLOR.GRAY}}>{DateFormat(this.props.date, 'monthNameShort')}</TextWrapper>
      </TouchableOpacity>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(TypeC);

