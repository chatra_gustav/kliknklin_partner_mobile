import { connect } from 'react-redux';

import TypeA from './type_a';
import TypeB from './type_b';
import TypeC from './type_c';
import MainDashboard from './main_dashboard';
import TypeD from './type_d';

export {
	TypeA,
	TypeB,
	TypeC,
	TypeD,
	MainDashboard,
}