import React, { Component } from "react";
import { View, Dimensions, TouchableHighlight, Image } from "react-native";
import ResponsiveImage from "react-native-responsive-image";

import { TextWrapper, Text, Scaling } from "app/data/components";
import * as STRING from "app/data/string";
import * as CONSTANT from "app/data/constant";

//redux
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from "app/data/actions";

class MainDasboard extends Component {
    static defaultProps = {
        routeCategory: "reset"
    };
    constructor(props) {
        super(props);
        this.state = {
            activeIndex: this.props.tabKey ? this.props.tabKey : 0
        };
    }

    componentDidMount() {}

    onPress(key) {
        var passProps = this.props.passProps ? this.props.passProps : {};
        if (this.props.item[key].routeCategory != "push") {
            passProps.tabKey = key;
            this.setState({ activeIndex: key });
        }

        if (this.props.item[key].routeCategory == "push") {
            this.props.actionNavigator.push(
                this.props.navigator,
                this.props.item[key].route,
                passProps
            );
        } else {
            this.props.actionNavigator.resetTo(
                this.props.navigator,
                this.props.item[key].route,
                passProps
            );
        }
    }

    _getProportionSize() {
        return 1 / this.props.item.length;
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    position: "absolute",
                    bottom: 0,
                    right: 0,
                    left: 0,
                    elevation: 10,
                    height: Scaling.verticalScale(
                        CONSTANT.STYLE.TAB_BAR.MAIN_DASHBOARD.HEIGHT
                    ),
                    paddingLeft: Scaling.scale(
                        CONSTANT.STYLE.TAB_BAR.MAIN_DASHBOARD.PADDING_LEFT
                    ),
                    paddingRight: Scaling.scale(
                        CONSTANT.STYLE.TAB_BAR.MAIN_DASHBOARD.PADDING_RIGHT
                    ),
                    backgroundColor: CONSTANT.COLOR.WHITE,
                    borderColor: CONSTANT.COLOR.LIGHT_GRAY,
                    flexDirection: "row"
                }}
            >
                {this.props.item.map((data, key) => {
                    let isActive = false;
                    if (key == this.state.activeIndex) {
                        isActive = true;
                    }
                    return (
                        <TabBarItem
                            {...data}
                            key={key}
                            isActive={isActive}
                            onPress={this.onPress.bind(this, key)}
                            proportionSize={this._getProportionSize()}
                        />
                    );
                })}
            </View>
        );
    }
}

class TabBarItem extends Component {
    render() {
        return (
            <TouchableHighlight
                onPress={this.props.onPress}
                underlayColor={"transparent"}
                style={{
                    flex: this.props.proportionSize,
                    alignItems: "center",
                    justifyContent: "center"
                }}
            >
                <View
                    style={{
                        flex: 1
                    }}
                >
                    <View
                        style={{
                            flex: 0.6,
                            alignItems: "center",
                            justifyContent: "flex-end"
                        }}
                    >
                        <Image
                            source={
                                this.props.isActive
                                    ? this.props.selectedImageSource
                                    : this.props.imageSource
                            }
                            style={{
                                width: Scaling.scale(CONSTANT.STYLE.IMAGE.ICON.WIDTH),
                                height: Scaling.verticalScale(
                                    CONSTANT.STYLE.IMAGE.ICON.HEIGHT
                                ),
                                resizeMode: "contain"
                            }}
                        />
                    </View>

                    <View
                        style={{
                            alignItems: "center",
                            justifyContent: "center",
                            flex: 0.4
                        }}
                    >
                        <Text
                            fontOption={CONSTANT.TEXT_OPTION.ICON}
                            textStyle={{
                                color: this.props.isActive
                                    ? CONSTANT.COLOR.BROWN
                                    : CONSTANT.COLOR.GRAY
                            }}
                        >
                            {this.props.itemText}
                        </Text>
                    </View>
                </View>
            </TouchableHighlight>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return {
        actionNavigator: bindActionCreators(actions.navigator, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MainDasboard);
