import React, { Component } from 'react';
import {View, Dimensions, TouchableOpacity, StyleSheet, Image} from 'react-native';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale, verticalScale} from 'react-native-size-matters';
import ResolveAssetSource from 'resolveAssetSource';

import {DateFormat, Text, Scaling} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class TypeD extends Component{
  constructor(props) {
    super(props);
    this.state = {
      activeIndex:this.props.tabKey ? this.props.tabKey : 0,
    };
  }

  onPress(key, data){
    this.props.onPress(key, data);
    this.setState({activeIndex:key});
  }

  render(){
    return (
      <View style={{width: '100%', alignItems: 'center'}}>
        <View
          style={[{
            height: Scaling.verticalScale(44),
            width: Dimensions.get('window').width * 0.85,
            justifyContent:'center',
            alignItems: 'center',
            flexDirection: 'row',
            backgroundColor: '#FFFFFF',
            borderRadius: Scaling.moderateScale(5),
            elevation: 1,
          }]}
        >
          <TabBarItem
            key={0}
            tabKey={0}
            data={this.props.item[0]}
            isActive={(0 == this.state.activeIndex ? true : false)}
            isShowImportantSign={this.props.item[0].isShowImportantSign}
            onPress={this.onPress.bind(this, 0, this.props.item[0])}
            image={this.state.image}
            icon={this.props.icon}
          />

          <View style={{
            width:Scaling.moderateScale(1), 
            height: Scaling.verticalScale(37), 
            borderColor : CONSTANT.COLOR.GRAY, 
            borderWidth: StyleSheet.hairlineWidth
          }}></View>

          <TabBarItem
            key={1}
            tabKey={1}
            data={this.props.item[1]}
            isActive={(1 == this.state.activeIndex ? true : false)}
            isShowImportantSign={this.props.item[1].isShowImportantSign}
            onPress={this.onPress.bind(this, 1, this.props.item[1])}
            image={this.state.image}
            icon={this.props.icon}
          />
          
        </View>
      </View>
    );
  }
}

TypeD.defaultProps = {
  onPress: () => {},
  icon: null
}

class TabBarItem extends Component{

  render(){

    let textColor = CONSTANT.COLOR.GRAY;
    
    if(this.props.isActive){
      textColor = CONSTANT.COLOR.LIGHT_BLUE;
    }

    return(
      <TouchableOpacity
        onPress={() => this.props.onPress(this.props.tabKey, this.props.data)}
        style={{
          flex: 1,
          justifyContent:'center',
          alignItems:'center',
          borderColor : CONSTANT.COLOR.LIGHT_BLACK,
          marginTop: Scaling.moderateScale(5),
          marginBottom: Scaling.moderateScale(5),
          flexDirection:'row'
        }}
      >
        <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: textColor}}>{this.props.data.itemText}</Text>
          {this.renderIcon()}

      </TouchableOpacity>
    );
  }

  renderIcon(){
    let {isShowImportantSign} = this.props;

    if(isShowImportantSign){
      return(
        <View
          style={{
            width: Scaling.moderateScale(10),
            height: Scaling.moderateScale(10),
            borderRadius: Scaling.moderateScale(15),
            backgroundColor:'red',
            marginLeft: Scaling.moderateScale(8),
            marginTop:Scaling.verticalScale(2),
          }}
        />
      )
    }
    else if(this.props.data.itemText == CONSTANT.NOTIFICATION_TAB_TYPE.SECOND){
      return(
        this.props.icon
      )
    }
    else{
      return null
    }
  }
}

function mapStateToProps(state, ownProps) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TypeD);

const styles = StyleSheet.create({
  icon:{
    position: 'absolute',
    top: 0,
    right: '40%'
  }
})
