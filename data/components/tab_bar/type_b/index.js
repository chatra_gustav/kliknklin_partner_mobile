import React, { Component } from 'react';
import {View, Dimensions, TouchableHighlight} from 'react-native';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';

import {Text, Scaling} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class TypeB extends Component{
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  onPress(key){
    this.props.onPress(key);
  }


  _getProportionSize(){
    return 1 / this.props.item.length;
  }

  render(){
    return (
      <View
        style={[{
          height:Scaling.verticalScale(40),
          alignItems:'center',
        },
        this.props.containerStyle,
        ]}
      >
          <View
            style={[
              {
                height:Scaling.verticalScale(40),
                width:'80%',
                marginHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                backgroundColor:'#fff',
                borderColor:'lightgray',
                borderRadius:5,
                flexDirection:'row',
                elevation:2,
              },
            ]}
          >
            {
                this.props.item.map((data, key) => {
                  let isActive = false;
                  let isLastIndex = false;
                  if (key == this.props.currentTabKey) {
                    isActive = true;
                  }

                  if (this.props.item.length == (key+1)) {
                    isLastIndex = true;
                  }
                  return (
                      <TabBarItem
                        {...this.props}
                        isActive={isActive}
                        key={key}
                        isLastIndex={isLastIndex}
                        itemText={data.itemText}
                        isShowImportantSign={data.isShowImportantSign}
                        rightItem={data.rightItem}
                        proportionSize={this._getProportionSize()}
                        onPress={this.onPress.bind(this, key)}
                      />
                  );
                })
            }
          </View>
      </View>
    );
  }
}

TypeB.defaultProps = {
  onPress: () => {}
}

class TabBarItem extends Component{
  
  render(){
    let {rightItem, index, isLastIndex} = this.props;
    return(
      <View
        style={{
          flex:this.props.proportionSize,
          flexDirection:'row',
        }}
      >  
          <TouchableHighlight 
            onPress={this.props.onPress}
            underlayColor={'transparent'}
            style={[
              {
                flex:1,
                justifyContent:'center',
                alignItems:'center',
              },
            ]}
          >
              <View style={{
                flexDirection:'row',
                alignItems:'center',
              }}>
                  <Text 
                    fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}
                    textStyle={[{color: this.props.isActive ? CONSTANT.COLOR.LIGHT_BLUE : CONSTANT.COLOR.GRAY}, this.props.tabItemTextStyle]}
                  >
                    {this.props.itemText}
                  </Text>
              </View>
          </TouchableHighlight>
          
          <View
            style={{
              position:'absolute',
              right:'5%',
              top:'25%',
            }}
          >
            {rightItem}
          </View>
          <View
            style={{
              justifyContent:'center',
            }}
          >
              <View 
                style={[
                  (
                      !isLastIndex ? 
                      {
                        height:'60%',
                        borderRightWidth:Scaling.moderateScale(2),
                        borderColor:CONSTANT.COLOR.GRAY,
                      } 
                      :{}
                  )
                ]}
              />
          </View>
      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TypeB);

