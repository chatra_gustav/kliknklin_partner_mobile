import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';
import {moderateScale} from 'react-native-size-matters';

import {TextWrapper, Label} from './../../components';
import * as CONSTANT from './../../constant';

export default class ShipmentStatus extends Component{
  constructor(props) {
      super(props);
      this.state = {
      };
    }

  _getData(){
    let result = {
        labelColor:'transparent',
        labelText:'label',
    };

    let {shipmentStatus} = this.props;

    if (shipmentStatus == CONSTANT.SHIPMENT_STATUS.WAIT_TO_START) {
        //result.labelColor = '#69CAF0'; 
        //result.labelText = 'AKTIF';
    }
    else if (shipmentStatus == CONSTANT.SHIPMENT_STATUS.IN_PROGRESS) {
        result.labelColor = CONSTANT.COLOR.BLUE_A; 
        result.labelText = 'TUNGGU';
    }
    else if (shipmentStatus == CONSTANT.SHIPMENT_STATUS.DELAY_CUSTOMER ||
      shipmentStatus == CONSTANT.SHIPMENT_STATUS.DELAY_PARTNER) {
        result.labelColor = CONSTANT.COLOR.YELLOW_A; 
        result.labelText = 'TUNDA';
    }
    else if (shipmentStatus == CONSTANT.SHIPMENT_STATUS.DONE) {
        result.labelColor = CONSTANT.COLOR.GREEN_A; 
        result.labelText = 'SELESAI';
    }
    else if (shipmentStatus == CONSTANT.SHIPMENT_STATUS.CANCEL) {
        result.labelColor = CONSTANT.COLOR.GRAY_B; 
        result.labelText = 'BATAL';
    }
    else if (shipmentStatus == CONSTANT.SHIPMENT_STATUS.LATE) {
        result.labelColor = CONSTANT.COLOR.RED_A; 
        result.labelText = 'TELAT';
    }
    else if (shipmentStatus == CONSTANT.SHIPMENT_STATUS.ACTIVE) {
        result.labelColor = CONSTANT.COLOR.BLUE_A; 
        result.labelText = 'AKTIF';
    }

    return result;
  }

  render(){
    let {labelColor, labelText} = this._getData();

    let {shipmentStatus} = this.props;

    if (shipmentStatus != CONSTANT.SHIPMENT_STATUS.WAIT_TO_START) {
      return(
          <Label.TypeA 
            {...this.props}
            labelText={labelText}
            labelColor={labelColor}
            containerStyle={{
              minWidth:0,
              width:moderateScale(60),
            }}
            fontType={'base'}
          />
      );
    }
    return null;
  }
}

ShipmentStatus.propTypes = {
  //shipmentStatus: PropTypes.oneOf(['AKTIF', 'TUNDA', 'BATAL', 'TELAT', 'SELESAI', 'NONE']),
  statusColor:PropTypes.string,
}

ShipmentStatus.defaultProps = {
  statusColor:'black',
}

