import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';

import {TextWrapper, DateFormat} from './../../components';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../actions";

class ShipmentScheduleListCourier extends Component {
  
  _onPressOrderDetail(data){
      this.props.actionOrderDetail.setOrderDetailData(data);
      this.props.actionNavigator.goToOrderDetailPickupScreen(this.props.navigator);
  }
  
  render() {
    return (
      <View style={{
        flex: 1,
        alignItems: 'center',
        marginBottom: Dimensions.get('window').height * 0.01,
      }}>
        <View style={{}}>
          <ShipmentDateList />
        </View>
        <View style={{flex:1, paddingTop:10}}>
        	<ShipmentOrderList 
              layoutHeight={this.props.layoutHeight}
              _onPressOrderDetail={this._onPressOrderDetail.bind(this)}
          />
        </View>
      </View>

    );
  }
}

class ShipmentDateList extends Component{
    constructor(props) {
          super(props);
          this.state = {
            activeIndex: 0,
          };  
    }

    componentWillMount(){
      this._getShipmentDateComponent();
    }

    _getShipmentDateComponent(){
      const shipmentDateList = [];

      this.setState({scheduleDateList:this._getListOfScheduleDate()}, () =>{
        this.state.scheduleDateList.forEach((date, key) => {
            shipmentDateList.push(
            <TouchableOpacity
                style={this.state.activeIndex == key ? styles.eachDateIndicatorActive : styles.eachDateIndicator} 
                key={key}
                onPress={() => {
                  this.setState({activeIndex:key}, () => {
                    this._getShipmentDateComponent();
                  });
                }}
            >
                <TextWrapper type={'base'} style={this.state.activeIndex == key ? styles.dateScheduleTextActive : {}}>{DateFormat(this.state.scheduleDateList[key], 'dayNameShort')}</TextWrapper>
                <TextWrapper type={'smallercontent'} style={this.state.activeIndex == key ? styles.dateScheduleTextActive : {}}>{DateFormat(this.state.scheduleDateList[key], 'day')}</TextWrapper>
                <TextWrapper type={'smallbase'} style={this.state.activeIndex == key ? styles.dateScheduleTextActive : {}}>{DateFormat(this.state.scheduleDateList[key], 'monthNameShort')}</TextWrapper>
            </TouchableOpacity>
            );
          })

        this.setState({shipmentDateList});
      });
    }

    _getListOfScheduleDate(startDate = new Date()){
        var scheduleDateList = [];

        for (var i = 0; i < 7; i++) {
          var date = new Date(startDate);
          scheduleDateList.push(new Date(date.setDate(date.getDate() + i)));

        }
        
        return scheduleDateList;
    }

  render(){
    return(
      <View style={{
        width: Dimensions.get('window').width * 0.85,
        height: Dimensions.get('window').height * 0.1,
        justifyContent:'center',
        flexDirection: 'row',
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        elevation: 1,
      }}>

        {this.state.shipmentDateList}

      </View>
    );
  }
}

class ShipmentOrderList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      shipmentOrderNode: {},
    };  
  }

  componentWillMount(){
    this._getShipmentOrderListComponent();
  }

  componentDidMount(){
  }

  _getShipmentOrderListComponent(){
      const shipmentOrderList = [];

      const shipmentData = Data.ShipmentData[0].Data;

      shipmentData.forEach((shipmentSlot, key) => {
        shipmentOrderList.push(
          <View 
            ref={component => {
                const shipmentOrderNode = this.state.shipmentOrderNode;
                shipmentOrderNode[shipmentSlot.timeSlot] = component;
                this.setState({shipmentOrderNode});
              }
            }

            onLayout={() => {
              const nodeActive = this.state.shipmentOrderNode['11.00'];
              nodeActive.measure( (ox, oy, width, height, px, py) => {
                var py = py - (this.props.layoutHeight + Dimensions.get('window').height * 0.025);
                this._scrollViewNode.scrollTo({x: 0, y: py, animated: true})
              });
            }}

            style={{
              flex: 1,
              flexDirection: 'row',
            }} 
            key={key}
          >

              <View style={styles.timeLine}>
                <View style={styles.insideTL}>
                  <TextWrapper type={'base'} style={{marginRight: 3}}>
                    {shipmentSlot.timeSlot}
                  </TextWrapper>
                </View>
                <View style={styles.insideTL}>
                </View>
              </View>

              <View style={styles.dataLine}>
                  <ShipmentOrder 
                      parentKey={key}
                      _onPressOrderDetail={this.props._onPressOrderDetail}
                  />
              </View>

         </View>
      );
    })

    this.setState({shipmentOrderList});
  }

  render() {

    return (
      <View style={styles.shipmentListWrapper}>
        <ScrollView 
          ref={component => {
            this._scrollViewNode = component;
          }}
          showsVerticalScrollIndicator={false}
          RefreshControl={true}
        >
        {this.state.shipmentOrderList}
        </ScrollView>
      </View> 
    )
  }
}

class ShipmentOrder extends Component {

  componentWillMount(){
    this._getShipmentOrderComponent();
  }

  _getShipmentOrderComponent(){
      const shipmentOrder = [];

      const shipmentData = Data.ShipmentData[0].Data[this.props.parentKey].dataSlot;

      shipmentData.forEach((shipmentSlot, key) => {
        const address = shipmentSlot.address.substr(0, shipmentSlot.address.indexOf(',', shipmentSlot.address.indexOf(',') + 1))
        shipmentOrder.push(
          <TouchableOpacity 
            style={shipmentSlot.orderStatus == 'WaitingForPickup' || shipmentSlot.orderStatus == 'GoToPickupPoint' ? styles.shipmentContainerPickup : styles.shipmentContainerDelivery}
            key={key}
            onPress={() => this.props._onPressOrderDetail(shipmentSlot)}
          >
              <View style={styles.shipment}>
                  <View style={{
                    flex:0.75,
                    marginRight: Dimensions.get('window').width * 0.02,
                  }}>
                    <TextWrapper type={'base'}>
                      {address}
                    </TextWrapper>
                  </View>
                  <View style={{
                    justifyContent:'center', 
                    flex:0.25,
                  }}
                  >
                  <View style={[
                              {
                                borderWidth:1,
                                borderRadius:4,
                                alignItems:'center'
                              }, 
                              {
                                borderColor: this.shipmentColor(shipmentSlot.shipmentStatus),
                                borderWidth:1,
                              }
                        ]}
                    >
                    <TextWrapper 
                      type={'smallbase'}
                      style={{ 
                        color: this.shipmentColor(shipmentSlot.shipmentStatus), 
                        fontWeight:'500' 
                      }}
                    >
                      {shipmentSlot.shipmentStatus}
                    </TextWrapper>
                  </View>
                  </View>
              </View>
          </TouchableOpacity>
      );
    })

    this.setState({shipmentOrder});
  }

  render() {

    return (
      <View>
      {this.state.shipmentOrder}
      </View>
    );

    
  }


shipmentColor(shipmentStatus) {
  var color = 'transparent';
  if(shipmentStatus == 'AKTIF') { color = '#69CAF0'; } // biru
  if(shipmentStatus == 'TUNDA') { color = '#FFC300'; } // yellow
  if(shipmentStatus == 'BATAL') { color = 'lightgray' ; } // gray
  if(shipmentStatus == 'TELAT') { color = '#BF2000'; } // red
  if(shipmentStatus == 'SELESAI') { color = '#AEC93A'; } // hijau

  return color;
}

}

const styles = StyleSheet.create({
  containerHeaderLogin:{
  	flex:1,
	flexDirection:'row',
  },
  paddingOnTop: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height * 0.085,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'yellow',
  },
  statIndicator: {
    flex: 1,
    width: Dimensions.get('window').width * 0.85,
    height: Dimensions.get('window').height * 0.10,
    flexDirection: 'row',
    marginVertical: Dimensions.get('window').height * 0.01,
    // backgroundColor: 'grey'
  },
  eachStatIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  dateScheduleTextActive: {
	color:'#61CDFF',
  },
  dateIndicatorWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: Dimensions.get('window').height * 0.02,
    // backgroundColor: 'blue',
  },
  dateIndicator: {
    flex: 0.15,
    width: Dimensions.get('window').width * 0.85,
    height: Dimensions.get('window').height * 0.10,
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    marginVertical: Dimensions.get('window').height * 0.01,
    elevation: 1,
    justifyContent:'center',
  },
  eachDateIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  eachDateIndicatorActive: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    borderBottomWidth: 2,
    borderBottomColor: '#61CDFF', 
    borderStyle: 'dotted'
  },
  shipmentListWrapper: {
    flex: 1,
    width: Dimensions.get('window').width * 0.90,
    height: Dimensions.get('window').height * 0.53,
  },
  timeLineWrapper: {
    flex: 1,
    flexDirection: 'row',
  },
  timeLine: {
    flex: 1,
    flexDirection: 'row',
    // borderRightWidth: 1,
    // borderRightColor: 'grey',
  },
  insideTL: {
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
  },
  dataLine: {
    flex: 9,
    borderLeftWidth: 1,
    borderLeftColor: '#E8E8E8',
    paddingHorizontal: Dimensions.get('window').width * 0.01,
    paddingBottom: Dimensions.get('window').height * 0.015,
    marginTop: Dimensions.get('window').height * 0.015,
  },
  shipmentContainerPickup: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingVertical: Dimensions.get('window').height * 0.01,
    borderRadius: 5,
    marginHorizontal: Dimensions.get('window').width * 0.02,
    borderLeftWidth: 5,
    borderLeftColor: '#61CDFF',
    marginVertical: Dimensions.get('window').height * 0.01,
    elevation: 2,
    height:Dimensions.get('window').height * 0.1,
  },
  shipmentContainerDelivery: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingVertical: Dimensions.get('window').height * 0.01,
    borderRadius: 5,
    marginHorizontal: Dimensions.get('window').width * 0.02,
    borderLeftWidth: 5,
    borderLeftColor: '#AEC93A',
    // borderLeftColor: '#A1D39A',
    marginVertical: Dimensions.get('window').height * 0.01,
    elevation: 2,
    height:Dimensions.get('window').height * 0.1,
  },
  shipment: {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: Dimensions.get('window').width * 0.02,
    alignItems:'center',
  },
  addressIndicator: {
    flex: 3,
    justifyContent: 'flex-start',
    alignItems: 'flex-start', 
    marginRight: Dimensions.get('window').width * 0.02,
  },
  shipmentStatusIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center', 
    // borderColor: 'green',
    borderRadius:4,
    borderWidth: 1,
  }

});


const Data = {
  StatData : {
    pickup:13,
    delivery:16,
  },

  ShipmentData: [
    {
      Date: '2018-01-21', 
      Data: [
        { 
          timeSlot: '10.00',
          dataSlot: [

            { orderID: '123131231', shipmentStatus: 'BATAL', orderStatus:'WaitingForPickup',  shipmentDate:'12-03-2018',pickupTime: '10.00', address: 'Ariobimo Sentral, Lt. 9, Jl. HR Rasuna Said Blok X-2 Kav 5, RT.9/RW.4, Kuningan Tim., Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950', addressInfo:'lantai 8 langsung ke resepsionis saja ya mas trims',
              orderItems:[
        {
          itemName:'Sepatu Nylon/Rubber/Canvas',
          washingMethodName:'Deep Clean',
          quantity:4,
          unitName:'Pair',
          addedService:[
            {
              serviceName:'Unyellowing',
            },
            {
              serviceName:'Reglue',
            }
          ],
          unitPrice:24000,
        },
        {
          itemName:'Bed Cover Besar',
          washingMethodName:'Hand Clean',
          quantity:2,
          unitName:'Pcs',
          addedService:[],
          unitPrice:72000,
        },
        {
          itemName:'Cuci, Lipat, Gosok',
          washingMethodName:'Wet Clean',
          quantity:6,
          unitName:'Kg',
          addedService:[],
          unitPrice:12000,
        },
      ],
      promo:[
        {
          promoName:'Promo code #WEEKENDSERU',
          type:'order',
          quantity:1,
          unitPrice:-20000,
        },
        {
          promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
          type:'shipment',
          quantity:1,
          unitPrice:-20000,
        },
      ],
      shipment:[
        {
          shipmentName:'Internal kurir-Pengambilan',
          shipmentPrice:12000,
        },
        {
          shipmentName:'Internal kurir-Pengantaran',
          shipmentPrice:12000,
        }
      ]
          },
            { orderID: '233454554', shipmentStatus: 'SELESAI', orderStatus:'GoToPickupPoint',  pickupTime: '10.00', address: 'Palma One, Jalan Haji R. Rasuna Said, Karet Kuningan, Setiabudi, RT.8/RW.4, Kuningan Tim., Setia Budi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12940', 
              orderItems:[
                {
                  itemName:'Sepatu Nylon/Rubber/Canvas',
                  washingMethodName:'Deep Clean',
                  quantity:4,
                  unitName:'Pair',
                  addedService:[
                    {
                      serviceName:'Unyellowing',
                    },
                    {
                      serviceName:'Reglue',
                    }
                  ],
                },
                {
                  itemName:'Bed Cover Besar',
                  washingMethodName:'Hand Clean',
                  quantity:2,
                  unitName:'Pcs',
                  addedService:[],
                },
                {
                  itemName:'Cuci, Lipat, Gosok',
                  washingMethodName:'Wet Clean',
                  quantity:6,
                  unitName:'Kg',
                  addedService:[],
                },
              ],
            },
            { orderID: '233454554', shipmentStatus: 'SELESAI', orderStatus:'GoToDeliveryPoint',  pickupTime: '10.00', address: 'Jl. Fachrudin No.3, RT.1/RW.7, Kp. Bali, Tanah Abang, Jakarta, Daerah Khusus Ibukota Jakarta 10250',
              
              orderItems:[
        {
          itemName:'Sepatu Nylon/Rubber/Canvas',
          washingMethodName:'Deep Clean',
          quantity:4,
          unitName:'Pair',
          addedService:[
            {
              serviceName:'Unyellowing',
            },
            {
              serviceName:'Reglue',
            }
          ],
          unitPrice:24000,
        },
        {
          itemName:'Bed Cover Besar',
          washingMethodName:'Hand Clean',
          quantity:2,
          unitName:'Pcs',
          addedService:[],
          unitPrice:72000,
        },
        {
          itemName:'Cuci, Lipat, Gosok',
          washingMethodName:'Wet Clean',
          quantity:6,
          unitName:'Kg',
          addedService:[],
          unitPrice:12000,
        },
      ],
      promo:[
        {
          promoName:'Promo code #WEEKENDSERU',
          type:'order',
          quantity:1,
          unitPrice:-20000,
        },
        {
          promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
          type:'shipment',
          quantity:1,
          unitPrice:-20000,
        },
      ],
      shipment:[
        {
          shipmentName:'Internal kurir-Pengambilan',
          shipmentPrice:12000,
        },
        {
          shipmentName:'Internal kurir-Pengantaran',
          shipmentPrice:12000,
        }
      ]

          },
          ]
        },
        {
          timeSlot: '11.00',
          dataSlot: [
            { orderID: '129599231', latitude:-6.1330, longitude:106.8267, shipmentStatus: 'BATAL', orderStatus:'ReadyToBeDelivered', pickupTime: '11.00', address: 'JW Mariot Hotel, Jalan DR Ide Anak Agung Gde Agung Kav E.1.2 No. 1 & 2, Kawasan Mega Kuningan, East Kuningan, Setia Budi, Jakarta, DKI Jakarta 12950 asdasd asdasd asdasd asdasd asdasd asdasd asdasd', addressInfo:'lantai 8 langsung ke resepsionis saja ya mas trims belok kanan ujung jalan kenangan bang msih kurang',
              orderItems:[
                {
                  itemName:'Sepatu Nylon/Rubber/Canvas',
                  washingMethodName:'Deep Clean',
                  quantity:4,
                  unitName:'Pair',
                  addedService:[
                    {
                      serviceName:'Unyellowing',
                    },
                    {
                      serviceName:'Reglue',
                    }
                  ],
                  unitPrice:24000,
                },
                {
                  itemName:'Bed Cover Besar',
                  washingMethodName:'Hand Clean',
                  quantity:2,
                  unitName:'Pcs',
                  addedService:[],
                  unitPrice:72000,
                },
                {
                  itemName:'Cuci, Lipat, Gosok',
                  washingMethodName:'Wet Clean',
                  quantity:6,
                  unitName:'Kg',
                  addedService:[],
                  unitPrice:12000,
                },
              ],
              promo:[
                {
                  promoName:'Promo code #WEEKENDSERU',
                  type:'order',
                  quantity:1,
                  unitPrice:-20000,
                },
                {
                  promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
                  type:'shipment',
                  quantity:1,
                  unitPrice:-20000,
                },
              ],
              shipment:[
                {
                  shipmentName:'Internal kurir-Pengambilan',
                  shipmentPrice:12000,
                },
                {
                  shipmentName:'Internal kurir-Pengantaran',
                  shipmentPrice:12000,
                }
              ]
            },
          ]
        },
        {
          timeSlot: '15.00',
          dataSlot: [
            { orderID: '120823134', shipmentStatus: 'TUNDA', orderStatus: 'GoToPickupPoint',  pickupTime: '15.00', address: 'Agro Plaza, Jl. H.R. Rasuna Said Kav. X 2 - 1, Kuningan Timur RT.7/RW.4, Setiabudi, Kuningan Timur, RT.7/RW.4, Kuningan Tim., Setia Budi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950',
                  orderItems:[
        {
          itemName:'Sepatu Nylon/Rubber/Canvas',
          washingMethodName:'Deep Clean',
          quantity:4,
          unitName:'Pair',
          addedService:[
            {
              serviceName:'Unyellowing',
            },
            {
              serviceName:'Reglue',
            }
          ],
          unitPrice:24000,
        },
        {
          itemName:'Bed Cover Besar',
          washingMethodName:'Hand Clean',
          quantity:2,
          unitName:'Pcs',
          addedService:[],
          unitPrice:72000,
        },
        {
          itemName:'Cuci, Lipat, Gosok',
          washingMethodName:'Wet Clean',
          quantity:6,
          unitName:'Kg',
          addedService:[],
          unitPrice:12000,
        },
      ],
      promo:[
        {
          promoName:'Promo code #WEEKENDSERU',
          type:'order',
          quantity:1,
          unitPrice:-20000,
        },
        {
          promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
          type:'shipment',
          quantity:1,
          unitPrice:-20000,
        },
      ],
      shipment:[
        {
          shipmentName:'Internal kurir-Pengambilan',
          shipmentPrice:12000,
        },
        {
          shipmentName:'Internal kurir-Pengantaran',
          shipmentPrice:12000,
        }
      ]
            },
            { orderID: '120823134', shipmentStatus: 'TUNDA', orderStatus: 'ReadyToBeDelivered',  pickupTime: '15.00', address: 'Agro Plaza, Jl. H.R. Rasuna Said Kav. X 2 - 1, Kuningan Timur RT.7/RW.4, Setiabudi, Kuningan Timur, RT.7/RW.4, Kuningan Tim., Setia Budi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950',

            orderItems:[
        {
          itemName:'Sepatu Nylon/Rubber/Canvas',
          washingMethodName:'Deep Clean',
          quantity:4,
          unitName:'Pair',
          addedService:[
            {
              serviceName:'Unyellowing',
            },
            {
              serviceName:'Reglue',
            }
          ],
          unitPrice:24000,
        },
        {
          itemName:'Bed Cover Besar',
          washingMethodName:'Hand Clean',
          quantity:2,
          unitName:'Pcs',
          addedService:[],
          unitPrice:72000,
        },
        {
          itemName:'Cuci, Lipat, Gosok',
          washingMethodName:'Wet Clean',
          quantity:6,
          unitName:'Kg',
          addedService:[],
          unitPrice:12000,
        },
      ],
      promo:[
        {
          promoName:'Promo code #WEEKENDSERU',
          type:'order',
          quantity:1,
          unitPrice:-20000,
        },
        {
          promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
          type:'shipment',
          quantity:1,
          unitPrice:-20000,
        },
      ],
      shipment:[
        {
          shipmentName:'Internal kurir-Pengambilan',
          shipmentPrice:12000,
        },
        {
          shipmentName:'Internal kurir-Pengantaran',
          shipmentPrice:12000,
        }
      ]
          },
            { orderID: '120823134', shipmentStatus: 'AKTIF', orderStatus: 'GoToPickupPoint',  pickupTime: '15.00', address: 'Agro Plaza, Jl. H.R. Rasuna Said Kav. X 2 - 1, Kuningan Timur RT.7/RW.4, Setiabudi, Kuningan Timur, RT.7/RW.4, Kuningan Tim., Setia Budi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950',

              orderItems:[
        {
          itemName:'Sepatu Nylon/Rubber/Canvas',
          washingMethodName:'Deep Clean',
          quantity:4,
          unitName:'Pair',
          addedService:[
            {
              serviceName:'Unyellowing',
            },
            {
              serviceName:'Reglue',
            }
          ],
          unitPrice:24000,
        },
        {
          itemName:'Bed Cover Besar',
          washingMethodName:'Hand Clean',
          quantity:2,
          unitName:'Pcs',
          addedService:[],
          unitPrice:72000,
        },
        {
          itemName:'Cuci, Lipat, Gosok',
          washingMethodName:'Wet Clean',
          quantity:6,
          unitName:'Kg',
          addedService:[],
          unitPrice:12000,
        },
      ],
      promo:[
        {
          promoName:'Promo code #WEEKENDSERU',
          type:'order',
          quantity:1,
          unitPrice:-20000,
        },
        {
          promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
          type:'shipment',
          quantity:1,
          unitPrice:-20000,
        },
      ],
      shipment:[
        {
          shipmentName:'Internal kurir-Pengambilan',
          shipmentPrice:12000,
        },
        {
          shipmentName:'Internal kurir-Pengantaran',
          shipmentPrice:12000,
        }
      ]
          },
          ]          
        },
        {
          timeSlot: '16.00',
          dataSlot: [
            { orderID: '120823134', shipmentStatus: 'TELAT', orderStatus: 'ReadyToBeDelivered',  pickupTime: '15.00', address: 'Agro Plaza, Jl. H.R. Rasuna Said Kav. X 2 - 1, Kuningan Timur RT.7/RW.4, Setiabudi, Kuningan Timur, RT.7/RW.4, Kuningan Tim., Setia Budi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950',
                      orderItems:[
        {
          itemName:'Sepatu Nylon/Rubber/Canvas',
          washingMethodName:'Deep Clean',
          quantity:4,
          unitName:'Pair',
          addedService:[
            {
              serviceName:'Unyellowing',
            },
            {
              serviceName:'Reglue',
            }
          ],
          unitPrice:24000,
        },
        {
          itemName:'Bed Cover Besar',
          washingMethodName:'Hand Clean',
          quantity:2,
          unitName:'Pcs',
          addedService:[],
          unitPrice:72000,
        },
        {
          itemName:'Cuci, Lipat, Gosok',
          washingMethodName:'Wet Clean',
          quantity:6,
          unitName:'Kg',
          addedService:[],
          unitPrice:12000,
        },
      ],
      promo:[
        {
          promoName:'Promo code #WEEKENDSERU',
          type:'order',
          quantity:1,
          unitPrice:-20000,
        },
        {
          promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
          type:'shipment',
          quantity:1,
          unitPrice:-20000,
        },
      ],
      shipment:[
        {
          shipmentName:'Internal kurir-Pengambilan',
          shipmentPrice:12000,
        },
        {
          shipmentName:'Internal kurir-Pengantaran',
          shipmentPrice:12000,
        }
      ]
            },
            { orderID: '120823134', shipmentStatus: '', orderStatus: 'GoToPickupPoint',  pickupTime: '15.00', address: 'Agro Plaza, Jl. H.R. Rasuna Said Kav. X 2 - 1, Kuningan Timur RT.7/RW.4, Setiabudi, Kuningan Timur, RT.7/RW.4, Kuningan Tim., Setia Budi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950',
                  orderItems:[
        {
          itemName:'Sepatu Nylon/Rubber/Canvas',
          washingMethodName:'Deep Clean',
          quantity:4,
          unitName:'Pair',
          addedService:[
            {
              serviceName:'Unyellowing',
            },
            {
              serviceName:'Reglue',
            }
          ],
          unitPrice:24000,
        },
        {
          itemName:'Bed Cover Besar',
          washingMethodName:'Hand Clean',
          quantity:2,
          unitName:'Pcs',
          addedService:[],
          unitPrice:72000,
        },
        {
          itemName:'Cuci, Lipat, Gosok',
          washingMethodName:'Wet Clean',
          quantity:6,
          unitName:'Kg',
          addedService:[],
          unitPrice:12000,
        },
      ],
      promo:[
        {
          promoName:'Promo code #WEEKENDSERU',
          type:'order',
          quantity:1,
          unitPrice:-20000,
        },
        {
          promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
          type:'shipment',
          quantity:1,
          unitPrice:-20000,
        },
      ],
      shipment:[
        {
          shipmentName:'Internal kurir-Pengambilan',
          shipmentPrice:12000,
        },
        {
          shipmentName:'Internal kurir-Pengantaran',
          shipmentPrice:12000,
        }
      ]
              
          },
          ]          
        },
        {
          timeSlot: '18.00',
          dataSlot: [
            { orderID: '120823134', shipmentStatus: '', orderStatus: 'GoToPickupPoint',  pickupTime: '15.00', address: 'Agro Plaza, Jl. H.R. Rasuna Said Kav. X 2 - 1, Kuningan Timur RT.7/RW.4, Setiabudi, Kuningan Timur, RT.7/RW.4, Kuningan Tim., Setia Budi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950',
                  orderItems:[
        {
          itemName:'Sepatu Nylon/Rubber/Canvas',
          washingMethodName:'Deep Clean',
          quantity:4,
          unitName:'Pair',
          addedService:[
            {
              serviceName:'Unyellowing',
            },
            {
              serviceName:'Reglue',
            }
          ],
          unitPrice:24000,
        },
        {
          itemName:'Bed Cover Besar',
          washingMethodName:'Hand Clean',
          quantity:2,
          unitName:'Pcs',
          addedService:[],
          unitPrice:72000,
        },
        {
          itemName:'Cuci, Lipat, Gosok',
          washingMethodName:'Wet Clean',
          quantity:6,
          unitName:'Kg',
          addedService:[],
          unitPrice:12000,
        },
      ],
      promo:[
        {
          promoName:'Promo code #WEEKENDSERU',
          type:'order',
          quantity:1,
          unitPrice:-20000,
        },
        {
          promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
          type:'shipment',
          quantity:1,
          unitPrice:-20000,
        },
      ],
      shipment:[
        {
          shipmentName:'Internal kurir-Pengambilan',
          shipmentPrice:12000,
        },
        {
          shipmentName:'Internal kurir-Pengantaran',
          shipmentPrice:12000,
        }
      ]
          },
            { orderID: '120823134', shipmentStatus: '', orderStatus: 'ReadyToBeDelivered',  pickupTime: '15.00', address: 'Agro Plaza, Jl. H.R. Rasuna Said Kav. X 2 - 1, Kuningan Timur RT.7/RW.4, Setiabudi, Kuningan Timur, RT.7/RW.4, Kuningan Tim., Setia Budi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950',
                orderItems:[
        {
          itemName:'Sepatu Nylon/Rubber/Canvas',
          washingMethodName:'Deep Clean',
          quantity:4,
          unitName:'Pair',
          addedService:[
            {
              serviceName:'Unyellowing',
            },
            {
              serviceName:'Reglue',
            }
          ],
          unitPrice:24000,
        },
        {
          itemName:'Bed Cover Besar',
          washingMethodName:'Hand Clean',
          quantity:2,
          unitName:'Pcs',
          addedService:[],
          unitPrice:72000,
        },
        {
          itemName:'Cuci, Lipat, Gosok',
          washingMethodName:'Wet Clean',
          quantity:6,
          unitName:'Kg',
          addedService:[],
          unitPrice:12000,
        },
      ],
      promo:[
        {
          promoName:'Promo code #WEEKENDSERU',
          type:'order',
          quantity:1,
          unitPrice:-20000,
        },
        {
          promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
          type:'shipment',
          quantity:1,
          unitPrice:-20000,
        },
      ],
      shipment:[
        {
          shipmentName:'Internal kurir-Pengambilan',
          shipmentPrice:12000,
        },
        {
          shipmentName:'Internal kurir-Pengantaran',
          shipmentPrice:12000,
        }
      ]
              
          },
          ]          
        } 
      ]
    },
  ]
};

function mapStateToProps(state, ownProps) {
  return {
    layoutHeight:state.rootReducer.layout.height,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionOrderDetail: bindActionCreators(actions.orderDetail, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ShipmentScheduleListCourier);

