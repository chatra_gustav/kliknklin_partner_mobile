import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';

import {TextWrapper, Label, toCurrency} from './../../components';
import * as CONSTANT from './../../constant';
import * as STRING from './../../string';

export default class DepositStatus extends Component{
  constructor(props) {
      super(props);
      this.state = {
      };
    }

  _getData(){
    let result = {
        labelColor:'transparent',
        labelText:'label',
        labelTextColor:'transparent',
    };

    let {depositStatus, amount} = this.props;

    if(depositStatus == CONSTANT.DEPOSIT_STATUS.CREDIT) { 
      result.labelColor =  'transparent'; 
      result.labelTextColor =  CONSTANT.COLOR.RED_STATUS_TEXT; 
      result.labelText = 'Penarikan '+amount;
    } // biru
    else if (depositStatus == CONSTANT.DEPOSIT_STATUS.DEBIT) {
      result.labelColor =  'transparent'; 
      result.labelTextColor =  CONSTANT.COLOR.GREEN_STATUS_TEXT; 
      result.labelText = 'Penambahan '+amount;
    }
    
    return result;
  }

  render(){
    let {labelColor, labelText, labelTextColor} = this._getData();
    return(
        <Label.TypeA 
          labelText={labelText}
          labelColor={labelColor}
          labelTextColor={labelTextColor}
          containerStyle={{alignItems: 'flex-start', paddingLeft: 0}}
        />
    );
  }
}

DepositStatus.propTypes = {
  //shipmentStatus: PropTypes.oneOf(['AKTIF', 'TUNDA', 'BATAL', 'TELAT', 'SELESAI', 'NONE']),
  statusColor:PropTypes.string,
}

DepositStatus.defaultProps = {
  statusColor:'black',
}

