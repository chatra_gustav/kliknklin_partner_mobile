import React, { Component } from 'react'
import { StyleSheet, View, Dimensions, Image } from 'react-native'

import * as CONSTANT from './../../constant';
import {Scaling, Button, DateFormat} from './../../components';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../actions";

import MonthSelectorCalendar from 'react-native-month-selector'
import moment from "moment"
import ResolveAssetSource from 'resolveAssetSource';

class MonthPicker extends Component {

  constructor(props){
    super(props);
    this.state ={
      selectedMonth: null,
      image:{
        arrow_right_blue:{
            width:0,
            height:0,
            multiplierWidth:0.5,
            multiplierHeight:0.5,
            url:require('app/data/image/attendance/arrow_right_blue.png')
        },
        arrow_left_blue:{
            width:0,
            height:0,
            multiplierWidth:0.5,
            multiplierHeight:0.5,
            url:require('app/data/image/attendance/arrow_left_blue.png')
        }
      },
    }
  }

  componentWillMount(){

    let today = new Date();
    let selectedMonth = '';

    if(this.props.selectedMonth === null){
      let today = new Date();
      selectedMonth = DateFormat(today, 'shortDate2');
      this.setState({ selectedMonth: selectedMonth });
    }else{
      this.setState({ selectedMonth: this.props.selectedMonth });
    }
  }

  componentDidMount(){
    this.getImageSize();
  }

  getImageSize(){
    var image = this.state.image;

    var arrow_right_blue = ResolveAssetSource(this.state.image.arrow_right_blue.url);

    image.arrow_right_blue.width = arrow_right_blue.width * this.state.image.arrow_right_blue.multiplierWidth;
    image.arrow_right_blue.height = arrow_right_blue.height * this.state.image.arrow_right_blue.multiplierHeight;

    var arrow_left_blue = ResolveAssetSource(this.state.image.arrow_left_blue.url);

    image.arrow_left_blue.width = arrow_left_blue.width * this.state.image.arrow_left_blue.multiplierWidth;
    image.arrow_left_blue.height = arrow_left_blue.height * this.state.image.arrow_left_blue.multiplierHeight;
    
    this.setState({image});
  }

  render() {
    console.log('selectedMonth: ',this.state.selectedMonth);
    let {width, height} = Dimensions.get('window');
    return (
      <MonthSelectorCalendar
        containerStyle={styles.container}
        selectedDate={moment(this.state.selectedMonth, 'DD/MM/YYYY', true)}
        onMonthTapped={(date) => this.setState({ month: date }, () => {
          this.onSubmit()
        })}
        swipable={true}
        selectedBackgroundColor={CONSTANT.COLOR.LIGHT_BLUE}
        initialView={moment(this.state.selectedMonth, 'DD/MM/YYYY', true)}
        nextIcon={<View>
            <Image
              style={{width: this.state.image.arrow_right_blue.width, height: this.state.image.arrow_right_blue.height}}
              source={this.state.image.arrow_right_blue.url}
            />
          </View>}
        prevIcon={<View>
            <Image
              style={{width: this.state.image.arrow_left_blue.width, height: this.state.image.arrow_left_blue.height}}
              source={this.state.image.arrow_left_blue.url}
            />
          </View>}
        seperatorColor={CONSTANT.COLOR.LIGHT_GRAY}
        yearTextStyle={{color: CONSTANT.COLOR.BLACK, fontWeight: CONSTANT.TEXT_WEIGHT.MEDIUM.weight, fontFamily: CONSTANT.TEXT_FAMILY.MAIN}}
      />
    );
  }

  onSubmit(){

    let selectedDate;

    if(this.state.month == undefined){

      let today = new Date();
      selectedDate = DateFormat(today, 'isoDate');

    }else{

      selectedDate = this.state.month.format('YYYY-MM-DD');

    }

    console.log('selectedDate: ',selectedDate);

    this.props.onPressButtonPrimary(selectedDate);
  }

}

function mapStateToProps(state, ownProps) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MonthPicker);


const styles = StyleSheet.create({
  container: {
    padding: Scaling.moderateScale(15),
    width: Dimensions.get('window').width * 0.8,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  }
});