import React from 'react';
import {StyleSheet, View, Text, Dimensions, Button} from 'react-native';

export class ErrorBox extends React.Component {

  render() {
    return (
      <View style={styles.container}>
        <View style={{flex: 8,alignItems:'center'}}>
          <Text style={styles.title}>{this.props.errorTitle}</Text>
          <Text style={styles.content}>{this.props.errorContent}</Text>
        </View>
        <View style={{}}>
          <Button
            title={'Tutup'}
            onPress={() => this.props.navigator.dismissLightBox()}
          />
          <Button
            title={'Coba Lagi'}
            onPress={async () => {
              await this.props.navigator.dismissLightBox();

              setTimeout(() => {
                this.props.retryFunction();
              }, 1000);
              
              
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width * 0.7,
    height: Dimensions.get('window').height * 0.3,
    backgroundColor: '#ffffff',
    borderRadius: 5,
    borderColor: "#FAF",
    borderWidth:1,
    padding: 16,
  },
  title: {
    fontSize: 17,
    fontWeight: '700',
  },
  content: {
    marginTop: 8,
  },
});