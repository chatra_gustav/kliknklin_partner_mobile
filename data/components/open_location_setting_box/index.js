import React from 'react';
import {StyleSheet, View, Text, Dimensions, Button} from 'react-native';

export class OpenLocationSettingBox extends React.Component {

  render() {
    return (
      <View style={styles.container}>
        <View style={{flex: 8,alignItems:'center'}}>
          <Text style={styles.title}>{this.props.title}</Text>
          <Text style={styles.content}>{this.props.content}</Text>
        </View>
        <View style={{}}>
          <Button
            title={'Buka Pengaturan'}
            onPress={() => this.props.onPress()}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width * 0.7,
    height: Dimensions.get('window').height * 0.3,
    backgroundColor: '#ffffff',
    borderRadius: 5,
    borderColor: "#FAF",
    borderWidth:1,
    padding: 16,
  },
  title: {
    fontSize: 17,
    fontWeight: '700',
  },
  content: {
    marginTop: 8,
  },
});