import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
  FlatList
} from 'react-native';
import PropTypes from 'prop-types';
import {moderateScale} from 'react-native-size-matters';
import { TextField } from 'react-native-material-textfield';
import ResolveAssetSource from 'resolveAssetSource';

import {TextWrapper, Form} from './../../components';
import * as STRING from './../../string';
import * as CONSTANT from './../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../actions";

class DelayOrder extends Component{
	constructor(props) {
      super(props);
      this.state = {
      		isLoading:true,
      }; 
  	}

  	componentDidMount(){
		setTimeout(() => {
			//this.setState({isLoading:false});
		},1000);
  	}

  	_getDelayOrderReason(){

  	}

	render(){
		let {isLoading} = this.state;

		if (isLoading) {
			return(
				<LoadingContent />
			);
		}

		return(
			<FlatList />
		);
	}
}

class LoadingContent extends Component{
	render(){
		return(
			<View style={styles.container}>
				<View style={styles.contentContainer}>
					<ActivityIndicator size={moderateScale(24)} color={CONSTANT.COLOR.GRAY_B} />
				</View>
			</View>
		);
	}
}

DelayOrder.propTypes = {
  onPress:PropTypes.func
}

DelayOrder.defaultProps = {
  	onPress: () => {},
}

const styles = StyleSheet.create({
	container:{
		flex:1,
		backgroundColor:'transparent',
		alignItems:'center',
		justifyContent:'center',
	},
	contentContainer:{
		width:Dimensions.get('window').width * 0.8,
		height:Dimensions.get('window').height * 0.6,
		backgroundColor:'white',
		alignItems:'center',
		justifyContent:'center',
		opacity:1,
	}
});


function mapStateToProps(state, ownProps) {
	return {
		userRole:state.rootReducer.user.role,
		orderData:state.rootReducer.order.detail,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionUser: bindActionCreators(actions.user, dispatch),
		actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(DelayOrder);

