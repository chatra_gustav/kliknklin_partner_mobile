//function
import DateFormat from './date_format';
import toCurrency from './to_currency';
import {FontScale} from './font_scale';
import {fontMaker} from './font_maker';
import {fontCreator} from './font_creator';
import nthIndexOccurenceString from  './nth_index_occurence_string';
import * as Scaling from './scaling';

//component
import Text from './text';
import TextTicker from './text_ticker';
import Avatar from'./avatar';
import WebView from './webview';

import {TextWrapper} from './text_wrapper';
import {ErrorBox} from './error_box';
import {OpenLocationSettingBox} from './open_location_setting_box';
import {LoadingModal} from './loading_modal';
import {UpdateBox} from './update_box';
import ShipmentScheduleListCourier from './shipment_schedule_list_courier';


import * as Box from './box';
import * as Container from './container';
import * as Button from './button';
import * as CustomView from './custom_view';
import * as Form from './form';
import * as TabBar from './tab_bar';
import * as Label from './label';
import DateRangePicker from './date_range_picker';
import DatePicker from './date_picker';
import MonthPicker from './month_picker';
import WheelPicker from './wheel_picker';

import OrderStatus from './order_status';
import DepositStatus from './deposit_status';
import OrderOption from './order_option';
import OutletSelector from './outlet_selector';
import DelayOrder from './delay_order';
import ConnectionStability from './connection_stability';
import ShipmentStatus from './shipment_status';
import ItemDetail from './item_detail';
import DepositLightBox from './deposit_light_box';
import BluetoothSelection from './bluetooth_selection';
import SolutionSelection from './solution_selection';
import NoActiveOutlet from './no_active_outlet';
import NoDataFound from './no_data_found';
import SelectCustomer from './select_customer';

export {
	//new components
	Text,
	TextTicker,
	Avatar,
	WebView,

	//components
	DateFormat,
	FontScale,
	Form,
	TextWrapper,
	ErrorBox,
	LoadingModal,
	ShipmentScheduleListCourier,
	OpenLocationSettingBox,
	Scaling,
	NoActiveOutlet,
	SolutionSelection,
	NoDataFound,
	SelectCustomer,

	//function
	fontMaker,
	fontCreator,
	toCurrency,
	nthIndexOccurenceString,

	Box,
	Container,
	Button,
	CustomView,
	TabBar,
	Label,
	DateRangePicker,
	DatePicker,
	MonthPicker,
	WheelPicker,
	
	//screen components
	OrderStatus,
	DepositStatus,
	OrderOption,
	OutletSelector,
	DelayOrder,
	ConnectionStability,
	ShipmentStatus,
	ItemDetail,
	DepositLightBox,
	BluetoothSelection,
};