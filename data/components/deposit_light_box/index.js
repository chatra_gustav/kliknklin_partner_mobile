import React, { Component } from 'react'
import { StyleSheet, View, Dimensions, Image, TouchableOpacity, Alert } from 'react-native'

import ResolveAssetSource from 'resolveAssetSource';
import ImagePicker from 'react-native-image-picker';

import * as CONSTANT from 'app/data/constant';
import {Scaling, Button, DateFormat, Form, Text, toCurrency} from 'app/data/components';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

const buktiTransferPlaceholder = 'Unggah bukti transfer';

class DepositLightBox extends Component {

  constructor(props){
    super(props);
    this.state={
      image:{
        edit_icon:{
            width:0,
            height:0,
            multiplierWidth:0.35,
            multiplierHeight:0.35,
            url:require('app/data/image/attendance/edit_icon.png'),
        },
        arrow_right_blue:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('app/data/image/attendance/arrow_right_blue.png'),
        },
        clock_icon:{
          width:0,
          height:0,
          multiplierWidth:0.3,
          multiplierHeight:0.3,
          url:require('app/data/image/deposit/clock_icon.png'),
        },
        info_icon:{
          width:0,
          height:0,
          multiplierWidth:0.3,
          multiplierHeight:0.3,
          url:require('app/data/image/deposit/info_icon.png'),
        },
        wallet_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/deposit/wallet_icon.png'),
        },
        arrow_down_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/iot_machine/arrow_down_icon.png'),
        },
        arrow_up_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/iot_machine/arrow_up_icon.png'),
        },
        clear_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/deposit/clear_icon.png')
        },
        bukti_trf_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/deposit/bukti_trf_icon.png')
        }
      },
      maxWithdrawableBalance: 0,
      balance: '0',
      image_uri: buktiTransferPlaceholder,
      selectedBank: {
        showIndex: 1
      }
    }
  }

  componentDidMount(){
    this.setMaxWithdrawableBalance();
    this.setSelectedBank();
    this.setBalance();
    this.getImageSize();
  }

  setSelectedBank(){
    if(this.props.kliknklinBanks.length > 0){
      this.setState({ selectedBank: this.props.kliknklinBanks[0] })
    }
  }

  setMaxWithdrawableBalance(){
    if(this.props.actiontype == 'withdrawBalance')
      this.setState({ maxWithdrawableBalance : this.props.balance});
  }

  setBalance(){
    this.setState({ balance: this.props.balance });
  }

  getImageSize(){
    var image = this.state.image;

    var arrow_right_blue = ResolveAssetSource(this.state.image.arrow_right_blue.url);

    image.arrow_right_blue.width = arrow_right_blue.width * this.state.image.arrow_right_blue.multiplierWidth;
    image.arrow_right_blue.height = arrow_right_blue.height * this.state.image.arrow_right_blue.multiplierHeight;

    var edit_icon = ResolveAssetSource(this.state.image.edit_icon.url);

    image.edit_icon.width = edit_icon.width * this.state.image.edit_icon.multiplierWidth;
    image.edit_icon.height = edit_icon.height * this.state.image.edit_icon.multiplierHeight;

    var clock_icon = ResolveAssetSource(this.state.image.clock_icon.url);

    image.clock_icon.width = clock_icon.width * this.state.image.clock_icon.multiplierWidth;
    image.clock_icon.height = clock_icon.height * this.state.image.clock_icon.multiplierHeight;

    var info_icon = ResolveAssetSource(this.state.image.info_icon.url);

    image.info_icon.width = info_icon.width * this.state.image.info_icon.multiplierWidth;
    image.info_icon.height = info_icon.height * this.state.image.info_icon.multiplierHeight;

    var wallet_icon = ResolveAssetSource(this.state.image.wallet_icon.url);

    image.wallet_icon.width = wallet_icon.width * this.state.image.wallet_icon.multiplierWidth;
    image.wallet_icon.height = wallet_icon.height * this.state.image.wallet_icon.multiplierHeight;

    var arrow_down_icon = ResolveAssetSource(this.state.image.arrow_down_icon.url);

    image.arrow_down_icon.width = arrow_down_icon.width * this.state.image.arrow_down_icon.multiplierWidth;
    image.arrow_down_icon.height = arrow_down_icon.height * this.state.image.arrow_down_icon.multiplierHeight;

    var arrow_up_icon = ResolveAssetSource(this.state.image.arrow_up_icon.url);

    image.arrow_up_icon.width = arrow_up_icon.width * this.state.image.arrow_up_icon.multiplierWidth;
    image.arrow_up_icon.height = arrow_up_icon.height * this.state.image.arrow_up_icon.multiplierHeight;

    var clear_icon = ResolveAssetSource(this.state.image.clear_icon.url);

    image.clear_icon.width = clear_icon.width * this.state.image.clear_icon.multiplierWidth;
    image.clear_icon.height = clear_icon.height * this.state.image.clear_icon.multiplierHeight;

    var bukti_trf_icon = ResolveAssetSource(this.state.image.bukti_trf_icon.url);

    image.bukti_trf_icon.width = bukti_trf_icon.width * this.state.image.bukti_trf_icon.multiplierWidth;
    image.bukti_trf_icon.height = bukti_trf_icon.height * this.state.image.bukti_trf_icon.multiplierHeight;
    
    this.setState({image});
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.title}>
          <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>{this.props.title}</Text>
        </View>

        <View style={styles.inputField}>
          <Form.InputField.TypeE
            value={this.state.balance}
            keyboardType={'numeric'}
            ref={(ref) => this.balance_textfield = ref}
            onChangeText={this._onChangeBalance.bind(this)}
            placeholder={this.props.placeholder}
            inputContainerStyle={{
              alignItems: 'center'
            }}
            containerStyle={[
              {
                alignItems:'center',
                justifyContent:'center', 
                width:Dimensions.get('window').width * 0.84,
                borderBottomWidth: StyleSheet.hairlineWidth,
                borderColor: CONSTANT.COLOR.LIGHTER_GRAY_FONT,
                paddingBottom: Scaling.moderateScale(20)
              }
            ]}
            inputTextStyle={{fontSize: Scaling.moderateScale(30), textAlign: 'center', height: Scaling.moderateScale(60) }}
          />
        </View>

        <View style={styles.accountLabel}>
          <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>yang akan ditransferkan kepada</Text>
        </View>

        {this.account()}

        {this.buktiTransfer()}

        <View style={styles.button}>
            <Button.TypeD
              buttonText={'Batalkan'}
              buttonColor={CONSTANT.COLOR.LIGHTER_GRAY_FONT}
              buttonTextStyle={[styles.buttonTextStyle]}
              buttonStyle={styles.buttonStyle}
              onPress={this._onCancel.bind(this)}
            />

            <Button.TypeD
              buttonText={'Konfirmasi'}
              buttonColor={CONSTANT.COLOR.GREEN}
              buttonTextStyle={[styles.buttonTextStyle]}
              buttonStyle={styles.buttonStyle}
              onPress={this._onConfirm.bind(this)}
            />
          </View>
      </View>
    );
  }

  buktiTransfer(){
    if(this.props.actiontype == 'addBalance'){
      return(
        <View style={styles.buktiTransfer}>
          <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT, fontSize: Scaling.moderateScale(11)}}>Bukti Transfer</Text>

          <TouchableOpacity style={styles.inputImage} onPress={this.pickImage}>

            <View Style={styles.leftIcon}>
              <Image
                style={{width: this.state.image.bukti_trf_icon.width, height: this.state.image.bukti_trf_icon.height}}
                source={this.state.image.bukti_trf_icon.url}
              />
            </View>

            <View style={styles.image_url_style}>
              <Text numberOfLines={1} fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT, fontSize: Scaling.moderateScale(11)}}>
                {this.state.image_uri}
              </Text>
            </View>

            <TouchableOpacity style={styles.rightIcon} onPress={this.deleteImage} hitSlop={styles.hitSlop}>
              {this.rightIcon()}
            </TouchableOpacity>

          </TouchableOpacity>
        </View>
      )
    }
    return null
  }

  account(){
    if(this.props.actiontype == 'withdrawBalance'){
      return(
        <View style={styles.accountDetail}>

          <View style={styles.icon_bank}>
            <Image
              style={{width: this.state.image.wallet_icon.width, height: this.state.image.wallet_icon.height}}
              source={this.state.image.wallet_icon.url}
            />
          </View>

          <View style={styles.accountDetailOwner}>

            <View style={styles.accountDetailRow}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>{this.props.accountNumber}</Text>
            </View>

            <View style={styles.accountDetailRow}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>a.n. {this.props.accountOwner}</Text>
            </View>

          </View>
        </View>
      )
    }else{
      return(
        <View style={styles.kliknklinBanks}>
          {this.props.kliknklinBanks.map((knkbank, index) => {
            if (this.state.selectedBank.showIndex == knkbank.showIndex) {
              return(
                <TouchableOpacity key={index} style={styles.selectedRow} onPress={ () => this.onPressBank(knkbank) }>
                  {this.bankDetail(knkbank)}
                </TouchableOpacity>
              )
            }else{
              return(
                <TouchableOpacity key={index} style={styles.row} onPress={ () => this.onPressBank(knkbank) }>
                  {this.bankDetail(knkbank)}
                </TouchableOpacity>
              )
            }
          })}
        </View>
      )
    }
  }

  bankDetail(knkbank){
    return(
      <View style={{marginVertical: Scaling.moderateScale(7), flexDirection: 'row', alignItems: 'center', width: '100%'}}>
        <View style={styles.bank_image}>
          <Image
            style={{width: Scaling.moderateScale(120), height: Scaling.moderateScale(40)}}
            source={{uri: knkbank.logo_image_url}}
            resizeMode={'contain'}
          />
        </View>

        <View style={styles.bank_detail}>
          <View style={styles.accountDetailRow}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>{knkbank.account_number}</Text>
          </View>

          <View style={styles.accountDetailRow}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>a.n. {knkbank.account_name}</Text>
          </View>
        </View>
      </View>
    )
  }

  rightIcon(){
    if(this.state.image_uri == buktiTransferPlaceholder){
      return null
    }
    else{
      return(
        <Image
          style={{width: this.state.image.clear_icon.width, height: this.state.image.clear_icon.height}}
          source={this.state.image.clear_icon.url}
        />
      )
    }
  }

  onPressBank = (selectedBank) => {
    this.setState({ selectedBank })
  }

  pickImage = () => {
    ImagePicker.launchImageLibrary({}, async (response)  => {
      if (response.didCancel) {
          console.log('User cancelled image picker');
      }
      else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
      }   
      else {

        let image_uri = response.uri;
        this.setState({ image_uri });
      }
    });
  }

  deleteImage = () => {
    this.setState({ image_uri: buktiTransferPlaceholder })
  }

  _onChangeBalance(balance){
    this.setState({ balance });
  }

  _onConfirm(){
    if(this.props.actiontype == 'withdrawBalance'){
      if(Number(this.state.balance) <= Number(this.state.maxWithdrawableBalance)){
        this.props.onPressButtonPrimary(this.props.actiontype, null, this.state.balance);
      }else if(Number(this.state.balance) < 100000){
        Alert.alert('','Minimal penarikan deposit sebesar Rp 100.000');
      }
      else{
        Alert.alert('','Tidak bisa menarik deposit melebihi '+toCurrency(this.state.maxWithdrawableBalance, 'Rp ')+'\n\nSaldo deposit harus tersisa sebesar Rp 20.000');
      }
    }else{
      if(Number(this.state.balance) < 20000)
        Alert.alert('',"Minimal penambahan deposit adalah sebesar Rp 20.000");
      else if(this.state.image_uri == buktiTransferPlaceholder)
        Alert.alert('',"Bukti transfer belum di unggah");
      else{
        this.props.onPressButtonPrimary(this.props.actiontype, this.state.selectedBank.bank_id, this.state.balance, this.state.image_uri);
      }
    }
  }

  _onCancel(){
    this.props.onPressButtonSecondary();
  }

}

function mapStateToProps(state, ownProps) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DepositLightBox);


const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width * 0.9,
    paddingTop: Scaling.moderateScale(30),
    paddingHorizontal: Dimensions.get('window').width * 0.05,
    borderRadius: 10,
    backgroundColor: CONSTANT.COLOR.WHITE
  },
  buttonTextStyle:{
    color: CONSTANT.COLOR.WHITE, 
    fontSize: Scaling.moderateScale(13), 
    fontFamily: CONSTANT.TEXT_FAMILY.MAIN_SEMI_BOLD
  },
  title:{
    alignItems: 'center'
  },
  inputField:{
    alignItems: 'center',
    marginBottom: Scaling.moderateScale(20)
  },
  accountLabel:{
  },
  accountDetail:{
    width: '100%',
    flexDirection: 'row',
    height: Scaling.moderateScale(140),
    elevation: 1
  },
  accountDetailRow:{
    paddingVertical: Scaling.moderateScale(3),
  },
  buktiTransfer:{
    marginBottom: Scaling.moderateScale(20)
  },
  icon_bank:{
    width: '35%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  accountDetailOwner:{
    width: '65%',
    justifyContent: 'center',
  },
  button:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: Scaling.verticalScale(25)
  },
  buttonStyle:{
    borderRadius: Scaling.moderateScale(10),
    width:Dimensions.get('window').width * 0.5 * 0.75,
    height:CONSTANT.STYLE.BUTTON.SMALL.HEIGHT
  },
  inputImage:{
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: CONSTANT.COLOR.LIGHT_GRAY,
    borderRadius: Scaling.moderateScale(5),
    width:Dimensions.get('window').width * 0.84,
    paddingHorizontal: Scaling.moderateScale(10),
    paddingVertical: Scaling.moderateScale(7),
    marginTop: Scaling.moderateScale(10)
  },
  leftIcon:{
    width: '20%',
    alignItems: 'center'
  },
  image_url_style:{
    paddingLeft: Scaling.moderateScale(10),
    width: '70%',
  },
  rightIcon:{
    width: '30%',
    alignItems: 'center'
  },
  hitSlop: {
    top: 5, 
    bottom: 5, 
    left: 5, 
    right: 5
  },
  selectedRow:{
    borderRadius: Scaling.moderateScale(10),
    borderWidth: 1,
    borderColor: CONSTANT.COLOR.LIGHT_BLUE,
    padding: Scaling.moderateScale(7)
  },
  row:{
    padding: Scaling.moderateScale(7)
  },
  kliknklinBanks: {
    marginTop: Scaling.moderateScale(10),
    marginBottom: Scaling.moderateScale(10)
  },
  bank_image:{
    width: '40%'
  },
  bank_detail:{
    width: '60%'
  }
});