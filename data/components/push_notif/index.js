import React, { Component } from 'react';
import {Platform, AppState} from 'react-native';

import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';

import {PushNotif as PushNotifAPI} from './../../route/api';

export class PushNotif extends Component{
  constructor(props) {
      super(props);
      this.state = {
        data:this.props.data ? this.props.data : [],
        _setData:this.props._setData ? this.props._setData : (()=>{}),
        image:this.props.image ? this.props.image : null,
      }
  }

  _saveRegistrationToken(user, token, setRegistrationToken = () => {}) {
    
  }

  _listenerPushNotif(){
      this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => { 
            this._reloadDashboard();
            // there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
            if(notif.local_notification){
              //buka notif dari foreground
              //console.log(notif);
              console.log('hehe');
              this._receivePushNotif(notif);
              return;
            }
            if(notif.opened_from_tray){
                //buka notif dari background
                //console.log(notif);
                console.log('huhu');
                this._receivePushNotif(notif);
                return;
            }
            //await someAsyncCall();
            
            if(Platform.OS ==='ios'){
              //optional
              //iOS requires developers to call completionHandler to end notification process. If you do not call it your background remote notifications could be throttled, to read more about it see the above documentation link. 
              //This library handles it for you automatically with default behavior (for remote notification, finish with NoData; for WillPresent, finish depend on "show_in_foreground"). However if you want to return different result, follow the following code to override
              //notif._notificationType is available for iOS platfrom
              switch(notif._notificationType){
                case NotificationType.Remote:
                  notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
                  break;
                case NotificationType.NotificationResponse:
                  notif.finish();
                  break;
                case NotificationType.WillPresent:
                  notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
                  break;
              }
            }

            FCM.presentLocalNotification({
                //id: "UNIQ_ID_STRING",                               // (optional for instant notification)
                title: notif.fcm.title,                     // as FCM payload
                body: notif.fcm.body,                    // as FCM payload (required)
                sound: "default",                                   // as FCM payload
                priority: "high",                                   // as FCM payload
                click_action: notif.fcm.action,                             // as FCM payload
                badge: 10,                                          // as FCM payload IOS only, set 0 to clear badges
                number: 10,                                         // Android only
                //ticker: "My Notification Ticker",                   // Android only
                auto_cancel: true,                                  // Android only (default true)
                large_icon: "ic_launcher",                           // Android only
                icon: "ic_launcher",                                // as FCM payload, you can relace this with custom icon you put in mipmap
                //big_text: "bisa diubah juga",     // Android only
                //sub_text: "bisa diubah",                      // Android only
                //color: "red",                                       // Android only
                vibrate: 300,                                       // Android only default: 300, no vibration if you pass null
                //tag: notif.fcm.tag,                                    // Android only
                //group: "group",                                     // Android only
                //my_custom_data:'my_custom_field_value',             // extra data you want to throw
                lights: true,                                       // Android only, LED blinking (default false)
                show_in_foreground: true                               // notification when app is in foreground (local & remote)
            });   
        });
  }

  async _configPushNotif(){
        FCM.requestPermissions(); // for iOS
        
        var data = {};
        data.success = 'nope';
        
        await FCM.getFCMToken().then((token) => {
          console.log(token)
          data.success = 'yeah';
          data.token = token;
          
          //buka notif dari dead apps
          FCM.getInitialNotification().then(notif=>{console.log('haha');this._reloadDashboard();this._receivePushNotif(notif)});
        }); 

        this.refreshTokenListener = await FCM.on(FCMEvent.RefreshToken, (token) => {
            data.success = 'yeah';
            data.token = token;

            FCM.getInitialNotification().then(notif=>{this._reloadDashboard();this._receivePushNotif(notif)});
        });

        this._listenerPushNotif();

        return data;
  }

  _reloadDashboard(){
      if (this.props._setShouldReloadSummary) this.props._setShouldReloadSummary();
      if (this.props._setShouldReloadSummaryIncoming) this.props._setShouldReloadSummaryIncoming();
  }

  _receivePushNotif(notif){
    var route = [ 
                  { id: 'Dashboard', title: 'Dashboard' },
                ];
    
    if (notif.fcm) {
      if (notif.fcm.action == 'incoming') {
        route.push(
          {
            id:'OrderList',
            contentType:'incoming',
            title:'Order Incoming',
          }
        );

        this.props.navigator().immediatelyResetRouteStack(route);
      }
      else if (notif.fcm.action == 'pickup') {
        route.push(
          {
            id:'OrderList',
            contentType:'pickup',
            title:'Order Pickup',
            listType:'important',
          }
        );
        
        this.props.navigator().immediatelyResetRouteStack(route);
      }
      else if (notif.fcm.action == 'onprogress') {
        route.push(
          {
            id:'OrderList',
            contentType:'onprogress',
            title:'Order On Progress',
            listType:'important',
          }
        );
        
        this.props.navigator().immediatelyResetRouteStack(route);
      }
      else if (notif.fcm.action == 'delivery') {
        route.push(
          {
            id:'OrderList',
            contentType:'delivery',
            title:'Order Delivery',
            listType:'important',
          }
        );
        
        this.props.navigator().immediatelyResetRouteStack(route);
      }
    }
    
  }

  _removeListenerPushNotif(){
        if (this.notificationListener && this.refreshTokenListener) {
          this.notificationListener.remove();
          this.refreshTokenListener.remove();
        }
  }

  render(){
    return null;
  }
}