import React, { Component } from 'react';
import {Modal, StyleSheet, View, Dimensions, TouchableOpacity, FlatList, ActivityIndicator, TouchableHighlight} from 'react-native';
import { LargeList } from "react-native-largelist-v3";
import {moderateScale} from 'react-native-size-matters';

import {Text, Scaling} from 'app/data/components';
import * as CONSTANT from 'app/data/constant';


//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";


class BluetoothSelection extends Component {
  static defaultProps = {
    data: [],
  }
  constructor(props) {
    super(props);
  
    this.state = {
      currentKey:null,
      currentInfo:'',
      extractedItem:[],
    };
  }

  renderContent(){
    let data = this.props.data;
    if (data == null) {
      return(
          <View
            style={{
              flex:1,
              alignItems:'center',
              justifyContent:'center',
            }}
          >
            <LoadingContent/>
          </View>
      );
    }
    return(
      <View
        style={{
          flex:1
        }}
      >
        <View style={styles.title}>
          <Text 
            fontOption={CONSTANT.TEXT_OPTION.HEADER}
            textStyle={{color: CONSTANT.COLOR.BLACK}}>
            Bluetooth Devices
          </Text>
        </View>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={this.props.data}
          keyExtractor={(item, index) => index+''}
          renderItem={(item) => { return (
            <View style={{alignItems: 'center'}}>
              <Bluetooth
                item={item.item}
                selectBluetooth={this.selectBluetooth.bind(this)}
              />
            </View>
          );}}
        /> 
      </View>
    );
  }

  selectBluetooth(bluetooth){
    this.props.onPressButtonPrimary(bluetooth);
  }

  render() {
    return (
      <TouchableHighlight
        hitSlop={{
          bottom:Dimensions.get('window').height * 0.5,
        }}

        underlayColor={'transparent'}
        style={[styles.container, this.props.containerStyle]}
        onPress={() =>{
            this.props.actionNavigator.dismissLightBox();
            this.props.actionNavigator.dismissModal();
        }}
      >
        <TouchableHighlight underlayColor={'transparent'} style={styles.contentContainer}>
            {this.renderContent()}
        </TouchableHighlight>
      </TouchableHighlight>
    );
  }
}

BluetoothSelection.defaultProps = {
    containerStyle: {},
    data:{
      titleText: 'title',
    },
}

class LoadingContent extends Component{
  render(){
    return(         
       <ActivityIndicator size={moderateScale(24)} color={CONSTANT.COLOR.GRAY_B} /> 
    );
  }
}

class Bluetooth extends Component{
  render(){
    return(
      <TouchableOpacity style={styles.customer} onPress={this.selectBluetooth}>
        <Text 
          fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}
          textStyle={{color: CONSTANT.COLOR.BLACK}}>{this.props.item.info}
        </Text>
      </TouchableOpacity>
    )
  }

  selectBluetooth = () => {
    this.props.selectBluetooth(this.props.item)
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width:'100%',
    height:'100%',
    alignItems:'center',
    justifyContent:'flex-end'
  },
  contentContainer:{
    width:'100%',
    height:'50%',
    backgroundColor:'white',
    zIndex:10, 
  },
  title:{
    padding: Scaling.moderateScale(10),
    marginBottom: Scaling.moderateScale(15),
    alignItems: 'center'
  },
  customer:{
    padding: Scaling.moderateScale(10), 
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'center',
    width: '100%'
  }
});



function mapStateToProps(state, ownProps) {
  return {
    delayOrderReasonList:state.rootReducer.masterData.delayOrderReasonList,
    rejectOrderReasonList:state.rootReducer.masterData.rejectOrderReasonList,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionMasterData: bindActionCreators(actions.masterData, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BluetoothSelection);
