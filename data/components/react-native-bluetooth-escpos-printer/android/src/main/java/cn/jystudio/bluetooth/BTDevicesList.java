package cn.jystudio.bluetooth;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class BTDevicesList extends ListActivity {
    private final String TAG = "BTDeviceList";

    public static final int REQUEST_CONNECT_BT = 0x2300;
    public static final int  REQUEST_CODE_LOC = 0x2000;

    private static final int REQUEST_ENABLE_BT = 0x1000;
    private static BluetoothAdapter mBluetoothAdapter = null;
    private static ArrayAdapter<String> mArrayAdapter = null;
    private static ArrayAdapter<BluetoothDevice> btDevices = null;
    private static BluetoothSocket mbtSocket = null;

    public BTDevicesList() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            if (initDevicesList() != 0) {
                this.finish();
                return;
            }

        } catch (Exception ex) {
            this.finish();
            return;
        }

        IntentFilter btIntentFilter = new IntentFilter(
                BluetoothDevice.ACTION_FOUND);
        registerReceiver(discoverReceiver, btIntentFilter);

    }

    private void flushData() {
        try {
            if (mBluetoothAdapter != null) {
                mBluetoothAdapter.cancelDiscovery();
            }

            if (btDevices != null) {
                btDevices.clear();
                btDevices = null;
            }

            if (mArrayAdapter != null) {
                mArrayAdapter.clear();
                mArrayAdapter.notifyDataSetChanged();
                mArrayAdapter.notifyDataSetInvalidated();
                mArrayAdapter = null;
            }

            finalize();

        } catch (Exception ex) {
        } catch (Throwable e) {
        }

    }

    private int initDevicesList() {

        flushData();

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(getApplicationContext(),
                    "Bluetooth not supported!!", Toast.LENGTH_LONG).show();
            return -1;
        }

        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
        }

        mArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView textView = (TextView) super.getView(position, convertView, parent);
                textView.setTextColor(Color.BLACK);

                return textView;
            }
        };

        setListAdapter(mArrayAdapter);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            accessLocationPermission();
        }else{
            getBluetoothDevice();
        }

        return 0;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void accessLocationPermission() {
        int accessCoarseLocation = checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int accessFineLocation   = checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION);

        List<String> listRequestPermission = new ArrayList<String>();

        if (accessCoarseLocation != PackageManager.PERMISSION_GRANTED) {
            listRequestPermission.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (accessFineLocation != PackageManager.PERMISSION_GRANTED) {
            listRequestPermission.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (!listRequestPermission.isEmpty()) {
            String[] strRequestPermission = listRequestPermission.toArray(new String[listRequestPermission.size()]);
            requestPermissions(strRequestPermission, REQUEST_CODE_LOC);
        }

        if(listRequestPermission.isEmpty()) {
            getBluetoothDevice();
        }
    }

    private void getBluetoothDevice(){
        Intent enableBtIntent = new Intent(
                BluetoothAdapter.ACTION_REQUEST_ENABLE);
        try {
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } catch (Exception ex) {

        }
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent intent) {
        super.onActivityResult(reqCode, resultCode, intent);
        Log.d(TAG,"reqCode: "+reqCode);
        switch (reqCode) {

            case REQUEST_ENABLE_BT:
                Log.d(TAG,"case REQUEST_ENABLE_BT");
                if (resultCode == RESULT_OK) {
                    Log.d(TAG,"resultCode == RESULT_OK");
                    Set<BluetoothDevice> btDeviceList = mBluetoothAdapter
                            .getBondedDevices();
                    try {
                        if (btDeviceList.size() > 0) {
                            Log.d(TAG,"btDeviceList.size() > 0");
                            for (BluetoothDevice device : btDeviceList) {
                                if (btDeviceList.contains(device) == false) {

                                    btDevices.add(device);

                                    mArrayAdapter.add(device.getName() + "\n"
                                            + device.getAddress());
                                    mArrayAdapter.notifyDataSetInvalidated();
                                }
                            }
                        }else{
                            Log.d(TAG,"btDeviceList.size() <= 0");
                        }
                    } catch (Exception ex) {

                    }
                }else{
                    finish();
                }
                break;
        }

        mBluetoothAdapter.startDiscovery();

    }

    private final BroadcastReceiver discoverReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "on receive:" + action);
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                try {
                    if (btDevices == null) {
                        btDevices = new ArrayAdapter<BluetoothDevice>(
                                getApplicationContext(), android.R.layout.simple_list_item_1);
                    }

                    if (btDevices.getPosition(device) < 0) { // belum ada di list
                        btDevices.add(device);
                        mArrayAdapter.add(device.getName() + "\n"
                                + device.getAddress() + "\n" );
                        mArrayAdapter.notifyDataSetInvalidated();
                    }
                } catch (Exception ex) {
                    //ignore
                }
            }
        }
    };

    public static BluetoothSocket getSocket() {
        return mbtSocket;
    }

    @Override
    protected void onListItemClick(ListView l, View v, final int position, long id) {
        super.onListItemClick(l, v, position, id);

        if (mBluetoothAdapter == null) {
            return;
        }

        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
        }

        BluetoothDevice selectedDevice = btDevices.getItem(position);

        Toast.makeText(
                getApplicationContext(),
                "Connect to " + selectedDevice.getName(),
                Toast.LENGTH_SHORT).show();



        Intent resultIntent = new Intent();
        resultIntent.putExtra("device", selectedDevice);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

}
