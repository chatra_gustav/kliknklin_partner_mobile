export const HOST_PRODUCTION = 'https://partners.kliknklin.co';

export const HOST_DEVELOPMENT = 'http://pospartner.semeru.kliknklin.co'

export const API_PRODUCTION = HOST_PRODUCTION + '/api/v1';

export const API_DEVELOPMENT = HOST_DEVELOPMENT + '/api/v1';

export const DEPOSIT_PRODUCTION = HOST_PRODUCTION + '/mobile/balance/';

export const DEPOSIT_DEVELOPMENT = HOST_DEVELOPMENT +'/mobile/balance/';

export const UPDATE_APP_ANDROID= 'market://details?id=com.kliknklin.knklaundrymitra&hl=in';

export const SIGN_UP = 'https://partners.kliknklin.co';

export const TERMS_AND_CONDITION_CUSTOMER = 'https://www.kliknklin.com/bahasa-terms-and-conditions';

export const STANDARD_OPERATING_PROCEDURE = 'https://www.kliknklin.com/sop-partner';

export const AGREEMENT = 'https://www.kliknklin.com/partner-agreement';

export const FAQ = 'https://www.kliknklin.com/partner-faq';

export const USER_GUIDE = 'https://www.kliknklin.com/partner-guideline';

export const MOBILE_WEB_DEVELOPMENT = HOST_DEVELOPMENT + '/mobileweb';

export const MOBILE_WEB_PRODUCTION = HOST_PRODUCTION + '/mobileweb';



export const API = API_PRODUCTION;
//export const API = API_DEVELOPMENT;

//export const DEPOSIT = DEPOSIT_DEVELOPMENT;
export const DEPOSIT = DEPOSIT_PRODUCTION;

//export const MOBILE_WEB = MOBILE_WEB_DEVELOPMENT;
export const MOBILE_WEB = MOBILE_WEB_PRODUCTION;