export const BLUE = '#498BC9';
export const LIGHT_BLUE = '#3EA8FE';
export const LIGHTER_BLUE = '#dbe6f5';
export const BLUE_STATUS_TEXT = '#3DA8FD';
export const BLUE_STATUS_BACKGROUND = '#C8EDFF';
//light,lighter blue
//dark,darker blue

export const BROWN = "#b19577";

export const WHITE = '#FFF';

export const DARKEST_GRAY = "#979D9B";
export const DARKER_GRAY = "#696969";
export const DARK_GRAY = "#8f8f94";
export const GRAY = "#C2C1C1";
export const LIGHT_GRAY = "rgba(241,242,242,1)"; //D3D3D3
export const LIGHTER_GRAY = "#F5FCFF";
export const LIGHTER_GRAY_FONT = "#C0C0C0";
export const LIGHTEST_GRAY = "#F1F1F2";

export const GRAY_STATUS_TEXT = '#B3B3B3';
export const GRAY_STATUS_BACKGROUND = '#F2F2F2';

export const BLACK = 'black';
export const LIGHT_BLACK = '#282828';

export const GREEN = '#07A255';
export const GREEN_STATUS_TEXT = '#02A155';
export const GREEN_STATUS_BACKGROUND = '#CDFFC8';

export const PURPLE = '#231F20';

export const ORANGE = '#F8951D';
export const ORANGE_STATUS_TEXT = '#F7941D';
export const ORANGE_STATUS_BACKGROUND = '#FFEBC8';

export const RED = '#EF4136';
export const LIGHT_RED = '#E28680';
export const RED_STATUS_TEXT = '#EE4035';
export const RED_STATUS_BACKGROUND = '#FFDADD';

//bawah obsolete, ga diapus dulu tar error banyak

export const BLUE_A = '#61CDFF';
export const BLUE_B = '#499ABF'

export const GREEN_A = '#AEC93A';

export const GRAY_A = '#F5F5F5';
export const GRAY_B = 'lightgray';
export const GRAY_C = '#9B9C9B';
export const GRAY_D = '#C2C1C1';

export const RED_A = '#BF2000';
export const RED_B = '#ff0033';

export const YELLOW_A = '#FFC300';

export const BLACK_A = '#000000';

export const WHITE_A = '#FFF';