export const WAIT_TO_START = 1;
export const IN_PROGRESS = 2;
export const DELAY_CUSTOMER = 3;
export const DELAY_PARTNER = 8;
export const DONE = 4;
export const CANCEL = 5;
export const LATE = 6;
export const ACTIVE = 7;