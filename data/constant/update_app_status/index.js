export const FORCE_UPDATE = 'force_update';
export const NEW_VERSION_AVAILABLE = 'new_version_available'
export const LATEST = 'latest';
export const NO_RESPONSE = 'no_response';