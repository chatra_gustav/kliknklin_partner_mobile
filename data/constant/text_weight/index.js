export const BLACK = {
	name: 'Black',
	weight: '900',
};
export const EXTRA_BOLD = {
	name: 'ExtraBold',
	weight: '800',
};;
export const BOLD = {
	name: 'Bold',
	weight: '700',
};
export const SEMI_BOLD = {
	name: 'SemiBold',
	weight: '600',
};
export const MEDIUM = {
	name: 'Medium',
	weight: '500',
};
export const REGULAR = {
	name: 'Regular',
	weight: '400',
};
export const LIGHT = {
	name: 'Light',
	weight: '300',
};
export const EXTRA_LIGHT = {
	name: 'ExtraLight',
	weight: '200',
};
export const THIN = {
	name: 'Thin',
	weight: '100',
};