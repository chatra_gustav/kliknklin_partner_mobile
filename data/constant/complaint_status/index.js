export const APPLIED = 1;
export const REJECTED = 2;
export const IN_PROGRESS = 3;
export const SOLVED = 4;
export const DONE = 5;
export const NEED_ESCALATION = 6;
export const CANCEL = 7;