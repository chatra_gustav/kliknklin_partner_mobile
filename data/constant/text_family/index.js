export const MAIN = 'Montserrat';
export const MAIN_SEMI_BOLD = 'Montserrat-SemiBold';
export const MAIN_BOLD = 'Montserrat-Bold';
export const MAIN_LIGHT = 'Montserrat-Light';
export const SECONDARY = 'Ubuntu';