export const WAIT_SOLUTION = 1;
export const SOLUTION_APPLIED = 2;
export const IN_PROGRESS = 3;
export const SOLVED =  4;
export const NEED_ESCALATION = 5;
export const DONE = 6;
export const REJECTED = 9;