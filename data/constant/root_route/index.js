export const LOGIN = 'login';
export const SMS_VERIFICATION = 'sms-verification';
export const BLANK_PAGE = 'blank-page';
export const AFTER_LOGIN = 'after-login';
export const AFTER_LOGIN_COURIER = 'after-login-courier';
export const AFTER_LOGIN_OWNER = 'after-login-owner';
export const AFTER_LOGIN_ADMIN_OUTLET = 'after-login-admin-outlet';