export const WAIT_OUTLET_CONFIRMATION = 11;
export const WAIT_PICKUP = 1;
export const IN_PROGRESS_PICKUP = 12;
export const DONE_PICKUP = 14;
export const IN_PROGRESS_LAUNDRY = 3;
export const DONE_LAUNDRY = 4;
export const IN_PROGRESS_DELIVERY = 13;
export const DONE_DELIVERY = 5;
export const DONE_ORDER = 9;
export const CANCEL = 6;
export const APPLIED_COMPLAIN = 7;
export const CANNOT_FIND_PARTNER = 10;
export const AFTER_SUBMIT_POS = 15;

//obsole
export const CHECKOUT = 2;