export const KLIKNKLIN_ICON = require('app/data/image/icon/kliknklin_icon.png');
export const BACK_BUTTON_ICON = require('app/data/image/icon/back_icon.png');
export const PRINT_ICON = require('app/data/image/icon/print_icon.png');
export const AUTO_SCROLL_ICON = require('app/data/image/icon/auto_scroll_icon.png');
export const RIGHT_ARROW_ICON = require('app/data/image/icon/right_arrow_icon.png');
export const BOTTOM_ARROW_ICON = require('app/data/image/icon/bottom_arrow_icon.png');
export const LEFT_ARROW_ICON = require('app/data/image/icon/left_arrow_icon.png');
export const EDIT_ICON = require('app/data/image/icon/edit_icon.png');
export const NO_OUTLET_ICON = require('app/data/image/icon/no_outlet_icon.png');
export const SEARCH_ICON = require('app/data/image/icon/search_icon.png');
export const CANCEL_ICON = require('app/data/image/icon/cancel_icon.png');
export const OPTION_ICON = require('app/data/image/icon/option_icon.png');
export const NOTE_ICON = require('app/data/image/icon/note_icon.png');
export const NO_ITEM_ICON = require('app/data/image/icon/no_item_icon.png');
export const PAYMENT_ICON = require('app/data/image/icon/payment_icon.png');
export const PLUS_ICON = require('app/data/image/icon/plus_icon.png');
export const MINUS_ICON = require('app/data/image/icon/minus_icon.png');
export const NOTIFICATION_ICON = require('app/data/image/icon/notification_icon.png');
export const HISTORY_ICON = require('app/data/image/icon/history_icon.png');
export const MENU_BURGER_ICON = require('app/data/image/icon/menu_burger_icon.png');
export const CUSTOMER_ICON = require('app/data/image/icon/customer_icon.png');
export const PHONE_ICON_2 = require('app/data/image/icon/phone_icon_2.png');
export const EMAIL_ICON_2 = require('app/data/image/icon/email_icon_2.png');
export const IMPORTANT_ICON = require('app/data/image/icon/important_icon.png');
export const HOME_TAB_ICON = require('app/data/image/icon/home_tab_icon.png');
export const HELP_TAB_ICON = require('app/data/image/icon/help_tab_icon.png');
export const POS_TAB_ICON = require('app/data/image/icon/pos_tab_icon.png');
export const SETTING_TAB_ICON = require('app/data/image/icon/setting_tab_icon.png');
export const UTILITY_TAB_ICON = require('app/data/image/icon/utility_tab_icon.png');
export const HOME_TAB_SELECTED_ICON = require('app/data/image/icon/home_tab_selected_icon.png');
export const HELP_TAB_SELECTED_ICON = require('app/data/image/icon/help_tab_selected_icon.png');
export const POS_TAB_SELECTED_ICON = require('app/data/image/icon/pos_tab_selected_icon.png');
export const SETTING_TAB_SELECTED_ICON = require('app/data/image/icon/setting_tab_selected_icon.png');
export const UTILITY_TAB_SELECTED_ICON = require('app/data/image/icon/utility_tab_selected_icon.png');



export const DEFAULT_AVATAR = require('app/data/image/icon/avatar_icon.png');

export const TROLLEY_ORDER = require('app/data/image/background/trolley_order.png');
export const PAID_ORDER = require('app/data/image/background/paid_order.png');
export const TAKEN_ORDER = require('app/data/image/background/taken_order.png');
export const NO_ORDER_FOUND = require('app/data/image/background/no_order_found.png');