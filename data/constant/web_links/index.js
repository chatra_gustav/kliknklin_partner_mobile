
export const FAQ = {
	id:7,
	title: 'Pertanyaan Umum',
	type:'webview',
}

export const AGREEMENT = {
	id:8,
	title: 'Perjanjian Kemitraan',
	type:'webview',
}

export const USER_GUIDE = {
	id:9,
	title: 'Panduan Pengguna',
	type:'webview',
}

export const STANDARD_OPERATING_PROCEDURE_PICKUP_DELIVERY = {
	id:12,
	title: 'Standar Operasi Pengambilan dan Pengantaran',
	type:'webview',
}

export const TNC_DEPOSIT_PARTNER = {
	id:14,
	title: 'Syarat dan Ketentuan Deposit',
	type:'webview',
}

export const STANDARD_OPERATING_PROCEDURE_PROBLEM = {
	id:11,
	title: 'Standar Operasi Masalah',
	type:'webview',
}

export const STANDARD_OPERATING_PROCEDURE_ORDER = {
	id:10,
	title: 'Standar Operasi Pesanan',
	type:'webview',
}

export const YOUTUBE_TUTORIAL = {
	id:6,
}

export const FEEDBACK = {
	title: 'Kritik dan Saran',
	route:'feedback',
	type:'push',
}

export const CONTACT_US = {
	title: 'Hubungi Kami',
	route:'contact-us',
	type:'push',
}

export const REGISTRATION_PRENEUR = {
	id:1,
	title: 'Registrasi LaundryPreneur',
	type:'webview',
}

export const REGISTRATION_PARTNER = {
	id:2,
	title: 'Registrasi Mitra Kliknklin',
	type:'webview',
}

export const REGISTRATION_INVESTOR = {
	id:3,
	title: 'Registrasi Investor Laundryklin',
	type:'webview',
}

export const PARTNER_KLIKNKLIN = {
	id:4,
	title: 'Mitra Belajar',
	type:'webview',
}

