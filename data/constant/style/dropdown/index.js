import { PixelRatio, Dimensions } from 'react-native';
import * as CONSTANT from './../../../constant';

//rata2 margin begini, tapi ada yang beda dibeberapa container
export const MAIN_DASHBOARD = {
	//
	PADDING_LEFT:23.99,
	PADDING_RIGHT:23.99,
	HEIGHT:66.66,
	BORDER_TOP_WIDTH:0.5,
	BACKGROUND_COLOR:CONSTANT.COLOR.WHITE,

	//
	LOGO_CONTAINER_WIDTH:47.99,

	//
	OUTLET_SELECTOR_CONTAINER_WIDTH:287.99,

	//
	FIRST_ICON_CONTAINER_WIDTH:47.99,

	//
	SECOND_ICON_CONTAINER_WIDTH:47.99,
};