import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

const BASE_HEIGHT = 11.1;
const BASE_WIDTH = 22.2;
const BASE_RADIUS = 6.66;

export const SMALL = {
	HEIGHT : BASE_HEIGHT * 3,
	WIDTH : BASE_WIDTH * 3,
	RADIUS: BASE_RADIUS * 3 / 2,
};

export const MEDIUM = {
	HEIGHT : BASE_HEIGHT * 3,
	WIDTH : BASE_WIDTH * 6,
	RADIUS: 12,
};

export const EXTRA_MEDIUM = {
	HEIGHT : BASE_HEIGHT * 3,
	WIDTH : BASE_WIDTH * 8,
	RADIUS: 12,
};

export const THICKER_EXTRA_MEDIUM = {
	HEIGHT : BASE_HEIGHT * 4,
	WIDTH : BASE_WIDTH * 8.75,
	RADIUS: 12,
};

export const LARGE = {
	HEIGHT : BASE_HEIGHT * 5,
	WIDTH : BASE_WIDTH * 16,
	RADIUS: 12,
};
export const EXTRA_LARGE = {
	HEIGHT : BASE_HEIGHT * 6,
	WIDTH : BASE_WIDTH * 19,
	RADIUS: 16,
};