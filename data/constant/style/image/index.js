import { PixelRatio } from 'react-native';

export const ICON = {
	HEIGHT : 27,
	WIDTH : 27,
};

export const INFORMATION ={
	HEIGHT: 168.72,
	WIDTH: 177.6,
	RADIUS: 7,
}

export const NAVBAR_ICON = {
	HEIGHT: 28,
	WIDTH: 28,
};