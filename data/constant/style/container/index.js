import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { Dimensions,} from 'react-native';
import * as CONSTANT from './../../../constant';

//rata2 margin begini, tapi ada yang beda dibeberapa container
//ga boleh gunain custom component disini
export const BASE = {
	MARGIN_LEFT:23.976,
	MARGIN_RIGHT:23.976,
	MARGIN_TOP:33.3,
	MARGIN_BOTTOM:33.3,
	BACKGROUND_COLOR:CONSTANT.COLOR.LIGHTER_GRAY,
};

export const BOX_DASHBOARD = {
	MARGIN_LEFT:75.6,
	MARGIN_RIGHT:75.6,
	MARGIN_TOP:0,
	PADDING_TOP:13.32,
	MARGIN_BOTTOM:BASE.MARGIN_BOTTOM,
	HEIGHT:204.24,
	BACKGROUND_COLOR:CONSTANT.COLOR.WHITE,
};

export const ATTENDANCE_DASHBOARD = {
	MARGIN_LEFT:moderateScale(15),
	MARGIN_RIGHT:moderateScale(15),
	MARGIN_TOP:BASE.MARGIN_TOP,
	MARGIN_BOTTOM:BASE.MARGIN_BOTTOM,
	WIDTH:moderateScale(974),
	HEIGHT:moderateScale(168),
}

export const INFO_DASHBOARD = {
	MARGIN_LEFT:moderateScale(0),
	MARGIN_RIGHT:moderateScale(0),
	MARGIN_TOP:moderateScale(30),
	MARGIN_BOTTOM:moderateScale(0),
	WIDTH:moderateScale(974),
	HEIGHT:moderateScale(168),
}