import { PixelRatio } from 'react-native';

export const TITLE = 46; //page title
export const HEADER = '14@ms1.25'; //head 1
export const SUB_HEADER = '14@ms1.2';
export const CONTENT = '20@ms0.8';
export const SUB_CONTENT = '20@ms0.8';
export const LINK = 28;
export const ICON = '12@ms1'; //baca react native size matter for more info, di local untuk ubah base screen size

//fadhli size
export const CONTENT_SMALLER = '11@ms0.5';