import { Navigation } from 'react-native-navigation';

//list of screen
import {
  SignIn, 
  ForgetPassword, 
  SMSVerification,
  HelpList,
  HelpContent,
  Setting,
  ChangePassword,
  ChangeLocation,
  SearchList,
  NotificationList,
  Invoice,
  OrderList,
  Attendance,
  AttendanceStatistics,
  AttendanceDetail,
  SignUp,
  ShipmentSchedule,
  OrderContent,
  Utility,
  ConfirmItem,
  ItemDetail,
  CheckoutItem,
  BlankPage,
  Deposit,
  Cashflow,
  InternetOfThings,
  IotMachine,
  IotMachineDetail,
  IotMachineSetting,
  Membership,
  OrderMembership,
  OrderMembershipSuccess,
  CreateMembership,
  EditMembership,
  SearchOrder,
  Help,
  Dashboard,
  DashboardAdminOutlet,
  CheckoutAdminOutlet,
  WebViewScreen,
  PosOldVersion,
  SearchTransaction,
  PaymentPosOldVersion,
  Expenses,
  CreateExpenditure,

  //pos
  POSItemSelection,
  POSCheckout,
  POSPayment,
  DashboardCourier,
  CheckoutCourier,
} from './screens';

//components
import {SelectCustomer, ErrorBox, WebView, UpdateBox, LoadingModal, OpenLocationSettingBox, Box, DelayOrder, DateRangePicker, MonthPicker, DepositLightBox, BluetoothSelection, DatePicker, WheelPicker, SolutionSelection} from './components';
import * as CONSTANT from './constant';


export function registerScreens(store, Provider) {
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.SIGN_IN, () => SignIn, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.SIGN_UP, () => SignUp, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.FORGET_PASSWORD, () => ForgetPassword, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.HELP_LIST, () => HelpList, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.HELP_CONTENT, () => HelpContent, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.SEARCH_LIST, () => SearchList, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.NOTIFICATION_LIST, () => NotificationList, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.INVOICE, () => Invoice, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.SETTING, () => Setting, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.CHANGE_PASSWORD, () => ChangePassword, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.CHANGE_LOCATION, () => ChangeLocation, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.ORDER_LIST, () => OrderList, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.ATTENDANCE, () => Attendance, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.ATTENDANCE_STATISTICS, () => AttendanceStatistics, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.ATTENDANCE_DETAIL, () => AttendanceDetail, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.SHIPMENT_SCHEDULE, () => ShipmentSchedule, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.ORDER_CONTENT, () => OrderContent, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.UTILITY, () => Utility, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.CONFIRM_ITEM, () => ConfirmItem, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.ITEM_DETAIL, () => ItemDetail, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.CHECKOUT_ITEM, () => CheckoutItem, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.BLANK_PAGE, () => BlankPage, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.DEPOSIT, () => Deposit, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.CASHFLOW, () => Cashflow, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.IOT_MACHINE, () => IotMachine, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.IOT_MACHINE_DETAIL, () => IotMachineDetail, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.IOT_MACHINE_SETTING, () => IotMachineSetting, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.MEMBERSHIP, () => Membership, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.ORDER_MEMBERSHIP, () => OrderMembership, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.ORDER_MEMBERSHIP_SUCCESS, () => OrderMembershipSuccess, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.CREATE_MEMBERSHIP, () => CreateMembership, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.EDIT_MEMBERSHIP, () => EditMembership, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.SEARCH_ORDER, () => SearchOrder, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.HELP, () => Help, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.WEBVIEW, () => WebViewScreen, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.POS_OLD_VERSION, () => PosOldVersion, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.EXPENSES, () => Expenses, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.CreateExpenditure', () => CreateExpenditure, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.SearchTransaction', () => SearchTransaction, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.PaymentPosOldVersion', () => PaymentPosOldVersion, store, Provider);

  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.DASHBOARD, () => Dashboard, store, Provider);  
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.IOT, () => InternetOfThings, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.DashboardAdminOutlet', () => DashboardAdminOutlet, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.CheckoutAdminOutlet', () => CheckoutAdminOutlet, store, Provider);

  //pos
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.POS_ITEM_SELECTION, () => POSItemSelection, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.POS_CHECKOUT, () => POSCheckout, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.POS_PAYMENT, () => POSPayment, store, Provider);

  Navigation.registerComponent('PartnerKliknKlin.DashboardCourier', () => DashboardCourier, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.CheckoutCourier', () => CheckoutCourier, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.UtilityCourier', () => UtilityCourier, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.SMSVerification', () => SMSVerification, store, Provider);

  //components
  Navigation.registerComponent('PartnerKliknKlin.SelectCustomer', () => SelectCustomer, store, Provider)
  Navigation.registerComponent('PartnerKliknKlin.WebView', () => WebView, store, Provider)
  Navigation.registerComponent('PartnerKliknKlin.DateRangePicker', () => DateRangePicker, store, Provider)
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.DATE_PICKER, () => DatePicker, store, Provider)
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.WHEEL_PICKER, () => WheelPicker, store, Provider)
  Navigation.registerComponent('PartnerKliknKlin.MonthPicker', () => MonthPicker, store, Provider)
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.BOX_ITEM_SELECTION_DETAILED, () => Box.ItemSelectionDetailed, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.BOX_SEARCH_CUSTOMER, () => Box.SearchCustomer, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.BOX_ORDER_CREATED, () => Box.OrderCreated, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.BOX_SINGLE_INPUT, () => Box.SingleInput, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.BoxComplaintItemDetail', () => Box.ComplaintItemDetail, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.BoxItemDetail', () => Box.ItemDetail, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.BoxPosOldItemDetail', () => Box.PosOldItemDetail, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.BoxTypeA', () => Box.TypeA, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.BoxTypeB', () => Box.TypeB, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.BoxTypeC', () => Box.TypeC, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.BoxTypeD', () => Box.TypeD, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.BoxTypeE', () => Box.TypeE, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.BoxTypeImage', () => Box.TypeImage, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.BoxTypeText', () => Box.TypeText, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.ItemSelection', () => Box.ItemSelection, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.ItemSelectionColumnBar', () => Box.ItemSelectionColumnBar, store, Provider);
  Navigation.registerComponent("PartnerKliknKlin.BoxMembershipTnC", () => Box.MembershipTnC, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.ErrorBox', () => ErrorBox, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.UpdateBox', () => UpdateBox, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.LoadingModal', () => LoadingModal, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.OpenLocationSettingBox', () => OpenLocationSettingBox, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.DelayOrder', () => DelayOrder, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.BluetoothSelection', () => BluetoothSelection, store, Provider);
  Navigation.registerComponent('PartnerKliknKlin.SolutionSelection', () => SolutionSelection, store, Provider);
  Navigation.registerComponent(CONSTANT.ROUTE_TYPE.DEPOSIT_LIGHT_BOX, () => DepositLightBox, store, Provider);
}
