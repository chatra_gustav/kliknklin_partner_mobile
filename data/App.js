import React, { Component } from 'react';
import {
 Platform,
 AppRegistry,
 BackHandler,
} from 'react-native';
import {createStore, applyMiddleware, combineReducers} from "redux";
import {Provider} from "react-redux";
import { Navigation, NativeEventsReceiver, ScreenVisibilityListener } from 'react-native-navigation';
import {registerScreens} from './registerScreens';
import * as reducers from "./reducers";
import * as Actions from "./actions";
import * as CONSTANT from './constant';
import thunk from "redux-thunk";
import type { RemoteMessage } from 'react-native-firebase';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const reducer = combineReducers(reducers);
const store = createStoreWithMiddleware(reducer);

registerScreens(store, Provider);

AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', (message: RemoteMessage) => {
    // handle your message
    console.log(message, 'dari app.js')
    return Promise.resolve();
}); // <-- Add this line

export default class App extends Component {

  constructor(props) {
    super(props);
    console.log('-- constructor app')
    this.listenerScreen = new ScreenVisibilityListener({
      backPress: ({screen, startTime, endTime, commandType}) => {
        console.log('screenVisibility', `Screen ${screen} displayed in ${endTime - startTime} millis after [${commandType}]`);
      }
    });
    this.listenerScreen.register();
    store.subscribe(this.onStoreUpdate.bind(this));
    store.dispatch(Actions.root.appInitialized());
  }

  onStoreUpdate() {
      let {route} = store.getState().rootReducer.root;

      // handle a root change
      // if your app doesn't change roots in runtime, you can remove onStoreUpdate() altogether
      if (this.currentRootRoute != route) {
        this.currentRootRoute = route;
        if (Platform.OS === 'ios') {
          this.startApp(route);
        } else {
          Navigation.isAppLaunched()
              .then((appLaunched) => {
                if (appLaunched) {
                  
                }
                new NativeEventsReceiver().appLaunched(this.startApp(route));
              })
        }
      }
    }
    
  startApp(route) {
    switch (route) {
        case CONSTANT.ROOT_ROUTE.BLANK_PAGE:
          Navigation.startSingleScreenApp({
              screen: {
                screen: 'PartnerKliknKlin.BlankPage', // unique ID registered with Navigation.registerScreen
                //title: 'Welcome', // title of the screen as appears in the nav bar (optional)
                navigatorStyle: {
                    navBarHidden: true,
                    overrideBackPress: true,
                }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
              navigatorButtons: {}, // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
              },
              animationType:'none',
              appStyle: {
                orientation: 'portrait'
              },
          });
        return;
        case CONSTANT.ROOT_ROUTE.LOGIN:
          Navigation.startSingleScreenApp({
              screen: {
                screen: 'PartnerKliknKlin.SignIn', // unique ID registered with Navigation.registerScreen
                //title: 'Welcome', // title of the screen as appears in the nav bar (optional)
                navigatorStyle: {
                    navBarHidden: true,
                    overrideBackPress: true,
                }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
              navigatorButtons: {}, // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
              },
              animationType:'none',
              appStyle: {
                orientation: 'portrait'
              },
          });
          return;
          
        case CONSTANT.ROOT_ROUTE.AFTER_LOGIN:
            Navigation.startSingleScreenApp({
                screen: {
                  screen: CONSTANT.ROUTE_TYPE.DASHBOARD, // unique ID registered with Navigation.registerScreen
                  //title: 'Welcome', // title of the screen as appears in the nav bar (optional)
                  navigatorStyle: {
                    navBarHidden: true,
                }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
                navigatorButtons: {}, // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
                },
                animationType:'none',
                appStyle: {
                  orientation: 'portrait'
                },
            });
            return;

        case CONSTANT.ROOT_ROUTE.SMS_VERIFICATION:
            Navigation.startSingleScreenApp({
                screen: {
                  screen: 'PartnerKliknKlin.SMSVerification', // unique ID registered with Navigation.registerScreen
                  //title: 'Welcome', // title of the screen as appears in the nav bar (optional)
                  navigatorStyle: {
                    navBarHidden: true,
                }, // override the navigator style for the screen, see "Styling the navigator" below (optional)
                navigatorButtons: {}, // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
                },
                animationType:'none',
                appStyle: {
                  orientation: 'portrait'
                },
            });
            // Navigation.startTabBasedApp({
            //   tabs: [
            //     {
            //       label: 'Beranda', // tab label as appears under the icon in iOS (optional)
            //       screen: 'PartnerKliknKlin.DashboardCourier', // unique ID registered with Navigation.registerScreen
            //       icon: require('./image/layout/home_icon.png'), // local image asset for the tab icon unselected state (optional on iOS)
            //       navigatorStyle: {
            //         navBarHidden:true,
            //         tabBarHidden:false,
            //       }, // override the navigator style for the tab screen, see "Styling the navigator" below (optional),
            //       navigatorButtons: {} // override the nav buttons for the tab screen, see "Adding buttons to the navigator" below (optional)
            //     },
            //     {
            //       label: 'Beranda', // tab label as appears under the icon in iOS (optional)
            //       screen: 'PartnerKliknKlin.DashboardAdminOutlet', // unique ID registered with Navigation.registerScreen
            //       icon: require('./image/layout/home_icon.png'), // local image asset for the tab icon unselected state (optional on iOS)
            //       navigatorStyle: {
            //         navBarHidden:true,
            //         tabBarHidden:false,
            //       }, // override the navigator style for the tab screen, see "Styling the navigator" below (optional),
            //       navigatorButtons: {} // override the nav buttons for the tab screen, see "Adding buttons to the navigator" below (optional)
            //     },
            //     {
            //       label: 'Peralatan', // tab label as appears under the icon in iOS (optional)
            //       screen: 'PartnerKliknKlin.UtilityCourier', // unique ID registered with Navigation.registerScreen
            //       icon: require('./image/layout/utility_icon.png'), // local image asset for the tab icon unselected state (optional on iOS)
            //       navigatorStyle: {
            //         navBarHidden:true,
            //         tabBarHidden:false,
            //       }, // override the navigator style for the tab screen, see "Styling the navigator" below (optional),
            //       navigatorButtons: {} // override the nav buttons for the tab screen, see "Adding buttons to the navigator" below (optional)
            //     },
            //     {
            //       label: 'Profil', // tab label as appears under the icon in iOS (optional)
            //       screen: 'PartnerKliknKlin.DashboardCourier', // unique ID registered with Navigation.registerScreen
            //       icon: require('./image/layout/profile_icon.png'), // local image asset for the tab icon unselected state (optional on iOS)
            //       navigatorStyle: {
            //         navBarHidden:true,
            //         tabBarHidden:false,
            //       }, // override the navigator style for the tab screen, see "Styling the navigator" below (optional),
            //       navigatorButtons: {} // override the nav buttons for the tab screen, see "Adding buttons to the navigator" below (optional)
            //     },
            //   ],
            //   tabsStyle: { //ios style
            //   },
            //   appStyle:{
            //     tabBarBackgroundColor: '#fff',
            //     tabBarButtonColor: '#C8C9CB',
            //     tabBarSelectedButtonColor: '#6BCBF1',
            //     tabFontSize:12,
            //     selectedTabFontSize:14,
            //     tabBarTranslucent:false,
            //     tabFontFamily: 'montserrat',
            //     forceTitlesDisplay: true,
            //     tabBarHidden:true,
            //   },
            //   passProps: {}, // simple serializable object that will pass as props to all top screens (optional)
            //   animationType: 'none' // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
            // });
            return;

        default: 
          console.log("Not Root Found");
        }
    }
}