import { combineReducers } from 'redux';

import * as user from './user';
import * as masterData from './master_data';
import * as service from './service';
import * as layout from './layout';
import * as map from './map';
import * as order from './order';
import * as message from './message';
import * as root from './root';
import * as pushNotif from './push_notif';
import * as pos from './pos';
import * as attendance from './attendance';
import * as deposit from './deposit';
import * as membership from './membership';
import * as customer from './customer';
import * as bluetoothDevice from './bluetooth_device';
import * as posOldVersion from './pos_old_version';
import * as expenses from './expenses';

export const rootReducer = combineReducers({
	'user':combineReducers({
		location:user.getUserLocationData,
		role:user.getUserRole,
		data:user.getUserData,
		outlet:user.getUserOutlet,
		outletLocation:user.getOutletLocation
	}),
	'masterData':combineReducers({
		'delayOrderReasonList':masterData.getDelayOrderReasonList,
		'rejectOrderReasonList':masterData.getRejectOrderReasonList,
		'courierList':masterData.getCourierList,
		'outletList':masterData.getOutletList,
		'complaintItemSolutionList':masterData.getComplaintItemSolutionList,
		'expenditureCategoryList':masterData.getExpenditureCategoryList,
	}),
	'service':combineReducers({
		'location':combineReducers({
			'status':service.getLocationServiceStatus,
			'permision':service.getLocationServicePermission,
		}),
		'connection':combineReducers({
			'status':service.getConnectionStatus,
		}),
	}),
	root:combineReducers({
		route:root.getRoute,
		smsVerificationTimer:root.getSMSVerificationTimer,
		smsVerificationSent:root.getSMSVerificationSent,
		notificationList: root.getNotificationList,
		messageList: root.getMessageList,
		totalImportantMessageNotRead: root.getTotalImportantMessageNotRead,
		isInitialLoadDataDone: root.getIsInitialLoadDataDone,
		informationList: root.getInformationList,
		webLinks: root.getWebLinks,
		mdTimes: root.getMdTime,
		overrideBackPress: root.getOverrideBackPress,
	}),
	'message':combineReducers({
		'error':message.getErrorMessage,
	}),
	'layout':combineReducers({
		'height':layout.getLayoutHeight,
		'pressBarrierVisibility':layout.getPressBarrierVisibility,
		'screenState': layout.getScreenState,
	}),
	map:combineReducers({
		directionCoords:map.getMapDirectionCoords,
		centerCoords:map.getMapCenterCoords,
		directionDistance:map.getMapDirectionDistance,
		directionDuration:map.getMapDirectionDuration,
	}),
	pos:combineReducers({
		selectedItem:pos.getSelectedItem,
		selectedDuration:pos.getSelectedDuration,
		selectedDeliveryMethod:pos.getSelectedDeliveryMethod,
		selectedPayment:pos.getSelectedPayment,
		selectedCustomer:pos.getSelectedCustomer,
		selectedPromo:pos.getSelectedPromo,
		orderCreated:pos.getOrderCreatedData,
		totalPrice:pos.getTotalPrice,
		outletItem:pos.getOutletItem,
		extractedOutletItem:pos.getExtractedOutletItem,
		availableDuration:pos.getAvailableDuration,
	}),
	order:combineReducers({
		active:order.getOrderActive,
		shipmentActivePickup:order.getShipmentActivePickup,
		shipmentActiveDelivery:order.getShipmentActiveDelivery,
		shipmentScheduleList:order.getShipmentScheduleList,
		shipmentScheduleDate:order.getShipmentScheduleDate,
		incomingOrderList:order.getIncomingOrderList,
		pickupOrderList:order.getPickupOrderList,
		inProgressOrderList:order.getInProgressOrderList,
		deliveryOrderList:order.getDeliveryOrderList,
		historyOrderList:order.getHistoryOrderList,
		historyOrderDate:order.getHistoryOrderDate,
		complaintOrderList:order.getComplaintOrderList,
		complaintMessageList:order.getComplaintMessageList,
		assignmentTodayCourier:order.getAssignmentTodayCourier,
		searchOrderResult:order.getSearchOrderResult,
		searchOrderLoadingStatus:order.getSearchOrderLoadingStatus,
		searchOrderCategory:order.getSearchOrderCategory,
		summary:order.getSummaryOrder,
		updatedCheckoutItemList:order.getUpdatedCheckoutItemList,
		selectedOrderDate:order.getSelectedOrderDate,
		lateInProgressOrderList:order.getLateInProgressOrderList,
		latePickupOrderList:order.getLatePickupOrderList,
		lateDeliveryOrderList:order.getLateDeliveryOrderList,
		orderListTypeTab:order.getOrderListTypeTab,
		orderListData:order.getOrderListData,
	}),
	pushNotif:combineReducers({
		token:pushNotif.getPushNotifToken,
	}),
	attendance:combineReducers({
		staffList: attendance.getStaffList,
		staffSummary: attendance.getStaffSummary,
		attendanceDate: attendance.getAttendanceDate,
		attendanceMonth: attendance.getAttendanceMonth
	}),
	customer:combineReducers({
		search: customer.getSearchData,
		temp: customer.getTempData,
		outlet: customer.getCustomerOutlet,
	}),
	deposit:combineReducers({
		summary: deposit.getDepositSummary,
		cashFlow: deposit.getCashflowTransaction,
		accountOwner: deposit.getAccountOwner,
		kliknklinBanks: deposit.getKnkBanks,
		wdrRequest: deposit.getWdrRequest
	}),
	membership: combineReducers({
		searchedCustomer: membership.getSearchedCustomer,
		membershipPackages: membership.getMembershipPackages,
		membershipCustomer: membership.getMembershipCustomer
	}),
	bluetoothDevice: combineReducers({
		connectedDevice: bluetoothDevice.getConnectedDevice,
		prevPrintedData: bluetoothDevice.getPrevPrintedData,
		counter: bluetoothDevice.getCounter
	}),
	posOldVersion: combineReducers({
		transactionList: posOldVersion.getTransactionList,
		activeTransaction: posOldVersion.getActiveTransaction,
		searchTransactionList: posOldVersion.getSearchTransactionList
	}),
	'expenses':combineReducers({
		'expenseList':expenses.getExpenseList,
		'searchDate':expenses.getExpenseSearchDate,
	}),
});



