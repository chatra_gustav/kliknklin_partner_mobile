import * as types from 'app/data/actions/actionTypes';

export function getSearchData(state = {
	mobilePhone:'',
}, action= {}){
	switch (action.type){
		case types.CUSTOMER_SEARCH_DATA:
			return action.data;

		default:
			return state;
	}
}

export function getTempData(state = {}, action= {}){
	switch (action.type){
		case types.CUSTOMER_TEMP_DATA:
			return action.data;

		default:
			return state;
	}
}

export function getCustomerOutlet(state = [], action= {}){
	switch (action.type){
		case types.CUSTOMER_OUTLET:
			return action.data;

		default:
			return state;
	}
}