import * as types from './../../actions/actionTypes';
import * as CONSTANT from './../../constant';

export function getOrderActive(state = {}, action = {}) {

  switch (action.type) {
    
    case types.ORDER_ACTIVE:
      return {
      	'data':action.data,
      	'orderActiveType':action.orderActiveType,
      }

    default:
      return state;
  }
}

export function getSelectedOrderDate(state = new Date(), action = {}) {

  switch (action.type) {
    
    case types.SELECTED_ORDER_DATE:
      return action.date;

    default:
      return state;
  }
}

export function getShipmentActivePickup(state = {}, action = {}) {

  switch (action.type) {
    
    case types.SHIPMENT_ACTIVE_PICKUP:
      return action.data;

    default:
      return state;
  }
}

export function getShipmentActiveDelivery(state = {}, action = {}) {

  switch (action.type) {
    
    case types.SHIPMENT_ACTIVE_DELIVERY:
      return action.data;

    default:
      return state;
  }
}

export function getShipmentScheduleDate(state = {}, action = {}) {

  switch (action.type) {
    
    case types.SHIPMENT_SCHEDULE_DATE:
      return action.date;
    default:
      return state;
  }
}

export function getShipmentScheduleList(state = [], action = {}) {

  switch (action.type) {
    
    case types.SHIPMENT_SCHEDULE_LIST:
      return action.data;

    default:
      return state;
  }
}

export function getOrderListData(state = [], action = {}) {

  switch (action.type) {
    
    case types.ORDER_LIST_DATA:
      return action.data;

    default:
      return state;
  }
}

this.toDate = new Date();
tempDate = new Date();
this.fromDate = new Date(tempDate.setDate(tempDate.getDate() - 30));
export function getHistoryOrderDate(state = {
  fromDate:this.fromDate.getFullYear() + '-' + ("0" + (this.fromDate.getMonth() + 1)).slice(-2) + '-' + ("0" + this.fromDate.getDate()).slice(-2),
  toDate:this.toDate.getFullYear() + '-' + ("0" + (this.toDate.getMonth() + 1)).slice(-2) + '-' + ("0" + this.toDate.getDate()).slice(-2) 
}, action = {}) {

  switch (action.type) {
    
    case types.HISTORY_ORDER_DATE:
      return {
        fromDate: action.fromDate,
        toDate: action.toDate,
      };

    default:
      return state;
  }
}

export function getHistoryOrderList(state = {
  online:[],
  pos:[],
}, action = {}) {

  switch (action.type) {
    
    case types.HISTORY_ORDER_LIST:
      return {
        online:action.online,
        pos:action.pos,
      };

    default:
      return state;
  }
}

export function getIncomingOrderList(state = {
  online:[],
  pos:[],
}, action = {}) {

  switch (action.type) {
    
    case types.INCOMING_ORDER_LIST:
      return {
        online:action.online,
        pos:action.pos,
      };

    default:
      return state;
  }
}

export function getPickupOrderList(state = [], action = {}) {

  switch (action.type) {
    
    case types.PICKUP_ORDER_LIST:
      return action.data;

    default:
      return state;
  }
}

export function getInProgressOrderList(state = [], action = {}) {

  switch (action.type) {
    
    case types.IN_PROGRESS_ORDER_LIST:
      return action.data;

    default:
      return state;
  }
}

export function getDeliveryOrderList(state = [], action = {}) {

  switch (action.type) {
    
    case types.DELIVERY_ORDER_LIST:
      return action.data;

    default:
      return state;
  }
}

export function getComplaintOrderList(state = {
  online:[],
  pos:[],
}, action = {}) {

  switch (action.type) {
    
    case types.COMPLAINT_ORDER_LIST:
      return {
        online:action.online,
        pos:action.pos,
      };

    default:
      return state;
  }
}

export function getComplaintMessageList(state = [], action = {}) {

  switch (action.type) {
    
    case types.COMPLAINT_MESSAGE_LIST:
      return action.data;

    default:
      return state;
  }
}

export function getLateInProgressOrderList(state = [], action = {}) {

  switch (action.type) {
    
    case types.LATE_IN_PROGRESS_ORDER_LIST:
      return action.data;

    default:
      return state;
  }
}

export function getLatePickupOrderList(state = [], action = {}) {

  switch (action.type) {
    
    case types.LATE_PICKUP_ORDER_LIST:
      return action.data;

    default:
      return state;
  }
}

export function getLateDeliveryOrderList(state = [], action = {}) {

  switch (action.type) {
    
    case types.LATE_DELIVERY_ORDER_LIST:
      return action.data;

    default:
      return state;
  }
}

export function getOrderListTypeTab(state = 0, action = {}) {

  switch (action.type) {
    
    case types.ORDER_LIST_TYPE_TAB:
      return action.data;

    default:
      return state;
  }
}

export function getSummaryOrder(state = {
  incoming:0,
  pickup:0,
  inProgress:0,
  delivery:0,
  complaint:0,
  pickupLate:0,
  inProgressLate:0,
  deliveryLate:0,
  isComplaintImportant:false,
  isNotificationImportant:false,
}, action = {}) {

  switch (action.type) {
    
    case types.ORDER_SUMMARY:
      return {
        incoming:action.incoming,
        pickup:action.pickup,
        inProgress:action.inProgress,
        delivery:action.delivery,
        complaint:action.complaint,
        pickupLate:action.pickupLate,
        inProgressLate:action.inProgressLate,
        deliveryLate:action.deliveryLate,
        isComplaintImportant:action.isComplaintImportant,
        isNotificationImportant:action.isNotificationImportant,
      };

    default:
      return state;
  }
}

export function getAssignmentTodayCourier(state = {
  totalPickup:0,
  totalDelivery:0,
}, action = {}) {

  switch (action.type) {
    
    case types.ASSIGNMENT_TODAY_COURIER:
      return {
        totalPickup:action.totalPickup,
        totalDelivery:action.totalDelivery,
      };

    default:
      return state;
  }
}

export function getSearchOrderResult(state = [], action = {}) {

  switch (action.type) {
    
    case types.SEARCH_ORDER_RESULT:
      return action.data;

    default:
      return state;
  }
}

export function getSearchOrderLoadingStatus(state = false, action = {}) {

  switch (action.type) {
    
    case types.SEARCH_ORDER_LOADING_STATUS:
      return action.loadingStatus;

    default:
      return state;
  }
}

export function getSearchOrderCategory(state = CONSTANT.SEARCH_ORDER_CATEGORY.DEFAULT, action = {}) {

  switch (action.type) {
    
    case types.SEARCH_ORDER_CATEGORY:
      return action.category;

    default:
      return state;
  }
}

export function getUpdatedCheckoutItemList(state = [], action = {}) {

  switch (action.type) {
    
    case types.UPDATED_CHECKOUT_ITEM_LIST:
      return action.data;

    default:
      return state;
  }
}

