import * as types from './../../actions/actionTypes';

export function getMapDirectionCoords(state = [], action = {}) {

  switch (action.type) {
    
    case types.MAP_DIRECTION_COORDS:
      return  action.coords

    default:
      return state;
  }
}

export function getMapDirectionDistance(state = '0 km', action = {}) {

  switch (action.type) {
    
    case types.MAP_DIRECTION_DISTANCE:
      return  action.distance

    default:
      return state;
  }
}

export function getMapDirectionDuration(state = '0 mins', action = {}) {

  switch (action.type) {
    
    case types.MAP_DIRECTION_DURATION:
      return  action.duration

    default:
      return state;
  }
}

export function getMapCenterCoords(state = null, action = {}) {

  switch (action.type) {
    
    case types.MAP_CENTER_COORDS:
      return  action.coords

    default:
      return state;
  }
}