import * as types from './../../actions/actionTypes';

export function getDelayOrderReasonList(state = [], action = {}) {
	switch (action.type) {

		case types.DELAY_ORDER_REASON_LIST:
			return action.reason;

		default:
			return state;
	}
}

export function getRejectOrderReasonList(state = [], action = {}) {
	switch (action.type) {

		case types.REJECT_ORDER_REASON_LIST:
			return action.reason;

		default:
			return state;
	}
}

export function getOutletList(state = [], action = {}) {
	switch (action.type) {

		case types.OUTLET_LIST:
			return action.listOutlet;

		default:
			return state;
	}
}

export function getComplaintItemSolutionList(state = [], action = {}) {
	switch (action.type) {

		case types.COMPLAINT_ITEM_SOLUTION_LIST:
			return action.listSolution;

		default:
			return state;
	}
}

export function getCourierList(state = ['Ichigo', 'Brook', 'Zhuge'], action = {}) {
	switch (action.type) {

		case types.COURIER_LIST:
			return action.listCourier;

		default:
			return state;
	}
}

export function getExpenditureCategoryList(state = [], action = {}) {
	switch (action.type) {

		case types.EXPENDITURE_CATEGORY_LIST:
			return action.data;

		default:
			return state;
	}
}



