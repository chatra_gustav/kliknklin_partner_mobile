import * as types from './../../actions/actionTypes';

export function getConnectedDevice(state = null, action = {}) {

  switch (action.type) {
    
    case types.SET_CONNECTED_DEVICE:
      return action.data;

    default:
      return state;
  }
}

export function getPrevPrintedData(state = null, action = {}) {

  switch (action.type) {
    
    case types.SET_PREV_PRINTED_DATA:
      return action.data;

    default:
      return state;
  }
}

export function getCounter(state = 1, action = {}) {

  switch (action.type) {
    
    case types.SET_COUNTER:
      return action.data;

    default:
      return state;
  }
}