import * as types from './../../actions/actionTypes';

export function setUserProfile (state = {}, action = {}) {
	switch (action.type) {

		case types.SET_PROFILE:
			return {
				profile:action.user,
			};

		default:
			return state;
	}
}

export function getUserLocationData(state = null, action = {}) {
	switch (action.type) {

		case types.USER_LOCATION:
			return action.location;

		default:
			return state;
	}
}

export function getUserRole(state = 'login', action = {}) {
	switch (action.type) {

		case types.USER_ROLE:
			return action.role;

		default:
			return state;
	}
}

export function getUserData (state = {}, action= {}){
	switch (action.type){
		case types.USER_DATA:
			return action.data;

		default:
			return state;
	}
}

export function getUserOutlet (state = {indexActiveOutlet:0,outletList:[]}, action= {}){
	switch (action.type){
		case types.USER_OUTLET:
			return {
				indexActiveOutlet:action.indexActiveOutlet,
				outletList:action.data,	
			};

		default:
			return state;
	}
}

export function getOutletLocation (state = {
	latitude: -7.7956,
	longitude: 110.3695,
	latitudeDelta: 999999,
	longitudeDelta: 999999,
}, action= {}){
	switch (action.type){
		case types.OUTLET_LOCATION:
			return action.data;

		default:
			return state;
	}
}