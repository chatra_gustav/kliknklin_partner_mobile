import * as types from 'app/data/actions/actionTypes';

export function getOutletItem(state = [], action= {}){
	switch (action.type){
		case types.POS_OUTLET_ITEM:
			return action.data;

		default:
			return state;
	}
}
export function getExtractedOutletItem(state = [], action= {}){
	switch (action.type){
		case types.POS_EXTRACTED_OUTLET_ITEM:
			return action.data;

		default:
			return state;
	}
}
export function getSelectedItem(state = [], action= {}){
	switch (action.type){
		case types.POS_SELECTED_ITEM:
			return action.data;

		default:
			return state;
	}
}

export function getSelectedPayment(state = {}, action= {}){
	switch (action.type){
		case types.POS_SELECTED_PAYMENT:
			return action.data;

		default:
			return state;
	}
}

export function getSelectedPromo(state = null, action= {}){
	switch (action.type){
		case types.POS_SELECTED_PROMO:
			return action.data;

		default:
			return state;
	}
}

export function getOrderCreatedData(state = {
	showCreated:false,
	data:{},
}, action= {}){
	switch (action.type){
		case types.POS_ORDER_CREATED_DATA:
			return action.data;

		default:
			return state;
	}
}

export function getAvailableDuration(state = {durationList:[], durationReferralList:[], durationReferral:0}, action= {}){
	switch (action.type){
		case types.POS_AVAILABLE_DURATION:
			return action.data;

		default:
			return state;
	}
}

export function getSelectedDuration(state = 0, action= {}){
	switch (action.type){
		case types.POS_SELECTED_DURATION:
			return action.data;

		default:
			return state;
	}
}

export function getSelectedDeliveryMethod(state = 0, action= {}){
	switch (action.type){
		case types.POS_SELECTED_DELIVERY_METHOD:
			return action.data;

		default:
			return state;
	}
}

export function getTotalPrice(state = 0, action= {}){
	switch (action.type){
		case types.POS_TOTAL_PRICE:
			return action.totalPrice;

		default:
			return state;
	}
}

export function getSelectedCustomer(state = null, action= {}){
	switch (action.type){
		case types.POS_SELECTED_CUSTOMER:
			return action.data;

		default:
			return state;
	}
}


