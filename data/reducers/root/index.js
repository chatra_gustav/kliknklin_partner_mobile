import * as types from './../../actions/actionTypes';
import * as CONSTANT from './../../constant';

export function getRoute(state = CONSTANT.ROOT_ROUTE.BLANK_PAGE, action = {}) {
  switch (action.type) {
    
    case types.ROOT_ROUTE:
      return action.route;

    default:
      return state;
  }
}

export function getOverrideBackPress(state = {
  isOverride:false,
  route:null,
  routeType:'reset',
  props:{},
}, action = {}) {
  switch (action.type) {
    
    case types.OVERRIDE_BACK_PRESS:
      return {
        isOverride:action.isOverride,
        route:action.route,
        routeType:action.routeType,
        props:action.props,
      };

    default:
      return state;
  }
}

export function getIsInitialLoadDataDone(state = false, action = {}) {
  switch (action.type) {
    
    case types.IS_INITIAL_LOAD_DATA_DONE:
      return action.data;

    default:
      return state;
  }
}

export function getSMSVerificationTimer(state = CONSTANT.SMS_VERIFICATION.BASE_TIME_TIMER, action = {}) {
  switch (action.type) {
    
    case types.SMS_VERIFICATION_TIMER:
      return action.timerSecond;

    default:
      return state;
  }
}

export function getSMSVerificationSent(state = false, action = {}) {
  switch (action.type) {
    
    case types.SMS_VERIFICATION_SENT:
      return action.isSent;

    default:
      return state;
  }
}

export function getNotificationList(state = [], action = {}) {
  switch (action.type) {
    
    case types.NOTIFICATION_LIST:
      return action.data;

    default:
      return state;
  }
}

export function getMessageList(state = [], action = {}) {
  switch (action.type) {
    
    case types.MESSAGE_LIST:
      return action.data;

    default:
      return state;
  }
}

export function getInformationList(state = [], action = {}) {
  switch (action.type) {
    
    case types.INFORMATION_LIST:
      return action.data;

    default:
      return state;
  }
}

export function getTotalImportantMessageNotRead(state = 0, action = {}) {
  switch (action.type) {
    
    case types.TOTAL_IMPORTANT_MESSAGE_NOT_READ:
      return action.data;

    default:
      return state;
  }
}

export function getWebLinks(state = [], action = {}) {
  switch (action.type) {
    
    case types.SET_WEB_LINKS:
      return action.data;

    default:
      return state;
  }
}

export function getMdTime(state = [], action = {}) {
  switch (action.type) {
    
    case types.SET_MD_TIME:
      return action.data;

    default:
      return state;
  }
}

