import * as types from './../../actions/actionTypes';

export function getDepositSummary(state = [], action = {}) {

  switch (action.type) {
    
    case types.SET_DEPOSIT_SUMMARY:
      return action.data;

    default:
      return state;
  }
}

export function getCashflowTransaction(state = [], action = {}){
	
	switch (action.type) {
    	case types.SET_CASHFLOW_TRANSACTION:
      		return action.data;

    	default:
      		return state;
  	}	
}

export function getAccountOwner(state = null, action = {}){
	
	switch (action.type) {
    	case types.SET_ACCOUNT_OWNER:
      		return action.data;

    	default:
      		return state;
  	}	
}

export function getKnkBanks(state = [], action = {}){
	
	switch (action.type) {
    	case types.SET_KLIKNKLIN_BANK:
      		return action.data;

    	default:
      		return state;
  	}	
}

export function getWdrRequest(state = [], action = {}){
	
	switch (action.type) {
    	case types.SET_WDR_REQUEST:
      		return action.data;

    	default:
      		return state;
  	}	
}