import * as types from './../../actions/actionTypes';
import {DateFormat} from 'app/data/components';

export function getStaffList(state = [], action = {}) {

  switch (action.type) {
    
    case types.SET_STAFF_LIST:
      return action.data;

    default:
      return state;
  }
}

export function getStaffSummary(state = [], action = {}) {

  switch (action.type) {
    
    case types.SET_STAFF_SUMMARY:
      return action.data;

    default:
      return state;
  }
}

export function getAttendanceDate(state = DateFormat(new Date(), 'isoDate'), action = {}) {

  switch (action.type) {
    
    case types.SET_ATTENDANCE_DATE:
      return action.data;

    default:
      return state;
  }
}

export function getAttendanceMonth(state = DateFormat(new Date(), 'isoDate'), action = {}) {

  switch (action.type) {
    
    case types.SET_ATTENDANCE_MONTH:
      return action.data;

    default:
      return state;
  }
}