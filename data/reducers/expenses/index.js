import * as types from './../../actions/actionTypes';
import {DateFormat} from 'app/data/components';

export function getExpenseList(state = [], action = {}) {

  switch (action.type) {
    
    case types.EXPENSE_LIST:
      return action.data;

    default:
      return state;
  }
}


export function getExpenseSearchDate(state = new Date(), action = {}) {

  switch (action.type) {
    
    case types.EXPENSE_SEARCH_DATE:
      return action.date;

    default:
      return state;
  }
}
