import * as types from './../../actions/actionTypes';

export function getSearchedCustomer(state = [], action = {}) {

  switch (action.type) {
    
    case types.SET_SEARCHED_CUSTOMER:
      return action.data;

    default:
      return state;
  }
}

export function getMembershipPackages(state = [], action = {}) {

  switch (action.type) {
    
    case types.SET_MEMBERSHIP_PACKAGES:
      return action.data;

    default:
      return state;
  }
}

export function getMembershipCustomer(state = {}, action = {}) {

  switch (action.type) {
    
    case types.SET_MEMBERSHIP_CUSTOMER:
      return action.data;

    default:
      return state;
  }
}