import * as types from './../../actions/actionTypes';

export function getErrorMessage(state = '', action = {}) {
  switch (action.type) {
    
    case types.SET_ERROR_MESSAGE:
      return action.message;

    default:
      return state;
  }
}
