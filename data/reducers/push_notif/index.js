import * as types from './../../actions/actionTypes';
import * as CONSTANT from './../../constant';

export function getPushNotifToken(state = null, action = {}) {

  switch (action.type) {
    
    case types.PUSH_NOTIF_TOKEN:
      return action.token;

    default:
      return state;
  }
}
