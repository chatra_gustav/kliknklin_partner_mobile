import * as types from 'app/data/actions/actionTypes';
import {DateFormat} from 'app/data/components';

export function getTransactionList(state = [], action = {}) {

  switch (action.type) {
    
    case types.SET_TRANSACTION_LIST:
      return action.data;

    default:
      return state;
  }
}

export function getSearchTransactionList(state = [], action = {}) {

  switch (action.type) {
    
    case types.SET_SEARCH_TRANSACTION_LIST:
      return action.data;

    default:
      return state;
  }
}

export function getActiveTransaction(state = {}, action = {}) {

  switch (action.type) {
    
    case types.SET_ACTIVE_TRANSACTION:
      return action.data;

    default:
      return state;
  }
}