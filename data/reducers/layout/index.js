import * as types from './../../actions/actionTypes';
import * as CONSTANT from './../../constant';

export function getLayoutHeight(state = 0, action = {}) {
  switch (action.type) {
    
    case types.LAYOUT_HEIGHT:
      return action.height;

    default:
      return state;
  }
}

export function getPressBarrierVisibility(state = false, action = {}) {
  switch (action.type) {
    
    case types.LAYOUT_PRESS_BARRIER_VISIBILITY:
      return action.visibility;

    default:
      return state;
  }
}

export function getScreenState(state = CONSTANT.SCREEN_STATE.DID_DISAPPEAR, action = {}){
  switch (action.type) {
    
    case types.SCREEN_STATE:
      return action.screenState;

    default:
      return state;
  }
}
