import * as types from './../../actions/actionTypes';


//service phone, gps, connection, camera etc2

export function getConnectionStatus(state = '4g', action = {}) {
  switch (action.type) {
    
    case types.CONNECTION_STATUS:
      return action.connectionStatus;

    default:
      return state;
  }
}



export function getLocationServiceStatus(state = false, action = {}) {
  switch (action.type) {
    
    case types.LOCATION_SERVICE_STATUS:
      return action.locationServiceStatus;

    default:
      return state;
  }
}

export function getLocationServicePermission(state = false, action = {}) {
  switch (action.type) {
    
    case types.LOCATION_SERVICE_PERMISSION:
      return action.locationServicePermission;

    default:
      return state;
  }
}

