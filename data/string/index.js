import * as ERROR from './error';
import * as LOADING_MODAL from './loading_modal';
import * as LOGIN from './login';
import * as FORGET_PASSWORD from './forget_password';
import * as DASHBOARD_COURIER from './dashboard/courier';
import * as DASHBOARD_ADMIN_OUTLET from './dashboard/admin_outlet';
import * as ORDER_DETAIL from './order_detail';
import * as CHECKOUT_COURIER from './checkout_courier';
import * as CONFIRM_ITEM from './confirm_item';
import * as ORDER_CONTENT from './order_content';
import * as COMPLAINT_STATUS from './complaint_status';
import * as USER_ROLE from './user_role';
import * as UTILITY from './utility'
import * as SEARCH_ORDER from './search_order';


const SCREEN = {
	LOGIN,
	FORGET_PASSWORD,
	DASHBOARD_COURIER,
	DASHBOARD_ADMIN_OUTLET,
	CONFIRM_ITEM,
	ORDER_CONTENT,
	UTILITY,
	SEARCH_ORDER
}

export {
	ERROR,
	LOADING_MODAL,
	SCREEN,
	ORDER_DETAIL,
	CHECKOUT_COURIER,
	COMPLAINT_STATUS,
	USER_ROLE,
}