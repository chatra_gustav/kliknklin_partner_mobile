export const IN_PROGRESS_PICKUP = 'Menuju Ke Lokasi Penjemputan';

export const DONE_PICKUP = 'Menyelesaikan Penjemputan';

export const IN_PROGRESS_LAUNDRY = 'Proses Pencucian';

export const DONE_LAUNDRY = 'Laundry Selesai';

export const IN_PROGRESS_DELIVERY = 'Menuju Ke Lokasi Pengiriman';

export const DONE_DELIVERY = 'Pengiriman Selesai';