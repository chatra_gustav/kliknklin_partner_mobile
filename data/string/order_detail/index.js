import * as SUBMIT_BUTTON from './submit_button';
import * as TITLE from './title';

export {
	SUBMIT_BUTTON,
	TITLE,
}