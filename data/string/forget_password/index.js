export const TITLE = 'Lupa Kata Kunci';

export const SUBTITLE = 'Masukkan alamat email';

export const EMAIL_PLACEHOLDER = 'Email Anda';

export const SEND_EMAIL = 'Kirim Reset Link';

export const SEND_EMAIL_AGAIN = 'Kirim Ulang Reset Link';

export const MIDDLE_TEXT_1 = 'Kami telah mengirimkan link reset password';

export const MIDDLE_TEXT_2 = ' ke email';

export const MIDDLE_TEXT_3 = 'Silahkan buka alamat email yang tercantum';