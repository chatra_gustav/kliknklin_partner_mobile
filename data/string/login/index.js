export const TITLE = 'Masuk';

export const SUBTITLE = 'Selamat Datang!';

export const FORGET_PASSWORD_LINK = 'Lupa Kata Kunci?';

export const SIGN_IN_LABEL = 'Ingin bergabung bersama kami?';

export const SIGN_IN_LINK = 'Daftar';

export const EMAIL_PLACEHOLDER = 'Email';

export const PASSWORD_PLACEHOLDER = 'Password';

export const FOOTER = '© '+ new Date().getFullYear() + ' Hak Cipta Terpelihara PT. KliknKlin Digital Nusantara';

export const SWITCH_LABEL_SHOW_PASSWORD = 'Perlihatkan Kata Kunci';