export const IN_PROGRESS = 'Sedang Berlangsung';

export const SOLVED = 'Teratasi';

export const DONE = 'Selesai';

export const CANCEL = 'Dibatalkan';

export const SOLUTION_NEEDED = 'Anda harus memberikan solusi dari barang cucian.'