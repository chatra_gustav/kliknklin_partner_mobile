export const LOGIN = {
	API_VALIDATOR_FAIL : 'Akun tidak tersedia / Kata Kunci salah.',
	EMPTY_FIELD : 'Mohon isi semua data yang dibutuhkan.',
	EMAIL_FORMAT :'Format email salah, contoh xxx@mail.com.',
	MINIMAL_CHARACTER_PASSWORD : 'Password minimal menggunakan 6 karakter.',
};

export const TIMEOUT = {
	TITLE:'Terdapat Kesalahan!',
	CONTENT:'Gagal menyambungkan ke server! Check koneksi internet anda.',
}

export const UNKNOWN = {
	TITLE:'Terdapat Kesalahan!',
	CONTENT:'Terdapat Kesalahan pada server! Hubungi kami untuk info lebih lanjut.',
}