import React, { Component } from 'react';
import { 
  View,
  Platform, 
  StyleSheet, 
  Image, 
  Dimensions, 
  TouchableOpacity, 
  TouchableWithoutFeedback,
  Modal,
  BackHandler,
  Keyboard,}from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

import {TextWrapper, TabBar, OrderOption, ConnectionStability} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from "./../../../actions";

class TabBarMainDashboard extends Component{
  constructor(props) {
        super(props);
        this.state = {
          image:{
            home_icon:{
                width:0,
                height:0,
                multiplierWidth:0.5,
                multiplierHeight:0.5,
                url:require('./../../../image/layout/home_icon.png'),
            },
            utility_icon:{
                width:0,
                height:0,
                multiplierWidth:0.1,
                multiplierHeight:0.1,
                url:require('./../../../image/layout/utility_icon.png'),
            },
            help_icon:{
                width:0,
                height:0,
                multiplierWidth:0.1,
                multiplierHeight:0.1,
                url:require('./../../../image/layout/help_icon.png'),
            },
            setting_icon:{
                width:0,
                height:0,
                multiplierWidth:0.1,
                multiplierHeight:0.1,
                url:require('./../../../image/layout/setting_icon.png'),
            },
          },
          homeRoute:this.setHomeRoute(),
        };
  }

  componentWillMount(){
    this._getImageSize();
  }

  _getImageSize(){
    var image = this.state.image;

    var image_home_icon = ResolveAssetSource(this.state.image.home_icon.url);

    image.home_icon.width = moderateScale(image_home_icon.width);
    image.home_icon.height = moderateScale(image_home_icon.height);

    var image_utility_icon = ResolveAssetSource(this.state.image.utility_icon.url);

    image.utility_icon.width = moderateScale(image_utility_icon.width);
    image.utility_icon.height = moderateScale(image_utility_icon.height);

    var image_setting_icon = ResolveAssetSource(this.state.image.setting_icon.url);

    image.setting_icon.width = moderateScale(image_setting_icon.width);
    image.setting_icon.height = moderateScale(image_setting_icon.height);

    var image_help_icon = ResolveAssetSource(this.state.image.help_icon.url);

    image.help_icon.width = moderateScale(image_help_icon.width);
    image.help_icon.height = moderateScale(image_help_icon.height);

    this.setState({image});
  }


  setHomeRoute(){
    let {userMobilePhoneVerifiedFlag, userRole} = this.props;

    if (!userMobilePhoneVerifiedFlag) {
      return CONSTANT.ROUTE_TYPE.SMS_VERIFICATION;
    }
    else{
      if (userRole == CONSTANT.USER_ROLE.COURIER) {
        return CONSTANT.ROUTE_TYPE.SHIPMENT_SCHEDULE;
      }
      else if (userRole == CONSTANT.USER_ROLE.ADMIN_OUTLET || 
                userRole == CONSTANT.USER_ROLE.OWNER
        ) {
        return CONSTANT.ROUTE_TYPE.DASHBOARD;
      }
    }
  }

  renderTabBar(){
    let {userMobilePhoneVerifiedFlag, userRole, showTabBar} = this.props;
    if (showTabBar) {
      if (!userMobilePhoneVerifiedFlag) {
        return(
            <TabBarUnverifiedAccount {...this.props} {...this.state}/>
        );
      }
      else{
        if (userRole == CONSTANT.USER_ROLE.COURIER) {
          return(
            <TabBarCourier {...this.props} {...this.state}/>
          );
        }
        else if (userRole == CONSTANT.USER_ROLE.ADMIN_OUTLET || 
          userRole == CONSTANT.USER_ROLE.OWNER
          ) {
          return(
            <TabBarAdminOutlet {...this.props} {...this.state}/>
          );
        }
      }
    }
    
    return null;
  }


  render(){
      return(this.renderTabBar());
  }
}

class TabBarCourier extends Component{
  constructor(props) {
        super(props);
        this.state = {
          image:this.props.image,
          homeRoute:this.props.homeRoute,
        }
  }

  render(){
    return(
        <TabBar.MainDashboard
            {...this.props}
            item={
              [
                {
                  itemText:'Beranda',
                  imageSource:CONSTANT.IMAGE_URL.HOME_TAB_ICON,
                  selectedImageSource:CONSTANT.IMAGE_URL.HOME_TAB_SELECTED_ICON,
                  route:this.state.homeRoute,
                },
                {
                  itemText:'Bantuan',
                  imageSource:CONSTANT.IMAGE_URL.HELP_TAB_ICON,
                  selectedImageSource:CONSTANT.IMAGE_URL.HELP_TAB_SELECTED_ICON,
                  route:CONSTANT.ROUTE_TYPE.HELP
                },
                {
                  itemText:'Pengaturan',
                  imageSource:CONSTANT.IMAGE_URL.SETTING_TAB_ICON,
                  selectedImageSource:CONSTANT.IMAGE_URL.SETTING_TAB_SELECTED_ICON,
                  route:CONSTANT.ROUTE_TYPE.SETTING,
                },
              ]
            }
          />
    );
  }
}

class TabBarUnverifiedAccount extends Component{
  constructor(props) {
        super(props);
        this.state = {
          image:this.props.image,
          homeRoute:this.props.homeRoute,
        }
  }

  render(){
    return(
        <TabBar.MainDashboard
            {...this.props}
            item={
              [
                {
                  itemText:'Beranda',
                  imageSource:CONSTANT.IMAGE_URL.HOME_TAB_ICON,
                  selectedImageSource:CONSTANT.IMAGE_URL.HOME_TAB_SELECTED_ICON,
                  route:CONSTANT.ROUTE_TYPE.SMS_VERIFICATION,
                },
                {
                  itemText:'Bantuan',
                  imageSource:CONSTANT.IMAGE_URL.HELP_TAB_ICON,
                  selectedImageSource:CONSTANT.IMAGE_URL.HELP_TAB_SELECTED_ICON,
                  route:CONSTANT.ROUTE_TYPE.HELP
                },
                {
                  itemText:'Pengaturan',
                  imageSource:CONSTANT.IMAGE_URL.SETTING_TAB_ICON,
                  selectedImageSource:CONSTANT.IMAGE_URL.SETTING_TAB_SELECTED_ICON,
                  route:CONSTANT.ROUTE_TYPE.SETTING,
                },
              ]
            }
          />
    );
  }
}

class TabBarAdminOutlet extends Component{
  constructor(props) {
        super(props);
        this.state = {
          image:this.props.image,
          homeRoute:this.props.homeRoute,
        }
  }

  render(){
    return(
        <TabBar.MainDashboard
            {...this.props}
            item={
              [
                {
                  itemText:'Beranda',
                  imageSource:CONSTANT.IMAGE_URL.HOME_TAB_ICON,
                  selectedImageSource:CONSTANT.IMAGE_URL.HOME_TAB_SELECTED_ICON,
                  route:CONSTANT.ROUTE_TYPE.DASHBOARD,
                },
                {
                  itemText:'Peralatan',
                  imageSource:CONSTANT.IMAGE_URL.UTILITY_TAB_ICON,
                  selectedImageSource:CONSTANT.IMAGE_URL.UTILITY_TAB_SELECTED_ICON,
                  route:CONSTANT.ROUTE_TYPE.UTILITY,
                },
                {
                  itemText:'POS',
                  imageSource:CONSTANT.IMAGE_URL.POS_TAB_ICON,
                  selectedImageSource:CONSTANT.IMAGE_URL.POS_TAB_SELECTED_ICON,
                  route:CONSTANT.ROUTE_TYPE.POS_ITEM_SELECTION,
                  routeCategory:'push',
                },
                {
                  itemText:'Bantuan',
                  imageSource:CONSTANT.IMAGE_URL.HELP_TAB_ICON,
                  selectedImageSource:CONSTANT.IMAGE_URL.HELP_TAB_SELECTED_ICON,
                  route:CONSTANT.ROUTE_TYPE.HELP
                },
                {
                  itemText:'Pengaturan',
                  imageSource:CONSTANT.IMAGE_URL.SETTING_TAB_ICON,
                  selectedImageSource:CONSTANT.IMAGE_URL.SETTING_TAB_SELECTED_ICON,
                  route:CONSTANT.ROUTE_TYPE.SETTING,
                },
              ]
            }
          />
    );
  }
}

class TabBarOwner extends Component{
  constructor(props) {
        super(props);
        this.state = {
          image:this.props.image,
          homeRoute:this.props.homeRoute,
        }
  }

  render(){
    return(
        <TabBar.MainDashboard
            {...this.props}
            item={
              [
                {
                  itemText:'Beranda',
                  imageSource:this.state.image.home_icon.url,
                  imageWidth:this.state.image.home_icon.width * this.state.image.home_icon.multiplierWidth,
                  imageHeight:this.state.image.home_icon.height * this.state.image.home_icon.multiplierHeight,
                  route:this.state.homeRoute,
                },
                {   
                  itemText:'POS',   
                  imageSource:this.state.image.home_icon.url,   
                  route:CONSTANT.ROUTE_TYPE.POS_ITEM_SELECTION,   
                  routeCategory:'push',   
                },  
                {
                  itemText:'Peralatan',
                  imageSource:this.state.image.utility_icon.url,
                  imageWidth:this.state.image.utility_icon.width * this.state.image.utility_icon.multiplierWidth,
                  imageHeight:this.state.image.utility_icon.height * this.state.image.utility_icon.multiplierHeight,
                  route:'PartnerKliknKlin.Utility'
                },
                {
                  itemText:'Help',
                  imageSource:this.state.image.help_icon.url,
                  imageWidth:this.state.image.help_icon.width * this.state.image.help_icon.multiplierWidth,
                  imageHeight:this.state.image.help_icon.height * this.state.image.help_icon.multiplierHeight,
                  route:CONSTANT.ROUTE_TYPE.HELP
                },
                {
                  itemText:'Setting',
                  imageSource:this.state.image.setting_icon.url,
                  imageWidth:this.state.image.setting_icon.width * this.state.image.setting_icon.multiplierWidth,
                  imageHeight:this.state.image.setting_icon.height * this.state.image.setting_icon.multiplierHeight,
                  route:'PartnerKliknKlin.Setting'
                },
              ]
            }
          />
    );
  }
}

TabBarMainDashboard.defaultProps = {
}

const styles = StyleSheet.create({

});

function mapStateToProps(state, ownProps) {
  return {
    userRole:state.rootReducer.user.role,
    userMobilePhoneVerifiedFlag:state.rootReducer.user.data.mobilePhoneVerifiedFlag,
    totalImportantMessageNotRead:state.rootReducer.root.totalImportantMessageNotRead,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(Actions.root, dispatch),
    actionNavigator: bindActionCreators(Actions.navigator, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TabBarMainDashboard);



