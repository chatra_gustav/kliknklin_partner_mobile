import React, { Component } from 'react';
import { 
  View,
  Platform, 
  StyleSheet, 
  Image, 
  Dimensions, 
  TouchableOpacity, 
  TouchableWithoutFeedback,
  Modal,
  BackHandler,
  Keyboard,
}from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';

import {Text, OrderOption, Scaling, OutletSelector, Button} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from "./../../../actions";

class NavBarMainDashboard extends Component{
  
  static defaultProps = {
    showNavBar:true,

    //first icon container
    showLogo:false,
    showBackButton:false,

    //middle container
    showTitle:false,
    showOutletSelector:false,
    disableOutletSelector:false,

    //second icon container
    showSearchButton:false,
    searchingType:'normal',

    //third icon container
    showOrderOptionButton:false,
    showNotificationButton:false,
    showCustomButton: false
  }

  constructor(props) {
        super(props);
        this.state = {
        };
  }

  componentWillMount(){
  }

  render(){
    let {showNavBar} = this.props;
    if (showNavBar) {
      return(
        <View style={styles.container}>

            <View style={styles.firstIconContainer}>
              <FirstIconContainer {...this.props} />
            </View>

            <View style={styles.outletSelectorContainer}>
              <MiddleContainer {...this.props} />
            </View>

            <View style={styles.secondIconContainer}>
              <SecondIconContainer {...this.props} />
            </View>

             <View style={styles.thirdIconContainer}>
              <ThirdIconContainer {...this.props} />
            </View>

        </View>
      );
    }
    return null;
  }
}

class FirstIconContainer extends Component{
  render(){
      let {showLogo, showBackButton} = this.props;

      if (showLogo) {
        return (
          <Logo  {...this.props} />
        );
      }
      else if (showBackButton) {
        return(
          <BackButton {...this.props} />
        );
      }
        return null;
  }
}

class MiddleContainer extends Component{

  render(){
      let {showTitle, showOutletSelector} = this.props;

      if (showOutletSelector) {
        return (
          <OutletSelector  {...this.props} />
        );
      }
      else if (showTitle) {
        return(
          <Title {...this.props} />
        );
      }
        return null;
  }
}

class SecondIconContainer extends Component{
  render(){
      let {showSearchButton} = this.props;

      if (showSearchButton) {
        return (
          <SearchButton  {...this.props} />
        );
      }
        return null;
  }
}

class ThirdIconContainer extends Component{
  render(){
      let {showOrderOptionButton, showNotificationButton, showCustomButton, showResetPOSButton} = this.props;

      if (showNotificationButton) {
        return (
          <NotificationButton  {...this.props} />
        );
      }
      if(showOrderOptionButton) {
        return(
          <OrderOption {...this.props} />
        )
      }
      else if (showResetPOSButton) {
        return(
          <ResetPOSButton {...this.props} />
        )
      }
      else if (showCustomButton) {
        return(
          this.props.customButton
        )
      }

      return null;
  }
}


class Logo extends Component{
  constructor(props) {
    super(props);
        this.state = {
        };
  }

  render(){
    return(
      <View
        style={{
          width:Scaling.scale(108 * 0.444),
          height:Scaling.verticalScale(150 * 0.444),
          alignItems:'center',
          justifyContent:'center',
        }}
      >
        <Image
            source={CONSTANT.IMAGE_URL.KLIKNKLIN_ICON}
            style={{
              tintColor: CONSTANT.COLOR.BLUE,
              width:Scaling.scale(CONSTANT.STYLE.IMAGE.NAVBAR_ICON.WIDTH),
              height:Scaling.verticalScale(CONSTANT.STYLE.IMAGE.NAVBAR_ICON.HEIGHT),
              resizeMode:'contain'
            }}
        /> 
      </View>
    );
  }
}

class Title extends Component{
  static defaultProps = {
    title:'',
  }
  constructor(props) {
    super(props);
        this.state = {
        };
  }

  render(){
    return(
      <View
        style={{
          flex:1,
          //alignItems:'center',
          justifyContent:'center',
        }}
      >
        <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>{this.props.title}</Text> 
      </View>
    );
  }
}

class SearchButton extends Component{

  render(){
    return(
      <TouchableOpacity onPress={this.openSearchingPage}>
        <Image
            source={CONSTANT.IMAGE_URL.SEARCH_ICON}
            style={{
              width:Scaling.scale(CONSTANT.STYLE.IMAGE.NAVBAR_ICON.WIDTH * 1.3),
              height:Scaling.verticalScale(CONSTANT.STYLE.IMAGE.NAVBAR_ICON.HEIGHT * 1.3),
              resizeMode:'contain'
            }}
        />
      </TouchableOpacity>
    );
  }

  openSearchingPage = () => {
    if(this.props.searchingType == 'POS_OLD_VERSION'){
      this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.SearchTransaction', {
        searchingType: this.props.searchingType
      });
    }else{
      this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.SEARCH_ORDER, {
        searchingType: this.props.searchingType
      });
    }
  }
}

class NotificationButton extends Component{

  render(){
    return(
      <TouchableOpacity onPress={this._onPressNotification}>
        <View>
          <Image
              source={CONSTANT.IMAGE_URL.NOTIFICATION_ICON}
              style={{
                width:Scaling.scale(CONSTANT.STYLE.IMAGE.NAVBAR_ICON.WIDTH * 1.1),
                height:Scaling.verticalScale(CONSTANT.STYLE.IMAGE.NAVBAR_ICON.HEIGHT * 1.1),
                resizeMode:'contain'
              }}
          />

          {this.renderTotalMessage()}

        </View>
      </TouchableOpacity>
    );
  }

  renderTotalMessage(){
    if(this.props.summaryOrder.isNotificationImportant){
      return(
        <View style={styles.totalNotificationMessage}/>
      )
    }else{
      return null
    }
  }

  _onPressNotification = () => {
    this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.NOTIFICATION_LIST);
  }
}

class ResetPOSButton extends Component{

  render(){
    return(
      <Button.Standard
        onPress={this._onPress}
        buttonText={'Hapus'}
        buttonSize={CONSTANT.STYLE.BUTTON.SMALL}
        buttonColor={CONSTANT.COLOR.GRAY}
        fontOption={CONSTANT.TEXT_OPTION.BUTTON}
      />
    );
  }

  renderTotalMessage(){
    if(this.props.summaryOrder.isNotificationImportant){
      return(
        <View style={styles.totalNotificationMessage}/>
      )
    }else{
      return null
    }
  }

  _onPress = () => {
    this.props.actionPOS.resetPOSData();
  }
}

class BackButton extends Component{
  constructor(props) {
    super(props);
        this.state = {
          isOverride:false,
          route:null,
          routeType:'reset',
          prop:{},
        };
  }



  componentDidUpdate(prevProps, prevState){
    if (prevProps.overrideBackPress.isOverride != this.props.overrideBackPress.isOverride) {
      let {isOverride, route, routeType, props} = this.props.overrideBackPress;

      this.setState({
        isOverride,
        route,
        routeType,
        prop:props,
      });
    }
  }

  _onPress(){
    let {isOverride, route, routeType, prop} = this.state;
    if (isOverride) {
      if (routeType == 'reset') {
        this.props.actionNavigator.resetTo(this.props.navigator, route, prop);
      }
      else if (routeType == 'push') {
        this.props.actionNavigator.push(this.props.navigator, route, prop);
      }

      this.props.actionRoot.setOverrideBackPress(false, null, 'reset', {});
    }
    else{
      this.props.navigator.pop();
    }
  }

  render(){
    return(
      <TouchableOpacity
        style={{
          flex:1,
          justifyContent:'center',
        }}
        onPress={this._onPress.bind(this)}
      >
          <Image
              source={CONSTANT.IMAGE_URL.BACK_BUTTON_ICON}
              style={{
                width:Scaling.scale(CONSTANT.STYLE.IMAGE.NAVBAR_ICON.WIDTH * 1.2),
                height:Scaling.verticalScale(CONSTANT.STYLE.IMAGE.NAVBAR_ICON.HEIGHT * 1.2),
                resizeMode:'contain',
              }}
          />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    backgroundColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BACKGROUND_COLOR,
    height:'7.5%',
    paddingLeft:Scaling.scale(CONSTANT.STYLE.TAB_BAR.MAIN_DASHBOARD.PADDING_LEFT),
    paddingRight:Scaling.scale(CONSTANT.STYLE.TAB_BAR.MAIN_DASHBOARD.PADDING_RIGHT),
    borderBottomWidth:Scaling.moderateScale(CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_BOTTOM_WIDTH),
    borderColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_COLOR,
    flexDirection:'row',
  },
  firstIconContainer:{
    width:Scaling.scale(CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.LOGO_CONTAINER_WIDTH),
    alignItems:'center',
    justifyContent:'center',
  },
  outletSelectorContainer:{
    paddingLeft:Scaling.scale(CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.OUTLET_SELECTOR_CONTAINER_PADDING_LEFT),
    justifyContent:'center',
    width:Scaling.scale(CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.OUTLET_SELECTOR_CONTAINER_WIDTH),
  },
  secondIconContainer:{
    width:Scaling.scale(CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.FIRST_ICON_CONTAINER_WIDTH),
    alignItems:'center',
    justifyContent:'center',
  },
  thirdIconContainer:{
    width:Scaling.scale(CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.SECOND_ICON_CONTAINER_WIDTH),
    alignItems:'center',
    justifyContent:'center',
  },
  totalNotificationMessage:{
    position: 'absolute',
    top: 0,
    right: Scaling.moderateScale(3) * -1,
    width:Scaling.scale(CONSTANT.STYLE.IMAGE.NAVBAR_ICON.WIDTH * 0.6),
    height:Scaling.scale(CONSTANT.STYLE.IMAGE.NAVBAR_ICON.WIDTH * 0.6),
    borderRadius: Scaling.scale(CONSTANT.STYLE.IMAGE.NAVBAR_ICON.WIDTH) / 2.4,
    backgroundColor: CONSTANT.COLOR.RED,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

function mapStateToProps(state, ownProps) {
  return {
    userRole:state.rootReducer.user.role,
    userMobilePhoneVerifiedFlag:state.rootReducer.user.data.mobilePhoneVerifiedFlag,
    totalImportantMessageNotRead:state.rootReducer.root.totalImportantMessageNotRead,
    overrideBackPress:state.rootReducer.root.overrideBackPress,
    summaryOrder:state.rootReducer.order.summary,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(Actions.root, dispatch),
    actionPOS: bindActionCreators(Actions.pos, dispatch),
    actionNavigator: bindActionCreators(Actions.navigator, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NavBarMainDashboard);

