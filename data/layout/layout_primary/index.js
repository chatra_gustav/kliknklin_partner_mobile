import React, { Component } from 'react';
import { 
  View,
  Platform, 
  StyleSheet, 
  Image, 
  Dimensions, 
  TouchableOpacity, 
  TouchableWithoutFeedback,
  Modal,
  BackHandler,
  ScrollView,
  Keyboard,}from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';

import {TextWrapper, TabBar, OrderOption, ConnectionStability, Scaling} from './../../components';
import * as STRING from './../../string';
import * as CONSTANT from './../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from "./../../actions";

//local component
import TabBarMainDashboard from './tab_bar_main_dashboard';
import NavBarMainDashboard from './nav_bar_main_dashboard';

class LayoutPrimary extends Component{
  static defaultProps = {
    showNavBar:true,
    showTabBar:true,
  }

  constructor(props) {
        super(props);
        this.state = {
          image:{
            layout_background: {
              width:0,
              height:0,
              url:require('./../../image/layout/layout_background.png'),
            },
            notification_icon:{
                width:0,
                height:0,
                multiplierWidth:0.1,
                multiplierHeight:0.1,
                url:require('./../../image/layout/notification_icon.png'),
            },
            search_icon:{
                width:0,
                height:0,
                multiplierWidth:0.1,
                multiplierHeight:0.1,
                url:require('./../../image/layout/search_icon.png'),
            },
            home_icon:{
                width:0,
                height:0,
                multiplierWidth:0.5,
                multiplierHeight:0.5,
                url:require('./../../image/layout/home_icon.png'),
            },
            utility_icon:{
                width:0,
                height:0,
                multiplierWidth:0.1,
                multiplierHeight:0.1,
                url:require('./../../image/layout/utility_icon.png'),
            },
            help_icon:{
                width:0,
                height:0,
                multiplierWidth:0.1,
                multiplierHeight:0.1,
                url:require('./../../image/layout/help_icon.png'),
            },
            setting_icon:{
                width:0,
                height:0,
                multiplierWidth:0.1,
                multiplierHeight:0.1,
                url:require('./../../image/layout/setting_icon.png'),
            },
            important_icon:{
                width:0,
                height:0,
                multiplierWidth:0.1,
                multiplierHeight:0.1,
                url:require('./../../image/layout/important_icon.png'),
            }
          },
        };
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  componentDidMount(){
    this.props.actionPushNotif._onNotificationOpenedListener(this.props.navigator);
  }

  componentWillUnmount(){
    this.props.actionPushNotif._removeOnNotificationOpenedListener();
  }


  onNavigatorEvent(event) {

      switch (event.id) {
        case 'willAppear':
          this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
          break;
        case 'willDisappear':
          //this.backHandler.remove();
          break;
        default:
          break;
    }
  }

    handleBackPress = () => {
      console.log('pressed');
      return false;
    }



  _showTitle(){
      if (this.props.showTitle) {
        return(
          <View>
              <TextWrapper
                type={'smallertitle'}
                style={{
                  color:'white',
                  fontWeight:'400',
                }}
              >
              {this.props.titleText}
              </TextWrapper>
          </View>
        );
      }
  }

  _showSearchButton(){
      // if (this.props.showSearchButton) {
      //   return(
      //     <TouchableOpacity 
      //       onPress={() => {
      //         this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.SearchList', {});
      //       }}
      //       underlayColor={'transparent'}
      //       style={{
      //         //position:'absolute',
      //         //right:this.props.showNotificationButton ? 45 : 15,
      //         padding:moderateScale(5),
      //         alignItems:'center',
      //         justifyContent:'center',
      //       }}
      //     >
      //       <ResponsiveImage
      //           source={this.state.image.search_icon.url}
      //           initWidth={this.state.image.search_icon.width * this.state.image.search_icon.multiplierWidth}
      //           initHeight={this.state.image.search_icon.height * this.state.image.search_icon.multiplierHeight}
              
      //         />
      //     </TouchableOpacity>
      //   );
      //}
  }

  _showNotificationButton(){
      let {totalImportantMessageNotRead} = this.props;
      
      let totalImportantMessageComponent = null;

      if (totalImportantMessageNotRead != 0) {
        totalImportantMessageComponent = (
           <View 
              style={{
                position:'absolute',
                right:moderateScale(2.5),
                top:0,
                width:moderateScale(17.5),
                height:moderateScale(17.5),
                borderRadius:moderateScale(15),
                backgroundColor:'red',
                justifyContent:'center',
                alignItems:'center',
              }}
            >
              <TextWrapper 
                type={'base'}
                style={{
                  color:'white',
                }}
              >
              {totalImportantMessageNotRead}
              </TextWrapper>
            </View>

        );
      }

      if (this.props.showNotificationButton) {
        return(
          <TouchableOpacity 
            onPress={() => {
              this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.NotificationList', {});
            }}
            underlayColor={'transparent'}
            style={{
              padding:moderateScale(5),
              alignItems:'center',
              justifyContent:'center',
            }}
          >
            <ResponsiveImage
                source={this.state.image.notification_icon.url}
                initWidth={this.state.image.notification_icon.width * this.state.image.notification_icon.multiplierWidth}
                initHeight={this.state.image.notification_icon.height * this.state.image.notification_icon.multiplierHeight}
              
            />

           {totalImportantMessageComponent}
          </TouchableOpacity>
        );
      }
  }

  _showOrderOption(){
    if (this.props.showOrderOptionButton) {
      return (
        <OrderOption
          {...this.props}
        />
      );
    }
  }

  render(){
      return(
              <View 
                style={[styles.container, this.props.containerStyle]}
              >
                <NavBarMainDashboard {...this.props} />

                <View style={[styles.contentContainer ,{
                  //marginTop:this.props.showNavBar ? Scaling.verticalScale(CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.HEIGHT) : 0,
                  marginBottom:this.props.showTabBar ? Scaling.verticalScale(CONSTANT.STYLE.TAB_BAR.MAIN_DASHBOARD.HEIGHT) : 0,
                }, 
                this.props.contentContainerStyle]}>
                    {this.props.children}
                </View>

                <ConnectionStability />
                
                <TabBarMainDashboard {...this.props} />
          </View> 
      );
  }
}

const styles = StyleSheet.create({
  layoutBackground: {
    position: 'absolute',
  },
  container: {
    flex:1,
  },
  contentContainer:{
    flex:1,
    backgroundColor:CONSTANT.STYLE.CONTAINER.BASE.BACKGROUND_COLOR,
  },

});

function mapStateToProps(state, ownProps) {
  return {
    layoutHeight:state.rootReducer.layout.height,
    userRole:state.rootReducer.user.role,
    userMobilePhoneVerifiedFlag:state.rootReducer.user.data.mobilePhoneVerifiedFlag,
    totalImportantMessageNotRead:state.rootReducer.root.totalImportantMessageNotRead,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(Actions.root, dispatch),
    actionNavigator: bindActionCreators(Actions.navigator, dispatch),
    actionPushNotif: bindActionCreators(Actions.pushNotif, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LayoutPrimary);



