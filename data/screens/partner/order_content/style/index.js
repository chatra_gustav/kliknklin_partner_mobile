import {
  StyleSheet,
  Dimensions,
} from 'react-native';


export const content = StyleSheet.create({
	titleRow:{
		flexDirection:'row', 
		alignItems:'center',
	},
	titleRowImage:{
		flex:0.1,
	},
	titleRowContent:{
		flex:0.4,
	},
	titleRowRightContent:{
		flex:0.5,
		alignItems:'flex-end',
	},
	itemRow:{
		marginTop:Dimensions.get('window').height * 0.01,
		flexDirection:'row',
	},
	itemRowTitle:{
		flex:0.18,
	},
	itemRowColon:{
		flex:0.04,
	},
	itemRowContent:{
		flex:0.78,
		flexDirection:'row',
		alignItems:'center',
	},
	itemDetail:{
		paddingHorizontal:Dimensions.get('window').width * 0.05,
		paddingVertical:Dimensions.get('window').height * 0.01,
		borderColor:'#F0F0F0',
	},
});
