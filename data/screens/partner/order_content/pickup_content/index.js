import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  Alert,
  Linking,
  PermissionsAndroid,
  TouchableOpacity,
  ScrollView,
  AppState,
} from 'react-native';

import ResponsiveImage from 'react-native-responsive-image';
import Collapsible from 'react-native-collapsible';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import {LayoutPrimary} from './../../../../layout';
import {TextWrapper, Container, CustomView} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

//local component
import OrderMap from './../map';
import * as Section from './../section';
import Header from './../header';
import SubmitButton from './../submit_button';
 
class PickupContent extends Component<{}> {
  constructor(props) {
    super(props);
  
    this.state = {
    }
  }

  _renderContent(){
    if (this.props.userRole == CONSTANT.USER_ROLE.COURIER) {
      return(
        <CourierContent
          {...this.props}
        />
      );
    }
    else if (this.props.userRole == CONSTANT.USER_ROLE.ADMIN_OUTLET ||
              this.props.userRole == CONSTANT.USER_ROLE.OWNER
        ) {
      return(
        <AdminOutletContent
          {...this.props}
        />
      );
    }
  }


  render() {
    try{
      let {image, orderData, actionRoot, shipmentActivePickup, userRole} = this.props;
      let {orderlaundry, order_hash, customer, payment, pricedetail, orderitem, appliedpromo, id} = orderData;
      let {order_status_laundry_id} = orderlaundry;
      let {shipment_status_id, shipment_method_id} = shipmentActivePickup;

      return (
        <View style={styles.container}>
              <Header 
                orderID={order_hash} 
                shipmentStatus={shipment_status_id}
                orderStatus={orderlaundry.order_status_laundry_id}
                userRole={userRole}
              />
          
              <ScrollView showsVerticalScrollIndicator={true}>      
                {this._renderContent()}
              </ScrollView>

              <Section.Payment 
                {...this.props}
                image={image} 
                orderPrice={pricedetail.full_price}
                shipmentFare={pricedetail.shipment_fare}
                subtotalPrice={pricedetail.outlet_profit}
                paidStatus={payment.payment_status_id}
                paymentMethod={payment.paymentmethod.info}
                orderitem={orderitem}
                appliedpromo={appliedpromo}
              />

              <SubmitButton
                {...this.props}
                shipmentStatus={shipment_status_id}
                orderStatus={orderlaundry.order_status_laundry_id}
                navigator={this.props.navigator}
                shipmentMethodId={shipment_method_id}
                orderHash={order_hash}
                orderPrice={pricedetail.full_price}
                orderId={id}
                paidStatus={payment.payment_status_id}
              />
        </View>
      )
    }
    catch(error){
      console.log(error, 'pickup_content');
      return null;
    }
  }
}

class CourierContent extends Component{
  render(){
    let {image, orderData, actionRoot, shipmentActivePickup, shipmentActiveDelivery, directionCoords, centerCoords, userRole} = this.props;

    let {orderlaundry, order_hash, customer, payment, pricedetail, orderitem} = orderData;
    let {order_status_laundry_id} = orderlaundry;

    let {shipment_status_id, shipmentschedule, shipment_method_id, internalshipment} = shipmentActivePickup;
    
    let user_courier = null;

    if (internalshipment) {
     let {courier} = internalshipment;
     user_courier = courier;
    }
    
    let userCustomer = customer.user;

    return(
      <View>
          <Section.Customer 
            image={image}
            user_image_url={userCustomer.user_image_url}
            customerName={userCustomer.name}
            customerPhoneNumber={userCustomer.mobile_phone}
          />

          <Section.Destination 
            image={image} 
            address={shipmentschedule.location_address} 
            addressInfo={shipmentschedule.notes}
            shipmentActivePickup={shipmentActivePickup}
            shipmentActiveDelivery={shipmentActiveDelivery}
          />

          <Section.Schedule 
            image={image} 
            shipmentDate={shipmentschedule.shipment_date} 
            shipmentTime={shipmentschedule.shipment_time_info}
          />

          <Section.Courier 
            title={'Kurir Penjemputan'}
            image={image}
            type={'pickup'} 
            courier={user_courier}
            courierType={shipment_method_id}
          />
      </View>
    );
  }
}

class AdminOutletContent extends Component{
  render(){
    try{
      let {image, orderData, actionRoot, shipmentActivePickup, shipmentActiveDelivery, directionCoords, centerCoords} = this.props;

      let {customer, payment, pricedetail, orderitem, appliedpromo} = orderData;
      
      let {shipment_status_id, shipmentschedule, shipment_method_id, internalshipment} = shipmentActivePickup;

      let user_courier = null;

      if (internalshipment) {
       let {courier} = internalshipment;
       user_courier = courier;
      }

      let userCustomer = customer.user;

      return(
        <View>
          <Section.Customer 
            image={image}
            user_image_url={userCustomer.user_image_url}
            customerName={userCustomer.name}
            customerPhoneNumber={userCustomer.mobile_phone}
          />

          <Section.Destination 
            image={image} 
            address={shipmentschedule.location_address} 
            addressInfo={shipmentschedule.notes}
            shipmentActivePickup={shipmentActivePickup}
            shipmentActiveDelivery={shipmentActiveDelivery}
          />

          <Section.Schedule 
            title={'Jadwal Penjemputan'}
            image={image} 
            shipmentDate={shipmentschedule.shipment_date} 
            shipmentTime={shipmentschedule.shipment_time_info}
          />

          <Section.Courier 
            title={'Kurir Penjemputan'}
            image={image}
            type={'pickup'} 
            courier={user_courier}
            courierType={shipment_method_id}
          />
        </View>
      );
    }
    catch(error){
      console.log(error, 'pickup_content.AdminOutletContent')
      return null;
    }
    
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1
  }
});


function mapStateToProps(state, ownProps) {
  return {
    userRole: state.rootReducer.user.role,
    orderData: state.rootReducer.order.active.data,
    orderActive: state.rootReducer.order.active,
    directionCoords: state.rootReducer.map.directionCoords,
    centerCoords: state.rootReducer.map.centerCoords,
    shipmentActivePickup: state.rootReducer.order.shipmentActivePickup,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionUser: bindActionCreators(actions.user, dispatch),
    actionMap: bindActionCreators(actions.map, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PickupContent);

