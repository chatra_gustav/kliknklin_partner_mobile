import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  NativeModules,
  LayoutAnimation,
  ScrollView
} from 'react-native';

import ResponsiveImage from 'react-native-responsive-image';
import Communications from 'react-native-communications';
import ResolveAssetSource from 'resolveAssetSource';
//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../../actions";

import {Avatar, Scaling, Text, TextWrapper, toCurrency} from "./../../../../../components";
import * as CONSTANT from "./../../../../../constant";

//local component
import * as style from './../../style';

const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

class SectionPayment extends Component{

	constructor(props){
		super(props);
		this.state = {
			image:{
		        phone_icon:{
		            width:0,
		            height:0,
		            multiplierWidth:0.45,
		            multiplierHeight:0.45,
		            url:require('./../../../../../image/order_detail/phone_icon.png'),
		        },
		        arrow_down_icon:{
          			width:0,
          			height:0,
          			multiplierWidth:0.4,
          			multiplierHeight:0.4,
          			url:require('app/data/image/iot_machine/arrow_down_icon.png'),
        		},
        		arrow_up_icon:{
          			width:0,
          			height:0,
          			multiplierWidth:0.4,
          			multiplierHeight:0.4,
          			url:require('app/data/image/iot_machine/arrow_up_icon.png'),
        		}
		    },
		    isShowDetailOrder: false
		}
	}

	componentDidMount(){
		this.getImageSize();
	}

	getImageSize(){
	    var image = this.state.image;

	    var phone_icon = ResolveAssetSource(this.state.image.phone_icon.url);

	    image.phone_icon.width = phone_icon.width * this.state.image.phone_icon.multiplierWidth;
	    image.phone_icon.height = phone_icon.height * this.state.image.phone_icon.multiplierHeight;

	    var arrow_down_icon = ResolveAssetSource(this.state.image.arrow_down_icon.url);

	    image.arrow_down_icon.width = arrow_down_icon.width * this.state.image.arrow_down_icon.multiplierWidth;
	    image.arrow_down_icon.height = arrow_down_icon.height * this.state.image.arrow_down_icon.multiplierHeight;

	    var arrow_up_icon = ResolveAssetSource(this.state.image.arrow_up_icon.url);

	    image.arrow_up_icon.width = arrow_up_icon.width * this.state.image.arrow_up_icon.multiplierWidth;
	    image.arrow_up_icon.height = arrow_up_icon.height * this.state.image.arrow_up_icon.multiplierHeight;
	    
	    this.setState({image});
  	}

  	getPaymentStatus(){
  		let {paidStatus} = this.props;

  		if (paidStatus == CONSTANT.PAYMENT_STATUS.PAID) {
  			return(
  				<Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.GREEN}}>{'SUDAH LUNAS'}</Text>
  			)
		}
		else if (paidStatus == CONSTANT.PAYMENT_STATUS.UNPAID) {
			return(
  				<Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.RED}}>{'BELUM LUNAS'}</Text>
  			)
		}
		else if (paidStatus == CONSTANT.PAYMENT_STATUS.WAIT_FOR_APPROVAL){
			return(
  				<Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.RED}}>{'MENUNGGU DISETUJUI'}</Text>
  			)
		}
		else if (paidStatus == CONSTANT.PAYMENT_STATUS.CANCEL) {
			return(
  				<Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.RED}}>{'DIBATALKAN'}</Text>
  			)
		}
  	}

  	getPaymentMethod(){
  		let {orderData} = this.props;
  		let {payment} = orderData;
		let {paymentmethod} = payment;

		return paymentmethod.info;
  	}

  	showDetailOrder = () => {

  		const CustomLayoutLinear = {
	        duration: 200,
	        create: {
	            type: LayoutAnimation.Types.linear,
	            property: LayoutAnimation.Properties.opacity
	        },
	        update: {
	            type: LayoutAnimation.Types.linear
	        },
	    };

	    LayoutAnimation.configureNext(CustomLayoutLinear);

  		this.setState({ isShowDetailOrder: !this.state.isShowDetailOrder })
  	}

	render(){

		return(
			<View style={[style.content.itemDetail, styles.paymentSection]}>
				<View>
					<TouchableOpacity style={styles.rowTitle} onPress={this.showDetailOrder}>
                		<Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>Detail Pesanan</Text>

                		<Image
			            	style={{width: this.state.image.arrow_down_icon.width, height: this.state.image.arrow_down_icon.height}}
			            	source={this.state.image.arrow_down_icon.url}
			          	/>
                	</TouchableOpacity>

                	{this.renderContent()}

				</View>
			</View>
		)
	}

	renderContent(){
		if(this.state.isShowDetailOrder){
			return(
				<ScrollView
					showsVerticalScrollIndicator={false}
					contentContainerStyle={{paddingBottom: Scaling.verticalScale(25)}}>
					{this.getDetailOrder()}
				</ScrollView>
			)
		}else{

			let {orderPrice, paidStatus} = this.props;

			return(
				<View>
					<View style={[styles.rowContent, {justifyContent: 'space-between', borderBottomWidth: 0, marginBottom: Scaling.verticalScale(0)}]}>
		        		<View style={[styles.contentLeft, {width: '35%'}]}>
		        			<Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>{this.getPaymentMethod()}</Text>
		        		</View>

		        		<View style={[styles.contentRight, {width: '65%'}]}>
		        			<Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: (paidStatus == CONSTANT.PAYMENT_STATUS.UNPAID ? CONSTANT.COLOR.RED : CONSTANT.COLOR.BLACK)}}>
			    				{(paidStatus == CONSTANT.PAYMENT_STATUS.UNPAID ? 'BELUM LUNAS' : 'TOTAL BAYAR')} : {toCurrency(orderPrice, 'Rp ')}
			    			</Text>
		        		</View>
		        	</View>

		        	{this.uploadBankTransferEvidence()}
				</View>
			)
		}
	}

	uploadBankTransferEvidence(){
		let {paidStatus} = this.props;
		if (this.getPaymentMethod() == 'Bank Transfer' && paidStatus == CONSTANT.PAYMENT_STATUS.UNPAID) {
			return(
				<View>
	        		<Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.ORANGE}}>
	    				{'Customer belum mengupload bukti transfer'}
	    			</Text>
	        	</View>
			)
		}return null
	}

	getDetailOrder(){
		return(
			<View>
				{this.getOrderItem()}
            	{this.getDiscount()}
            	{this.getDetailPayment()}
			</View>
		)
	}

	getOrderItem(){
		if(this.state.isShowDetailOrder){
			let {orderitem} = this.props;

			return(
				<View>
					<View style={[styles.rowContent]}>
		        		<View style={styles.contentLeft}>
		        			<Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>{this.getPaymentMethod()}</Text>
		        		</View>

		        		<View style={styles.contentRight}>
		        			{this.getPaymentStatus()}
		        		</View>
		        	</View>

		        	{orderitem.map((item, index) => (
		        		<View key={index}>
							<View style={styles.orderDetail}>
				        		<View style={styles.detailLeft}>
				        			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
				        				{item.quantity} {this.getUnitMeasurement(item)}
				        			</Text>
				        		</View>

				        		<View style={styles.detailCenter}>
				        			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>
				        				{this.getItemName(item)}
				        			</Text>
				        		</View>

				        		<View style={styles.detailRight}>
				        			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
				        				{toCurrency(this.getItemPrice(item), 'Rp ')}
				        			</Text>
				        		</View>
				        	</View>
				        	{this.renderWashingServices(item)}
				        	{this.renderItemNotes(item)}
				        </View>
					))}
				</View>
			)
		}else{
			return null
		}
	}

	renderItemNotes(item){
		if(item.notes !== ''){
			return(
				<View style={styles.orderDetail}>
		    		<View style={styles.detailLeft}>
		    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
		    			</Text>
		    		</View>

		    		<View style={styles.detailCenter}>
		    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK, fontStyle: 'italic'}}>
		    				*{item.notes}
		    			</Text>
		    		</View>

		    		<View style={styles.detailRight}>
		    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
		    			</Text>
		    		</View>
		    	</View>
			)
		}
		return null
	}

	renderWashingServices(item){
		let timeservice = item.outlet.outletitemwashingtime.itemwashingtime.timeservice.info;
		let washingservice = item.outlet.outletitemwashingtime.itemwashingtime.washingservice.info;

		return(
			<View style={styles.orderDetail}>
	    		<View style={styles.detailLeft}>
	    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
	    			</Text>
	    		</View>

	    		<View style={styles.detailCenter}>
	    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>
	    				{washingservice+' - '+timeservice}
	    			</Text>
	    		</View>

	    		<View style={styles.detailRight}>
	    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
	    			</Text>
	    		</View>
	    	</View>
		)
	}

	getUnitMeasurement(item){
		try{
			let unit_measurement = item.outlet.outletitemwashingtime.itemwashingtime.item.unit_measurement;
			return unit_measurement
		}catch(err){
			return ""
		}
	}

	getItemName(item){
		try{
			let item_name = item.outlet.outletitemwashingtime.itemwashingtime.item.name;
			return item_name
		}catch(err){
			return ""
		}
	}

	getItemPrice(item){
		try{
			let sub_total_price = item.quantity * item.item_price;
			return sub_total_price
		}catch(err){
			return 0
		}
	}

	getSubTotalPrice(){
		try{
			let {orderitem} = this.props;

			let sub_total_price = 0;

			orderitem.map((item) => {
				sub_total_price += this.getItemPrice(item);
			})

			return sub_total_price
		}catch(err){
			return 0
		}
	}

	getDiscount(){
		let {appliedpromo} = this.props;
		if(appliedpromo.length > 0){
			
			return(
				<View style={styles.discount}>
		    		<View style={styles.detailLeft}>
		    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
		    				{""}
		    			</Text>
		    		</View>

		    		<View style={styles.detailCenter}>
		    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>
		    				{this.getPromoName(appliedpromo)}
		    			</Text>
		    		</View>

		    		<View style={styles.detailRight}>
		    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
		    				{toCurrency(this.getDiscountPrice(), '- Rp ')}
		    			</Text>
		    		</View>
		    	</View>
			)
		}
		return(
			<View style={styles.discount}>
		    </View>
		)
	}

	getPromoName(appliedpromo){
		return appliedpromo[0].promo.promo_name;
	}

	getDiscountPrice(){
		let {appliedpromo} = this.props;
		
		if(appliedpromo.length > 0){
			return appliedpromo[0].discount_price;
		}else{
			return 0
		}
	}

	getShipmentFare(){
		let {shipmentFare} = this.props;

		return shipmentFare;
	}

	getDetailPayment(){
		return(
			<View>
				{this.subTotal()}
				{this.deliveryCharge()}
				{this.discount()}
				{this.totalPrice()}
			</View>
		)
	}

	subTotal(){
		return(
			<View style={styles.orderDetail}>
	    		<View style={styles.detailLeft}>
	    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
	    				{""}
	    			</Text>
	    		</View>

	    		<View style={styles.detailPaymentCenter}>
	    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
	    				{'Total Biaya Cucian'}
	    			</Text>
	    		</View>

	    		<View style={styles.detailPaymentRight}>
	    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
	    				{toCurrency(this.getSubTotalPrice(), 'Rp ')}
	    			</Text>
	    		</View>
	    	</View>
		)
	}

	deliveryCharge(){
		return(
			<View style={styles.orderDetail}>
	    		<View style={styles.detailLeft}>
	    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
	    				{""}
	    			</Text>
	    		</View>

	    		<View style={styles.detailPaymentCenter}>
	    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
	    				{'Biaya Pengiriman'}
	    			</Text>
	    		</View>

	    		<View style={styles.detailPaymentRight}>
	    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
	    				{toCurrency(this.getShipmentFare(), 'Rp ')}
	    			</Text>
	    		</View>
	    	</View>
		)
	}

	discount(){
		return(
			<View style={styles.orderDetail}>
	    		<View style={styles.detailLeft}>
	    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
	    				{""}
	    			</Text>
	    		</View>

	    		<View style={styles.detailPaymentCenter}>
	    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
	    				{'Total Diskon'}
	    			</Text>
	    		</View>

	    		<View style={styles.detailPaymentRight}>
	    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
	    				{toCurrency(this.getDiscountPrice(), '- Rp ')}
	    			</Text>
	    		</View>
	    	</View>
		)
	}

	totalPrice(){
		let {orderPrice, paidStatus} = this.props;
		return(
			<View style={[styles.orderDetail, {paddingVertical: Scaling.verticalScale(10)}]}>
	    		<View style={styles.detailLeft}>
	    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
	    				{""}
	    			</Text>
	    		</View>

	    		<View style={styles.detailPaymentCenter}>
	    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: (paidStatus == CONSTANT.PAYMENT_STATUS.UNPAID ? CONSTANT.COLOR.RED : CONSTANT.COLOR.BLACK)}}>
	    				{(paidStatus == CONSTANT.PAYMENT_STATUS.UNPAID ? 'BELUM LUNAS' : 'TOTAL BAYAR')} 
	    			</Text>
	    		</View>

	    		<View style={styles.detailPaymentRight}>
	    			<Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{fontSize: Scaling.scale(20), color: (paidStatus == CONSTANT.PAYMENT_STATUS.UNPAID ? CONSTANT.COLOR.RED : CONSTANT.COLOR.BLACK)}}>
	    				{toCurrency(orderPrice, 'Rp ')}
	    			</Text>
	    		</View>
	    	</View>
		)
	}

	openMaps = () => {

	}
}

function mapStateToProps(state, ownProps) {
	return {
		orderData: state.rootReducer.order.active.data,
		orderActiveType: state.rootReducer.order.active.orderActiveType,
	    userRole: state.rootReducer.user.role,
	};
}

function mapDispatchToProps(dispatch) {
	return {
	    actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SectionPayment);

const styles = StyleSheet.create({

	paymentSection:{
		backgroundColor: CONSTANT.COLOR.WHITE,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: CONSTANT.COLOR.LIGHTER_GRAY_FONT,
		maxHeight: Dimensions.get('window').height * 0.6
	},
	rowTitle:{
		flexDirection: 'row',
		alignItems: 'center',
		marginVertical: Scaling.verticalScale(10)
	},
	rowContent:{
		flexDirection: 'row',
		alignItems: 'center',
		marginVertical: Scaling.verticalScale(5),
		marginBottom: Scaling.verticalScale(10),
		paddingVertical: Scaling.verticalScale(5),
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: CONSTANT.COLOR.LIGHTER_GRAY_FONT
	},
	column:{
		marginRight: Scaling.moderateScale(15)	
	},
	contentLeft: {
		width: '50%'
	},
	contentRight:{
		width: '50%',
		alignItems: 'flex-end'
	},
	orderDetail:{
		flexDirection: 'row',
		alignItems: 'center',
		marginVertical: Scaling.verticalScale(5)
	},
	detailLeft:{
		width: '15%'
	},
	detailCenter:{
		width: '55%'
	},
	detailRight:{
		width: '30%',
		alignItems: 'flex-end'
	},
	detailPaymentCenter:{
		width: '50%',
		alignItems: 'flex-end'
	},
	detailPaymentRight:{
		width: '35%',
		alignItems: 'flex-end'
	},
	discount:{
		flexDirection: 'row',
		alignItems: 'center',
		marginVertical: Scaling.verticalScale(5),
		paddingBottom: Scaling.verticalScale(10),
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: CONSTANT.COLOR.LIGHTER_GRAY_FONT
	},
	buttonTextStyle:{
	    color: CONSTANT.COLOR.BLACK,
	    fontSize: Scaling.moderateScale(13), 
	    fontFamily: CONSTANT.TEXT_FAMILY.MAIN, 
	    fontWeight: CONSTANT.TEXT_WEIGHT.REGULAR.weight
	},
	buttonStyle:{
		flexDirection: 'row',
	    borderRadius: Scaling.moderateScale(10),
	    height:CONSTANT.STYLE.BUTTON.THICKER_EXTRA_MEDIUM.HEIGHT,
	    width:'100%',
	    alignItems:'center',
		justifyContent:'center',
		borderColor: CONSTANT.COLOR.LIGHT_BLUE,
		borderWidth: 1
	},
	button:{
		width: '100%',
		alignItems: 'center',
		paddingVertical: Scaling.verticalScale(10)
	}
})
