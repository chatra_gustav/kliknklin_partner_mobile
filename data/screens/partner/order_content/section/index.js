import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  View,
  TouchableOpacity,
  Text
} from 'react-native';

import ResponsiveImage from 'react-native-responsive-image';
import Communications from 'react-native-communications';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

import {TextWrapper} from "./../../../../components";
import * as CONSTANT from "./../../../../constant";

//local
import * as style from './../style';
import SectionCourier from './section_courier';
import SectionPayment from './section_payment';
import SectionDestination from './section_destination';
import SectionSchedule from './section_schedule';
import SectionCustomer from './section_customer';
import SectionComplaint from './section_complaint';
import SectionCashier from './section_cashier';

export class Destination extends Component{
	render(){
		return(
			<SectionDestination 
				{...this.props}
			/>
		);
	}
}

export class Schedule extends Component{

	render(){
		return(
			<SectionSchedule
				{...this.props}
			/>
		);
	}
}

export class Customer extends Component{
	render(){
		return(
			<SectionCustomer
				{...this.props}
			/>
		);
	}
}

export class Courier extends Component{

	render(){
		return(
			<SectionCourier
				{...this.props}
			/>
		);
	}
}

export class Complaint extends Component{

	render(){
		return(
			<SectionComplaint
				{...this.props}
			/>
		);
	}
}

export class Cashier extends Component{

	render(){
		return(
			<SectionCashier
				{...this.props}
			/>
		);
	}
}

export class InvoiceSummary extends Component{

	render(){
		return(
			<View style={[style.content.itemDetail, {borderBottomWidth: this.props.endContent ? 0 : 1} ]}>
			
				<View style={style.content.titleRow}>
					<View style={style.content.titleRowImage}>
						<ResponsiveImage
							source={this.props.image.invoice_icon.url}
							initWidth={this.props.image.invoice_icon.width * this.props.image.invoice_icon.multiplierWidth}
							initHeight={this.props.image.invoice_icon.height * this.props.image.invoice_icon.multiplierHeight}
	                    />
					</View>

					<View style={style.content.titleRowContent}>
						<TextWrapper type={'base'} style={{color:'#69CAF0'}}>
							Pembayaran
						</TextWrapper>
					</View>

					<View style={style.content.titleRowRightContent}>
						<TouchableOpacity
							style={{
								borderRadius:10,
								borderWidth:1.5,
								borderColor:CONSTANT.COLOR.BLUE_A,
								//elevation:2,
								width:Dimensions.get('window').width * 0.35,
								alignItems:'center',
							}}

							onPress={this.props.onPressInvoice}
						>
							<TextWrapper fontWeight={'SemiBold'} type={'base'} style={{color:CONSTANT.COLOR.BLUE_A}}>Rincian Pesanan</TextWrapper>
						</TouchableOpacity>
					</View>
				</View>


				<View
					style={{
						alignItems:'center',
						marginVertical:Dimensions.get('window').height * 0.008,
					}}
				>
					<TextWrapper type={'bigtitle'} style={{fontWeight:'bold'}}>Rp 10.000</TextWrapper>
					<TextWrapper type={'base'} style={{marginTop:Dimensions.get('window').height * 0.005}}>Jumlah yang harus ditagih ke konsumen</TextWrapper>
				</View>
			</View>
		);
	}
}

export class Payment extends Component{
	

	render(){
		return(
			<SectionPayment
				{...this.props}
			/>

		);
	}
}
