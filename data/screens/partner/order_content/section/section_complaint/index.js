import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  View,
  TouchableOpacity
} from 'react-native';

import ResponsiveImage from 'react-native-responsive-image';
import Communications from 'react-native-communications';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../../actions";

import {TextWrapper, DateFormat, Text, Scaling} from "./../../../../../components";
import * as CONSTANT from "./../../../../../constant";

//local component
import * as style from './../../style';

class SectionComplaint extends Component{
	render(){
		let {customerName, customerPhoneNumber, complaintReason, complaintInfo} = this.props;
		return(
			<View style={[style.content.itemDetail, styles.complaintSection]}>
				<View>
					<View style={styles.rowTitle}>
	            		<Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_FONT_GRAY}}>Keluhan</Text>
	            	</View>

	            	<View style={styles.rowContent}>
                		<View style={styles.contentLeft}>
                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Keluhan</Text>
                		</View>

                		<View style={styles.contentCenter}>
                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>:</Text>
                		</View>

                		<View style={styles.contentRight}>
                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{complaintReason}</Text>
                		</View>
                	</View>

                	<View style={styles.rowContent}>
                		<View style={styles.contentLeft}>
                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Keterangan</Text>
                		</View>

                		<View style={styles.contentCenter}>
                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>:</Text>
                		</View>

                		<View style={styles.contentRight}>
                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{complaintInfo}</Text>
                		</View>
                	</View>

				</View>

			</View>
		)
	}
}

function mapStateToProps(state, ownProps) {
	return {
		orderData: state.rootReducer.order.active.data,
		orderActiveType: state.rootReducer.order.active.orderActiveType,
	    userRole: state.rootReducer.user.role,
	};
}

function mapDispatchToProps(dispatch) {
	return {
	    actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SectionComplaint);

const styles = StyleSheet.create({
	complaintSection:{
		flex: 1,
		backgroundColor: CONSTANT.COLOR.WHITE,
		marginBottom: Scaling.verticalScale(20),
		elevation:2
	},
	row:{
		marginVertical: Scaling.verticalScale(5)
	},
	column:{
		marginHorizontal: Scaling.verticalScale(5)	
	},
	rowTitle:{
		marginVertical: Scaling.verticalScale(15)
	},
	rowContent:{
		flex: 1,
		flexDirection: 'row',
		marginVertical: Scaling.verticalScale(5)
	},
	contentLeft: {
		flex: 0.2
	},
	contentCenter:{
		flex: 0.1,
		alignItems: 'center'
	},
	contentRight:{
		flex: 0.7
	}
})

