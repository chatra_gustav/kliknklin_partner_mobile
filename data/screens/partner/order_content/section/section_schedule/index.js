import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  View,
  TouchableOpacity,
  Image
} from 'react-native';

import ResponsiveImage from 'react-native-responsive-image';
import Communications from 'react-native-communications';
import ResolveAssetSource from 'resolveAssetSource';
//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../../actions";

import {Avatar, Scaling, Text, TextWrapper, DateFormat} from "./../../../../../components";
import * as CONSTANT from "./../../../../../constant";

//local component
import * as style from './../../style';

class SectionSchedule extends Component{

	constructor(props){
		super(props);
		this.state = {
			image:{
		        phone_icon:{
		            width:0,
		            height:0,
		            multiplierWidth:0.45,
		            multiplierHeight:0.45,
		            url:require('./../../../../../image/order_detail/phone_icon.png'),
		        },
		        sms_icon:{
		            width:0,
		            height:0,
		            multiplierWidth:0.45,
		            multiplierHeight:0.45,
		            url:require('./../../../../../image/order_detail/sms_icon.png'),
		        }
		    }
		}
	}

	componentDidMount(){
		this.getImageSize();
	}

	getImageSize(){
	    var image = this.state.image;

	    var phone_icon = ResolveAssetSource(this.state.image.phone_icon.url);

	    image.phone_icon.width = phone_icon.width * this.state.image.phone_icon.multiplierWidth;
	    image.phone_icon.height = phone_icon.height * this.state.image.phone_icon.multiplierHeight;

	    var sms_icon = ResolveAssetSource(this.state.image.sms_icon.url);

	    image.sms_icon.width = sms_icon.width * this.state.image.sms_icon.multiplierWidth;
	    image.sms_icon.height = sms_icon.height * this.state.image.sms_icon.multiplierHeight;
	    
	    this.setState({image});
  	}

	render(){
		let {shipmentDate, shipmentTime, title, shipmentMethodId} = this.props;

		if(shipmentMethodId == CONSTANT.SHIPMENT_TYPE.SELF_SHIPMENT){
			title = 'Jadwal Penjemputan'
		}

		return(
			<View style={[style.content.itemDetail, styles.destinationSection]}>
				<View>
					<View style={styles.rowTitle}>
                		<Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_FONT_GRAY}}>{title}</Text>
                	</View>

                	<View style={styles.rowContent}>
                		<View style={styles.contentLeft}>
                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Tanggal</Text>
                		</View>

                		<View style={styles.contentCenter}>
                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>:</Text>
                		</View>

                		<View style={styles.contentRight}>
                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{DateFormat(shipmentDate, 'fullDate')}</Text>
                		</View>
                	</View>

                	<View style={styles.rowContent}>
                		<View style={styles.contentLeft}>
                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Waktu</Text>
                		</View>

                		<View style={styles.contentCenter}>
                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>:</Text>
                		</View>

                		<View style={styles.contentRight}>
                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{shipmentTime}</Text>
                		</View>
                	</View>
				</View>

			</View>
		)
	}
}

function mapStateToProps(state, ownProps) {
	return {
		orderData: state.rootReducer.order.active.data,
		orderActiveType: state.rootReducer.order.active.orderActiveType,
	    userRole: state.rootReducer.user.role,
	};
}

function mapDispatchToProps(dispatch) {
	return {
	    actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SectionSchedule);

const styles = StyleSheet.create({
	destinationSection:{
		flex: 1,
		backgroundColor: CONSTANT.COLOR.WHITE,
		marginBottom: Scaling.verticalScale(20),
		elevation:2
	},
	rowTitle:{
		marginVertical: Scaling.verticalScale(10)
	},
	rowContent:{
		flex: 1,
		flexDirection: 'row',
		marginVertical: Scaling.verticalScale(5)
	},
	column:{
		marginHorizontal: Scaling.verticalScale(5)	
	},
	contentLeft: {
		flex: 0.15
	},
	contentCenter:{
		flex: 0.1,
		alignItems: 'center'
	},
	contentRight:{
		flex: 0.75,
	}
})
