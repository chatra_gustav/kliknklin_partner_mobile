import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  Linking
} from 'react-native';

import ResponsiveImage from 'react-native-responsive-image';
import Communications from 'react-native-communications';
import ResolveAssetSource from 'resolveAssetSource';
//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../../actions";

import {Avatar, Scaling, Text, TextWrapper, DateFormat} from "./../../../../../components";
import * as CONSTANT from "./../../../../../constant";

//local component
import * as style from './../../style';

class SectionCahsier extends Component{

	constructor(props){
		super(props);
		this.state = {
			image:{
		        maps_icon:{
		            width:0,
		            height:0,
		            multiplierWidth:0.3,
		            multiplierHeight:0.3,
		            url:require('./../../../../../image/order_detail/maps_icon.png'),
		        }
		    }
		}
	}

	componentDidMount(){
		this.getImageSize();
	}

	getImageSize(){
	    var image = this.state.image;

	    var maps_icon = ResolveAssetSource(this.state.image.maps_icon.url);

	    image.maps_icon.width = maps_icon.width * this.state.image.maps_icon.multiplierWidth;
	    image.maps_icon.height = maps_icon.height * this.state.image.maps_icon.multiplierHeight;
	    
	    this.setState({image});
  	}

  	isPosOrder(){
  		let {orderID} = this.props;
    	return orderID.includes('POS');
  	}

  	_getCashierName(){
  		let {orderData} = this.props;
  		let {orderlaundry} = orderData;
  		let {log} = orderlaundry;
  		let cashier_name  = '';

  		log.map((log_order) => {
    		if(log_order.order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_LAUNDRY){
	      		cashier_name = log_order.user.name;
	    	}
  		})

  		return cashier_name
	}

	_getCashierPhoneNumber(){
  		let {orderData} = this.props;
  		let {orderlaundry} = orderData;
  		let {log} = orderlaundry;
  		let cashier_phone_number  = '';

  		log.map((log_order) => {
    		if(log_order.order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_LAUNDRY){
	      		cashier_phone_number = log_order.user.mobile_phone;
	    	}
  		})

  		return cashier_phone_number
	}

	_getDate(){
		let {orderData} = this.props;
		let date = orderData.created_at.split(' ')[0];
		
		return DateFormat(date, 'fullDate');
  		
	}

	_getTime(){
		let {orderData} = this.props;
		let time_arr = orderData.created_at.split(' ')[1];
		let time = time_arr.split(':');
		
		return time[0]+' : '+time[1];
  		
	}

	render(){
		if(this.isPosOrder()){
			return(
				<View style={[style.content.itemDetail, styles.destinationSection]}>
					<View>
						<View style={styles.rowTitle}>
	                		<Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_FONT_GRAY}}>Kasir</Text>
	                	</View>

	                	<View style={styles.rowContent}>
	                		<View style={styles.contentLeft}>
	                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Nama</Text>
	                		</View>

	                		<View style={styles.contentCenter}>
	                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>:</Text>
	                		</View>

	                		<View style={styles.contentRight}>
	                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{this._getCashierName()}</Text>
	                		</View>
	                	</View>

	                	<View style={styles.rowContent}>
	                		<View style={styles.contentLeft}>
	                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Tanggal dibuat</Text>
	                		</View>

	                		<View style={styles.contentCenter}>
	                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>:</Text>
	                		</View>

	                		<View style={styles.contentRight}>
	                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{this._getDate()}  {this._getTime()}</Text>
	                		</View>
	                	</View>
					</View>
				</View>
			)
		}else{
			return null
		}
	}
}

function mapStateToProps(state, ownProps) {
	return {
		orderData: state.rootReducer.order.active.data,
		orderActiveType: state.rootReducer.order.active.orderActiveType,
	    userRole: state.rootReducer.user.role,
	};
}

function mapDispatchToProps(dispatch) {
	return {
	    actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SectionCahsier);

const styles = StyleSheet.create({
	destinationSection:{
		flex: 1,
		backgroundColor: CONSTANT.COLOR.WHITE,
		marginBottom: Scaling.verticalScale(20),
		elevation:2
	},
	rowTitle:{
		marginVertical: Scaling.verticalScale(10)
	},
	rowContent:{
		flex: 1,
		flexDirection: 'row',
		marginVertical: Scaling.verticalScale(5)
	},
	column:{
		marginLeft: Scaling.moderateScale(3)	
	},
	contentLeft: {
		flex: 0.25
	},
	contentCenter:{
		flex: 0.1,
		alignItems: 'center'
	},
	contentRight:{
		flex: 0.65,
	},
	buttonTextStyle:{
	    color: CONSTANT.COLOR.BLACK,
	    fontSize: Scaling.moderateScale(13), 
	    fontFamily: CONSTANT.TEXT_FAMILY.MAIN, 
	    fontWeight: CONSTANT.TEXT_WEIGHT.REGULAR.weight
	},
	buttonStyle:{
		flexDirection: 'row',
	    borderRadius: Scaling.moderateScale(10),
	    width:Dimensions.get('window').width * 0.45,
		height:CONSTANT.STYLE.BUTTON.THICKER_EXTRA_MEDIUM.HEIGHT,
	    alignItems:'center',
		justifyContent:'center',
	},
	button:{
		width: '100%',
		alignItems: 'center',
		paddingVertical: Scaling.verticalScale(25)
	}
})
