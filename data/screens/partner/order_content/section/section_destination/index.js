import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  Linking
} from 'react-native';

import ResponsiveImage from 'react-native-responsive-image';
import Communications from 'react-native-communications';
import ResolveAssetSource from 'resolveAssetSource';
//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../../actions";

import {Avatar, Scaling, Text, TextWrapper} from "./../../../../../components";
import * as CONSTANT from "./../../../../../constant";

//local component
import * as style from './../../style';

class SectionDestination extends Component{

	constructor(props){
		super(props);
		this.state = {
			image:{
		        maps_icon:{
		            width:0,
		            height:0,
		            multiplierWidth:0.3,
		            multiplierHeight:0.3,
		            url:require('./../../../../../image/order_detail/maps_icon.png'),
		        }
		    }
		}
	}

	componentDidMount(){
		this.getImageSize();
	}

	getImageSize(){
	    var image = this.state.image;

	    var maps_icon = ResolveAssetSource(this.state.image.maps_icon.url);

	    image.maps_icon.width = maps_icon.width * this.state.image.maps_icon.multiplierWidth;
	    image.maps_icon.height = maps_icon.height * this.state.image.maps_icon.multiplierHeight;
	    
	    this.setState({image});
  	}

  	isPosOrder(){
  		let {orderData} = this.props;
  		let {order_hash} = orderData;
    	return order_hash.includes('POS');
  	}

	render(){
		if(this.isPosOrder()){
			return null
		}else{
			let {addressInfo, address} = this.props;

			return(
				<View style={[style.content.itemDetail, styles.destinationSection]}>
					<View>
						<View style={styles.rowTitle}>
	                		<Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_FONT_GRAY}}>Lokasi Pelanggan</Text>
	                	</View>

	                	<View style={styles.rowContent}>
	                		<View style={styles.contentLeft}>
	                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Alamat</Text>
	                		</View>

	                		<View style={styles.contentCenter}>
	                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>:</Text>
	                		</View>

	                		<View style={styles.contentRight}>
	                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{address}</Text>
	                		</View>
	                	</View>

	                	<View style={styles.rowContent}>
	                		<View style={styles.contentLeft}>
	                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Catatan</Text>
	                		</View>

	                		<View style={styles.contentCenter}>
	                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>:</Text>
	                		</View>

	                		<View style={styles.contentRight}>
	                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{addressInfo}</Text>
	                		</View>
	                	</View>
					</View>

					<View style={styles.button}>
			            <TouchableOpacity
							style={[{backgroundColor: CONSTANT.COLOR.LIGHT_GRAY}, styles.buttonStyle]}
							onPress={this.openMaps}
					    >
					    	<TextWrapper
					    		type={this.props.buttonTextType} 
					    		fontWeight={'SemiBold'}
								style={[styles.buttonTextStyle]}
					    	>
								{'Lihat Arah Maps'}
					    	</TextWrapper>

					    	<View style={styles.column}>
			                	<Image
			                    	style={{width: this.state.image.maps_icon.width, height: this.state.image.maps_icon.height}}
			                    	source={this.state.image.maps_icon.url}
			                  	/>
			                </View>
					    </TouchableOpacity>
	          		</View>
				</View>
			)
		}
	}

	openMaps = () => {
		try{
			let { orderData, orderActiveType, shipmentActivePickup, shipmentActiveDelivery } = this.props;
			let location = {};

			if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP
				) {
				
				let {shipmentschedule} = shipmentActivePickup;
				let {geolocation} = shipmentschedule;
				location['latitude'] = geolocation.coordinates[1];
				location['longitude'] = geolocation.coordinates[0];

			}
			else if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY
				) {
			
				let {shipmentschedule} = shipmentActiveDelivery;
				let {geolocation} = shipmentschedule;

				location['latitude'] = geolocation.coordinates[1];
				location['longitude'] = geolocation.coordinates[0];
			}
			
			Linking.canOpenURL('http://maps.google.com/maps?daddr='+location.latitude+','+location.longitude).then(supported => {
				if (supported) {
				    Linking.openURL('http://maps.google.com/maps?daddr='+location.latitude+','+location.longitude +'&avoid=tolls&avoid=highways');
				} else {
				    console.log('Don\'t know how to go');
				}
			}).catch(err => console.error('An error occurred', err));
		}
		catch(error){
			console.log(error);
			alert('gagal membuka maps');
		}
	}
}

function mapStateToProps(state, ownProps) {
	return {
		orderData: state.rootReducer.order.active.data,
		orderActiveType: state.rootReducer.order.active.orderActiveType,
	    userRole: state.rootReducer.user.role,
	};
}

function mapDispatchToProps(dispatch) {
	return {
	    actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SectionDestination);

const styles = StyleSheet.create({
	destinationSection:{
		flex: 1,
		backgroundColor: CONSTANT.COLOR.WHITE,
		marginBottom: Scaling.verticalScale(20),
		elevation:2
	},
	rowTitle:{
		marginVertical: Scaling.verticalScale(10)
	},
	rowContent:{
		flex: 1,
		flexDirection: 'row',
		marginVertical: Scaling.verticalScale(5)
	},
	column:{
		marginLeft: Scaling.moderateScale(3)	
	},
	contentLeft: {
		flex: 0.15
	},
	contentCenter:{
		flex: 0.1,
		alignItems: 'center'
	},
	contentRight:{
		flex: 0.75,
	},
	buttonTextStyle:{
	    color: CONSTANT.COLOR.BLACK,
	    fontSize: Scaling.moderateScale(13), 
	    fontFamily: CONSTANT.TEXT_FAMILY.MAIN, 
	    fontWeight: CONSTANT.TEXT_WEIGHT.REGULAR.weight
	},
	buttonStyle:{
		flexDirection: 'row',
	    borderRadius: Scaling.moderateScale(10),
	    width:Dimensions.get('window').width * 0.45,
		height:CONSTANT.STYLE.BUTTON.THICKER_EXTRA_MEDIUM.HEIGHT,
	    alignItems:'center',
		justifyContent:'center',
	},
	button:{
		width: '100%',
		alignItems: 'center',
		paddingVertical: Scaling.verticalScale(25)
	}
})
