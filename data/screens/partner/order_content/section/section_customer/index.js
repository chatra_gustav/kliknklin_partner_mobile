import React, { Component } from 'react';

import {
  StyleSheet,
  Dimensions,
  View,
  TouchableOpacity,
  Image
} from 'react-native';

import ResponsiveImage from 'react-native-responsive-image';
import Communications from 'react-native-communications';
import ResolveAssetSource from 'resolveAssetSource';
//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../../actions";

import {Avatar, Scaling, Text} from "./../../../../../components";
import * as CONSTANT from "./../../../../../constant";

//local component
import * as style from './../../style';

class SectionCustomer extends Component{

	constructor(props){
		super(props);
		this.state = {
			image:{
		        phone_icon:{
		            width:0,
		            height:0,
		            multiplierWidth:0.3,
		            multiplierHeight:0.3,
		            url:require('./../../../../../image/order_detail/phone_icon2.png'),
		        },
		        sms_icon:{
		            width:0,
		            height:0,
		            multiplierWidth:0.3,
		            multiplierHeight:0.3,
		            url:require('./../../../../../image/order_detail/sms_icon2.png'),
		        }
		    }
		}
	}

	componentDidMount(){
		this.getImageSize();
	}

	getImageSize(){
	    var image = this.state.image;

	    var phone_icon = ResolveAssetSource(this.state.image.phone_icon.url);

	    image.phone_icon.width = phone_icon.width * this.state.image.phone_icon.multiplierWidth;
	    image.phone_icon.height = phone_icon.height * this.state.image.phone_icon.multiplierHeight;

	    var sms_icon = ResolveAssetSource(this.state.image.sms_icon.url);

	    image.sms_icon.width = sms_icon.width * this.state.image.sms_icon.multiplierWidth;
	    image.sms_icon.height = sms_icon.height * this.state.image.sms_icon.multiplierHeight;
	    
	    this.setState({image});
  	}

	render(){
		let {customerName, customerPhoneNumber, user_image_url} = this.props;
		return(
			<View style={[style.content.itemDetail, styles.customerSection, this.props.containerStyle]}>
				<View style={{flex: 0.35}}>
					<Avatar 
	                  size={Scaling.scale(120)}
	                  img={user_image_url}
	                />
	            </View>

                <View style={{flex: 0.4}}>
                	<View style={styles.row}>
                		<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{customerName}</Text>
                	</View>
                	<View style={styles.row}>
                		<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{customerPhoneNumber}</Text>
                	</View>
                </View>

                <View style={{flexDirection: 'row', alignItems: 'center', flex: 0.25, justifyContent: 'flex-end'}}>

		            <TouchableOpacity onPress={() => Communications.text(customerPhoneNumber)}>
		                <View style={styles.column}>
		                  	<Image
		                    	style={{width: this.state.image.sms_icon.width, height: this.state.image.sms_icon.height}}
		                    	source={this.state.image.sms_icon.url}
		                  	/>
		                </View>
	                </TouchableOpacity>

	                <TouchableOpacity onPress={() => Communications.phonecall(customerPhoneNumber, true)}>
	                	<View style={styles.column}>
		                	<Image
		                    	style={{width: this.state.image.phone_icon.width, height: this.state.image.phone_icon.height}}
		                    	source={this.state.image.phone_icon.url}
		                  	/>
		                </View>
		            </TouchableOpacity>
                </View>
			</View>
		)
	}
}

function mapStateToProps(state, ownProps) {
	return {
		orderData: state.rootReducer.order.active.data,
		orderActiveType: state.rootReducer.order.active.orderActiveType,
	    userRole: state.rootReducer.user.role,
	};
}

function mapDispatchToProps(dispatch) {
	return {
	    actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SectionCustomer);

const styles = StyleSheet.create({
	customerSection:{
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		paddingTop: 0,
		paddingBottom:Dimensions.get('window').height * 0.01,
		backgroundColor: CONSTANT.COLOR.WHITE,
		marginBottom: Scaling.verticalScale(20),
		elevation:2,
		borderColor:'black',
		borderWidth:0,
	},
	row:{
		marginVertical: Scaling.verticalScale(5)
	},
	column:{
		marginHorizontal: Scaling.verticalScale(2)	
	}
})
