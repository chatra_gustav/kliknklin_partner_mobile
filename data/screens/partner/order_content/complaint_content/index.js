import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Dimensions,
  Alert,
  Linking,
  PermissionsAndroid,
  TouchableOpacity,
  ScrollView,
  AppState,
} from 'react-native';

import ResponsiveImage from 'react-native-responsive-image';
import Collapsible from 'react-native-collapsible';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import {LayoutPrimary} from './../../../../layout';
import {TextWrapper, Container, CustomView, TabBar, Scaling, Text} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

//local component
import OrderMap from './../map';
import * as Section from './../section';
import Header from './../header';
import SubmitButton from './../submit_button';
import ComplaintMessage from './complaint_message';
import ComplaintItem from './complaint_item';
import ComplaintInfo from './complaint_info';
 
class ComplaintContent extends Component<{}> {
  constructor(props) {
    super(props);
  
    this.state = {
      currentTabKey:CONSTANT.COMPLAINT_TAB_TYPE.FIRST,
    }
  }

  componentWillMount(){
    this.setup();
  }

  async setupComplaintInfo(){
    let {orderData} = this.props;
    let {complaint} = orderData;
    let complaintItems = complaint.items;
    let complaintStatusID = complaint.complaint_status_id;
    let {message, isImportant} = await this.props.actionOrder.getComplaintInfo(complaintItems, complaintStatusID);

    this.setState({message, isImportant});
  }

  setup(){
    this.props.actionMasterData.getComplaintItemSolution();

    this.setupComplaintInfo();
  }

  onPress(key){
    if(key == 0)
      this.setState({currentTabKey:CONSTANT.COMPLAINT_TAB_TYPE.FIRST});
    else
      this.setState({currentTabKey:CONSTANT.COMPLAINT_TAB_TYPE.SECOND});
  }

  getComplaintContent(){
    let {orderData} = this.props;
    let {complaint} = orderData;
    let {currentTabKey} = this.state;
    if (currentTabKey == CONSTANT.COMPLAINT_TAB_TYPE.FIRST) {

      if (complaint.complaint_reason_id == CONSTANT.COMPLAINT_REASON.ITEM_DAMAGE) {

        return(
          <ComplaintItem
            {...this.props}
          />
        );

      }else{

        return(
          <ComplaintDetail 
            {...this.props}
          />
        );

      }
    }
    else if (currentTabKey == CONSTANT.COMPLAINT_TAB_TYPE.SECOND) {
      return(
        <ComplaintMessage
          {...this.props}
        />
      );
    }
  }

  getComplaintHeader(){
    try{
      let {orderData, image, userRole} = this.props;
      let {complaint, read_flag, important, customer, order_hash} = orderData;

      return(
        <View
          style={{
            width:'100%',
          }}
        >
          <Header 
              orderID={order_hash}
              complaintStatus={complaint.complaint_status_id}
              userRole={userRole}
          />

        </View>
      );
    }
    catch(error){
      console.log(error);
    }
  }

  getComplaintInfo(){
    try{
      let {orderData, image, userRole} = this.props;
      let {complaint} = orderData;

      return(
        <View
          style={{
            width:'100%',
            backgroundColor:this.state.isImportant ? CONSTANT.COLOR.RED_STATUS_BACKGROUND : CONSTANT.COLOR.GREEN_STATUS_BACKGROUND,
            height:Scaling.verticalScale(35),
            elevation:2,
            alignItems:'center',
            justifyContent:'center',
          }}
        >
          <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}
            textStyle={{
              color:this.state.isImportant ? CONSTANT.COLOR.RED_STATUS_TEXT : CONSTANT.COLOR.GREEN_STATUS_TEXT,
            }}
          >{this.state.message}</Text>

        </View>
      );
    }
    catch(error){
      console.log(error);
    }
  }

  renderSubmitButton(){
    let {currentTabKey} = this.state;
    let {orderData} = this.props;
    let {complaint} = orderData;

    if (currentTabKey == CONSTANT.COMPLAINT_TAB_TYPE.FIRST) {
      return(
        <View style={{width:'100%', position:'absolute', bottom:'5%'}}>
          <SubmitButton
            complaintStatus={complaint.complaint_status_id}
            navigator={this.props.navigator}
            buttonStyle={{backgroundColor: 'transparent'}}
            onPress={() => {
              this.setupComplaintInfo();
            }}
          />
        </View>
      );
    }
  }

  render() {
    try{
      let {image, orderData, actionRoot, orderActiveType, shipmentActivePickup, directionCoords, centerCoords,userRole} = this.props;

      let {currentTabKey} = this.state;
      let {orderlaundry, order_hash, customer, payment, pricedetail, orderitem, appliedpromo, complaint} = orderData;
      
      let userCustomer = customer.user;

      return (
        <View>

          {this.getComplaintInfo()}
          
          <KeyboardAwareScrollView
            contentContainerStyle={{paddingBottom: Scaling.verticalScale(currentTabKey == CONSTANT.COMPLAINT_TAB_TYPE.SECOND ? 0 : 115)}}
            showsVerticalScrollIndicator={false}
          >

            <View
              style={{
                marginTop:Scaling.verticalScale(15),
                elevation:2,
                backgroundColor:CONSTANT.COLOR.WHITE
              }}
            >
              {this.getComplaintHeader()}
            
              <Section.Customer 
                image={image}
                user_image_url={userCustomer.user_image_url}
                customerName={userCustomer.name}
                customerPhoneNumber={userCustomer.mobile_phone}
                endContent
                containerStyle={{
                  elevation:0
                }}
              />
            </View>

            <ComplaintTab
              {...this.props}
              onPress={this.onPress.bind(this)}
              currentTabKey={this.state.currentTabKey}
            />

            {this.getComplaintContent()}
          </KeyboardAwareScrollView>


          {this.renderSubmitButton()}
        </View>
      )
    }
    catch(error){
      console.log(error, 'complaint_content');
      return null;
    }
  }
}

class ComplaintDetail extends Component{

  render(){
    try{
      let {orderData, image} = this.props;
      let {complaint, read_flag, important, customer} = orderData;

      return(
        <View>
            <Section.Complaint
              image={image}
              customerName={customer.user.name}
              customerPhoneNumber={customer.user.mobile_phone}
              complaint_reason_id={complaint.complaint_reason_id}
              complaintReason={complaint.complaintreason.info}
              complaintInfo={complaint.complaint_details}
              endContent
            />
        </View>
      );
    }
    catch(error){
      console.log(error);
    }
  }
}

class ComplaintTab extends Component{
  render(){
    let {orderData} = this.props;
    let {complaint} = orderData;

    if (orderData.read_flag == null) {
      orderData.read_flag = 0;
    }

    let item = [
      {
        itemText:CONSTANT.COMPLAINT_TAB_TYPE.FIRST,
      },
      {
        itemText:CONSTANT.COMPLAINT_TAB_TYPE.SECOND,
        isShowImportantSign:orderData.read_flag,
      },
    ]

    return(
      <View style={{paddingBottom: Scaling.verticalScale(10), marginTop: Scaling.verticalScale(10)}}>
        <TabBar.TypeD
          {...this.props}
          item={item}
        />
      </View>
    );
  }
}

class HeaderComplaint extends Component<{}> {
  constructor(props) {
    super(props);
  
    this.state = {
    }
  }

  render() {
    try{
      return (
        <View style={styles.container}>
          <TabBar.TypeB
            {...this.props}
            item={[
              {
                itemText:'Rincian',
              },
              {
                itemText:'Pesan',
                //rightItem:rightItem,
              },
              {
                itemText:'Barang',
                //rightItem:rightItem,
              },
            ]}
          />
        </View>
      )
    }
    catch(error){
      console.log(error, 'complaint_content');
      return null;
    }
  }
}


const styles = StyleSheet.create({
  container:{
  }
});


function mapStateToProps(state, ownProps) {
	return {
    userRole: state.rootReducer.user.role,
    orderData: state.rootReducer.order.active.data,
    orderActive: state.rootReducer.order.active,
    shipmentActiveDelivery: state.rootReducer.order.shipmentActiveDelivery,
	};
}

function mapDispatchToProps(dispatch) {
	return {
      actionMasterData: bindActionCreators(actions.masterData, dispatch),
      actionNavigator: bindActionCreators(actions.navigator, dispatch),
      actionOrder: bindActionCreators(actions.order, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ComplaintContent);

