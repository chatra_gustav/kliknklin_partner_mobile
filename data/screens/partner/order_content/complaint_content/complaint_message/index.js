import { GiftedChat, Send, Bubble, Time, Message } from 'react-native-gifted-chat';

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
} from 'react-native';

import ResponsiveImage from 'react-native-responsive-image';
import Collapsible from 'react-native-collapsible';
import ResolveAssetSource from 'resolveAssetSource';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

import {LayoutPrimary} from './../../../../../layout';
import {TextWrapper, Container, CustomView, TabBar} from './../../../../../components';
import * as STRING from './../../../../../string';
import * as CONSTANT from './../../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../../actions";
class ComplaintMessage extends Component<{}> {
	constructor(props) {
	    super(props);
	  
	    this.state = {
	    	image:{
	    		send_icon:{
	    			width:0,
					height:0,
					multiplierWidth:0.125,
					multiplierHeight:0.125,
					url:require('./../../../../../image/icon/send_icon.png'),
	    		},
	    	},
		    currentTabKey:CONSTANT.COMPLAINT_TAB_TYPE.FIRST,
		    message:[],
	    }
	}

	componentWillMount() {
	    this.setupMessage();
		this.getComplaintMessageList();

	    this.intervalComplaintMessageList = setInterval(
				() => {
					this.getComplaintMessageList();
				}
	    	,15000);
	}

	componentWillUnmount(){
		clearInterval(this.intervalComplaintMessageList);
	}

  	componentDidMount(){
		this._getImageSize();
	}

	_getImageSize(){
		var image = this.state.image;

		var image_send_icon = ResolveAssetSource(this.state.image.send_icon.url);

		image.send_icon.width = moderateScale(image_send_icon.width);
		image.send_icon.height = moderateScale(image_send_icon.height);

      	this.setState({image});
	}

	getComplaintMessageList(){
  		let {actionComplaint, orderActive} = this.props;

		actionComplaint.getComplaintMessageList(orderActive.data.complaint.id)
	    .then((response) => {
			if (response) {
				this.setupMessage();
			}
	    });
	}


  setupMessage(){
  	let {complaintMessageList} = this.props;
	let messages = [];

    for(let index in complaintMessageList){
		let result = {};
		result._id = complaintMessageList[index].id;
		result.text = complaintMessageList[index].message;
		result.createdAt = complaintMessageList[index].created_at,
		result.user = {
			_id:complaintMessageList[index].user.id,
			name:complaintMessageList[index].user.name,
			avatar:complaintMessageList[index].user.user_image_url,
			role:complaintMessageList[index].user.role.id,
		};

		messages.push(result);
	}

	messages.reverse();

	this.setState({messages});
  }

  onSend(messages = []) {
  	if (messages.length > 0) {
  		if (messages[0].text.trim() != '') {

			let {actionComplaint} = this.props;

			actionComplaint.sendComplaintMessage(messages[0]);
		      	
  			this.setState(previousState => ({
		      	messages: GiftedChat.append(previousState.messages, messages),
		    }))
  		}
  	}
    
  }

  renderCustomView(userRole, name, props) {

  	userName='';
  	userRole='';

	if(props.user._id == props.currentMessage.user._id){
		userName = name.split(' ')[0];

		if (userRole == CONSTANT.USER_ROLE.ADMIN_OUTLET) {
			userRole = STRING.USER_ROLE.ADMIN_OUTLET;
		}
		else if(userRole == CONSTANT.USER_ROLE.CUSTOMER){
			userRole == STRING.USER_ROLE.CUSTOMER;
		}
		else if (userRole == CONSTANT.USER_ROLE.OWNER) {
			userRole == STRING.USER_ROLE.OWNER;
		}
	}
	else{
		userName = props.currentMessage.user.name.split(' ')[0];

		if (props.currentMessage.user.role == CONSTANT.USER_ROLE.ADMIN_OUTLET) {
			userRole = STRING.USER_ROLE.ADMIN_OUTLET;
		}
		else if(props.currentMessage.user.role == CONSTANT.USER_ROLE.CUSTOMER){
			userRole == STRING.USER_ROLE.CUSTOMER;
		}
		else if (props.currentMessage.user.role == CONSTANT.USER_ROLE.OWNER) {
			userRole == STRING.USER_ROLE.OWNER;
		}
	}

    return (
    	<View
			style={{
				  marginTop: 5,
				  //marginBottom: 5,
				  marginLeft: 10,
				  marginRight: 10,
			}}
    	>
    		<TextWrapper type={'smallercontent'}>{userName}</TextWrapper>
    	</View>
    );
  }

  renderMessageText(props){
  	let {currentMessage} = props;
  	return(
  		<View
			style={{
				  marginTop: 5,
				  marginBottom: 5,
				  marginLeft: 10,
				  marginRight: 10,
			}}
    	>
    		<TextWrapper type={'smallercontent'} fontWeight={'Medium'}>{currentMessage.text}</TextWrapper>
    	</View>
  	);
  }

  renderSend(props){
	return (
        <Send
            {...props}
        >
            <View style={{marginRight: 10, marginBottom: moderateScale(7.5)}}>
                <ResponsiveImage
					source={this.state.image.send_icon.url}
					initWidth={this.state.image.send_icon.width * this.state.image.send_icon.multiplierWidth}
					initHeight={this.state.image.send_icon.height * this.state.image.send_icon.multiplierHeight}
				/>
            </View>
        </Send>
    );
  }

  renderBubble(props){
  	if(!props.currentMessage.sent){
        return(
			<Bubble
				wrapperStyle={{
					right:{
						backgroundColor:'#b9e9ff',
					}
				}}
				{...props}
			/>
	  	);
    }
    return (
      <Bubble {...props}  />
    );
  }

  render() {
    try{
      return (
      		<Container.TypeA
	          containerStyle={{
	          	flex: 1,
	          	borderRadius:0,
	          	backgroundColor:'red',
	          	minHeight:Dimensions.get('window').height * 0.51,
	            marginVertical:moderateScale(5),
	            backgroundColor: 'transparent',
	            elevation: 0
	          }}
	          contentStyle={{
	          	flex:1,
	          	padding: 0
	          }}
	        >
					<GiftedChat
				        alwaysShowSend={true}
				        placeholder={'Tulis pesan anda...'}
		        		renderCustomView={this.renderCustomView.bind(this, this.props.userRole, this.props.userData.name)}
				        renderMessageText={this.renderMessageText}
				        renderSend={this.renderSend.bind(this)}
				        renderBubble={this.renderBubble}
				        messages={this.state.messages}
				        textInputStyle={{
				        	color:'black',
				        	fontFamily:'Montserrat',
				        	fontSize:moderateScale(14)
				        }}
				        placeholderTextColor={'black'}
				        onSend={messages => this.onSend(messages)}
				        user={{
				          _id: this.props.userData.id,
				        }}
				        isLoadingEarlier={true}
				        //loadEarlier={true}
				    />
	        </Container.TypeA>
      )
    }
    catch(error){
      console.log(error, 'complaint_content');
      return null;
    }
  }
}

styles = StyleSheet.create({
  container:{
    flex:1,
    alignItems:'center',
  }
});


function mapStateToProps(state, ownProps) {
	return {
	    userRole: state.rootReducer.user.role,
	    userData: state.rootReducer.user.data,
	    complaintMessageList: state.rootReducer.order.complaintMessageList,
	    orderActive: state.rootReducer.order.active,
	};
}

function mapDispatchToProps(dispatch) {
	return {
    	actionComplaint: bindActionCreators(actions.complaint, dispatch),
    	actionOrder: bindActionCreators(actions.order, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ComplaintMessage);

