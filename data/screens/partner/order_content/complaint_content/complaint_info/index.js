import { GiftedChat, Send, Bubble, Time } from 'react-native-gifted-chat';

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';

import ResponsiveImage from 'react-native-responsive-image';
import Collapsible from 'react-native-collapsible';
import ResolveAssetSource from 'resolveAssetSource';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

import {LayoutPrimary} from './../../../../../layout';
import {TextWrapper, Container, CustomView, TabBar} from './../../../../../components';
import * as STRING from './../../../../../string';
import * as CONSTANT from './../../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../../actions";
class ComplaintInfo extends Component<{}> {
	constructor(props) {
	    super(props);
	  
	    this.state = {
	    	image:{
	    		send_icon:{
	    			width:0,
					height:0,
					multiplierWidth:0.125,
					multiplierHeight:0.125,
					url:require('./../../../../../image/icon/send_icon.png'),
	    		},
	    	},
	    }
	}

	componentWillMount() {
	}

  	componentDidMount(){

	}

	isSolutionAccepted(){
		try{
			let {orderActive} = this.props;
			let {data} = orderActive;
			let {complaint} = data;
			let {items} = complaint;

			let isSolutionAccepted = true;

			for (let index in items){
				if (items[index].complaint_item_status_id == CONSTANT.COMPLAINT_ITEM_STATUS.WAITING_SOLUTION ||
					items[index].complaint_item_status_id == CONSTANT.COMPLAINT_ITEM_STATUS.REJECTED ||
					items[index].complaint_item_status_id == CONSTANT.COMPLAINT_ITEM_STATUS.SOLUTION_APPLIED
					) {
					isSolutionAccepted = false;
				}
			}

			return isSolutionAccepted;
		}
		catch(error){

		}
	}

	isSolutionRejected(){
		try{
			let {orderActive} = this.props;
			let {data} = orderActive;
			let {complaint} = data;
			let {items} = complaint;

			let isSolutionRejected = false;

			for (let index in items){

				if (items[index].complaint_item_status_id == CONSTANT.COMPLAINT_ITEM_STATUS.REJECTED 
					) {
					isSolutionRejected = true;
				}
			}

			return isSolutionRejected;
		}
		catch(error){
			console.log(error);
		}
	}

	isWaitCustomerResponse(){
		try{
			let {orderActive} = this.props;
			let {data} = orderActive;
			let {complaint} = data;
			let {items} = complaint;

			let isWaitCustomerResponse = false;

			for (let index in items){
				
				if (items[index].complaint_item_status_id == CONSTANT.COMPLAINT_ITEM_STATUS.SOLUTION_APPLIED
					) {
					isWaitCustomerResponse = true;
				}
			}

			return isWaitCustomerResponse;
		}
		catch(error){
			console.log(error);
		}
	}

	getInfo(){
		try{
			let {orderActive} = this.props;
			let {data} = orderActive
			
			if (data.complaint.complaint_status_id == CONSTANT.COMPLAINT_STATUS.DONE) {
				return 'Komplain telah selesai.'
			}
			else if (data.complaint.complaint_status_id == CONSTANT.COMPLAINT_STATUS.SOLVED) {
				return 'Menunggu konfirmasi pelanggan.'
			}
			else if (data.complaint.complaint_status_id == CONSTANT.COMPLAINT_STATUS.IN_PROGRESS) {
				if(data.important){
					return 'Anda harus memberikan solusi barang.'
				}
				else if (this.isSolutionAccepted()) {
					return 'Solusi diterima, selesaikan komplain anda.'
				}
				else if (this.isSolutionRejected()) {
					return 'Solusi ada yang ditolak, periksa kembali.'
				}
				else if (this.isWaitCustomerResponse()) {
					return 'Mohon tunggu, pelanggan menilai solusi'
				}
			}
		}
		catch(error){

		}
	}

	render() {
	    try{
	    	//let {orderData} = this.props;
	    	//let {complaint} = orderData;
	    	//let {items} = complaint;

		    return (
		      	<Container.TypeA
		          containerStyle={{
		            marginVertical:moderateScale(5),
		            minHeight:Dimensions.get('window').height * 0.07,
		            height:Dimensions.get('window').height * 0.07,
		            width:Dimensions.get('window').width * 0.9,
		          }}
		          contentStyle={{
		          	justifyContent:'center',
		          	alignItems:'center',
		          }}
		        >
		        	<TextWrapper type={'smallercontent'} fontWeight={'SemiBold'} style={{
		        		color:CONSTANT.COLOR.RED_A
		        	}}>{this.getInfo()}</TextWrapper>
		        </Container.TypeA>
		    )
	    }
	    catch(error){
	      console.log(error, 'complaint_content');
	      return null;
	    }
	  }
	}

styles = StyleSheet.create({
  container:{
    flex:1,
    alignItems:'center',
  }
});


function mapStateToProps(state, ownProps) {
	return {
    userRole: state.rootReducer.user.role,
    userData: state.rootReducer.user.data,
    orderActive: state.rootReducer.order.active,
	};
}

function mapDispatchToProps(dispatch) {
	return {
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
    	actionComplaint: bindActionCreators(actions.complaint, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ComplaintInfo);

