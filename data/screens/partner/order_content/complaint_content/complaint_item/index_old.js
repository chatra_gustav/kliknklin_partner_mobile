import { GiftedChat, Send, Bubble, Time } from 'react-native-gifted-chat';

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';

import ResponsiveImage from 'react-native-responsive-image';
import Collapsible from 'react-native-collapsible';
import ResolveAssetSource from 'resolveAssetSource';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

import {LayoutPrimary} from './../../../../../layout';
import {TextWrapper, Container, CustomView, TabBar} from './../../../../../components';
import * as STRING from './../../../../../string';
import * as CONSTANT from './../../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../../actions";
class ComplaintItem extends Component<{}> {
	constructor(props) {
	    super(props);
	  
	    this.state = {
	    	image:{
	    		send_icon:{
	    			width:0,
					height:0,
					multiplierWidth:0.125,
					multiplierHeight:0.125,
					url:require('./../../../../../image/icon/send_icon.png'),
	    		},
	    	},
		    currentTabKey:CONSTANT.COMPLAINT_TAB_TYPE.FIRST,
		    message:[],
	    }
	}

	componentWillMount() {
	}

  	componentDidMount(){
	}


  render() {
    try{
    	let {orderData} = this.props;
    	let {complaint} = orderData;
    	let {items} = complaint;

	    return (
	      	<Container.TypeA
	          containerStyle={{
	          	flex:1,
	            marginVertical:moderateScale(5),
	            marginBottom:moderateScale(10),
	            minHeight:Dimensions.get('window').height * 0.575,
	            height:Dimensions.get('window').height * 0.575,
	            width:Dimensions.get('window').width * 0.9,
	          }}
	          contentStyle={{
	          	//flex:1,
	          }}
	        >
	        	<FlatList
				  data={items}
				  renderItem={({item}) => <ItemBox item={item} {...this.props} />}
				  keyExtractor={(item, index) => index+''}
				  contentContainerStyle={{
				  	//minHeight:Dimensions.get('window').height * 0.7,
				  }}
				/>
	        </Container.TypeA>
	    )
    }
    catch(error){
      console.log(error, 'complaint_content');
      return null;
    }
  }
}

class ItemBox extends Component<{}> {
	_getItemImage(){
        let {item} = this.props;
        let {evidence_image_url} = item;

        if (evidence_image_url && evidence_image_url != '') {
            return {
                uri:evidence_image_url,
            }
        }
        return null;
    }

    _getItemName(){
		let {item} = this.props;
		let {orderitem} = item;
		let {kliknklin, outlet} = orderitem; 

		if (kliknklin) {
        	return kliknklin.itempricearea.itemwashingtime.item.name;
		}
		else{
			return outlet.outletitemwashingtime.itemwashingtime.item.name;
		}
    }

    renderSolutionButton(){
    	let {item, actionNavigator, actionComplaint} = this.props;
		let {orderitem, complaintitemsolution} = item;

		if (CONSTANT.COMPLAINT_ITEM_STATUS.WAIT_SOLUTION == item.complaint_item_status_id ||
			CONSTANT.COMPLAINT_ITEM_STATUS.REJECTED == item.complaint_item_status_id
			) {
			return(
				<TouchableOpacity
						style={{
							backgroundColor:CONSTANT.COLOR.BLUE_A,
							//margin:moderateScale(1.5),
							padding:moderateScale(6),
							borderRadius:moderateScale(10),
						}}
						onPress={()=>{
							actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeC', {
		                    containerStyle:{
		                        height:Dimensions.get('window').height * 0.6,
		                      },
		                      data:{
		                        titleText:'Pilih Solusi',
		                        type:'custom',
		                        itemList:this.props.complaintItemSolutionList,
		                        buttonPrimaryText:'Pilih',
		                        onPressButtonPrimary:(complaintItemSolutionID, complaintItemSolutionInfo) =>{
									let complaintItemID = item.id;
									actionNavigator.dismissLightBox();
									setTimeout(() => {
										actionComplaint.setItemSolution(complaintItemSolutionID, complaintItemSolutionInfo, complaintItemID);
									},500);
		                        }
		                      },
		                    });
						}}
					>
							<TextWrapper 
								fontWeight={'SemiBold'} 
								type={'base'}
								style={{
									color:'white',
								}}
							>Beri Solusi</TextWrapper>
				</TouchableOpacity>
			);
		}
		else{
			return(
				<TouchableOpacity
					disabled
					style={{
						backgroundColor:CONSTANT.COLOR.GREEN_A,
						//margin:moderateScale(1.5),
						padding:moderateScale(6),
						borderRadius:moderateScale(10),
					}}
				>
						<TextWrapper 
							fontWeight={'SemiBold'} 
							type={'base'}
							style={{
								color:'white',
							}}
						>{complaintitemsolution.info}</TextWrapper>
				</TouchableOpacity>
			);
		}
		
    }

	render(){
		try{
			let {item, actionNavigator} = this.props;
			let itemImage = this._getItemImage();

			let zoomImageDisabled = false;

			if (itemImage == null) {
				zoomImageDisabled = true;
			}
			return(
				<View
					style={{
						width:'100%',
						height:moderateScale(120),
						marginBottom:moderateScale(15),
						backgroundColor:'rgba(255,255,255,1)',
						flexDirection:'row',
						borderWidth:0,
						borderBottomWidth:moderateScale(1.5),
						borderColor:'lightgray',
					}}
				>
					<View
						style={{
							flex:0.25,
							margin:5,
						}}
					>
						<View
							style={{
								flex:0.6,
								alignItems:'center',
								justifyContent:'center'
							}}
						>
							<TouchableOpacity
		                        style={{
		                            width:moderateScale(70),
		                            height:moderateScale(70),
		                            borderRadius: moderateScale(35),
		                            borderWidth:1,
		                            borderColor:CONSTANT.COLOR.GRAY_B,
		                            backgroundColor:'white',
		                            alignItems:'center',
		                            justifyContent:'center',
		                        }}
		                        onPress={() => {
		                        	this.props.actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeImage', {
										sourceImage:this._getItemImage(),
		                        	})
		                        }}

		                        disabled={zoomImageDisabled}
		                    >
		                        <Image
		                            style={{
		                                width:moderateScale(65),
		                                height:moderateScale(65),
		                                borderRadius: moderateScale(32.5),
		                                borderWidth:1,
		                            }}
		                            source={this._getItemImage()}
		                            onError={() =>{
		                                this.setState({imageFailLoad:true});
		                            }}
		                        />
	                    	</TouchableOpacity>
						</View>

						<View
							style={{
								flex:0.4,
								alignItems:'center',
								justifyContent:'center'
							}}
						>
							{this.renderSolutionButton()}
						</View>
					</View>
					<View
						style={{
							flex:0.75,
							margin:moderateScale(5),
						}}
					>
						<View
							style={{flex:0.275,}}
						>
							<TextWrapper
								ellipsizeMode={'tail'}
								numberOfLines={2}
								type={'smallercontent'}
							>
							{this._getItemName()}
							</TextWrapper>
						</View>
						<View style={{flex:0.725}}
						>
							<TextWrapper
								ellipsizeMode={'tail'}
								numberOfLines={6}
								type={'base'}
								onPress={() => {
									this.props.actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeText', {
										contentText:item.problem_details,
		                        	})
								}}
							>
								{item.problem_details}
							</TextWrapper>
						</View>
					</View>
				</View>
			);
		}
		catch(error){
			console.log(error);
			return null;
		}
	}
}

styles = StyleSheet.create({
  container:{
    flex:1,
    alignItems:'center',
  }
});


function mapStateToProps(state, ownProps) {
	return {
    userRole: state.rootReducer.user.role,
    userData: state.rootReducer.user.data,
    complaintMessageList: state.rootReducer.order.complaintMessageList,
    complaintItemSolutionList: state.rootReducer.masterData.complaintItemSolutionList,
	};
}

function mapDispatchToProps(dispatch) {
	return {
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
    	actionComplaint: bindActionCreators(actions.complaint, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ComplaintItem);

