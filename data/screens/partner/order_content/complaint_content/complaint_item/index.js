import { GiftedChat, Send, Bubble, Time } from 'react-native-gifted-chat';

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';

import ResponsiveImage from 'react-native-responsive-image';
import Collapsible from 'react-native-collapsible';
import ResolveAssetSource from 'resolveAssetSource';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import ViewMoreText from 'react-native-view-more-text';

import {LayoutPrimary} from './../../../../../layout';
import {TextWrapper, Container, CustomView, TabBar, Scaling, Text} from './../../../../../components';
import * as STRING from './../../../../../string';
import * as CONSTANT from './../../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../../actions";

//local component
import * as style from './../../style';
import SubmitButton from './../../submit_button';

class ComplaintItem extends Component<{}> {
	constructor(props) {
	    super(props);
	  
	    this.state = {
	    	image:{
	    		send_icon:{
	    			width:0,
					height:0,
					multiplierWidth:0.125,
					multiplierHeight:0.125,
					url:require('./../../../../../image/icon/send_icon.png'),
	    		},
	    	},
		    currentTabKey:CONSTANT.COMPLAINT_TAB_TYPE.FIRST,
		    message:[],
	    }
	}

	componentWillMount() {
	}

  	componentDidMount(){
	}

	renderViewMore(onPress){
      return(
        <Text onPress={onPress} fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>Read more</Text>
      )
    }
    
    renderViewLess(onPress){
      return(
        <Text onPress={onPress} fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>Read less</Text>
      )
    }

  	render() {
	    try{
	    	let {orderData} = this.props;
	    	let {complaint} = orderData;
	    	let {items} = complaint;
	    	
	    	return(
	    		<FlatList 
		          	showsVerticalScrollIndicator={false}
		          	data={items}
		          	keyExtractor={(item, index) => index+''}
		          	renderItem={(item, index) => { 
		          		
		          		return (
		          		<View style={[
		          			style.content.itemDetail, 
		          			styles.complaintSection, 
		          			{marginBottom: (item.index == (items.length) - 1 ? Scaling.verticalScale(5) : Scaling.verticalScale(20))}
		          			]}>
		          				{this.renderContent(item)}
		          		</View>
		          	);}}
		        />
	    	)
	    }
	    catch(error){
	      console.log(error, 'complaint_content');
	      return null;
	    }
  	}

  	getComplaintItemStatus(complaintItemStatusID){
  		let message = '';
  		let isImportant = false;
  		if (complaintItemStatusID == CONSTANT.COMPLAINT_ITEM_STATUS.WAIT_SOLUTION) {
  			message = 'BUTUH SOLUSI';
			isImportant = true;
  		}
  		else if (complaintItemStatusID == CONSTANT.COMPLAINT_ITEM_STATUS.REJECTED) {
  			message = 'SOLUSI DITOLAK';
			isImportant = true;
		}
		else if (complaintItemStatusID == CONSTANT.COMPLAINT_ITEM_STATUS.IN_PROGRESS) {
  			message = 'SOLUSI DITERIMA';
			isImportant = false;
		}
		else if (complaintItemStatusID == CONSTANT.COMPLAINT_ITEM_STATUS.SOLUTION_APPLIED) {
  			message = 'TUNGGU KONFIRMASI';
			isImportant = false;
		}
		else if (complaintItemStatusID == CONSTANT.COMPLAINT_ITEM_STATUS.SOLVED) {
			message = 'PRODUK TERATASI';
			isImportant = false;
		}

  		return {
  			message,
  			isImportant,
  		}
  	}

  	renderContent(data){
		let {item} = data;
		let {orderitem, evidence_image_url, problem_details, complaintitemreason, complaint_item_status_id} = item;
		let {outlet} = orderitem;

		let complaintItemStatus = this.getComplaintItemStatus(complaint_item_status_id);
		console.log(complaintItemStatus, complaint_item_status_id);
  		return (
			<View>
				<View style={{flex:1, flexDirection: 'row'}}>
					<View style={{flex: 0.7}}>
						<View style={styles.rowTitle}>
		            		<Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_FONT_GRAY}}>Keluhan</Text>
		            	</View>

		            	<View style={styles.rowContent}>
	                		<View style={styles.contentLeft}>
	                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Item</Text>
	                		</View>

	                		<View style={styles.contentCenter}>
	                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>:</Text>
	                		</View>

	                		<View style={styles.contentRight}>
	                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>
	                				{outlet.outletitemwashingtime.itemwashingtime.item.name}
	                			</Text>
	                		</View>
	                	</View>

	                	<View style={styles.rowContent}>
	                		<View style={styles.contentLeft}>
	                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Keluhan</Text>
	                		</View>

	                		<View style={styles.contentCenter}>
	                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>:</Text>
	                		</View>

	                		<View style={styles.contentRight}>
	                			<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{complaintitemreason.info}</Text>
	                		</View>
	                	</View>
	                </View>

	                <View style={{flex: 0.35, alignItems: 'center', marginVertical:Scaling.verticalScale(15)}}>
	                	<Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_CONTENT}
							textStyle={{
								color:complaintItemStatus.isImportant ? CONSTANT.COLOR.RED_STATUS_TEXT : CONSTANT.COLOR.GREEN_STATUS_TEXT,
								marginBottom:Scaling.verticalScale(3),
							}}
	                	> 
							{complaintItemStatus.message}
	                	</Text>
	                	<Image
                            style={{
                                width:moderateScale(65),
                                height:moderateScale(65),
                                borderRadius: moderateScale(10)
                            }}
                            source={this._getItemImage(evidence_image_url)}
                            onError={() =>{
                                this.setState({imageFailLoad:true});
                            }}
                        />
	                </View>
				</View>

            	<View style={styles.rowTitle}>
            		<Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_FONT_GRAY}}>Keterangan</Text>
            	</View>

            	<View style={styles.rowContent}>

            		<ViewMoreText
			          	numberOfLines={3}
			          	renderViewMore={this.renderViewMore}
			          	renderViewLess={this.renderViewLess}
			        >
        				<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>
        					{problem_details}
        				</Text>
        			</ViewMoreText>
            	</View>

            	<View style={styles.rowTitle}>
            		<Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_FONT_GRAY}}>Solusi</Text>
            	</View>

            	{this.renderSolutionButton(item)}

			</View>
    	)  			
  	}

  	renderSolutionButton(item){

    	let {actionNavigator, actionComplaint, complaintItemSolutionList} = this.props;
		let {orderitem, complaintitemsolution} = item;

		if (CONSTANT.COMPLAINT_ITEM_STATUS.WAIT_SOLUTION == item.complaint_item_status_id ||
			CONSTANT.COMPLAINT_ITEM_STATUS.REJECTED == item.complaint_item_status_id
			) {
			return(
				<View style={styles.rowButtonContent}>
            		<TouchableOpacity
						style={{
							backgroundColor:CONSTANT.COLOR.LIGHT_BLUE,
							//margin:moderateScale(1.5),
							paddingVertical:moderateScale(6),
							paddingHorizontal:moderateScale(10),
							borderRadius:moderateScale(5),
						}}
						onPress={()=>{
							this.props.actionNavigator.showLightBox('PartnerKliknKlin.ItemSelection', {
	                          data:complaintItemSolutionList,
	                          categoryToFind:'info',
	                          titleBar:true,
	                          title:'Pilih Solusi',
	                          onPressButtonPrimary:(selectedItemIndex) => {
	                            this.props.actionNavigator.dismissLightBox();
	                              
	                            setTimeout(() => {
	                              this.props.actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeA',{
	                                  title:'konfirmasi', 
	                                  message: 'Solusi yang dipilih : \n' + complaintItemSolutionList[selectedItemIndex].info, 
	                                  buttonPrimaryText: 'ya', 
	                                  buttonSecondaryText: 'batal', 
	                                  onPressButtonPrimary: () => {
	                                    this.props.actionNavigator.dismissLightBox();
	                                    
	                                    let complaintItemSolutionID = complaintItemSolutionList[selectedItemIndex].id;
	                                    let complaintItemSolutionInfo = complaintItemSolutionList[selectedItemIndex].info;
										let complaintItemID = item.id 

	                                    setTimeout(() => {
	                                    	actionComplaint.setItemSolution(complaintItemSolutionID, complaintItemSolutionInfo, complaintItemID);
	                                    }, 350)
	                                  }, 
	                                  onPressButtonSecondary: () =>{
	                                    this.props.actionNavigator.dismissLightBox();
	                                  },
	                              });
	                            },350);
	                          }
	                        });
						}}
					>
						<TextWrapper 
							fontWeight={'SemiBold'} 
							type={'base'}
							style={{
								color:'white',
							}}
						>Buat Solusi</TextWrapper>
					</TouchableOpacity>
            	</View>
			);
		}
		else{
			return(
				<View style={styles.rowContent}>
					<Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>
						{complaintitemsolution.info}
					</Text>
				</View>
			);
		}
		
    }

  	_getItemImage(evidence_image_url){

        if (evidence_image_url && evidence_image_url != '') {
            return {
                uri:evidence_image_url,
            }
        }
        return null;
  	}
}

const styles = StyleSheet.create({
	complaintSection:{
		flex: 1,
		backgroundColor: CONSTANT.COLOR.WHITE,
		elevation:2
	},
	row:{
		marginVertical: Scaling.verticalScale(5)
	},
	column:{
		marginHorizontal: Scaling.verticalScale(5)	
	},
	rowTitle:{
		marginTop: Scaling.verticalScale(15),
		marginBottom: Scaling.verticalScale(7),
	},
	rowContent:{
		flex: 1,
		flexDirection: 'row',
		marginVertical: Scaling.verticalScale(5)
	},
	rowButtonContent:{
		flex: 1,
		marginVertical: Scaling.verticalScale(8),
		alignItems: 'center'

	},
	contentLeft: {
		flex: 0.25
	},
	contentCenter:{
		flex: 0.1,
		alignItems: 'center'
	},
	contentRight:{
		flex: 0.65
	}
})


function mapStateToProps(state, ownProps) {
	return {
	    userRole: state.rootReducer.user.role,
	    userData: state.rootReducer.user.data,
	    complaintMessageList: state.rootReducer.order.complaintMessageList,
	    complaintItemSolutionList: state.rootReducer.masterData.complaintItemSolutionList,
	};
}

function mapDispatchToProps(dispatch) {
	return {
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
    	actionComplaint: bindActionCreators(actions.complaint, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ComplaintItem);

