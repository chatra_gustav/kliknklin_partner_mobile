import React, { Component } from 'react';
import {AppState} from 'react-native';

import {
  StyleSheet,
  View,
  Dimensions,
  Linking,
  TouchableOpacity,
  Image,
} from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import MapView from 'react-native-maps';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';

import {TextWrapper} from "./../../../../components";
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

class Map extends Component {
	state = {
	    appState: AppState.currentState
	}

	componentDidMount(){
		this.configLocation();
		//AppState.addEventListener('change', this._handleAppStateChange);
	}

	async configLocation(){
		console.log('nyihaha');
		setTimeout(async () => {
			console.log('hihuha');
			let locationPermission = await this.props.actionLocation.checkLocationServicePermission(this.props.navigator, this.configLocation.bind(this));

			console.log(locationPermission);
			if (locationPermission) {
				let userLocation = await this.props.actionUser.getUserLocation(this.props.navigator);
			}
		},1000)
	}

  	componentWillUnmount() {
  		this.props.actionMap.resetCoords();
  		
    	AppState.removeEventListener('change', this._handleAppStateChange);
  	}

	_handleAppStateChange = (nextAppState) => {
	    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
	      //this.props.actionUser.getUserLocation(this.props.navigator);
	    }
	    this.setState({appState: nextAppState});

	}
	
	render(){
		let {directionCoords, centerCoords} = this.props;

		return(
			<MapComponent 
				{...this.props}
				directionCoords={this.props.directionCoords}
				centerCoords={this.props.centerCoords}
			/>
		);
		
		// if (this.props.directionCoords != null && this.props.centerCoords!= null && this.props.isMapAllowedToLoad) {
		// 	return(
		// 		<MapComponent 
		// 			directionCoords={this.props.directionCoords}
		// 			centerCoords={this.props.centerCoords}
		// 		/>
		// 	);
		// }
		// else{
		// 	return(
		// 		<LoadingMapComponent />
		// 	);
		// }
	}
}

class MapComponent extends Component{
	
	renderChild(){
		if (this.props.directionCoords.length != 0) {
			return(
				<View>
					<DestinationMarker 
						{...this.props}
					/>

					<DirectionMarker 
						{...this.props}
					/>

					<SelfMarker 
						{...this.props}
					/>
				</View>
			);
		}
	}
	render(){
		return(
			<View style={styles.container}>
				<View style={styles.containerMap}>
					<DetailMap 
						centerCoords={this.props.centerCoords}
					>
						
						{this.renderChild()}
						
					</DetailMap>

					<MapTool
						{...this.props}
					/>
				</View>
			</View>
		);
	}
}

class LoadingMapComponent extends Component{
	render(){
		return(
			<View style={styles.container}>
				<View style={styles.containerMap}>
					<TextWrapper type={'base'} style={{color:'white'}}>
						Memuat peta...
					</TextWrapper>
				</View>
			</View>
		);
	}
}

class DetailMap extends Component{

	render(){
		return(
			<MapView
				initialRegion={{
			      latitude: -7.7956,
			      longitude: 110.3695,
			      latitudeDelta: 999999,
			      longitudeDelta: 999999,
			    }}
				region={this.props.centerCoords}
				style={styles.map}
				onPress={()=>{}}
			>
				{this.props.children}
			</MapView>
		);
	}
}

class DestinationMarker extends Component{
	render(){
		return(
			<MapView.Marker
				coordinate={this.props.directionCoords[this.props.directionCoords.length - 1]}
				pinColor="orange"
				title={'tujuan anda'}
			/>
		);
	}
}

class DirectionMarker extends Component{
	render(){
		return(
			<MapView.Polyline
				coordinates={this.props.directionCoords}
				strokeWidth={6}
				strokeColor="#23b1e9"
			/>
		);
	}
}

class SelfMarker extends Component{
	render(){
		return(
			<MapView.Marker
				coordinate={this.props.directionCoords[0]}
				pinColor={'red'}
				title={'lokasi anda'}
			/>
		);
	}
}

class MapTool extends Component{
	render(){
		return(
			<View
				style={{
					width:'100%',
					alignItems:'center',
				}}
			>
				<DirectionDetail
					{...this.props}
				/>
				
				<OpenMapsButton 
						{...this.props}
				/>
			</View>
		);
	}
}

class DirectionDetail extends Component{

	render(){
		return(
			<View 
				//onPress={this._openMaps.bind(this)} 
				style={styles.containerDirectionDetail}
			>
				<View
					style={{flex:1}}
				>
					<View style={{
						flex:0.5,
						alignItems:'center',
					}}>
						<TextWrapper type={'smallercontent'} fontWeight={'SemiBold'}>Jarak</TextWrapper>
					</View>
					<View style={{
						flex:0.5,
						alignItems:'center',
					}}>
						<TextWrapper type={'smallercontent'} fontWeight={'SemiBold'}>{this.props.directionDistance}</TextWrapper>
					</View>
				</View>
			</View> 
		);
	}

}

class OpenMapsButton extends Component{
	constructor(props) {
        super(props);
        this.state = {
            image:{
                open_map_icon:{
                    width:0,
                    height:0,
                    multiplierWidth:0.5,
                    multiplierHeight:0.5,
                    url:require('./../../../../image/icon/open_map_icon.png'),
                },
            },
        };  
    }

    componentWillMount(){
        this.getImageSize();
    }

    getImageSize(){
        var image = this.state.image;

        var image_open_map_icon = ResolveAssetSource(this.state.image.open_map_icon.url);

        image.open_map_icon.width = image_open_map_icon.width;
        image.open_map_icon.height = image_open_map_icon.height;
        
        this.setState({image});
    }



	_openMaps(){
		try{
			let { orderData, orderActiveType, shipmentActivePickup, shipmentActiveDelivery } = this.props;
			let location = {};

			if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP
				) {
				
				let {shipmentschedule} = shipmentActivePickup;
				let {geolocation} = shipmentschedule;
				location['latitude'] = geolocation.coordinates[1];
				location['longitude'] = geolocation.coordinates[0];

			}
			else if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY
				) {
			
				let {shipmentschedule} = shipmentActiveDelivery;
				let {geolocation} = shipmentschedule;

				location['latitude'] = geolocation.coordinates[1];
				location['longitude'] = geolocation.coordinates[0];
			}
			
			Linking.canOpenURL('http://maps.google.com/maps?daddr='+location.latitude+','+location.longitude).then(supported => {
				if (supported) {
				    Linking.openURL('http://maps.google.com/maps?daddr='+location.latitude+','+location.longitude +'&avoid=tolls&avoid=highways');
				} else {
				    console.log('Don\'t know how to go');
				}
			}).catch(err => console.error('An error occurred', err));
		}
		catch(error){
			console.log(error);
			alert('gagal membuka maps');
		}
	}

	render(){

		return(
			<TouchableOpacity 
				onPress={this._openMaps.bind(this)} 
				style={styles.containerOpenMapsButton}
			>
				<Image
					style={{
						width:this.state.image.open_map_icon.width * this.state.image.open_map_icon.multiplierWidth,
						height:this.state.image.open_map_icon.height * this.state.image.open_map_icon.multiplierHeight,
					}}
					source={this.state.image.open_map_icon.url}
                />
			</TouchableOpacity> 
		);
	}
}

const styles = StyleSheet.create({
	container:{
		height:moderateScale(260),
		width:'100%',
		justifyContent: 'center',
		alignItems: 'center',
	},
	containerMap: {
		height:moderateScale(260),
		width:'100%',
		justifyContent: 'flex-end',
		alignItems: 'flex-end',
		borderRadius: 10,
		borderWidth:1,
		borderColor:'transparent',
		overflow: 'hidden',
		backgroundColor:'#69CAF0',
		elevation:3,
	},
	map: {
		...StyleSheet.absoluteFillObject,
	},
	containerDirectionDetail:{
		width:moderateScale(75),
		height:moderateScale(40),
		justifyContent:'flex-end',
		alignItems:'center',
		flexDirection:'row',
		margin:moderateScale(5),
		backgroundColor: 'rgba(256,256,256, 1)',
		borderRadius:moderateScale(5),
	},
	containerOpenMapsButton:{
		width:moderateScale(50),
		height:moderateScale(50),
		position:'absolute',
		right:0,
		margin:moderateScale(5),
	}
});

function mapStateToProps(state, ownProps) {
	return {
		userLocation:state.rootReducer.user.location,
		orderData:state.rootReducer.order.active.data,
		orderActiveType:state.rootReducer.order.active.orderActiveType,
		shipmentActivePickup:state.rootReducer.order.shipmentActivePickup,
		shipmentActiveDelivery:state.rootReducer.order.shipmentActiveDelivery,
	    directionCoords: state.rootReducer.map.directionCoords,
	    centerCoords: state.rootReducer.map.centerCoords,
	    directionDistance: state.rootReducer.map.directionDistance,
	    directionDuration: state.rootReducer.map.directionDuration,
	};
}

function mapDispatchToProps(dispatch) {
	return {
    	actionUser: bindActionCreators(actions.user, dispatch),
    	actionLocation: bindActionCreators(actions.location, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Map);

