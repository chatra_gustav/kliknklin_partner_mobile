import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';

import {TextWrapper, OrderDetail, OrderStatus, ShipmentStatus} from './../../../../components';
import * as CONSTANT from './../../../../constant';

export default class Header extends Component{
	constructor(props) {
		super(props);
	  
	    this.state = {
			buttonColor:this.props.buttonColor,
			buttonText:this.props.buttonText,
	    };
	}

	_renderStatus(){
		let {userRole, onlineFlag} = this.props;

		if (userRole == CONSTANT.USER_ROLE.COURIER) {
			return(
				<ShipmentStatus
					{...this.props}
				/>
			);
		}
		else if (userRole == CONSTANT.USER_ROLE.ADMIN_OUTLET || 
			userRole == CONSTANT.USER_ROLE.OWNER) {
			return(
				<OrderStatus
					{...this.props}
					onlineFlag={onlineFlag != undefined ? onlineFlag : true}
				/>
			)
		}
	}

	render(){
		try{
			return(
				<View
					style={[styles.headerContainer, this.props.headerContainer]}
	    		>
					<View style={styles.leftContentContainer}>
						{this._renderStatus()}
	    			</View>

					<View style={styles.rightContentContainer}>
					</View>
	    		</View>
			);
		}
		catch(error){
			console.log(error);
		}
	}
}

Header.propTypes = {
  //shipmentStatus: PropTypes.oneOf(['AKTIF', 'TUNDA', 'BATAL', 'TELAT', 'SELESAI']),
  orderID:PropTypes.string,
}

Header.defaultProps = {
  	orderID:'Order ID',
  	shipmentStatus:'Unknown',
}

const styles = StyleSheet.create({
  	headerContainer: {
		flexDirection:'row',
		justifyContent:'flex-start',
		alignItems:'center',
		paddingTop: Dimensions.get('window').width * 0.03,
		paddingBottom: Dimensions.get('window').width * 0.01,
		paddingHorizontal:Dimensions.get('window').width * 0.05,
		backgroundColor: CONSTANT.COLOR.WHITE
	},
	rightContentContainer:{
		flex:0.5,
		alignItems:'flex-end',
	},
	leftContentContainer:{
		flex:0.5,
	},
	orderIDText:{
		color:'white',
	},
});