import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  Alert,
  Linking,
  PermissionsAndroid,
  TouchableOpacity,
  ScrollView,
  AppState,
} from 'react-native';

import ResponsiveImage from 'react-native-responsive-image';
import Collapsible from 'react-native-collapsible';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

import {LayoutPrimary} from './../../../../layout';
import {Scaling} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

//local component
import OrderMap from './../map';
import * as Section from './../section';
import Header from './../header';
import SubmitButton from './../submit_button';
 
class InProgressContent extends Component<{}> {
  constructor(props) {
    super(props);
  
    this.state = {
    }
  }

  componentDidMount(){
  }

  render() {
    let {image, orderData, actionRoot, shipmentActiveDelivery, directionCoords, centerCoords,userRole} = this.props;
    
    let {orderlaundry, order_hash, customer, payment, pricedetail, orderitem, appliedpromo, id} = orderData;

    let {order_status_laundry_id} = orderlaundry;

    let {shipment_status_id, shipmentschedule, shipment_method_id, internalshipment} = shipmentActiveDelivery;

    let userCourier = null;
    let user_courier_notes = null;

    if (internalshipment) {
      let {courier, courier_notes} = internalshipment;
      userCourier = courier;
      user_courier_notes = courier_notes;
    }
    
    let userCustomer = customer.user;

    return (
      <View style={styles.container}>

        <Header 
          orderID={order_hash} 
          shipmentStatus={shipment_status_id}
          orderStatus={orderlaundry.order_status_laundry_id}
          userRole={userRole}
        />

        <ScrollView showsVerticalScrollIndicator={false}>

          <Section.Customer 
            image={image}
            user_image_url={userCustomer.user_image_url}
            customerName={userCustomer.name}
            customerPhoneNumber={userCustomer.mobile_phone}
            endContent
          />

          <Section.Cashier
            orderData={orderData}
            image={image} 
            orderID={order_hash} 
          />

          <Section.Schedule 
            title={'Jadwal Pengantaran'}
            image={image} 
            shipmentDate={shipmentschedule.shipment_date} 
            shipmentTime={shipmentschedule.shipment_time_info}
            shipmentMethodId={shipment_method_id}
          />

          <Section.Courier 
            title={'Kurir Pengantaran'}
            image={image}
            type={'on-progress'}
            courier={userCourier}
            courierType={shipment_method_id}
            courier_notes={user_courier_notes}
          />
      
        </ScrollView>

        <Section.Payment 
          {...this.props}
          image={image} 
          orderPrice={pricedetail.full_price}
          shipmentFare={pricedetail.shipment_fare}
          subtotalPrice={pricedetail.outlet_profit}
          paidStatus={payment.payment_status_id}
          paymentMethod={payment.paymentmethod.info}
          orderitem={orderitem}
          appliedpromo={appliedpromo}
        />

        <SubmitButton
          {...this.props}
          shipmentStatus={shipment_status_id}
          orderStatus={orderlaundry.order_status_laundry_id}
          navigator={this.props.navigator}
          shipmentMethodId={shipment_method_id}
          orderHash={order_hash}
          orderPrice={pricedetail.full_price}
          orderId={id}
          paidStatus={payment.payment_status_id}
        />
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container:{
    flex:1
  }
});


function mapStateToProps(state, ownProps) {
  return {
    userRole: state.rootReducer.user.role,
    orderData: state.rootReducer.order.active.data,
    orderActive: state.rootReducer.order.active,
    directionCoords: state.rootReducer.map.directionCoords,
    centerCoords: state.rootReducer.map.centerCoords,
    shipmentActivePickup: state.rootReducer.order.shipmentActivePickup,
    shipmentActiveDelivery: state.rootReducer.order.shipmentActiveDelivery,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionUser: bindActionCreators(actions.user, dispatch),
    actionMap: bindActionCreators(actions.map, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(InProgressContent);
