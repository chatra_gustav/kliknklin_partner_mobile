import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  Alert,
  Linking,
  PermissionsAndroid,
  TouchableOpacity,
  ScrollView,
  AppState,
  BackHandler,
} from 'react-native';
import ResolveAssetSource from 'resolveAssetSource';
import MapView from 'react-native-maps';
import Polyline from '@mapbox/polyline';
import FusedLocation from 'react-native-fused-location';
import ResponsiveImage from 'react-native-responsive-image';
import Communications from 'react-native-communications';
import Collapsible from 'react-native-collapsible';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Navigation} from 'react-native-navigation';

import {LayoutPrimary} from './../../../layout';
import {TextWrapper, OrderDetail, Container, CustomView} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

//local component
import PickupContent from './pickup_content';
import InProgressContent from './in_progress_content';
import DeliveryContent from './delivery_content';
import ComplaintContent from './complaint_content';
 
class OrderContent extends Component<{}> {
  constructor(props) {
    super(props);
  
    this.state = {
  		image: {
  			jadwal_icon : {
  				width: 0,
  				height: 0,
  				multiplierWidth:0.4,
  				multiplierHeight:0.4,
  				url:require('./../../../image/order_detail/jadwal_icon.png'),
  			},
  			tujuan_icon : {
  				width: 0,
  				height: 0,
  				multiplierWidth:0.08,
  				multiplierHeight:0.08,
  				url:require('./../../../image/order_detail/tujuan_icon.png'),
  			},
  			konsumen_icon : {
  				width: 0,
  				height: 0,
  				multiplierWidth:0.4,
  				multiplierHeight:0.4,
  				url:require('./../../../image/order_detail/konsumen_icon.png'),
  			},
  			invoice_icon : {
  				width: 0,
  				height: 0,
  				multiplierWidth:0.08,
  				multiplierHeight:0.08,
  				url:require('./../../../image/order_detail/invoice_icon.png'),
  			},
  			phone_icon : {
  				width: 0,
  				height: 0,
  				multiplierWidth:0.5,
  				multiplierHeight:0.5,
  				url:require('./../../../image/order_detail/phone_icon.png'),
  			},
  			sms_icon : {
  				width: 0,
  				height: 0,
  				multiplierWidth:0.5,
  				multiplierHeight:0.5,
  				url:require('./../../../image/order_detail/sms_icon.png'),
  			},
        wallet_icon : {
          width: 0,
          height: 0,
          multiplierWidth:0.08,
          multiplierHeight:0.08,
          url:require('./../../../image/order_detail/wallet_icon.png'),
        },
        courier_icon : {
          width: 0,
          height: 0,
          multiplierWidth:0.08,
          multiplierHeight:0.08,
          url:require('./../../../image/icon/courier_icon.png'),
        },
  		},
      isCollapsed:false,
      isMapAllowedToLoad:false,
      appState:AppState.currentState,
      data:this.props.orderData,
    };
  }

  componentWillMount() {
    this.getImageSize();
  }

  componentDidMount(){

      if (this.props.overrideBackPress) {
        let route = this.props.route ? this.props.route : CONSTANT.ROUTE_TYPE.ORDER_LIST;
        let prop = {
          type:this.props.routeType ? this.props.routeType : CONSTANT.ORDER_ACTIVE_TYPE.DEFAULT,
          tabBarIndex:1,
          overrideBackPress:true,
        };
        
        this.props.actionRoot.setOverrideBackPress(true, route, prop, 'reset');

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
          this.props.actionRoot.setOverrideBackPress(false, null, 'reset', {});
          this.props.actionNavigator.resetTo(this.props.navigator, route, prop);
          return true;
        });
      }
  }

  componentWillUnmount() {
    if (this.props.overrideBackPress) {
      this.backHandler.remove();
    }
  }

  _getUserLocation(){
    this.setState({
        isMapAllowedToLoad:true,
      }, () => {
        
      });
  }

  componentWillReceiveProps(nextProps){
    //this.checkUserLocationAvailiblity(nextProps);
  }

  checkUserLocationAvailiblity(nextProps){
    if (this.props.userLocation != nextProps.userLocation && !this.state.isLoadedMap) {
        const userLocation = nextProps.userLocation;
        const destinationLocation = this.props.orderData;

        this.getDirection(userLocation.latitude, userLocation.longitude, destinationLocation.latitude, destinationLocation.longitude);    
    }
  }

  getDirection(selfLatitude, selfLongitude, destinationLatitude, destinationLongitude){
      this.props.actionMap.fetchDirectionGoogleMaps(selfLatitude, selfLongitude, destinationLatitude, destinationLongitude);
  }

  getImageSize(){
    var image = this.state.image;

    var image_jadwal_icon = ResolveAssetSource(this.state.image.jadwal_icon.url);

    image.jadwal_icon.width = moderateScale(image_jadwal_icon.width);
    image.jadwal_icon.height = moderateScale(image_jadwal_icon.height);

    var image_konsumen_icon = ResolveAssetSource(this.state.image.konsumen_icon.url);
    image.konsumen_icon.width = moderateScale(image_konsumen_icon.width);
    image.konsumen_icon.height = moderateScale(image_konsumen_icon.height);

    var image_phone_icon = ResolveAssetSource(this.state.image.phone_icon.url);
    image.phone_icon.width = moderateScale(image_phone_icon.width);
    image.phone_icon.height = moderateScale(image_phone_icon.height);

    var image_sms_icon = ResolveAssetSource(this.state.image.sms_icon.url);
    image.sms_icon.width = moderateScale(image_sms_icon.width);
    image.sms_icon.height = moderateScale(image_sms_icon.height);

    var image_tujuan_icon = ResolveAssetSource(this.state.image.tujuan_icon.url);
    image.tujuan_icon.width = moderateScale(image_tujuan_icon.width);
    image.tujuan_icon.height = moderateScale(image_tujuan_icon.height);

    var image_invoice_icon = ResolveAssetSource(this.state.image.invoice_icon.url);
    image.invoice_icon.width = moderateScale(image_invoice_icon.width);
    image.invoice_icon.height = moderateScale(image_invoice_icon.height);

    var image_wallet_icon = ResolveAssetSource(this.state.image.wallet_icon.url);
    image.wallet_icon.width = moderateScale(image_wallet_icon.width);
    image.wallet_icon.height = moderateScale(image_wallet_icon.height);

    var image_courier_icon = ResolveAssetSource(this.state.image.courier_icon.url);
    image.courier_icon.width = moderateScale(image_courier_icon.width);
    image.courier_icon.height = moderateScale(image_courier_icon.height);

    this.setState({image});
  }

  onPressInvoice(){
    this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.Invoice');
  }

  getTitle(){
    try{
      let {orderData} = this.props;
      let {order_hash} = orderData;

      return order_hash;
    }
    catch(error){
      return null;
    }
  }

  renderContent(){
    try{
      let {orderData, orderActiveType} = this.props;

      if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP) {
        return(
          <PickupContent
            {...this.props}
            {...this.state}
          />
        )
      }
      else if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.IN_PROGRESS) {
        return (
          <InProgressContent
            {...this.props}
            {...this.state}
          />
        );
      }
      else if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY) {
        return (
          <DeliveryContent
              {...this.props}
              {...this.state}
          />
        );
      }
      else if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.COMPLAINT) {
        return (
          <ComplaintContent
              {...this.props}
              {...this.state}
          />
        );
      }

    }
    catch(error) {
      console.log(error, 'order_content');
      return null;
    }
  }

  _isShowOptionButton(){
    let {orderStatus} = this.props;

    let result = true;

    if (orderStatus == CONSTANT.ORDER_STATUS.DONE_PICKUP ||
          orderStatus == CONSTANT.ORDER_STATUS.IN_PROGRESS_LAUNDRY
      ) {

      result = false;
    }

    return true;
  }

  render() {
    try{
      return (
            <LayoutPrimary
              showTitle={true}
              showOrderOptionButton={true}
              showBackButton={true}
              title={this.getTitle()}
              navigator={this.props.navigator}
              showTabBar={false}
              contentContainerStyle={{}}
            >
              {this.renderContent()}
            </LayoutPrimary>
        )
      if (!this.props.orderCreated.showCreated) {
        
      }
      else{
        return (
          <View style={{flex:1, backgroundColor:'white'}}/>
        );
      }
    }
    catch(error){
      console.log(error, 'order_content.render()');
      return null;
    }
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    alignItems:'center',
  }
});


function mapStateToProps(state, ownProps) {
	return {
    orderData: state.rootReducer.order.active.data,
    orderActiveType: state.rootReducer.order.active.orderActiveType,
    userLocation: state.rootReducer.user.location,
    directionCoords: state.rootReducer.map.directionCoords,
    centerCoords: state.rootReducer.map.centerCoords,
    orderCreated:state.rootReducer.pos.orderCreated,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    actionUser: bindActionCreators(actions.user, dispatch),
    actionMap: bindActionCreators(actions.map, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderContent);

