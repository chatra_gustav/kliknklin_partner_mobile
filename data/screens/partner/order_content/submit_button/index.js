import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions,
  Image
} from 'react-native';
import PropTypes from 'prop-types';
import ResolveAssetSource from 'resolveAssetSource';

import {TextWrapper, Button, Scaling, toCurrency} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";
import * as style from './../style';

class SubmitButton extends Component{
	static defaultProps = {
	  onPress: () => {},
	}

	constructor(props) {
		super(props);
	  
	    this.state = {
	    	image:{
		        printer_icon:{
		            width:0,
		            height:0,
		            multiplierWidth:0.3,
		            multiplierHeight:0.3,
		            url:require('app/data/image/order_detail/printer_icon.png'),
		        },
		        email_icon:{
		            width:0,
		            height:0,
		            multiplierWidth:0.3,
		            multiplierHeight:0.3,
		            url:require('app/data/image/order_detail/email_icon.png'),
		        }
		    },
			buttonColor:this.props.buttonColor,
			buttonText:this.props.buttonText,
			isShowSubmitButton:false
	    };
	  }

	componentWillMount(){
		this.configButton();
		this.configShowSubmitButton();
	}

	componentDidUpdate(prevProps, prevState){
		try{
			let {orderData} = this.props;

			if (JSON.stringify(orderData) != JSON.stringify(prevProps.orderData) ) {
				this.configButton();
				this.configShowSubmitButton();
			}
		}
		catch(error){

		}
	}

	componentDidMount(){
		this.getImageSize();
	}

	getImageSize(){
	    var image = this.state.image;

	    var printer_icon = ResolveAssetSource(this.state.image.printer_icon.url);

	    image.printer_icon.width = printer_icon.width * this.state.image.printer_icon.multiplierWidth;
	    image.printer_icon.height = printer_icon.height * this.state.image.printer_icon.multiplierHeight;
	    
		var email_icon = ResolveAssetSource(this.state.image.email_icon.url);

	    image.email_icon.width = email_icon.width * this.state.image.email_icon.multiplierWidth;
	    image.email_icon.height = email_icon.height * this.state.image.email_icon.multiplierHeight;

	    this.setState({image});
  	}

  	isPosOrder(){
  		let {orderHash} = this.props;
    	return orderHash.includes('POS');
  	}

	configButton(){

		try{
			let {orderData, orderActiveType, paidStatus} = this.props;


			if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.COMPLAINT) {
				let {complaint} = orderData;

				if (complaint.complaint_status_id == CONSTANT.COMPLAINT_STATUS.IN_PROGRESS) {
					this.setState({
						buttonColor: CONSTANT.COLOR.LIGHT_BLUE, 
						buttonText:'Selesaikan Komplain',
					});
				}
			}
			else{
				let {orderlaundry, payment} = orderData;
				let {order_status_laundry_id} = orderlaundry;

				console.log(order_status_laundry_id, CONSTANT.ORDER_STATUS.IN_PROGRESS_PICKUP, 'configButton.submit_button');

				if (order_status_laundry_id == CONSTANT.ORDER_STATUS.WAIT_PICKUP) {
					this.setState({
						buttonColor:CONSTANT.COLOR.BLUE, 
						buttonText:STRING.ORDER_DETAIL.SUBMIT_BUTTON.IN_PROGRESS_PICKUP,
					});
				}
				else if (order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_PICKUP) {
					this.setState({
						buttonColor:CONSTANT.COLOR.ORANGE, 
						buttonText:STRING.ORDER_DETAIL.SUBMIT_BUTTON.DONE_PICKUP,
					});
				}
				else if (order_status_laundry_id == CONSTANT.ORDER_STATUS.DONE_PICKUP) {
					this.setState({
						buttonColor:CONSTANT.COLOR.BLUE,
						buttonText:STRING.ORDER_DETAIL.SUBMIT_BUTTON.IN_PROGRESS_LAUNDRY,
					});
				}
				else if (order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_LAUNDRY) {

					if(this.isPosOrder()){
						this.setState({
							buttonColor:CONSTANT.COLOR.GREEN,
							buttonText:'Diambil Pelanggan',
						});
						if(paidStatus == 2){ // done
							// this.setState({
							// 	buttonColor:CONSTANT.COLOR.GREEN,
							// 	buttonText:STRING.ORDER_DETAIL.SUBMIT_BUTTON.DONE_LAUNDRY,
							// });
						}else{
							
						}
					}else{
						this.setState({
							buttonColor:CONSTANT.COLOR.GREEN,
							buttonText:STRING.ORDER_DETAIL.SUBMIT_BUTTON.DONE_LAUNDRY,
						});
					}
				}
				else if (order_status_laundry_id == CONSTANT.ORDER_STATUS.DONE_LAUNDRY) {
					this.setState({
						buttonColor:CONSTANT.COLOR.BLUE, 
						buttonText:STRING.ORDER_DETAIL.SUBMIT_BUTTON.IN_PROGRESS_DELIVERY,
					});
				}
				else if (order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_DELIVERY) {
					this.setState({
						buttonColor:CONSTANT.COLOR.ORANGE, 
						buttonText:STRING.ORDER_DETAIL.SUBMIT_BUTTON.DONE_DELIVERY,
					});
				}

			}
			
		}
		catch(error){
			console.log(error, 'error.configButton.submit_button');
		}
	}

	async onPress(){
		try{
			let {orderData, orderActiveType, paidStatus, onPress, orderId, orderPrice} = this.props;
			if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.COMPLAINT) {
				let {complaint} = orderData;

				if (complaint.complaint_status_id == CONSTANT.COMPLAINT_STATUS.IN_PROGRESS) {
					await this.props.actionComplaint.markComplaintSolved(this.props.navigator);

					onPress();
				}
			}
			else{
				let {orderlaundry, payment} = orderData;
				let {order_status_laundry_id} = orderlaundry;

				if (order_status_laundry_id == CONSTANT.ORDER_STATUS.WAIT_PICKUP) {
					this.props.actionOrder.submitStartPickup(this.props.navigator);
				}
				else if (order_status_laundry_id == CONSTANT.ORDER_STATUS.DONE_LAUNDRY) {
					this.props.actionOrder.submitStartPickup(this.props.navigator);
				}
				else if (order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_PICKUP) {
					this.props.actionOrder.submitFinishPickup(this.props.navigator);
				}
				else if (order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_DELIVERY) {
					this.props.actionOrder.submitConfirmItem(this.props.navigator);
				}
				else if (order_status_laundry_id == CONSTANT.ORDER_STATUS.DONE_PICKUP) {
					this.props.actionOrder.submitInProgressLaundry(this.props.navigator);
				}
				else if (order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_LAUNDRY) {
					if(this.isPosOrder() && paidStatus == 1){ // belum bayar
					 	this.props.actionPOS.showPaymentBox('pay-later', (value) => {
                			let change = value - orderPrice;

                          	this.props.actionOrder.submitFinishLaundry(this.props.navigator, orderId, change, 'paid-order' );
                        }, 
                        'Jumlah yang harus dibayarkan : ' + toCurrency(orderPrice, 'Rp '),
                        orderPrice);
					}
					else{
						this.props.actionOrder.submitFinishLaundry(this.props.navigator, orderId, -1, 'taken-order');	
					}
				}
				else{
					this.props.onPress();
				}
			}
		}
		catch(error){

		}
	}

	showPaymentBox(){
		//not used
		this.props.actionNavigator.showLightBox(CONSTANT.ROUTE_TYPE.BOX_SINGLE_INPUT, {
            title:'Jumlah Uang Pelanggan',
            onPressButtonPrimary:(value)=>{
                let {orderPrice, orderId} = this.props;
                let change = value - orderPrice;
                this.props.actionOrder.submitFinishLaundry(this.props.navigator, orderId, change, 'paid-order' );
            },
            buttonPrimaryText:'Bayar',
            placeholder:'masukkan uang pelanggan',
            validation:(value) => {
                let {orderPrice} = this.props;
                if (parseInt(value) < parseInt(orderPrice)) {
                    return 'Uang yang dibayarkan tidak bisa kurang dari ' + toCurrency(orderPrice, 'Rp');
                }
                else{
                    return '';
                }
            }
        });
	}

 	async configShowSubmitButton(){
		let result = false;
		try{
			let {userRole, orderData, orderActiveType, actionComplaint,} = this.props;
			
			if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.COMPLAINT) {
				let {complaint} = orderData;
				await actionComplaint.isSolutionAccepted()
					.then((response) => {
						console.log(response);
						if (complaint.complaint_status_id == CONSTANT.COMPLAINT_STATUS.IN_PROGRESS && response) {
							result = true;
						}
					})
					.catch((error) => {

					});
				
			}
			else{
				let {shipmentMethodId} = this.props;
				let {orderlaundry, payment} = orderData;
				let {order_status_laundry_id} = orderlaundry;
				let {payment_status_id, payment_method_id} = payment;

	  			if(shipmentMethodId == CONSTANT.SHIPMENT_TYPE.SELF_SHIPMENT){
	  				if (order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_LAUNDRY) {
	  					result = true;
	  				}
	  				else{
	  					result = false;
	  				}
	  			}else{
	  				if(orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP &&
						order_status_laundry_id == CONSTANT.ORDER_STATUS.WAIT_PICKUP 
						// &&
						// (shipmentStatusPickup == CONSTANT.SHIPMENT_STATUS.ACTIVE ||
						// shipmentStatusPickup == CONSTANT.SHIPMENT_STATUS.LATE
						// )
						){
						if (userRole == CONSTANT.USER_ROLE.OWNER ||
							userRole == CONSTANT.USER_ROLE.ADMIN_OUTLET ||
							userRole == CONSTANT.USER_ROLE.COURIER
							) {
							result = true;
						}
					}
					else if (
						orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP &&
						order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_PICKUP 
						// &&
						// (shipmentStatusPickup == CONSTANT.SHIPMENT_STATUS.ACTIVE ||
						// shipmentStatusPickup == CONSTANT.SHIPMENT_STATUS.LATE ||
						// shipmentStatusPickup == CONSTANT.SHIPMENT_STATUS.DELAY_PARTNER)
					) {
						if (userRole == CONSTANT.USER_ROLE.OWNER ||
							userRole == CONSTANT.USER_ROLE.ADMIN_OUTLET ||
							userRole == CONSTANT.USER_ROLE.COURIER
							) {
							result = true;
						}
					}
					else if (
						orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP &&
						order_status_laundry_id == CONSTANT.ORDER_STATUS.DONE_PICKUP
					) {
						if (userRole == CONSTANT.USER_ROLE.OWNER ||
							userRole == CONSTANT.USER_ROLE.ADMIN_OUTLET
							) {
							result = true;
						}
					}
					else if (
						orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.IN_PROGRESS &&
						order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_LAUNDRY
					) {
						if (userRole == CONSTANT.USER_ROLE.OWNER ||
							userRole == CONSTANT.USER_ROLE.ADMIN_OUTLET
							) {
							result = true;
						}
					}
					else if(orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY &&
						order_status_laundry_id == CONSTANT.ORDER_STATUS.DONE_LAUNDRY 
						// &&
						// (shipmentStatusDelivery == CONSTANT.SHIPMENT_STATUS.ACTIVE ||
						// 	shipmentStatusDelivery == CONSTANT.SHIPMENT_STATUS.LATE
						// 	)
						){
						let {online_flag} = orderData;
						if (!online_flag) {
							result = false;
						}
						else if (
							(payment_status_id == CONSTANT.PAYMENT_STATUS.UNPAID ||
							payment_status_id == CONSTANT.PAYMENT_STATUS.WAIT_FOR_APPROVAL ||
							payment_status_id == CONSTANT.PAYMENT_STATUS.CANCEL) &&
							payment_method_id == CONSTANT.PAYMENT_METHOD.BANK_TRANSFER
						) {
							result = false;
						}
						else{
							if (userRole == CONSTANT.USER_ROLE.OWNER ||
								userRole == CONSTANT.USER_ROLE.ADMIN_OUTLET ||
								userRole == CONSTANT.USER_ROLE.COURIER
								) {
								result = true;
							}
						}
						
					}
					else if (
						orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY &&
						order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_DELIVERY 
						// &&
						// (shipmentStatusDelivery == CONSTANT.SHIPMENT_STATUS.ACTIVE ||
						// shipmentStatusDelivery == CONSTANT.SHIPMENT_STATUS.LATE ||
						// shipmentStatusDelivery == CONSTANT.SHIPMENT_STATUS.DELAY_PARTNER)
					) {
						if (userRole == CONSTANT.USER_ROLE.OWNER ||
							userRole == CONSTANT.USER_ROLE.ADMIN_OUTLET ||
							userRole == CONSTANT.USER_ROLE.COURIER
							) {
							result = true;
						}
					}
	  			}
			}
			
			// if ((
			// 	//status order : sedang dalam perjalanan, status shipment: ditunda karena customer -pickup
			// 	(orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP && 
			// 		order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_PICKUP && 
			// 		shipmentStatusPickup == CONSTANT.SHIPMENT_STATUS.DELAY_CUSTOMER) 
			// 	||
			// 	//status order : sedang dalam perjalanan, status shipment: ditunda karena customer -delivery
			// 	(orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY && 
			// 		order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_DELIVERY &&
			// 	 	shipmentStatusDelivery == CONSTANT.SHIPMENT_STATUS.DELAY_CUSTOMER)) 
			// 	||
			// 	//status order : sudah diantar
			// 	order_status_laundry_id == CONSTANT.ORDER_STATUS.DONE_DELIVERY
			// 	// ||
			// 	// //status order: menunggu dijemput, shipment status: selain aktif
			// 	// (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP && 
			// 	// 	order_status_laundry_id == CONSTANT.ORDER_STATUS.WAIT_PICKUP &&
			// 	//  	shipmentStatusDelivery != CONSTANT.SHIPMENT_STATUS.ACTIVE)
			// 	){
			// 	return null;
			// }
			// else{
			// 	return(
			// 		<View style={styles.buttonContainer}>
			// 			<Button.TypeA
			// 				onPress={this.onPress.bind(this)}
			// 				buttonColor={this.state.buttonColor}
			// 				buttonText={this.state.buttonText}
			// 			/>
			// 	    </View>
			// 	);
			// }
		}
		catch(error){
			console.log(error, 'order_content.submit_button.render()');
		}

		this.setState({isShowSubmitButton:result});
	}

	render(){
		try{
			return(
				<View style={[style.content.itemDetail, styles.button, this.props.buttonStyle]}>
		            {this.email_print_button()}
				    {this.submitButton()}
	      		</View>
			)
		}
		catch(error){
			console.log(error, 'order_content.submit_button.render()');
		}

		return null;
	}

	isPrintButtonAvailable(){
		let {orderActiveType} = this.props;

		if(orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP){
			return false
		}

		return this.props.complaintStatus == undefined
	}

	email_print_button(){

		let {userRole, orderData} = this.props;

		if(this.isPrintButtonAvailable()){
			if (userRole == CONSTANT.USER_ROLE.OWNER || userRole == CONSTANT.USER_ROLE.ADMIN_OUTLET) {
				return(
					<View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%'}}>
					    <TouchableOpacity
							style={[{backgroundColor: CONSTANT.COLOR.WHITE}, styles.buttonStyle, {width: '47%', borderColor: CONSTANT.COLOR.LIGHT_BLUE}]}
							onPress={() => this.props.actionBluetoothDevices.sendEmail(orderData)}
					    >
					    	<View style={{marginRight: Scaling.moderateScale(15)}}>
			                	<Image
			                    	style={{width: this.state.image.email_icon.width, height: this.state.image.email_icon.height}}
			                    	source={this.state.image.email_icon.url}
			                  	/>
			                </View>

					    	<TextWrapper
					    		type={this.props.buttonTextType}
					    		fontWeight={'SemiBold'}
								style={[styles.buttonTextStyle, {color: CONSTANT.COLOR.BLUE}]}
					    	>
								{'Kirim Nota'}
					    	</TextWrapper>
					    </TouchableOpacity>

					    <TouchableOpacity
							style={[{backgroundColor: CONSTANT.COLOR.WHITE}, styles.buttonStyle, {width: '47%', borderColor: CONSTANT.COLOR.LIGHT_BLUE}]}
							onPress={() => this.props.actionBluetoothDevices.cetak(orderData)}
					    >
					    	<View style={{marginRight: Scaling.moderateScale(15)}}>
			                	<Image
			                    	style={{width: this.state.image.printer_icon.width, height: this.state.image.printer_icon.height}}
			                    	source={this.state.image.printer_icon.url}
			                  	/>
			                </View>

					    	<TextWrapper
					    		type={this.props.buttonTextType}
					    		fontWeight={'SemiBold'}
								style={[styles.buttonTextStyle, {color: CONSTANT.COLOR.BLUE}]}
					    	>
								{'Cetak Nota'}
					    	</TextWrapper>
					    </TouchableOpacity>
					</View>
				)
			}
			return null
		}
		return null
	}


	submitButton(){
		if(this.state.isShowSubmitButton){
			return(

	            <TouchableOpacity
					style={[{backgroundColor: this.state.buttonColor}, styles.buttonStyle, {marginTop: Scaling.verticalScale(10), borderColor: CONSTANT.COLOR.LIGHT_GRAY}]}
					onPress={this.onPress.bind(this)}
			    >
			    	<TextWrapper
			    		type={this.props.buttonTextType} 
			    		fontWeight={'SemiBold'}
						style={[styles.buttonTextStyle, {color: CONSTANT.COLOR.WHITE, fontFamily: CONSTANT.TEXT_FAMILY.MAIN_BOLD}]}
			    	>
						{this.state.buttonText}
			    	</TextWrapper>
			    </TouchableOpacity>

			);
		}
		return null
	}
}

SubmitButton.propTypes = {
  buttonColor:PropTypes.string,
  buttonText:PropTypes.string,
  onPress:PropTypes.func
}

SubmitButton.defaultProps = {
  	buttonColor:'black',
  	buttonText:'button',
  	onPress: () => {},
}

const styles = StyleSheet.create({
	buttonTextStyle:{
	    color: CONSTANT.COLOR.LIGHT_BLUE,
	    fontSize: Scaling.moderateScale(13), 
	    fontFamily: CONSTANT.TEXT_FAMILY.MAIN, 
	    fontWeight: CONSTANT.TEXT_WEIGHT.REGULAR.weight
	},
	buttonStyle:{
		flexDirection: 'row',
	    borderRadius: Scaling.moderateScale(10),
	    height:CONSTANT.STYLE.BUTTON.THICKER_EXTRA_MEDIUM.HEIGHT,
	    width:'100%',
	    alignItems:'center',
		justifyContent:'center',
		borderWidth: 1
	},
	button:{
		width: '100%',
		alignItems: 'center',
		paddingVertical: Scaling.verticalScale(10),
		backgroundColor: CONSTANT.COLOR.WHITE
	}
});

function mapStateToProps(state, ownProps) {
	return {
		pos: state.rootReducer.pos,
		userRole: state.rootReducer.user.role,
		orderData: state.rootReducer.order.active.data,
		orderActiveType: state.rootReducer.order.active.orderActiveType,
    	shipmentActivePickup: state.rootReducer.order.shipmentActivePickup,
    	shipmentActiveDelivery: state.rootReducer.order.shipmentActiveDelivery,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionOrder: bindActionCreators(actions.order, dispatch),
		actionComplaint: bindActionCreators(actions.complaint, dispatch),
		actionBluetoothDevices: bindActionCreators(actions.bluetoothDevice, dispatch),
		actionNavigator: bindActionCreators(actions.navigator, dispatch),
		actionPOS: bindActionCreators(actions.pos, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SubmitButton);