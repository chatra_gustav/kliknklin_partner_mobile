import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Image,
        StyleSheet
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';
import ImagePicker from 'react-native-image-picker';

import {LayoutPrimary} from './../../../layout';
import {Text, Button, Form, TabBar, OrderStatus, Avatar, Scaling, DateFormat} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class IotMachine extends Component{

  constructor(props){
    super(props);
    this.state={
      image:{
        arrow_right_gray:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('./../../../image/attendance/arrow_right_gray.png'),
        },
        arrow_right_blue:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('./../../../image/attendance/arrow_right_blue.png'),
        },
        hanger_icon:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('./../../../image/iot_machine/hanger_icon.png'),
        },
        raindrop_icon:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('./../../../image/iot_machine/raindrop_icon.png'),
        },
        temperature_icon:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('./../../../image/iot_machine/temperature_icon.png'),
        }
      },
      totalOrder:{
        pos: 30,
        online: 15
      },
      dateOfStatistics:{
        startDate: '',
        endDate: ''
      },
    }
  }

  componentDidMount(){
    this.setDateOfStatistics();
    this.getMachineStatistics();
    this.getImageSize();
  }

  setDateOfStatistics(){
    let today = new Date();
    let today_fullDate = DateFormat(today, 'fullDate');
    let today_isoDate = DateFormat(today, 'isoDate');

    this.state.dateOfStatistics.startDate = today_fullDate;

    let oneDayAfter = new Date(today.getTime());
    oneDayAfter.setDate(oneDayAfter.getDate() + 1);
    let oneDayAfter_fullDate = DateFormat(oneDayAfter, 'fullDate');
    let oneDayAfter_isoDate = DateFormat(oneDayAfter, 'isoDate');

    this.state.dateOfStatistics.endDate = oneDayAfter_fullDate;

  }

  getMachineStatistics(){
    var image = this.state.image;

    var temperature_icon = ResolveAssetSource(this.state.image.temperature_icon.url);

    image.temperature_icon.width = temperature_icon.width * this.state.image.temperature_icon.multiplierWidth;
    image.temperature_icon.height = temperature_icon.height * this.state.image.temperature_icon.multiplierHeight;
    
    this.setState({image});
  }

  getImageSize(){
    var image = this.state.image;

    var arrow_right_blue = ResolveAssetSource(this.state.image.arrow_right_blue.url);

    image.arrow_right_blue.width = arrow_right_blue.width * this.state.image.arrow_right_blue.multiplierWidth;
    image.arrow_right_blue.height = arrow_right_blue.height * this.state.image.arrow_right_blue.multiplierHeight;

    var arrow_right_gray = ResolveAssetSource(this.state.image.arrow_right_gray.url);

    image.arrow_right_gray.width = arrow_right_gray.width * this.state.image.arrow_right_gray.multiplierWidth;
    image.arrow_right_gray.height = arrow_right_gray.height * this.state.image.arrow_right_gray.multiplierHeight;

    var hanger_icon = ResolveAssetSource(this.state.image.hanger_icon.url);

    image.hanger_icon.width = hanger_icon.width * this.state.image.hanger_icon.multiplierWidth;
    image.hanger_icon.height = hanger_icon.height * this.state.image.hanger_icon.multiplierHeight;

    var raindrop_icon = ResolveAssetSource(this.state.image.raindrop_icon.url);

    image.raindrop_icon.width = raindrop_icon.width * this.state.image.raindrop_icon.multiplierWidth;
    image.raindrop_icon.height = raindrop_icon.height * this.state.image.raindrop_icon.multiplierHeight;

    var temperature_icon = ResolveAssetSource(this.state.image.temperature_icon.url);

    image.temperature_icon.width = temperature_icon.width * this.state.image.temperature_icon.multiplierWidth;
    image.temperature_icon.height = temperature_icon.height * this.state.image.temperature_icon.multiplierHeight;
    
    this.setState({image});
  }



  render(){
      return (
        <LayoutPrimary
            showTitle={true}
            showBackButton={true}
            showOutletSelector={true}
            showSearchButton={true}
            showTabBar={false}
            navigator={this.props.navigator}
            titleText={'Mesin IOT'}
            contentContainerStyle={{
            }}
          >
            <View
              style={{
                flex:1,
                alignItems: 'center'
              }}
            >
              
              <View style={styles.header}>
                <StatisticsHeader 
                  image={this.state.image}
                  setDateRange={this.setDateRange.bind(this)}
                  dateOfStatistics={this.state.dateOfStatistics}
                />
              </View>

              <View
                style={{
                  flex:1,
                  paddingTop: Scaling.moderateScale(20),
                  width:Dimensions.get('window').width
                }}
              >
                <TotalOrder
                  image={this.state.image}
                />

                <Washer
                  image={this.state.image}
                  seeDetail={this.seeDetail.bind(this)}
                />

                <Drier
                  image={this.state.image}
                  seeDetail={this.seeDetail.bind(this)}
                />
              </View>
            </View>
        </LayoutPrimary>
      );
  }

  setDateRange(){

    this.props.actionNavigator.showLightBox(CONSTANT.ROUTE_TYPE.DATE_RANGE_PICKER,{
        
        onPressButtonPrimary: async (startDate, endDate) => {

          this.props.actionNavigator.dismissLightBox();

          let startDate_fullDate = DateFormat(startDate, 'fullDate');
          let startDate_isoDate = DateFormat(startDate, 'isoDate');

          this.state.dateOfStatistics.startDate = startDate_fullDate;

          let endDate_fullDate = DateFormat(endDate, 'fullDate');
          let endDate_isoDate = DateFormat(endDate, 'isoDate');

          this.state.dateOfStatistics.endDate = endDate_fullDate;

          this.getMachineStatistics();

        },
        onPressButtonSecondary: () =>{
          this.props.actionNavigator.dismissLightBox();
        },
    })
  }

  seeDetail(page){
    let dateOfStatistics = this.state.dateOfStatistics;
    this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.IOT_MACHINE_DETAIL, {page: page, dateOfStatistics: dateOfStatistics});
  }
}

class StatisticsHeader extends Component{
  render(){
    return(
      <View style={styles.statisticsHeader}>
          <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>Ringkasan Penggunaan Mesin</Text>

          <View style={{marginTop: Scaling.moderateScale(12)}}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT}
              textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>
                Periode
              </Text>
            
            <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center'}} hitSlop={{top: 5, left: 5, bottom: 5, right: 5}}
              onPress={this.showDatePicker}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>{this.props.dateOfStatistics.startDate} - {this.props.dateOfStatistics.endDate}</Text>
              
              <View style={{marginLeft: Scaling.moderateScale(10)}}>
                <Image
                  style={{width: this.props.image.arrow_right_blue.width, height: this.props.image.arrow_right_blue.height}}
                  source={this.props.image.arrow_right_blue.url}
                />
              </View>
            </TouchableOpacity>
          </View>
      </View>
    )
  }

  showDatePicker = () => {
    this.props.setDateRange();
  }
}

class TotalOrder extends Component{

  render(){
    return(
      <View
        style={{
          height:Dimensions.get('window').height * 0.16,
          backgroundColor:'white',
          elevation:3,
          marginBottom:Scaling.moderateScale(20),
          marginHorizontal:Scaling.moderateScale(2),
        }}
      >
        <View style={styles.contentContainer}>

          
          <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.GRAY}}>Total Order</Text>
          
          <View style={{flex: 1, justifyContent: 'center'}}>

            <View style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center'
            }}>

              <View style={{
                width: '55%',
                flexDirection: 'row',
                alignItems: 'center'
              }}>

                <View style={styles.left_icon}>
                  <Image
                    style={{width: this.props.image.hanger_icon.width, height: this.props.image.hanger_icon.height}}
                    source={this.props.image.hanger_icon.url}
                  />
                </View>

                <View style={styles.POS_Order}>
                  <View style={{alignItems: 'flex-start'}}>
                    <View style={{alignItems: 'center'}}>
                      <Text fontOption={CONSTANT.TEXT_OPTION.IOT_NUMBER_ORDER} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>30</Text>
                      <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT}>POS</Text>
                    </View>
                  </View>
                </View>
              </View>

              <View style={styles.Online_Order}>
                <Text fontOption={CONSTANT.TEXT_OPTION.IOT_NUMBER_ORDER} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>15</Text>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT}>Online</Text>
              </View>
            </View>
          </View>

        </View>
        
      </View>
    );
  }

  onPressTotalOrder = () => {
    
  }
}

class Washer extends Component{

  render(){
    return(
      <TouchableOpacity
        style={{
          height:Dimensions.get('window').height * 0.16,
          backgroundColor:'white',
          elevation:3,
          marginBottom: Scaling.moderateScale(20),
          marginHorizontal: Scaling.moderateScale(2),
        }}

        onPress={this.onPressWasher}
      >
        <View style={styles.contentContainer}>

          <View style={{flex: 1, justifyContent: 'center'}}>
            <View style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center'
            }}>

              <View style={{
                width: '55%',
                flexDirection: 'row',
                alignItems: 'center'
              }}>

                <View style={styles.left_icon}>
                  <Image
                    style={{width: this.props.image.raindrop_icon.width, height: this.props.image.raindrop_icon.height}}
                    source={this.props.image.raindrop_icon.url}
                  />
                </View>

                <View style={styles.laundryData}>
                  <View style={{justifyContent: 'flex-start', flexDirection: 'row'}}>

                    <Text fontOption={CONSTANT.TEXT_OPTION.IOT_NUMBER_ORDER} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK, fontSize: Scaling.moderateScale(30)}}>50</Text>
                    
                    <View style={{marginLeft: Scaling.moderateScale(10), marginBottom: Scaling.moderateScale(5), justifyContent: 'flex-end'}}>
                      <Text fontOption={CONSTANT.TEXT_OPTION.CONTENT}>Pencucian</Text>
                    </View>
                  
                  </View>
                </View>
              </View>

              <View style={styles.rightIcon}>
                <Image
                  style={{width: this.props.image.arrow_right_gray.width, height: this.props.image.arrow_right_gray.height}}
                  source={this.props.image.arrow_right_gray.url}
                />
              </View>
            </View>
          </View>

        </View>
        
      </TouchableOpacity>
    );
  }

  onPressWasher = () => {
    this.props.seeDetail('washer');
  }
}

class Drier extends Component{

  render(){
    return(
      <TouchableOpacity
        style={{
          height:Dimensions.get('window').height * 0.16,
          backgroundColor:'white',
          elevation:3,
          marginBottom: Scaling.moderateScale(20),
          marginHorizontal: Scaling.moderateScale(2),
        }}

        onPress={this.onPressDrier}
      >
        <View style={styles.contentContainer}>

          <View style={{flex: 1, justifyContent: 'center'}}>
            <View style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center'
            }}>

              <View style={{
                width: '55%',
                flexDirection: 'row',
                alignItems: 'center'
              }}>

                <View style={styles.left_icon}>
                  <Image
                    style={{width: this.props.image.temperature_icon.width, height: this.props.image.temperature_icon.height}}
                    source={this.props.image.temperature_icon.url}
                  />
                </View>

                <View style={styles.laundryData}>
                  <View style={{justifyContent: 'flex-start', flexDirection: 'row'}}>

                    <Text fontOption={CONSTANT.TEXT_OPTION.IOT_NUMBER_ORDER} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK, fontSize: Scaling.moderateScale(30)}}>33</Text>
                    
                    <View style={{marginLeft: Scaling.moderateScale(10), marginBottom: Scaling.moderateScale(5), justifyContent: 'flex-end'}}>
                      <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT}>Pengeringan</Text>
                    </View>
                  
                  </View>
                </View>
              </View>

              <View style={styles.rightIcon}>
                <Image
                  style={{width: this.props.image.arrow_right_gray.width, height: this.props.image.arrow_right_gray.height}}
                  source={this.props.image.arrow_right_gray.url}
                />
              </View>
            </View>
          </View>

        </View>
        
      </TouchableOpacity>
    );
  }

  onPressDrier = () => {
    this.props.seeDetail('drier');
  }
}


function mapStateToProps(state, ownProps) {
  return {
    layoutHeight:state.rootReducer.layout.height,
    userRole:state.rootReducer.user.role,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(IotMachine);

const styles = StyleSheet.create({
  header:{
    width: '100%',
    backgroundColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BACKGROUND_COLOR,
    borderBottomWidth:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_BOTTOM_WIDTH,
    borderColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_COLOR,
    flexDirection:'row'
  },
  statisticsHeader:{
    flex: 1,
    paddingTop: Dimensions.get('window').width * 0.025,
    paddingBottom: Dimensions.get('window').width * 0.025,
    paddingLeft: Dimensions.get('window').width * 0.025
  },
  contentContainer:{
    flex: 1,
    paddingTop: Dimensions.get('window').width * 0.025,
    paddingBottom: Dimensions.get('window').width * 0.025,
    paddingLeft: Dimensions.get('window').width * 0.025,
  },
  POS_Order:{
    width: '55%',
    justifyContent: 'center',
    borderRightWidth: StyleSheet.hairlineWidth,
    borderColor: CONSTANT.COLOR.GRAY
  },
  left_icon:{
    width: '45%'
  },
  Online_Order:{
    width: '45%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  rightIcon:{
    width: '45%',
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  laundryData:{
    width: '55%',
    justifyContent: 'center'
  }
})

