import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Image,
        StyleSheet,
        NativeModules,
        LayoutAnimation
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';
import ImagePicker from 'react-native-image-picker';

import {LayoutPrimary} from './../../../../layout';
import {Text, Button, Form, TabBar, OrderStatus, Avatar, Scaling} from './../../../../components';
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

class IotMachineSetting extends Component{

  constructor(props){
    super(props);
    this.state={
      image:{
        arrow_right_blue:{
            width:0,
            height:0,
            multiplierWidth:0.14,
            multiplierHeight:0.14,
            url:require('./../../../../image/attendance/arrow_right_blue.png'),
        },
        raindrop_icon:{
            width:0,
            height:0,
            multiplierWidth:0.33,
            multiplierHeight:0.33,
            url:require('./../../../../image/iot_machine/raindrop_icon.png'),
        },
        info_icon:{
            width:0,
            height:0,
            multiplierWidth:0.14,
            multiplierHeight:0.14,
            url:require('./../../../../image/icon/info_icon.png'),
        },
      }
    }
  }

  componentDidMount(){
    this._getDate();
    this.getImageSize();
  }

  getImageSize(){
    var image = this.state.image;

    var raindrop_icon = ResolveAssetSource(this.state.image.raindrop_icon.url);

    image.raindrop_icon.width = raindrop_icon.width * this.state.image.raindrop_icon.multiplierWidth;
    image.raindrop_icon.height = raindrop_icon.height * this.state.image.raindrop_icon.multiplierHeight;

    var arrow_right_blue = ResolveAssetSource(this.state.image.raindrop_icon.url);

    image.arrow_right_blue.width = arrow_right_blue.width * this.state.image.arrow_right_blue.multiplierWidth;
    image.arrow_right_blue.height = arrow_right_blue.height * this.state.image.arrow_right_blue.multiplierHeight;

    var info_icon = ResolveAssetSource(this.state.image.raindrop_icon.url);

    image.info_icon.width = info_icon.width * this.state.image.info_icon.multiplierWidth;
    image.info_icon.height = info_icon.height * this.state.image.info_icon.multiplierHeight;
    
    this.setState({image});
  }

  _getDate(){
    let daysList = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
    let monthsList = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

    let today = new Date();
    let day = today.getDay();
    let dd = today.getDate();
    let mm = today.getMonth()+1; //January is 0!
    let yyyy = today.getFullYear();

    let one_day_before = new Date(today.getTime());
    one_day_before.setDate(one_day_before.getDate() - 1);

    let day_one_day_before = one_day_before.getDay();
    let dd_one_day_before = one_day_before.getDate();
    let mm_one_day_before = one_day_before.getMonth()+1; //January is 0!
    let yyyy_one_day_before = one_day_before.getFullYear();

    let two_day_before = new Date(today.getTime());
    two_day_before.setDate(two_day_before.getDate() - 2);
    
    let day_two_day_before = two_day_before.getDay();
    let dd_two_day_before = two_day_before.getDate();
    let mm_two_day_before = two_day_before.getMonth()+1; //January is 0!
    let yyyy_two_day_before = two_day_before.getFullYear();


    let result = [
      {
        id: 1,
        value: daysList[day_two_day_before] +', '+ dd_two_day_before +' '+ monthsList[mm_two_day_before] +' '+ yyyy_two_day_before
      },
      {
        id: 2,
        value: daysList[day_one_day_before] +', '+ dd_one_day_before +' '+ monthsList[mm_one_day_before] +' '+ yyyy_one_day_before
      },
      {
        id: 3,
        value: daysList[day] +', '+ dd +' '+ monthsList[mm] +' '+ yyyy
      }
    ];

    return result;

  }

  render(){
      return (
        <LayoutPrimary
            showTitle={true}
            showBackButton={true}
            showOutletSelector={false}
            showSearchButton={false}
            showTabBar={false}
            navigator={this.props.navigator}
            titleText={'Pengaturan Perangkat'}
            contentContainerStyle={{
            }}
          >
            <View
              style={{
                flex:1,
                backgroundColor: '#FFFFFF',
                paddingHorizontal: Dimensions.get('window').width * 0.025,
              }}
            >

              <View style={[styles.row, styles.machineStatus]}>
                <View style={{flexDirection: 'row'}}>
                  <Image
                    style={{width: this.state.image.raindrop_icon.width, height: this.state.image.raindrop_icon.height}}
                    source={this.state.image.raindrop_icon.url}
                  />

                  <View style={{ justifyContent: 'space-around' }}>
                    <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>MC - 1223092</Text>
                    <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>Washer</Text>
                  </View>
                </View>

                <View style={styles.status}>
                  <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.GRAY}}>OFFLINE</Text>
                </View>
              </View>

              <View style={[styles.row, styles.machineOwner]}>
                <View style={styles.owner}>
                  <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>Owner</Text>
                  <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>Alia Lutfiyani</Text>
                </View>

                <View style={styles.transferOwnership}>
                  <View style={styles.transferOwnershipText}>
                    <Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_CONTENT_SMALLER} textStyle={{color: CONSTANT.COLOR.WHITE}}>Transfer Ownership</Text>
                  </View>
                </View>
              </View>

              <View style={[styles.row, styles.machineOutlet]}>
                <View style={styles.outletLabel}>
                  <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>Outlet</Text>

                  <View style={styles.outletName}>
                    <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>Laundryklin Tubagus Ismail</Text>

                    <Image
                      style={{width: this.state.image.arrow_right_blue.width, height: this.state.image.arrow_right_blue.height, position: 'absolute', right: Scaling.moderateScale(-7)}}
                      source={this.state.image.arrow_right_blue.url}
                    />
                  </View>
                </View>
              </View>

              <View style={[styles.row, styles.machineWifi]}>
                <View style={styles.wifiLabel}>
                  <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>Wifi</Text>

                  <View style={styles.wifiName}>
                    <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>Kantor Kemang</Text>

                    <Image
                      style={{width: this.state.image.arrow_right_blue.width, height: this.state.image.arrow_right_blue.height, position: 'absolute', right: Scaling.moderateScale(-7)}}
                      source={this.state.image.arrow_right_blue.url}
                    />
                  </View>
                </View>

                <View style={{marginTop: Scaling.moderateScale(20)}}>
                  <View style={styles.wifiConfig}>
                    <Image
                      style={{width: this.state.image.info_icon.width, height: this.state.image.info_icon.height, position: 'absolute', left: Scaling.moderateScale(-5)}}
                      source={this.state.image.info_icon.url}
                    />

                    <View style={styles.wifiConfigLabel}>
                      <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>Info pengaturan Wifi</Text>
                    </View>
                  </View>

                  <View style={styles.wifiConfigInfo}>
                    <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{fontFamily: CONSTANT.TEXT_FAMILY.MAIN_LIGHT}}>
                      1. Koneksi perangkat (hp/Laptop) pada apn dari device: dilihat dari nama device
                    </Text>
                    <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{fontFamily: CONSTANT.TEXT_FAMILY.MAIN_LIGHT}}>
                      2. Buka mobile / web view soket pada ip yang sudah ditentukan
                    </Text>
                    <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{fontFamily: CONSTANT.TEXT_FAMILY.MAIN_LIGHT}}>
                      3. Isi form ssid dan password lalu connect
                    </Text>
                  </View>
                </View>
              </View>

            </View>


        </LayoutPrimary>
      );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    layoutHeight:state.rootReducer.layout.height,
    userRole:state.rootReducer.user.role,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(IotMachineSetting);

const styles = StyleSheet.create({
  header:{
    width: '100%',
    backgroundColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BACKGROUND_COLOR,
    borderBottomWidth:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_BOTTOM_WIDTH,
    borderColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_COLOR,
    flexDirection:'row'
  },
  status:{
    position: 'absolute',
    top: Scaling.moderateScale(10),
    right: 0,
    width:'25%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: CONSTANT.COLOR.LIGHTER_GRAY,
    padding: Scaling.moderateScale(5),
    borderRadius: 10
  },
  row:{
    height:Dimensions.get('window').height * 0.14,
    borderBottomWidth: 1,
    borderColor: CONSTANT.COLOR.LIGHT_GRAY,
    paddingTop: Scaling.moderateScale(5)
  },
  transferOwnership:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  transferOwnershipText:{
    borderRadius: 10, 
    backgroundColor: CONSTANT.COLOR.ORANGE, 
    justifyContent: 'center', 
    alignItems: 'center', 
    paddingHorizontal: Scaling.moderateScale(15), 
    paddingVertical: Scaling.moderateScale(5)
  },
  wifiConfig:{
    flexDirection: 'row', 
    alignItems: 'center'
  },
  wifiConfigLabel:{
    paddingLeft: Scaling.moderateScale(20)
  },
  wifiConfigInfo:{
    borderRadius: 10,
    backgroundColor: CONSTANT.COLOR.LIGHTER_GRAY,
    padding: Scaling.moderateScale(10),
    marginVertical: Scaling.moderateScale(15)
  },
  machineStatus:{
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'space-between'
  },
  machineOwner:{
    paddingHorizontal: Scaling.moderateScale(10)
  },
  owner:{
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'space-between', 
    paddingTop: Scaling.moderateScale(10)
  },
  machineOutlet:{
    paddingHorizontal: Scaling.moderateScale(10)
  },
  outletLabel:{
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'space-between', 
    paddingTop: Scaling.moderateScale(10)
  },
  outletName:{
    flexDirection: 'row', 
    alignItems: 'center',
    paddingRight: Scaling.moderateScale(20)
  },
  machineWifi:{
    paddingHorizontal: Scaling.moderateScale(10),
    borderBottomWidth: 0
  },
  wifiLabel:{
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'space-between', 
    paddingTop: Scaling.moderateScale(10)
  },
  wifiName:{
    flexDirection: 'row', 
    alignItems: 'center', 
    paddingRight: Scaling.moderateScale(20)
  }
})

