import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Image,
        StyleSheet,
        NativeModules,
        LayoutAnimation
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';
import ImagePicker from 'react-native-image-picker';

import {LayoutPrimary} from './../../../../layout';
import {Text, Button, Form, TabBar, OrderStatus, Avatar, Scaling, DateFormat} from './../../../../components';
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

class IotMachineDetail extends Component{

  constructor(props){
    super(props);
    this.state={
      image:{
        arrow_right_blue:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('./../../../../image/attendance/arrow_right_blue.png'),
        },
        raindrop_icon:{
            width:0,
            height:0,
            multiplierWidth:0.14,
            multiplierHeight:0.14,
            url:require('./../../../../image/iot_machine/raindrop_icon.png'),
        },
        temperature_icon:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('./../../../../image/iot_machine/temperature_icon.png'),
        },
        setting_icon:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('./../../../../image/icon/setting_icon.png'),
        },
        arrow_down_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('./../../../../image/iot_machine/arrow_down_icon.png'),
        }
      },
      detailPenggunaan: [
        {
          startTime: '14-02-2019 08.00',
          endTime: '14-02-2019 20.00'
        },
        {
          startTime: '14-02-2019 08.00',
          endTime: '14-02-2019 20.00'
        },
        {
          startTime: '14-02-2019 08.00',
          endTime: '14-02-2019 20.00'
        }
      ],
      dateOfStatistics:{
        startDate: '',
        endDate: ''
      }
    }
  }

  componentDidMount(){
    this.setDateOfStatistics();
    this.getMachineDetail();
    this.getImageSize();
  }

  setDateOfStatistics(){

    this.state.dateOfStatistics.startDate = this.props.dateOfStatistics.startDate;
    this.state.dateOfStatistics.endDate = this.props.dateOfStatistics.endDate;


  }

  getMachineDetail(){
    var image = this.state.image;

    var temperature_icon = ResolveAssetSource(this.state.image.temperature_icon.url);

    image.temperature_icon.width = temperature_icon.width * this.state.image.temperature_icon.multiplierWidth;
    image.temperature_icon.height = temperature_icon.height * this.state.image.temperature_icon.multiplierHeight;
    
    this.setState({image});
  }

  getImageSize(){
    var image = this.state.image;

    var arrow_right_blue = ResolveAssetSource(this.state.image.arrow_right_blue.url);

    image.arrow_right_blue.width = arrow_right_blue.width * this.state.image.arrow_right_blue.multiplierWidth;
    image.arrow_right_blue.height = arrow_right_blue.height * this.state.image.arrow_right_blue.multiplierHeight;

    var setting_icon = ResolveAssetSource(this.state.image.setting_icon.url);

    image.setting_icon.width = setting_icon.width * this.state.image.setting_icon.multiplierWidth;
    image.setting_icon.height = setting_icon.height * this.state.image.setting_icon.multiplierHeight;

    var raindrop_icon = ResolveAssetSource(this.state.image.raindrop_icon.url);

    image.raindrop_icon.width = raindrop_icon.width * this.state.image.raindrop_icon.multiplierWidth;
    image.raindrop_icon.height = raindrop_icon.height * this.state.image.raindrop_icon.multiplierHeight;

    var temperature_icon = ResolveAssetSource(this.state.image.temperature_icon.url);

    image.temperature_icon.width = temperature_icon.width * this.state.image.temperature_icon.multiplierWidth;
    image.temperature_icon.height = temperature_icon.height * this.state.image.temperature_icon.multiplierHeight;

    var arrow_down_icon = ResolveAssetSource(this.state.image.arrow_down_icon.url);

    image.arrow_down_icon.width = arrow_down_icon.width * this.state.image.arrow_down_icon.multiplierWidth;
    image.arrow_down_icon.height = arrow_down_icon.height * this.state.image.arrow_down_icon.multiplierHeight;
    
    this.setState({image});
  }

  render(){
      return (
        <LayoutPrimary
            showTitle={true}
            showBackButton={true}
            showOutletSelector={true}
            showSearchButton={true}
            showTabBar={false}
            navigator={this.props.navigator}
            titleText={'Mesin IOT'}
            contentContainerStyle={{
            }}
          >
            <View
              style={{
                flex:1,
                alignItems: 'center'
              }}
            >
              
              <View style={styles.header}>
                <StatisticsHeader 
                  image={this.state.image}
                  setDateRange={this.setDateRange.bind(this)}
                  dateOfStatistics={this.state.dateOfStatistics}
                />
              </View>

              <View
                style={{
                  flex:1,
                  paddingTop: Scaling.moderateScale(20),
                  width:Dimensions.get('window').width
                }}
              >
                <TabBar.TypeD
                  item={[
                    {
                      itemText:CONSTANT.IOT_MACHINE_DETAIL_TAB.FIRST,
                    },
                    {
                      itemText:CONSTANT.IOT_MACHINE_DETAIL_TAB.SECOND,
                    },
                  ]}
                />

                <MachineDetail
                  image={this.state.image}
                  detailPenggunaan={this.state.detailPenggunaan}
                  openSettingPage={this.openSettingPage.bind(this)}
                />

              </View>
            </View>
        </LayoutPrimary>
      );
  }

  openSettingPage = () => {
    this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.IOT_MACHINE_SETTING);
  }

  setDateRange(){

    this.props.actionNavigator.showLightBox(CONSTANT.ROUTE_TYPE.DATE_RANGE_PICKER,{
        
        onPressButtonPrimary: async (startDate, endDate) => {

          this.props.actionNavigator.dismissLightBox();

          let startDate_fullDate = DateFormat(startDate, 'fullDate');
          let startDate_isoDate = DateFormat(startDate, 'isoDate');

          this.state.dateOfStatistics.startDate = startDate_fullDate;

          let endDate_fullDate = DateFormat(endDate, 'fullDate');
          let endDate_isoDate = DateFormat(endDate, 'isoDate');

          this.state.dateOfStatistics.endDate = endDate_fullDate;

          this.getMachineDetail();

        },
        onPressButtonSecondary: () =>{
          this.props.actionNavigator.dismissLightBox();
        },
    })
  }
}

class StatisticsHeader extends Component{
  render(){
    return(
      <View style={styles.statisticsHeader}>
          <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>Ringkasan Penggunaan Mesin</Text>

          <View style={{marginTop: Scaling.moderateScale(12)}}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>Periode</Text>
            
            <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center'}} hitSlop={{top: 5, left: 5, bottom: 5, right: 5}}
              onPress={this.showDatePicker}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>{this.props.dateOfStatistics.startDate} - {this.props.dateOfStatistics.endDate}</Text>
              
              <View style={{marginLeft: Scaling.moderateScale(10)}}>
                <Image
                  style={{width: this.props.image.arrow_right_blue.width, height: this.props.image.arrow_right_blue.height}}
                  source={this.props.image.arrow_right_blue.url}
                />
              </View>
            </TouchableOpacity>
          </View>
      </View>
    )
  }

  showDatePicker = () => {
    this.props.setDateRange();
  }
}

class MachineDetail extends Component{

  constructor(props){
    super(props);
    this.state={
      isDetailPenggunaanVisible: true
    }
  }

  render(){
    return(
      <View 
        style={{
          backgroundColor:'white',
          elevation:3,
          marginTop: Scaling.moderateScale(20),
          marginHorizontal: Scaling.moderateScale(2),
        }}>
        <View
          style={{
            height:Dimensions.get('window').height * 0.14
          }}
        >
          <View style={styles.contentContainer}>

            <View style={{flex: 1}}>
              <View style={styles.machineDetailHeader}>

                <View style={styles.machineId}>
                  <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>MC - 1223092</Text>
                </View>

                <View style={styles.machineStatus}>
                  <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.GRAY}}>OFFLINE</Text>
                </View>

                <TouchableOpacity style={styles.settingIcon} onPress={this.setting}>
                  <Image
                    style={{width: this.props.image.setting_icon.width, height: this.props.image.setting_icon.height}}
                    source={this.props.image.setting_icon.url}
                  />
                </TouchableOpacity>
              </View>

              <View style={styles.machineDetail}>

                <Image
                  style={{width: this.props.image.raindrop_icon.width, height: this.props.image.raindrop_icon.height}}
                  source={this.props.image.raindrop_icon.url}
                />

                <View style={styles.machineDetailRow}>

                  <View style={{flexDirection: 'row'}}>
                    <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{fontSize: Scaling.moderateScale(15)}}>33</Text>
                  
                    <View style={{marginLeft: Scaling.moderateScale(10), marginBottom: Scaling.moderateScale(2), justifyContent: 'flex-end'}}>
                      <Text fontOption={CONSTANT.TEXT_OPTION.CONTENT}>Pengeringan</Text>
                    </View>
                  </View>

                  <View style={styles.detailPenggunaan}>

                    <TouchableOpacity style={styles.detailPenggunaanText}
                      onPress={this.showDetail} hitSlop={styles.hitSlop}>
                      <Text fontOption={CONSTANT.TEXT_OPTION.CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>Detail Penggunaan</Text>
                    </TouchableOpacity>

                    <View style={styles.detailPenggunaanIcon}>
                      <Image
                        style={{width: this.props.image.arrow_down_icon.width, height: this.props.image.arrow_down_icon.height}}
                        source={this.props.image.arrow_down_icon.url}
                      />
                    </View>

                  </View> 
                </View>

              </View>

            </View>
          </View>
        </View>

        <DetailPenggunaan
          isVisible={this.state.isDetailPenggunaanVisible}
          detailPenggunaan={this.props.detailPenggunaan}
        />
      </View>
    );
  }

  showDetail = () => {

    const CustomLayoutLinear = {
        duration: 200,
        create: {
            type: LayoutAnimation.Types.linear,
            property: LayoutAnimation.Properties.opacity
        },
        update: {
            type: LayoutAnimation.Types.linear
        },
    };

    LayoutAnimation.configureNext(CustomLayoutLinear);

    this.setState({isDetailPenggunaanVisible: !this.state.isDetailPenggunaanVisible})
  }

  setting = () => {
    this.props.openSettingPage();
  }
}

class DetailPenggunaan extends Component{

  render(){

    if(!this.props.isVisible)
      return null

    return(
      <View style={{
        backgroundColor:'white',
        padding: Dimensions.get('window').width * 0.05,
        paddingTop: 0
      }}>

        <View style={styles.usingOfMachineDetailHeader}>
          <View style={styles.startTimeHeader}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>Waktu Mulai</Text>
          </View>

          <View style={styles.endTimeHeader}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>Waktu Selesai</Text>
          </View>
        </View>

        {this.props.detailPenggunaan.map((time, index) => {
          return(
            <View key={index} style={{width: '100%', flexDirection: 'row'}}>
              <View style={styles.startTime}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{fontFamily: CONSTANT.TEXT_FAMILY.MAIN_LIGHT}}>{time.startTime}</Text>
              </View>

              <View style={styles.endTime}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{fontFamily: CONSTANT.TEXT_FAMILY.MAIN_LIGHT}}>{time.endTime}</Text>
              </View>
            </View>
          )
        })}

      </View>
    );
  }
}


function mapStateToProps(state, ownProps) {
  return {
    layoutHeight:state.rootReducer.layout.height,
    userRole:state.rootReducer.user.role,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(IotMachineDetail);

const styles = StyleSheet.create({
  header:{
    width: '100%',
    backgroundColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BACKGROUND_COLOR,
    borderBottomWidth:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_BOTTOM_WIDTH,
    borderColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_COLOR,
    flexDirection:'row'
  },
  statisticsHeader:{
    flex: 1,
    paddingTop: Dimensions.get('window').width * 0.025,
    paddingBottom: Dimensions.get('window').width * 0.025,
    paddingLeft: Dimensions.get('window').width * 0.025
  },
  statisticsHeaderDate:{
    flexDirection: 'row', 
    alignItems: 'center'
  },
  contentContainer:{
    flex: 1,
    paddingTop: Dimensions.get('window').width * 0.025,
    paddingBottom: Dimensions.get('window').width * 0.025,
    paddingLeft: Dimensions.get('window').width * 0.025,
  },
  machineDetailHeader:{
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center'
  },
  machineDetail:{
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center'
  },
  machineId:{
    width:'25%'
  },
  machineStatus:{
    width:'25%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: CONSTANT.COLOR.LIGHTER_GRAY,
    padding: Scaling.moderateScale(5),
    borderRadius: 10
  },
  settingIcon:{
    width:'50%',
    alignItems: 'flex-end',
    paddingRight: Scaling.moderateScale(5)
  },
  usingOfMachineDetailHeader:{
    width: '100%', 
    flexDirection: 'row'
  },
  startTimeHeader:{
    width: '50%', 
    paddingBottom: Scaling.moderateScale(8), 
    justifyContent: 'center'
  },
  endTimeHeader:{
    width: '50%', 
    paddingBottom: Scaling.moderateScale(8), 
    justifyContent: 'center', 
    paddingLeft: Scaling.moderateScale(10)
  },
  startTime:{
    width: '50%', 
    paddingTop: Scaling.moderateScale(7), 
    paddingBottom: Scaling.moderateScale(7), 
    justifyContent: 'center', 
    borderRightWidth: StyleSheet.hairlineWidth, 
    borderColor: CONSTANT.COLOR.LIGHT_GRAY, 
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  endTime:{
    width: '50%', 
    paddingTop: Scaling.moderateScale(7), 
    paddingBottom: Scaling.moderateScale(7), 
    justifyContent: 'center', 
    paddingLeft: Scaling.moderateScale(10), 
    borderColor: CONSTANT.COLOR.LIGHT_GRAY, 
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  machineDetailRow:{
    flex: 1, flexDirection: 'row', justifyContent: 'space-between'
  },
  detailPenggunaan:{
    flexDirection: 'row', 
    marginRight: Scaling.moderateScale(5)
  },
  detailPenggunaanText:{
    paddingRight: Scaling.moderateScale(20), 
    marginBottom: Scaling.moderateScale(2), 
    justifyContent: 'flex-end'
  },
  detailPenggunaanIcon:{
    position: 'absolute', 
    bottom: -1, 
    right: 0, 
    zIndex: -1
  },
  hitSlop:{
    top: 5, 
    bottom: 5, 
    left: 5, 
    right: 5
  }
})

