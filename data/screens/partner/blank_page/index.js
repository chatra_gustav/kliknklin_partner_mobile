import React, { Component } from 'react';
import {
  View,
} from 'react-native';

import {LayoutPrimary} from 'app/data/layout';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class BlankPage extends Component {

	render() {
  	return (
  		<LayoutPrimary
          showTabBar={false}
          showNavBar={false}
          navigator={this.props.navigator}
        >
              
      </LayoutPrimary>
  	);
	}
}

function mapStateToProps(state, ownProps) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BlankPage);

