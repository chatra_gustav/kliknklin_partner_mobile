import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Image,
        StyleSheet,
        Alert
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import { Dropdown } from 'react-native-material-dropdown';

import {Text, fontCreator, Form, Scaling, TextTicker} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class TransactionStatusPicker extends Component{

  constructor(props) {
    super(props);
    this.state = {
      value:null,
      image:{
        arrow_down_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/iot_machine/arrow_down_icon.png'),
        }
      },
    };
  }

  componentDidMount(){
    this.getImageSize();
  }

  getImageSize(){
    var image = this.state.image;

    var arrow_down_icon = ResolveAssetSource(this.state.image.arrow_down_icon.url);

    image.arrow_down_icon.width = arrow_down_icon.width * this.state.image.arrow_down_icon.multiplierWidth;
    image.arrow_down_icon.height = arrow_down_icon.height * this.state.image.arrow_down_icon.multiplierHeight;

    this.setState({image});
  }

  focus(){
    this.dropdown_field.focus();
  }

  blur(){
    this.dropdown_field.blur();
  }
  
  setValue(value){
    this.setState({value:this.dropdown_field.value()});
  }

  selectedIndex(){
    this.dropdown_field.selectedIndex();
  }

  selectedItem(){
    this.dropdown_field.selectedItem();
  }

  isFocused(){
    this.dropdown_field.isFocused();
  }

  render(){
    const styles = Scaling.ScaledSheet.create({
      fontStyle:fontCreator(CONSTANT.TEXT_OPTION.LARGEST_CONTENT),
    });
    
    return (
      <Dropdown
        ref={(component) => {this.dropdown_field = component}}
        dropdownPosition={0}
        tickerDuration={7000}
        tickerLopp={true}
        tickerBounce={true}
        tickerRepeatSpacer={50}
        tickerMarqueeDelay={2000}
        containerStyle={{
          height: Scaling.verticalScale(44),
          width: Dimensions.get('window').width * 0.77,
          borderRadius: Scaling.moderateScale(5),
          paddingHorizontal: Scaling.moderateScale(15),
          justifyContent: 'center',
          backgroundColor: CONSTANT.COLOR.WHITE,
          elevation: 2
        }}
        inputContainerStyle={{
          justifyContent:'center',
        }}
        {...this.props}
        renderBase={(props) => {
          let {value, tickerDuration, tickerMarqueeDelay, tickerRepeatSpacer, tickerLoop, tickerBounce, inputContainerStyle, containerStyle, renderAccessory} = props;
          return (
            <View style={[{flexDirection:'row', alignItems:'center', justifyContent: 'space-between'}]}>
              <View style={{marginRight: Scaling.moderateScale(10)}}>
                  <TextTicker
                    style={[styles.fontStyle, inputContainerStyle, {color: CONSTANT.COLOR.LIGHT_BLUE}]}
                    duration={tickerDuration}
                    loop={tickerLoop}
                    bounce={tickerBounce}
                    repeatSpacer={tickerRepeatSpacer}
                    marqueeDelay={tickerMarqueeDelay}
                  >
                    {value}
                  </TextTicker>
              </View>

              {renderAccessory()}
            </View>
          );
        }}
        renderAccessory={(props) => {
          return(
            <View style={{height:Scaling.verticalScale(60 * 0.444), alignItems:'center', justifyContent: 'center'}}>
              <Image
                style={{width: this.state.image.arrow_down_icon.width, height: this.state.image.arrow_down_icon.height}}
                source={this.state.image.arrow_down_icon.url}
              />
            </View>
          );
        }}
        fontSize={Scaling.moderateScale(14)}
        textColor={CONSTANT.COLOR.LIGHT_BLUE}
      />
    );
  }
}

function mapStateToProps(state, ownProps) {

  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TransactionStatusPicker);

const styles = StyleSheet.create({
})

