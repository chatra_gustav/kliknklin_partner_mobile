import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Image,
        StyleSheet,
        Alert
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import { Dropdown } from 'react-native-material-dropdown';

import {Text, fontCreator, Form, Scaling, TextTicker} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class SubmitButton extends Component{

  constructor(props) {
    super(props);
    this.state = {
      value:null,
      image:{
        print:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/icon/print.png')
        }
      },
      showMoreItem: false,
      text: '',
      buttonColor: CONSTANT.COLOR.WHITE
    };
  }

  componentDidMount(){
    this.getImageSize();
  }

  getImageSize(){
    var image = this.state.image;

    var print = ResolveAssetSource(this.state.image.print.url);

    image.print.width = print.width * this.state.image.print.multiplierWidth;
    image.print.height = print.height * this.state.image.print.multiplierHeight;

    this.setState({image});
  }

  doneTransaction(){

    let {item} = this.props;

    let params = {
      transaction_id: item.id,
      payment_status_id: item.payment_status_id,
      transaction_status_id: 2,
    }

    this.props.actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeA',{
      title:'konfirmasi', 
      message: 'apakah anda yakin ingin menyelesaikan transaksi ini ?', 
      buttonPrimaryText: 'ya', 
      buttonSecondaryText: 'batal', 
      onPressButtonPrimary: async () => {
        this.props.actionNavigator.dismissLightBox();
        await this.props.actionPosOld.doneTransaction(params);
      },
      onPressButtonSecondary: () =>{
        this.props.actionNavigator.dismissLightBox();
      },
    });
  }

  payTransaction(){
    let {item} = this.props;

    let params = {
      transaction_id: item.id,
      payment_status_id: 2,
      transaction_status_id: item.transaction_status_id,
    }

    this.props.actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeA',{
      title:'konfirmasi', 
      message: 'apakah anda yakin ingin membayar transaksi ini ?', 
      buttonPrimaryText: 'ya', 
      buttonSecondaryText: 'batal', 
      onPressButtonPrimary: async () => {
        this.props.actionNavigator.dismissLightBox();
        await this.props.actionPosOld.payTransaction(params);
      },
      onPressButtonSecondary: () =>{
        this.props.actionNavigator.dismissLightBox();
      },
    });
  }

  gotoPaymentScreen(){
    this.props.actionPosOld.setActiveTransaction(this.props.item);
    this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.PaymentPosOldVersion');
  }

  _onPressButton = () => {

    let {item} = this.props;
    let {payment_status_id} = item;
    payment_status_id = Number(payment_status_id);
    
    if(payment_status_id !== 2){

      if(this.props.isPayTransaction){
        this.payTransaction();
      }else{
        this.gotoPaymentScreen();
      }
    }
    else{
      this.doneTransaction();
    }
  }

  _onPrint = () => {
    let {item} = this.props;
    let isOldVersion = true;

    this.props.actionBluetoothDevices.cetak(item, isOldVersion);
  }

  
  render(){

    let text = '';
    let buttonColor = '';

    let {item} = this.props;
    let {payment_status_id, transaction_status_id} = item;
    payment_status_id = Number(payment_status_id);
    
    if(payment_status_id == 2){
      text = 'Selesaikan Transaksi';
      buttonColor = CONSTANT.COLOR.BLUE
    }
    else{
      text = 'Bayar';
      buttonColor = CONSTANT.COLOR.GREEN
    }

    if(payment_status_id == 2 && transaction_status_id == 2){
      return(
        <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>
          {'Transaksi Selesai'}
        </Text>
      )
    }

    return (
      <View style={styles.container}>
        <TouchableOpacity style={[styles.button, {marginRight: Scaling.moderateScale(15), backgroundColor: buttonColor}]}
          onPress={this._onPrint}>
          <Image
            source={this.state.image.print.url}
            style={{width: this.state.image.print.width, height: this.state.image.print.height}}
          />
        </TouchableOpacity>

        <TouchableOpacity style={[styles.button, {width: Dimensions.get('window').width * 0.5, backgroundColor: buttonColor}]}
          onPress={this._onPressButton}>
          <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.WHITE}}>
            {text}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {

  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionPosOld: bindActionCreators(actions.posOldVersion, dispatch),
    actionBluetoothDevices: bindActionCreators(actions.bluetoothDevice, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SubmitButton);

const styles = StyleSheet.create({
  container:{
    flexDirection: 'row'
  },
  button:{
    justifyContent: 'center', 
    alignItems: 'center',
    borderRadius: Scaling.moderateScale(10),
    paddingVertical: Scaling.moderateScale(5),
    paddingHorizontal: Scaling.moderateScale(8)
  }
})

