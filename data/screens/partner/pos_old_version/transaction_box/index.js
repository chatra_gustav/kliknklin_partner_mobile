import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Image,
        StyleSheet,
        Alert,
        NativeModules,
        LayoutAnimation
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import { Dropdown } from 'react-native-material-dropdown';

import {Text, fontCreator, Form, Scaling, TextTicker, DateFormat, toCurrency} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

//local
import SubmitButton from './submit_button';

const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

class Transaction extends Component{

  constructor(props) {
    super(props);
    this.state = {
      value:null,
      image:{
        arrow_down_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/iot_machine/arrow_down_icon.png'),
        }
      },
      showMoreItem: false
    };
  }

  componentDidMount(){
    this.getImageSize();
  }

  getImageSize(){
    var image = this.state.image;

    var arrow_down_icon = ResolveAssetSource(this.state.image.arrow_down_icon.url);

    image.arrow_down_icon.width = arrow_down_icon.width * this.state.image.arrow_down_icon.multiplierWidth;
    image.arrow_down_icon.height = arrow_down_icon.height * this.state.image.arrow_down_icon.multiplierHeight;

    this.setState({image});
  }

  getTotalPrice(){
    let subTotalPrice = this.getSubtotalPrice();
    let expressCharge = this.getExpressCharge();

    return subTotalPrice + expressCharge;
  }

  getSubtotalPrice(){
    let subTotalPrice = 0;
    let {itemtransaction} = this.props.item;

    itemtransaction.map((item) => {
      subTotalPrice += this.getTotalItemPrice(item);
    })

    return subTotalPrice;
  }

  getTotalItemPrice(item){
    return item.sub_total_price - (item.sub_total_price * item.itemvariant.discount / 100);
  }

  getExpressCharge(){
    let expressCharge = 0;
    let {express_charge} = this.props.item;

    if(express_charge){
      expressCharge = express_charge;
    }

    return expressCharge;
  }
  
  render(){
    let {item} = this.props;
    console.log(item);
    let {customer, itemtransaction, transaction_hash, payment_status_id, outlet} = item;
    let end_date = item.end_time.split(" ")[0];
    let hh = item.end_time.split(" ")[1].split(":")[0];
    let mm = item.end_time.split(" ")[1].split(":")[1];
    let date = new Date(end_date);

    payment_status_id = Number(payment_status_id);
    let isPaid = (payment_status_id === 2 ? 'Lunas' : 'Belum Bayar');

    return (
      <View style={styles.container}>
        <View style={styles.header}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{transaction_hash}</Text>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} 
                  textStyle={{color: (isPaid == 'Lunas' ? CONSTANT.COLOR.LIGHT_BLUE : CONSTANT.COLOR.RED), fontFamily:CONSTANT.TEXT_FAMILY.MAIN_SEMI_BOLD}}>
              {isPaid} {':'} {toCurrency(this.getTotalPrice(itemtransaction).toString(), 'Rp ')}
            </Text>
        </View>

        <View style={styles.body}>

          <View style={styles.rowContent}>
            <View style={styles.contentLeft}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Outlet</Text>
            </View>

            <View style={styles.contentCenter}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>:</Text>
            </View>

            <View style={styles.contentRight}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>
                {outlet.name}
              </Text>
            </View>
          </View>

          <View style={styles.rowContent}>
            <View style={styles.contentLeft}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Konsumen</Text>
            </View>

            <View style={styles.contentCenter}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>:</Text>
            </View>

            <View style={styles.contentRight}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>
                {customer.name}
              </Text>
            </View>
          </View>

          <View style={styles.rowContent}>
            <View style={styles.contentLeft}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Est. Selesai</Text>
            </View>

            <View style={styles.contentCenter}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>:</Text>
            </View>

            <View style={styles.contentRight}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>
                {DateFormat(date, 'fullDate')} {hh+':'+mm}
              </Text>
            </View>
          </View>

          <View style={{marginVertical: Scaling.verticalScale(5)}}>
            {this.renderOrderItems(itemtransaction)}
            {this.renderMoreItemButton(itemtransaction)}
          </View>
        </View>

        <View style={styles.footer}>
          <SubmitButton
            {...this.props} 
          />
        </View>
      </View>
    );
  }

  renderOrderItems(itemtransaction){
    return(
      <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>
        {this.renderTotalItem(itemtransaction)} {this.getUnitMeasurement(itemtransaction)} {this.renderItemName(itemtransaction)}
      </Text>
    )
  }

  renderTotalItem(itemtransaction){
    try{
      return itemtransaction[0].total_item;
    }catch(err){
      return ''
    }
  }

  renderItemName(itemtransaction){
    try{
      return itemtransaction[0].itemvariant.item.name;
    }catch(err){
      return ''
    }
  }
  
  getUnitMeasurement(itemtransaction){
    try{
      let is_kilo = itemtransaction[0].itemvariant.item.is_kilo;

      if(is_kilo == 1){
        return 'kg'
      }
      else return ''
    }catch(err){
      return ''
    }
  }

  renderMoreItemButton(itemtransaction){
    let {item} = this.props;
    if(itemtransaction.length > 1){
      return(
        <TouchableOpacity onPress={() => {
          this.props.actionNavigator.showLightBox('PartnerKliknKlin.BoxPosOldItemDetail', {
            item: item
          });
        }}>
          <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>
            {'more item +'}
          </Text>
        </TouchableOpacity>
      )
    }
    return null
  }
}

function mapStateToProps(state, ownProps) {

  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Transaction);

const styles = StyleSheet.create({
  container:{
    flex: 1,
    width: Dimensions.get('window').width,
    backgroundColor: CONSTANT.COLOR.WHITE,
    marginVertical: Scaling.verticalScale(20),
    paddingHorizontal: Scaling.verticalScale(20),
    paddingVertical: Scaling.verticalScale(20),
    elevation: 2
  },
  header:{
    flexDirection:'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  body:{
    marginVertical: Scaling.verticalScale(20),
  },
  rowContent:{
    flexDirection: 'row',
    marginVertical: Scaling.verticalScale(5)
  },
  contentLeft: {
    flex: 0.2
  },
  contentCenter:{
    flex: 0.1,
    alignItems: 'center'
  },
  contentRight:{
    flex: 0.7,
  },
  footer:{
    marginVertical: Scaling.moderateScale(10),
    alignItems: 'center'
  }
})

