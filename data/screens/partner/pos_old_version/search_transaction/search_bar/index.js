import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity, Linking, ScrollView, Platform, BackHandler} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from 'app/data/layout';
import {Text, DateFormat, ShipmentScheduleListCourier, OutletSelector, Label, Scaling, Form} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

import debounce from 'lodash.debounce';

class SearchBar extends Component {
	constructor(props) {
        super(props);
        this.state = {
        	image:{
        		cancel_icon:{
        			width:0,
					height:0,
					multiplierWidth:0.35,
					multiplierHeight:0.35,
					url:require('app/data/image/membership/cancel_icon.png'),
        		}
		      },
        	searchCategory:[
        		{
        			id: 0,
        			value: CONSTANT.SEARCH_ORDER_CATEGORY.ID
        		},
        		{
        			id: 1,
        			value: CONSTANT.SEARCH_ORDER_CATEGORY.NAME
        		}
        	],
        	selectedSearchCategory: CONSTANT.SEARCH_ORDER_CATEGORY.NAME,
        	keyword: '',
        };  
    }

    componentDidMount(){
		this._getImageSize();
	}

	_getImageSize(){
		var image = this.state.image;

		var cancel_icon = ResolveAssetSource(this.state.image.cancel_icon.url);

		image.cancel_icon.width = Scaling.moderateScale(cancel_icon.width * this.state.image.cancel_icon.multiplierWidth);
		image.cancel_icon.height = Scaling.moderateScale(cancel_icon.height * this.state.image.cancel_icon.multiplierHeight);

      	this.setState({image});
	}

	render(){
		try{
			return(
				<View style={styles.container}>
					<View style={{flex: 0.23, alignItems: 'center'}}>
						<SearchCategory
							onChangeSearchBy={this.onChangeSearchBy.bind(this)}
						/>
					</View>

					<View style={{flex: 0.67, alignItems: 'center'}}>
						<SearchingBox
							searchCategory={this.state.selectedSearchCategory}
							onChangeText={this.onChangeText.bind(this)}
						/>
					</View>

					<View style={{flex: 0.1, alignItems: 'flex-end'}}>
						<TouchableOpacity onPress={this.onClose}>	
							<Image
								style={{width: this.state.image.cancel_icon.width, height: this.state.image.cancel_icon.height}}
								source={this.state.image.cancel_icon.url}
							/>
						</TouchableOpacity>
					</View>
				</View>
			);
		}
		catch(error){
			console.log(error, 'dashboard.index');
			return null;
		}
		
	}

	onChangeSearchBy(data){

		if(data.value == STRING.SCREEN.SEARCH_ORDER.SEARCH_BY_CUSTOMER_NAME){
			this.setState({selectedSearchCategory: CONSTANT.SEARCH_ORDER_CATEGORY.NAME}, () => {
				this.onChangeText(this.state.keyword);
			})	
		}else{
			this.setState({selectedSearchCategory: CONSTANT.SEARCH_ORDER_CATEGORY.ID}, () => {
				this.onChangeText(this.state.keyword);
			})
		}
	}

	onClose = () => {
		this.props.actionNavigator.pop(this.props.navigator);
	}

	onChangeText(keyword){
		if(keyword.length >= 3){
			this.props.actionPosOld.searchTransaction(keyword, this.state.selectedSearchCategory);
			this.setState({ keyword });
		}
	}
}

class SearchCategory extends Component{

	constructor(props){
		super(props);
		this.state = {
			data : [
				{
        			id: 0,
        			value: STRING.SCREEN.SEARCH_ORDER.SEARCH_BY_CUSTOMER_NAME
        		},
        		{
        			id: 1,
        			value: STRING.SCREEN.SEARCH_ORDER.SEARCH_BY_ORDER_ID
        		}
			]
		}
	}

	render(){
		return(
			<View style={styles.searchBy}>
				<Form.DropdownField.SearchBy
					ref={(ref) => this.dropdownSearchBy = ref }
	              	data={this.state.data} 
	              	value={this.state.data[0].value}
	              	onChangeText={(value, index, data) => {
	                	this._onChangeText(index);
	              	}}
				/>
			</View>
		)
	}

	_onChangeText(index){
    	this.props.onChangeSearchBy(this.state.data[index]);
  	}
}

class SearchingBox extends Component{

	constructor(props){
		super(props);

		this.onChangeTextDelayed = debounce(this.props.onChangeText, 1500);
	}

	render(){
		return(
			<View style={styles.searchingBox}>
	            <Form.InputField.TypeE
	              	keyboardType={'default'}
	              	ref={(ref) => this.search_by_ref = ref}
	              	onChangeText={this.onChangeTextDelayed.bind(this)}
	              	placeholder={this._getPlaceholder()}
	              	placeholderTextColor={'gray'}
	              	containerStyle={[
	                	{
	                  		width: Dimensions.get('window').width * 0.6,
	                  		height: Scaling.verticalScale(40),
	                  		backgroundColor: CONSTANT.COLOR.LIGHT_GRAY,
	                  		justifyContent: 'center',
	                  		borderRadius: 14,
	                  		paddingLeft: Scaling.moderateScale(12),
	                  		paddingTop: Scaling.verticalScale(5)
	                	}
	              	]}
	              	inputContainerStyle={{
	                	alignItems: 'center'
	              	}}
	              	inputTextStyle={{fontSize: Scaling.moderateScale(13), color: CONSTANT.COLOR.BLACK}}
	            />
			</View>
		)
	}

	_getPlaceholder(){
		let {searchCategory} = this.props;

		if(searchCategory == CONSTANT.SEARCH_ORDER_CATEGORY.NAME){
			return 'Nama konsumen'
		}else{
			return 'Id Pesanan'
		}
	}
}

function mapStateToProps(state, ownProps) {
	return {
		userRole:state.rootReducer.user.role,
	};
}

function mapDispatchToProps(dispatch) {
	return {
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
    	actionPosOld: bindActionCreators(actions.posOldVersion, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);

const styles = StyleSheet.create({
	container:{
		height:Scaling.verticalScale(CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.HEIGHT),
		paddingLeft:Scaling.scale(CONSTANT.STYLE.TAB_BAR.MAIN_DASHBOARD.PADDING_LEFT),
	    paddingRight:Scaling.scale(CONSTANT.STYLE.TAB_BAR.MAIN_DASHBOARD.PADDING_RIGHT),
	    backgroundColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BACKGROUND_COLOR,
	    borderBottomWidth:Scaling.moderateScale(CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_BOTTOM_WIDTH),
	    borderColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_COLOR,
	    flexDirection:'row',
	    alignItems: 'center'
	},
	searchBy:{
		justifyContent: 'center'
	},
	searchingBox:{
	}
});