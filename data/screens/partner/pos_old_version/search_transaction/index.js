import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity, Linking, ScrollView, Platform, BackHandler, FlatList} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from 'app/data/layout';
import {Text, DateFormat, ShipmentScheduleListCourier, OutletSelector, Label, Scaling} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

//local
import SearchBar from './search_bar';
import TransactionBox from '../transaction_box'

class SearchTransaction extends Component {
	constructor(props) {
        super(props);
        this.state = {};  
    }

    componentDidMount(){
    	this.props.actionPosOld.setSearchedTransactionList(this.props.transactionList);
    }

	render(){
		
		try{

			return(
				<LayoutPrimary
					showOutletSelector={false}
					showLogo={false}
					showSearchButton={false}
					showNotificationButton={false}
					showTabBar={false}
					showNavBar={false}
					navigator={this.props.navigator}
				>

					<SearchBar 
						navigator={this.props.navigator}
						searchingType={this.props.searchingType}
					/>

					{this.props.searchTransactionList.length == 0 ?
		                <View
		                  style={{
		                    flex:1,
		                    alignItems:'center',
		                  }}
		                >
		                    <Text
		                      textStyle={{
		                        textAlign:'center',
		                        marginTop:Scaling.verticalScale(50),
		                      }}
		                      fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}
		                    >{'Tidak ada pesanan yang ditemukan, \n jangan lupa untuk terus \n meningkatkan kualitas layanan anda'} </Text>
		                    <Image
		                        source={CONSTANT.IMAGE_URL.NO_ORDER_FOUND}
		                        style={{
		                          width:Scaling.scale(300),
		                          height:Scaling.verticalScale(300),
		                          resizeMode:'contain',
		                        }}
		                    /> 
		                </View>
		                :
		                <View>
		                  <FlatList
		                    showsVerticalScrollIndicator={false}
		                    contentContainerStyle={{
		                      paddingBottom:Scaling.verticalScale(200),
		                    }}
		                    initialNumToRender={4}
		                    data={this.props.searchTransactionList}
		                    keyExtractor={(item, index) => index+''}
		                    renderItem={({item, index}) => {

		                      return <TransactionBox 
		                        {...this.props} 
		                        item={item} 
		                      />;
		                    }}
		                /> 
		                </View>
		              }
			      	
			  	</LayoutPrimary>
			);
		}
		catch(error){
			console.log(error, 'dashboard.index');
			return null;
		}
		
	}
}

function mapStateToProps(state, ownProps) {
	return {
		userRole:state.rootReducer.user.role,
		transactionList: state.rootReducer.posOldVersion.transactionList,
		searchTransactionList: state.rootReducer.posOldVersion.searchTransactionList,
	};
}

function mapDispatchToProps(dispatch) {
	return {
    	actionPosOld: bindActionCreators(actions.posOldVersion, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchTransaction);

const styles = StyleSheet.create({
	tabBarContainer:{
		flexDirection: 'row', 
		alignItems: 'center',
		paddingLeft:Scaling.scale(CONSTANT.STYLE.TAB_BAR.MAIN_DASHBOARD.PADDING_LEFT),
	    paddingRight:Scaling.scale(CONSTANT.STYLE.TAB_BAR.MAIN_DASHBOARD.PADDING_RIGHT),
	    paddingVertical: Scaling.verticalScale(15)
	},
	tabBar: {
		paddingVertical: Scaling.verticalScale(5),
		paddingHorizontal: Scaling.moderateScale(10),
		borderRadius: 8,
		justifyContent: 'center',
		marginRight: Scaling.moderateScale(15)
	}
})
