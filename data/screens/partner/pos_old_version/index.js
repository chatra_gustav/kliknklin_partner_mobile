import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Image,
        StyleSheet,
        Alert
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';
import ImagePicker from 'react-native-image-picker';

import {LayoutPrimary} from 'app/data/layout';
import {Scaling, Text} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

//local
import TransactionStatusPicker from './transaction_status_picker'
import TransactionBox from './transaction_box'

class POS_OLD_VERSION extends Component{

  constructor(props){
    super(props);
    this.state={
      transactionStatus:[
        {
          id: 1,
          value: 'Semua Status Transaksi'
        },
        {
          id: 2,
          value: 'Belum Dibayar - Sudah Selesai'
        },
        {
          id: 3,
          value: 'Belum Dibayar - Sedang Dikerjakan'
        },
        {
          id: 4,
          value: 'Sudah Dibayar - Sedang Dikerjakan'
        }
      ],
      selectedTransactionStatus: {
        id: 1,
        value: 'Semua Status Transaksi'
      }
    }
  }

  async componentDidMount(){
    await this.getTransactions();
  }

  getTransactions(){
    this.props.actionPosOld.getTransaction();
  }

  configTransactionStatus(){
    this.props.actionPosOld.configTransactionStatus(this.state.selectedTransactionStatus);
  }

  _onChangeText(index){
    let selectedTransactionStatus = this.state.transactionStatus[index];
    this.setState({ selectedTransactionStatus }, () => {
      this.configTransactionStatus();
    });
  }

  render(){
      return (
        <LayoutPrimary
            showTitle={true}
            title={'Order POS Sistem Lama'}
            showBackButton={true}
            showOutletSelector={false}
            showSearchButton={false}
            navigator={this.props.navigator}
            showTabBar={false}
            showSearchButton={true}
            searchingType={'POS_OLD_VERSION'}
            contentContainerStyle={{
            }}
          >
            <View
              style={{
                flex:1,
                alignItems: 'center'
              }}
            >

              <View style={styles.searchBy}>
                <TransactionStatusPicker
                  data={this.state.transactionStatus} 
                  value={this.state.transactionStatus[0].value}
                  onChangeText={(value, index, data) => {
                    this._onChangeText(index);
                  }}
                />
              </View>

              {this.props.transactionList.length == 0 ?
                <View
                  style={{
                    flex:1,
                    alignItems:'center',
                  }}
                >
                    <Text
                      textStyle={{
                        textAlign:'center',
                        marginTop:Scaling.verticalScale(50),
                      }}
                      fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}
                    >{'Tidak ada pesanan yang ditemukan, \n jangan lupa untuk terus \n meningkatkan kualitas layanan anda'} </Text>
                    <Image
                        source={CONSTANT.IMAGE_URL.NO_ORDER_FOUND}
                        style={{
                          width:Scaling.scale(300),
                          height:Scaling.verticalScale(300),
                          resizeMode:'contain',
                        }}
                    /> 
                </View>
                :
                <View>
                  <FlatList
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{
                      paddingBottom:Scaling.verticalScale(200),
                    }}
                    initialNumToRender={4}
                    data={this.props.transactionList}
                    keyExtractor={(item, index) => index+''}
                    renderItem={({item, index}) => {

                      return <TransactionBox 
                        {...this.props} 
                        item={item} 
                      />;
                    }}
                /> 
                </View>
              }

            </View>
        </LayoutPrimary>
      );
  }
}

function mapStateToProps(state, ownProps) {

  return {
    transactionList: state.rootReducer.posOldVersion.transactionList
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionPosOld: bindActionCreators(actions.posOldVersion, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(POS_OLD_VERSION);

const styles = StyleSheet.create({
  searchBy:{
    marginVertical: Scaling.verticalScale(20)
  },
})

