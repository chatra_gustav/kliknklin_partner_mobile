import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Image,
        StyleSheet,
        Alert
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';
import ImagePicker from 'react-native-image-picker';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Form, TabBar, OrderStatus, Avatar, Scaling, DateFormat, toCurrency} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

//local
import SubmitButton from '../transaction_box/submit_button';

class POS_OLD_VERSION extends Component{

  constructor(props){
    super(props);
    this.state={
      image:{
        rupiah_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/icon/rupiah_icon.png'),
        }
      },
      customerMoney: 0,
      changeMoney: 0
    }
  }

  componentDidMount(){
    this.getImageSize();
  }

  getImageSize(){
    var image = this.state.image;

    var rupiah_icon = ResolveAssetSource(this.state.image.rupiah_icon.url);

    image.rupiah_icon.width = rupiah_icon.width * this.state.image.rupiah_icon.multiplierWidth;
    image.rupiah_icon.height = rupiah_icon.height * this.state.image.rupiah_icon.multiplierHeight;
    
    this.setState({image});
  }

  _onInputCash(customerMoney){

    let {itemtransaction} = this.props.activeTransaction;
    let totalPrice = this.getTotalPrice();

    let changeMoney = 0;
    if(customerMoney > totalPrice){
      changeMoney = customerMoney - totalPrice;
      this.setState({ customerMoney, changeMoney })
    }else{
      this.setState({ customerMoney, changeMoney })
    }
  }

  getTotalPrice(){
    let subTotalPrice = this.getSubtotalPrice();
    let expressCharge = this.getExpressCharge();

    return subTotalPrice + expressCharge;
  }

  getSubtotalPrice(){
    let subTotalPrice = 0;
    let {itemtransaction} = this.props.activeTransaction;

    itemtransaction.map((item) => {
      subTotalPrice += this.getTotalItemPrice(item);
    })

    return subTotalPrice;
  }

  getTotalItemPrice(item){
    return item.sub_total_price - (item.sub_total_price * item.itemvariant.discount / 100);
  }

  getExpressCharge(){
    let expressCharge = 0;
    let {express_charge} = this.props.activeTransaction;

    if(express_charge){
      expressCharge = express_charge;
    }

    return expressCharge;
  }

  getCustomerName(){
    let customerName = '';
    customerName = this.props.activeTransaction.customer.name;

    return customerName;
  }

  getUnitMeasurement(itemtransaction){
    try{
      let is_kilo = itemtransaction[0].itemvariant.item.is_kilo;

      if(is_kilo == 1){
        return 'kg'
      }
      else return ''
    }catch(err){
      return ''
    }
  }

  render(){
      return (
        <LayoutPrimary
            showTitle={true}
            title={'Pembayaran'}
            showBackButton={true}
            showOutletSelector={false}
            showSearchButton={false}
            navigator={this.props.navigator}
            showTabBar={false}
            contentContainerStyle={{
              backgroundColor: CONSTANT.COLOR.WHITE
            }}
          >
            <KeyboardAwareScrollView>
              <View
                style={{
                  flex: 1,
                  backgroundColor: CONSTANT.COLOR.WHITE,
                  paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                  paddingVertical:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT)
                }}
              >
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK, fontFamily: CONSTANT.TEXT_FAMILY.MAIN_SEMI_BOLD}}>
                  {this.getCustomerName()}
                </Text>
                <View style={{flex: 1, marginTop: Scaling.verticalScale(10)}}>
                  {this.getOrderItem()}
                  {this.renderDetailPayment()}
                  {this.renderTextInput()}
                  {this.renderChangeMoney()}
                  {this.renderButton()}
                </View>
              </View>
            </KeyboardAwareScrollView>
        </LayoutPrimary>
      );
  }

  getOrderItem(){
    let {itemtransaction} = this.props.activeTransaction;

    return(
      <View style={styles.orderItem}>
        {itemtransaction.map((item, index) => (
          <View key={index}>
            <View style={styles.orderDetail}>
              <View style={styles.detailLeft}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
                  {item.total_item} {this.getUnitMeasurement(itemtransaction)}
                </Text>
              </View>

              <View style={styles.detailCenter}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>
                  {item.itemvariant.item.name}
                </Text>
              </View>

              <View style={styles.detailRight}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
                  {toCurrency((item.sub_total_price).toString(), 'Rp ')}
                </Text>
              </View>
            </View>

            {this.renderItemNotes(item.notes)}
          </View>
        ))}
      </View>
    )
  }

  renderItemNotes(notes){
    if(notes != ''){
      return(
        <View style={styles.orderDetail}>
          <View style={styles.detailLeft}>
          </View>

          <View style={styles.detailCenter}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK, fontStyle: 'italic'}}>
              {notes}
            </Text>
          </View>

          <View style={styles.detailRight}>
          </View>
        </View>
      )
    }
    return null
  }

  getTotalPrice(){
    let subTotalPrice = this.getSubtotalPrice();
    let expressCharge = this.getExpressCharge();

    return subTotalPrice + expressCharge;
  }

  getSubtotalPrice(){
    let subTotalPrice = 0;
    let {itemtransaction} = this.props.activeTransaction;

    itemtransaction.map((item) => {
      subTotalPrice += this.getTotalItemPrice(item);
    })

    return subTotalPrice;
  }

  getTotalItemPrice(item){
    return item.sub_total_price - (item.sub_total_price * item.itemvariant.discount / 100);
  }

  getExpressCharge(){
    let expressCharge = 0;
    let {express_charge} = this.props.activeTransaction;

    if(express_charge){
      expressCharge = express_charge;
    }

    return expressCharge;
  }

  renderDetailPayment(){
    return(
      <View style={styles.detailPayment}>
        {this.subTotal()}
        {this.expressCharge()}
        {this.totalPrice()}
      </View>
    )
  }

  subTotal(){
    return(
      <View style={styles.orderDetail}>
          <View style={styles.detailLeft}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
              {""}
            </Text>
          </View>

          <View style={styles.detailPaymentCenter}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
              {'Subtotal'}
            </Text>
          </View>

          <View style={styles.detailPaymentRight}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
              {toCurrency(this.getSubtotalPrice().toString(), 'Rp ')}
            </Text>
          </View>
        </View>
    )
  }

  expressCharge(){

    return(
      <View style={styles.orderDetail}>
          <View style={styles.detailLeft}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
              {""}
            </Text>
          </View>

          <View style={styles.detailPaymentCenter}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
              {'Biaya Ekspres'}
            </Text>
          </View>

          <View style={styles.detailPaymentRight}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
              {toCurrency(this.getExpressCharge().toString(), 'Rp ')}
            </Text>
          </View>
        </View>
    )
  }

  totalPrice(){
    let {orderPrice} = this.props;
    return(
      <View style={[styles.orderDetail, {paddingVertical: Scaling.verticalScale(10)}]}>
        <View style={styles.detailLeft}>
          <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
            {""}
          </Text>
        </View>

        <View style={styles.detailPaymentCenter}>
          <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>
            {'TOTAL BAYAR'}
          </Text>
        </View>

        <View style={styles.detailPaymentRight}>
          <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{fontSize: Scaling.scale(20), color: CONSTANT.COLOR.BLACK}}>
            {toCurrency(this.getTotalPrice().toString(), 'Rp ')}
          </Text>
        </View>
      </View>
    )
  }

  renderTextInput(){
    return(
      <View style={{marginTop: Scaling.verticalScale(50)}}>

        <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>
          {'Jumlah Uang Konsumen'}
        </Text>

        <View style={styles.textinputContainer}>

          <View style={styles.leftIcon}>
            <Image
              style={{width: this.state.image.rupiah_icon.width, height: this.state.image.rupiah_icon.height}}
              source={this.state.image.rupiah_icon.url}
            />
          </View>

          <View style={styles.textinput}>
            <Form.InputField.TypeE
              value={this.state.phone_number}
              keyboardType={'numeric'}
              onChangeText={this._onInputCash.bind(this)}
              placeholder={'0'}
              placeholderTextColor={CONSTANT.COLOR.LIGHTER_GRAY_FONT}
              containerStyle={[
                {
                  width: Dimensions.get('window').width * 0.63,
                  height: Scaling.verticalScale(40),
                  justifyContent: 'center',
                  paddingTop: Scaling.verticalScale(7)
                }
              ]}
              inputContainerStyle={{
                justifyContent: 'center'
              }}
              inputTextStyle={{fontSize: Scaling.moderateScale(15), color: CONSTANT.COLOR.BLACK}}
            />
          </View>
        </View>
      </View>
    )
  }

  renderChangeMoney(){
    let {customerMoney, changeMoney} = this.state;
    if(changeMoney > 0){
      return(
        <View style={{alignItems: 'flex-end'}}>
          <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>
            {'Kembalian '} {toCurrency((changeMoney).toString(), 'Rp ')}
          </Text>
        </View>
      )
    }
    return null
  }

  renderButton(){
    let totalPrice = this.getTotalPrice();
    let isPayTransaction = (this.state.customerMoney >= totalPrice);

    return(
      <View style={{marginTop: Scaling.verticalScale(40), alignItems: 'center'}}>
        <SubmitButton
          {...this.props}
          item={this.props.activeTransaction}
          isPayTransaction={isPayTransaction}
        />
      </View>
    )
  }
}

function mapStateToProps(state, ownProps) {

  return {
    activeTransaction: state.rootReducer.posOldVersion.activeTransaction
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(POS_OLD_VERSION);

const styles = StyleSheet.create({

  paymentSection:{
    flex: 1,
    backgroundColor: CONSTANT.COLOR.WHITE,
    elevation:0
  },
  rowTitle:{
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: Scaling.verticalScale(10)
  },
  rowContent:{
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: Scaling.verticalScale(5),
    marginBottom: Scaling.verticalScale(10),
    paddingVertical: Scaling.verticalScale(5),
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: CONSTANT.COLOR.LIGHTER_GRAY_FONT
  },
  column:{
    marginRight: Scaling.moderateScale(15)  
  },
  contentLeft: {
    flex: 0.5
  },
  contentRight:{
    flex: 0.5,
    alignItems: 'flex-end'
  },
  orderDetail:{
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: Scaling.verticalScale(5)
  },
  detailLeft:{
    flex: 0.15,
    alignItems: 'center'
  },
  detailCenter:{
    flex: 0.65
  },
  detailRight:{
    flex: 0.2,
    alignItems: 'flex-end',
    paddingRight: Scaling.moderateScale(10)
  },
  detailPaymentCenter:{
    flex: 0.5,
    alignItems: 'flex-end'
  },
  detailPaymentRight:{
    flex: 0.35,
    alignItems: 'flex-end',
    paddingRight: Scaling.moderateScale(10)
  },
  discount:{
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: Scaling.verticalScale(5),
    paddingBottom: Scaling.verticalScale(10),
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: CONSTANT.COLOR.LIGHTER_GRAY_FONT
  },
  buttonTextStyle:{
      color: CONSTANT.COLOR.BLACK,
      fontSize: Scaling.moderateScale(13), 
      fontFamily: CONSTANT.TEXT_FAMILY.MAIN, 
      fontWeight: CONSTANT.TEXT_WEIGHT.REGULAR.weight
  },
  buttonStyle:{
    flexDirection: 'row',
      borderRadius: Scaling.moderateScale(10),
      height:CONSTANT.STYLE.BUTTON.THICKER_EXTRA_MEDIUM.HEIGHT,
      width:'100%',
      alignItems:'center',
    justifyContent:'center',
    borderColor: CONSTANT.COLOR.LIGHT_BLUE,
    borderWidth: 1
  },
  totalPayment:{
    paddingVertical: Scaling.verticalScale(10),
    backgroundColor: CONSTANT.COLOR.LIGHTEST_GRAY,
    borderRadius: Scaling.moderateScale(5)
  },
  textinputContainer: {
    marginTop: Scaling.moderateScale(10),
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: Scaling.moderateScale(5),
    paddingHorizontal: Scaling.moderateScale(10),
    paddingVertical: Scaling.moderateScale(7),
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: CONSTANT.COLOR.LIGHTER_GRAY_FONT,
    width: Dimensions.get('window').width * 0.9
  },
  message:{
    marginTop: Scaling.moderateScale(10)
  },
  leftIcon:{
    alignItems: 'center'
  },
  textinput:{
    paddingLeft: Scaling.moderateScale(10)
  },
  orderItem:{
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: CONSTANT.COLOR.LIGHTER_GRAY_FONT,
    paddingBottom: Scaling.verticalScale(10)
  },
  detailPayment:{
    paddingTop: Scaling.verticalScale(10)
  }
})

