import React, { Component } from 'react';
import {View, ScrollView,TouchableOpacity, Dimensions, Alert, StyleSheet, Linking, PermissionsAndroid} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import MapView from 'react-native-maps';
import Polyline from '@mapbox/polyline';
import FusedLocation from 'react-native-fused-location';
import Communications from 'react-native-communications';

export class DDelivery extends Component {
	constructor(props) {
        super(props);
        this.state = {
        	image:{
				detail_button:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../../image/searchresult/detail_button.png'),
				},
				report_accept_button:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../../image/searchresult/report_accept_button.png'),
				},
				report_reject_button:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../../image/searchresult/report_reject_button.png'),
				},
				report_delivered_button:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../../image/searchresult/report_delivered_button.png'),
				},
				report_checkout_button:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../../image/searchresult/report_checkout_button.png'),
				},
				report_finish_button:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../../image/searchresult/report_finish_button.png'),
				},
				konfirmasi:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../../image/order/konfirmasi.png'),
				},
				kilo:{
					width:0,
					height:0,
					multiplierWidth:0.25,
					multiplierHeight:0.25,
					url:require('./../../../../../image/order/kilo.png'),
				},
				pieces:{
					width:0,
					height:0,
					multiplierWidth:0.25,
					multiplierHeight:0.25,
					url:require('./../../../../../image/order/pieces.png'),
				},
				location:{
					width:0,
					height:0,
					multiplierWidth:0.4,
					multiplierHeight:0.4,
					url:require('./../../../../../image/order/orderdetail/location.png'),
				},
				peta:{
					width:0,
					height:0,
					multiplierWidth:0.26,
					multiplierHeight:0.26,
					url:require('./../../../../../image/order/orderdetail/peta.png'),
				},
				jadwal:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../../image/order/orderdetail/jadwal.png'),
				},
				customer:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../../image/order/orderdetail/customer.png'),
				},
				open_maps:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../../image/order/orderdetail/open_maps.png'),
				},
				sms_icon:{
					width:0,
					height:0,
					multiplierWidth:0.65,
					multiplierHeight:0.65,
					url:require('./../../../../../image/order/orderdetail/sms_icon.png'),
				},
				telephone_icon:{
					width:0,
					height:0,
					multiplierWidth:0.65,
					multiplierHeight:0.65,
					url:require('./../../../../../image/order/orderdetail/telephone_icon.png'),
				},
        	},
        	alert:{
        		failOpenForgetPassword:'Something went wrong, try again or contact us for more information',
        	},
        	Modal:{
                isProgressLoadingVisible:false,
                typeProgressModal:'standard',
                loadingWord:'mencoba masuk...'
            },
            string:{

            },
            isLoadMap:true,
            data:this.props.data ? this.props.data : [],
            testData:[],
            centerCoord:{latitude:-6, longitude:-10, latitudeDelta:0.1, longitudeDelta:0.1},
            location: [],
      isTracking: false

        };  
    }

	componentWillMount(){
		this._getImageSize();
		
		this._configLocation();
	}

	async _configLocation(){
		const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
                        title: 'Kliknklin butuh mengakses lokasi anda',
                        message: 'Aktifkan lokasi anda agar kami dapat memberikan arah perjalanan menuju lokasi',
                        }
                    );

		if (granted) {
			FusedLocation.setLocationPriority(FusedLocation.Constants.HIGH_ACCURACY);
        
	        // Get location once. 
	        const location = await FusedLocation.getFusedLocation(); 
			this.setState({location} , () => {
				this._getDirection();
			});
		}
		
	}

	componentWillUnmount(){
		
	}

	_translateDestination(){
		var destination = this.state.data.delivery_location;
		if (destination.split(' ').length == 2) {
			var check = destination.split(' ')[1];
			check = check.split(',');
			destination = {};
			destination.latitude = check[0].substring(1,check[0].length);
			destination.longitude = check[1].substring(0,check[1].length - 1);
		}
		else if (destination.split(',').length == 2) {
			destination = {};
			destination.latitude = destination.split(',')[0];
			destination.longitude = destination.split(',')[1];
		}
		return destination;
	}

	_getDirection(){
		this.setState({isLoadMap:true, isFailLoadMap:false}, () => {
			var checkLoadMap = setTimeout(() => {
				this.setState({isLoadMap:false, isFailLoadMap:true});
			}, 15000);
			return fetch('https://maps.googleapis.com/maps/api/directions/json?origin='+this.state.location.latitude+','+this.state.location.longitude + '&destination='+this._translateDestination().latitude+','+this._translateDestination().longitude+'&key=AIzaSyC7TdQ2gY21ils1brRLqVixAHC5gjhne5Q')
			.then(
				(response) => response.json()
			)
			.then(
				(data) => {
					clearTimeout(checkLoadMap);

					let points = data.routes[0].overview_polyline.points;
				    let steps = Polyline.decode(points);
				    let polylineCoords = [];

				    for (let i=0; i < steps.length; i++) {
				      let tempLocation = {
				        latitude : steps[i][0],
				        longitude : steps[i][1]
				      }
				      polylineCoords.push(tempLocation);
				    }

				    var northeastBounds = data.routes[0].bounds.northeast;
				    var southwestBounds = data.routes[0].bounds.southwest;
							
					var centerCoord = this.state.centerCoord;
				 	centerCoord.latitude = (northeastBounds.lat + southwestBounds.lat) / 2;
				 	centerCoord.longitude = (northeastBounds.lng + southwestBounds.lng) / 2;
					centerCoord.latitudeDelta = Math.abs(northeastBounds.lat - southwestBounds.lat) * 1.15;
					centerCoord.longitudeDelta = Math.abs(northeastBounds.lng - southwestBounds.lng) * 1.15;
					
				    this.setState({testData:polylineCoords,centerCoord:centerCoord, isLoadMap:false, isFailLoadMap:false});
				}
			)
			.catch((error) => {
				clearTimeout(checkLoadMap);

				this.setState({isLoadMap:false, isFailLoadMap:true});
			});
		});
	}

	_getImageSize(){
		var image = this.state.image;

		var image_detail_button = ResolveAssetSource(this.state.image.detail_button.url);

		image.detail_button.width = image_detail_button.width;
		image.detail_button.height = image_detail_button.height;

		var image_report_accept_button = ResolveAssetSource(this.state.image.report_accept_button.url);

		image.report_accept_button.width = image_report_accept_button.width;
		image.report_accept_button.height = image_report_accept_button.height;

		var image_report_reject_button = ResolveAssetSource(this.state.image.report_reject_button.url);

		image.report_reject_button.width = image_report_reject_button.width;
		image.report_reject_button.height = image_report_reject_button.height;

		var image_report_delivered_button = ResolveAssetSource(this.state.image.report_delivered_button.url);

		image.report_delivered_button.width = image_report_delivered_button.width;
		image.report_delivered_button.height = image_report_delivered_button.height;

		var image_report_checkout_button = ResolveAssetSource(this.state.image.report_checkout_button.url);

		image.report_checkout_button.width = image_report_checkout_button.width;
		image.report_checkout_button.height = image_report_checkout_button.height;

		var image_report_finish_button = ResolveAssetSource(this.state.image.report_finish_button.url);

		image.report_finish_button.width = image_report_finish_button.width;
		image.report_finish_button.height = image_report_finish_button.height;

		var image_konfirmasi = ResolveAssetSource(this.state.image.konfirmasi.url);

		image.konfirmasi.width = image_konfirmasi.width;
		image.konfirmasi.height = image_konfirmasi.height;

		var image_kilo = ResolveAssetSource(this.state.image.kilo.url);

		image.kilo.width = image_kilo.width;
		image.kilo.height = image_kilo.height;

		var image_pieces = ResolveAssetSource(this.state.image.pieces.url);

		image.pieces.width = image_pieces.width;
		image.pieces.height = image_pieces.height;

		var image_location = ResolveAssetSource(this.state.image.location.url);

		image.location.width = image_location.width;
		image.location.height = image_location.height;

		var image_peta = ResolveAssetSource(this.state.image.peta.url);

		image.peta.width = image_peta.width;
		image.peta.height = image_peta.height;

		var image_jadwal = ResolveAssetSource(this.state.image.jadwal.url);

		image.jadwal.width = image_jadwal.width;
		image.jadwal.height = image_jadwal.height;

		var image_customer = ResolveAssetSource(this.state.image.customer.url);

		image.customer.width = image_customer.width;
		image.customer.height = image_customer.height;

		var image_open_maps = ResolveAssetSource(this.state.image.open_maps.url);

		image.open_maps.width = image_open_maps.width;
		image.open_maps.height = image_open_maps.height;

		var image_sms_icon = ResolveAssetSource(this.state.image.sms_icon.url);

		image.sms_icon.width = image_sms_icon.width;
		image.sms_icon.height = image_sms_icon.height;

		var image_telephone_icon = ResolveAssetSource(this.state.image.telephone_icon.url);

		image.telephone_icon.width = image_telephone_icon.width;
		image.telephone_icon.height = image_telephone_icon.height;

      	this.setState({image});
	}

 	_setData(data){
 		this.setState({data});
 	}

 	_formatDate(date){
    	date = new Date(date.replace(/-/g,"/"));
	  	return DateFormat(date, 'fullDate');
	}

	_toCurrency(n, currency) {
    	return currency + " " + Number(n).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
	}

	_getMetodePembayaran(){
		if (this.state.data) {
			if (this.state.data.payment_flag == 1) {
				return 'Cash';
			}
			else if (this.state.data.payment_flag == 2) {
				return 'Bank Transfer';
			}
			else if (this.state.data.payment_flag == 3) {
				return 'Membership'
			}
		}
	}

	_renderHarga(section){
		if (section.payment_flag == 1 || section.payment_flag == 2) {
			return(
				<View style={{flexDirection:'row', marginBottom:6}}>
					<View style={{flex:0.275}}>
						<TextWrapper type={'base'} style={{color:'darkslategray'}}>
							Harga
						</TextWrapper>
					</View>
					<View style={{flex:0.05}}>
						<TextWrapper type={'base'} style={{color:'darkslategray'}}>
							:
						</TextWrapper>
					</View>
					<View style={{flex:0.675}}>
						<TextWrapper type={'base'} style={{color:'darkslategray'}}>
							{this._toCurrency(this.state.data.price, 'Rp')}
						</TextWrapper>
					</View>
				</View>
			);
		}
		return null;
	}

	_markOrderNotPaid(){
		if (!this.props._getIsBankTransferPaid(this.state.data)) {
			return(
				<View style={{alignItems:'center', marginTop:7.5}}>
					<View style={{width:Dimensions.get('window').width * 0.75, backgroundColor:'#FFD9D9', borderRadius:5, alignItems:'center'}}>
	            		<TextWrapper type={'base'} style={{color:'black'}}>
		            		{'Order ini belum dibayar (bank transfer)'}
		            	</TextWrapper>
		            	<TextWrapper type={'base'} style={{color:'black'}}>
		            		{'hubungi admin outlet atau customer'}
		            	</TextWrapper>
	            	</View>
	            </View>
			);
		}
	}

	render(){
		return(
			<View style={{flex:1, backgroundColor:'#f2f2f2'}}>
				<View style={{backgroundColor:'white'}}>
	 				<View style={{margin:15, marginBottom:0, marginTop:10}}>
						<View style={{paddingBottom:6, borderBottomWidth:1, borderColor:'lightgray', alignItems:'center'}}>
							<TextWrapper type={'subtitle'} style={{color:'darkslategray', fontWeight:'bold'}}>
										{'Delivery'}
							</TextWrapper>
						</View>
						
						<View style={{alignItems:'center', marginTop:2.5}}>
							<TextWrapper type={'base'} style={{color:'black'}}>
										{'Order No.'}
										<TextWrapper type={'subtitle'} style={{fontWeight:'bold'}}>
											{this.state.data.order_hash}
										</TextWrapper>
							</TextWrapper>
						</View>

						{this.props._markOrderLate(this.state.data.is_late)}

						{this.props._markOrderImportant(this.state.data.important)}

						{this._markOrderNotPaid()}
					</View>
				</View>

				<View style={{backgroundColor:'white', marginBottom:5}}>
					<View style={{margin:15}}>
						<View style={styles.container}>
							<MapView.Animated
							    region={{
							      latitude: this.state.centerCoord.latitude,
							      longitude: this.state.centerCoord.longitude,
							      latitudeDelta: this.state.centerCoord.latitudeDelta,
            					  longitudeDelta: this.state.centerCoord.longitudeDelta,
							    }}
							    style={styles.map}
							    onPress={()=>{
							    	
							    }}
							>
								{
									this.state.testData.length != 0 ? 

									<MapView.Marker
								       coordinate={this.state.testData[0]}
								       pinColor={'red'}
								       title={'lokasi anda'}
								    />

								    :

								    null
								}

								
								<MapView.Polyline
								   coordinates={this.state.testData}
								   strokeWidth={2}
								   strokeColor="blue"
							   	/>
								
								{
									this.state.testData.length != 0 ? 

									<MapView.Marker
								      coordinate={this.state.testData[this.state.testData.length - 1]}
								      pinColor="orange"
								      title={'tujuan anda'}
								    />

								    :

								    null
								}
							   	
									
							</MapView.Animated>

							<View style={{flex:1,alignSelf:'stretch',height:200}}>
								<View style={{flex:0.3, justifyContent:'center', alignItems:'center'}}>
									{this.state.isFailLoadMap ? 
										<TouchableOpacity 
											onPress={() => {
												this._getDirection();
											}}
											style={{backgroundColor:'dodgerblue', padding:5}}>
											<TextWrapper type={'content'} style={{color:'white'}}>Muat Ulang</TextWrapper>
										</TouchableOpacity>
										 : 
										null
									}

									
								</View>
								<View style={{flex:0.4, justifyContent:'center', alignItems:'center'}}>
									{this.state.isFailLoadMap ? <TextWrapper type={'content'} style={{fontWeight:'bold'}}>Gagal memuat peta!</TextWrapper> : null}

									{this.state.isLoadMap ? <TextWrapper type={'content'} style={{fontWeight:'bold'}}>Memuat...</TextWrapper> : null}
								</View>
								
								<View style={{flex:0.3,justifyContent:'flex-end', alignItems:'flex-end'}}>
									<TouchableOpacity onPress={()=> {
										Linking.canOpenURL('http://maps.google.com/maps?daddr='+this._translateDestination().latitude+','+this._translateDestination().longitude).then(supported => {
									        if (supported) {
									            Linking.openURL('http://maps.google.com/maps?daddr='+this._translateDestination().latitude+','+this._translateDestination().longitude);
									        } else {
									            console.log('Don\'t know how to go');
									        }
									    }).catch(err => console.error('An error occurred', err));
									}} 
									style={{margin:5}}>
										<ResponsiveImage
											source={this.state.image.open_maps.url}
											initWidth={this.state.image.open_maps.width * this.state.image.open_maps.multiplierWidth}
											initHeight={this.state.image.open_maps.height * this.state.image.open_maps.multiplierHeight}
										/>
									</TouchableOpacity>
								</View>
							</View>
						</View>
							
						<View style={{marginBottom:6, flexDirection:'row', }}>
							<View style={{flex:0.275}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									Alamat
								</TextWrapper>
							</View>
							<View style={{flex:0.05}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									:
								</TextWrapper>
							</View>
							<View style={{flex:0.675}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									{this.state.data.delivery_address}
								</TextWrapper>
							</View>
						</View>
						<View style={{flexDirection:'row',}}>
							<View style={{flex:0.275}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									Keterangan
								</TextWrapper>
							</View>
							<View style={{flex:0.05}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									:
								</TextWrapper>
							</View>
							<View style={{flex:0.675}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									{this.state.data.delivery_info}
								</TextWrapper>
							</View>
						</View>
					</View>
				</View>

				<View style={{backgroundColor:'white', marginBottom:5}}>
	 				<View style={{margin:15}}>
						<View style={{ marginBottom:6, flexDirection:'row', alignItems:'center'}}>
							<ResponsiveImage
								source={this.state.image.jadwal.url}
								initWidth={this.state.image.jadwal.width * this.state.image.jadwal.multiplierWidth}
								initHeight={this.state.image.jadwal.height * this.state.image.jadwal.multiplierHeight}
							/>

							<TextWrapper type={'content'} style={{marginLeft:15, color:'darkslategray', fontWeight:'bold'}}>
								{'Jadwal'}
							</TextWrapper>
						</View>
						<View style={{marginBottom:6, flexDirection:'row',}}>
							<View style={{flex:0.275}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									Tanggal
								</TextWrapper>
							</View>
							<View style={{flex:0.05}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									:
								</TextWrapper>
							</View>
							<View style={{flex:0.675}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									{this._formatDate(this.state.data.delivery_date)}
								</TextWrapper>
							</View>
						</View>
						<View style={{marginBottom:6, flexDirection:'row',}}>
							<View style={{flex:0.275}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									Waktu
								</TextWrapper>
							</View>
							<View style={{flex:0.05}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									:
								</TextWrapper>
							</View>
							<View style={{flex:0.675}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									{this.state.data.deliverytime.info + ' WIB'}
								</TextWrapper>
							</View>
						</View>
					</View>
				</View>

				<View style={{backgroundColor:'white'}}>
					<View style={{margin:15}}>
						<View style={{flexDirection:'row', alignItems:'center', marginBottom:6}}>
							<ResponsiveImage
								source={this.state.image.customer.url}
								initWidth={this.state.image.customer.width * this.state.image.customer.multiplierWidth}
								initHeight={this.state.image.customer.height * this.state.image.customer.multiplierHeight}
							/>

							<TextWrapper type={'content'} style={{marginLeft:15, color:'darkslategray', fontWeight:'bold'}}>
								{'Customer'}
							</TextWrapper>
						</View>
						<View style={{flexDirection:'row', marginBottom:6}}>
							<View style={{flex:0.275}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									Nama
								</TextWrapper>
							</View>
							<View style={{flex:0.05}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									:
								</TextWrapper>
							</View>
							<View style={{flex:0.675}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									{this.state.data.customer.first_name}
								</TextWrapper>
							</View>
						</View>


						<View style={{flexDirection:'row', marginBottom:6}}>
							<View style={{flex:0.275}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									Telepon
								</TextWrapper>
							</View>
							<View style={{flex:0.05}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									:
								</TextWrapper>
							</View>
							<View style={{flex:0.325}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									{this.state.data.customer.mobile_phone}
								</TextWrapper>
							</View>
							<View style={{flex:0.35, justifyContent:'flex-end', flexDirection:'row'}}>
									<TouchableOpacity style={{right:10}} onPress={() => Communications.phonecall(this.state.data.customer.mobile_phone, true)}>
							          <ResponsiveImage
					                    source={this.state.image.telephone_icon.url}
										initWidth={this.state.image.telephone_icon.width * this.state.image.telephone_icon.multiplierWidth}
										initHeight={this.state.image.telephone_icon.height * this.state.image.telephone_icon.multiplierHeight}
					                  />
							        </TouchableOpacity>

					        		<TouchableOpacity onPress={() => Communications.text(this.state.data.customer.mobile_phone)}>
							          <ResponsiveImage
					                    source={this.state.image.sms_icon.url}
										initWidth={this.state.image.sms_icon.width * this.state.image.sms_icon.multiplierWidth}
										initHeight={this.state.image.sms_icon.height * this.state.image.sms_icon.multiplierHeight}
					                  />
							        </TouchableOpacity>
				            </View>
						</View>

						<View style={{flexDirection:'row', marginBottom:6}}>
							<View style={{flex:0.275}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									Berat
								</TextWrapper>
							</View>
							<View style={{flex:0.05}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									:
								</TextWrapper>
							</View>
							<View style={{flex:0.675}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									{this.state.data.item_weight + ' Kg'}
								</TextWrapper>
							</View>
						</View>

						<View style={{flexDirection:'row', marginBottom:6}}>
							<View style={{flex:0.275}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									Pieces
								</TextWrapper>
							</View>
							<View style={{flex:0.05}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									:
								</TextWrapper>
							</View>
							<View style={{flex:0.675}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									{this.state.data.pieces + ' pcs'}
								</TextWrapper>
							</View>
						</View>

						{this._renderHarga(this.state.data)}

						<View style={{flexDirection:'row', marginBottom:6}}>
							<View style={{flex:0.275}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									Pembayaran
								</TextWrapper>
							</View>
							<View style={{flex:0.05}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									:
								</TextWrapper>
							</View>
							<View style={{flex:0.675}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									{this._getMetodePembayaran()}
								</TextWrapper>
							</View>
						</View>
					</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    height:204,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth:2,
    borderColor:'lightgray',
    marginBottom:10
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    height:200,
    flex:1
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
});