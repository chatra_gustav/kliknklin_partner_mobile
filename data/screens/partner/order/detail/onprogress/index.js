import React, { Component } from 'react';
import {View, ScrollView,TouchableOpacity, Dimensions, Alert, StyleSheet, Linking} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import MapView from 'react-native-maps';


export class DOnProgress extends Component {
	constructor(props) {
        super(props);
        this.state = {
        	image:{
				detail_button:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../../image/searchresult/detail_button.png'),
				},
				report_accept_button:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../../image/searchresult/report_accept_button.png'),
				},
				report_reject_button:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../../image/searchresult/report_reject_button.png'),
				},
				report_delivered_button:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../../image/searchresult/report_delivered_button.png'),
				},
				report_checkout_button:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../../image/searchresult/report_checkout_button.png'),
				},
				report_finish_button:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../../image/searchresult/report_finish_button.png'),
				},
				konfirmasi:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../../image/order/konfirmasi.png'),
				},
				kilo:{
					width:0,
					height:0,
					multiplierWidth:0.25,
					multiplierHeight:0.25,
					url:require('./../../../../../image/order/kilo.png'),
				},
				pieces:{
					width:0,
					height:0,
					multiplierWidth:0.25,
					multiplierHeight:0.25,
					url:require('./../../../../../image/order/pieces.png'),
				},
				location:{
					width:0,
					height:0,
					multiplierWidth:0.4,
					multiplierHeight:0.4,
					url:require('./../../../../../image/order/orderdetail/location.png'),
				},
				peta:{
					width:0,
					height:0,
					multiplierWidth:0.25,
					multiplierHeight:0.25,
					url:require('./../../../../../image/order/orderdetail/peta.png'),
				},
				jadwal:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../../image/order/orderdetail/jadwal.png'),
				},
				customer:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../../image/order/orderdetail/customer.png'),
				},
        	},
        	alert:{
        		failOpenForgetPassword:'Something went wrong, try again or contact us for more information',
        	},
        	Modal:{
                isProgressLoadingVisible:false,
                typeProgressModal:'standard',
                loadingWord:'mencoba masuk...'
            },
            string:{

            },
            data:this.props.data ? this.props.data : [],
        };  
    }

	componentWillMount(){
		this._getImageSize();
	}

	_getImageSize(){
		var image = this.state.image;

		var image_detail_button = ResolveAssetSource(this.state.image.detail_button.url);

		image.detail_button.width = image_detail_button.width;
		image.detail_button.height = image_detail_button.height;

		var image_report_accept_button = ResolveAssetSource(this.state.image.report_accept_button.url);

		image.report_accept_button.width = image_report_accept_button.width;
		image.report_accept_button.height = image_report_accept_button.height;

		var image_report_reject_button = ResolveAssetSource(this.state.image.report_reject_button.url);

		image.report_reject_button.width = image_report_reject_button.width;
		image.report_reject_button.height = image_report_reject_button.height;

		var image_report_delivered_button = ResolveAssetSource(this.state.image.report_delivered_button.url);

		image.report_delivered_button.width = image_report_delivered_button.width;
		image.report_delivered_button.height = image_report_delivered_button.height;

		var image_report_checkout_button = ResolveAssetSource(this.state.image.report_checkout_button.url);

		image.report_checkout_button.width = image_report_checkout_button.width;
		image.report_checkout_button.height = image_report_checkout_button.height;

		var image_report_finish_button = ResolveAssetSource(this.state.image.report_finish_button.url);

		image.report_finish_button.width = image_report_finish_button.width;
		image.report_finish_button.height = image_report_finish_button.height;

		var image_konfirmasi = ResolveAssetSource(this.state.image.konfirmasi.url);

		image.konfirmasi.width = image_konfirmasi.width;
		image.konfirmasi.height = image_konfirmasi.height;

		var image_kilo = ResolveAssetSource(this.state.image.kilo.url);

		image.kilo.width = image_kilo.width;
		image.kilo.height = image_kilo.height;

		var image_pieces = ResolveAssetSource(this.state.image.pieces.url);

		image.pieces.width = image_pieces.width;
		image.pieces.height = image_pieces.height;

		var image_location = ResolveAssetSource(this.state.image.location.url);

		image.location.width = image_location.width;
		image.location.height = image_location.height;

		var image_peta = ResolveAssetSource(this.state.image.peta.url);

		image.peta.width = image_peta.width;
		image.peta.height = image_peta.height;

		var image_jadwal = ResolveAssetSource(this.state.image.jadwal.url);

		image.jadwal.width = image_jadwal.width;
		image.jadwal.height = image_jadwal.height;

		var image_customer = ResolveAssetSource(this.state.image.customer.url);

		image.customer.width = image_customer.width;
		image.customer.height = image_customer.height;

      	this.setState({image});
	}

 	_setData(data){
 		this.setState({data});
 	}

 	_formatDate(date){
    	date = new Date(date.replace(/-/g,"/"));
	  	return DateFormat(date, 'fullDate');
	}

	_formatTime(date){
		date = new Date(date.replace(/-/g,"/"));
	  	return DateFormat(date, 'simpleTime');
	}

	render(){
		return(
			<View style={{flex:1, backgroundColor:'#f2f2f2'}}>
				<View style={{backgroundColor:'white'}}>
	 				<View style={{margin:15, marginBottom:0, marginTop:10}}>
						<View style={{paddingBottom:6, borderBottomWidth:1, borderColor:'lightgray', alignItems:'center'}}>
							<TextWrapper type={'subtitle'} style={{color:'darkslategray', fontWeight:'bold'}}>
										{'On Progress'}
							</TextWrapper>
						</View>
						<View style={{alignItems:'center', marginTop:2.5}}>
							<TextWrapper type={'base'} style={{color:'black'}}>
										{'Order No.'}
										<TextWrapper type={'subtitle'} style={{fontWeight:'bold'}}>
											{this.state.data.order_hash}
										</TextWrapper>
							</TextWrapper>
						</View>

						{this.props._markOrderLate(this.state.data.is_late)}

						{this.props._markOrderImportant(this.state.data.important)}
					</View>
				</View>

				<View style={{backgroundColor:'white', marginBottom:5}}>
	 				<View style={{margin:15}}>
						<View style={{ marginBottom:6, flexDirection:'row', alignItems:'center'}}>
							<ResponsiveImage
								source={this.state.image.jadwal.url}
								initWidth={this.state.image.jadwal.width * this.state.image.jadwal.multiplierWidth}
								initHeight={this.state.image.jadwal.height * this.state.image.jadwal.multiplierHeight}
							/>

							<TextWrapper type={'content'} style={{marginLeft:15, color:'darkslategray', fontWeight:'bold'}}>
								{'Jadwal'}
							</TextWrapper>
						</View>
						<View style={{marginBottom:6, flexDirection:'row',}}>
							<View style={{flex:0.225}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									Batas Lapor
								</TextWrapper>
							</View>
							<View style={{flex:0.05}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									:
								</TextWrapper>
							</View>
							<View style={{flex:0.725}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									{this._formatDate(this.state.data.report_time_limit.date) + ', ' +  this._formatTime(this.state.data.report_time_limit.date) + ' WIB'}
								</TextWrapper>
							</View>
						</View>
					</View>
				</View>

				<View style={{backgroundColor:'white', marginBottom:5}}>
					<View style={{margin:15}}>
						<View style={{flexDirection:'row', alignItems:'center', marginBottom:6}}>
							<ResponsiveImage
								source={this.state.image.customer.url}
								initWidth={this.state.image.customer.width * this.state.image.customer.multiplierWidth}
								initHeight={this.state.image.customer.height * this.state.image.customer.multiplierHeight}
							/>

							<TextWrapper type={'content'} style={{marginLeft:15, color:'darkslategray', fontWeight:'bold'}}>
								{'Customer'}
							</TextWrapper>
						</View>
						<View style={{flexDirection:'row', marginBottom:6}}>
							<View style={{flex:0.225}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									Nama
								</TextWrapper>
							</View>
							<View style={{flex:0.05}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									:
								</TextWrapper>
							</View>
							<View style={{flex:0.725}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									{this.state.data.customer.first_name}
								</TextWrapper>
							</View>
						</View>

						<View style={{flexDirection:'row', marginBottom:6}}>
							<View style={{flex:0.225}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									Berat
								</TextWrapper>
							</View>
							<View style={{flex:0.05}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									:
								</TextWrapper>
							</View>
							<View style={{flex:0.725}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									{this.state.data.item_weight + ' Kg'}
								</TextWrapper>
							</View>
						</View>

						<View style={{flexDirection:'row', marginBottom:6}}>
							<View style={{flex:0.225}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									Pieces
								</TextWrapper>
							</View>
							<View style={{flex:0.05}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									:
								</TextWrapper>
							</View>
							<View style={{flex:0.725}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>
									{this.state.data.pieces + ' Pcs'}
								</TextWrapper>
							</View>
						</View>
					</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
    margin:10,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
});