import React, { Component } from 'react';
import {View, ScrollView,TouchableOpacity, Dimensions, Alert, StyleSheet, Linking, Modal, AsyncStorage} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import MapView from 'react-native-maps';

export class OrderDetail extends Component {
	constructor(props) {
        super(props);
        this.state = {
        	image:{
				detail_button:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../image/searchresult/detail_button.png'),
				},
				cancel_button:{
					width:0,
					height:0,
					multiplierWidth:1,
					multiplierHeight:1,
					url:require('./../../../../image/order/cancel_button.png'),
				},
				report_accept_button:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../image/searchresult/report_accept_button.png'),
				},
				report_reject_button:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../image/searchresult/report_reject_button.png'),
				},
				report_delivered_button:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../image/searchresult/report_delivered_button.png'),
				},
				report_checkout_button:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../image/searchresult/report_checkout_button.png'),
				},
				report_finish_button:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../image/searchresult/report_finish_button.png'),
				},
				konfirmasi:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../image/order/konfirmasi.png'),
				},
				kilo:{
					width:0,
					height:0,
					multiplierWidth:0.25,
					multiplierHeight:0.25,
					url:require('./../../../../image/order/kilo.png'),
				},
				pieces:{
					width:0,
					height:0,
					multiplierWidth:0.25,
					multiplierHeight:0.25,
					url:require('./../../../../image/order/pieces.png'),
				},
				location:{
					width:0,
					height:0,
					multiplierWidth:0.4,
					multiplierHeight:0.4,
					url:require('./../../../../image/order/orderdetail/location.png'),
				},
				peta:{
					width:0,
					height:0,
					multiplierWidth:0.25,
					multiplierHeight:0.25,
					url:require('./../../../../image/order/orderdetail/peta.png'),
				},
				jadwal:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../image/order/orderdetail/jadwal.png'),
				},
				customer:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../image/order/orderdetail/customer.png'),
				},
				report_detail_delivered:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../image/order/orderdetail/report_detail_delivered.png'),
				},
				report_detail_pickup:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../image/order/orderdetail/report_detail_pickup.png'),
				},
				report_detail_finish:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../image/order/orderdetail/report_detail_finish.png'),
				},
				kilo:{
					width:0,
					height:0,
					multiplierWidth:0.25,
					multiplierHeight:0.25,
					url:require('./../../../../image/order/kilo.png'),
				},
				pieces:{
					width:0,
					height:0,
					multiplierWidth:0.25,
					multiplierHeight:0.25,
					url:require('./../../../../image/order/pieces.png'),
				},
				konfirmasi:{
					width:0,
					height:0,
					multiplierWidth:0.65,
					multiplierHeight:0.65,
					url:require('./../../../../image/order/konfirmasi.png'),
				},
        	},
        	alert:{
        		failOpenForgetPassword:'Something went wrong, try again or contact us for more information',
        	},
        	Modal:{
                isProgressLoadingVisible:false,
                typeProgressModal:'standard',
                loadingWord:'mencoba masuk...'
            },
            data:this.props.data ? this.props.data : [],
            contentType:this.props.contentType ? this.props.contentType : 'delivery',
        };  
    }

	componentWillMount(){
		this._getImageSize();
		this._getRoleUser();
    }

    async _getRoleUser(){
    	var role = await AsyncStorage.getItem('partnerkliknklin_role');

		this.setState({role});
    }
	componentDidMount(){
	}

	_getImageSize(){
		var image = this.state.image;

		var image_detail_button = ResolveAssetSource(this.state.image.detail_button.url);

		image.detail_button.width = image_detail_button.width;
		image.detail_button.height = image_detail_button.height;

		var image_cancel_button = ResolveAssetSource(this.state.image.cancel_button.url);

		image.cancel_button.width = image_cancel_button.width;
		image.cancel_button.height = image_cancel_button.height;

		var image_report_accept_button = ResolveAssetSource(this.state.image.report_accept_button.url);

		image.report_accept_button.width = image_report_accept_button.width;
		image.report_accept_button.height = image_report_accept_button.height;

		var image_report_reject_button = ResolveAssetSource(this.state.image.report_reject_button.url);

		image.report_reject_button.width = image_report_reject_button.width;
		image.report_reject_button.height = image_report_reject_button.height;

		var image_report_delivered_button = ResolveAssetSource(this.state.image.report_delivered_button.url);

		image.report_delivered_button.width = image_report_delivered_button.width;
		image.report_delivered_button.height = image_report_delivered_button.height;

		var image_report_checkout_button = ResolveAssetSource(this.state.image.report_checkout_button.url);

		image.report_checkout_button.width = image_report_checkout_button.width;
		image.report_checkout_button.height = image_report_checkout_button.height;

		var image_report_finish_button = ResolveAssetSource(this.state.image.report_finish_button.url);

		image.report_finish_button.width = image_report_finish_button.width;
		image.report_finish_button.height = image_report_finish_button.height;

		var image_konfirmasi = ResolveAssetSource(this.state.image.konfirmasi.url);

		image.konfirmasi.width = image_konfirmasi.width;
		image.konfirmasi.height = image_konfirmasi.height;

		var image_kilo = ResolveAssetSource(this.state.image.kilo.url);

		image.kilo.width = image_kilo.width;
		image.kilo.height = image_kilo.height;

		var image_pieces = ResolveAssetSource(this.state.image.pieces.url);

		image.pieces.width = image_pieces.width;
		image.pieces.height = image_pieces.height;

		var image_location = ResolveAssetSource(this.state.image.location.url);

		image.location.width = image_location.width;
		image.location.height = image_location.height;

		var image_peta = ResolveAssetSource(this.state.image.peta.url);

		image.peta.width = image_peta.width;
		image.peta.height = image_peta.height;

		var image_jadwal = ResolveAssetSource(this.state.image.jadwal.url);

		image.jadwal.width = image_jadwal.width;
		image.jadwal.height = image_jadwal.height;

		var image_customer = ResolveAssetSource(this.state.image.customer.url);

		image.customer.width = image_customer.width;
		image.customer.height = image_customer.height;

		var image_report_detail_delivered = ResolveAssetSource(this.state.image.report_detail_delivered.url);

		image.report_detail_delivered.width = image_report_detail_delivered.width;
		image.report_detail_delivered.height = image_report_detail_delivered.height;

		var image_report_detail_pickup= ResolveAssetSource(this.state.image.report_detail_pickup.url);

		image.report_detail_pickup.width = image_report_detail_pickup.width;
		image.report_detail_pickup.height = image_report_detail_pickup.height;

		var image_report_detail_finish= ResolveAssetSource(this.state.image.report_detail_finish.url);

		image.report_detail_finish.width = image_report_detail_finish.width;
		image.report_detail_finish.height = image_report_detail_finish.height;

		var image_kilo = ResolveAssetSource(this.state.image.kilo.url);

		image.kilo.width = image_kilo.width;
		image.kilo.height = image_kilo.height;

		var image_pieces = ResolveAssetSource(this.state.image.pieces.url);

		image.pieces.width = image_pieces.width;
		image.pieces.height = image_pieces.height;

		var image_konfirmasi = ResolveAssetSource(this.state.image.konfirmasi.url);

		image.konfirmasi.width = image_konfirmasi.width;
		image.konfirmasi.height = image_konfirmasi.height;

      	this.setState({image});
	}

 	_setData(data){
 		this.setState({data});
 	}

 	_markOrderLate(isLate){
		if (isLate) {
			return(
				<View style={{alignItems:'center', marginTop:7.5}}>
					<View style={{width:65, backgroundColor:'#ed1c24', borderRadius:5, alignItems:'center'}}>
	            		<TextWrapper type={'base'} style={{fontWeight:'bold', color:'white'}}>
		            		{'Telat'}
		            	</TextWrapper>
	            	</View>
	            </View>
			);
		}
 	}

 	_markOrderImportant(isImportant){
		if (isImportant) {
			return(
				<View style={{alignItems:'center', marginTop:7.5}}>
					<View style={{width:65, backgroundColor:'orange', borderRadius:5, alignItems:'center'}}>
	            		<TextWrapper type={'base'} style={{fontWeight:'bold', color:'white'}}>
		            		{'Penting'}
		            	</TextWrapper>
	            	</View>
	            </View>
			);
		}
 	}

 	_getContent(){
 		if (this.state.contentType == 'pickup') {
 			return(
	 			<OrderDetailContent.Pickup
					data={this.props.data}
					_markOrderLate={this._markOrderLate.bind(this)}
					_markOrderImportant={this._markOrderImportant.bind(this)}
				/>
			);
 		}
 		else if (this.state.contentType == 'onprogress') {
 			return (
				<OrderDetailContent.OnProgress
					data={this.props.data}
					_markOrderLate={this._markOrderLate.bind(this)}
					_markOrderImportant={this._markOrderImportant.bind(this)}
				/>
 			);
 		}
 		else if (this.state.contentType == 'delivery') {
 			return (
				<OrderDetailContent.Delivery
					data={this.props.data}
					_markOrderLate={this._markOrderLate.bind(this)}
					_markOrderImportant={this._markOrderImportant.bind(this)}
					_getIsBankTransferPaid={this.props._getIsBankTransferPaid}
				/>
 			);
 		}
 	}

 	_getButtonContent(){
 		if (this.state.contentType == 'pickup') {
 			return(
	 			<ButtonPickup
					image={this.state.image}
					data={this.props.data}
					navigator={this.props.navigator}
					_afterSubmit={this.props._afterSubmit}
					_showModal={this.props._showModal}
					_setShouldReloadSummary={this.props._setShouldReloadSummary}
					_getDataLoginState={this.props._getDataLoginState}
					_fetch={this.props._fetch}
					_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
				/>
			);
 		}
 		else if (this.state.contentType == 'onprogress') {
 			return (
				<ButtonOnProgress
					image={this.state.image}
					data={this.props.data}
					navigator={this.props.navigator}
					_afterSubmit={this.props._afterSubmit}
					_showModal={this.props._showModal}
					_setShouldReloadSummary={this.props._setShouldReloadSummary}
					_getDataLoginState={this.props._getDataLoginState}
					_fetch={this.props._fetch}
					_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
				/>
 			);
 		}
 		else if (this.state.contentType == 'delivery') {
 			return (
				<ButtonDelivery
					image={this.state.image}
					data={this.props.data}
					navigator={this.props.navigator}
					_afterSubmit={this.props._afterSubmit}
					_showModal={this.props._showModal}
					_setShouldReloadSummary={this.props._setShouldReloadSummary}
					_getDataLoginState={this.props._getDataLoginState}
					_fetch={this.props._fetch}
					_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
				/>
 			);
 		}
 	}

 	_renderBottomContent(){
 		if (this.state.role){
			if (this.state.role == 'staff') {
				if (this.props.data.payment_flag == 2 && this.props.data.banktransfer && this.props.data.banktransfer.status_transfer == 0) {
					return null;
				}
				return (
					<View style={{flex:0.1, justifyContent:'flex-end', backgroundColor:'#F2F2F2'}}>
						{this._getButtonContent()}
					</View>
				)
			}
		}
 	}

	render(){
		return(
			<View style={{flex:1, justifyContent:'flex-end'}}>
				<ScrollView style={{flex:1, backgroundColor:'#F2F2F2'}}>
					{this._getContent()}
				</ScrollView>
				

				{this._renderBottomContent()}
			</View>
		);
	}
}

class ButtonPickup extends Component{
	constructor(props) {
        super(props);
        this.state = {
        	image:this.props.image ? this.props.image : null,
            data:[],
        };  
    }

    componentWillMount(){
    	this._configData();
    }

    _configData(){
    	var data = JSON.parse(JSON.stringify(this.props._getDataLoginState()));
    	data.order_id = this.props.data.id;
    	this.setState({data});
    }

    _onPressOrderPickup(){
		this.confirm_order_pickup._show(true);
    }

    _afterPickup(){
    	if (this.props._afterSubmit) {
    		this.props._afterSubmit(() => {
    			this.props._setShouldReloadSummary(true);
    			this.props.navigator.pop();
    		});
    	}
    }

	render(){
		return(
			<TouchableOpacity onPress={this._onPressOrderPickup.bind(this)}>
				<ResponsiveImage
					source={this.state.image.report_detail_pickup.url}
					initWidth={this.state.image.report_detail_pickup.width * this.state.image.report_detail_pickup.multiplierWidth}
					initHeight={this.state.image.report_detail_pickup.height * this.state.image.report_detail_pickup.multiplierHeight}
				>
					<ConfirmOrder.Pickup
						ref={ref=>this.confirm_order_pickup = ref}
						data={this.state.data}
						_callback={this._afterPickup.bind(this)}
						_showModal={this.props._showModal}
						_fetch={this.props._fetch}
						_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
					/>
				</ResponsiveImage>
			</TouchableOpacity>
		);
	}
}

class ButtonOnProgress extends Component{
	constructor(props) {
        super(props);
        this.state = {
        	image:this.props.image ? this.props.image : null,
            data:[],
        };  
    }

    componentWillMount(){
    	this._configData();
    }

    _configData(){
    	var data = JSON.parse(JSON.stringify(this.props._getDataLoginState()));
    	data.order_id = this.props.data.id;
    	this.setState({data});
    }

    _onPressOrderOnProgress(){
		this.confirm_order_onprogress._show(true);
    }

    _afterOnProgress(){
    	if (this.props._afterSubmit) {
    		this.props._afterSubmit(() => {
    			this.props._setShouldReloadSummary(true);
    			this.props.navigator.pop();
    		});
    	}
    }

	render(){
		if (this.props.data) {
			if (this.props.data.order_status_id == 3) {
				return(
					<TouchableOpacity onPress={this._onPressOrderOnProgress.bind(this)}>
						<ResponsiveImage
							source={this.state.image.report_detail_finish.url}
							initWidth={this.state.image.report_detail_finish.width * this.state.image.report_detail_finish.multiplierWidth}
							initHeight={this.state.image.report_detail_finish.height * this.state.image.report_detail_finish.multiplierHeight}
						>
							<ConfirmOrder.OnProgress
								ref={ref=>this.confirm_order_onprogress = ref}
								data={this.state.data}
								_callback={this._afterOnProgress.bind(this)}
								_showModal={this.props._showModal}
								_fetch={this.props._fetch}
								_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
							/>
						</ResponsiveImage>
					</TouchableOpacity>
				);
			}
		}
		return null;
	}
}

class ButtonDelivery extends Component{
	constructor(props) {
        super(props);
        this.state = {
        	image:this.props.image ? this.props.image : null,
            data:[],
        };  
    }

    componentWillMount(){
    	this._configData();
    }

    _configData(){
    	var data = JSON.parse(JSON.stringify(this.props._getDataLoginState()));
    	data.order_id = this.props.data.id;
    	this.setState({data});
    }

    _onPressOrderDelivery(){
		this.confirm_order_delivery._show(true);
    }

    _afterDelivery(){
    	if (this.props._afterSubmit) {
    		this.props._afterSubmit(() => {
    			this.props._setShouldReloadSummary(true);
    			this.props.navigator.pop();
    		});
    	}
    }

	render(){
		return(
			<TouchableOpacity onPress={this._onPressOrderDelivery.bind(this)}>
				<ResponsiveImage
					source={this.state.image.report_detail_delivered.url}
					initWidth={this.state.image.report_detail_delivered.width * this.state.image.report_detail_delivered.multiplierWidth}
					initHeight={this.state.image.report_detail_delivered.height * this.state.image.report_detail_delivered.multiplierHeight}
				/>

					<ConfirmOrder.Delivery
						ref={ref=>this.confirm_order_delivery = ref}
						data={this.state.data}
						_callback={this._afterDelivery.bind(this)}
						_showModal={this.props._showModal}
						_fetch={this.props._fetch}
						_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
					/>
			</TouchableOpacity>
		);
	}
}