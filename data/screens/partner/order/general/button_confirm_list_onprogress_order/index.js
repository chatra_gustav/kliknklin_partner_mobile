import React, { Component } from 'react';
import {TouchableOpacity, AsyncStorage} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';

export default class ButtonConfirmListOnProgress extends Component{
	constructor(props) {
        super(props);
        this.state = {
        	data:this.props.data ? this.props.data : [],
        	_setData:this.props._setData ? this.props._setData : (()=>{}),
        	image:{
				report_finish_button:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../../image/searchresult/report_finish_button.png'),
				},
        	},
        }
    }

    componentWillMount(){
    	this._getImageSize();
    	this._getRoleUser();
    }

    async _getRoleUser(){
    	var role = await AsyncStorage.getItem('partnerkliknklin_role');

		this.setState({role});
    }
    _getImageSize(){
		var image = this.state.image;

		var image_report_finish_button = ResolveAssetSource(this.state.image.report_finish_button.url);

		image.report_finish_button.width = image_report_finish_button.width;
		image.report_finish_button.height = image_report_finish_button.height;

      	this.setState({image});
	}

    _onPressConfirmFinish(id){
    	this.confirm_order_onprogress._show(true);
    }

	render(){
		if (this.state.role){
			if (this.state.role == 'staff') {
				return(
					<TouchableOpacity onPress={this._onPressConfirmFinish.bind(this)}>
						<ResponsiveImage
							source={this.state.image.report_finish_button.url}
							initWidth={this.state.image.report_finish_button.width * this.state.image.report_finish_button.multiplierWidth}
							initHeight={this.state.image.report_finish_button.height * this.state.image.report_finish_button.multiplierHeight}
						>
							<ConfirmOrder.OnProgress
								ref={ref=>this.confirm_order_onprogress = ref}
								data={this.props.data}
								_setData={this.state._setData}
								_showModal={this.props._showModal}
								_callback={this.props._callback}
								_fetch={this.props._fetch}
								_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
							/>
						</ResponsiveImage>
					</TouchableOpacity>
				);
			}
		}
		return null;
	}
}
