import React, { Component } from 'react';
import {Alert, Modal, View, Dimensions, TouchableOpacity} from 'react-native';

import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ModalDropdown from 'react-native-modal-dropdown';

export default class ConfirmIncomingOrder extends Component {
	constructor(props) {
        super(props);
        this.state = {
        	image:{
				cancel_button:{
					width:0,
					height:0,
					multiplierWidth:1.1,
					multiplierHeight:1.1,
					url:require('./../../../../../image/order/cancel_button.png'),
				},
				konfirmasi:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../../image/order/submit.png'),
				},
				kilo:{
					width:0,
					height:0,
					multiplierWidth:0.25,
					multiplierHeight:0.25,
					url:require('./../../../../../image/order/kilo.png'),
				},
				pieces:{
					width:0,
					height:0,
					multiplierWidth:0.25,
					multiplierHeight:0.25,
					url:require('./../../../../../image/order/pieces.png'),
				},
        	},
        	modal:{
                isVisible:false,
                typeProgressModal:'standard',
                loadingWord:'mencoba masuk...'
            },
            courier:null,
            string:{
				alertTitle:'Konfirmasi',
				alertContent:'Apakah anda yakin untuk menerima order ini ?',
				alertReject:'Tidak',
				alertAccept:'Ya',
            },
            data:this.props.data ? this.props.data : [],
            _setData:this.props._setData ? this.props._setData : (()=>{}),
        };  
    }

	componentWillMount(){
		this._getImageSize();
	}

	_getImageSize(){
		var image = this.state.image;

		var image_cancel_button = ResolveAssetSource(this.state.image.cancel_button.url);

		image.cancel_button.width = image_cancel_button.width;
		image.cancel_button.height = image_cancel_button.height;

		var image_konfirmasi = ResolveAssetSource(this.state.image.konfirmasi.url);

		image.konfirmasi.width = image_konfirmasi.width;
		image.konfirmasi.height = image_konfirmasi.height;

		var image_kilo = ResolveAssetSource(this.state.image.kilo.url);

		image.kilo.width = image_kilo.width;
		image.kilo.height = image_kilo.height;

		var image_pieces = ResolveAssetSource(this.state.image.pieces.url);

		image.pieces.width = image_pieces.width;
		image.pieces.height = image_pieces.height;

      	this.setState({image});
	}

	_show(isVisible){
		var modal = this.state.modal;
		modal.isVisible = isVisible;

		var data = this.state.data;

		if (!isVisible) {
			data.item_weight = '';
			data.pieces = '';
		}

		this.setState({modal,data});
	}

	_onPressSubmitIncomingOrder(){
		if (!this._validateIsEmpty()) {
			Alert.alert(
	            this.state.string.alertTitle,
	            this.state.string.alertContent,
	            [
	                {text:this.state.string.alertReject, onPress: () =>{

	                }},
	                {text:this.state.string.alertAccept, onPress: () =>{
	                	var data = JSON.parse(JSON.stringify(this.state.data));
	                	data.courier_id = this.state.courier;
	                	data.order_action = 'accept';
	                	this._show(false);

	                	setTimeout(() => {
							this.props._showModal(true, 'menerima order...', () => {
								this._acceptIncoming(data);
		                 	})
	                	}, 250);
	                }},
	            ]
	        );
        }
        else{
        	alert('mohon pilih kurir terlebih dahulu');
        }
	}

	_acceptIncoming(data){
		this.props._fetch(10000, Order._submitIncomingOrder(data))
            .then((response) => {
            	console.log(response);
                if (response.success == 'yeah') {
                	if (response.data.status == 'Taken') {
                		if (this.props._callback) this.props._callback(()=>{}, false);
                	}
                	else{
                		if (this.props._callback) this.props._callback(()=>{}, true);
                	}
				}
				else{
					throw 'nope error';
				}
            })
            .catch((error) => {
            	console.log(error);
                this.props._showApiErrorRetryCancel(this._acceptIncoming.bind(this, data));
            })
	}

	_validateIsEmpty(){
		var courier = this.state.courier;
		return (courier == null);
	}

	_setData(data, callback = () => {}){
		this.setState({data}, callback());
	}

	render(){
		return(
			<Modal
	          animationType={'fade'}
	          transparent={true}
	          visible={this.state.modal.isVisible}
	          onRequestClose={() => {}}
	          >
	          	<View style={{flex:1, alignItems:'center', justifyContent:'center', backgroundColor:'rgba(0,0,0,0.6)'}}>
		          	<View style={{width:Dimensions.get('window').width * 0.7, height:Dimensions.get('window').height * 0.3 ,backgroundColor:'transparent'}}>
		          		<View style={{flex:0.25, justifyContent:'center', backgroundColor:'#f7931e', alignItems:'center'}}>
		          			<View style={{flex:0.1, alignItems:'flex-end', alignSelf:'stretch'}}>
			          			<TouchableOpacity onPress={() => this._show(false)} style={{margin:5, width:this.state.image.cancel_button.width * this.state.image.cancel_button.multiplierWidth, height:this.state.image.cancel_button.height * this.state.image.cancel_button.multiplierHeight}}>
			          				<ResponsiveImage
										source={this.state.image.cancel_button.url}
										initWidth={this.state.image.cancel_button.width * this.state.image.cancel_button.multiplierWidth}
										initHeight={this.state.image.cancel_button.height * this.state.image.cancel_button.multiplierHeight}
									/>
			          			</TouchableOpacity>
		          			</View>
		          			<View style={{flex:0.9, justifyContent:'center', bottom:Dimensions.get('window').height * 0.5 * 0.1 * 0.33 / 2}}>
		          				<TextWrapper type={'subtitle'} style={{color:'white', fontWeight:'bold'}}>
		          					Pilih Kurir
		          				</TextWrapper>
		          			</View>
		          			
		          		</View>

						<View
							style={{flex:0.45,backgroundColor:'white', justifyContent:'center', alignItems:'center'}}
						>
							<TouchableOpacity 
							onPress={() => {
								//if (this.props._getDataOutlet().listName.length > 1) {
									this.dropdown_type_search.show();
								//} 
							}}
							style={{width:Dimensions.get('window').width * 0.55, borderWidth:1,borderRadius:3, borderColor:'lightgray', alignItems:'center', justifyContent:'center', flexDirection:'row'}}>

				          		<ModalDropdown ref={ref => this.dropdown_type_search = ref}
				                    style={{marginLeft:Dimensions.get('window').width * 0.01, flex:1, alignItems:'center',justifyContent:'center', left:6.25, height:Dimensions.get('window').height * 0.07}}
				                    textStyle={{color:'darkslategray', fontSize:FontScale._getFontSize(8), fontFamily:'Montserrat',}}
				                    dropdownStyle={{width:Dimensions.get('window').width * 0.55, height:Dimensions.get('window').height * 0.5, borderWidth:1, borderColor:'lightgray'}}
				                    options={this.props._getDataCourier().listName}
				                    renderSeparator={(sectionID, rowID, adjacentRowHighlighted) => {
				                    	if (rowID != this.props._getDataCourier().listName.length - 1) {
				                    		return (
												<View key={rowID} style={{borderBottomWidth:1, borderColor:'lightgray'}}/>
				                    		);
				                    	}
				                    	return null;
				                    	
				                	}}
				                    renderRow={(rowData, rowID, highlighted) => {
				                    	return (
											<View style={{flex:1, height:45, justifyContent:'center', alignItems:'center'}}>
												<TextWrapper type={'base'} style={{color:'darkslategray'}}>
													{rowData}
												</TextWrapper>
											</View>
				                    	);
				                    }}
				                    defaultValue={'Pilih kurir untuk order ini..'}
				                    disabled={true}
				                    adjustFrame={(style) => {
										style.height = 49 * this.props._getDataCourier().listName.lengthr;
				                    	return style;
				                    }}
				                    //onDropdownWillShow={this._dropdown_5_willShow.bind(this)}
				                    //onDropdownWillHide={this._dropdown_5_willHide.bind(this)}
				                    onSelect={(index, value) => {
				                    	this.setState({courier:this.props._getDataCourier().listID[index]});
				                    }}
				            	/>

			            	<View style={{width:25, alignItems:'flex-end', justifyContent:'center', top:2, right:5}}>
			            		<View
									style={{
										width: 0,
									    height: 0,
									    backgroundColor: 'transparent',
									    borderStyle: 'solid',
									    borderLeftWidth: 7,
									    borderRightWidth: 7,
									    borderBottomWidth: 12,
									    borderLeftColor: 'transparent',
									    borderRightColor: 'transparent',
									    borderBottomColor: 'black',
									    transform: [
									      {rotate: '180deg'}
									    ]
									}}
								/>
							</View>
		            		</TouchableOpacity>
						</View>
						
						<View style={{flex:0.3,backgroundColor:'white', justifyContent:'center', alignItems:'center'}}>
							<TouchableOpacity
	                    		onPress={this._onPressSubmitIncomingOrder.bind(this)}
	                    	>
	                    		<ResponsiveImage
										source={this.state.image.konfirmasi.url}
										initWidth={this.state.image.konfirmasi.width * this.state.image.konfirmasi.multiplierWidth}
										initHeight={this.state.image.konfirmasi.height * this.state.image.konfirmasi.multiplierHeight}
								/>
	                    	</TouchableOpacity>
                    	</View>
		        	</View>
	        	</View>
	        </Modal>
		);
	}
}