import React, { Component } from 'react';
import {View, Modal, Dimensions, TouchableOpacity, Alert} from 'react-native';

import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';


export default class ConfirmPickupOrder extends Component {
	constructor(props) {
        super(props);
        this.state = {
        	image:{
				cancel_button:{
					width:0,
					height:0,
					multiplierWidth:1.1,
					multiplierHeight:1.1,
					url:require('./../../../../../image/order/cancel_button.png'),
				},
				konfirmasi:{
					width:0,
					height:0,
					multiplierWidth:0.6,
					multiplierHeight:0.6,
					url:require('./../../../../../image/order/konfirmasi.png'),
				},
				kilo:{
					width:0,
					height:0,
					multiplierWidth:0.25,
					multiplierHeight:0.25,
					url:require('./../../../../../image/order/kilo.png'),
				},
				pieces:{
					width:0,
					height:0,
					multiplierWidth:0.25,
					multiplierHeight:0.25,
					url:require('./../../../../../image/order/pieces.png'),
				},
        	},
        	limit_order:{
				weight:25,
				pieces:100,
        	},
        	modal:{
                isVisible:false,
                typeProgressModal:'standard',
                loadingWord:'mencoba masuk...'
            },
            string:{
				alertTitle:'Konfirmasi',
				alertContent:'Apakah anda yakin untuk menandakan order ini telah diambil dari konsumen dan siap untuk dicuci ?',
				alertReject:'Tidak',
				alertAccept:'Ya',
            },
            data:this.props.data ? this.props.data : [],
            _setData:this.props._setData ? this.props._setData : (()=>{}),
        };  
    }

	componentWillMount(){
		this._getImageSize();
	}

	componentDidUpdate(){
    	if (this.props.data.order_id !== this.state.data.order_id && this.props.data.order_id != undefined ) {
    		var data = this.state.data;
    		data.order_id = this.props.data.order_id;
    		this.setState({data});
		}
    }

	_getImageSize(){
		var image = this.state.image;

		var image_cancel_button = ResolveAssetSource(this.state.image.cancel_button.url);

		image.cancel_button.width = image_cancel_button.width;
		image.cancel_button.height = image_cancel_button.height;

		var image_konfirmasi = ResolveAssetSource(this.state.image.konfirmasi.url);

		image.konfirmasi.width = image_konfirmasi.width;
		image.konfirmasi.height = image_konfirmasi.height;

		var image_kilo = ResolveAssetSource(this.state.image.kilo.url);

		image.kilo.width = image_kilo.width;
		image.kilo.height = image_kilo.height;

		var image_pieces = ResolveAssetSource(this.state.image.pieces.url);

		image.pieces.width = image_pieces.width;
		image.pieces.height = image_pieces.height;

      	this.setState({image});
	}

	_show(isVisible){
		var modal = this.state.modal;
		modal.isVisible = isVisible;

		var data = this.state.data;

		if (!isVisible) {
			data.item_weight = '';
			data.pieces = '';
		}

		this.setState({modal,data});
	}

	_onValueChangeBerat(value){
		if (this._validateOnlyNumber(value)) {
			var data = this.state.data;

			data.item_weight = value;

			if (!this._validateMaxWeight(value)) {
				data.item_weight = this.state.limit_order.weight.toString();
			}

			if (value.substring(0,1) == 0) {
				data.item_weight = null;
			}

			this.setState({data}, () => {
				this.weight_inputfield.setValue(data.item_weight);
			});
		}
		else{
			if (this.state.data.item_weight == undefined) {
				this.state.data.item_weight = '';
			}

			this.weight_inputfield.setValue(this.state.data.item_weight.toString());
		}
	}

	_onValueChangePieces(value){
		if (this._validateOnlyNumber(value)) {
			var data = this.state.data;

			data.pieces = value;

			if (!this._validateMaxPieces(value)) {
				data.pieces = this.state.limit_order.pieces.toString();
			}

			if (value.substring(0,1) == 0) {
				data.pieces = null;
			}

			this.setState({data}, () => {
				this.pieces_inputfield.setValue(data.pieces);
			});
		}
		else{
			if (this.state.data.pieces == undefined) {
				this.state.data.pieces = '';
			}

			this.pieces_inputfield.setValue(this.state.data.pieces.toString());
		}
	}

	_onPressConfirmPickupOrder(){
		if (!this._validateIsEmpty()) {
			Alert.alert(
	            this.state.string.alertTitle,
	            this.state.string.alertContent,
	            [
	                {text:this.state.string.alertReject, onPress: () =>{

	                }},
	                {text:this.state.string.alertAccept, onPress: () =>{
	                	data = JSON.parse(JSON.stringify(this.state.data));
	                	this._show(false);

	                	setTimeout(() => {
							this.props._showModal(true, 'mengirimkan data pickup...', () => {
								this._submitPickup(data);
		                 	})
	                	}, 250);
	                }},
	            ]
	        );
        }
        else{
        	alert('mohon isi semua data yang diperlukan');
        }
	}

	_submitPickup(data){
		console.log(data);
		this.props._fetch(10000, Order._submitPickupOrder(data))
			.then((response) => {
				console.log(response);
				if (response.success == 'yeah') {
					if (this.props._callback) this.props._callback();
				}
				else{
					throw 'nope error';
				}
			})
			.catch((error) => {
				console.log(error);
				this.props._showApiErrorRetryCancel(this._submitPickup.bind(this, data));
			})
	}

	_validateIsEmpty(){
		var data = this.state.data;
		return (data.item_weight == null || data.item_weight.trim() == '' || data.pieces == null || data.pieces.trim() == '');
	}


	_validateOnlyNumber(value){
		var regexOnlyNumber = /^[0-9]+$/;
    	
    	if(regexOnlyNumber.test(value) || value == ''){
    		return true;
    	}
    	return false;
	}

	_validateMaxWeight(weight){
		return weight <= this.state.limit_order.weight;
	}

	_validateMaxPieces(pieces){
		return pieces <= this.state.limit_order.pieces;
	}

	render(){
		return(
				<Modal
		          animationType={'fade'}
		          transparent={true}
		          visible={this.state.modal.isVisible}
		          onRequestClose={() => {}}
		          >
		          	<View style={{flex:1, alignItems:'center', justifyContent:'center', backgroundColor:'rgba(0,0,0,0.6)'}}>
			          	<View style={{width:Dimensions.get('window').width * 0.8, height:Dimensions.get('window').height * 0.5 ,backgroundColor:'transparent'}}>
			          		<View style={{flex:0.3, justifyContent:'center', backgroundColor:'orange', alignItems:'center'}}>
			          			<View style={{flex:0.1, alignItems:'flex-end', alignSelf:'stretch'}}>
				          			<TouchableOpacity onPress={() => this._show(false)} style={{margin:5, width:this.state.image.cancel_button.width * this.state.image.cancel_button.multiplierWidth, height:this.state.image.cancel_button.height * this.state.image.cancel_button.multiplierHeight}}>
				          				<ResponsiveImage
											source={this.state.image.cancel_button.url}
											initWidth={this.state.image.cancel_button.width * this.state.image.cancel_button.multiplierWidth}
											initHeight={this.state.image.cancel_button.height * this.state.image.cancel_button.multiplierHeight}
										/>
				          			</TouchableOpacity>
			          			</View>
			          			<View style={{flex:0.9, justifyContent:'center', bottom:Dimensions.get('window').height * 0.5 * 0.1 * 0.33 / 2}}>
			          				<TextWrapper type={'subtitle'} style={{color:'white', fontWeight:'bold'}}>
			          					Konfirmasi Order
			          				</TextWrapper>
			          			</View>
			          			
			          		</View>
			          		<KeyboardAwareScrollView contentContainerStyle={{flex:0.4,backgroundColor:'white', justifyContent:'center'}}>
				          		<Form.InputField
				          			ref={ref => this.weight_inputfield = ref}
			                        keyboardType={'number-pad'}
			                        placeholder={'Masukan Berat (dalam Kg)'}
			                        placeholderTextColor = 'lightgray'
			                        height={Dimensions.get('window').height * 0.085}
			                        onValueChange={this._onValueChangeBerat.bind(this)}
			                        value={this.state.data.item_weight}
			                        containerStyle={{margin:15, marginTop:5, marginBottom:5}}
			                        iconLeft={
			                        	<ResponsiveImage
			                        		style={{marginLeft:Dimensions.get('window').width * 0.025}}
			                        		size={25}
											source={this.state.image.kilo.url}
											initWidth={this.state.image.kilo.width * this.state.image.kilo.multiplierWidth}
											initHeight={this.state.image.kilo.height * this.state.image.kilo.multiplierHeight}
										/>
		                        	}
		                    	/>

		                    	<Form.InputField
		                    		ref={ref => this.pieces_inputfield = ref}
			                        keyboardType={'number-pad'}
			                        placeholder={'Masukan Pieces (dalam pcs)'}
			                        placeholderTextColor = 'lightgray'
			                        height={Dimensions.get('window').height * 0.085}
			                        onValueChange={this._onValueChangePieces.bind(this)}
			                        value={this.state.data.pieces}
			                        containerStyle={{margin:15, marginTop:5, marginBottom:5}}
			                        iconLeft={
			                        	<ResponsiveImage
			                        		style={{marginLeft:Dimensions.get('window').width * 0.025}}
			                        		size={25}
											source={this.state.image.pieces.url}
											initWidth={this.state.image.pieces.width * this.state.image.pieces.multiplierWidth}
											initHeight={this.state.image.pieces.height * this.state.image.pieces.multiplierHeight}
										/>
		                        	}
		                    	/>
	                    	</KeyboardAwareScrollView>
							
							<View style={{flex:0.3,backgroundColor:'white', justifyContent:'center', alignItems:'center'}}>
								<TouchableOpacity
		                    		onPress={this._onPressConfirmPickupOrder.bind(this)}
									style={{flex:1}}
		                    	>
		                    		<ResponsiveImage
											source={this.state.image.konfirmasi.url}
											initWidth={this.state.image.konfirmasi.width * this.state.image.konfirmasi.multiplierWidth}
											initHeight={this.state.image.konfirmasi.height * this.state.image.konfirmasi.multiplierHeight}
									/>
		                    	</TouchableOpacity>
	                    	</View>
			        	</View>
		        	</View>
		        </Modal>
		);
	}
}