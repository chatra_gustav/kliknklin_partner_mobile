import React, { Component } from 'react';
import {TouchableOpacity, AsyncStorage} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';

export default class ButtonConfirmListPickup extends Component{
	constructor(props) {
        super(props);
        this.state = {
        	data:this.props.data ? this.props.data : {},
        	_setData:this.props._setData ? this.props._setData : (()=>{}),
        	image:{
        		report_checkout_button:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../../image/searchresult/report_checkout_button.png'),
				},
        	},
        }
    }

    componentWillMount(){
    	this._getImageSize();
    	this._getRoleUser();
    }

    async _getRoleUser(){
    	var role = await AsyncStorage.getItem('partnerkliknklin_role');

		this.setState({role});
    }

    _getImageSize(){
		var image = this.state.image;

		var image_report_checkout_button = ResolveAssetSource(this.state.image.report_checkout_button.url);

		image.report_checkout_button.width = image_report_checkout_button.width;
		image.report_checkout_button.height = image_report_checkout_button.height;

      	this.setState({image});
	}

    _onPressOrderPickUp(){
    	this.confirm_order_pickup._show(true);
    }

	render(){
		if (this.state.role){
			if (this.state.role == 'staff') {
				return(
					<TouchableOpacity onPress={this._onPressOrderPickUp.bind(this)}>
						<ResponsiveImage
							source={this.state.image.report_checkout_button.url}
							initWidth={this.state.image.report_checkout_button.width * this.state.image.report_checkout_button.multiplierWidth}
							initHeight={this.state.image.report_checkout_button.height * this.state.image.report_checkout_button.multiplierHeight}
						>
							<ConfirmOrder.Pickup
								ref={ref=>this.confirm_order_pickup = ref}
								data={this.props.data}
								_setData={this.state._setData}
								_showModal={this.props._showModal}
								_callback={this.props._callback}
								_fetch={this.props._fetch}
								_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
							/>
						</ResponsiveImage>
					</TouchableOpacity>
				);
			}
		}
		return null;
	}
}
