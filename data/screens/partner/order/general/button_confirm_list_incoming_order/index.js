import React, { Component } from 'react';
import {TouchableOpacity} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';

export default class ButtonConfirmListIncoming extends Component{
	constructor(props) {
        super(props);
        this.state = {
        	data:this.props.data ? this.props.data : [],
        	_setData:this.props._setData ? this.props._setData : (()=>{}),
        	image:{
        		report_accept_button:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../../image/searchresult/report_accept_button.png'),
				},
        	},
        }
    }

    componentWillMount(){
    	this._getImageSize();
    }

    _getImageSize(){
		var image = this.state.image;

		var image_report_accept_button = ResolveAssetSource(this.state.image.report_accept_button.url);

		image.report_accept_button.width = image_report_accept_button.width;
		image.report_accept_button.height = image_report_accept_button.height;

      	this.setState({image});
	}

    _onPressOrderAcceptPickup(){
    	this.confirm_order_incoming._show();
    }

	render(){
		return(
			<TouchableOpacity onPress={this._onPressOrderAcceptPickup.bind(this)}>
				<ResponsiveImage
					source={this.state.image.report_accept_button.url}
					initWidth={this.state.image.report_accept_button.width * this.state.image.report_accept_button.multiplierWidth}
					initHeight={this.state.image.report_accept_button.height * this.state.image.report_accept_button.multiplierHeight}
				>
					<ConfirmOrder.Incoming
						ref={ref=>this.confirm_order_incoming = ref}
						data={this.props.data}
						_getDataCourier={this.props._getDataCourier}
						_setData={this.state._setData}
						_showModal={this.props._showModal}
						_callback={this.props._callback}
						_fetch={this.props._fetch}
						_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
					/>
				</ResponsiveImage>
			</TouchableOpacity>
		);
	}
}
