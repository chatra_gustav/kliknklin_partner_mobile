import React, { Component } from 'react';
import {Alert} from 'react-native';

export default class RejectIncomingOrder extends Component {
	constructor(props) {
        super(props);
        this.state = {
        	image:{
        	},
            string:{
				alertTitle:'Konfirmasi',
				alertContent:'Apakah anda yakin untuk menolak order ini ?',
				alertReject:'Tidak',
				alertAccept:'Ya',
            },
            data:this.props.data ? this.props.data : [],
            _setData:this.props._setData ? this.props._setData : (()=>{}),
        };  
    }

	componentWillMount(){
	}

	_show(){
		Alert.alert(
            this.state.string.alertTitle,
            this.state.string.alertContent,
            [
                {text:this.state.string.alertReject},
                {text:this.state.string.alertAccept, onPress: () =>{
                	var data = JSON.parse(JSON.stringify(this.state.data));
                	data.order_action = 'decline';

					this.props._showModal(true, 'menolak order...', () => {
						this._rejectIncoming(data);
                 	})
                }},
            ]
        );
	}

	_rejectIncoming(data){
		this.props._fetch(10000, Order._submitIncomingOrder(data))
            .then((response) => {
            	console.log(response);
                if (response.success == 'yeah') {
					if (this.props._callback) this.props._callback();
				}
				else{
					throw 'nope error';
				}
            })
            .catch((error) => {
            	console.log(error);
                this.props._showApiErrorRetryCancel(this._submitIncoming.bind(this, data));
            })
	}

	_setData(data, callback = () => {}){
		this.setState({data}, callback());
	}

	render(){
		return(
			null
		);
	}
}