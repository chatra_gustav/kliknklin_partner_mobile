import React, { Component } from 'react';
import {TouchableOpacity, AsyncStorage} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';

export default class ButtonConfirmListDelivery extends Component{
	constructor(props) {
        super(props);
        this.state = {
        	data:this.props.data ? this.props.data : [],
        	_setData:this.props._setData ? this.props._setData : (()=>{}),
        	image:{
				report_delivered_button:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../../image/searchresult/report_delivered_button.png'),
				},
        	},
        }
    }

    componentWillMount(){
    	this._getImageSize();
    	this._getRoleUser();
    }

    async _getRoleUser(){
    	var role = await AsyncStorage.getItem('partnerkliknklin_role');

		this.setState({role});
    }
    _getImageSize(){
		var image = this.state.image;

		var image_report_delivered_button = ResolveAssetSource(this.state.image.report_delivered_button.url);

		image.report_delivered_button.width = image_report_delivered_button.width;
		image.report_delivered_button.height = image_report_delivered_button.height;

      	this.setState({image});
	}

    _onPressConfirmDelivered(){
    	this.confirm_order_delivery._show(true);
    }

	render(){
		if (this.state.role){
			if (this.state.role == 'staff') {
				if (this.props.data.payment_flag == 2 && this.props.data.banktransfer.status_transfer == 0) {
					return null;
				}
				return(
					<TouchableOpacity onPress={this._onPressConfirmDelivered.bind(this)}>
						<ResponsiveImage
							source={this.state.image.report_delivered_button.url}
							initWidth={this.state.image.report_delivered_button.width * this.state.image.report_delivered_button.multiplierWidth}
							initHeight={this.state.image.report_delivered_button.height * this.state.image.report_delivered_button.multiplierHeight}
						>
							<ConfirmOrder.Delivery
								ref={ref=>this.confirm_order_delivery = ref}
								data={this.props.data}
								_setData={this.state._setData}
								_showModal={this.props._showModal}
								_callback={this.props._callback}
								_fetch={this.props._fetch}
								_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
							/>
						</ResponsiveImage>
					</TouchableOpacity>
				);
			}
		}
		return null;
	}
}
