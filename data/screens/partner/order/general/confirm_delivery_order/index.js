import React, { Component } from 'react';
import {Alert} from 'react-native';

export default class ConfirmDeliveryOrder extends Component {
	constructor(props) {
        super(props);
        this.state = {
        	image:{
        	},
            string:{
				alertTitle:'Konfirmasi',
				alertContent:'Apakah anda yakin untuk menandakan order ini telah diantar ke konsumen?',
				alertReject:'Tidak',
				alertAccept:'Ya',
            },
            data:this.props.data ? this.props.data : [],
            _setData:this.props._setData ? this.props._setData : (()=>{}),
        };  
    }

	componentDidUpdate(){
    	if (this.props.data.order_id !== this.state.data.order_id && this.props.data.order_id != undefined ) {
    		var data = this.state.data;
    		data.order_id = this.props.data.order_id;
    		this.setState({data});
		}
    }

	_show(){
		Alert.alert(
            this.state.string.alertTitle,
            this.state.string.alertContent,
            [
                {text:this.state.string.alertReject},
                {text:this.state.string.alertAccept, onPress: () =>{
                	var data = JSON.parse(JSON.stringify(this.state.data));
                	this.props._showModal(true, 'mengirimkan data delivery...', () => {
						this._submitDelivery(data);
                 	});
                }},
            ]
        );
	}

    _submitDelivery(data){
        this.props._fetch(10000, Order._submitDeliveryOrder(data))
            .then((response) => {
                console.log(response);
                if (response.success == 'yeah') {
                    if (this.props._callback) this.props._callback();
                }
                else{
                    throw 'nope error';
                }
            })
            .catch((error) => {
                console.log(error);
                this.props._showApiErrorRetryCancel(this._submitDelivery.bind(this, data));
            })
    }

	render(){
		return(
			null
		);
	}
}