import React, { Component } from 'react';
import {View, ScrollView,TouchableOpacity, Dimensions, Alert} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export class LOnProgress extends Component{
	constructor(props) {
        super(props);
        this.state = {
        	data:this.props.data ? this.props.data : [],
        	_setData:this.props._setData ? this.props._setData : (()=>{}),
        	image:this.props.image ? this.props.image : null,        }
    }

    componentWillMount(){
    }

    componentDidUpdate(){
    }

    _setData(data){
		this.setState({data});
    }

    _formatDate(date){
    	date = new Date(date.replace(/-/g,"/"));
	  	return DateFormat(date, 'fullDate');
	}

	_formatTime(date){
		date = new Date(date.replace(/-/g,"/"));
	  	return DateFormat(date, 'simpleTime');
	}

    _onPressDetail(data, loginState){
    	this.props.navigator.push({
			id:'OrderDetail',
			title: 'Detail On Progress',
			contentType: 'onprogress',
			data:data,
			loginState:loginState,
			_afterSubmit:this._afterSubmitOnProgress.bind(this),
    	});
    }

    _afterSubmitOnProgress(callback = () => {}){
		this.props._configOrder(() => {
			this.props._setShouldReloadSummary(true);
			alert('order telah ditandai selesai dan siap diantar ke konsumen');
			if (callback) callback();
		});
    }

	render(){
		return(
			<View style={{flex:1, backgroundColor:'#F2F2F2'}}>
				<View style={{}}>
					{this.state.data.map((section, key) => { 
						var loginState= JSON.parse(JSON.stringify(this.props._getDataLoginState()));
						loginState.order_id = section.id;
						return(
						<TouchableOpacity key={key} onPress={this._onPressDetail.bind(this, section,loginState)}>
					        <View style={{marginTop:5, backgroundColor:'white'}}>
				          		<View style={{margin:10}}>
						            <View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.85, flexDirection:'row'}}>
						            		<View style={{flex:0.6}}>
						            			<TextWrapper type={'base'} style={{fontWeight:'normal', color:'darkslategray'}}>
								            		Order No. 
									            	<TextWrapper type={'content'} style={{fontWeight:'bold'}}>
									            		{section.order_hash}
									            	</TextWrapper>
								            	</TextWrapper>
						            		</View>
							            	

											{this.props._markOrderLate(section.is_late)}

											{this.props._markOrderImportant(section.important)}
							            </View>
						            	<View style={{flex:0.15, alignItems:'flex-end'}}>
						            		<ResponsiveImage
												source={this.state.image.detail_button.url}
												initWidth={this.state.image.detail_button.width * this.state.image.detail_button.multiplierWidth}
												initHeight={this.state.image.detail_button.height * this.state.image.detail_button.multiplierHeight}
											/>
						            	</View>
						            </View>
						            <View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.3}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Konsumen'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.65}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.customer.first_name}</TextWrapper>
						            	</View>
						            </View>
						            <View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.3}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Batas Lapor'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.65}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{this._formatDate(section.report_time_limit.date) + '\n' +  this._formatTime(section.report_time_limit.date) + ' WIB'}</TextWrapper>
						            	</View>
						            </View>
						            <View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.3}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Berat'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.65}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.item_weight + ' Kg'}</TextWrapper>
						            	</View>
						            </View>

						            <View style={{flexDirection:'row', marginTop:5, marginBottom:5}}>
						            	<View style={{flex:0.3}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Pieces'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.65}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.pieces + ' Pcs'}</TextWrapper>
						            	</View>
						            </View>

						            
										{
											section.orderstatus.id == 3 ? 
												<View style={{flexDirection:'row', marginTop:15, marginBottom:5, alignItems:'center', justifyContent:'center'}}>
													<ButtonConfirmList.OnProgress
														data={loginState}
														_setData={this.state._setData}
														_showModal={this.props._showModal}
														_callback={this._afterSubmitOnProgress.bind(this)}
														_fetch={this.props._fetch}
														_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
													/>
												</View>
												:
												null
										}
										
						            
					            </View>
					        </View>
				        </TouchableOpacity>
				     )})}
				</View>
			</View>
		);
	}
}

