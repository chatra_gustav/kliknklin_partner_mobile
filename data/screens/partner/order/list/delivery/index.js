import React, { Component } from 'react';
import {View, ScrollView,TouchableOpacity, Dimensions, Alert,ActivityIndicator} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export class LDelivery extends Component{
	constructor(props) {
        super(props);
        this.state = {
        	data:this.props.data ? this.props.data : [],
        	_setData:this.props._setData ? this.props._setData : (()=>{}),
        	image:this.props.image ? this.props.image : null,
        	initLoad:false,
        }
    }
	
    _setData(data){
		this.setState({data});
    }

    _onPressDetail(data, loginState){
    	this.props.navigator.push({
			id:'OrderDetail',
			title: 'Detail Delivery',
			contentType: 'delivery',
			data:data,
			loginState:loginState,
			_afterSubmit:this._afterSubmitDelivery.bind(this),
			_getIsBankTransferPaid:this._getIsBankTransferPaid.bind(this),
    	});
    }

    _afterSubmitDelivery(callback = () => {}){
    	this.setState({initLoad:false}, () => {
    		this.props._configOrder(() => {
    			this.props._setShouldReloadSummary(true);
    			alert('order telah selesai dikirim ke konsumen, terima kasih');
    			if (callback) callback();
    		});
    	});
    }

    _formatDate(date){
    	date = new Date(date.replace(/-/g,"/"));
	  	return DateFormat(date, 'fullDate');
	}

	_toCurrency(n, currency) {
    	return currency + " " + Number(n).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
	}

	_getPaymentMethod(section){
		if (section.payment_flag == 1) {
			return 'Cash';
		}
		else if (section.payment_flag == 2) {
			return 'Bank Transfer';
		}
		else if (section.payment_flag == 3) {
			return  'Membership';
		}
	}

	_renderHarga(section){
		if (section.payment_flag == 1 || section.payment_flag == 2) {
			return(
				 <View style={{flexDirection:'row', marginTop:5}}>
	            	<View style={{flex:0.25}}>
	            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Harga'}</TextWrapper>
	            	</View>
	            	<View style={{flex:0.05}}>
	            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
	            	</View>
	            	<View style={{flex:0.7}}>
						<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.payment_flag == 1 || section.payment_flag == 2 ? this._toCurrency(section.price, 'Rp') : this._toCurrency(0, 'Rp')}</TextWrapper>
	            	</View>
	            </View>
			);
		}
		return null;
	}

	_getIsBankTransferPaid(section){
		if (section.payment_flag == 2) {
			if (section.banktransfer) {
				if (section.banktransfer.status_transfer == 0 ) {
					return false;
				}
			}
		}
		return true;
	}

	_renderCaraPembayaran(section){

		var status_transfer = '';

		if (!this._getIsBankTransferPaid(section)) {
			status_transfer =  ' (Belum Dibayar)';
		}

		return(
			 <View style={{flexDirection:'row', marginTop:5}}>
            	<View style={{flex:0.25}}>
            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Pembayaran'}</TextWrapper>
            	</View>
            	<View style={{flex:0.05}}>
            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
            	</View>
            	<View style={{flex:0.7, flexDirection:'row'}}>
					<TextWrapper type={'base'} style={{color:'darkslategray'}}>{this._getPaymentMethod(section)}</TextWrapper>
					
					<TextWrapper type={'base'} style={{color:'red', fontWeight:'bold'}}>{status_transfer}</TextWrapper>	

            	</View>
            </View>
		);
	}

	render(){
		return(
			<View style={{flex:1, backgroundColor:'#F2F2F2'}}>
				<View style={{}}>
					{this.state.data.map((section, key) => {
						var loginState= JSON.parse(JSON.stringify(this.props._getDataLoginState()));
						loginState.order_id = section.id;
						loginState.payment_flag = section.payment_flag;
						loginState.banktransfer = section.banktransfer;
						return(
						<TouchableOpacity key={key} onPress={this._onPressDetail.bind(this, section, loginState)}>
					        <View style={{marginTop:5, backgroundColor:'white'}}>
				          		<View style={{margin:10}}>
						            <View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.85, flexDirection:'row'}}>
						            		<View style={{flex:0.6}}>
						            			<TextWrapper type={'base'} style={{fontWeight:'normal', color:'darkslategray'}}>
								            		Order No. 
									            	<TextWrapper type={'content'} style={{fontWeight:'bold'}}>
									            		{section.order_hash}
									            	</TextWrapper>
								            	</TextWrapper>
						            		</View>
							            	

											{this.props._markOrderLate(section.is_late)}

											{this.props._markOrderImportant(section.important)}
							            </View>
						            	<View style={{flex:0.15, alignItems:'flex-end'}}>
						            		<ResponsiveImage
												source={this.state.image.detail_button.url}
												initWidth={this.state.image.detail_button.width * this.state.image.detail_button.multiplierWidth}
												initHeight={this.state.image.detail_button.height * this.state.image.detail_button.multiplierHeight}
											/>
						            	</View>
						            </View>
						            <View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.25}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Nama'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.7}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.customer.first_name}</TextWrapper>
						            	</View>
						            </View>
									<View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.25}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Tanggal'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.7}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{this._formatDate(section.delivery_date)}</TextWrapper>
						            	</View>
						            </View>
						            <View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.25}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Waktu'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.7}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.deliverytime.info + ' WIB'}</TextWrapper>
						            	</View>
						            </View>

						            {this._renderHarga(section)}

						            {this._renderCaraPembayaran(section)}
						           
						            <View style={{flexDirection:'row', marginTop:5, marginBottom:5}}>
						            	<View style={{flex:0.25}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Alamat'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.7}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.delivery_address}</TextWrapper>
						            	</View>
						            </View>
						            <View style={{flexDirection:'row', marginTop:15, marginBottom:5, alignItems:'center', justifyContent:'center'}}>
										<ButtonConfirmList.Delivery
											data={loginState}
											_setData={this.state._setData}
											_showModal={this.props._showModal}
											_callback={this._afterSubmitDelivery.bind(this)}
											_fetch={this.props._fetch}
											_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
										/>
						            </View>
					            </View>
					        </View>
				        </TouchableOpacity>
				     )})}
				</View>
			</View>
		);
		
	}
}
