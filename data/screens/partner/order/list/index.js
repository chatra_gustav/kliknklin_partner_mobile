import React, { Component } from 'react';
import {View, ScrollView,TouchableOpacity, TouchableWithoutFeedback, Dimensions, Alert, ActivityIndicator} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export class OrderList extends Component {
	constructor(props) {
        super(props);
        this.state = {
        	image:{
				detail_button:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../image/searchresult/detail_button.png'),
				},
				cancel_button:{
					width:0,
					height:0,
					multiplierWidth:1,
					multiplierHeight:1,
					url:require('./../../../../image/order/cancel_button.png'),
				},
				konfirmasi:{
					width:0,
					height:0,
					multiplierWidth:0.65,
					multiplierHeight:0.65,
					url:require('./../../../../image/order/konfirmasi.png'),
				},
				kilo:{
					width:0,
					height:0,
					multiplierWidth:0.25,
					multiplierHeight:0.25,
					url:require('./../../../../image/order/kilo.png'),
				},
				pieces:{
					width:0,
					height:0,
					multiplierWidth:0.25,
					multiplierHeight:0.25,
					url:require('./../../../../image/order/pieces.png'),
				},
				late_active:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../image/order/late_active.png'),
				},
				late_inactive:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../image/order/late_inactive.png'),
				},
				warning_active:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../image/order/warning_active.png'),
				},
				warning_inactive:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../image/order/warning_inactive.png'),
				},
        	},
        	alert:{
        		failOpenForgetPassword:'Something went wrong, try again or contact us for more information',
        	},
        	modal:{
                isProgressLoadingVisible:false,
                typeProgressModal:'standard',
                loadingWord:'mencoba masuk...'
            },
            string:{
				loadingIncoming:'memuat data incoming...',
				loadingPickup:'memuat data pickup...',
				loadingOnProgress:'memuat data on progress',
				loadingDelivery:'memuat data delivery',
            },
            sort:{
				isPressedImportantButton:false,
				isPressedLateButton:false,
            },
            data:[],
            isDataEmpty:false,
            modal:{
                isProgressLoadingVisible:false,
                typeProgressModal:'standard',
                loadingWord:'memuat data pickup...',
            },
            contentLayout:{
				height:null,
        	},
        	contentType:this.props.contentType ? this.props.contentType : 'incoming',
        };  
    }

	componentDidMount(){
		this._getImageSize();
		this._configOrder(() =>{
			if (this.props.listType) {
				if (this.props.listType == 'important') {
					this._onPressImportantButton();
				}
				else if (this.props.listType == 'late') {
					this._onPressLateButton();
				}
			}
		});
	}

	_configOrder(callback = () => {}){
		this._resetFilterButton();
		this._searchbar._resetSearchForm();

		if (this.state.contentType == 'pickup') {
			this._configPickupOrder(callback);
		}
		else if (this.state.contentType == 'delivery') {
			this._configDeliveryOrder(callback);
		}
		else if (this.state.contentType == 'onprogress') {
			this._configOnprogressOrder(callback);
		}
		else if (this.state.contentType == 'incoming') {
			this._configIncomingOrder(callback);
		}
	}

	_configIncomingOrder(callback){
    	this.props._showModal(true, this.state.string.loadingIncoming, () => {
    		this.props._fetch(5000, Order._fetchIncomingOrder(this.props._getDataLoginState()))
				.then((response) => {
					this.props._showModal(false, '', () => {
						if (response.success == 'yeah') {
							this.props._showModal(false, '', () => {
								var isDataEmpty = true;

								if (response.data.length != 0) {
									isDataEmpty = false;
								}
								
								if (response.data == '') {
									response.data = [];
								}

								this.setState({data:response.data, isDataEmpty});

								this.order_incoming._setData(response.data);

								if (callback) callback();
							});
						}
						else{
							var isDataEmpty = true;

							this.setState({data:[], isDataEmpty});

							this.order_incoming._setData([]);
							
							if (callback) callback();
						}
					});
					
				})
				.catch((error) => {
					var isDataEmpty = true;
					this.setState({data:[], isDataEmpty}, () => {
						this.order_incoming._setData([]);

						this.props._showApiErrorRetryCancel(this._configIncomingOrder.bind(this));
					});
				});
    	});
    }

	_configPickupOrder(callback){
    	this.props._showModal(true, this.state.string.loadingPickup, () => {
    		this.props._fetch(5000, Order._fetchPickupOrder(this.props._getDataLoginState()))
				.then((response) => {
					if (response.success == 'yeah') {
						this.props._showModal(false, '', () => {
							var isDataEmpty = true;
							if (response.data.length != 0) {
								isDataEmpty = false;
							}
							
							this.setState({data:response.data, isDataEmpty}, () => {
								this.order_pickup._setData(response.data);
								if (callback) callback();
							});
						});
					}
					else{
						throw 'nope error';
					}
				})
				.catch((error) => {
					console.log(error);
					var isDataEmpty = true;
					this.setState({data:[], isDataEmpty}, () => {
						this.order_pickup._setData([]);
						this.props._showApiErrorRetryCancel(this._configPickupOrder.bind(this));
					});
				})
    	});
    }

    _configDeliveryOrder(callback){
		this.props._showModal(true, this.state.string.loadingDelivery, () => {
			this.props._fetch(5000, Order._fetchDeliveryOrder(this.props._getDataLoginState()))
				.then((response) => {
					if (response.success == 'yeah') {
						this.props._showModal(false, '', () => {
							var isDataEmpty = true;
							if (response.data.length != 0) {
								isDataEmpty = false;
							}

							this.setState({data:response.data, isDataEmpty}, () => {
								this.order_delivery._setData(response.data);
								if (callback) callback();
							});
						});
					}
					else{
						throw 'nope error';
					}
				})
				.catch((error) => {
					var isDataEmpty = true;
					this.setState({data:[], isDataEmpty}, () => {
						this.order_delivery._setData([]);
						this.props._showApiErrorRetryCancel(this._configDeliveryOrder.bind(this));
					});
				})
		});
    }

    _configOnprogressOrder(callback){
    	this.props._showModal(true, this.state.string.loadingOnProgress, () => {
    		this.props._fetch(5000, Order._fetchOnProgressOrder(this.props._getDataLoginState()))
				.then((response) => {
					if (response.success == 'yeah') {
						this.props._showModal(false, '', () => {
							var isDataEmpty = true;
							if (response.data.length != 0) {
								isDataEmpty = false;
							}

							this.setState({data:response.data, isDataEmpty}, () => {
								this.order_onprogress._setData(response.data);
								if (callback) callback();
							});
						});
					}
					else{
						throw 'nope error';
					}
				})
				.catch((error) => {
					var isDataEmpty = true;
					this.setState({data:[], isDataEmpty}, () => {
						this.order_onprogress._setData([]);
						this.props._showApiErrorRetryCancel(this._configOnprogressOrder.bind(this));
					});
				})
    	});
    }

	_afterSearch(filteredData){
		this._resetFilterButton();

		var isDataEmpty = false;

		if (filteredData.length == 0) {
			isDataEmpty = true;
		}
		else{
			isDataEmpty = false;
		}

		this.setState({isDataEmpty});

		var filter = JSON.parse(JSON.stringify(this.state.data));
		filter = filteredData;
		if (this.state.contentType == 'incoming') {
			this.order_incoming._setData(filter);
 		}
 		if (this.state.contentType == 'pickup') {
			this.order_pickup._setData(filter);
 		}
 		else if (this.state.contentType == 'onprogress') {
			this.order_onprogress._setData(filter);
 		}
 		else if (this.state.contentType == 'delivery') {
			this.order_delivery._setData(filter);
 		}
	}

	_resetFilterButton(){
		var sort = this.state.sort;
		sort.isPressedLateButton = false;
		sort.isPressedImportantButton = false;
		this.setState({sort});
	}

	_getImageSize(){
		var image = this.state.image;

		var image_detail_button = ResolveAssetSource(this.state.image.detail_button.url);

		image.detail_button.width = image_detail_button.width;
		image.detail_button.height = image_detail_button.height;

		var image_cancel_button = ResolveAssetSource(this.state.image.cancel_button.url);

		image.cancel_button.width = image_cancel_button.width;
		image.cancel_button.height = image_cancel_button.height;

		var image_konfirmasi = ResolveAssetSource(this.state.image.konfirmasi.url);

		image.konfirmasi.width = image_konfirmasi.width;
		image.konfirmasi.height = image_konfirmasi.height;

		var image_kilo = ResolveAssetSource(this.state.image.kilo.url);

		image.kilo.width = image_kilo.width;
		image.kilo.height = image_kilo.height;

		var image_pieces = ResolveAssetSource(this.state.image.pieces.url);

		image.pieces.width = image_pieces.width;
		image.pieces.height = image_pieces.height;

		var image_late_active = ResolveAssetSource(this.state.image.late_active.url);

		image.late_active.width = image_late_active.width;
		image.late_active.height = image_late_active.height;

		var image_late_inactive = ResolveAssetSource(this.state.image.late_inactive.url);

		image.late_inactive.width = image_late_inactive.width;
		image.late_inactive.height = image_late_inactive.height;

		var image_warning_active = ResolveAssetSource(this.state.image.warning_active.url);

		image.warning_active.width = image_warning_active.width;
		image.warning_active.height = image_warning_active.height;

		var image_warning_inactive = ResolveAssetSource(this.state.image.warning_inactive.url);

		image.warning_inactive.width = image_warning_inactive.width;
		image.warning_inactive.height = image_warning_inactive.height;

      	this.setState({image});
	}

 	_setData(data){
 		this.setState({data});
 	}

 	_getContent(){
 		if (this.state.contentType == 'incoming') {
			return(
				<OrderListContent.Incoming
					ref={(ref) => this.order_incoming = ref}
					image={this.state.image}
					data={this.state.data}
					navigator={this.props.navigator}
					_getDataLoginState={this.props._getDataLoginState}
					_getDataCourier={this.props._getDataCourier}
					_showModal={this.props._showModal}
					_configOrder={this._configOrder.bind(this)}
					_setShouldReloadSummary={this.props._setShouldReloadSummary}
					_setShouldReloadSummaryIncoming={this.props._setShouldReloadSummaryIncoming}
					_fetch={this.props._fetch}
					_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
				/>
			);
 		}
 		else if (this.state.contentType == 'pickup') {
			return(
				<OrderListContent.Pickup
					ref={(ref) => this.order_pickup = ref}
					image={this.state.image}
					data={this.state.data}
					navigator={this.props.navigator}
					_getDataLoginState={this.props._getDataLoginState}
					_markOrderLate={this._markOrderLate.bind(this)}
					_markOrderImportant={this._markOrderImportant.bind(this)}
					_showModal={this.props._showModal}
					_configOrder={this._configOrder.bind(this)}
					_setShouldReloadSummary={this.props._setShouldReloadSummary}
					_fetch={this.props._fetch}
					_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
				/>
			);
 		}
 		else if (this.state.contentType == 'onprogress') {
 			return(
				<OrderListContent.OnProgress
					ref={(ref) => this.order_onprogress = ref}
					image={this.state.image}
					data={this.state.data}
					navigator={this.props.navigator}
					_getDataLoginState={this.props._getDataLoginState}
					_markOrderLate={this._markOrderLate.bind(this)}
					_markOrderImportant={this._markOrderImportant.bind(this)}
					_showModal={this.props._showModal}
					_configOrder={this._configOrder.bind(this)}
					_setShouldReloadSummary={this.props._setShouldReloadSummary}
					_fetch={this.props._fetch}
					_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
				/>
			);
 		}
 		else if (this.state.contentType == 'delivery') {
			return (
				<OrderListContent.Delivery 
					ref={(ref) => this.order_delivery = ref}
					image={this.state.image}
					data={this.state.data}
					navigator={this.props.navigator}
					_getDataLoginState={this.props._getDataLoginState}
					_markOrderLate={this._markOrderLate.bind(this)}
					_markOrderImportant={this._markOrderImportant.bind(this)}
					_showModal={this.props._showModal}
					_configOrder={this._configOrder.bind(this)}
					_setShouldReloadSummary={this.props._setShouldReloadSummary}
					_fetch={this.props._fetch}
					_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
				/>
			);
 		}
 	}

 	_filterDataLate() {
	  	return this.state.data.filter((n) => {
	  		var result = n.is_late;
	  		
	  		return result;
	  	});
	}

	_filterDataImportant() {
	  	return this.state.data.filter((n) => {
	  		var result = n.important;
	  		
	  		return result;
	  	});
	}

 	_getEmptyContent(){
 		if (this.state.isDataEmpty) {
 			return(
				<View style={{alignItems:'center', marginTop:20}}>
					<TextWrapper type={'content'}>
						{'Tidak ada data order yang ditemukan'}
					</TextWrapper>
				</View>
 			);
 		}
 	}

 	_markOrderLate(isLate){
		if (isLate) {
			return(
				<View style={{flex:0.4, justifyContent:'center'}}>
					<View style={{width:65, backgroundColor:'#ed1c24', borderRadius:5, alignItems:'center' }}>
	            		<TextWrapper type={'base'} style={{fontWeight:'bold', color:'white'}}>
		            		{'Telat'}
		            	</TextWrapper>
	            	</View>
				</View>
			)
		}
		return null;
	}

	_markOrderImportant(isImportant){
		if (isImportant) {
			return(
				<View style={{flex:0.4, justifyContent:'center'}}>
					<View style={{width:65, backgroundColor:'#e79e2d', borderRadius:5, alignItems:'center' }}>
	            		<TextWrapper type={'base'} style={{fontWeight:'bold', color:'white'}}>
		            		{'Penting'}
		            	</TextWrapper>
	            	</View>
				</View>
			)
		}
		return null;
	}

	_onPressLateButton(){
		var sort = this.state.sort;
		sort.isPressedLateButton = !this.state.sort.isPressedLateButton;

		if (sort.isPressedLateButton && sort.isPressedImportantButton ) {
			sort.isPressedImportantButton = false;
		}

		if (sort.isPressedLateButton) {
			var data = JSON.parse(JSON.stringify(this.state.data));
			data = this._filterDataLate();

			if (this.state.contentType == 'pickup') {
				this.order_pickup._setData(data);
	 		}
	 		else if (this.state.contentType == 'onprogress') {
				this.order_onprogress._setData(data);
	 		}
	 		else if (this.state.contentType == 'delivery') {
				this.order_delivery._setData(data);
	 		}
		}
		else{
			if (this.state.contentType == 'pickup') {
				this.order_pickup._setData(this.state.data);
	 		}
	 		else if (this.state.contentType == 'onprogress') {
				this.order_onprogress._setData(this.state.data);
	 		}
	 		else if (this.state.contentType == 'delivery') {
				this.order_delivery._setData(this.state.data);
	 		}
		}
		
		this.setState({sort});
	}

	_onPressImportantButton(){
		var sort = this.state.sort;
		sort.isPressedImportantButton = !this.state.sort.isPressedImportantButton;

		if (sort.isPressedImportantButton && sort.isPressedLateButton) {
			sort.isPressedLateButton = false;
		}

		if (sort.isPressedImportantButton) {
			var data = JSON.parse(JSON.stringify(this.state.data));
			data = this._filterDataImportant();

			if (this.state.contentType == 'pickup') {
				this.order_pickup._setData(data);
	 		}
	 		else if (this.state.contentType == 'onprogress') {
				this.order_onprogress._setData(data);
	 		}
	 		else if (this.state.contentType == 'delivery') {
				this.order_delivery._setData(data);
				
	 		}
		}
		else{
			if (this.state.contentType == 'pickup') {
				this.order_pickup._setData(this.state.data);
	 		}
	 		else if (this.state.contentType == 'onprogress') {
				this.order_onprogress._setData(this.state.data);
	 		}
	 		else if (this.state.contentType == 'delivery') {
				this.order_delivery._setData(this.state.data);
	 		}
		}

		this.setState({sort});
	}

	_renderImportantButton(){
		if (this.state.sort.isPressedImportantButton) {
			return(
				<TouchableOpacity onPress={this._onPressImportantButton.bind(this)}>
					<ResponsiveImage
						source={this.state.image.warning_active.url}
						initWidth={this.state.image.warning_active.width * this.state.image.warning_active.multiplierWidth}
						initHeight={this.state.image.warning_active.height * this.state.image.warning_active.multiplierHeight}
					/>
				</TouchableOpacity>
			);
		}
		else{
			return(
				<TouchableOpacity onPress={this._onPressImportantButton.bind(this)}>
					<ResponsiveImage
						source={this.state.image.warning_inactive.url}
						initWidth={this.state.image.warning_inactive.width * this.state.image.warning_inactive.multiplierWidth}
						initHeight={this.state.image.warning_inactive.height * this.state.image.warning_inactive.multiplierHeight}
					/>
				</TouchableOpacity>
			);
		}	
	}

	_renderLateButton(){
		if (this.state.sort.isPressedLateButton) {
			return(
				<TouchableOpacity onPress={this._onPressLateButton.bind(this)}>
					<ResponsiveImage
						source={this.state.image.late_active.url}
						initWidth={this.state.image.late_active.width * this.state.image.late_active.multiplierWidth}
						initHeight={this.state.image.late_active.height * this.state.image.late_active.multiplierHeight}
					/>
				</TouchableOpacity>
			);
		}
		else{
			return(
				<TouchableOpacity onPress={this._onPressLateButton.bind(this)}>
					<ResponsiveImage
						source={this.state.image.late_inactive.url}
						initWidth={this.state.image.late_inactive.width * this.state.image.late_inactive.multiplierWidth}
						initHeight={this.state.image.late_inactive.height * this.state.image.late_inactive.multiplierHeight}
					/>
				</TouchableOpacity>
			);	
		}
	}

	_renderSideSorting(){
		if (this.state.contentType != 'incoming') {
			return(
				<View
					style={{flex:1,justifyContent:'space-between', width:55, height:Dimensions.get('window').height * 0.165 , position: 'absolute', bottom:Dimensions.get('window').height * 0.015, right:3,}}
				>
					{this._renderImportantButton()}

					{this._renderLateButton()}
				</View>
			);
		}
	}

	render(){
		return(
			<View style={{flex:1}}>
				<SearchBar 
					ref={(ref) => {this._searchbar = ref}}
					_getNavigator={() => {
						return this.props.navigator;
					}}
					type={this.state.contentType}
					data={this.state.data}
					_afterSearch={this._afterSearch.bind(this)}
				/>

				<ScrollView style={{flex:1}}>

					<Modal
						isVisible={this.state.modal.isProgressLoadingVisible}
						loadingWord={this.state.modal.loadingWord}
						type={this.state.modal.typeProgressModal}
					/>

					{this._getEmptyContent()}

					{this._getContent()}
				</ScrollView>

				
				{this._renderSideSorting()}
				
				

			</View>
		);
	}
}
