import React, { Component } from 'react';
import {View, ScrollView,TouchableOpacity, Dimensions, Alert, Modal} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export class LIncoming extends Component{
	constructor(props) {
        super(props);
        this.state = {
        	data:this.props.data ? this.props.data : [],
        	_setData:this.props._setData ? this.props._setData : (()=>{}),
        	image:this.props.image ? this.props.image : null,
        }
    }

    _setData(data){
		this.setState({data});
    }

    _formatDate(date){
    	date = new Date(date.replace(/-/g,"/"));
	  	return DateFormat(date, 'fullDate');
	}

	_afterSubmitAcceptIncoming(callback = () => {}, isAccepted){
    	this.setState({initLoad:false}, () => {
    		this.props._configOrder(() => {
    			this.props._setShouldReloadSummary();
    			this.props._setShouldReloadSummaryIncoming();
    			if (isAccepted) {
    				alert('order telah diterima, lihat menu pickup untuk melihat detail');
    			}
    			else{
    				alert('order gagal diterima, diambil terlebih dahulu oleh laundry lain atau internet anda bermasalah')
    			}
    			
    			if (callback) callback();
    		});
    	});
    }

    _afterSubmitRejectIncoming(callback = () => {}){
    	this.setState({initLoad:false}, () => {
    		this.props._configOrder(() => {
    			this.props._setShouldReloadSummary();
    			this.props._setShouldReloadSummaryIncoming();
    			alert('order telah ditolak, terima kasih');
    			if (callback) callback();
    		});
    	});
    }

    _renderAvailableOrder(section, loginState, key){
    	return(
				<View key={key} style={{marginTop:5, backgroundColor:'white'}}>
	          		<View style={{margin:10}}>
	          			<View style={{flexDirection:'row', marginTop:5}}>
			            	<View style={{flex:0.35}}>
			            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Tanggal Pickup'}</TextWrapper>
			            	</View>
			            	<View style={{flex:0.05}}>
			            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
			            	</View>
			            	<View style={{flex:0.6}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>{this._formatDate(section.order.pickup_date)}</TextWrapper>
			            	</View>
			            </View>
	          			<View style={{flexDirection:'row', marginTop:5}}>
			            	<View style={{flex:0.35}}>
			            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Waktu Pickup'}</TextWrapper>
			            	</View>
			            	<View style={{flex:0.05}}>
			            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
			            	</View>
			            	<View style={{flex:0.6}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.order.pickuptime.info + ' WIB'}</TextWrapper>
			            	</View>
			            </View>
			            <View style={{flexDirection:'row', marginTop:5}}>
			            	<View style={{flex:0.35}}>
			            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Tanggal Delivery'}</TextWrapper>
			            	</View>
			            	<View style={{flex:0.05}}>
			            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
			            	</View>
			            	<View style={{flex:0.6}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>{this._formatDate(section.order.delivery_date)}</TextWrapper>
			            	</View>
			            </View>
			            <View style={{flexDirection:'row', marginTop:5}}>
			            	<View style={{flex:0.35}}>
			            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Waktu Delivery'}</TextWrapper>
			            	</View>
			            	<View style={{flex:0.05}}>
			            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
			            	</View>
			            	<View style={{flex:0.6}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.order.deliverytime.info + ' WIB'}</TextWrapper>
			            	</View>
			            </View>
			            <View style={{flexDirection:'row', marginTop:5}}>
			            	<View style={{flex:0.35}}>
			            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Alamat'}</TextWrapper>
			            	</View>
			            	<View style={{flex:0.05}}>
			            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
			            	</View>
			            	<View style={{flex:0.6}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.order.pickup_address}</TextWrapper>
			            	</View>
			            </View>
			            <View style={{flexDirection:'row', marginTop:20, marginBottom:15, alignItems:'center', justifyContent:'center'}}>
			            	<View style={{flex:0.45, alignItems:'flex-end'}}>
								<ButtonRejectList.Incoming
									data={loginState}
									_getDataCourier={this.props._getDataCourier}
									_setData={this.state._setData}
									_showModal={this.props._showModal}
									_callback={this._afterSubmitRejectIncoming.bind(this)}
									_fetch={this.props._fetch}
									_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
								/>
							</View>
							<View style={{flex:0.1}}>
								
							</View>
							<View style={{flex:0.45, alignItems:'flex-start'}}>
								<ButtonConfirmList.Incoming
									data={loginState}
									_getDataCourier={this.props._getDataCourier}
									_setData={this.state._setData}
									_showModal={this.props._showModal}
									_callback={this._afterSubmitAcceptIncoming.bind(this)}
									_fetch={this.props._fetch}
									_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
								/>
							</View>
			            </View>
		            </View>
		        </View>
		     );
    }

    _renderUnavailableOrder(section, loginState, key){
    	return(
				<View key={key} style={{marginTop:5, backgroundColor:'#c0caca'}}>
					<View style={{height:Dimensions.get('window').height * 0.05, backgroundColor:'#97a7a7', alignItems:'center', justifyContent:'center'}}>
						<TextWrapper type={'base'} style={{color:'darkslategray', fontWeight:'bold'}}>{'Order sudah terlewatkan'}</TextWrapper>
					</View>
	          		<View style={{margin:10}}>
	          			<View style={{flexDirection:'row', marginTop:5}}>
			            	<View style={{flex:0.35}}>
			            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Tanggal Pickup'}</TextWrapper>
			            	</View>
			            	<View style={{flex:0.05}}>
			            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
			            	</View>
			            	<View style={{flex:0.6}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>{this._formatDate(section.order.pickup_date)}</TextWrapper>
			            	</View>
			            </View>
			            <View style={{flexDirection:'row', marginTop:5}}>
			            	<View style={{flex:0.35}}>
			            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Alamat'}</TextWrapper>
			            	</View>
			            	<View style={{flex:0.05}}>
			            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
			            	</View>
			            	<View style={{flex:0.6}}>
								<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.order.pickup_address}</TextWrapper>
			            	</View>
			            </View>
		            </View>
		        </View>
		     );
    }

	render(){
		return(
			<View style={{flex:1, backgroundColor:'#F2F2F2'}}>
				<View style={{}}>
					{this.state.data.map((section, key) => {
						var loginState= JSON.parse(JSON.stringify(this.props._getDataLoginState()));
						loginState.order_id = section.order.id;
							if (section.status == 'Available') {
								return this._renderAvailableOrder(section, loginState, key);
							}
							else{
								return this._renderUnavailableOrder(section, loginState, key);	
							}
					})}
				</View>
			</View>
		);
	}
}

