import React, { Component } from 'react';
import {View, ScrollView,TouchableOpacity, Dimensions, Alert} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export class LPickup extends Component{
	constructor(props) {
        super(props);
        this.state = {
        	data:this.props.data ? this.props.data : [],
        	_setData:this.props._setData ? this.props._setData : (()=>{}),
        	image:this.props.image ? this.props.image : null,
        	initLoad:false,
        }
    }

    componentWillMount(){
    }

    componentDidUpdate(){
    }

    _setData(data){
		this.setState({data});
    }

    _onPressDetail(data, loginState){
    	this.props.navigator.push({
			id:'OrderDetail',
			title:'Detail Pickup',
			contentType:'pickup',
			data:data,
			loginState:loginState,
			_afterSubmit:this._afterSubmitPickup.bind(this),
    	});
    }

    _afterSubmitPickup(callback = () => {}){
    	this.setState({initLoad:false}, () => {
    		this.props._configOrder(() => {
    			this.props._setShouldReloadSummary(true);
    			alert('order telah diambil, silahkan lihat tab on progress untuk detail');
    			if (callback) callback();
    		});
    	});
    }

     _formatDate(date){
    	date = new Date(date.replace(/-/g,"/"));
	  	return DateFormat(date, 'fullDate');
	}

	render(){
		return(
			<View style={{flex:1, backgroundColor:'#F2F2F2'}}>
				<View style={{}}>
					{this.state.data.map((section, key) => { 
						var loginState= JSON.parse(JSON.stringify(this.props._getDataLoginState()));
						loginState.order_id = section.id;
						return(
						<TouchableOpacity key={key} onPress={this._onPressDetail.bind(this, section, loginState)}>
					        <View style={{marginTop:5, backgroundColor:'white'}}>
				          		<View style={{margin:10}}>
						            <View style={{flexDirection:'row'}}>
						            	<View style={{flex:0.85, flexDirection:'row'}}>
						            		<View style={{flex:0.6}}>
						            			<TextWrapper type={'base'} style={{fontWeight:'normal', color:'darkslategray'}}>
								            		Order No. 
									            	<TextWrapper type={'content'} style={{fontWeight:'bold'}}>
									            		{section.order_hash}
									            	</TextWrapper>
								            	</TextWrapper>
						            		</View>
							            	

											{this.props._markOrderLate(section.is_late)}

											{this.props._markOrderImportant(section.important)}
							            </View>
						            	<View style={{flex:0.15, alignItems:'flex-end'}}>
						            		<ResponsiveImage
												source={this.state.image.detail_button.url}
												initWidth={this.state.image.detail_button.width * this.state.image.detail_button.multiplierWidth}
												initHeight={this.state.image.detail_button.height * this.state.image.detail_button.multiplierHeight}
											/>
						            	</View>
						            </View>
						            <View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.2}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Nama'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.75}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.customer.first_name}</TextWrapper>
						            	</View>
						            </View>
						             <View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.2}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Tanggal'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.75}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{this._formatDate(section.pickup_date)}</TextWrapper>
						            	</View>
						            </View>
						            <View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.2}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Waktu'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.75}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.pickuptime.info + ' WIB'}</TextWrapper>
						            	</View>
						            </View>
						            <View style={{flexDirection:'row', marginTop:5, marginBottom:5}}>
						            	<View style={{flex:0.2}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Alamat'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.75}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.pickup_address}</TextWrapper>
						            	</View>
						            </View>
						            <View style={{flexDirection:'row', marginTop:15, marginBottom:5, alignItems:'center', justifyContent:'center'}}>
										<ButtonConfirmList.Pickup
											data={loginState}
											_setData={this.state._setData}
											_showModal={this.props._showModal}
											_callback={this._afterSubmitPickup.bind(this)}
											_fetch={this.props._fetch}
											_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
										/>
						            </View>
					            </View>
					        </View>
					   </TouchableOpacity>
					    )})}
				</View>
			</View>
		);
	}
}
