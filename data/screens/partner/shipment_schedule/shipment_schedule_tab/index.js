import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

import {TextWrapper, DateFormat, TabBar} from './../../../../components';
import * as STRING from './../../../../string';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from "./../../../../actions";

class ShipmentScheduleTab extends Component{

    componentDidMount(){
    }
    getListOfScheduleDate(startDate = new Date()){
        var scheduleDateList = [];

        for (var i = 0; i < 7; i++) {
          var date = new Date(startDate);
          scheduleDateList.push(new Date(date.setDate(date.getDate() + i)));

        }
        
        return scheduleDateList;
    }

  onPress(key, dates){
    try{
      let {actionOrder} = this.props;
      actionOrder.setShipmentDate(dates);
    }
    catch(error){
      console.log(error);
    }
  }

  render(){
    return(
        <TabBar.TypeC
          item={this.getListOfScheduleDate()}
          containerStyle={this.props.containerStyle}
          onPress={this.onPress.bind(this)}
        />
    );
  }
}

const styles = StyleSheet.create({
  dateScheduleTextActive: {
    color:'#61CDFF',
  },
  eachDateIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  eachDateIndicatorActive: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    borderBottomWidth: 2,
    borderBottomColor: '#61CDFF', 
    borderStyle: 'dotted'
  },
});

function mapStateToProps(state, ownProps) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionOrder: bindActionCreators(Actions.order, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ShipmentScheduleTab);