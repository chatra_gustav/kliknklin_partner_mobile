import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
} from 'react-native';

import {TextWrapper, } from './../../../../components';
import * as STRING from './../../../../string';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from "./../../../../actions";

class AssignmentTodayCourier extends Component {
  render () {
    let {totalPickup, totalDelivery} = this.props;
    return (
      <View style={styles.container}>
      		<View style={styles.section}>
            	<TextWrapper fontWeight={'SemiBold'} type={'smalltitle'} style={styles.text}>{totalPickup}</TextWrapper>
	          	<TextWrapper type={'smallercontent'} style={styles.text}>{STRING.SCREEN.DASHBOARD_COURIER.AMOUNT_PICKUP_TODAY_LABEL}</TextWrapper>
	        </View>
	        <View style={styles.section}>
	          	<TextWrapper fontWeight={'SemiBold'} type={'smalltitle'} style={styles.text}>{totalDelivery}</TextWrapper>
	          	<TextWrapper type={'smallercontent'} style={styles.text}>{STRING.SCREEN.DASHBOARD_COURIER.AMOUNT_DELIVERY_TODAY_LABEL}</TextWrapper>
	        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
    container:{
      height:Dimensions.get('window').height * 0.1,
      flexDirection:'row',
    },
    section:{
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    text:{
      color:'white',
    },
});

function mapStateToProps(state, ownProps) {
  return {
    totalPickup:state.rootReducer.order.assignmentTodayCourier.totalPickup,
    totalDelivery:state.rootReducer.order.assignmentTodayCourier.totalDelivery,
  };
}

function mapDispatchToProps(dispatch) {
  return {

  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AssignmentTodayCourier);

