import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  FlatList,
  Image,
} from 'react-native';

import {LayoutPrimary} from './../../../../layout';
import {TextWrapper, DateFormat, ShipmentStatus, nthIndexOccurenceString} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

import ResolveAssetSource from 'resolveAssetSource';
import {moderateScale} from 'react-native-size-matters';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from "./../../../../actions";


class ShipmentScheduleList extends Component {
  
  componentDidMount(){
    //this.scrollToIndex();
  }

  scrollToIndex(index = Math.floor(this.props.data.length / 2)){
    setTimeout(() => {this.flatListRef.scrollToIndex({viewPosition:0, index: index});}, 250);
  }

  render() {
    let {shipmentScheduleList} = this.props;
    return (
      <View style={shipmentScheduleListStyle.container}>
        <FlatList
          ref={(component) => this.flatListRef = component }
          data={shipmentScheduleList}
          keyExtractor={(item, index) => index+''}
          onScrollToIndexFailed={() => {}}
          renderItem={(data) => { 
            return (
              <ShipmentSlot
                
                {...this.props}
                item={data.item}
              />
            );
          }}
        /> 
      </View> 
    )
  }
}

const shipmentScheduleListStyle = StyleSheet.create({
  container:{
    flex: 1,
    height: Dimensions.get('window').height * 0.53,
  }
});

class ShipmentSlot extends Component{

  _getTimeInfo(){
    let {item} = this.props;

    let {order_time_info} = item;

    let timeInfo = order_time_info.split('-');
    if (timeInfo.length == 2) {
      return timeInfo[0] + ' - ' + timeInfo[1];
    }
    return order_time_info;
  }

  render(){
    let {item} = this.props;

    let {order_time_info, logschedule, shipment_id} = item;
    return(
      <View 
          style={shipmentSlotStyle.container} 
        >

            <View style={shipmentSlotStyle.leftSection}>
                <TextWrapper type={'base'} style={shipmentSlotStyle.timeSlotText}>
                  {this._getTimeInfo()}
                </TextWrapper>
            </View>

            <View style={shipmentSlotStyle.rightSection}>
                <FlatList
                  data={logschedule}
                  keyExtractor={(item, index) => index+''}
                  renderItem={(data) => { 
                    return (
                      <ShipmentOrder
                        {...this.props}
                        item={data.item}
                      />
                    );
                  }}
                />
            </View>

      </View>
    );
  }
}

const shipmentSlotStyle = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    marginTop: Dimensions.get('window').height * 0.01,
  },
  leftSection:{
    flex: 0.12,
    flexDirection: 'row',
    //backgroundColor:'blue',
  },
  rightSection:{
    flex: 0.88,
    borderLeftWidth: 1.5,
    borderLeftColor: CONSTANT.COLOR.GRAY_B,
  },
  timeSlotText:{
    top:moderateScale(-3),
  }
});

class ShipmentOrder extends Component {

  getShipment(shipments, shipment_id){
    for(let index in shipments){
      if (shipments[index].id == shipment_id) {
        return shipments[index];
      }
    }
    return null;
  }

  getBorderLeftColor(){
    let {item} = this.props;
    let {order, shipment_id} = item;
    let {shipments} = order;
    let {shipment_type_id} = this.getShipment(shipments, shipment_id);

    if (shipment_type_id == CONSTANT.SHIPMENT_TYPE.PICKUP) {
      return CONSTANT.COLOR.BLUE_A;
    }
    else if (shipment_type_id == CONSTANT.SHIPMENT_TYPE.DELIVERY) {
      return CONSTANT.COLOR.GREEN_A;
    }
    return 'transparent';
  }

  onPress(item){
    let {order, shipment_id} = item;
    let {shipments} = order;
    let {shipment_type_id} = this.getShipment(shipments, shipment_id);

    let orderActiveType = null;

    if (shipment_type_id == CONSTANT.SHIPMENT_TYPE.PICKUP) {
      orderActiveType = CONSTANT.ORDER_ACTIVE_TYPE.PICKUP;
    }
    else if (shipment_type_id == CONSTANT.SHIPMENT_TYPE.DELIVERY) {
      orderActiveType = CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY;
    }

    this.props.actionOrder.setOrderActive(order, orderActiveType);
    this.props.actionOrder.setShipmentActivePickup(order);
    this.props.actionOrder.setShipmentActiveDelivery(order);
    this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.OrderContent', {})
  }

  render() {
    let {item} = this.props;

    let {order, shipment_id} = item;
    let {shipments, customer} = order;
    let {shipment_type_id, shipment_status_id, shipmentschedule} = this.getShipment(shipments, shipment_id);
    let {location_address} = shipmentschedule;
    let {user} = customer;

    const address = location_address.substr(0, nthIndexOccurenceString(location_address, ',', 4));

    return (
        <TouchableOpacity
            onPress={this.onPress.bind(this, item)}
            style={[shipmentOrderStyle.container,{borderLeftColor:this.getBorderLeftColor()}]}
          >
                  <View style={shipmentOrderStyle.leftSection}>
                    <TextWrapper type={'base'} style={{marginBottom:moderateScale(5)}}>{user.name}</TextWrapper>
                    <TextWrapper type={'base'}>{address}</TextWrapper>
                  </View>
                  <View style={shipmentOrderStyle.rightSection}>
                      <ShipmentStatus
                          shipmentStatus={shipment_status_id}
                      />
                  </View>
          </TouchableOpacity>
    );

    
  }
}

const shipmentOrderStyle = StyleSheet.create({
  container:{
    minHeight: Dimensions.get('window').height * 0.095,
    backgroundColor:'white',
    borderRadius: 5,
    borderLeftWidth: 5,
    marginHorizontal:moderateScale(3),
    marginBottom:moderateScale(5),
    elevation: 1,
    flexDirection:'row'
  },
  leftSection:{
    flex:0.7,
    paddingHorizontal:moderateScale(10),
    paddingVertical:moderateScale(5),
    justifyContent:'center',
  },
  rightSection:{
    justifyContent:'center', 
    alignItems:'center',
    flex:0.3,
  },
});

function mapStateToProps(state, ownProps) {
  return {
    errorMessage:state.rootReducer.message.error,
    layoutHeight:state.rootReducer.layout.height,
    shipmentScheduleList:state.rootReducer.order.shipmentScheduleList,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionOrder: bindActionCreators(Actions.order, dispatch),
    actionRoot: bindActionCreators(Actions.root, dispatch),
    actionNavigator: bindActionCreators(Actions.navigator, dispatch),

  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ShipmentScheduleList);


