import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';

import {LayoutPrimary} from './../../../layout';
import {TextWrapper, DateFormat, TabBar} from './../../../components';
import * as STRING from './../../../string';

import ResolveAssetSource from 'resolveAssetSource';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from "./../../../actions";


//local component
import AssignmentTodayCourier from './assignment_today_courier';
import ShipmentScheduleTab from './shipment_schedule_tab';
import ShipmentScheduleList from './shipment_schedule_list';

class ShipmentSchedule extends Component {

  componentWillMount(){
    this.props.navigator.setOnNavigatorEvent((event) => {
      if (event.id == 'backPress') {
        console.log('hai');
      }
    });
  }

	_renderContent(){
		return (
			<View
				style={{
					flex:1,
          marginBottom:Dimensions.get('window').height * 0.09,
				}}
			>
				<AssignmentTodayCourier/>

        <ShipmentScheduleTab
          containerStyle={{marginVertical:Dimensions.get('window').height * 0.025}}
        />

        <ShipmentScheduleList
          data={dataOrder.shipmentData[0].Data}
          navigator={this.props.navigator}
        />
	   </View>
		);
	}

	render() {
  	return (
  		<LayoutPrimary
  			showTitle={true}
  			showSearchButton={true}
  			showNotificationButton={true}
        showTabBar={true}
        tabKey={this.props.tabKey}
        navigator={this.props.navigator}
  			titleText={STRING.SCREEN.DASHBOARD_COURIER.TITLE}
  		>
  	      	{this._renderContent()}
  	  	</LayoutPrimary>
  	);
	}

}

const styles = StyleSheet.create({

});

function mapStateToProps(state, ownProps) {
	return {
		errorMessage:state.rootReducer.message.error,
		layoutHeight:state.rootReducer.layout.height,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionAuthentication: bindActionCreators(Actions.authentication, dispatch),
		actionRoot: bindActionCreators(Actions.root, dispatch),
    actionNavigator: bindActionCreators(Actions.navigator, dispatch),
    actionOrder: bindActionCreators(Actions.order, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ShipmentSchedule);

const dataAssignment = {
  pickup:7,
  delivery:17,
};

const dataOrder = {
  shipmentData: [
    {
      Date: '2018-01-21', 
      Data: [
        { 
          timeSlot: '10.00',
          dataSlot: [

            { orderID: '123131231', shipmentStatus: 'BATAL', orderStatus:'WaitingForPickup',  shipmentDate:'12-03-2018',pickupTime: '10.00', address: 'Ariobimo Sentral, Lt. 9, Jl. HR Rasuna Said Blok X-2 Kav 5, RT.9/RW.4, Kuningan Tim., Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950', addressInfo:'lantai 8 langsung ke resepsionis saja ya mas trims',
              orderItems:[
        {
          itemName:'Sepatu Nylon/Rubber/Canvas',
          washingMethodName:'Deep Clean',
          quantity:4,
          unitName:'Pair',
          addedService:[
            {
              serviceName:'Unyellowing',
            },
            {
              serviceName:'Reglue',
            }
          ],
          unitPrice:24000,
        },
        {
          itemName:'Bed Cover Besar',
          washingMethodName:'Hand Clean',
          quantity:2,
          unitName:'Pcs',
          addedService:[],
          unitPrice:72000,
        },
        {
          itemName:'Cuci, Lipat, Gosok',
          washingMethodName:'Wet Clean',
          quantity:6,
          unitName:'Kg',
          addedService:[],
          unitPrice:12000,
        },
      ],
      promo:[
        {
          promoName:'Promo code #WEEKENDSERU',
          type:'order',
          quantity:1,
          unitPrice:-20000,
        },
        {
          promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
          type:'shipment',
          quantity:1,
          unitPrice:-20000,
        },
      ],
      shipment:[
        {
          shipmentName:'Internal kurir-Pengambilan',
          shipmentPrice:12000,
        },
        {
          shipmentName:'Internal kurir-Pengantaran',
          shipmentPrice:12000,
        }
      ]
          },
            { orderID: '233454554', shipmentStatus: 'SELESAI', orderStatus:'GoToPickupPoint',  pickupTime: '10.00', address: 'Palma One, Jalan Haji R. Rasuna Said, Karet Kuningan, Setiabudi, RT.8/RW.4, Kuningan Tim., Setia Budi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12940', 
              orderItems:[
                {
                  itemName:'Sepatu Nylon/Rubber/Canvas',
                  washingMethodName:'Deep Clean',
                  quantity:4,
                  unitName:'Pair',
                  addedService:[
                    {
                      serviceName:'Unyellowing',
                    },
                    {
                      serviceName:'Reglue',
                    }
                  ],
                },
                {
                  itemName:'Bed Cover Besar',
                  washingMethodName:'Hand Clean',
                  quantity:2,
                  unitName:'Pcs',
                  addedService:[],
                },
                {
                  itemName:'Cuci, Lipat, Gosok',
                  washingMethodName:'Wet Clean',
                  quantity:6,
                  unitName:'Kg',
                  addedService:[],
                },
              ],
              payment:{
                method:'Bank Transfer',
                toBePaid:0,
              }
            },
            { orderID: '233454554', shipmentStatus: 'SELESAI', orderStatus:'GoToDeliveryPoint',  pickupTime: '10.00', address: 'Jl. Fachrudin No.3, RT.1/RW.7, Kp. Bali, Tanah Abang, Jakarta, Daerah Khusus Ibukota Jakarta 10250',
              
              orderItems:[
        {
          itemName:'Sepatu Nylon/Rubber/Canvas',
          washingMethodName:'Deep Clean',
          quantity:4,
          unitName:'Pair',
          addedService:[
            {
              serviceName:'Unyellowing',
            },
            {
              serviceName:'Reglue',
            }
          ],
          unitPrice:24000,
        },
        {
          itemName:'Bed Cover Besar',
          washingMethodName:'Hand Clean',
          quantity:2,
          unitName:'Pcs',
          addedService:[],
          unitPrice:72000,
        },
        {
          itemName:'Cuci, Lipat, Gosok',
          washingMethodName:'Wet Clean',
          quantity:6,
          unitName:'Kg',
          addedService:[],
          unitPrice:12000,
        },
      ],
      promo:[
        {
          promoName:'Promo code #WEEKENDSERU',
          type:'order',
          quantity:1,
          unitPrice:-20000,
        },
        {
          promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
          type:'shipment',
          quantity:1,
          unitPrice:-20000,
        },
      ],
      shipment:[
        {
          shipmentName:'Internal kurir-Pengambilan',
          shipmentPrice:12000,
        },
        {
          shipmentName:'Internal kurir-Pengantaran',
          shipmentPrice:12000,
        }
      ]

          },
          ]
        },
        {
          timeSlot: '11.00',
          dataSlot: [
            { orderID: '129599231', latitude:-6.1330, longitude:106.8267, shipmentStatus: 'BATAL', orderStatus:'ReadyToBeDelivered', pickupTime: '11.00', address: 'JW Mariot Hotel, Jalan DR Ide Anak Agung Gde Agung Kav E.1.2 No. 1 & 2, Kawasan Mega Kuningan, East Kuningan, Setia Budi, Jakarta, DKI Jakarta 12950 asdasd asdasd asdasd asdasd asdasd asdasd asdasd', addressInfo:'lantai 8 langsung ke resepsionis saja ya mas trims belok kanan ujung jalan kenangan bang msih kurang',
              orderItems:[
                {
                  itemName:'Sepatu Nylon/Rubber/Canvas',
                  washingMethodName:'Deep Clean',
                  quantity:4,
                  unitName:'Pair',
                  addedService:[
                    {
                      serviceName:'Unyellowing',
                    },
                    {
                      serviceName:'Reglue',
                    }
                  ],
                  unitPrice:24000,
                },
                {
                  itemName:'Bed Cover Besar',
                  washingMethodName:'Hand Clean',
                  quantity:2,
                  unitName:'Pcs',
                  addedService:[],
                  unitPrice:72000,
                },
                {
                  itemName:'Cuci, Lipat, Gosok',
                  washingMethodName:'Wet Clean',
                  quantity:6,
                  unitName:'Kg',
                  addedService:[],
                  unitPrice:12000,
                },
              ],
              promo:[
                {
                  promoName:'Promo code #WEEKENDSERU',
                  type:'order',
                  quantity:1,
                  unitPrice:-20000,
                },
                {
                  promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
                  type:'shipment',
                  quantity:1,
                  unitPrice:-20000,
                },
              ],
              shipment:[
                {
                  shipmentName:'Internal kurir-Pengambilan',
                  shipmentPrice:12000,
                },
                {
                  shipmentName:'Internal kurir-Pengantaran',
                  shipmentPrice:12000,
                }
              ]
            },
          ]
        },
        {
          timeSlot: '15.00',
          dataSlot: [
            { orderID: '120823134', latitude:-6.1330, longitude:106.8267,  deliveryLatitude:-6.1330, deliveryLongitude:106.8267, pickupLatitude:-6.1330, pickupLongitude:106.8267, shipmentStatus: 'TUNDA', orderStatus: 'GoToPickupPoint',  pickupTime: '15.00', address: 'Agro Plaza, Jl. H.R. Rasuna Said Kav. X 2 - 1, Kuningan Timur RT.7/RW.4, Setiabudi, Kuningan Timur, RT.7/RW.4, Kuningan Tim., Setia Budi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950',
                  orderItems:[
                  {
                    itemName:'Sepatu Nylon/Rubber/Canvas',
                    washingMethodName:'Deep Clean',
                    quantity:4,
                    unitName:'Pair',
                    addedService:[
                      {
                        serviceName:'Unyellowing',
                      },
                      {
                        serviceName:'Reglue',
                      }
                    ],
                    unitPrice:24000,
                  },
                  {
                    itemName:'Bed Cover Besar',
                    washingMethodName:'Hand Clean',
                    quantity:2,
                    unitName:'Pcs',
                    addedService:[],
                    unitPrice:72000,
                  },
                  {
                    itemName:'Cuci, Lipat, Gosok',
                    washingMethodName:'Wet Clean',
                    quantity:6,
                    unitName:'Kg',
                    addedService:[],
                    unitPrice:12000,
                  },
                ],
                promo:[
                  {
                    promoName:'Promo code #WEEKENDSERU',
                    type:'order',
                    quantity:1,
                    unitPrice:-20000,
                  },
                  {
                    promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
                    type:'shipment',
                    quantity:1,
                    unitPrice:-20000,
                  },
                ],
                shipment:[
                  {
                    shipmentName:'Internal kurir-Pengambilan',
                    shipmentPrice:12000,
                  },
                  {
                    shipmentName:'Internal kurir-Pengantaran',
                    shipmentPrice:12000,
                  }
                ],
                payment:{
                  method:'Bank Transfer',
                  toBePaid:0,
                },
            },
            { orderID: '120823134', shipmentStatus: 'TUNDA', orderStatus: 'ReadyToBeDelivered',  pickupTime: '15.00', address: 'Agro Plaza, Jl. H.R. Rasuna Said Kav. X 2 - 1, Kuningan Timur RT.7/RW.4, Setiabudi, Kuningan Timur, RT.7/RW.4, Kuningan Tim., Setia Budi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950',

            orderItems:[
        {
          itemName:'Sepatu Nylon/Rubber/Canvas',
          washingMethodName:'Deep Clean',
          quantity:4,
          unitName:'Pair',
          addedService:[
            {
              serviceName:'Unyellowing',
            },
            {
              serviceName:'Reglue',
            }
          ],
          unitPrice:24000,
        },
        {
          itemName:'Bed Cover Besar',
          washingMethodName:'Hand Clean',
          quantity:2,
          unitName:'Pcs',
          addedService:[],
          unitPrice:72000,
        },
        {
          itemName:'Cuci, Lipat, Gosok',
          washingMethodName:'Wet Clean',
          quantity:6,
          unitName:'Kg',
          addedService:[],
          unitPrice:12000,
        },
      ],
      promo:[
        {
          promoName:'Promo code #WEEKENDSERU',
          type:'order',
          quantity:1,
          unitPrice:-20000,
        },
        {
          promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
          type:'shipment',
          quantity:1,
          unitPrice:-20000,
        },
      ],
      shipment:[
        {
          shipmentName:'Internal kurir-Pengambilan',
          shipmentPrice:12000,
        },
        {
          shipmentName:'Internal kurir-Pengantaran',
          shipmentPrice:12000,
        }
      ]
          },
            { orderID: '120823134', latitude:-6.1330, longitude:106.8267, shipmentStatus: 'AKTIF', orderStatus: 'GoToPickupPoint',  pickupTime: '15.00', address: 'Agro Plaza, Jl. H.R. Rasuna Said Kav. X 2 - 1, Kuningan Timur RT.7/RW.4, Setiabudi, Kuningan Timur, RT.7/RW.4, Kuningan Tim., Setia Budi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950',

              orderItems:[
        {
          itemName:'Sepatu Nylon/Rubber/Canvas',
          washingMethodName:'Deep Clean',
          quantity:4,
          unitName:'Pair',
          addedService:[
            {
              serviceName:'Unyellowing',
            },
            {
              serviceName:'Reglue',
            }
          ],
          unitPrice:24000,
        },
        {
          itemName:'Bed Cover Besar',
          washingMethodName:'Hand Clean',
          quantity:2,
          unitName:'Pcs',
          addedService:[],
          unitPrice:72000,
        },
        {
          itemName:'Cuci, Lipat, Gosok',
          washingMethodName:'Wet Clean',
          quantity:6,
          unitName:'Kg',
          addedService:[],
          unitPrice:12000,
        },
      ],
      promo:[
        {
          promoName:'Promo code #WEEKENDSERU',
          type:'order',
          quantity:1,
          unitPrice:-20000,
        },
        {
          promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
          type:'shipment',
          quantity:1,
          unitPrice:-20000,
        },
      ],
      shipment:[
        {
          shipmentName:'Internal kurir-Pengambilan',
          shipmentPrice:12000,
        },
        {
          shipmentName:'Internal kurir-Pengantaran',
          shipmentPrice:12000,
        }
      ]
          },
          ]          
        },
        {
          timeSlot: '16.00',
          dataSlot: [
            { orderID: '120823134', shipmentStatus: 'TELAT', orderStatus: 'ReadyToBeDelivered',  pickupTime: '15.00', address: 'Agro Plaza, Jl. H.R. Rasuna Said Kav. X 2 - 1, Kuningan Timur RT.7/RW.4, Setiabudi, Kuningan Timur, RT.7/RW.4, Kuningan Tim., Setia Budi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950',
                      orderItems:[
        {
          itemName:'Sepatu Nylon/Rubber/Canvas',
          washingMethodName:'Deep Clean',
          quantity:4,
          unitName:'Pair',
          addedService:[
            {
              serviceName:'Unyellowing',
            },
            {
              serviceName:'Reglue',
            }
          ],
          unitPrice:24000,
        },
        {
          itemName:'Bed Cover Besar',
          washingMethodName:'Hand Clean',
          quantity:2,
          unitName:'Pcs',
          addedService:[],
          unitPrice:72000,
        },
        {
          itemName:'Cuci, Lipat, Gosok',
          washingMethodName:'Wet Clean',
          quantity:6,
          unitName:'Kg',
          addedService:[],
          unitPrice:12000,
        },
      ],
      promo:[
        {
          promoName:'Promo code #WEEKENDSERU',
          type:'order',
          quantity:1,
          unitPrice:-20000,
        },
        {
          promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
          type:'shipment',
          quantity:1,
          unitPrice:-20000,
        },
      ],
      shipment:[
        {
          shipmentName:'Internal kurir-Pengambilan',
          shipmentPrice:12000,
        },
        {
          shipmentName:'Internal kurir-Pengantaran',
          shipmentPrice:12000,
        }
      ]
            },
            { orderID: '120823134', shipmentStatus: 'NONE', orderStatus: 'GoToPickupPoint',  pickupTime: '15.00', address: 'Agro Plaza, Jl. H.R. Rasuna Said Kav. X 2 - 1, Kuningan Timur RT.7/RW.4, Setiabudi, Kuningan Timur, RT.7/RW.4, Kuningan Tim., Setia Budi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950',
                  orderItems:[
        {
          itemName:'Sepatu Nylon/Rubber/Canvas',
          washingMethodName:'Deep Clean',
          quantity:4,
          unitName:'Pair',
          addedService:[
            {
              serviceName:'Unyellowing',
            },
            {
              serviceName:'Reglue',
            }
          ],
          unitPrice:24000,
        },
        {
          itemName:'Bed Cover Besar',
          washingMethodName:'Hand Clean',
          quantity:2,
          unitName:'Pcs',
          addedService:[],
          unitPrice:72000,
        },
        {
          itemName:'Cuci, Lipat, Gosok',
          washingMethodName:'Wet Clean',
          quantity:6,
          unitName:'Kg',
          addedService:[],
          unitPrice:12000,
        },
      ],
      promo:[
        {
          promoName:'Promo code #WEEKENDSERU',
          type:'order',
          quantity:1,
          unitPrice:-20000,
        },
        {
          promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
          type:'shipment',
          quantity:1,
          unitPrice:-20000,
        },
      ],
      shipment:[
        {
          shipmentName:'Internal kurir-Pengambilan',
          shipmentPrice:12000,
        },
        {
          shipmentName:'Internal kurir-Pengantaran',
          shipmentPrice:12000,
        }
      ]
              
          },
          ]          
        },
        {
          timeSlot: '18.00',
          dataSlot: [
            { orderID: '120823134', shipmentStatus: 'NONE', orderStatus: 'GoToPickupPoint',  pickupTime: '15.00', address: 'Agro Plaza, Jl. H.R. Rasuna Said Kav. X 2 - 1, Kuningan Timur RT.7/RW.4, Setiabudi, Kuningan Timur, RT.7/RW.4, Kuningan Tim., Setia Budi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950',
                  orderItems:[
        {
          itemName:'Sepatu Nylon/Rubber/Canvas',
          washingMethodName:'Deep Clean',
          quantity:4,
          unitName:'Pair',
          addedService:[
            {
              serviceName:'Unyellowing',
            },
            {
              serviceName:'Reglue',
            }
          ],
          unitPrice:24000,
        },
        {
          itemName:'Bed Cover Besar',
          washingMethodName:'Hand Clean',
          quantity:2,
          unitName:'Pcs',
          addedService:[],
          unitPrice:72000,
        },
        {
          itemName:'Cuci, Lipat, Gosok',
          washingMethodName:'Wet Clean',
          quantity:6,
          unitName:'Kg',
          addedService:[],
          unitPrice:12000,
        },
      ],
      promo:[
        {
          promoName:'Promo code #WEEKENDSERU',
          type:'order',
          quantity:1,
          unitPrice:-20000,
        },
        {
          promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
          type:'shipment',
          quantity:1,
          unitPrice:-20000,
        },
      ],
      shipment:[
        {
          shipmentName:'Internal kurir-Pengambilan',
          shipmentPrice:12000,
        },
        {
          shipmentName:'Internal kurir-Pengantaran',
          shipmentPrice:12000,
        }
      ]
          },
            { orderID: '120823134', shipmentStatus: 'NONE', orderStatus: 'ReadyToBeDelivered',  pickupTime: '15.00', address: 'Agro Plaza, Jl. H.R. Rasuna Said Kav. X 2 - 1, Kuningan Timur RT.7/RW.4, Setiabudi, Kuningan Timur, RT.7/RW.4, Kuningan Tim., Setia Budi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950',
                orderItems:[
        {
          itemName:'Sepatu Nylon/Rubber/Canvas',
          washingMethodName:'Deep Clean',
          quantity:4,
          unitName:'Pair',
          addedService:[
            {
              serviceName:'Unyellowing',
            },
            {
              serviceName:'Reglue',
            }
          ],
          unitPrice:24000,
        },
        {
          itemName:'Bed Cover Besar',
          washingMethodName:'Hand Clean',
          quantity:2,
          unitName:'Pcs',
          addedService:[],
          unitPrice:72000,
        },
        {
          itemName:'Cuci, Lipat, Gosok',
          washingMethodName:'Wet Clean',
          quantity:6,
          unitName:'Kg',
          addedService:[],
          unitPrice:12000,
        },
      ],
      promo:[
        {
          promoName:'Promo code #WEEKENDSERU',
          type:'order',
          quantity:1,
          unitPrice:-20000,
        },
        {
          promoName:'Promo outlet Laundry Klin : Pengantaran gratis Rp 20.000',
          type:'shipment',
          quantity:1,
          unitPrice:-20000,
        },
      ],
      shipment:[
        {
          shipmentName:'Internal kurir-Pengambilan',
          shipmentPrice:12000,
        },
        {
          shipmentName:'Internal kurir-Pengantaran',
          shipmentPrice:12000,
        }
      ]
              
          },
          ]          
        } 
      ]
    },
  ]
};

