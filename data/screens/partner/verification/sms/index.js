import React, { Component } from 'react';
import { 	
        Platform,
        Modal, 
        View,
        Text,
        Dimensions,
        Alert,
        Image,
        TouchableHighlight,
        ActivityIndicator,
  		} from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import ResolveAssetSource from 'resolveAssetSource';

import ResponsiveImage from 'react-native-responsive-image';

import TimerMixin from 'react-timer-mixin';

class SMSVerification extends Component {
    constructor(props){
        super(props);
        this.baseTimeTimer=60
        this.state = {
            isVisible: false,
            isAlreadySendCode: null,
            isPressed: this.props.isPressed,
            isLoading:false,
            validationVerification:'',
            timer:this.baseTimeTimer,
            mobile_verification_code:'',
            onSuccess:this.props.onSuccess,
            image:{
                arrow_form:{
                    width:0,
                    height:0,
                    multiplierWidth:0.22,
                    multiplierHeight:0.22,
                    url:require('./../../../../image/verification/arrow_form.png'),
                },
                box:{
                    width:0,
                    height:0,
                    multiplierWidth:0.18,
                    multiplierHeight:0.22,
                    url:require('./../../../../image/verification/box.png'),
                },
                sms_icon:{
                    width:0,
                    height:0,
                    multiplierWidth:0.2,
                    multiplierHeight:0.2,
                    url:require('./../../../../image/verification/sms_icon.png'),
                }
            },
            data:this.props.data ? this.props.data : null,
        };
    }

    componentWillMount(){
        this._getImageSize();
    }

    _getImageSize(){
        var image = this.state.image;

        var image_arrow_form = ResolveAssetSource(this.state.image.arrow_form.url);

        image.arrow_form.width = image_arrow_form.width;
        image.arrow_form.height = image_arrow_form.height;

        var image_box= ResolveAssetSource(this.state.image.box.url);

        image.box.width = image_box.width;
        image.box.height = image_box.height;

        var image_sms_icon = ResolveAssetSource(this.state.image.sms_icon.url);

        image.sms_icon.width = image_sms_icon.width;
        image.sms_icon.height = image_sms_icon.height;
        
        this.setState({image});
    }

    componentWillUnmount() {
        TimerMixin.clearInterval(this.intervalCheckTimer);
    }

    _getPhoneNumber(){
        if (this.props._getDataUserState()) {
             if (this.props._getDataUserState().role == 'partner') {
                 return this.props._getDataUserState().user.partner.mobile_phone;
             }
             else{
                 return this.props._getDataUserState().user.staff.mobile_phone;
             }
        }
        
    }

    _setIsVisible(isVisible){
        this.setState({isVisible:isVisible});
    }

    _setData(data){
        this.setState({data}, () => {
            this._getPhoneNumber();
        });
    }

    _verifyMobileCode(code){
        this.setState({isLoading:true, validationVerification:''}, () =>{
            var data = JSON.parse(JSON.stringify(this.props._getDataLoginState()));
            data.mobile_verification_code = code;
            Verification._submitCodeMobile(data)
            .then((response) => {
                console.log(response);
                this.setState({isLoading:false}, () => {
                    if (response.success == 'yeah') {
                        this.setState({isVisible:false}, () => {
                            this.props._onSuccess();
                            alert('verifikasi telah berhasil! terima kasih');
                        })
                    }
                    else{
                        this.setState({validationVerification:response.message});
                    }
                })
            })
            .catch((err) => {
                console.log(err);
                this.setState({isLoading:false}, () => {
                    alert('connection error, try again');
                })
            });
        });
    }

    _setIsAlreadySendCode(isAlreadySendCode){
        this.setState({isAlreadySendCode:isAlreadySendCode}, () => {
            if (isAlreadySendCode == 'yeah') {
                this.setState({isPressed:'yeah'})
                this.intervalCheckTimer = TimerMixin.setInterval(
                    () => {
                        if (this.state.timer > 1) {
                            var timer = this.state.timer;
                            this.setState({timer:timer - 1});
                        }
                        else{
                            TimerMixin.clearInterval(this.intervalCheckTimer);
                            this.setState({isPressed:false, timer:this.baseTimeTimer});
                        }
                     }
                ,1000);
            }
            else{

            }
        });
    }

    _onPressVerifyCode(){
        this._verifyMobileCode(this.state.mobile_verification_code);
    }

    _onPressResendSMSActivation(){
        this.setState({isAlreadySendCode:'yeah'});
            this.setState({isPressed:true},() => {
            Verification._sendCodeMobile(this.props._getDataLoginState())
                .then((response) => {
                    if (response.success == 'yeah') {
                        this.intervalCheckTimer = TimerMixin.setInterval(
                            () => {
                                if (this.state.timer > 1) {
                                    var timer = this.state.timer;
                                    this.setState({timer:timer - 1});
                                }
                                else{
                                    TimerMixin.clearInterval(this.intervalCheckTimer);
                                    this.setState({isPressed:false, timer:this.baseTimeTimer});
                                }
                            }
                        ,1000);
                    }
                })
                .catch();
            });
    }

    _renderResendSmsVerificationCode(){
        if (this.state.isPressed) {
            return (
                <View>
                    <TextWrapper type='content' style={{fontWeight:'bold', color:'gray'}}>
                        Kirim ulang kode pada {this.state.timer}
                    </TextWrapper>
                </View>
            );
        }
        else{
            if (this.state.isAlreadySendCode == 'yeah') {
                return (
                <TouchableHighlight onPress={this._onPressResendSMSActivation.bind(this)}>
                    <View>
                        <TextWrapper type='subtitle' style={{fontWeight:'bold', color:'#33ACE0'}}>
                            Kirim Ulang Kode Aktivasi
                        </TextWrapper>
                    </View>
                </TouchableHighlight>
                );
            }
            else{
                return (
                <TouchableHighlight onPress={this._onPressResendSMSActivation.bind(this)}>
                    <View>
                        <TextWrapper type='subtitle' style={{fontWeight:'bold', color:'#33ACE0'}}>
                            Kirim SMS Kode Aktivasi
                        </TextWrapper>
                    </View>
                </TouchableHighlight>
                );
            }
        }
    }

    _setVisible(isVisible){
        this.setState({isVisible});
    }

    render(){
        return(
            <View>
                <Modal
                    visible={this.state.isVisible}
                    animationType={'fade'}
                    onRequestClose={()=>{}}
                    transparent={true}
                >
                        <KeyboardAwareScrollView 
                          contentContainerStyle={{flex:1, backgroundColor:'rgba(0, 0, 0, 0.6)', justifyContent:'center', alignItems:'center', paddingTop:50, paddingBottom:100}}
                        >

                            <ResponsiveImage
                                source={this.state.image.box.url}
                                initWidth={this.state.image.box.width * this.state.image.box.multiplierWidth}
                                initHeight={this.state.image.box.height * this.state.image.box.multiplierHeight}
                            >
                                <View style={{flexDirection:'row', flex:1.5, justifyContent:'center'}} >
                                    <ResponsiveImage
                                        source={this.state.image.sms_icon.url}
                                        initWidth={this.state.image.sms_icon.width * this.state.image.sms_icon.multiplierWidth}
                                        initHeight={this.state.image.sms_icon.height * this.state.image.sms_icon.multiplierHeight}
                                        style={{margin:20}}
                                    />

                                    <View style={{flex:1, marginTop:20}}>
                                        <TextWrapper type='content' style={{fontWeight:'bold', color:'#33ACE0'}}>
                                            Verifikasi Nomor Handphone
                                        </TextWrapper>

                                        {
                                            this.state.isAlreadySendCode == 'yeah' ? 
                                                <TextWrapper fontSize={6} style={{color:'gray', marginTop:10}}>
                                                    masukan kode yang baru saja dikirimkan ke nomer handphone anda 
                                                    <TextWrapper fontSize={6} style={{color:'gray', fontWeight:'bold'}}>
                                                    {' ' + this._getPhoneNumber()}
                                                    </TextWrapper>
                                                </TextWrapper>
                                            :
                                                null
                                        }
                                        
                                    </View>
                                </View>

                                <View style={{flex:1, justifyContent:'flex-start'}}>
                                    {this.state.isAlreadySendCode == 'yeah' ? 
                                        <Form.InputField
                                          keyboardType={'default'}
                                          ref='mobile_verification_code'
                                          placeholder='Masukan kode disini'
                                          placeholderTextColor='gray'
                                          onValueChange={(value) => {this.setState({mobile_verification_code:value})}}
                                          containerStyle={{marginLeft:Dimensions.get('window').width * 0.15, marginRight:Dimensions.get('window').width * 0.15}}
                                          iconRight={
                                            <TouchableHighlight size={25} onPress={this._onPressVerifyCode.bind(this)} style={{right:10, }}>
                                                <View>
                                                    <ResponsiveImage
                                                        source={this.state.image.arrow_form.url}
                                                        initWidth={this.state.image.arrow_form.width * this.state.image.arrow_form.multiplierWidth}
                                                        initHeight={this.state.image.arrow_form.height * this.state.image.arrow_form.multiplierHeight}
                                                    />
                                                </View>
                                            </TouchableHighlight>
                                            }
                                        />
                                        :
                                        null
                                    }

                                    

                                    <View style={{marginTop:10, justifyContent:'center', alignItems:'center', alignSelf:'stretch'}}>
                                        {
                                            this.state.isLoading ? 
                                                <ActivityIndicator
                                                  animating={this.state.isLoading}
                                                  style={{}}
                                                  color='black'
                                                />
                                            :

                                            <TextWrapper type='base' style={{color:'gray'}}> 
                                                {this.state.validationVerification}
                                            </TextWrapper>
                                            }
                                    </View>
                                </View>



                                <View style={{flex:0.87, justifyContent:'center', alignItems:'center'}}>
                                    {this._renderResendSmsVerificationCode()}
                                </View>
                            </ResponsiveImage>
                        </KeyboardAwareScrollView>
                </Modal>
            </View>
        );
    }
}

module.exports = {SMSVerification};