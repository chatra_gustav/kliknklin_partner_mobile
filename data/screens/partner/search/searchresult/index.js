import React, { Component } from 'react';
import {View, ScrollView,TouchableOpacity, Dimensions, Alert} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import TimerMixin from 'react-timer-mixin';

export class SearchResult extends Component {
	constructor(props) {
        super(props);
        this.state = {
        	image:{
				detail_button:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../image/searchresult/detail_button.png'),
				},
				report_accept_button:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../image/searchresult/report_accept_button.png'),
				},
				report_reject_button:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../image/searchresult/report_reject_button.png'),
				},
				report_delivered_button:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../image/searchresult/report_delivered_button.png'),
				},
				report_checkout_button:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../image/searchresult/report_checkout_button.png'),
				},
				report_finish_button:{
					width:0,
					height:0,
					multiplierWidth:0.5,
					multiplierHeight:0.5,
					url:require('./../../../../image/searchresult/report_finish_button.png'),
				},
        	},
        	alert:{
        		failOpenForgetPassword:'Something went wrong, try again or contact us for more information',
        	},
            string:{

            },
            data:[],
            showVisibility:{
            	incoming:false,
            	onprogress:false,
            	pickup:false,
            	delivery:false,
            },
            secondUserNotType:0,
            limitSecondUserNotType:1,
            isFinishSearch:false,
            isQueryEmpty:false,
        };  
        this.checkIsTyping = null;
    }

	componentWillMount(){
		this._getImageSize();
		this._configSearchAllResult(this.props.search)
	}

	componentDidUpdate(){
		if (this.state.secondUserNotType == this.state.limitSecondUserNotType) {
			this.setState({secondUserNotType:0});
			
			if (this.checkIsTyping) {
				TimerMixin.clearInterval(this.checkIsTyping);
				this.checkIsTyping = null;
			}

			this._searchAllResult();
		}
	}

	_configSearchAllResult(search){
		
		this.setState({isFinishSearch:false});

		var configSearch = JSON.parse(JSON.stringify(this.props._getDataLoginState()));
		configSearch.search_query = search.query;
		configSearch.search_by = search.by;

		if (this.checkIsTyping) {
			TimerMixin.clearInterval(this.checkIsTyping);
		}

		this.setState({secondUserNotType : 0, configSearch}, () => {

			if (this.state.configSearch.search_query == '') {
				var showVisibility = this.state.showVisibility;
				showVisibility.incoming = false;
				showVisibility.onprogress = false;
				showVisibility.pickup = false;
				showVisibility.delivery = false;

				if (this.checkIsTyping) {
					TimerMixin.clearInterval(this.checkIsTyping);
					this.checkIsTyping = null;
				}

				this.setState({isQueryEmpty:true, isFinishSearch:true, showVisibility});
			}
			else{
				this.checkIsTyping = TimerMixin.setInterval(() => {
					var secondUserNotType = this.state.secondUserNotType + 1;
					
					this.setState({secondUserNotType});
				}, 1000);
			}
			
		});
	}

	_searchAllResult(){
		this.setState({isQueryEmpty:false}, () => {
			Order._searchAllOrder(this.state.configSearch)
				.then((response) => {
					if (response.success == 'yeah') {
						this._setData(response.data);
					}
				})
				.catch((error) => {
					console.log(error);
				})
			});
	}

	_getImageSize(){
		var image = this.state.image; 

		var image_detail_button = ResolveAssetSource(this.state.image.detail_button.url);

		image.detail_button.width = image_detail_button.width;
		image.detail_button.height = image_detail_button.height;

		var image_report_accept_button = ResolveAssetSource(this.state.image.report_accept_button.url);

		image.report_accept_button.width = image_report_accept_button.width;
		image.report_accept_button.height = image_report_accept_button.height;

		var image_report_reject_button = ResolveAssetSource(this.state.image.report_reject_button.url);

		image.report_reject_button.width = image_report_reject_button.width;
		image.report_reject_button.height = image_report_reject_button.height;

		var image_report_delivered_button = ResolveAssetSource(this.state.image.report_delivered_button.url);

		image.report_delivered_button.width = image_report_delivered_button.width;
		image.report_delivered_button.height = image_report_delivered_button.height;

		var image_report_checkout_button = ResolveAssetSource(this.state.image.report_checkout_button.url);

		image.report_checkout_button.width = image_report_checkout_button.width;
		image.report_checkout_button.height = image_report_checkout_button.height;

		var image_report_finish_button = ResolveAssetSource(this.state.image.report_finish_button.url);

		image.report_finish_button.width = image_report_finish_button.width;
		image.report_finish_button.height = image_report_finish_button.height;

      	this.setState({image});
	}

	_setData(data){
		this.setState({data, isFinishSearch:true}, () => {
			this._countData(data);
		});
	}

	_countData(data){
		var incomingCount = 0;
		var onprogressCount = 0;
		var pickupCount = 0;
		var deliveryCount = 0;
		for(var index in data){
			if (data[index].order_status_id == 99) {
				incomingCount++;
			}
			else if (data[index].order_status_id == 1) {
				pickupCount++;
			}
			else if (data[index].order_status_id == 2 || data[index].order_status_id == 3) {
				onprogressCount++;
			}
			else if (data[index].order_status_id == 4) {
				deliveryCount++;
			}
		}
		
		var showVisibility = this.state.showVisibility;

		if (incomingCount == 0) {
			showVisibility.incoming = false;
		}
		else{
			showVisibility.incoming = true;
		}

		if (onprogressCount == 0) {
			showVisibility.onprogress = false;
		}
		else{
			showVisibility.onprogress = true;
		}

		if (pickupCount == 0) {
			showVisibility.pickup = false;
		}
		else{
			showVisibility.pickup = true;
		}

		if (deliveryCount == 0) {
			showVisibility.delivery = false;
		}
		else{
			showVisibility.delivery = true;
		}

		this.setState({showVisibility});
	}

	_markOrderLate(isLate){
		if (isLate) {
			return(
				<View style={{flex:0.4, justifyContent:'center'}}>
					<View style={{width:65, backgroundColor:'#ed1c24', borderRadius:5, alignItems:'center' }}>
	            		<TextWrapper type={'base'} style={{fontWeight:'bold', color:'white'}}>
		            		{'Telat'}
		            	</TextWrapper>
	            	</View>
				</View>
			)
		}
		return null;
	}

	_markOrderImportant(isImportant){
		if (isImportant) {
			return(
				<View style={{flex:0.4, justifyContent:'center'}}>
					<View style={{width:65, backgroundColor:'#e79e2d', borderRadius:5, alignItems:'center' }}>
	            		<TextWrapper type={'base'} style={{fontWeight:'bold', color:'white'}}>
		            		{'Penting'}
		            	</TextWrapper>
	            	</View>
				</View>
			)
		}
		return null;
	}

	_getEmptyContent(){
 		return(
			<View style={{alignItems:'center', top:Dimensions.get('window').height * 0.25}}>
				<TextWrapper type={'content'} style={{color:'darkslategray'}}>
					{'Tidak ada data order yang ditemukan'}
				</TextWrapper>
			</View>
 		);
 	}

 	_afterSearch(data){
		this._configSearchAllResult(data);
	}

	_renderContent(){
		if (this.checkIsTyping == null && this.state.isFinishSearch) {
			if ((this.state.showVisibility.incoming || this.state.showVisibility.onprogress ||
				this.state.showVisibility.pickup || this.state.showVisibility.delivery )&& !this.state.isQueryEmpty
			) {
				return(
					<ScrollView>
						{
							this.state.showVisibility.incoming ? 
							<View style={{marginTop:7.5}}>
								<SearchResultIncoming 
									data={this.state.data}
									image={this.state.image}
									navigator={this.props.navigator}
									_setData={this._setData.bind(this)}
									_markOrderLate={this._markOrderLate.bind(this)}
									_markOrderImportant={this._markOrderImportant.bind(this)}
								/>
							</View>
							:
							null
						}

						{
							this.state.showVisibility.pickup ? 
							<View style={{marginTop:7.5,backgroundColor:'red'}}>
								<SearchResultPickUp
									data={this.state.data}
									image={this.state.image}
									navigator={this.props.navigator}
									_setData={this._setData.bind(this)}
									_setShouldReloadSummary={this.props._setShouldReloadSummary}
									_getDataLoginState={this.props._getDataLoginState}
									_showModal={this.props._showModal}
									_markOrderLate={this._markOrderLate.bind(this)}
									_markOrderImportant={this._markOrderImportant.bind(this)}
									_fetch={this.props._fetch}
									_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
								/>
							</View>
							:
							null
						}
						
						{
							this.state.showVisibility.onprogress ? 
							<View style={{marginTop:7.5}}>
								<SearchResultOnProgress
									data={this.state.data}
									image={this.state.image}
									navigator={this.props.navigator}
									_setData={this._setData.bind(this)}
									_setShouldReloadSummary={this.props._setShouldReloadSummary}
									_getDataLoginState={this.props._getDataLoginState}
									_showModal={this.props._showModal}
									_markOrderLate={this._markOrderLate.bind(this)}
									_markOrderImportant={this._markOrderImportant.bind(this)}
									_fetch={this.props._fetch}
									_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
								/>
							</View>
							:
							null
						}

						{
							this.state.showVisibility.delivery ? 
							<View style={{marginTop:7.5}}>
								<SearchResultDelivery
									data={this.state.data}
									image={this.state.image}
									navigator={this.props.navigator}
									_setData={this._setData.bind(this)}
									_setShouldReloadSummary={this.props._setShouldReloadSummary}
									_getDataLoginState={this.props._getDataLoginState}
									_showModal={this.props._showModal}
									_markOrderLate={this._markOrderLate.bind(this)}
									_markOrderImportant={this._markOrderImportant.bind(this)}
									_fetch={this.props._fetch}
									_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
								/>
							</View>
							:
							null
						}
					</ScrollView>
				);
			}
			else{
				return this._getEmptyContent();
			}
		}
		else{
			return this._loadingData();
		}
		
	}

	_loadingData(){
		return(
			<View style={{alignItems:'center', top:Dimensions.get('window').height * 0.25}}>
				<TextWrapper type={'content'} style={{color:'darkslategray'}}>
					{'Memuat data...'}
				</TextWrapper>
			</View>
		)
	}

	render(){
		return(
			<View style={{flex:1}}>
				<SearchBar 
					isInSearchResult={true}
					_getNavigator={() => {
						return this.props.navigator;
					}}
					search={this.props.search}
					_afterSearch={this._afterSearch.bind(this)}
				/>
				
				{this._renderContent()}
			</View>
		);
	}
}

class SearchResultIncoming extends Component{
	constructor(props) {
        super(props);
        this.state = {
        	data:this.props.data ? this.props.data : [],
        	_setData:this.props._setData ? this.props._setData : null,
        	image:this.props.image ? this.props.image : null,
        }
    }

	render(){
		return(
			<View style={{flex:1, backgroundColor:'white'}}>
				<View style={{margin:10}}>
					<View>
						<TextWrapper type={'base'} style={{color:'darkslategray', fontWeight:'bold'}}>
							{'Hasil Pencarian Incoming'}
						</TextWrapper>
					</View>
					{this.props.data.map((section, key) => (
						section.activeTab == 'incoming' ?
						<View key={key} style={{marginTop:5, borderTopWidth:1, borderTopColor:'lightgray'}}>
			          		<View>
			          			<View style={{flexDirection:'row', marginTop:7.5}}>
					            	<View style={{flex:0.35}}>
					            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Waktu Pickup'}</TextWrapper>
					            	</View>
					            	<View style={{flex:0.05}}>
					            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
					            	</View>
					            	<View style={{flex:0.6}}>
										<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.pickup_time}</TextWrapper>
					            	</View>
					            </View>
					            <View style={{flexDirection:'row', marginTop:5}}>
					            	<View style={{flex:0.35}}>
					            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Waktu Delivery'}</TextWrapper>
					            	</View>
					            	<View style={{flex:0.05}}>
					            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
					            	</View>
					            	<View style={{flex:0.6}}>
										<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.delivery_time}</TextWrapper>
					            	</View>
					            </View>
					            <View style={{flexDirection:'row', marginTop:5}}>
					            	<View style={{flex:0.35}}>
					            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Alamat'}</TextWrapper>
					            	</View>
					            	<View style={{flex:0.05}}>
					            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
					            	</View>
					            	<View style={{flex:0.6}}>
										<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.pickup_place}</TextWrapper>
					            	</View>
					            </View>
					            <View style={{flexDirection:'row', marginTop:20, marginBottom:15, alignItems:'center', justifyContent:'center'}}>
					            	<View style={{flex:0.45, alignItems:'flex-end'}}>
										<ButtonRejectList.Incoming
											data={this.state.data[section.id - 1]}
											_setData={this.state._setData}
										/>
									</View>
									<View style={{flex:0.1}}>
										
									</View>
									<View style={{flex:0.45, alignItems:'flex-start'}}>
										<ButtonConfirmList.Incoming
											data={this.state.data[section.id - 1]}
											_setData={this.state._setData}
										/>
									</View>
					            </View>
				            </View>
				        </View>
				        :
				        null
						
				     ))}
				</View>
			</View>
		);
	}
}

class SearchResultPickUp extends Component{
	constructor(props) {
        super(props);
        this.state = {
        	data:this.props.data ? this.props.data : [],
        	_setData:this.props._setData ? this.props._setData : null,
        	image:this.props.image ? this.props.image : null,
        }
    }

    componentDidMount(){


    }

    _onPressDetail(data, loginState){
    	this.props.navigator.push({
			id:'OrderDetail',
			title: 'Detail Pickup',
			contentType: 'pickup',
			data:data,
			loginState:loginState,
			_afterSubmit:this._afterSubmitPickup.bind(this),
    	});
    }

    _afterSubmitPickup(callback = () => {}){
    	this.props._setShouldReloadSummary(true);

    	this.props.navigator.resetTo({
			id:'Dashboard',
			title:'Dashboard',
		});

    	this.props._showModal(false,'', () => {
			alert('order telah diambil, silahkan lihat tab on progress untuk detail');
			//if (callback) callback();
    	})
    }

    _formatDate(date){
    	date = new Date(date.replace(/-/g,"/"));
	  	return DateFormat(date, 'fullDate');
	}

	render(){
		return(
			<View style={{flex:1, backgroundColor:'white'}}>
				<View style={{margin:10}}>
					<View>
						<TextWrapper type={'base'} style={{color:'darkslategray', fontWeight:'bold'}}>
							{'Hasil Pencarian Pickup'}
						</TextWrapper>
					</View>
					{this.props.data.map((section, key) => {
						var loginState= JSON.parse(JSON.stringify(this.props._getDataLoginState()));
						loginState.order_id = section.id;

						if (section.order_status_id == 1) {
						return(
							<TouchableOpacity key={key} onPress={this._onPressDetail.bind(this, section, loginState)}>
						         <View key={key} style={{marginTop:5, borderTopWidth:1, borderTopColor:'lightgray'}}>
					          		<View style={{margin:10}}>
							            <View style={{flexDirection:'row', marginTop:5}}>
							            	<View style={{flex:0.85, flexDirection:'row'}}>
							            		<View style={{flex:0.6}}>
							            			<TextWrapper type={'base'} style={{fontWeight:'normal', color:'darkslategray'}}>
									            		Order No. 
										            	<TextWrapper type={'content'} style={{fontWeight:'bold'}}>
										            		{section.order_hash}
										            	</TextWrapper>
									            	</TextWrapper>
							            		</View>
								            	
												{this.props._markOrderLate(section.is_late)}

												{this.props._markOrderImportant(section.important)}
								            </View>
							            	<View style={{flex:0.15, alignItems:'flex-end'}}>
							            		<ResponsiveImage
													source={this.state.image.detail_button.url}
													initWidth={this.state.image.detail_button.width * this.state.image.detail_button.multiplierWidth}
													initHeight={this.state.image.detail_button.height * this.state.image.detail_button.multiplierHeight}
												/>
							            	</View>
							            </View>
							            <View style={{flexDirection:'row', marginTop:5}}>
							            	<View style={{flex:0.2}}>
							            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Nama'}</TextWrapper>
							            	</View>
							            	<View style={{flex:0.05}}>
							            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
							            	</View>
							            	<View style={{flex:0.75}}>
												<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.customer.first_name}</TextWrapper>
							            	</View>
							            </View>
							             <View style={{flexDirection:'row', marginTop:5}}>
							            	<View style={{flex:0.2}}>
							            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Tanggal'}</TextWrapper>
							            	</View>
							            	<View style={{flex:0.05}}>
							            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
							            	</View>
							            	<View style={{flex:0.75}}>
												<TextWrapper type={'base'} style={{color:'darkslategray'}}>{this._formatDate(section.pickup_date)}</TextWrapper>
							            	</View>
							            </View>
							            <View style={{flexDirection:'row', marginTop:5}}>
							            	<View style={{flex:0.2}}>
							            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Waktu'}</TextWrapper>
							            	</View>
							            	<View style={{flex:0.05}}>
							            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
							            	</View>
							            	<View style={{flex:0.75}}>
												<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.pickuptime.info + ' WIB'}</TextWrapper>
							            	</View>
							            </View>
							            <View style={{flexDirection:'row', marginTop:5, marginBottom:5}}>
							            	<View style={{flex:0.2}}>
							            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Alamat'}</TextWrapper>
							            	</View>
							            	<View style={{flex:0.05}}>
							            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
							            	</View>
							            	<View style={{flex:0.75}}>
												<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.pickup_address}</TextWrapper>
							            	</View>
							            </View>
							            <View style={{flexDirection:'row', marginTop:15, marginBottom:5, alignItems:'center', justifyContent:'center'}}>
											<ButtonConfirmList.Pickup
												data={loginState}
												_setData={this.state._setData}
												_showModal={this.props._showModal}
												_callback={this._afterSubmitPickup.bind(this)}
												_fetch={this.props._fetch}
												_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
											/>
							            </View>
						            </View>
						        </View>
						   </TouchableOpacity>
						  	
					    )}})}
				</View>
			</View>
		);
	}
}

class SearchResultOnProgress extends Component{
	constructor(props) {
        super(props);
        this.state = {
        	data:this.props.data ? this.props.data : [],
        	_setData:this.props._setData ? this.props._setData : null,
        	image:this.props.image ? this.props.image : null,
        }
    }

    _onPressDetail(data, loginState){
    	data = JSON.parse(JSON.stringify(data));
    	data.orderstatus = {};
    	data.orderstatus.id = data.order_status_id;

    	this.props.navigator.push({
			id:'OrderDetail',
			title: 'Detail On Progress',
			contentType: 'onprogress',
			data:data,
			loginState:loginState,
			_afterSubmit:this._afterSubmitOnProgress.bind(this),
    	});
    }

    _afterSubmitOnProgress(callback = () => {}){
    	this.props._setShouldReloadSummary(true);

    	this.props.navigator.resetTo({
			id:'Dashboard',
			title:'Dashboard',
		});

    	this.props._showModal(false,'', () => {
			alert('order telah ditandai selesai dan siap diantar ke konsumen');
			//if (callback) callback();
    	})
    }


	_formatDate(date){
    	date = new Date(date.replace(/-/g,"/"));
	  	return DateFormat(date, 'fullDate');
	}

	_formatTime(date){
		date = new Date(date.replace(/-/g,"/"));
	  	return DateFormat(date, 'simpleTime');
	}

	render(){
		return(
			<View style={{flex:1, backgroundColor:'white'}}>
				<View style={{margin:10}}>
					<View>
						<TextWrapper type={'base'} style={{color:'darkslategray', fontWeight:'bold'}}>
							{'Hasil Pencarian On Progress'}
						</TextWrapper>
					</View>
					{this.props.data.map((section, key) => { 
						var loginState = JSON.parse(JSON.stringify(this.props._getDataLoginState()));
						loginState.order_id = section.id;

						if (section.order_status_id == 2 || section.order_status_id == 3) {
						return(
						<TouchableOpacity key={key} onPress={this._onPressDetail.bind(this, section, loginState)}>
					        <View key={key} style={{marginTop:5, borderTopWidth:1, borderTopColor:'lightgray'}}>
				          		<View>
						            <View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.85, flexDirection:'row'}}>
						            		<View style={{flex:0.6}}>
						            			<TextWrapper type={'base'} style={{fontWeight:'normal', color:'darkslategray'}}>
								            		Order No. 
									            	<TextWrapper type={'content'} style={{fontWeight:'bold'}}>
									            		{section.order_hash}
									            	</TextWrapper>
								            	</TextWrapper>
						            		</View>

							            	{this.props._markOrderLate(section.is_late)}

											{this.props._markOrderImportant(section.important)}
							            </View>
						            	<View style={{flex:0.15, alignItems:'flex-end'}}>
						            		<ResponsiveImage
												source={this.state.image.detail_button.url}
												initWidth={this.state.image.detail_button.width * this.state.image.detail_button.multiplierWidth}
												initHeight={this.state.image.detail_button.height * this.state.image.detail_button.multiplierHeight}
											/>
						            	</View>
						            </View>
						            <View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.3}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Konsumen'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.65}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.customer.first_name}</TextWrapper>
						            	</View>
						            </View>
						            <View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.3}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Batas Lapor'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.65}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{this._formatDate(section.report_time_limit.date) + '\n' +  this._formatTime(section.report_time_limit.date) + ' WIB'}</TextWrapper>
						            	</View>
						            </View>
						            <View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.3}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Berat'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.65}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.item_weight + ' Kg'}</TextWrapper>
						            	</View>
						            </View>

						            <View style={{flexDirection:'row', marginTop:5, marginBottom:5}}>
						            	<View style={{flex:0.3}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Pieces'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.65}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.pieces + ' Pcs'}</TextWrapper>
						            	</View>
						            </View>

						            
										{
											section.order_status_id == 3 ? 
												<View style={{flexDirection:'row', marginTop:15, marginBottom:5, alignItems:'center', justifyContent:'center'}}>
													<ButtonConfirmList.OnProgress
														data={loginState}
														_setData={this.state._setData}
														_showModal={this.props._showModal}
														_callback={this._afterSubmitOnProgress.bind(this)}
														_fetch={this.props._fetch}
														_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
													/>
												</View>
												:
												null
										}
										
					            </View>
					        </View>
				        </TouchableOpacity>
				     )}})}
				</View>
			</View>
		);
	}
}

class SearchResultDelivery extends Component{
	constructor(props) {
        super(props);
        this.state = {
        	data:this.props.data ? this.props.data : [],
        	_setData:this.props._setData ? this.props._setData : null,
        	image:this.props.image ? this.props.image : null,
        }
    }

    componentDidMount(){
    }

    _onPressDetail(data, loginState){
    	this.props.navigator.push({
			id:'OrderDetail',
			title: 'Detail Delivery',
			contentType: 'delivery',
			data:data,
			loginState:loginState,
			_afterSubmit:this._afterSubmitDelivery.bind(this),
    	});
    }

	
	_afterSubmitDelivery(callback = () => {}){
		this.props._setShouldReloadSummary(true);

		this.props.navigator.resetTo({
			id:'Dashboard',
			title:'Dashboard',
		});

		this.props._showModal(false,'', () => {
			alert('order telah selesai dikirim ke konsumen, terima kasih');
			//if (callback) callback();
    	})
    }

    _formatDate(date){
    	date = new Date(date.replace(/-/g,"/"));
	  	return DateFormat(date, 'fullDate');
	}

	_toCurrency(n, currency) {
    	return currency + " " + Number(n).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
	}

	render(){
		return(
			<View style={{flex:1, backgroundColor:'white'}}>
				<View style={{margin:10}}>
					<View>
						<TextWrapper type={'base'} style={{color:'darkslategray', fontWeight:'bold'}}>
							{'Hasil Pencarian Delivery'}
						</TextWrapper>
					</View>
					{this.props.data.map((section, key) => {
						var loginState= JSON.parse(JSON.stringify(this.props._getDataLoginState()));
						loginState.order_id = section.id;

						if (section.order_status_id == 4) {
						return(
						<TouchableOpacity key={key} onPress={this._onPressDetail.bind(this, section, loginState)}>
					        <View key={key} style={{marginTop:5, borderTopWidth:1, borderTopColor:'lightgray'}}>
				          		<View>
						            <View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.85, flexDirection:'row'}}>
						            		<View style={{flex:0.6}}>
						            			<TextWrapper type={'base'} style={{fontWeight:'normal', color:'darkslategray'}}>
								            		Order No. 
									            	<TextWrapper type={'content'} style={{fontWeight:'bold'}}>
									            		{section.order_hash}
									            	</TextWrapper>
								            	</TextWrapper>
						            		</View>

							            	{this.props._markOrderLate(section.is_late)}

											{this.props._markOrderImportant(section.important)}
							            </View>
						            	<View style={{flex:0.15, alignItems:'flex-end'}}>
						            		<ResponsiveImage
												source={this.state.image.detail_button.url}
												initWidth={this.state.image.detail_button.width * this.state.image.detail_button.multiplierWidth}
												initHeight={this.state.image.detail_button.height * this.state.image.detail_button.multiplierHeight}
											/>
						            	</View>
						            </View>
 									<View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.2}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Nama'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.75}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.customer.first_name}</TextWrapper>
						            	</View>
						            </View>
									<View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.2}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Tanggal'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.75}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{this._formatDate(section.delivery_date)}</TextWrapper>
						            	</View>
						            </View>
						            <View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.2}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Waktu'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.75}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.deliverytime.info + ' WIB'}</TextWrapper>
						            	</View>
						            </View>
						            <View style={{flexDirection:'row', marginTop:5}}>
						            	<View style={{flex:0.2}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Harga'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.75}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{this._toCurrency(section.price, 'Rp')}</TextWrapper>
						            	</View>
						            </View>
						            <View style={{flexDirection:'row', marginTop:5, marginBottom:5}}>
						            	<View style={{flex:0.2}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{'Alamat'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.05}}>
						            		<TextWrapper type={'base'} style={{color:'darkslategray'}}>{':'}</TextWrapper>
						            	</View>
						            	<View style={{flex:0.75}}>
											<TextWrapper type={'base'} style={{color:'darkslategray'}}>{section.delivery_address}</TextWrapper>
						            	</View>
						            </View>
						            <View style={{flexDirection:'row', marginTop:15, marginBottom:5, alignItems:'center', justifyContent:'center'}}>
										<ButtonConfirmList.Delivery
											data={loginState}
											_setData={this.state._setData}
											_showModal={this.props._showModal}
											_callback={this._afterSubmitDelivery.bind(this)}
											_fetch={this.props._fetch}
											_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
										/>
						            </View>					            
						        </View>
					        </View>
				        </TouchableOpacity>
				    )}})}
				</View>
			</View>
		);
	}
}