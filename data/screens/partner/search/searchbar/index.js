import React, { Component } from 'react';
import {View, Dimensions, TextInput, TouchableOpacity} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import ModalDropdown from 'react-native-modal-dropdown';

//style js class import
import Styles from './../../../../style/home';

export class SearchBar extends Component {
	constructor(props) {
        super(props);
        this.state = {
        	isInSearchResult:this.props.isInSearchResult ? true : false,
        	string:{
        		placeholder:{
        			basic:'Cari berdasarkan ',
        			end:'...',
        		}
        	},
        	image:{
				search_icon:{
					width:0,
					height:0,
					multiplierWidth:0.4,
					multiplierHeight:0.4,
					url:require('./../../../../image/searchbar/search_icon.png'),
				},
				cancel_icon:{
					width:0,
					height:0,
					multiplierWidth:0.55,
					multiplierHeight:0.55,
					url:require('./../../../../image/searchbar/cancel_icon.png'),
				}
        	},
        	data:this.props.loginState ? this.props.loginState : {},	
        	search:{
        		query:'',
        		byindex:0,
        	},
        	optionSearch:{
        		incoming:['Alamat'],
        		pickup:['Order No' , 'Nama', 'Alamat'],
        		onProgress:['Order No' , 'Nama'],
        		delivery:['Order No' , 'Nama', 'Alamat'],
        		all:['Order No' , 'Nama', 'Alamat'],
        	},
        	type: this.props.type ? this.props.type : 'all',
        	_afterSearch: this.props._afterSearch ? this.props._afterSearch : null,
        };  
	}

	componentDidMount(){
		this._initSearchBar();
	}

	_initSearchBar(){
		this._getImageSize();

		if (this.props.search) {
			var search= this.props.search;
			this.setState({search});
		}
	}

	_filterData(searchText, data) {
	  	let text = searchText.toLowerCase();
	
	  	return data.filter((n) => {
	  		var result = '';
	  		var currenctSearchBy = this._getOptionSearch()[this.state.search.byindex];
			

			if (currenctSearchBy == 'Order No') {
				result = n.order_hash.toLowerCase();
	  		}
	  		else if (currenctSearchBy == 'Nama') {
	  			result = n.customer.first_name.toLowerCase();
	  		}
	  		else if (currenctSearchBy == 'Alamat') {
	  			if (this.state.type == 'pickup') {
					result = n.pickup_address.toLowerCase();
	  			}
	  			else if(this.state.type == 'delivery'){
					result = n.delivery_address.toLowerCase();
	  			}
	  			else if (this.state.type == 'incoming') {
	  				result = n.order.pickup_address.toLowerCase();
	  			}
	  		}

	    	return result.search(text) !== -1;
	  	});
	}

	_getIsShouldFocus(){
		if (this.state.isInSearchResult) {
			return true;
		}
		return false;
	}

	_getImageSize(){
		var image = this.state.image;

		var image_search_icon = ResolveAssetSource(this.state.image.search_icon.url);

		image.search_icon.width = image_search_icon.width;
		image.search_icon.height = image_search_icon.height;

		var image_cancel_icon = ResolveAssetSource(this.state.image.cancel_icon.url);

		image.cancel_icon.width = image_cancel_icon.width;
		image.cancel_icon.height = image_cancel_icon.height;

      	this.setState({image});
	}

	_translateOrderBy(){
		var order_by = this._getOptionSearch()[this.state.search.byindex];

		if (order_by == 'Order No') {
			order_by = 'order_id';
		}
		else if (order_by == 'Nama') {
			order_by = 'first_name';
		}
		else if (order_by == 'Alamat') {
			order_by = 'address';
		}

		return order_by;
	}

	_onValueChangeSearchBar(value){
		if (this._validateAlphaNumericOnly(value)) {
			if (this.state.type == 'all') {
				var search= JSON.parse(JSON.stringify(this.state.search));
				search.query = value;
				search.by = this._translateOrderBy();
				
				if (this.state.isInSearchResult) {
					this.setState({search}, () => {
						this.state._afterSearch(search);
					});
				}
				else{
					if (value.length == 1) {
						this.props._getNavigator().push({
							id:'SearchResult',
							title:'Hasil Pencarian',
							search:search,
						});
					}
					
				}
			}
			else{
				var search = this.state.search;
				search.query = value;
				this.setState({search}, () => {
					if (this.state.type != 'all') {
						this._searchLocal(value, this.props.data, this.state._afterSearch);
					}
				});
			}
		}
	}

	_validateAlphaNumericOnly(value){
		var regexOnlyLetter = /^[0-9a-zA-Z ]+$/;
    	
    	if(regexOnlyLetter.test(value) || value == ''){
    		return true;
    	}

    	return false;
	}

	_onPressResetSearchQuery(){
		var search = this.state.search;
		search.query = '';
		this.setState({search}, () => {
			if (this.state.type != 'all') {
				this._searchLocal(search.query, this.props.data, this.state._afterSearch);
			}
			else{
				search= JSON.parse(JSON.stringify(this.state.search));
				search.query = '';
				search.by = this._translateOrderBy();
				
				this.setState({search}, () => {
					if (this.state._afterSearch) {
						this.state._afterSearch(search);
					}
				});
			}
		});
	}

	_resetSearchForm(){
		var search = this.state.search;
		search.query = '';

		this.setState({search});
	}

	_searchLocal(query, data, callback){
		callback(this._filterData(query, data));
	}

	_getOptionSearch(){
		if (this.state.type == 'incoming') {
			return this.state.optionSearch.incoming;
		}
		else if (this.state.type == 'pickup') {
			return this.state.optionSearch.pickup;
		}
		else if (this.state.type == 'onprogress') {
			return this.state.optionSearch.onProgress;

		}
		else if (this.state.type == 'delivery') {
			return this.state.optionSearch.delivery;
		}
		else{
			return this.state.optionSearch.delivery;
		}
	}

	_renderResetSearchQuery(){
		if (this.state.search.query.trim() != '') {
			return(
				<TouchableOpacity onPress={this._onPressResetSearchQuery.bind(this)}>
					<ResponsiveImage
						style={{marginLeft:Dimensions.get('window').width * 0.01}}
						source={this.state.image.cancel_icon.url}
						initWidth={this.state.image.cancel_icon.width * this.state.image.cancel_icon.multiplierWidth}
						initHeight={this.state.image.cancel_icon.height * this.state.image.cancel_icon.multiplierHeight}
					/>
				</TouchableOpacity>
			);
		}
	}

	render(){
		return(
			<View style={{backgroundColor:'white', width:Dimensions.get('window').width, height:Dimensions.get('window').height * 0.075, flexDirection:'row', justifyContent:'center', alignItems:'center', borderBottomWidth:1, borderColor:'lightgray'}}>
				<TouchableOpacity 
					onPress={() => {this.dropdown_type_search.show()}}
					style={{height:Dimensions.get('window').height * 0.075, width:Dimensions.get('window').width * 0.25, borderRightWidth:1, borderColor:'lightgray', alignItems:'center', flexDirection:'row'}}>
					<View style={{flex:0.3, bottom:1, alignItems:'flex-end'}}>
						<TextWrapper type={'content'} style={{color:'darkslategray'}}>
							{'\u25BC'}
						</TextWrapper>
					</View>
					<View style={{flex:0.7}}>
						<ModalDropdown ref={ref => this.dropdown_type_search = ref}
		                    style={{marginLeft:Dimensions.get('window').width * 0.01}}
		                    textStyle={{color:'darkslategray',fontSize:FontScale._getFontSize(8), fontFamily:'monserrat'}}
		                    dropdownStyle={{width:Dimensions.get('window').width * 0.35, borderWidth:1, borderColor:'lightgray'}}
		                    options={this._getOptionSearch()}
		                    renderSeparator={(sectionID, rowID, adjacentRowHighlighted) => {
		                    	if (rowID != this._getOptionSearch().length - 1) {
		                    		return (
										<View key={rowID} style={{borderBottomWidth:1, borderColor:'lightgray'}}/>
		                    		);
		                    	}
		                    	return null;
		                    	
		                	}}
		                    renderRow={(rowData, rowID, highlighted) => {
		                    	return (
									<View style={{flex:1, height:40, justifyContent:'center', alignItems:'center'}}>
										<TextWrapper type={'base'} style={{color:'darkslategray'}}>
											{rowData}
										</TextWrapper>
									</View>
		                    	);
		                    }}
		                    defaultValue={this._getOptionSearch()[this.state.search.byindex]}
		                    disabled={true}
		                    adjustFrame={(style) => {
								style.height = 44 * (this._getOptionSearch().length);
		                    	return style;
		                    }}
		                    //onDropdownWillShow={this._dropdown_5_willShow.bind(this)}
		                    //onDropdownWillHide={this._dropdown_5_willHide.bind(this)}
		                    onSelect={(index, value) => {
		                    	var search = this.state.search;
		                    	search.byindex = index;
		                    	this.setState({search});

		                    	this._onPressResetSearchQuery();
		                    }}
		            	/>
	            	</View>
            	</TouchableOpacity>

				<View style={{width:Dimensions.get('window').width * 0.075, alignItems:'center'}}>
					{this._renderResetSearchQuery()}
				</View>

				<View style={{width:Dimensions.get('window').width * 0.575}}>
					<TextInput
						placeholder={this.state.string.placeholder.basic + this._getOptionSearch()[this.state.search.byindex] + this.state.string.placeholder.end}
						autoFocus={this.state.isInSearchResult}
						value={this.state.search.query}
						style={[Styles.text, {paddingTop:0,paddingBottom:0}]}
						onChangeText={this._onValueChangeSearchBar.bind(this)}
						underlineColorAndroid={'transparent'}
					/>
				</View>

				<View style={{width:Dimensions.get('window').width * 0.10, alignItems:'flex-start'}}>
					<TouchableOpacity onPress={()=>{}}>
						<ResponsiveImage
							source={this.state.image.search_icon.url}
							initWidth={this.state.image.search_icon.width * this.state.image.search_icon.multiplierWidth}
							initHeight={this.state.image.search_icon.height * this.state.image.search_icon.multiplierHeight}
						/>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}
