import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableHighlight, Linking, Platform} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ResolveAssetSource from 'resolveAssetSource';
import SInfo from 'react-native-sensitive-info';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

//custom component js class import
import {TextWrapper, Form, Button, CustomView} from './../../../components';

//layout
import {LayoutPrimary} from './../../../layout';

import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class SignUp extends Component {
	constructor(props) {
        super(props);
        this.state = {
        	image:{
        		kliknklin_logo:{
        			width:0,
					height:0,
					multiplierWidth:0.53,
					multiplierHeight:0.53,
					url:require('./../../../image/signin/kliknklin_logo.png'),
        		},
        		login_button:{
					width:0,
					height:0,
					multiplierWidth:0.42,
					multiplierHeight:0.41,
					url:require('./../../../image/signin/login_button.png'),
        		},
        		show_password:{
					width:0,
					height:0,
					multiplierWidth:0.04,
					multiplierHeight:0.04,
					url:require('./../../../image/signin/show_password.png'),
        		},
        		hide_password:{
					width:0,
					height:0,
					multiplierWidth:0.04,
					multiplierHeight:0.04,
					url:require('./../../../image/signin/hide_password.png'),
        		},
        	},
        	data:{
        		email:'',
        		password:'',
				showPassword:false,
        	}
        }; 
        this.navigator = this.props.navigator; 
    }

    componentWillMount(){
    }

	componentDidMount(){
		this._getImageSize();
	}

	_getImageSize(){
		var image = this.state.image;

		var image_kliknklin_logo = ResolveAssetSource(this.state.image.kliknklin_logo.url);

		image.kliknklin_logo.width = moderateScale(image_kliknklin_logo.width);
		image.kliknklin_logo.height = moderateScale(image_kliknklin_logo.height);

		var image_login_button = ResolveAssetSource(this.state.image.login_button.url);

		image.login_button.width = moderateScale(image_login_button.width);
		image.login_button.height = moderateScale(image_login_button.height);

		var image_show_password = ResolveAssetSource(this.state.image.show_password.url);

		image.show_password.width = moderateScale(image_show_password.width);
		image.show_password.height = moderateScale(image_show_password.height);

		var image_hide_password = ResolveAssetSource(this.state.image.hide_password.url);

		image.hide_password.width = moderateScale(image_hide_password.width);
		image.hide_password.height = moderateScale(image_hide_password.height);

      	this.setState({image});
	}

	_validateLogin(){
		var error = '';
		
		error = this._validateEmpty();

		if (error == '') {
			error = this._validateEmail();
		}

		if (error == '') {
			error = this._validateMinimalChar();
		}

		this.props.actionRoot.setErrorMessage(error);
	}

	_validateEmpty(data = this.state.data){
		var result = '';
		
		if (data.email.trim() == '' || data.password.trim() == '') {
			result = STRING.ERROR.LOGIN.EMPTY_FIELD;
		}

		return result;
	}

	_validateMinimalChar(data = this.state.data){
		var result = '';
		
		if (data.password.length < 6) {
			result = STRING.ERROR.LOGIN.MINIMAL_CHARACTER_PASSWORD;
		}

		return result;
	}

	_validateEmail(data = this.state.data){
		var result = '';

    	var regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;;
    	
    	if(!regexEmail.test(data.email)){
			result = STRING.ERROR.LOGIN.EMAIL_FORMAT;
    	}

    	return result;
    }

    _onChangeEmail(email){
    	var data = this.state.data;
    	data.email = email;
		this.setState({data});
    }

    _onChangePassword(password){
    	var data = this.state.data;
    	data.password = password;
		this.setState({data});
    }

    _onPressForgetPassword(){
		this.props.actionNavigator.goToForgetPasswordScreen(this.props.navigator);
	}

    _onPressShowPassword(){
    	if (!this.state.showPassword) {
    		this.setState({showPassword:true});
		}
    	else {
    		this.setState({showPassword:false});
    	}
    }

    async _onPressSubmitButton(){
    	//await this._validateLogin();

    	//if (this.props.errorMessage == '') {
    		this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.SMSVerification')
    	//}
    }

    _renderShowPasswordIcon(){
    	if (!this.state.showPassword) {
    		return(
				<ResponsiveImage
					source={this.state.image.hide_password.url}
					initWidth={this.state.image.hide_password.width * this.state.image.hide_password.multiplierWidth}
					initHeight={this.state.image.hide_password.height * this.state.image.hide_password.multiplierHeight}
				/>
			);
    	}
    	else {
    		return(
				<ResponsiveImage
					source={this.state.image.show_password.url}
					initWidth={this.state.image.show_password.width * this.state.image.show_password.multiplierWidth}
					initHeight={this.state.image.show_password.height * this.state.image.show_password.multiplierHeight}
				/>
			);
    	}
    }

	_renderContent(){ 
		return(		
			<KeyboardAwareScrollView>
				<View style={style.container}>		
					<View style={[style.containerHeaderLogin,{height:this.props.layoutHeight * 0.7}]}>
						<ResponsiveImage
							source={this.state.image.kliknklin_logo.url}
							initWidth={this.state.image.kliknklin_logo.width * this.state.image.kliknklin_logo.multiplierWidth}
							initHeight={this.state.image.kliknklin_logo.height * this.state.image.kliknklin_logo.multiplierHeight}
						/>
					</View>

					<View style={style.containerBodyLogin}>
						<View style={style.containerTitleBodyLogin}>
							<TextWrapper fontWeight={'Bold'} type={'title'} style={{color:'#9B9C9B'}}>
								{'Daftar'}
							</TextWrapper>

							<TextWrapper type={'smallercontent'} style={{color:'#9B9C9B', marginTop:5, marginBottom:5}}>
								{'Buat Akun'}
							</TextWrapper>

							<TextWrapper type={'smallercontent'} style={{textAlign:'center', marginTop:5, color:'red',  width:Dimensions.get('window').width}}>
								{this.props.errorMessage}
							</TextWrapper>
						</View>
						
						<View style={style.containerFormBodyLogin}>
							<Form.InputField.TypeA
		                        keyboardType={'email-address'}
		                        ref='username_login' 
		                        label={STRING.SCREEN.LOGIN.EMAIL_PLACEHOLDER}
		                        onChangeText={this._onChangeEmail.bind(this)}
		                    />

		                    <Form.InputField.TypeA
		                        keyboardType={'default'}
		                        ref='password_login'
		                        //secureTextEntry={!this.state.showPassword}
		                        label={'Nomer Handphone'}
		                        onChangeText={this._onChangePassword.bind(this)}
		                        containerStyle={{marginTop:Dimensions.get('window').height * 0.035}}
		                    />

		                    <Form.InputField.TypeA
		                        keyboardType={'default'}
		                        ref='password_login'
		                        secureTextEntry={!this.state.showPassword}
		                        label={STRING.SCREEN.LOGIN.PASSWORD_PLACEHOLDER}
		                        onChangeText={this._onChangePassword.bind(this)}
		                        containerStyle={{marginTop:Dimensions.get('window').height * 0.035}}
		                        iconRight={
		                        	<TouchableHighlight underlayColor={'transparent'} onPress={this._onPressShowPassword.bind(this)}>
		                        		{this._renderShowPasswordIcon()}
		                        	</TouchableHighlight>
								}
		                    />

							
							<Button.TypeA
								buttonColor={CONSTANT.COLOR.BLUE_A}
								buttonText={'Daftar'}
								containerStyle={{marginTop:Dimensions.get('window').height * 0.04, height:Dimensions.get('window').height * 0.06}}
								onPress={this._onPressSubmitButton.bind(this)}
							/>

		                </View>
					</View>
				</View>
			</KeyboardAwareScrollView>
		);
	}

	render(){
		return (
			<LayoutPrimary
				containerStyle={{
					backgroundColor:'white',
				}}
				contentContainerStyle={{

				}}
				showBackButton={true}
				navigator={this.props.navigator}
				titleText={'Daftar Baru'}
			>
				<CustomView.WebView
                    uri={CONSTANT.APP_URL.SIGN_UP}
                />
			</LayoutPrimary>
		)
	}
}

var style = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor:'transparent',
        marginTop:Dimensions.get('window').height * 0.1,
    },

    containerHeaderLogin:{
    	marginTop:5,
		alignItems:'center',
    },

    containerBodyLogin:{
    },

	containerTitleBodyLogin:{
		alignItems:'center',
		height:Dimensions.get('window').height * 0.15,
	},

	containerFormBodyLogin:{
		alignItems:'center',
    },

    containerFooterLogin:{
		justifyContent:'center',
		alignItems:'center',
    }
});

function mapStateToProps(state, ownProps) {
	return {
		errorMessage:state.rootReducer.message.error,
		layoutHeight:state.rootReducer.layout.height,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionAuthentication: bindActionCreators(actions.authentication, dispatch),
		actionRoot: bindActionCreators(actions.root, dispatch),
		actionNavigator: bindActionCreators(actions.navigator, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);

