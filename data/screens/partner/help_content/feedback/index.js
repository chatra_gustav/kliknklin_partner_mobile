import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        Image,
        StyleSheet
        } from 'react-native';

import {moderateScale} from 'react-native-size-matters';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import {LayoutPrimary} from './../../../../layout';
import {TextWrapper, Button, Container, Form} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";


class Feedback extends Component {
    constructor(props){
        super(props);
        this.state = {
            feedback:'',
        };
    }

    onChangeText(feedback){
        this.setState({feedback});
    }

    render(){
        let {userData} = this.props;

        let {name} = userData;
        return(
            <View
            >
                <KeyboardAwareScrollView
                    contentContainerStyle={{
                    }}
                >
                    <Container.TypeA
                        containerStyle={{
                            //flex:1,
                        }}
                        contentStyle={{
                            height:'100%',
                            padding:Dimensions.get('window').height * 0.02,
                        }}
                    >   
                        
                            <Form.InputField.TypeD
                              label={'Nama'}
                              value={name}
                              containerStyle={{
                              }}
                              inputContainerStyle={{

                              }}
                              inputTextStyle={{
                              }}
                              numberOfLines={1}
                              //disabled
                            />

                            <Form.SeparatorField.TypeA />

                            <Form.InputField.TypeD
                                label={'Kritik / Saran Anda'}
                                containerStyle={{
                                }}
                                inputContainerStyle={{
                                  //backgroundColor:'red',
                                }}
                                numberOfLines={6}
                                maxLength={500}
                                multiline={true}
                                autoFocus
                                onChangeText={this.onChangeText.bind(this)}

                            />
                            

                            <Button.Standard
                                buttonText={'Kirim'}
                                buttonSize={CONSTANT.STYLE.BUTTON.LARGE}
                                buttonColor={CONSTANT.COLOR.GREEN}
                                fontOption={CONSTANT.TEXT_OPTION.HEADER}
                                onPress={() => {
                                    if (this.state.feedback == '') {
                                        alert('feedback tidak boleh kosong');
                                    }
                                    else{
                                         this.props.actionRoot.submitFeedback(this.state.feedback, this.props.navigator);
                                    }
                                }}
                                buttonStyle={{
                                    position:'absolute',
                                    bottom:'2%',
                                    left:'14%',
                                }}
                              />
                             
                    </Container.TypeA>

                    
                   
                </KeyboardAwareScrollView>
                
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    buttonContainer: {
        position:'absolute',
        bottom:0,
        height:Dimensions.get('window').height * 0.1,
        backgroundColor:'transparent',
        alignItems:'center',
        justifyContent:'center',
    },
});

function mapStateToProps(state, ownProps) {
    return {
        userData:state.rootReducer.user.data,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actionRoot: bindActionCreators(actions.root, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Feedback);


