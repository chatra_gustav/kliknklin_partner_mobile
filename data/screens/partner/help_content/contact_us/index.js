import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        Image,
        Linking,
        TouchableOpacity
        } from 'react-native';

import {moderateScale} from 'react-native-size-matters';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ResponsiveImage from 'react-native-responsive-image';
import Communications from 'react-native-communications';
import ResolveAssetSource from 'resolveAssetSource';

import {LayoutPrimary} from './../../../../layout';
import {TextWrapper, Button, Container, Form} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

export default class ContactUs extends Component {
    constructor(props){
        super(props);
        this.state = {
          image:{
            phone_icon : {
              width: 0,
              height: 0,
              multiplierWidth:0.5,
              multiplierHeight:0.5,
              url:require('./../../../../image/order_detail/phone_icon.png'),
            },
            sms_icon : {
              width: 0,
              height: 0,
              multiplierWidth:0.5,
              multiplierHeight:0.5,
              url:require('./../../../../image/order_detail/sms_icon.png'),
            },
          },
        };
    }

    componentWillMount() {
      this.getImageSize();
    }

    getImageSize(){
      var image = this.state.image;

      var image_phone_icon = ResolveAssetSource(this.state.image.phone_icon.url);
      image.phone_icon.width = moderateScale(image_phone_icon.width);
      image.phone_icon.height = moderateScale(image_phone_icon.height);

      var image_sms_icon = ResolveAssetSource(this.state.image.sms_icon.url);
      image.sms_icon.width = moderateScale(image_sms_icon.width);
      image.sms_icon.height = moderateScale(image_sms_icon.height);

      this.setState({image});
    }

    render(){
        return(
              <Container.TypeA
                  containerStyle={{
                      height:'100%',
                      justifyContent:'center',
                  }}
                  contentStyle={{
                      height:'60%',
                      padding:Dimensions.get('window').height * 0.02,
                  }}
              >   
                  
                      <View
                        style={{
                          flex:0.25
                        }}
                      >
                        <View style={{
                          flex:0.3,
                          alignItems:'center',
                        }}>
                          <TextWrapper type={'smallercontent'}>{'Telepon Kami'}</TextWrapper>
                        </View>

                        <View
                          style={{
                            flex:0.7,
                            justifyContent:'center',
                            alignItems:'center',
                            flexDirection:'row',
                          }}
                        >
                          <View
                            style={{
                              flex:1,
                              alignItems:'center',
                              justifyContent:'center',
                            }}
                          >
                            <TextWrapper type={'smallcontent'}>{'021-22717954'}</TextWrapper>
                          </View>
                          
                          <View style={{position:'absolute', right:0,}}>
                            <TouchableOpacity onPress={() => Communications.phonecall('02122717954', true)}>
                              <ResponsiveImage
                              source={this.state.image.phone_icon.url}
                              initWidth={this.state.image.phone_icon.width * this.state.image.phone_icon.multiplierWidth}
                              initHeight={this.state.image.phone_icon.height * this.state.image.phone_icon.multiplierHeight}
                              />
                            </TouchableOpacity>
                          </View>
                        </View>
                        
                      </View>

                      <Form.SeparatorField.TypeA />

                      <View
                        style={{
                          flex:0.25
                        }}
                      >
                        <View style={{
                          flex:0.3,
                          alignItems:'center',
                        }}>
                          <TextWrapper type={'smallcontent'}>{'Whatsapp Kami'}</TextWrapper>
                        </View>

                        <View
                          style={{
                            flex:0.7,
                            justifyContent:'center',
                            alignItems:'center'
                          }}
                        >
                          <Form.LinkField.TypeA
                            label={'08111566664'}
                            type={'smallcontent'}
                            containerStyle={{marginRight:3}}
                            labelStyle={{color:CONSTANT.COLOR.BLUE_A}}
                            onPress={()=>{
                              Linking.openURL('whatsapp://send?text=halo admin&phone=+628111566664')
                            }}
                          />
                        </View>
                        
                      </View>

                      <Form.SeparatorField.TypeA />

                      <View
                        style={{
                          flex:0.25
                        }}
                      >
                        <View style={{
                          flex:0.3,
                          alignItems:'center',
                        }}>
                          <TextWrapper type={'smallercontent'}>{'Email Kami'}</TextWrapper>
                        </View>

                        <View
                          style={{
                            flex:0.7,
                            justifyContent:'center',
                            alignItems:'center'
                          }}
                        >
                          <Form.LinkField.TypeA
                            label={'partnership@kliknklin.co'}
                            type={'smallcontent'}
                            containerStyle={{marginRight:3}}
                            labelStyle={{color:CONSTANT.COLOR.BLUE_A}}
                            onPress={()=>{
                              Communications.email(['partnership@kliknklin.co'],null,null,null,null);
                            }}
                          />
                           
                        </View>
                        
                      </View>

                      <Form.SeparatorField.TypeA />

                      <View
                        style={{
                          flex:0.25
                        }}
                      >
                        <View style={{
                          flex:0.3,
                          alignItems:'center',
                        }}>
                          <TextWrapper type={'smallercontent'}>{'Website Kami'}</TextWrapper>
                        </View>

                        <View
                          style={{
                            flex:0.7,
                            justifyContent:'center',
                            alignItems:'center'
                          }}
                        >
                          <Form.LinkField.TypeA
                            label={'https://partners.kliknklin.co'}
                            type={'smallcontent'}
                            containerStyle={{marginRight:3}}
                            labelStyle={{color:CONSTANT.COLOR.BLUE_A}}
                            onPress={()=>{
                              Communications.web('https://partners.kliknklin.co');
                            }}
                          />
                        </View>
                        
                      </View>

              </Container.TypeA>
        );
    }
}


