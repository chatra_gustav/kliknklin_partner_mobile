import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        WebView,
        } from 'react-native';

import {LayoutPrimary} from './../../../../layout';
import {TextWrapper, Button, Container} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

export default class WebviewContent extends Component {
    constructor(props){
        super(props);
        this.state = {
        };
    }

    render(){
        return(
            <Container.TypeA
                containerStyle={{
                    flex:1,
                }}
                contentStyle={{
                    height:Dimensions.get('window').height * 0.8,
                    padding:Dimensions.get('window').height * 0.01,
                }}
            >
                <WebView
                    startInLoadingState={true}
                    renderLoading={() => {
                            return (
                                <LoadingContent/>
                            )
                        }
                    }
                    source={{uri: this.props.uri}}
                    style={{flex:1}}
                />
            </Container.TypeA>
        );
    }
}

class LoadingContent extends Component{

    render(){
        return (
            <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                <TextWrapper type={'smallercontent'}>{'Memuat Konten...'}</TextWrapper>
            </View>
        )
    }
}


