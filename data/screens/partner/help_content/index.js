import React, { Component } from 'react';
import { 	
        Platform,
        View,
        Dimensions,
        TouchableHighlight,
  		} from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import TimerMixin from 'react-timer-mixin';

import {LayoutPrimary} from './../../../layout';
import {TextWrapper, Button, Container, CustomView, WebView} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

//local component
import WebviewContent from './webview_content';
import Feedback from './feedback';
import ContactUs from './contact_us';

class HelpContent extends Component {
    constructor(props){
        super(props);
        this.state = {
            title:'',
        };
    }

    componentWillMount(){
        this._getTitle();
    }

    _getTitle(){
        let title = '';

        if (this.props.helpType == 'sop') {
            title = 'Standar Prosedur';
        }
        else if (this.props.helpType == 'faq') {
            title = 'Pertanyaan Umum'
        }
         else if (this.props.helpType == 'user-guide') {
            title = 'Panduan Aplikasi';
        }
         else if (this.props.helpType == 'terms-and-condition-customer') {
            title = 'Syarat dan Ketentuan Konsumen';
        }
         else if (this.props.helpType == 'agreement') {
            title = 'Perjanjian Mitra';
        }
        else if (this.props.helpType == 'feedback') {
            title = 'Kritik dan Saran';
        }
        else if (this.props.helpType == 'contact-us') {
            title = 'Hubungi Kami';
        }

        this.setState({title});
    }

    _renderContent(){
        if (this.props.helpType == 'sop') {
            return(
                <WebView
                    uri={CONSTANT.APP_URL.STANDARD_OPERATING_PROCEDURE}
                />
            )
        }
        else if (this.props.helpType == 'faq') {
            return(
                <CustomView.WebView
                    uri={CONSTANT.APP_URL.FAQ}
                />
            )
        }
         else if (this.props.helpType == 'user-guide') {
            return(
                <CustomView.WebView
                    uri={CONSTANT.APP_URL.USER_GUIDE}
                />
            )
        }
         else if (this.props.helpType == 'terms-and-condition-customer') {
            return(
                <CustomView.WebView
                    uri={CONSTANT.APP_URL.TERMS_AND_CONDITION_CUSTOMER}
                />
            )
        }
         else if (this.props.helpType == 'agreement') {
            return(
                <CustomView.WebView
                    uri={CONSTANT.APP_URL.AGREEMENT}
                />
            )
        }
        else if (this.props.helpType == 'feedback') {
            return(
                <Feedback
                    {...this.props}
                />
            )
        }
        else if (this.props.helpType == 'contact-us') {
            return(
                <ContactUs/>
            );
        }
    }


    render(){
        return(
            <LayoutPrimary
                showTitle={true}
                showBackButton={true}
                showTabBar={false}
                navigator={this.props.navigator}
                title={this.state.title}
            >
                {this._renderContent()}
                
            </LayoutPrimary>
        );
    }
}


function mapStateToProps(state, ownProps) {
    return {
        layoutHeight:state.rootReducer.layout.height,
        userRole:state.rootReducer.user.role,
    };
}

function mapDispatchToProps(dispatch) {
    return {
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(HelpContent);


