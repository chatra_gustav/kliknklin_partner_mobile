import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';

import {LayoutPrimary} from './../../../layout';
import {TextWrapper, Container, Button, Form, toCurrency} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

import ResolveAssetSource from 'resolveAssetSource';
import {moderateScale} from 'react-native-size-matters';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class Invoice extends Component {

	render() {
	return (
		<LayoutPrimary
			showTitle={true}
      showBackButton={true}
      navigator={this.props.navigator}
			titleText={STRING.CHECKOUT_COURIER.TITLE}
		>

	      	<Container.TypeA
            containerStyle={{
              flex:0.9
            }}
          >
                <Header/>

                <HeaderItems/>

                <Items 
                  orderItems={this.props.orderDetailData.orderItems}
                />

                <Shipment
                  shipment={this.props.orderDetailData.shipment}
                />

                <Promo
                  promo={this.props.orderDetailData.promo}
                />

                <SubTotal 
                  orderItems={this.props.orderDetailData.orderItems}
                  shipment={this.props.orderDetailData.shipment}
                  promo={this.props.orderDetailData.promo}
                />

          </Container.TypeA>


          <Button.TypeA
            containerStyle={{
              flex:0.1,
            }}
          />
	  </LayoutPrimary>
	);

	}

}

class Header extends Component{
  render(){
    return(
      <View style={{
        borderBottomWidth:1,
        borderColor:CONSTANT.COLOR.GRAY_B,
        paddingTop:moderateScale(7.5),
        paddingBottom:moderateScale(15),
      }}>
        <View
          style={{
            width:'100%',
            alignItems:'center',
            marginBottom:moderateScale(20),
          }}
        >
            <TextWrapper fontWeight={'Bold'} type={'title'}>Laundry Klin Tebet</TextWrapper>
            <TextWrapper type={'base'}>Jln. tebet indah no 2 kurang 3 tambah 4</TextWrapper>
            <TextWrapper type={'base'}>Telp. 05235623</TextWrapper>
        </View>
        <View
          style={{

          }}
        >
          <View style={headerStyle.itemRow}>
            <View style={headerStyle.itemRowTitle}>
              <TextWrapper type={'base'}>
                ID Transaksi
              </TextWrapper>
            </View>

            <View style={headerStyle.itemRowColon}>
              <TextWrapper type={'base'}>
                :
              </TextWrapper>
            </View>

            <View style={headerStyle.itemRowContent}>
              <TextWrapper type={'base'}
                numberOfLines={3}
              >
                123123123
              </TextWrapper>
            </View>
          </View>

          <View style={headerStyle.itemRow}>
            <View style={headerStyle.itemRowTitle}>
              <TextWrapper type={'base'}>
                Konsumen
              </TextWrapper>
            </View>

            <View style={headerStyle.itemRowColon}>
              <TextWrapper type={'base'}>
                :
              </TextWrapper>
            </View>

            <View style={headerStyle.itemRowContent}>
              <TextWrapper type={'base'}
                numberOfLines={1}
              >
                Kim Yoona
              </TextWrapper>
            </View>
          </View>

          <View style={headerStyle.itemRow}>
            <View style={headerStyle.itemRowTitle}>
              <TextWrapper type={'base'}>
                Waktu Trans.
              </TextWrapper>
            </View>

            <View style={headerStyle.itemRowColon}>
              <TextWrapper type={'base'}>
                :
              </TextWrapper>
            </View>

            <View style={headerStyle.itemRowContent}>
              <TextWrapper type={'base'}
                numberOfLines={1}
              >
                Rabu, 24 Desember 1992 15:03
              </TextWrapper>
            </View>
          </View>

          <View style={headerStyle.itemRow}>
            <View style={headerStyle.itemRowTitle}>
              <TextWrapper type={'base'}>
                Pegawai
              </TextWrapper>
            </View>

            <View style={headerStyle.itemRowColon}>
              <TextWrapper type={'base'}>
                :
              </TextWrapper>
            </View>

            <View style={headerStyle.itemRowContent}>
              <TextWrapper type={'base'}
                numberOfLines={1}
              >
                Jessica Jung
              </TextWrapper>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const headerStyle = StyleSheet.create({

  itemRow:{
    marginTop:Dimensions.get('window').height * 0.005,
    flexDirection:'row',
  },
  itemRowTitle:{
    flex:0.3,
  },
  itemRowColon:{
    flex:0.04,
  },
  itemRowContent:{
    flex:0.66,
    flexDirection:'row',
    alignItems:'center',
  },
});

class HeaderItems extends Component{
  render(){
    return(
      <View style={{
        flexDirection:'row',
        justifyContent:'center',
        marginHorizontal:Dimensions.get('window').width * 0.025,
        marginTop:Dimensions.get('window').height * 0.015,
      }}>

        <View
          style={{flex:0.25}}
        >
          <TextWrapper type={'base'} style={{fontWeight:'bold'}}>Jumlah</TextWrapper>
        </View>
        <View
          style={{flex:0.75}}
        >
          <TextWrapper type={'base'} style={{fontWeight:'bold'}}>Produk</TextWrapper>
        </View>
      </View>
    );
  }
} 

class Items extends Component{
  renderAddedService(addedService){
    return addedService.map((data, key) => {
      return (
        <TextWrapper type={'smallbase'} style={{marginTop:2.5}} key={key}>{'+' + data.serviceName}</TextWrapper>
      )
    })
  }

  itemList(){
    return this.props.orderItems.map((data, key) => {
      return (
        <View style={{
          flexDirection:'row',
          justifyContent:'center',
          marginHorizontal:Dimensions.get('window').width * 0.025,
          marginTop:Dimensions.get('window').height * 0.015,
        }}
        key={key}
        >

            <View
              style={{flex:0.15}}
            >
              <TextWrapper type={'smallercontent'}>{data.quantity + ' ' + data.unitName}</TextWrapper>
            </View>
            <View
              style={{flex:0.55}}
            >
              <TextWrapper type={'smallercontent'} style={{fontWeight:'bold'}}>{data.itemName}</TextWrapper>
              <TextWrapper type={'smallbase'} style={{marginTop:2.5}}>{data.washingMethodName}</TextWrapper>

              {this.renderAddedService(data.addedService)}
            </View>
            <View
              style={{flex:0.30, flexDirection:'row'}}
            >
              <View
                style={{flex:0.3}}
              >
                <TextWrapper type={'base'}>{CONSTANT.CURRENCY.INDONESIA}</TextWrapper>
              </View>
              <View style={{
                flex:0.7,
                alignItems:'flex-end',
              }}>
                <TextWrapper type={'smallercontent'}>{toCurrency(data.quantity * data.unitPrice, '')}</TextWrapper>
              </View>
            </View>
        </View>
      )
    })
  }

  render(){
    return (
        <View>
          {this.itemList()}
        </View>
    )
  }
}

class Shipment extends Component{


  shipmentList(){
    return this.props.shipment.map((data, key) => {
      return (
        <View style={{
          flexDirection:'row',
          justifyContent:'center',
          marginHorizontal:Dimensions.get('window').width * 0.025,
          marginTop:Dimensions.get('window').height * 0.015,
        }}
        key={key}
        >
            <View
              style={{flex:0.15}}
            >
              <TextWrapper type={'smallercontent'}>{data.quantity}</TextWrapper>
            </View>
            <View
              style={{flex:0.55}}
            >
              <TextWrapper type={'smallercontent'} style={{fontWeight:'bold'}}>{data.shipmentName}</TextWrapper>
            </View>
            <View
              style={{flex:0.30, flexDirection:'row'}}
            >
              <View
                style={{flex:0.3}}
              >
                <TextWrapper type={'base'}>{CONSTANT.CURRENCY.INDONESIA}</TextWrapper>
              </View>
              <View style={{
                flex:0.7,
                alignItems:'flex-end',
              }}>
                <TextWrapper type={'smallercontent'}>{toCurrency(data.shipmentPrice, '')}</TextWrapper>
              </View>
            </View>
            
        </View>
      )
    })
  }

  render(){
    return (
        <View>
          {this.shipmentList()}
        </View>
    )
  }
}


class Promo extends Component{


  promoList(){
    return this.props.promo.map((data, key) => {
      return (
        <View style={{
          flexDirection:'row',
          justifyContent:'center',
          marginHorizontal:Dimensions.get('window').width * 0.025,
          marginTop:Dimensions.get('window').height * 0.015,
        }}
        key={key}
        >
            <View
              style={{flex:0.15}}
            >
              <TextWrapper type={'smallercontent'}>{data.quantity}</TextWrapper>
            </View>
            <View
              style={{flex:0.55}}
            >
              <TextWrapper type={'smallercontent'} style={{fontWeight:'bold'}}>{data.promoName}</TextWrapper>
            </View>
            <View
              style={{flex:0.30, flexDirection:'row'}}
            >
              <View
                style={{flex:0.3}}
              >
                <TextWrapper type={'base'}>{CONSTANT.CURRENCY.INDONESIA}</TextWrapper>
              </View>
              <View style={{
                flex:0.7,
                alignItems:'flex-end',
              }}>
                <TextWrapper type={'smallercontent'}>{toCurrency(data.quantity * data.unitPrice, '')}</TextWrapper>
              </View>
            </View>
            
        </View>
      )
    })
  }

  render(){
    return (
        <View>
          {this.promoList()}
        </View>
    )
  }
}

class SubTotal extends Component{
  
  getTotalPrice(){
      const item = this.props.orderItems;
      const shipment = this.props.shipment;
      const promo = this.props.promo;

      let totalHarga = 0;

      for(var key in item){
        totalHarga += item[key].quantity * item[key].unitPrice;
      }

      for(var key in shipment){
        totalHarga += shipment[key].shipmentPrice;
      }

      for(var key in promo){
        totalHarga += promo[key].quantity * promo[key].unitPrice;
      }

      return totalHarga;
  }

  render(){
    return (
        <View style={{

        }}>   
            <View style={{
                borderTopWidth:1.5, 
                  borderColor:CONSTANT.COLOR.BLUE_A,
                  margin:moderateScale(10),   
            }} />

            <View style={{
              flexDirection:'row',
              //justifyContent:'center',
              marginHorizontal:Dimensions.get('window').width * 0.025,
            }}
            >
                <View
                  style={{flex:0.15}}
                >
                </View>
                <View
                  style={{flex:0.55}}
                >
                  <TextWrapper fontWeight={'SemiBold'} type={'smallercontent'}>{'Total Yang Dibayar'}</TextWrapper>
                </View>
                <View
                  style={{flex:0.30, flexDirection:'row'}}
                >
                  <View
                    style={{flex:0.2}}
                  >
                    <TextWrapper fontWeight={'SemiBold'} type={'base'}>{CONSTANT.CURRENCY.INDONESIA}</TextWrapper>
                  </View>
                  <View style={{
                    flex:0.8,
                    alignItems:'flex-end',
                  }}>
                    <TextWrapper fontWeight={'SemiBold'} type={'smallcontent'}>{toCurrency(this.getTotalPrice(), '')}</TextWrapper>
                  </View>
                </View>
                
            </View>
        </View>
    )
  }
}


const styles = StyleSheet.create({
  textItems:{
    
  },
  textItemsOther:{
    marginTop:2.5,
  },
});

function mapStateToProps(state, ownProps) {
	return {
		orderDetailData:state.rootReducer.order.active.data,
		layoutHeight:state.rootReducer.layout.height,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Invoice);

