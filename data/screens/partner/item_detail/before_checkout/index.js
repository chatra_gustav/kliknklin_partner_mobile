import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList,
} from 'react-native';

import {LayoutPrimary} from './../../../../layout';
import * as CONSTANT from './../../../../constant';
import {TextWrapper, Container, Button, Form, toCurrency} from './../../../../components';
import * as STRING from './../../../../string';

import ResolveAssetSource from 'resolveAssetSource';
import {moderateScale} from 'react-native-size-matters';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

class BeforeCheckout extends Component {

  _renderFooter(){
    let {mode} = this.props;
    if (mode == 'confirm') {
      return(
          <FooterContent 
            {...this.props}
          />
      );
    }
  }

  _getTitle(){
    let {mode} = this.props;
    if (mode == 'confirm') {
        return STRING.SCREEN.CONFIRM_ITEM.CONFIRM_MODE_TITLE;
    }
    else{
      return STRING.SCREEN.CONFIRM_ITEM.READ_MODE_TITLE;
    }
  }

	render() {
    try{
      let {orderData} = this.props;

      let {orderitem, orderlaundry,shipments, pricedetail, appliedpromo} = orderData;

    
    	return (
        <ScrollView
          style={{flex:1}}
        >
          <Container.TypeA
            containerStyle={{
              flex:0.7,
            }}
          >
              <FlatList
                data={orderitem}
                keyExtractor={(item, index) => index+''}
                renderItem={(items, separator) => { 
                  let {item} = items; 
                  console.log(item);
                  return (<ItemContent item={item}/>);
                }}
                contentContainerStyle={{
                  flex:1,
                }}
                ListEmptyComponent={() => {return (<EmptyContent />);}}
                ListHeaderComponent={() => {return (<HeaderContent />);}}
              /> 

              <FlatList
                data={shipments}
                keyExtractor={(item, index) => index+''}
                renderItem={(items, separator) => { 
                  let {item} = items; 
                  return (<ShippingContent item={item}/>);
                }}
                contentContainerStyle={{
                  flex:1,
                }}
              /> 

              <FlatList
                data={appliedpromo}
                keyExtractor={(item, index) => index+''}
                renderItem={(items, separator) => { 
                  let {item} = items; 
                  return (<PromoContent item={item}/>);
                }}
                contentContainerStyle={{
                  flex:1,
                }}
              /> 
              
          </Container.TypeA>

          <View
            style={{
              flex:0.3,
            }}
          >
            <DurationContent orderlaundry={orderlaundry}/>
          </View>
          
        </ScrollView>
        );
      }
      catch(error){

      }
	}
}

class DurationContent extends Component{
  render(){
    try {
        let {orderlaundry} = this.props;
        let {timeservice, time_service_id} = orderlaundry;
        let info = ''
        if (timeservice) {
          info = timeservice.info;
        }
        
        return(
          <View style={{
            flex:1,
            marginTop:moderateScale(10),
            height:Dimensions.get('window').height * 0.125,
          }}>
            <View
              style={{flex:0.3,
                justifyContent:'flex-end'
              }}
            >
              <TextWrapper type={'base'} fontWeight={'SemiBold'}>Durasi</TextWrapper>
            </View>

            <View
              style={{flex:0.7, justifyContent:'center'}}
            >
              <Container.TypeA
                containerStyle={{
                  minHeight:0,
                  height:Dimensions.get('window').height * 0.055,
                }}
                contentStyle={{
                  flex:1,
                  flexDirection:'row',
                  justifyContent:'center',
                  alignItems:'center',
                }}
            >
              <View
                style={{
                  flex:0.65,
                }}
              >
                <TextWrapper type={'smallercontent'}>{info}</TextWrapper>
              </View>
              <View
                style={{
                  flex:0.35,
                }}
              >
                <TextWrapper type={'smallercontent'}>{orderlaundry.expected_duration +' Jam'}</TextWrapper>
              </View>
            </Container.TypeA>
            </View>
          </View>
        );
    }
    catch(error){
      return null;
    }
  }
}

class HeaderContent extends Component{
  render(){
    return(
      <View style={styleTable.container}>
        <View
          style={styleTable.containerFirstSection}
        >
          <TextWrapper fontWeight={'SemiBold'} type={'base'} style={styleTable.headerItemText}>Produk</TextWrapper>
        </View>

        <View
          style={styleTable.containerSecondSection}
        >
          <TextWrapper fontWeight={'SemiBold'} type={'base'} style={styleTable.headerItemText}>Jumlah</TextWrapper>
        </View>
        
      </View>
    );
  }
} 

class FooterContent extends Component{
  render(){
    return(
      <View style={{flex:1, justifyContent:'flex-end', alignItems:'center'}}>
          <Form.InputField.TypeB
            keyboardType={'visible-password'}
            ref='username_login' 
            placeholder={'Tambahkan catatan bila ada perubahan'}
            topLabel={true}
            topLabelText={'Catatan Tambahan'}
            //onChangeText={this._onChangeEmail.bind(this)}
          />

          <Button.TypeA
            buttonColor={CONSTANT.COLOR.BLUE_A}
            buttonText={STRING.CHECKOUT_COURIER.BUTTON}
            onPress={this.props.actionOrder.submitConfirmItem.bind((this.props.navigator))}
            containerStyle={{height:Dimensions.get('window').height * 0.075, marginTop:Dimensions.get('window').height * 0.05}}
          />

      </View>
    );
  }
}

class PromoContent extends Component{
  _getPromoInfo(){
    try{
      let {item} = this.props;
      let {promo} = item;
      let {promosource, promocode} = promo;

      if (promo.promo_source_id == CONSTANT.PROMO_SOURCE.PROMO_CODE) {
        return promosource.info + ' - ' + promocode.promo_code;
      }
      else{
        return promosource.info;
      }
    }
    catch(error){
      console.log(error, 'anjebg');
    }
  }

  render(){
    try{
      let {item} = this.props;
      return (
        <View style={styleTable.container}>

            <View
              style={styleTable.containerFirstSection}
            >
                <TextWrapper type={'base'} fontWeight={'SemiBold'}>
                  {this._getPromoInfo()}
                </TextWrapper> 
            </View>

            <View
              style={styleTable.containerSecondSection}
            >
                <TextWrapper type={'base'} fontWeight={'SemiBold'}>
                  {1}
                </TextWrapper>
            </View>  


        </View>
      );
    }
    catch(error){
      console.log(error);
      return null;
    }
  }
}

class ShippingContent extends Component{
  _getShippingText(){
    try{
      let {item} = this.props;
      let {shipment_type_id} = item;

      if (shipment_type_id == CONSTANT.SHIPMENT_TYPE.PICKUP) {
        return 'Biaya Penjemputan';
      }
      else if (shipment_type_id == CONSTANT.SHIPMENT_TYPE.DELIVERY) {
        return 'Biaya Pengantaran';
      }
    }
    catch(error){
      return null;
    }
  }
  render(){
    try{
      let {item} = this.props;
      let {distance} = item;
      return (
        <View style={styleTable.container}>

            <View
              style={styleTable.containerFirstSection}
            >
                <TextWrapper type={'base'} fontWeight={'SemiBold'}>
                  {this._getShippingText()}
                </TextWrapper>
            </View>

            <View
              style={styleTable.containerSecondSection}
            >
                <TextWrapper type={'base'} fontWeight={'SemiBold'}>
                  {distance + ' ' + CONSTANT.UNIT_MEASUREMENT.DISTANCE}
                </TextWrapper>
            </View>  


        </View>
      );
    }
    catch(error){
      console.log(error);
      return null;
    }
  }
}

class ItemContent extends Component{

  render(){
    try{

      return (
        <View style={styleTable.container}>

            <View
              style={styleTable.containerFirstSection}
            >
                <ProductContent  
                  {...this.props}
                /> 
            </View>

            <View
              style={styleTable.containerSecondSection}
            >
                <QuantityContent
                  {...this.props}
                />
            </View>  


        </View>
      );
    }
    catch(error){

    }
  }
}

class EmptyContent extends Component{
  render(){
    return(
      <View/>
    );
  }
}

class ProductContent extends Component{
  render(){
    try{
      let {item} = this.props;

      let {outlet, kliknklin, notes} = item;
     
      let itemName = '';
      let washingServiceInfo = '';
      let timeServiceInfo = '';
      let itemAddedService = [];
      let isOnDemand = false;

      if (outlet) {
          let {outletitemwashingtime, choosenaddedservice} = outlet;
          let {itemwashingtime, itemaddedservice} = outletitemwashingtime;
          let data = itemwashingtime.item;
          let {washingservice, timeservice, item} = itemwashingtime;
          itemName = item.name;
          washingServiceInfo = washingservice.info;
          timeServiceInfo = timeservice.info;
          itemAddedService = choosenaddedservice;
      }
      else if (
        kliknklin
      ) {
        let {itempricearea, choosenaddedservice} = kliknklin;
        let {itemwashingtime, itemaddedservice} = itempricearea;
        let data = itemwashingtime.item;
        let {washingservice, timeservice, item} = itemwashingtime;
        itemName = item.name;
        washingServiceInfo = washingservice.info;
        timeServiceInfo = timeservice.info;
        itemAddedService = choosenaddedservice;

        isOnDemand = true;
      }
    
      return(
        <View>
          <TextWrapper type={'base'} fontWeight={'SemiBold'}>{itemName}</TextWrapper>
          <TextWrapper type={'smallbase'} style={{marginTop:2.5}}>{washingServiceInfo + ' - ' + timeServiceInfo}</TextWrapper>
    
          
          <View
            style={{
              marginLeft:moderateScale(10),
            }}
          >
              <FlatList
                data={itemAddedService}
                keyExtractor={(item, index) => index+''}
                renderItem={(items, separator) => { 
                  let {item} = items; 
                  return (<AddedServiceContent isOnDemand={isOnDemand} item={item}/>);
                }}
                ListHeaderComponent={() => {
                  if (itemAddedService.length > 0) {
                    return (<HeaderAddedServiceContent />);
                  }
                  return null;
                }}
              /> 
          </View>
          
          <TextWrapper type={'smallbase'} fontWeight={'SemiBold'} style={{marginTop:2.5}}>{notes}</TextWrapper>
        </View>
      );
    }
    catch(error){
      console.log(error, 'before_checkout.ProductContent')
      return null;
    }
  }
}

class QuantityContent extends Component{
    render(){
      try{
        let {item} = this.props;
        let {outlet, kliknklin} = item;

        let itemQuantity = item.quantity;
        let itemUnitMeasurement = '';

        if (outlet) {
            let {outletitemwashingtime, choosenaddedservice} = outlet;
            let {itemwashingtime, itemaddedservice} = outletitemwashingtime;
            let data = itemwashingtime.item;
            let {washingservice, timeservice, item} = itemwashingtime;
            itemUnitMeasurement = item.unit_measurement;
        }
        else if (
          kliknklin
        ) {
            let {itempricearea, choosenaddedservice} = kliknklin;
            let {itemwashingtime, itemaddedservice} = itempricearea;
            let data = itemwashingtime.item;
            let {washingservice, timeservice, item} = itemwashingtime;
            itemUnitMeasurement = item.unit_measurement;
        }

        return(
             <TextWrapper type={'base'} fontWeight={'SemiBold'}>{itemQuantity + ' ' + itemUnitMeasurement}</TextWrapper>
        );
      }
      catch(error){
        return null;
      }
    }
}

class QuantityContents extends Component{
    render(){
      let {item} = this.props;
      return(
          <View style={{
            flexDirection:'row',
            alignItems:'center',
          }}>
            <View
              style={{
                flex:0.25,
              }}
            >
              <TouchableOpacity
                  style={{
                      width:moderateScale(20),
                      height:moderateScale(20),
                      borderRadius:moderateScale(20/2),
                      backgroundColor:CONSTANT.COLOR.BLUE_A,
                      alignItems:'center',
                      justifyContent:'center'
                  }}
              >

                  <TextWrapper type={'content'} style={{color:'white'}}>{'-'}</TextWrapper>
              </TouchableOpacity>

            </View>
            
            <View
              style={{
                flex:0.5,
                alignItems:'center',
              }}
            >
            <TextWrapper type={'smallercontent'}>{this.props.data.quantity + ' ' + this.props.data.unitName}</TextWrapper>
            </View>

            <View
              style={{
                flex:0.25,
              }}
            >
            <TouchableOpacity
                style={{
                    width:moderateScale(20),
                    height:moderateScale(20),
                    borderRadius:moderateScale(20/2),
                    backgroundColor:CONSTANT.COLOR.BLUE_A,
                    alignItems:'center',
                    justifyContent:'center'
                }}
            >
              <TextWrapper type={'content'} style={{color:'white'}}>{'+'}</TextWrapper>
            </TouchableOpacity>

            </View>

          </View>
      );
    }
}



class AddedServiceContent extends Component{
    render(){
      try{
        let {item, isOnDemand} = this.props;
        let info = '';

        if (isOnDemand) {
          let {kliknklinaddeditem} = item;
          let {itemaddedservice} = kliknklinaddeditem;
          let {mdaddedservice} = itemaddedservice;

          info = mdaddedservice.info;
        }
        else{
          let {outletaddeditem} = item;
          let {detail} = outletaddeditem;
          let {mdaddedservice} = detail;

          info = mdaddedservice.info;
        }

        return(
          <TextWrapper type={'smallbase'} style={{marginTop:0}} >
            {info}
          </TextWrapper>
        );
      }
      catch(error){
        return null;
      }
    }
}

class HeaderAddedServiceContent extends Component{
  render(){
    return(
      <View>
        <TextWrapper type={'smallbase'} fontWeight={'SemiBold'} style={{marginTop:3, color:CONSTANT.COLOR.GRAY_C}}>{'Servis Tambahan'}</TextWrapper>
      </View>
    );
  }
}

const styleTable = StyleSheet.create({
  container:{
    flex:1,
    flexDirection:'row',
    marginHorizontal:Dimensions.get('window').width * 0.025,
    marginTop:Dimensions.get('window').height * 0.015,
  },
  containerFirstSection:{
    flex:0.8,
  },
  containerSecondSection:{
    flex:0.2,
  },
  headerItemText:{
    color:CONSTANT.COLOR.GRAY_C,
  },
  contentItemText:{
    color:CONSTANT.COLOR.BLACK_A,
  },
});


const styles = StyleSheet.create({
  textItems:{
    
  },
  textItemsOther:{
    marginTop:2.5,
  },
});

function mapStateToProps(state, ownProps) {
	return {
		orderData:state.rootReducer.order.active.data,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    actionOrder: bindActionCreators(actions.order, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(BeforeCheckout);

