import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList,
} from 'react-native';

import {LayoutPrimary} from './../../../../layout';
import * as CONSTANT from './../../../../constant';
import {TextWrapper, Container, Button, Form, toCurrency} from './../../../../components';
import * as STRING from './../../../../string';

import ResolveAssetSource from 'resolveAssetSource';
import {moderateScale} from 'react-native-size-matters';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

class AfterCheckout extends Component {

  _renderFooter(){
    let {mode} = this.props;
    if (mode == 'confirm') {
      return(
          <FooterContent 
            {...this.props}
          />
      );
    }
  }

  _getTitle(){
    let {mode} = this.props;
    if (mode == 'confirm') {
        return STRING.SCREEN.CONFIRM_ITEM.CONFIRM_MODE_TITLE;
    }
    else{
      return STRING.SCREEN.CONFIRM_ITEM.READ_MODE_TITLE;
    }
  }

	render() {
    try{
      let {orderData} = this.props;

      let {orderitem, orderlaundry, shipments, appliedpromo, pricedetail} = orderData;

    	return (
        <ScrollView
          style={{flex:1}}
        >
          <Container.TypeA
            containerStyle={{
              minHeight:0,
              padding:moderateScale(5),
            }}
          >
              <FlatList
                data={orderitem}
                keyExtractor={(item, index) => index+''}
                renderItem={(items, separator) => { 
                  let {item} = items; 
                  return (<ItemContent item={item}/>);
                }}
                contentContainerStyle={{
                  flex:1,
                }}
                ListEmptyComponent={() => {return (<EmptyContent />);}}
                ListHeaderComponent={() => {return (<HeaderContent />);}}
              /> 

              <FlatList
                data={shipments}
                keyExtractor={(item, index) => index+''}
                renderItem={(items, separator) => { 
                  let {item} = items; 
                  return (<ShippingContent item={item}/>);
                }}
                contentContainerStyle={{
                  flex:1,
                }}
              /> 

              <FlatList
                data={appliedpromo}
                keyExtractor={(item, index) => index+''}
                renderItem={(items, separator) => { 
                  let {item} = items; 
                  return (<PromoContent item={item}/>);
                }}
                contentContainerStyle={{
                  flex:1,
                }}
              /> 

              <TotalContent item={pricedetail}/>

          </Container.TypeA>

          <DurationContent orderlaundry={orderlaundry}/>

          <CourierNoteContent {...this.props}/>  
        </ScrollView>
        );
      }
      catch(error){

      }
	}
}

class DurationContent extends Component{
  render(){
    try {
        let {orderlaundry} = this.props;
        let {timeservice, time_service_id} = orderlaundry;
        let info = ''
        if (timeservice) {
          info = timeservice.info;
        }
        
        return(
          <View style={{
            flex:1,
            marginTop:moderateScale(10),
            height:Dimensions.get('window').height * 0.125,
          }}>
            <View
              style={{flex:0.3,
                justifyContent:'flex-end'
              }}
            >
              <TextWrapper type={'base'} fontWeight={'SemiBold'}>Durasi</TextWrapper>
            </View>

            <View
              style={{flex:0.7, justifyContent:'center'}}
            >
              <Container.TypeA
                containerStyle={{
                  minHeight:0,
                  height:Dimensions.get('window').height * 0.055,
                }}
                contentStyle={{
                  flex:1,
                  flexDirection:'row',
                  justifyContent:'center',
                  alignItems:'center',
                }}
            >
              <View
                style={{
                  flex:0.65,
                }}
              >
                <TextWrapper type={'smallercontent'}>{info}</TextWrapper>
              </View>
              <View
                style={{
                  flex:0.35,
                }}
              >
                <TextWrapper type={'smallercontent'}>{orderlaundry.expected_duration +' Jam'}</TextWrapper>
              </View>
            </Container.TypeA>
            </View>
          </View>
        );
    }
    catch(error){
      return null;
    }
  }
}

class CourierNoteContent extends Component{
  _getCourierNotes(){
    let result = 'Tidak ada catatan tambahan'
    let {shipmentActivePickup} = this.props;
    let {internalshipment} = shipmentActivePickup;
    let {courier_notes} = internalshipment;
    
    if (courier_notes) {
      result = courier_notes;
    }

    return result;
  }

  render(){
    try {
        return(
          <View style={{
            flex:1,
            marginTop:moderateScale(10),
            height:Dimensions.get('window').height * 0.2,
          }}>
            <View
              style={{flex:0.25,
                justifyContent:'flex-end'
              }}
            >
              <TextWrapper type={'smallbase'} fontWeight={'SemiBold'}>Catatan Kurir</TextWrapper>
            </View>
            
            <View
              style={{flex:0.75, justifyContent:'center'}}
            >

            <Container.TypeA
                containerStyle={{
                  minHeight:0,
                  height:Dimensions.get('window').height * 0.125,
                }}
                contentStyle={{
                  flex:1,
                  flexDirection:'row',
                  justifyContent:'center',
                  alignItems:'center',
                }}
            >
              <View
                style={{
                  flex:1,
                }}
              >
                <TextWrapper type={'smallercontent'}>{this._getCourierNotes()}</TextWrapper>
              </View>
            </Container.TypeA>
            </View>
          </View>
        );
    }
    catch(error){
      console.log(error);
      return null;
    }
  }
}

class HeaderContent extends Component{
  render(){
    return(
      <View style={styleTable.container}>
        <View
          style={styleTable.containerFirstSection}
        >
          <TextWrapper fontWeight={'SemiBold'} type={'smallestcontent'} style={styleTable.headerItemText}>Jumlah</TextWrapper>
        </View>

        <View
          style={styleTable.containerSecondSection}
        >
          <TextWrapper fontWeight={'SemiBold'} type={'smallestcontent'} style={styleTable.headerItemText}>Produk</TextWrapper>
        </View>
        
        <View
          style={styleTable.containerThirdSection}
        >
          <TextWrapper fontWeight={'SemiBold'} type={'smallestcontent'} style={styleTable.headerItemText}>Harga</TextWrapper>
        </View>
      </View>
    );
  }
} 

class FooterContent extends Component{
  render(){
    return(
      <View style={{flex:1, justifyContent:'flex-end', alignItems:'center'}}>
          <Form.InputField.TypeB
            keyboardType={'visible-password'}
            ref='username_login' 
            placeholder={'Tambahkan catatan bila ada perubahan'}
            topLabel={true}
            topLabelText={'Catatan Tambahan'}
            //onChangeText={this._onChangeEmail.bind(this)}
          />

          <Button.TypeA
            buttonColor={CONSTANT.COLOR.BLUE_A}
            buttonText={STRING.CHECKOUT_COURIER.BUTTON}
            onPress={this.props.actionOrder.submitConfirmItem.bind((this.props.navigator))}
            containerStyle={{height:Dimensions.get('window').height * 0.075, marginTop:Dimensions.get('window').height * 0.05}}
          />

      </View>
    );
  }
}

class ItemContent extends Component{

  render(){
    return (
        <View style={styleTable.container}>

            <View
              style={styleTable.containerFirstSection}
            >   
                <QuantityContent
                  {...this.props}
                />   
            </View>

            <View
              style={styleTable.containerSecondSection}
            >
                <ProductContent
                  {...this.props}
                />
            </View>

            <View
              style={styleTable.containerThirdSection}
            >
                <PriceContent
                  {...this.props}
                />
            </View>
        </View>
    )
  }
}

class TotalContent extends Component{
  _getTotalPrice(){
    try{
      let {item} = this.props;
      let {full_price} = item;

      return full_price ? toCurrency(full_price, 'Rp') : 'TBA';
    }
    catch(error){
      return '';
    }
  }
  render(){
    try{
      let {item} = this.props;

      return (
        <View style={[styleTable.container, { 
          borderTopWidth:0.75, 
          height:moderateScale(25),
          alignItems:'center',
          marginTop:moderateScale(5),
        }]}>

            <View
              style={styleTable.containerFirstSecondSection}
            >   
                <TextWrapper type={'base'} fontWeight={'SemiBold'}>
                  TOTAL
                </TextWrapper> 
            </View>

            <View
              style={styleTable.containerThirdSection}
            >
                <TextWrapper type={'base'} fontWeight={'SemiBold'}>
                  {this._getTotalPrice()}
                </TextWrapper> 
            </View>
        </View>
      );
    }
    catch(error){
      return null;
    }
  }
}

class PromoContent extends Component{
  _getPromoInfo(){
    try{
      let {item} = this.props;
      let {promo} = item;
      let {promosource, promocode} = promo;

      if (promo.promo_source_id == CONSTANT.PROMO_SOURCE.PROMO_CODE) {
        return promosource.info + ' - ' + promocode.promo_code;
      }
      else{
        return promosource.info;
      }
    }
    catch(error){
      console.log(error, 'anjebg');
    }
  }
  render(){
    try{
      let {item} = this.props;
      let {promo, discount_price, promo_quantity} = item;
      return (
        <View style={styleTable.container}>
            <View
              style={styleTable.containerFirstSection}
            >   
                <TextWrapper type={'base'} fontWeight={'SemiBold'}>
                  {promo_quantity}
                </TextWrapper> 
            </View>

            <View
              style={styleTable.containerSecondSection}
            >
                <TextWrapper type={'base'} fontWeight={'SemiBold'}>
                  {this._getPromoInfo()}
                </TextWrapper> 
            </View>

            <View
              style={styleTable.containerThirdSection}
            >
                <TextWrapper type={'base'} fontWeight={'SemiBold'}>
                  {toCurrency(discount_price,'Rp -')}
                </TextWrapper> 
            </View>

        </View>
      );
    }
    catch(error){
      console.log(error);
      return null;
    }
  }
}

class ShippingContent extends Component{
  _getShippingText(){
    try{
      let {item} = this.props;
      let {shipment_type_id} = item;

      if (shipment_type_id == CONSTANT.SHIPMENT_TYPE.PICKUP) {
        return 'Biaya Penjemputan';
      }
      else if (shipment_type_id == CONSTANT.SHIPMENT_TYPE.DELIVERY) {
        return 'Biaya Pengantaran';
      }
    }
    catch(error){
      return null;
    }
  }
  render(){
    try{
      let {item} = this.props;
      let {distance, internalshipment} = item;

      return (
        <View style={styleTable.container}>
            <View
              style={styleTable.containerFirstSection}
            >   
                <TextWrapper type={'base'} fontWeight={'SemiBold'}>
                  {distance + ' ' + CONSTANT.UNIT_MEASUREMENT.DISTANCE}
                </TextWrapper>
            </View>

            <View
              style={styleTable.containerSecondSection}
            >
                <TextWrapper type={'base'} fontWeight={'SemiBold'}>
                  {this._getShippingText()}
                </TextWrapper> 
            </View>

            <View
              style={styleTable.containerThirdSection}
            >
                <TextWrapper type={'base'} fontWeight={'SemiBold'}>
                  {toCurrency(internalshipment.fare,'Rp ')}
                </TextWrapper> 
            </View> 

        </View>
      );
    }
    catch(error){
      console.log(error);
      return null;
    }
  }
}

class EmptyContent extends Component{
  render(){
    return(
      <View/>
    );
  }
}

class ProductContent extends Component{
  render(){
    try{
      let {item} = this.props;

      let {outlet, kliknklin, notes} = item;
     
      let itemName = '';
      let washingServiceInfo = '';
      let timeServiceInfo = '';
      let itemAddedService = [];
      let isOnDemand = false;

      if (outlet) {
          let {outletitemwashingtime, choosenaddedservice} = outlet;
          let {itemwashingtime, itemaddedservice} = outletitemwashingtime;
          let data = itemwashingtime.item;
          let {washingservice, timeservice, item} = itemwashingtime;
          itemName = item.name;
          washingServiceInfo = washingservice.info;
          timeServiceInfo = timeservice.info;
          itemAddedService = choosenaddedservice;
      }
      else if (
        kliknklin
      ) {
        let {itempricearea, choosenaddedservice} = kliknklin;
        let {itemwashingtime, itemaddedservice} = itempricearea;
        let data = itemwashingtime.item;
        let {washingservice, timeservice, item} = itemwashingtime;
        itemName = item.name;
        washingServiceInfo = washingservice.info;
        timeServiceInfo = timeservice.info;
        itemAddedService = choosenaddedservice;

        isOnDemand = true;
      }
    
      return(
        <View>
          <TextWrapper type={'base'} fontWeight={'SemiBold'}>{itemName}</TextWrapper>
          <TextWrapper type={'smallbase'} style={{marginTop:2.5}}>{washingServiceInfo + ' - ' + timeServiceInfo}</TextWrapper>
    
          
          <View
            style={{
              marginLeft:moderateScale(10),
            }}
          >
              <FlatList
                data={itemAddedService}
                keyExtractor={(item, index) => index+''}
                renderItem={(items, separator) => { 
                  let {item} = items; 
                  return (<AddedServiceContent isOnDemand={isOnDemand} item={item}/>);
                }}
                ListHeaderComponent={() => {
                  if (itemAddedService.length > 0) {
                    return (<HeaderAddedServiceContent />);
                  }
                  return null;
                }}
              /> 
          </View>
          
          <TextWrapper type={'smallbase'} fontWeight={'SemiBold'} style={{marginTop:2.5}}>{notes}</TextWrapper>
        
        </View>
      );
    }
    catch(error){
      console.log(error, 'before_checkout.ProductContent')
      return null;
    }
  }
}

class QuantityContent extends Component{
    render(){
      try{
        let {item} = this.props;
        let {id, item_price, quantity, item_duration, kliknklin, outlet} = item;
        let itemUnitMeasurement = '';
        let itemQuantity = quantity;

        let kliknklin_item_price_area_id = kliknklin ? kliknklin.kliknklin_item_price_area_id : null;
        let outlet_item_washing_time_id = outlet ? outlet.outlet_item_washing_time_id : null;
        let list_added_services = null;

        if (outlet) {
            let {outletitemwashingtime, choosenaddedservice} = outlet;
            let {itemwashingtime, itemaddedservice} = outletitemwashingtime;
            let data = itemwashingtime.item;
            let {washingservice, timeservice, item} = itemwashingtime;
            itemUnitMeasurement = item.unit_measurement;
        }
        else if (
          kliknklin
        ) {
            let {itempricearea, choosenaddedservice} = kliknklin;
            let {itemwashingtime, itemaddedservice} = itempricearea;
            let data = itemwashingtime.item;
            let {washingservice, timeservice, item} = itemwashingtime;
            itemUnitMeasurement = item.unit_measurement;
        }

        return(
           <TextWrapper type={'base'} fontWeight={'SemiBold'}>{itemQuantity + ' ' + itemUnitMeasurement}</TextWrapper>
        );
      }
      catch(error){
        console.log(error, 'checkout_item.QuantityContent')
        return null;
      }
    }
}

class PriceContent extends Component{
  render(){
    try{
      let {item} = this.props;
      let {item_price, quantity} = item;

      return(
        <View>
          <TextWrapper type={'base'} fontWeight={'SemiBold'}>{toCurrency(item_price * quantity, 'Rp ')}</TextWrapper>
    
        </View>
      );
    }
    catch(error){
      return null;
    }
  }
}

class QuantityContents extends Component{
    render(){
      let {item} = this.props;
      return(
          <View style={{
            flexDirection:'row',
            alignItems:'center',
          }}>
            <View
              style={{
                flex:0.25,
              }}
            >
              <TouchableOpacity
                  style={{
                      width:moderateScale(20),
                      height:moderateScale(20),
                      borderRadius:moderateScale(20/2),
                      backgroundColor:CONSTANT.COLOR.BLUE_A,
                      alignItems:'center',
                      justifyContent:'center'
                  }}
              >

                  <TextWrapper type={'content'} style={{color:'white'}}>{'-'}</TextWrapper>
              </TouchableOpacity>

            </View>
            
            <View
              style={{
                flex:0.5,
                alignItems:'center',
              }}
            >
            <TextWrapper type={'smallercontent'}>{this.props.data.quantity + ' ' + this.props.data.unitName}</TextWrapper>
            </View>

            <View
              style={{
                flex:0.25,
              }}
            >
            <TouchableOpacity
                style={{
                    width:moderateScale(20),
                    height:moderateScale(20),
                    borderRadius:moderateScale(20/2),
                    backgroundColor:CONSTANT.COLOR.BLUE_A,
                    alignItems:'center',
                    justifyContent:'center'
                }}
            >
              <TextWrapper type={'content'} style={{color:'white'}}>{'+'}</TextWrapper>
            </TouchableOpacity>

            </View>

          </View>
      );
    }
}

class AddedServiceContent extends Component{
    render(){
      try{
        let {item, isOnDemand} = this.props;
        let info = '';

        if (isOnDemand) {
          let {kliknklinaddeditem} = item;
          let {itemaddedservice} = kliknklinaddeditem;
          let {mdaddedservice} = itemaddedservice;

          info = mdaddedservice.info;
        }
        else{
          let {outletaddeditem} = item;
          let {detail} = outletaddeditem;
          let {mdaddedservice} = detail;

          info = mdaddedservice.info;
        }

        return(
          <TextWrapper type={'smallbase'} style={{marginTop:0}} >
            {info}
          </TextWrapper>
        );
      }
      catch(error){
        return null;
      }
    }
}

class HeaderAddedServiceContent extends Component{
  render(){
    return(
      <View>
        <TextWrapper type={'smallbase'} fontWeight={'SemiBold'} style={{marginTop:3, color:CONSTANT.COLOR.GRAY_C}}>{'Servis Tambahan'}</TextWrapper>
      </View>
    );
  }
}

class AddItems extends Component{
  render(){
    return(
        <View style={{
          flexDirection:'row',
          justifyContent:'center',
          marginHorizontal:Dimensions.get('window').width * 0.025,
          marginTop:Dimensions.get('window').height * 0.015,
        }}>

        <View
          style={{flex:0.35,alignItems:'center',}}
        >
            <TouchableOpacity
              style={{ 
                backgroundColor:CONSTANT.COLOR.BLUE_A, 
                padding:5,
                borderRadius:moderateScale(5),
                alignItems:'center',
                width:moderateScale(75),
              }}
            >
              <TextWrapper type={'base'} style={{fontWeight:'bold', color:'white'}}>{'Add Item'}</TextWrapper>
            </TouchableOpacity> 
        </View>

        <View
          style={{flex:0.65}}
        >
        </View>
      </View>
    );
  }
}



const styleTable = StyleSheet.create({
  container:{
    flex:1,
    flexDirection:'row',
    marginHorizontal:Dimensions.get('window').width * 0.025,
    marginBottom:Dimensions.get('window').height * 0.015,
  },
  containerFirstSecondSection:{
    flex:0.75,
    alignItems:'center',
  },
  containerFirstSection:{
    flex:0.2,
  },
  containerSecondSection:{
    flex:0.55,
  },
  containerThirdSection:{
    flex:0.25
  },
  headerItemText:{
    color:CONSTANT.COLOR.GRAY_C,
  },
  contentItemText:{
    color:CONSTANT.COLOR.BLACK_A,
  },
});


const styles = StyleSheet.create({
  textItems:{
    
  },
  textItemsOther:{
    marginTop:2.5,
  },
});

function mapStateToProps(state, ownProps) {
  return {
    orderData:state.rootReducer.order.active.data,
    shipmentActivePickup: state.rootReducer.order.shipmentActivePickup,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionOrder: bindActionCreators(actions.order, dispatch),
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(AfterCheckout);

