import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList,
} from 'react-native';

import {LayoutPrimary} from './../../../layout';
import * as CONSTANT from './../../../constant';
import {TextWrapper, Container, Button, Form} from './../../../components';
import * as STRING from './../../../string';

import ResolveAssetSource from 'resolveAssetSource';
import {moderateScale} from 'react-native-size-matters';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

//local component
import BeforeCheckout from './before_checkout';
import AfterCheckout from './after_checkout';

class ItemDetail extends Component {

  _getTitle(){
    let {mode} = this.props;
    if (mode == 'confirm') {
        return STRING.SCREEN.CONFIRM_ITEM.CONFIRM_MODE_TITLE;
    }
    else{
      return STRING.SCREEN.CONFIRM_ITEM.READ_MODE_TITLE;
    }
  }

  _getContent(orderActiveType =  CONSTANT.ORDER_ACTIVE_TYPE.DEFAULT){
    console.log(orderActiveType);
    if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP) {
      return(
        <BeforeCheckout/>
      ); 
    }
    else if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.IN_PROGRESS ||
        orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY
      ) {
      return(
        <AfterCheckout/>
      );
    }
  }

  render() {
    let {orderActiveType} = this.props;

    return (
      <LayoutPrimary
        {...this.props}

        showTitle={true}
        showBackButton={true}
        titleText={this._getTitle()}
      >
            {this._getContent(orderActiveType)}
      </LayoutPrimary>
    );

  }
}

const styleTable = StyleSheet.create({
  container:{
    flex:1,
    flexDirection:'row',
    marginHorizontal:Dimensions.get('window').width * 0.025,
    marginTop:Dimensions.get('window').height * 0.015,
  },
  containerFirstSection:{
    flex:0.75,
  },
  containerSecondSection:{
    flex:0.25,
  },
  headerItemText:{
    color:CONSTANT.COLOR.GRAY_C,
  },
  contentItemText:{
    color:CONSTANT.COLOR.BLACK_A,
  },
});


const styles = StyleSheet.create({
  textItems:{
    
  },
  textItemsOther:{
    marginTop:2.5,
  },
});

function mapStateToProps(state, ownProps) {
  return {
    orderActiveType:state.rootReducer.order.active.orderActiveType,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionOrder: bindActionCreators(actions.order, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemDetail);