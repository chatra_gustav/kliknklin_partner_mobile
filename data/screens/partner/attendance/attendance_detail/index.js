import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Image,
        StyleSheet
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';
import ImagePicker from 'react-native-image-picker';
import ImageView from 'react-native-image-view';

import {LayoutPrimary} from './../../../../layout';
import {Text, Button, Form, TabBar, OrderStatus, Avatar, Scaling, DateFormat} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

class AttendanceDetail extends Component{

  constructor(props){
    super(props);
    this.state={
      image:{
        arrow_right_gray:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('./../../../../image/attendance/arrow_right_gray.png'),
        },
        arrow_right_blue:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('./../../../../image/attendance/arrow_right_blue.png'),
        }
      },
      monthYear: '',
      selectedMonth: null,
      staff:{},
      isImageViewVisible: false,
      photo: [],
    }
  }

  componentWillMount(){
    this.setStaff();
  }

  componentDidMount(){
    this.setMonthOfStatistics();
    this.getImageSize();
  }

  setMonthOfStatistics(){
    this.state.monthYear = this.props.monthYear;
    this.state.selectedMonth = this.props.selectedMonth;
  }

  setStaff(){

    let staff = this.props.staff;

    this.setState({staff});
  }

  async getStaffStatistics(user_id){

    let staff = await this.props._getStaffStatistics(user_id);
      
    console.log('staff: ',staff.user_name);

    this.setState({staff});
  }

  getImageSize(){
    var image = this.state.image;

    var arrow_right_blue = ResolveAssetSource(this.state.image.arrow_right_blue.url);

    image.arrow_right_blue.width = arrow_right_blue.width * this.state.image.arrow_right_blue.multiplierWidth;
    image.arrow_right_blue.height = arrow_right_blue.height * this.state.image.arrow_right_blue.multiplierHeight;

    var arrow_right_gray = ResolveAssetSource(this.state.image.arrow_right_gray.url);

    image.arrow_right_gray.width = arrow_right_gray.width * this.state.image.arrow_right_gray.multiplierWidth;
    image.arrow_right_gray.height = arrow_right_gray.height * this.state.image.arrow_right_gray.multiplierHeight;
    
    this.setState({image});
  }

  render(){
      return (
        <LayoutPrimary
            showTitle={false}
            showBackButton={true}
            showOutletSelector={true}
            disableOutletSelector={true}
            showSearchButton={false}
            showTabBar={false}
            navigator={this.props.navigator}
            contentContainerStyle={{
            }}
          >
            <View
              style={{
                flex:1,
                alignItems: 'center'
              }}
            >
              
              <View style={styles.header}>
                <StatisticsHeader 
                  image={this.state.image}
                  changeMonth={this.changeMonth.bind(this)}
                  monthYear={this.state.monthYear}
                />
              </View>

              <View
                style={{
                  flex:1,
                  paddingTop: Scaling.moderateScale(20),
                  width:Dimensions.get('window').width
                }}
              >
                <AttendanceContent 
                  item={this.state.staff}
                  image={this.state.image}
                />

                <View style={{
                  flex: 1,
                  backgroundColor:'white',
                  elevation:3,
                  paddingTop: Dimensions.get('window').width * 0.05,
                  marginHorizontal: Scaling.moderateScale(2),
                  paddingHorizontal: Dimensions.get('window').width * 0.05
                }}>
                  <View style={{width: '100%'}}>
                    <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>Rincian Absen</Text>
                  </View>

                  <View style={styles.attendanceDetailHeader}>
                    <View style={styles.arrivalTimeHeader}>
                      <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>Waktu Masuk</Text>
                    </View>

                    <View style={styles.hoursHomeHeader}>
                      <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>Waktu Keluar</Text>
                    </View>
                  </View>

                  {this.state.staff.attendance.map((time, index) => {
                    return(
                      <View key={index} style={{width: '100%', flexDirection: 'row'}}>
                        <TouchableOpacity
                          activeOpacity={1}
                          style={styles.arrivalTime}
                          onPress={() => this.showImage(time.photo_url_in)}
                        >
                          <Text fontOption={CONSTANT.TEXT_OPTION.HEADER} textStyle={{fontFamily: CONSTANT.TEXT_FAMILY.MAIN_LIGHT}}>{time.datetime_in}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                          activeOpacity={1}
                          style={styles.hoursHome}
                          onPress={() => this.showImage(time.photo_url_out)}
                        >
                          <Text fontOption={CONSTANT.TEXT_OPTION.HEADER} textStyle={{fontFamily: CONSTANT.TEXT_FAMILY.MAIN_LIGHT}}>{time.datetime_out}</Text>
                        </TouchableOpacity>
                      </View>
                    )
                  })}

                </View>
              </View>
              <ImageView
                glideAlways
                images={this.state.photo}
                animationType="fade"
                isVisible={this.state.isImageViewVisible}
                onClose={() => this.setState({isImageViewVisible: false})}
              />
            </View>
        </LayoutPrimary>
      );
  }

  showImage(img_url){
    let photo = [];

    if(img_url){
      let img_url_obj = {
        source: {
            uri: img_url,
        },
      }

      photo.push(img_url_obj);
    }else{
      let img_url_obj = {
        source: {
            uri: "https://ik.imagekit.io/mfwrv7dkbia3y/image_not_available_d9u4tx_bTZ.jpg", // no image available
        },
      } 

      photo.push(img_url_obj);
    }

    this.setState({isImageViewVisible: true, photo})
  }

  async changeMonth(){

    this.props.actionNavigator.showLightBox(CONSTANT.ROUTE_TYPE.MONTH_PICKER,{
        
        selectedMonth: this.state.selectedMonth,

        onPressButtonPrimary: async (selectedDate) => {

          this.props.actionNavigator.dismissLightBox();
          this.props.actionAttendance.setAttendanceMonth(DateFormat(selectedDate, 'isoDate'));

          let monthYear = DateFormat(selectedDate, 'monthYear');

          this.setState({ monthYear, selectedMonth: DateFormat(selectedDate, 'shortDate2') }, () => {
            let user_id = this.props.staff.user_id;
            this.getStaffStatistics(user_id);
          });

        },
        onPressButtonSecondary: () =>{
          this.props.actionNavigator.dismissLightBox();
        },
    })
  }

}

class StatisticsHeader extends Component{
  render(){
    return(
      <View style={styles.statisticsHeader}>
          <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>Ringkasan Statistik</Text>

          <View style={{marginTop: Scaling.moderateScale(12)}}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT}
              textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>
              Periode
            </Text>
            
            <TouchableOpacity style={styles.statisticsHeaderDate} hitSlop={styles.hitSlop}
              onPress={this.showMonthPicker}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>{this.props.monthYear}</Text>
              
              <View style={{marginLeft: Scaling.moderateScale(10)}}>
                <Image
                  style={{width: this.props.image.arrow_right_blue.width, height: this.props.image.arrow_right_blue.height}}
                  source={this.props.image.arrow_right_blue.url}
                />
              </View>
            </TouchableOpacity>
          </View>
      </View>
    )
  }

  showMonthPicker = () => {
    this.props.changeMonth();
  }
}

class AttendanceContent extends Component{

  render(){
    return(
      <TouchableOpacity
        style={{
          height:Dimensions.get('window').height * 0.16,
          backgroundColor:CONSTANT.COLOR.WHITE,
          elevation:2,
          marginBottom: Scaling.moderateScale(20),
          marginHorizontal: Scaling.moderateScale(2),
          paddingHorizontal: Dimensions.get('window').width * 0.05
        }}
      >
        <View style={styles.attendanceContentContainer}>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.avatar}>
              <Avatar
                size={Scaling.scale(CONSTANT.STYLE.ATTENDANCE.AVATAR_SIZE)}
                img={this.props.item.img_url}
              />
            </View>

            <View style={styles.employeeDetail}>
              <View style={{marginBottom: Scaling.moderateScale(8)}}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>{this.props.item.user_role}</Text>
              </View>
              <View style={{marginBottom: Scaling.moderateScale(8)}}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>{this.props.item.user_name}</Text>
              </View>
              <View style={{marginBottom: Scaling.moderateScale(8)}}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT}>{this.getAttendanceDetail(this.props.item.days_in,this.props.item.hours_in)}</Text>
              </View>
            </View>

            <View style={styles.percentage}>
              <AttendancePercentage
                percentage={this.props.item.percentage}
                image={this.props.image}
              />
            </View>
          </View>
        </View>
        
      </TouchableOpacity>
    );
  }

  getAttendanceDetail(days_in, hours_in){
    console.log('days_in: ',days_in);
    console.log('hours_injkh: ',hours_in);
    if(days_in !== ""){
      return "Masuk : "+days_in+" Hari "+"( "+hours_in+" Jam )";
    }else{
      return "Masuk : -";
    }
  }

  onPressAttendanceContent = () => {
    let staff = this.props.item;
    this.props.seeDetail(staff);
  }
}

class AttendancePercentage extends Component{
  render(){

    let {
      size,
      percentage
    } = this.props;

    let _borderColor = CONSTANT.COLOR.LIGHT_BLUE;

    if(percentage < 75)
      _borderColor = CONSTANT.COLOR.RED;

    return(
      <View style={styles.attendanceContainer}>
        <View style={[styles.attendancePercentage, {borderColor: _borderColor}]}>
          <Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_PERCENTAGE_NUMBER}>{this.props.percentage} %</Text>
        </View>
      </View>
    )
  }
}


function mapStateToProps(state, ownProps) {
  return {
    layoutHeight:state.rootReducer.layout.height,
    userRole:state.rootReducer.user.role,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionAttendance: bindActionCreators(actions.attendance, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AttendanceDetail);

const styles = StyleSheet.create({
  header:{
    width: '100%',
    backgroundColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BACKGROUND_COLOR,
    borderBottomWidth:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_BOTTOM_WIDTH,
    borderColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_COLOR,
    flexDirection:'row'
  },
  statisticsHeader:{
    flex: 1,
    paddingTop: Dimensions.get('window').width * 0.025,
    paddingBottom: Dimensions.get('window').width * 0.025,
    paddingHorizontal: Dimensions.get('window').width * 0.05
  },
  attendancePercentage:{
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: StyleSheet.hairlineWidth,
    width: CONSTANT.STYLE.ATTENDANCE.PERCENTAGE_CIRCLE_SIZE,
    height: CONSTANT.STYLE.ATTENDANCE.PERCENTAGE_CIRCLE_SIZE,
    borderRadius: CONSTANT.STYLE.ATTENDANCE.PERCENTAGE_CIRCLE_SIZE * 0.5
  },
  attendanceContentContainer:{
    flex: 1,
    marginTop:Scaling.moderateScale(10), 
    marginBottom:Scaling.moderateScale(10),
    justifyContent: 'center'
  },
  attendanceContainer:{
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  avatar:{
    flex:0.22, 
    alignItems: 'flex-start', 
    justifyContent: 'center',
  },
  employeeDetail:{
    flex:0.53, 
    paddingBottom: Scaling.moderateScale(5)
  },
  percentage:{
    flex: 0.25,
    alignItems: 'flex-end',
  },
  attendanceDetailHeader:{
    width: '100%', 
    flexDirection: 'row', 
    marginTop: Scaling.moderateScale(10)
  },
  arrivalTimeHeader:{
    width: '50%', 
    paddingTop: Scaling.moderateScale(8), 
    paddingBottom: Scaling.moderateScale(8), 
    justifyContent: 'center',
  },
  hoursHomeHeader:{
    width: '50%', 
    paddingTop: Scaling.moderateScale(8), 
    paddingBottom: Scaling.moderateScale(8), 
    justifyContent: 'center', 
    paddingLeft: Scaling.moderateScale(10)
  },
  arrivalTime:{
    width: '50%', 
    paddingTop: Scaling.moderateScale(10), 
    paddingBottom: Scaling.moderateScale(10), 
    justifyContent: 'center', 
    borderRightWidth: StyleSheet.hairlineWidth, 
    borderColor: CONSTANT.COLOR.LIGHT_GRAY, 
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  hoursHome:{
    width: '50%', 
    paddingTop: Scaling.moderateScale(10), 
    paddingBottom: Scaling.moderateScale(10), 
    justifyContent: 'center', 
    paddingLeft: Scaling.moderateScale(10), 
    borderColor: CONSTANT.COLOR.LIGHT_GRAY, 
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  statisticsHeaderDate:{
    flexDirection: 'row', 
    alignItems: 'center'
  },
  hitSlop:{
    top: 5, 
    bottom: 5, 
    left: 5, 
    right: 5
  }
})

