import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Image,
        StyleSheet,
        Alert
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';
import ImagePicker from 'react-native-image-picker';

import {LayoutPrimary} from './../../../layout';
import {Text, Button, Form, TabBar, OrderStatus, Avatar, Scaling, DateFormat} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class Attendance extends Component{

  constructor(props){
    super(props);
    this.state={
      image:{
        edit_icon:{
            width:0,
            height:0,
            multiplierWidth:0.35,
            multiplierHeight:0.35,
            url:require('./../../../image/attendance/edit_icon.png'),
        },
        statistics_icon:{
            width:0,
            height:0,
            multiplierWidth:0.3,
            multiplierHeight:0.3,
            url:require('./../../../image/attendance/statistics_icon.png'),
        },
        arrow_right_gray:{
            width:0,
            height:0,
            multiplierWidth:0.3,
            multiplierHeight:0.3,
            url:require('./../../../image/attendance/arrow_right_gray.png'),
        },
        arrow_right_blue:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('./../../../image/attendance/arrow_right_blue.png'),
        },
      },
      isoDate: 'today',
      fullDate: ''
    }
  }

  componentDidMount(){
    this._getDate();
    this.getImageSize();
    this.getStaff(this.state.isoDate);
  }

  async getStaff(date){

    let isoDate = '';

    if(date == 'today'){
      let today = new Date();
      let today_isoDate = DateFormat(today, 'isoDate');

      isoDate = today_isoDate;
    }else{
      isoDate = date;
    }
    
    await this.props.actionAttendance.setAttendanceDate(isoDate);
    await this.props.actionAttendance.getStaff();
    this.setState({ isoDate });
  }

  getImageSize(){
    var image = this.state.image;

    var edit_icon = ResolveAssetSource(this.state.image.edit_icon.url);

    image.edit_icon.width = edit_icon.width * this.state.image.edit_icon.multiplierWidth;
    image.edit_icon.height = edit_icon.height * this.state.image.edit_icon.multiplierHeight;

    var statistics_icon = ResolveAssetSource(this.state.image.statistics_icon.url);

    image.statistics_icon.width = statistics_icon.width * this.state.image.statistics_icon.multiplierWidth;
    image.statistics_icon.height = statistics_icon.height * this.state.image.statistics_icon.multiplierHeight;

    var arrow_right_gray = ResolveAssetSource(this.state.image.arrow_right_gray.url);

    image.arrow_right_gray.width = arrow_right_gray.width * this.state.image.arrow_right_gray.multiplierWidth;
    image.arrow_right_gray.height = arrow_right_gray.height * this.state.image.arrow_right_gray.multiplierHeight;

    var arrow_right_blue = ResolveAssetSource(this.state.image.arrow_right_blue.url);

    image.arrow_right_blue.width = arrow_right_blue.width * this.state.image.arrow_right_blue.multiplierWidth;
    image.arrow_right_blue.height = arrow_right_blue.height * this.state.image.arrow_right_blue.multiplierHeight;
    
    this.setState({image});
  }

  _getDate(){

    let today = new Date();
    let today_fullDate = DateFormat(today, 'fullDate');

    this.setState({ fullDate: today_fullDate });

  }

  render(){
      return (
        <LayoutPrimary
            showTitle={false}
            showBackButton={true}
            showOutletSelector={true}
            disableOutletSelector={true}
            showSearchButton={false}
            navigator={this.props.navigator}
            showTabBar={false}
            contentContainerStyle={{
            }}
          >
            <View
              style={{
                flex:1,
                alignItems: 'center'
              }}
            >
              
              <View style={styles.header}>
                <AttendanceHeader 
                  fullDate={this.state.fullDate}
                />
              </View>

              <TouchableOpacity style={styles.attendanceStatistics} onPress={this.onPressAttendanceStatistics}>
                <View style={{flex: 0.1, alignItems: 'flex-start'}}>
                  <Image
                    style={{width: this.state.image.statistics_icon.width, height: this.state.image.statistics_icon.height}}
                    source={this.state.image.statistics_icon.url}
                  />
                </View>
                <View style={{flex: 0.8}}>
                  <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>Lihat Ringkasan Statistik</Text>
                </View>
                <View style={{flex: 0.1, alignItems: 'flex-end'}}>
                  <Image
                    style={{width: this.state.image.arrow_right_blue.width, height: this.state.image.arrow_right_blue.height}}
                    source={this.state.image.arrow_right_blue.url}
                  />
                </View>
              </TouchableOpacity>

              <View
                style={{
                  flex:1,
                  paddingTop: Scaling.moderateScale(20),
                  width:Dimensions.get('window').width
                }}
              >
              <FlatList
                  showsVerticalScrollIndicator={false}
                  extraData={this.state}
                  data={this.props.staffList}
                  keyExtractor={(item, index) => index+''}
                  renderItem={(item) => { return (
                    <AttendanceContent 
                      actionRoot={this.props.actionRoot}
                      actionLocation={this.props.actionLocation}
                      actionUser={this.props.actionUser}
                      navigator={this.props.navigator}
                      item={item.item}
                      absent={this.absent.bind(this)}
                      image={this.state.image}
                      actionUploadImage={this.props.actionUploadImage}
                      getDateOfAttendance={this.getDateOfAttendance.bind(this)}
                    />
                  );}}
                /> 
              </View>
            </View>
        </LayoutPrimary>
      );
  }

  getDateOfAttendance(){
    return this.state.isoDate;
  }

  async absent(staff, absentAction, photo_uri){

    let _staff = {
      user_id: staff.user_id,
      photo_uri: photo_uri
    }

    if(absentAction === 'in')
      await this.props.actionAttendance.staffIn(this.props.navigator, _staff);
    else
      await this.props.actionAttendance.staffOut(this.props.navigator, _staff);

    await this.props.actionAttendance.getStaff();
  }

  onPressAttendanceStatistics = () => {
    this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.ATTENDANCE_STATISTICS)
  }
}

class AttendanceHeader extends Component{
  render(){
    return(
      <View style={styles.attendanceHeader}>
          <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>Absen Karyawan Outlet</Text>

          <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>{this.props.fullDate}</Text>

      </View>
    )
  }
}

class AttendanceContent extends Component{

  async takePicture(staff, absentAction, isAlreadyTakePicture) {
    if(this.isValidToTakePicture(staff, absentAction, isAlreadyTakePicture)){
     
      ImagePicker.launchCamera({}, async (response)  => {

        if (response.didCancel) {
            console.log('User cancelled image picker');
        }
        else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
        }   
        else {
          let photo_uri = await this.props.actionUploadImage.uploadImage(response.uri);

          this.props.absent(staff, absentAction, photo_uri)
        }
      });
      // await this.props.actionLocation.checkLocationServicePermission(this.props.navigator)
      // .then( async (response) => {
      //   if(response){
      //     await this.props.actionUser.getUserLocation(this.props.navigator);

      //     ImagePicker.launchCamera({}, async (response)  => {

      //     if (response.didCancel) {
      //         console.log('User cancelled image picker');
      //     }
      //     else if (response.error) {
      //         console.log('ImagePicker Error: ', response.error);
      //     }   
      //     else {

      //       let photo_uri = await this.props.actionUploadImage.uploadImage(response.uri);

      //       this.props.absent(staff, absentAction, photo_uri)   
      //     }
      //   });
          
      //   }
      // })
    }
  };

  isValidToTakePicture(staff, absentAction, isAlreadyTakePicture){
    if( isAlreadyTakePicture ){
      return false;
    }
    else if( absentAction == 'out' && staff.time_in == ''){
      this.props.actionRoot.showMessage('Anda belum absen masuk', 'Terjadi Kesalahan');

      return false;
    }
    else if(!this.isValidDate()){
      Alert.alert(
        '',
        'Tidak dapat mengkonfirmasi kehadiran pada tanggal ini',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: true},
      );

      return false;
    }

    return true;
  }

  isValidDate(){

    let today = new Date();
    let today_isoDate = DateFormat(today, 'isoDate');

    return today_isoDate === this.props.getDateOfAttendance();
  }

  render(){

    return(
      <View
        style={{
          flex:1,
          height:Dimensions.get('window').height * 0.16,
          backgroundColor:CONSTANT.COLOR.WHITE,
          elevation:2,
          marginBottom: Scaling.moderateScale(20),
          marginHorizontal: Scaling.moderateScale(2),
          paddingHorizontal: Dimensions.get('window').width * 0.05
        }}
      >
        <View
          style={styles.attendanceContentContainer}
        >
            <View style={{flex:1, flexDirection: 'row'}}>

              <View style={styles.avatar}>
                <Avatar 
                  size={Scaling.scale(CONSTANT.STYLE.ATTENDANCE.AVATAR_SIZE)}
                  img={this.props.item.img_url}
                />
              </View>

              <View style={{flex:0.78}}>

                <View style={styles.employeeDetail}>
                  
                  <View style={{marginBottom: Scaling.moderateScale(8)}}>
                    <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>{this.props.item.user_role}</Text>
                  </View>

                  <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>{this.props.item.user_name}</Text>
                </View>

                <View style={styles.attendanceButton}>
                  <View style={styles.button}>
                    {this.renderTimeIn(this.props.item)}
                    <Button.TypeD
                      buttonText={(this.props.item.isIn ? 'Masuk' : 'Absen Masuk')}
                      buttonColor={(this.props.item.isIn ? CONSTANT.COLOR.LIGHTEST_GRAY : CONSTANT.COLOR.LIGHT_BLUE)}
                      buttonTextStyle={
                        [(this.props.item.isOut ? {color: CONSTANT.COLOR.GRAY} : {color: CONSTANT.COLOR.WHITE}), 
                        {fontSize: Scaling.moderateScale(12), fontFamily: CONSTANT.TEXT_FAMILY.MAIN_SEMI_BOLD }]}
                      buttonStyle={styles.buttonStyle}
                      onPress={() => {this.takePicture(this.props.item, 'in', this.props.item.isIn)}}
                    />
                  </View>

                  <View style={{marginRight: Scaling.moderateScale(10)}}></View>
                  
                  <View style={styles.button}>
                    {this.renderTimeOut(this.props.item)}
                    <Button.TypeD
                      buttonText={(this.props.item.isOut ? 'Keluar' : 'Absen Keluar')}
                      buttonColor={(this.props.item.isOut ? CONSTANT.COLOR.LIGHTEST_GRAY : CONSTANT.COLOR.ORANGE)}
                      buttonTextStyle={
                        [(this.props.item.isOut ? {color: CONSTANT.COLOR.GRAY} : {color: CONSTANT.COLOR.WHITE}), 
                        {fontSize: Scaling.moderateScale(12), fontFamily: CONSTANT.TEXT_FAMILY.MAIN_SEMI_BOLD}]}
                      buttonStyle={styles.buttonStyle}
                      onPress={() => {this.takePicture(this.props.item, 'out', this.props.item.isOut)}}
                    />
                  </View>
                </View>

              </View>
            </View>

            
        </View>
        
      </View>
    );
  }

  renderTimeIn(item){
    if(item.isIn){
      return <Text fontOption={CONSTANT.TEXT_OPTION.CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>Waktu: {item.time_in}</Text>
    }
    return <Text fontOption={CONSTANT.TEXT_OPTION.CONTENT} textStyle={{color: CONSTANT.COLOR.WHITE}}>:</Text>
  }

  renderTimeOut(item){
    if(item.isOut){
      return <Text fontOption={CONSTANT.TEXT_OPTION.CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>Waktu: {item.time_out}</Text>
    }
    return <Text fontOption={CONSTANT.TEXT_OPTION.CONTENT} textStyle={{color: CONSTANT.COLOR.WHITE}}>:</Text>
  }
}


function mapStateToProps(state, ownProps) {

  return {
    layoutHeight:state.rootReducer.layout.height,
    staffList: state.rootReducer.attendance.staffList,
    attendanceDate: state.rootReducer.attendance.attendanceDate,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionAttendance: bindActionCreators(actions.attendance, dispatch),
    actionUploadImage: bindActionCreators(actions.uploadImage, dispatch),
    actionLocation: bindActionCreators(actions.location, dispatch),
    actionUser: bindActionCreators(actions.user, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Attendance);

const styles = StyleSheet.create({
  header:{
    height:Scaling.verticalScale(CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.HEIGHT),
    width: '100%',
    backgroundColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BACKGROUND_COLOR,
    borderBottomWidth:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_BOTTOM_WIDTH,
    borderColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_COLOR,
    flexDirection:'row'
  },
  attendanceContentContainer:{
    flex:1, 
    marginTop:Scaling.moderateScale(10), 
    marginBottom:Scaling.moderateScale(10)
  },
  attendanceHeader:{
    flex: 1,
    flexDirection: 'row', 
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: Dimensions.get('window').width * 0.05
  },
  attendanceStatistics:{
    height:Scaling.verticalScale(CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.HEIGHT / 1.8),
    width: '100%',
    paddingHorizontal: Dimensions.get('window').width * 0.05,
    backgroundColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BACKGROUND_COLOR,
    borderBottomWidth:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_BOTTOM_WIDTH,
    borderColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_COLOR,
    flexDirection:'row',
    alignItems: 'center',
    backgroundColor: '#E3F2FF'
  },
  employeeDetail:{
    paddingBottom: Scaling.moderateScale(5)
  },
  attendanceButton:{
    flex:1, 
    marginTop: Scaling.moderateScale(5),
    flexDirection:'row', 
    alignItems:'flex-end',
    marginRight: Scaling.moderateScale(5)
  },
  edit_icon:{
    position: 'absolute', 
    top: 0, 
    right: 10
  },
  avatar:{
    flex:0.22, 
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  buttonStyle:{
    borderRadius: Scaling.moderateScale(10),
    width: Dimensions.get('window').width * 0.7 * 0.4,
    height: CONSTANT.STYLE.BUTTON.SMALL.HEIGHT
  },
  button:{
    alignItems: 'center'
  }
})

