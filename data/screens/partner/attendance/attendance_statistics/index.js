import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Image,
        StyleSheet
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';
import ImagePicker from 'react-native-image-picker';

import {LayoutPrimary} from './../../../../layout';
import {Text, Button, Form, TabBar, OrderStatus, Avatar, Scaling, DatePicker, DateFormat} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

class AttendanceStatistics extends Component{

  constructor(props){
    super(props);
    this.state={
      image:{
        arrow_right_gray:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('./../../../../image/attendance/arrow_right_gray.png'),
        },
        arrow_right_blue:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('./../../../../image/attendance/arrow_right_blue.png'),
        }
      },
      monthYear: '',
      selectedMonth: null
    }
  }

  componentDidMount(){
    this.setMonth();
    this.getAllStaffStatistics();
    this.getImageSize();
  }

  async setMonth(){

    let today = new Date();
    let monthYear = DateFormat(today, 'monthYear');

    await this.props.actionAttendance.setAttendanceMonth(DateFormat(today, 'isoDate'));

    this.setState({ monthYear });
  }

  async getAllStaffStatistics(){
    await this.props.actionAttendance.getStaffStatistics();
  }

  async _getStaffStatistics(user_id){

    this.state.monthYear = DateFormat(this.props.attendanceMonth, 'monthYear');

    await this.props.actionAttendance.getStaffStatistics();
    let returnStaff;

    this.props.staffSummary.map((data) => {
      if(data.user_id == user_id)
        returnStaff = data;
    })    

    return returnStaff;
  }

  getImageSize(){
    var image = this.state.image;

    var arrow_right_blue = ResolveAssetSource(this.state.image.arrow_right_blue.url);

    image.arrow_right_blue.width = arrow_right_blue.width * this.state.image.arrow_right_blue.multiplierWidth;
    image.arrow_right_blue.height = arrow_right_blue.height * this.state.image.arrow_right_blue.multiplierHeight;

    var arrow_right_gray = ResolveAssetSource(this.state.image.arrow_right_gray.url);

    image.arrow_right_gray.width = arrow_right_gray.width * this.state.image.arrow_right_gray.multiplierWidth;
    image.arrow_right_gray.height = arrow_right_gray.height * this.state.image.arrow_right_gray.multiplierHeight;
    
    this.setState({image});
  }

  render(){
      return (
        <LayoutPrimary
            showTitle={false}
            showBackButton={true}
            showOutletSelector={true}
            showSearchButton={false}
            navigator={this.props.navigator}
            showTabBar={false}
            contentContainerStyle={{
            }}
          >
            <View
              style={{
                flex:1,
                alignItems: 'center'
              }}
            >
              
              <View style={styles.header}>
                <StatisticsHeader 
                  image={this.state.image}
                  changeMonth={this.changeMonth.bind(this)}
                  monthYear={this.state.monthYear}
                />
              </View>

              <View
                style={{
                  flex:1,
                  paddingTop:Scaling.moderateScale(20),
                  width:Dimensions.get('window').width
                }}
              >
              <FlatList
                  showsVerticalScrollIndicator={false}
                  extraData={this.state}
                  data={this.props.staffSummary}
                  keyExtractor={(item, index) => index+''}
                  renderItem={(item) => { return (
                    <AttendanceContent 
                      item={item.item}
                      image={this.state.image}
                      seeDetail={this.seeDetail.bind(this)}
                    />
                  );}}
                /> 
              </View>
            </View>
        </LayoutPrimary>
      );
  }

  async changeMonth(){

    this.props.actionNavigator.showLightBox(CONSTANT.ROUTE_TYPE.MONTH_PICKER,{
        
        selectedMonth: this.state.selectedMonth,
        
        onPressButtonPrimary: async (selectedDate) => {

          this.props.actionNavigator.dismissLightBox();
          this.props.actionAttendance.setAttendanceMonth(DateFormat(selectedDate, 'isoDate'));

          let monthYear = DateFormat(selectedDate, 'monthYear');

          this.setState({ monthYear : monthYear, selectedMonth: DateFormat(selectedDate, 'shortDate2') }, () => {
            this.getAllStaffStatistics()
          });

        },
        onPressButtonSecondary: () =>{
          this.props.actionNavigator.dismissLightBox();
        },
    })
  }

  seeDetail(staff){
    
    let monthYear = this.state.monthYear;
    this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.ATTENDANCE_DETAIL, {
      staff: staff,
      monthYear: monthYear,
      selectedMonth: this.state.selectedMonth,
      _getStaffStatistics: (user_id) => this._getStaffStatistics(user_id)
    });
  }
}

class StatisticsHeader extends Component{
  render(){
    return(
      <View style={styles.statisticsHeader}>
          <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>Ringkasan Statistik</Text>

          <View style={{marginTop:Scaling.moderateScale(12)}}>
            <Text 
              fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT}
              textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>
              Periode
            </Text>
            
            <TouchableOpacity style={styles.statisticsHeaderDate} hitSlop={styles.hitSlop}
              onPress={this.showMonthPicker}>
              <Text 
                fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}
                textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>
                {this.props.monthYear}
              </Text>
              
              <View style={{marginLeft: Scaling.moderateScale(10)}}>
                <Image
                  style={{width: this.props.image.arrow_right_blue.width, height: this.props.image.arrow_right_blue.height}}
                  source={this.props.image.arrow_right_blue.url}
                />
              </View>
            </TouchableOpacity>
          </View>
      </View>
    )
  }

  showMonthPicker = () => {
    this.props.changeMonth();
  }
}

class AttendanceContent extends Component{

  render(){
    return(
      <TouchableOpacity
        style={{
          height:Dimensions.get('window').height * 0.16,
          backgroundColor:CONSTANT.COLOR.WHITE,
          elevation:2,
          marginBottom: Scaling.moderateScale(20),
          marginHorizontal: Scaling.moderateScale(2),
          paddingHorizontal: Dimensions.get('window').width * 0.05
        }}

        onPress={this.onPressAttendanceContent}
      >
        <View
          style={styles.attendanceContentContainer}
        >
          <View style={{flexDirection: 'row'}}>
            <View style={styles.avatar}>
              <Avatar
                size={Scaling.scale(CONSTANT.STYLE.ATTENDANCE.AVATAR_SIZE)}
                img={this.props.item.img_url}
              />
            </View>

            <View style={styles.employeeDetail}>
              <View style={{marginBottom: Scaling.moderateScale(8)}}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>{this.props.item.user_role}</Text>
              </View>
              <View style={{marginBottom: Scaling.moderateScale(8)}}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>{this.props.item.user_name}</Text>
              </View>
              <View style={{marginBottom: Scaling.moderateScale(8)}}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT}>{this.getAttendanceDetail(this.props.item.days_in,this.props.item.hours_in)}</Text>
              </View>
            </View>

            <View style={styles.percentage}>
              <AttendancePercentage
                percentage={this.props.item.percentage}
                image={this.props.image}
              />
            </View>
          </View>
        </View>
        
      </TouchableOpacity>
    );
  }

  getAttendanceDetail(days_in, hours_in){
    console.log('days_in: ',days_in);
    console.log('hours_injkh: ',hours_in);
    if(days_in !== ""){
      return "Masuk : "+days_in+" Hari "+"( "+hours_in+" Jam )";
    }else{
      return "Masuk : -";
    }
  }

  onPressAttendanceContent = () => {
    let staff = this.props.item;
    this.props.seeDetail(staff);
  }
}

class AttendancePercentage extends Component{
  render(){

    let {
      size,
      percentage
    } = this.props;

    let _borderColor = CONSTANT.COLOR.LIGHT_BLUE;

    if(percentage < 75)
      _borderColor = CONSTANT.COLOR.RED;

    return(
      <View style={styles.attendanceContainer}>
        <View style={[styles.attendancePercentage, {borderColor: _borderColor}]}>
          <Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_PERCENTAGE_NUMBER}>{this.props.percentage} %</Text>
        </View>


        <View>
          <Image
              style={{width: this.props.image.arrow_right_gray.width, height: this.props.image.arrow_right_gray.height}}
              source={this.props.image.arrow_right_gray.url}
            />
        </View>
      </View>
    )
  }
}


function mapStateToProps(state, ownProps) {
  return {
    layoutHeight:state.rootReducer.layout.height,
    staffSummary: state.rootReducer.attendance.staffSummary,
    attendanceMonth: state.rootReducer.attendance.attendanceMonth,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionAttendance: bindActionCreators(actions.attendance, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AttendanceStatistics);

const styles = StyleSheet.create({
  header:{
    width: '100%',
    backgroundColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BACKGROUND_COLOR,
    borderBottomWidth:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_BOTTOM_WIDTH,
    borderColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_COLOR,
    flexDirection:'row'
  },
  statisticsHeader:{
    flex: 1,
    paddingTop: Dimensions.get('window').width * 0.025,
    paddingBottom: Dimensions.get('window').width * 0.025,
    paddingHorizontal: Dimensions.get('window').width * 0.05
  },
  attendanceContentContainer:{
    flex:1, 
    marginTop:Scaling.moderateScale(10), 
    marginBottom:Scaling.moderateScale(10),
    justifyContent: 'center'
  },
  attendancePercentage:{
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: StyleSheet.hairlineWidth,
    width: CONSTANT.STYLE.ATTENDANCE.PERCENTAGE_CIRCLE_SIZE,
    height: CONSTANT.STYLE.ATTENDANCE.PERCENTAGE_CIRCLE_SIZE,
    borderRadius: CONSTANT.STYLE.ATTENDANCE.PERCENTAGE_CIRCLE_SIZE * 0.5
  },
  attendanceContainer:{
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  avatar:{
    flex:0.22, 
    alignItems: 'flex-start', 
    justifyContent: 'center',
  },
  employeeDetail:{
    flex:0.53, 
    paddingBottom: Scaling.moderateScale(5)
  },
  percentage:{
    flex: 0.25,
    alignItems: 'flex-end',
  },
  statisticsHeaderDate:{
    flexDirection: 'row', 
    alignItems: 'center'
  },
  hitSlop:{
    top: 5, 
    bottom: 5, 
    left: 5, 
    right: 5
  }
})

