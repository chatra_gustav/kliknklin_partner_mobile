import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Image,
        StyleSheet,
        NativeModules,
        LayoutAnimation
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';
import ImagePicker from 'react-native-image-picker';
import { TextField } from 'react-native-material-textfield';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Form, TabBar, OrderStatus, Avatar, Scaling, DateFormat, fontMaker, toCurrency} from 'app/data/components';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

class Deposit extends Component{

  constructor(props){
    super(props);
    this.state={
      image:{
        edit_icon:{
            width:0,
            height:0,
            multiplierWidth:0.35,
            multiplierHeight:0.35,
            url:require('app/data/image/attendance/edit_icon.png'),
        },
        arrow_right_blue:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('app/data/image/attendance/arrow_right_blue.png'),
        },
        clock_icon:{
          width:0,
          height:0,
          multiplierWidth:0.3,
          multiplierHeight:0.3,
          url:require('app/data/image/deposit/clock_icon.png'),
        },
        info_icon:{
          width:0,
          height:0,
          multiplierWidth:0.3,
          multiplierHeight:0.3,
          url:require('app/data/image/deposit/info_icon.png'),
        },
        wallet_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/deposit/wallet_icon.png'),
        },
        arrow_down_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/iot_machine/arrow_down_icon.png'),
        },
        arrow_up_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/iot_machine/arrow_up_icon.png'),
        }
      },
      dateOfStatistics:{
        startDate: '',
        endDate: ''
      },
      depositSummary: {
        currentbalance: 0, 
        pendingtransactions: 0,
        successtransactions: 0
      },
      accountOwner: {
        name: '',
        account_holder_name: '',
        account_number: '',
        bank_image_url: ''
      },
      kliknklinBanks: []
    }
  }

  componentDidMount(){
    this.getKnkBanks();
    this.getPartnerData();
    this.getDepositSummary();
    this.getImageSize();
  }

  async getKnkBanks(){
    await this.props.actionDeposit.getKnkBanks();
    this.setState({ kliknklinBanks: this.props.kliknklinBanks }, () => {
      console.log('kliknklinBanks l : ',this.props.kliknklinBanks.length);
    })
  }

  async getPartnerData(){
    await this.props.actionDeposit.getPartnerData();

    if (this.props.accountOwner) {
      this.setState({ accountOwner: this.props.accountOwner })
    }
    else{
      this.props.actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
          isDismissAllowed:false,
          title:'Perhatian', 
          message: 'Saat ini anda tidak memili data bank. \n Atur data bank anda atau hubungi mitra kami.',
          buttonPrimaryText: 'Kembali', 
          onPressButtonPrimary: () => {
            this.props.actionNavigator.pop(this.props.navigator);
            this.props.actionNavigator.dismissLightBox();
          },
      });
    }
  }

  async getDepositSummary(){
    await this.props.actionDeposit.getSummary();
    this.setState({ depositSummary: this.props.depositSummary })
  }

  getImageSize(){
    var image = this.state.image;

    var arrow_right_blue = ResolveAssetSource(this.state.image.arrow_right_blue.url);

    image.arrow_right_blue.width = arrow_right_blue.width * this.state.image.arrow_right_blue.multiplierWidth;
    image.arrow_right_blue.height = arrow_right_blue.height * this.state.image.arrow_right_blue.multiplierHeight;

    var edit_icon = ResolveAssetSource(this.state.image.edit_icon.url);

    image.edit_icon.width = edit_icon.width * this.state.image.edit_icon.multiplierWidth;
    image.edit_icon.height = edit_icon.height * this.state.image.edit_icon.multiplierHeight;

    var clock_icon = ResolveAssetSource(this.state.image.clock_icon.url);

    image.clock_icon.width = clock_icon.width * this.state.image.clock_icon.multiplierWidth;
    image.clock_icon.height = clock_icon.height * this.state.image.clock_icon.multiplierHeight;

    var info_icon = ResolveAssetSource(this.state.image.info_icon.url);

    image.info_icon.width = info_icon.width * this.state.image.info_icon.multiplierWidth;
    image.info_icon.height = info_icon.height * this.state.image.info_icon.multiplierHeight;

    var wallet_icon = ResolveAssetSource(this.state.image.wallet_icon.url);

    image.wallet_icon.width = wallet_icon.width * this.state.image.wallet_icon.multiplierWidth;
    image.wallet_icon.height = wallet_icon.height * this.state.image.wallet_icon.multiplierHeight;

    var arrow_down_icon = ResolveAssetSource(this.state.image.arrow_down_icon.url);

    image.arrow_down_icon.width = arrow_down_icon.width * this.state.image.arrow_down_icon.multiplierWidth;
    image.arrow_down_icon.height = arrow_down_icon.height * this.state.image.arrow_down_icon.multiplierHeight;

    var arrow_up_icon = ResolveAssetSource(this.state.image.arrow_up_icon.url);

    image.arrow_up_icon.width = arrow_up_icon.width * this.state.image.arrow_up_icon.multiplierWidth;
    image.arrow_up_icon.height = arrow_up_icon.height * this.state.image.arrow_up_icon.multiplierHeight;
    
    this.setState({image});
  }

  getContent(){
    let {accountOwner} = this.props;

    if (accountOwner) {
      return(
        <View
          style={{
            flex:1
          }}
        >
          <AccountDetail
              image={this.state.image}
              accountOwner={this.state.accountOwner}
          />
          

          <BalanceDetail
            image={this.state.image}
            seeCashfLow={this.seeCashfLow.bind(this)}
            showPopUp={this.showPopUp.bind(this)}
            depositSummary={this.state.depositSummary}
            kliknklinBanks={this.state.kliknklinBanks}
            accountOwner={this.state.accountOwner}
            {...this.props}
          />

        </View>
      );
    }
  }

  render(){
      return (
        <LayoutPrimary
            showTabBar={false}
            showBackButton={true}
            showOutletSelector={false}
            showSearchButton={false}
            navigator={this.props.navigator}
            showTitle={true}
            title={'Pengaturan Deposit'}
            contentContainerStyle={{
            }}
          >
            {this.getContent()}
        </LayoutPrimary>
      );
  }

  seeCashfLow = () => {
    this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.CASHFLOW);
  }

  showPopUp = (params) => {

    this.props.actionNavigator.showLightBox(CONSTANT.ROUTE_TYPE.DEPOSIT_LIGHT_BOX,{

      title: params.title,
      balance: params.balance,
      placeholder: params.placeholder,
      accountNumber: params.accountNumber,
      accountOwner: params.accountOwner,
      postscript: params.postscript,
      actiontype: params.actiontype,
      kliknklinBanks: params.kliknklinBanks,

      onPressButtonPrimary: async (actionType, kliknklin_bank_detail_id, amount, image_uri) => {

        if(actionType === 'withdrawBalance'){
          await this.props.actionDeposit.withdrawBalance(1, amount) // karena withdraw, kliknklin_bank_detail_id di buat 1, seharusnya tidak perlu ada
        }else{
          let payment_image_url = await this.props.actionUploadImage.uploadImage(image_uri);

          await this.props.actionDeposit.topUpBalance(kliknklin_bank_detail_id, amount, payment_image_url)
        }

        this.props.actionNavigator.dismissLightBox();
      },
      onPressButtonSecondary: () =>{
        this.props.actionNavigator.dismissLightBox();
      },
    })
  }
}

class AccountDetail extends Component{

  render(){

    let {name, account_holder_name, account_number, bank_image_url} = this.props.accountOwner;

    return(
      <View style={styles.accountDetail}>
        <View style={styles.icon_bank}>
          <Image
            style={{width: (bank_image_url == "" ? 0 : Scaling.moderateScale(120)), height: (bank_image_url == "" ? 0 : Scaling.moderateScale(40))}}
            source={(bank_image_url == "" ? this.props.image.wallet_icon.url : {uri: (bank_image_url == "" ? null : bank_image_url)})}
            resizeMode={'contain'}
          />
        </View>

        <View style={styles.accountDetailOwner}>
          <View style={styles.accountDetailRow}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>Rekening Owner</Text>
          </View>

          <View style={styles.accountDetailRow}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>{name}</Text>
          </View>

          <View style={styles.accountDetailRow}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>{account_number}</Text>
          </View>

          <View style={styles.accountDetailRow}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>a.n {account_holder_name}</Text>
          </View>
        </View>
      </View>
    )
  }
  /*
  <View style={styles.icon_edit}>
          <Image
            style={{width: this.props.image.edit_icon.width, height: this.props.image.edit_icon.height}}
            source={this.props.image.edit_icon.url}
          />
        </View>
  */
}

class BalanceDetail extends Component{
  onPressTNCDeposit(){
    this.props.actionNavigator.goToWebView(
      CONSTANT.WEB_LINKS.TNC_DEPOSIT_PARTNER,
      this.props.navigator
    )
  }

  render(){

    let {currentbalance, pendingtransactions, successtransactions} = this.props.depositSummary;
    console.log("currentbalance",currentbalance);

    return(
      <View style={styles.balanceDetail}>
        <View style={styles.currentSaldo}>

          <View style={styles.currentSaldoHeader}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>Saldo saat ini</Text>

            <TouchableOpacity style={styles.seeCashflowButton} onPress={this.seeCashfLow}>
              <Image
                style={{width: this.props.image.clock_icon.width, height: this.props.image.clock_icon.height}}
                source={this.props.image.clock_icon.url}
              />

              <View style={{marginLeft: Scaling.moderateScale(5)}}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>Lihat Cashflow Transaksi</Text>
              </View>

              <Image
                style={{width: this.props.image.arrow_right_blue.width, height: this.props.image.arrow_right_blue.height}}
                source={this.props.image.arrow_right_blue.url}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.balance}>
            <Image
              style={{width: this.props.image.wallet_icon.width, height: this.props.image.wallet_icon.height}}
              source={this.props.image.wallet_icon.url}
            />
            <View style={{marginLeft: Scaling.moderateScale(15)}}>
              <Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_PERCENTAGE_NUMBER} textStyle={{fontSize: Scaling.moderateScale(30)}}>
                {(currentbalance ? toCurrency(currentbalance,'Rp ') : '')}
              </Text>
            </View>
          </View>
        </View>

        <View style={styles.withdrawableBalance}>
          <View style={styles.accountDetailRow}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>Uang yang dapat dicairkan</Text>
          </View>

          <View style={styles.accountDetailRow}>
            <Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_PERCENTAGE_NUMBER}>
              {toCurrency(successtransactions,'Rp ')}
            </Text>
          </View>
        </View>

        <View style={styles.holdBalance}>
          <View style={styles.accountDetailRow}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>
              Saldo gantung
            </Text>
          </View>

          <View style={styles.accountDetailRow}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{fontFamily: CONSTANT.TEXT_FAMILY.MAIN_LIGHT}}>
              Saldo transaksi dari order yang belum dibayarkan customer
            </Text>
          </View>

          <View style={styles.accountDetailRow}>
            <Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_PERCENTAGE_NUMBER}>
              {toCurrency(pendingtransactions,'Rp ')}
            </Text>
          </View>
        </View>

        <View style={styles.button}>
          <Button.TypeD
            buttonText={'Tarik Deposit'}
            buttonColor={CONSTANT.COLOR.LIGHT_BLUE}
            buttonTextStyle={[styles.buttonTextStyle]}
            buttonStyle={styles.buttonStyle}
            onPress={this.withdrawBalance}
          />

          <Button.TypeD
            buttonText={'Tambah Deposit'}
            buttonColor={CONSTANT.COLOR.GREEN}
            buttonTextStyle={[styles.buttonTextStyle]}
            buttonStyle={styles.buttonStyle}
            onPress={this.addBalance}
          />
        </View>

        <TouchableOpacity style={styles.tnc} hitSlop={styles.hitSlop} onPress={this.onPressTNCDeposit.bind(this)}>
          <Image
            style={{width: this.props.image.info_icon.width, height: this.props.image.info_icon.height}}
            source={this.props.image.info_icon.url}
          />

          <Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_ROLE} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>Lihat Ketentuan Penarikan Deposit</Text>
        </TouchableOpacity>
      </View>
    )
  }

  seeCashfLow = () => {
    this.props.seeCashfLow();
  }

  _onChangeEmail = () => {

  }

  _onSubmitEditingEmail = () => {

  }

  withdrawBalance = () => {

    let {account_holder_name, account_number} = this.props.accountOwner;
    let {successtransactions} = this.props.depositSummary;

    let params = {
      actiontype: 'withdrawBalance',
      title: 'Anda melakukan pencairan deposit senilai :',
      balance: this.getMaxWithdrawable(successtransactions),
      placeholder: '0',
      accountNumber: account_number,
      accountOwner: account_holder_name,
      postscript: '',
      kliknklinBanks: this.props.kliknklinBanks
    }

    this.props.showPopUp(params);
  }

  getMaxWithdrawable(successtransactions){
    if(successtransactions - 20000 <= 20000){
      return '0';
    }else{
      let x = successtransactions - 20000;
      return x.toString();
    }
  }

  addBalance = () => {

    let params = {
      actiontype: 'addBalance',
      title: 'Anda melakukan penambahan deposit senilai :',
      balance: '',
      placeholder: 'min: Rp 20.000',
      accountNumber: '',
      accountOwner: '',
      postscript: 'segera upload bukti transfer setelah transaksi berhasil',
      kliknklinBanks: this.props.kliknklinBanks
    }

    this.props.showPopUp(params);

  }

}


function mapStateToProps(state, ownProps) {
  return {
    layoutHeight:state.rootReducer.layout.height,
    userRole:state.rootReducer.user.role,
    depositSummary: state.rootReducer.deposit.summary,
    accountOwner: state.rootReducer.deposit.accountOwner,
    userData: state.rootReducer.user.data,
    kliknklinBanks: state.rootReducer.deposit.kliknklinBanks
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionDeposit: bindActionCreators(actions.deposit, dispatch),
    actionUploadImage: bindActionCreators(actions.uploadImage, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Deposit);

const styles = StyleSheet.create({
  accountDetail:{
    flexDirection: 'row',
    paddingHorizontal:Dimensions.get('window').width * 0.05,
    height: Scaling.moderateScale(140),
    backgroundColor: CONSTANT.COLOR.WHITE,
    elevation: 1
  },
  icon_bank:{
    flex: 0.37,
    justifyContent: 'center',
    alignItems: 'center'
  },
  accountDetailOwner:{
    flex: 0.5,
    justifyContent: 'center',
  },
  icon_edit:{
    flex: 0.13,
    alignItems: 'center',
    paddingTop: Scaling.moderateScale(5)
  },
  accountDetailRow:{
    paddingVertical: Scaling.moderateScale(3)
  },
  balanceDetail:{
    flex: 1,
    marginTop: Scaling.moderateScale(20),
    paddingHorizontal:Dimensions.get('window').width * 0.05,
    backgroundColor: CONSTANT.COLOR.WHITE
  },
  currentSaldo:{
    marginBottom: Scaling.moderateScale(35)
  },
  currentSaldoHeader:{
    marginTop: Scaling.moderateScale(15),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  seeCashflowButton:{
    flexDirection: 'row',
    alignItems: 'center'
  },
  balance:{
    marginTop: Scaling.moderateScale(35),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  editBalance:{
    marginBottom: Scaling.moderateScale(30),
  },
  withdrawableBalance:{
    marginTop: Scaling.moderateScale(20),
    marginBottom: Scaling.moderateScale(30)
  },
  holdBalance:{
    marginBottom: Scaling.moderateScale(35)
  },
  button:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: Scaling.moderateScale(35)
  },
  buttonTextStyle:{
    color: CONSTANT.COLOR.WHITE, 
    fontSize: Scaling.moderateScale(13), 
    fontFamily: CONSTANT.TEXT_FAMILY.MAIN_SEMI_BOLD
  },
  buttonStyle:{
    borderRadius: Scaling.moderateScale(10),
    width:Dimensions.get('window').width * 0.5 * 0.82,
    height:CONSTANT.STYLE.BUTTON.SMALL.HEIGHT
  },
  tnc: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: Scaling.moderateScale(35)
  },
  hitSlop: {
    top: 5, 
    bottom: 5, 
    left: 5, 
    right: 5
  }
})

