import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Image,
        StyleSheet,
        NativeModules,
        LayoutAnimation
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';
import ImagePicker from 'react-native-image-picker';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Form, TabBar, OrderStatus, Avatar, Scaling, DateFormat, DepositStatus, Label} from 'app/data/components';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

const FirstTab = {
  key: 0,
  data: 'Transactions'
};

const SecondTab = {
  key: 1,
  data: 'Request'
};

class Cashflow extends Component{

  constructor(props){
    super(props);
    this.state={
      image:{
        no_data_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/deposit/no_data_icon.png'),
        },
        arrow_right_blue:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('app/data/image/attendance/arrow_right_blue.png'),
        },
        raindrop_icon:{
            width:0,
            height:0,
            multiplierWidth:0.14,
            multiplierHeight:0.14,
            url:require('app/data/image/iot_machine/raindrop_icon.png'),
        },
        temperature_icon:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('app/data/image/iot_machine/temperature_icon.png'),
        },
        setting_icon:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('app/data/image/icon/setting_icon.png'),
        },
        arrow_down_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/iot_machine/arrow_down_icon.png'),
        },
        arrow_up_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/iot_machine/arrow_up_icon.png'),
        },
        info_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/deposit/info_icon.png'),
        }
      },
      dateOfStatistics:{
        startDate: '',
        startDateIso: '',
        endDate: '',
        endDateIso: ''
      },
      showContent:{
        key: FirstTab.key,
        data: FirstTab.data,
      },
      contentList: [],
      cashFlow: [],
      wdrRequest: []
    }
  }

  componentDidMount(){
    this.setDateOfStatistics();
    this.getCashflowTransaction();
    this.getWdrRequest();
    this.getImageSize();
  }

  setDateOfStatistics(){

    let today = new Date();
    let today_fullDate = DateFormat(today, 'fullDate');
    let today_isoDate = DateFormat(today, 'isoDate');

    let sevenDayBefore = new Date(today.getTime());
    sevenDayBefore.setDate(sevenDayBefore.getDate() - 7);
    let sevenDayBefore_fullDate = DateFormat(sevenDayBefore, 'fullDate');
    let sevenDayBefore_isoDate = DateFormat(sevenDayBefore, 'isoDate');

    let obj = {}
          
    obj.startDate = sevenDayBefore_fullDate;
    obj.startDateIso = sevenDayBefore_isoDate;
    obj.endDate = today_fullDate;
    obj.endDateIso = today_isoDate;

    this.state.dateOfStatistics = obj;

  }

  getImageSize(){
    var image = this.state.image;

    var arrow_right_blue = ResolveAssetSource(this.state.image.arrow_right_blue.url);

    image.arrow_right_blue.width = arrow_right_blue.width * this.state.image.arrow_right_blue.multiplierWidth;
    image.arrow_right_blue.height = arrow_right_blue.height * this.state.image.arrow_right_blue.multiplierHeight;

    var setting_icon = ResolveAssetSource(this.state.image.setting_icon.url);

    image.setting_icon.width = setting_icon.width * this.state.image.setting_icon.multiplierWidth;
    image.setting_icon.height = setting_icon.height * this.state.image.setting_icon.multiplierHeight;

    var raindrop_icon = ResolveAssetSource(this.state.image.raindrop_icon.url);

    image.raindrop_icon.width = raindrop_icon.width * this.state.image.raindrop_icon.multiplierWidth;
    image.raindrop_icon.height = raindrop_icon.height * this.state.image.raindrop_icon.multiplierHeight;

    var temperature_icon = ResolveAssetSource(this.state.image.temperature_icon.url);

    image.temperature_icon.width = temperature_icon.width * this.state.image.temperature_icon.multiplierWidth;
    image.temperature_icon.height = temperature_icon.height * this.state.image.temperature_icon.multiplierHeight;

    var arrow_down_icon = ResolveAssetSource(this.state.image.arrow_down_icon.url);

    image.arrow_down_icon.width = arrow_down_icon.width * this.state.image.arrow_down_icon.multiplierWidth;
    image.arrow_down_icon.height = arrow_down_icon.height * this.state.image.arrow_down_icon.multiplierHeight;

    var arrow_up_icon = ResolveAssetSource(this.state.image.arrow_up_icon.url);

    image.arrow_up_icon.width = arrow_up_icon.width * this.state.image.arrow_up_icon.multiplierWidth;
    image.arrow_up_icon.height = arrow_up_icon.height * this.state.image.arrow_up_icon.multiplierHeight;

    var no_data_icon = ResolveAssetSource(this.state.image.no_data_icon.url);

    image.no_data_icon.width = Scaling.moderateScale(no_data_icon.width * this.state.image.no_data_icon.multiplierWidth);
    image.no_data_icon.height = Scaling.moderateScale(no_data_icon.height * this.state.image.no_data_icon.multiplierHeight);

    var info_icon = ResolveAssetSource(this.state.image.info_icon.url);

    image.info_icon.width = info_icon.width * this.state.image.info_icon.multiplierWidth;
    image.info_icon.height = info_icon.height * this.state.image.info_icon.multiplierHeight;
    
    this.setState({image});
  }

  async getCashflowTransaction(){
    let {startDateIso, endDateIso } = this.state.dateOfStatistics;

    await this.props.actionDeposit.getCashflowTransaction(startDateIso, endDateIso);
    this.setState({ cashFlow: this.props.cashFlow, contentList: this.props.cashFlow });
  }

  async getWdrRequest(){
    await this.props.actionDeposit.getWdrRequest();
    this.setState({ wdrRequest: this.props.wdrRequest }); 
  }

  render(){
      return (
        <LayoutPrimary
            showTabBar={false}
            showTitle={true}
            showBackButton={true}
            showOutletSelector={false}
            showSearchButton={false}
            navigator={this.props.navigator}
            title={'Cashflow Transaksi'}
            contentContainerStyle={{
            }}
          >
            <View
              style={{
                flex:1,
                alignItems: 'center'
              }}
            >

              <View
                style={{
                  flex:1,
                  paddingTop: Scaling.moderateScale(20),
                  width:Dimensions.get('window').width
                }}
              >
                <TabBar.TypeD
                  item={[
                    {
                      itemText:FirstTab.data,
                    },
                    {
                      itemText:SecondTab.data,
                    },
                  ]}
                  onPress={this._onChangeTab.bind(this)}
                />

                <StatisticsHeader 
                  image={this.state.image}
                  setDateRange={this.setDateRange.bind(this)}
                  dateOfStatistics={this.state.dateOfStatistics}
                  showContent={this.state.showContent}
                />
                
                {this.renderX()}

              </View>
            </View>
        </LayoutPrimary>
      );
  }

  renderX(){
    if(this.state.contentList.length == 0){
      return(
        <NoData
          image={this.state.image}
        />
      )
    }else{
      return(
        <FlatList 
          showsVerticalScrollIndicator={false}
          extraData={this.state}
          data={this.state.contentList}
          keyExtractor={(item, index) => index+''}
          renderItem={(item) => { return (
            this.renderContent(item)
          );}}
        />
      )
    }
  }

  _onChangeTab(key, data){
    let showContent = this.state.showContent;

    showContent.key = key;
    showContent.data = data;

    const CustomLayoutLinear = {
        duration: 200,
        create: {
            type: LayoutAnimation.Types.linear,
            property: LayoutAnimation.Properties.opacity
        },
        update: {
            type: LayoutAnimation.Types.linear
        },
    };

    LayoutAnimation.configureNext(CustomLayoutLinear);

    if(key == 0){ //firstTab
      this.state.contentList = this.props.cashFlow;
    }else{ // secondTab
      this.state.contentList = this.props.wdrRequest;
    }

    this.setState({ showContent });
  }

  renderContent(item){
    if(this.state.showContent.key === FirstTab.key){
      
      if(item.item.category === 2){
        return(
          <Deposit
            image={this.state.image}
            cashFlow={item.item}
            isShowPriceDetail={(item.index == 0 ? true : false)}
          />
        )
      }else if(item.item.category === 1){
        return(
          <Transaction
            image={this.state.image}
            cashFlow={item.item}
            isShowPriceDetail={(item.index == 0 ? true : false)}
          />
        )
      }else{
        return(
          <Others // Revert Order Transaction, Complaint Charge, Super Admin Privileges
            image={this.state.image}
            cashFlow={item.item}
            isShowPriceDetail={(item.index == 0 ? true : false)}
          />
        )
      }
      
    }else{
      return(
        <WDR
          image={this.state.image}
          wdrRequest={item.item}
          cashFlow={item.item}
          navigator={this.props.navigator}
          actionNavigator={this.props.actionNavigator}
        />
      )
    }
  }

  setDateRange(){

    this.props.actionNavigator.showLightBox(CONSTANT.ROUTE_TYPE.DATE_RANGE_PICKER,{
        
        onPressButtonPrimary: async (startDate, endDate) => {

          this.props.actionNavigator.dismissLightBox();

          let startDate_fullDate = DateFormat(startDate, 'fullDate');
          let startDate_isoDate = DateFormat(startDate, 'isoDate');

          this.state.dateOfStatistics.startDate = startDate_fullDate;

          let endDate_fullDate = DateFormat(endDate, 'fullDate');
          let endDate_isoDate = DateFormat(endDate, 'isoDate');

          this.state.dateOfStatistics.endDate = endDate_fullDate;

          let obj = {}
          
          obj.startDate = startDate_fullDate;
          obj.startDateIso = startDate_isoDate;
          obj.endDate = endDate_fullDate;
          obj.endDateIso = endDate_isoDate;

          this.state.dateOfStatistics = obj;

          this.getCashflowTransaction();

        },
        onPressButtonSecondary: () =>{
          this.props.actionNavigator.dismissLightBox();
        },
    })
  }
}

class StatisticsHeader extends Component{
  render(){

    if(this.props.showContent.key == 0){ // Transactions tab
      return(
        <View style={styles.statisticsHeader}>
          <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>Periode</Text>
          
          <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center'}} hitSlop={{top: 5, left: 5, bottom: 5, right: 5}}
            onPress={this.showDatePicker}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>{this.props.dateOfStatistics.startDate} - {this.props.dateOfStatistics.endDate}</Text>
            
            <View style={{marginLeft: Scaling.moderateScale(5)}}>
              <Image
                style={{width: this.props.image.arrow_right_blue.width, height: this.props.image.arrow_right_blue.height}}
                source={this.props.image.arrow_right_blue.url}
              />
            </View>
          </TouchableOpacity>
        </View>
      )
    }else{
      return null
    }
  }

  showDatePicker = () => {
    this.props.setDateRange();
  }
}

class Transaction extends Component{

  constructor(props){
    super(props);
    this.state={
      isShowPriceDetail: false
    }
  }

  componentDidMount(){
    this.setState({isShowPriceDetail:this.props.isShowPriceDetail})
  }

  render(){

    let cashFlow = this.props.cashFlow;

    return(
      <View style={styles.contentContainer}>
        <View>
          <View style={styles.orderDetailHeader}>
            <Text fontOption={CONSTANT.TEXT_OPTION.HEADER} textStyle={{color: CONSTANT.COLOR.BLACK}}>{cashFlow.transaction_hash}</Text>
            {this.saldoTransaction()}
          </View>

          <View style={styles.orderDetailCustomer}>
            <View style={styles.customerLabel}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Customer</Text>
            </View>

            <View style={styles.customerName}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{cashFlow.customer_name}</Text>
            </View>

          </View>

          <View style={styles.orderDetailPayment}>
            <View style={styles.customerLabel}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Metode Bayar</Text>
            </View>

            <View style={styles.customerName}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{cashFlow.payment_method} ({cashFlow.is_paid})</Text>
            </View>
          </View>
            
          <TouchableOpacity style={styles.rincianRow} onPress={this.showDetail}>
            <View style={styles.rincianLabel}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>Rincian</Text>
            </View>

            {this.renderIconArrow()}
          </TouchableOpacity>

          {this.renderPriceDetailTransaction(cashFlow)} 
        </View>
      </View>
    );
  }

  saldoTransaction(){

    let {balance} = this.props.cashFlow;
    let color = CONSTANT.COLOR.GREEN;

    return(
      <Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} textStyle={{color: color}}>Saldo: {balance}</Text>
    )
  }

  renderIconArrow(){
    if(this.state.isShowPriceDetail){
      return(
        <Image
          style={{width: this.props.image.arrow_down_icon.width, height: this.props.image.arrow_down_icon.height}}
          source={this.props.image.arrow_down_icon.url}
        />
      ) 
    }
    return(
      <Image
        style={{width: this.props.image.arrow_up_icon.width, height: this.props.image.arrow_up_icon.height}}
        source={this.props.image.arrow_up_icon.url}
      />
    ) 
  }

  renderPriceDetailTransaction(cashFlow){

    if(this.state.isShowPriceDetail){

      if(cashFlow.payment_method === 'Cash'){
        return this.renderTransactionCash(cashFlow)
      }else{
        return this.renderTransactionTransfer(cashFlow)
      }
    }
    
    return null
  }

  renderTransactionCash(cashFlow){
    return(
      <View style={styles.priceDetail}>

        <View style={styles.priceDetailTop}>

          <View style={styles.priceDetailRow}>
            <View style={styles.priceDetailLeftColumn}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Saldo Terakhir</Text>
            </View>

            <View style={styles.priceDetailRightColumn}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{cashFlow.last_balance}</Text>
            </View>
          </View>

          <View style={styles.priceDetailRow}>
            <View style={styles.priceDetailLeftColumn}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Bagi Hasil Knk</Text>
            </View>

            <View style={styles.priceDetailRightColumn}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>-{cashFlow.knk_profit}</Text>
            </View>
          </View>

          <View style={styles.priceDetailRow}>
            <View style={styles.priceDetailLeftColumn}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Diskon Knk</Text>
            </View>

            <View style={styles.priceDetailRightColumn}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>+{cashFlow.marketing_cost_kliknklin}</Text>
            </View>
          </View>
        </View>

        <View style={styles.priceDetailBottom}>
          <View style={styles.priceDetailRow}>
            <View style={styles.priceDetailLeftColumn}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Saldo Deposit</Text>
            </View>

            <View style={styles.priceDetailRightColumn}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{cashFlow.balance}</Text>
            </View>
          </View>
        </View>
      </View>
    ) 
  }

  renderTransactionTransfer(cashFlow){
    return(
      <View style={styles.priceDetail}>

        <View style={styles.priceDetailTop}>

          <View style={styles.priceDetailRow}>
            <View style={styles.priceDetailLeftColumn}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Saldo Terakhir</Text>
            </View>

            <View style={styles.priceDetailRightColumn}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{cashFlow.last_balance}</Text>
            </View>
          </View>

          <View style={styles.priceDetailRow}>
            <View style={styles.priceDetailLeftColumn}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Bagi Hasil Outlet</Text>
            </View>

            <View style={styles.priceDetailRightColumn}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>+{cashFlow.outlet_profit}</Text>
            </View>
          </View>

          <View style={styles.priceDetailRow}>
            <View style={styles.priceDetailLeftColumn}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Antar Jemput</Text>
            </View>

            <View style={styles.priceDetailRightColumn}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>+{cashFlow.shipment_fare}</Text>
            </View>
          </View>

          <View style={styles.priceDetailRow}>
            <View style={styles.priceDetailLeftColumn}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Diskon Outlet</Text>
            </View>

            <View style={styles.priceDetailRightColumn}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>-{cashFlow.marketing_cost_outlet}</Text>
            </View>
          </View>
        </View>

        <View style={styles.priceDetailBottom}>
          <View style={styles.priceDetailRow}>
            <View style={styles.priceDetailLeftColumn}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Saldo Deposit</Text>
            </View>

            <View style={styles.priceDetailRightColumn}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{cashFlow.balance}</Text>
            </View>
          </View>
        </View>
      </View>
    ) 
  }

  showDetail = () => {

    const CustomLayoutLinear = {
        duration: 200,
        create: {
            type: LayoutAnimation.Types.linear,
            property: LayoutAnimation.Properties.opacity
        },
        update: {
            type: LayoutAnimation.Types.linear
        },
    };

    LayoutAnimation.configureNext(CustomLayoutLinear);

    this.setState({isShowPriceDetail: !this.state.isShowPriceDetail})
  }
}

class Deposit extends Component{

  constructor(props){
    super(props);
    this.state={
      isShowPriceDetail: false
    }
  }

  componentDidMount(){
    this.setState({isShowPriceDetail:this.props.isShowPriceDetail})
  }

  render(){
    
    let cashFlow = this.props.cashFlow;

    return(
      <View style={styles.contentContainer}>
        <View>
          <View style={styles.orderDetailHeader}>
            <Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} textStyle={{color: CONSTANT.COLOR.BLACK}}>{cashFlow.transaction_hash}</Text>
            {this.saldoDeposit()}
          </View>

          <View style={styles.orderDetailCustomer}>
            <View style={styles.customerLabel}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Bank</Text>
            </View>

            <View style={styles.customerName}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{cashFlow.bank_name}</Text>
            </View>

          </View>

          <View style={styles.orderDetailPayment}>
            <View style={styles.customerLabel}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>No Rek</Text>
            </View>

            <View style={styles.customerName}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{cashFlow.account_number}</Text>
            </View>
          </View>
            
          <TouchableOpacity style={styles.rincianRow} onPress={this.showDetail}>
            <View style={styles.rincianLabel}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>Rincian</Text>
            </View>

            {this.renderIconArrow()}
          </TouchableOpacity>

          {this.renderPriceDetailDeposit()} 
        </View>
      </View>
    );
  }

  saldoDeposit(){

    let {balance} = this.props.cashFlow;
    
    let color = CONSTANT.COLOR.GREEN;

    return(
      <Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} textStyle={{color: color}}>Saldo: {balance}</Text>
    )
  }

  renderIconArrow(){
    if(this.state.isShowPriceDetail){
      return(
        <Image
          style={{width: this.props.image.arrow_down_icon.width, height: this.props.image.arrow_down_icon.height}}
          source={this.props.image.arrow_down_icon.url}
        />
      ) 
    }
    return(
      <Image
        style={{width: this.props.image.arrow_up_icon.width, height: this.props.image.arrow_up_icon.height}}
        source={this.props.image.arrow_up_icon.url}
      />
    ) 
  }

  renderPriceDetailDeposit(){

    if(this.state.isShowPriceDetail){
      let cashFlow = this.props.cashFlow;
      return(
        <View style={styles.priceDetail}>

          <View style={styles.priceDetailTop}>

            <View style={styles.priceDetailRow}>
              <View style={styles.priceDetailLeftColumn}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Saldo Terakhir</Text>
              </View>

              <View style={styles.priceDetailRightColumn}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{cashFlow.last_balance}</Text>
              </View>
            </View>

            <View style={styles.priceDetailRow}>
              <View style={styles.priceDetailLeftColumn}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Total Penarikan</Text>
              </View>

              <View style={styles.priceDetailRightColumn}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>-{cashFlow.credit}</Text>
              </View>
            </View>

            <View style={styles.priceDetailRow}>
              <View style={styles.priceDetailLeftColumn}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Total Penambahan</Text>
              </View>

              <View style={styles.priceDetailRightColumn}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>+{cashFlow.debit}</Text>
              </View>
            </View>
          </View>

          <View style={styles.priceDetailBottom}>
            <View style={styles.priceDetailRow}>
              <View style={styles.priceDetailLeftColumn}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Saldo Deposit</Text>
              </View>

              <View style={styles.priceDetailRightColumn}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{cashFlow.balance}</Text>
              </View>
            </View>
          </View>
        </View>
      ) 
    }
    
    return null
  }

  showDetail = () => {

    const CustomLayoutLinear = {
        duration: 200,
        create: {
            type: LayoutAnimation.Types.linear,
            property: LayoutAnimation.Properties.opacity
        },
        update: {
            type: LayoutAnimation.Types.linear
        },
    };

    LayoutAnimation.configureNext(CustomLayoutLinear);

    this.setState({isShowPriceDetail: !this.state.isShowPriceDetail})
  }
}

class Others extends Component{
  constructor(props){
    super(props);
    this.state={
      isShowPriceDetail: false
    }
  }

  componentDidMount(){
    this.setState({isShowPriceDetail:this.props.isShowPriceDetail})
  }

  render(){
    
    let cashFlow = this.props.cashFlow;

    return(
      <View style={styles.contentContainer}>
        <View>
          <View style={styles.orderDetailHeader}>
            <Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} textStyle={{color: CONSTANT.COLOR.BLACK}}>{cashFlow.transaction_hash}</Text>
            {this.saldoDeposit()}
          </View>

          <View style={styles.orderDetailCustomer}>
            <View style={styles.customerLabel}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Info</Text>
            </View>

            <View style={styles.customerName}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{cashFlow.info}</Text>
            </View>
          </View>
            
          <TouchableOpacity style={styles.rincianRow} onPress={this.showDetail}>
            <View style={styles.rincianLabel}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>Rincian</Text>
            </View>

            {this.renderIconArrow()}
          </TouchableOpacity>

          {this.renderPriceDetail()} 
        </View>
      </View>
    );
  }

  saldoDeposit(){

    let {balance} = this.props.cashFlow;
    
    let color = CONSTANT.COLOR.GREEN;

    return(
      <Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} textStyle={{color: color}}>Saldo: {balance}</Text>
    )
  }

  renderIconArrow(){
    if(this.state.isShowPriceDetail){
      return(
        <Image
          style={{width: this.props.image.arrow_down_icon.width, height: this.props.image.arrow_down_icon.height}}
          source={this.props.image.arrow_down_icon.url}
        />
      ) 
    }
    return(
      <Image
        style={{width: this.props.image.arrow_up_icon.width, height: this.props.image.arrow_up_icon.height}}
        source={this.props.image.arrow_up_icon.url}
      />
    ) 
  }

  renderPriceDetail(){

    if(this.state.isShowPriceDetail){
      let cashFlow = this.props.cashFlow;
      return(
        <View style={styles.priceDetail}>

          <View style={styles.priceDetailTop}>

            <View style={styles.priceDetailRow}>
              <View style={styles.priceDetailLeftColumn}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Saldo Terakhir</Text>
              </View>

              <View style={styles.priceDetailRightColumn}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{cashFlow.last_balance}</Text>
              </View>
            </View>

            <View style={styles.priceDetailRow}>
              <View style={styles.priceDetailLeftColumn}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Total Penambahan</Text>
              </View>

              <View style={styles.priceDetailRightColumn}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>+{cashFlow.amount}</Text>
              </View>
            </View>
          </View>

          <View style={styles.priceDetailBottom}>
            <View style={styles.priceDetailRow}>
              <View style={styles.priceDetailLeftColumn}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Saldo Deposit</Text>
              </View>

              <View style={styles.priceDetailRightColumn}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{cashFlow.balance}</Text>
              </View>
            </View>
          </View>
        </View>
      ) 
    }
    
    return null
  }

  showDetail = () => {

    const CustomLayoutLinear = {
        duration: 200,
        create: {
            type: LayoutAnimation.Types.linear,
            property: LayoutAnimation.Properties.opacity
        },
        update: {
            type: LayoutAnimation.Types.linear
        },
    };

    LayoutAnimation.configureNext(CustomLayoutLinear);

    this.setState({isShowPriceDetail: !this.state.isShowPriceDetail})
  }
}

class WDR extends Component{

  constructor(props){
    super(props);
    this.state={
    }
  }

  render(){

    let wdrRequest = this.props.wdrRequest;
    return(
      <View style={[styles.contentContainer, {marginBottom: Scaling.verticalScale(1)}]}>
        <View style={styles.orderDetailHeader}>
          <DepositStatus depositStatus={wdrRequest.type_id} amount={wdrRequest.amount}/>
          <RequestStatus requestStatus={wdrRequest.type_id} />
        </View>

        <View style={styles.orderDetailCustomer}>
          <View style={styles.customerLabel}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Bank</Text>
          </View>

          <View style={styles.customerName}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{wdrRequest.bank_name}</Text>
          </View>

        </View>

        <View style={styles.orderDetailPayment}>
          <View style={styles.customerLabel}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>No Rek</Text>
          </View>

          <View style={styles.customerName}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{wdrRequest.account_number}</Text>
          </View>
        </View>

        {wdrRequest.type_id == CONSTANT.DEPOSIT_STATUS.DEBIT ?
          <View style={styles.orderDetailPayment}>
            <View style={styles.customerLabel}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>Bukti Transfer</Text>
            </View>

            <TouchableOpacity
              onPress={() => this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.WEBVIEW, {
                  title: 'Bukti Transfer',
                  url: wdrRequest.payment_image_url
              })}
              style={styles.customerName}>
              <Text numberOfLines={1} enum={'tail'} fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT_LIGHT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>{wdrRequest.payment_image_url}</Text>
            </TouchableOpacity>

          </View>
          :
          null
        }
      </View>
    );
  }

  saldo(){
    let {is_withdraw, amount} = this.props.wdrRequest;
    let color = CONSTANT.COLOR.GREEN;

    if(is_withdraw){
      color = CONSTANT.COLOR.RED;
    }

    return(
      <Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} textStyle={{color: color}}>Saldo: {amount}</Text>
    )
  }
}

class NoData extends Component{
  render(){
    return(
      <View style={styles.NoData_container}>

        <View style={styles.NoData_content}>
          <View style={styles.NoData_info1}>
              <Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} textStyle={{textAlign: 'center', color: CONSTANT.COLOR.LIGHT_BLACK}}>Ups, tidak ada transaksi</Text>
          </View>

          <Image
              style={{width: this.props.image.no_data_icon.width, height: this.props.image.no_data_icon.height}}
              source={this.props.image.no_data_icon.url}
          />

          <View style={styles.NoData_info2}>
            <Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} textStyle={{textAlign: 'center', color: CONSTANT.COLOR.LIGHT_BLACK}}>Transaksi berkaitan dengan pertambahan atau</Text>
            <Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} textStyle={{textAlign: 'center', color: CONSTANT.COLOR.LIGHT_BLACK}}>pengurangan jumlah uang deposit yang</Text>
            <Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} textStyle={{textAlign: 'center', color: CONSTANT.COLOR.LIGHT_BLACK}}>terdapat pada akun pemilik</Text>
          </View>
        </View>

        <View style={styles.moreInfo}>
          <Image
            style={{width: this.props.image.info_icon.width, height: this.props.image.info_icon.height}}
            source={this.props.image.info_icon.url}
          />

          <TouchableOpacity style={{marginLeft: Scaling.moderateScale(10)}}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{textAlign: 'center', color: CONSTANT.COLOR.LIGHT_BLUE, fontWeight: CONSTANT.TEXT_WEIGHT.REGULAR.weight}}>informasi lebih lanjut</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

class RequestStatus extends Component{
  constructor(props) {
      super(props);
      this.state = {
      };
    }

  _getData(){
    let result = {
        labelColor:'transparent',
        labelText:'label',
        labelTextColor:'transparent',
    };

    let {requestStatus} = this.props;

    if(requestStatus == CONSTANT.DEPOSIT_STATUS.CREDIT) { 
      result.labelColor =  CONSTANT.COLOR.LIGHTER_BLUE; 
      result.labelTextColor =  CONSTANT.COLOR.LIGHT_BLUE; 
      result.labelText = 'Menunggu diproses';
    } // biru
    else if (requestStatus ==CONSTANT.DEPOSIT_STATUS.DEBIT) {
      result.labelColor =  CONSTANT.COLOR.LIGHTER_BLUE; 
      result.labelTextColor =  CONSTANT.COLOR.LIGHT_BLUE; 
      result.labelText = 'Menunggu Persetujuan';
    }
    
    return result;
  }

  render(){
    let {labelColor, labelText, labelTextColor} = this._getData();
    return(
        <Label.TypeA 
          labelText={labelText}
          labelColor={labelColor}
          labelTextColor={labelTextColor}
          containerStyle={{width:Scaling.scale(180)}}
        />
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    cashFlow: state.rootReducer.deposit.cashFlow,
    wdrRequest: state.rootReducer.deposit.wdrRequest
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionDeposit: bindActionCreators(actions.deposit, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Cashflow);

const styles = StyleSheet.create({
  
  statisticsHeader:{
    marginTop: Scaling.moderateScale(20),
    justifyContent: 'center',
    paddingTop: Dimensions.get('window').width * 0.035,
    paddingBottom: Dimensions.get('window').width * 0.035,
    paddingHorizontal: Dimensions.get('window').width * 0.05,
    backgroundColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BACKGROUND_COLOR,
    borderBottomWidth:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_BOTTOM_WIDTH,
    borderColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_COLOR,
    elevation:1
  },
  statisticsHeaderDate:{
    flexDirection: 'row', 
    alignItems: 'center'
  },
  contentContainer:{
    backgroundColor:'white',
    elevation:2,
    marginTop: Scaling.moderateScale(20),
    marginHorizontal: Scaling.moderateScale(2),
    paddingTop: Dimensions.get('window').width * 0.025,
    paddingBottom: Dimensions.get('window').width * 0.025,
    paddingHorizontal: Dimensions.get('window').width * 0.05
  },
  orderDetailHeader:{
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  orderDetailCustomer:{
    marginTop: Scaling.moderateScale(15),
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  orderDetailPayment:{
    marginTop: Scaling.moderateScale(5),
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  customerLabel:{
    width: '40%',
    alignItems: 'flex-start'
  },
  customerName:{
    width: '60%',
    alignItems: 'flex-start'
  },
  rincianRow:{
    marginTop:Scaling.moderateScale(10),
    flexDirection: 'row',
    alignItems: 'center'
  },
  rincianLabel:{
    marginRight: Scaling.moderateScale(10)
  },
  priceDetail:{
    backgroundColor: CONSTANT.COLOR.LIGHT_GRAY,
    padding: Scaling.moderateScale(15),
    borderRadius: CONSTANT.STYLE.BUTTON.SMALL.RADIUS
  },
  priceDetailRow:{
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%'
  },
  priceDetailLeftColumn: {
    width: '65%'
  },
  priceDetailRightColumn: {
    width: '35%',
    alignItems: "flex-end"
  },
  priceDetailTop:{
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: CONSTANT.COLOR.LIGHT_BLACK,
    paddingBottom: Scaling.moderateScale(10)
  },
  priceDetailBottom:{
    paddingTop: Scaling.moderateScale(10)
  },

  machineDetail:{
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center'
  },
  settingIcon:{
    width:'50%',
    alignItems: 'flex-end',
    paddingRight: Scaling.moderateScale(5)
  },
  usingOfMachineDetailHeader:{
    width: '100%', 
    flexDirection: 'row'
  },
  startTimeHeader:{
    width: '50%', 
    paddingBottom: Scaling.moderateScale(8), 
    justifyContent: 'center'
  },
  endTimeHeader:{
    width: '50%', 
    paddingBottom: Scaling.moderateScale(8), 
    justifyContent: 'center', 
    paddingLeft: Scaling.moderateScale(10)
  },
  startTime:{
    width: '50%', 
    paddingTop: Scaling.moderateScale(7), 
    paddingBottom: Scaling.moderateScale(7), 
    justifyContent: 'center', 
    borderRightWidth: StyleSheet.hairlineWidth, 
    borderColor: CONSTANT.COLOR.LIGHT_GRAY, 
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  endTime:{
    width: '50%', 
    paddingTop: Scaling.moderateScale(7), 
    paddingBottom: Scaling.moderateScale(7), 
    justifyContent: 'center', 
    paddingLeft: Scaling.moderateScale(10), 
    borderColor: CONSTANT.COLOR.LIGHT_GRAY, 
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  machineDetailRow:{
    flex: 1, flexDirection: 'row', justifyContent: 'space-between'
  },
  detailPenggunaanText:{
    paddingRight: Scaling.moderateScale(20), 
    marginBottom: Scaling.moderateScale(2), 
    justifyContent: 'flex-end'
  },
  detailPenggunaanIcon:{
    position: 'absolute', 
    bottom: -1, 
    right: 0, 
    zIndex: -1
  },
  hitSlop:{
    top: 5, 
    bottom: 5, 
    left: 5, 
    right: 5
  },

  NoData_container: {
    flex: 1
  },
  NoData_content: {
    marginTop: Scaling.moderateScale(40),
    alignItems: 'center'
  },
  NoData_info1:{
    marginBottom: Scaling.moderateScale(10)
  },
  NoData_info2:{
    marginTop: Scaling.moderateScale(10)
  },
  moreInfo:{
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: Scaling.moderateScale(30)
  }
})

