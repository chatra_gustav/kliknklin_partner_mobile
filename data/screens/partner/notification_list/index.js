import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Image,
        StyleSheet,
        NativeModules,
        LayoutAnimation
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';
import ImagePicker from 'react-native-image-picker';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Form, TabBar, OrderStatus, Avatar, Scaling, DateFormat} from 'app/data/components';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from 'app/data/actions';

import Notification from '../notification_content'

const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

class NotificationList extends Component{

  constructor(props){
    super(props);
    this.state={
      image:{
        empty_message_icon:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('app/data/image/notification/empty_message_icon.png')
        },
        empty_notification_icon:{
            width:0,
            height:0,
            multiplierWidth:0.4,
            multiplierHeight:0.4,
            url:require('app/data/image/notification/empty_notification_icon.png')
        },
        new_notif_icon:{
            width:0,
            height:0,
            multiplierWidth:0.35,
            multiplierHeight:0.35,
            url:require('app/data/image/notification/new_notif_icon.png'),
        }
      },
      showContent: CONSTANT.NOTIFICATION_TAB_TYPE.FIRST,
    }
  }

  componentDidMount(){
    this.props.actionRoot.getNotification();

    this.getImageSize();
  }

  getImageSize(){
    var image = this.state.image;

    var empty_notification_icon = ResolveAssetSource(this.state.image.empty_notification_icon.url);

    image.empty_notification_icon.width = empty_notification_icon.width * this.state.image.empty_notification_icon.multiplierWidth;
    image.empty_notification_icon.height = empty_notification_icon.height * this.state.image.empty_notification_icon.multiplierHeight;

    var empty_message_icon = ResolveAssetSource(this.state.image.empty_message_icon.url);

    image.empty_message_icon.width = empty_message_icon.width * this.state.image.empty_message_icon.multiplierWidth;
    image.empty_message_icon.height = empty_message_icon.height * this.state.image.empty_message_icon.multiplierHeight;

    var new_notif_icon = ResolveAssetSource(this.state.image.new_notif_icon.url);

    image.new_notif_icon.width = new_notif_icon.width * this.state.image.new_notif_icon.multiplierWidth;
    image.new_notif_icon.height = new_notif_icon.height * this.state.image.new_notif_icon.multiplierHeight;
    
    this.setState({image});
  }

  render(){
      return (
        <LayoutPrimary
            showTitle={true}
            showBackButton={true}
            showOutletSelector={false}
            showSearchButton={false}
            showTabBar={false}
            navigator={this.props.navigator}
            title={'Pemberitahuan'}
            contentContainerStyle={{
            }}
          >
            <View
              style={{
                flex:1,
                alignItems: 'center'
              }}
            >

              <View
                style={{
                  flex:1,
                  paddingTop: Scaling.moderateScale(20),
                  width:Dimensions.get('window').width
                }}
              >
                <TabBar.TypeD
                  item={[
                    {
                      itemText:CONSTANT.NOTIFICATION_TAB_TYPE.FIRST,
                    },
                    {
                      itemText:CONSTANT.NOTIFICATION_TAB_TYPE.SECOND,
                    },
                  ]}
                  onPress={this._onChangeTab.bind(this)}
                  icon={this.renderImportantIcon()}
                />

                {this.renderX()}
              </View>
            </View>
        </LayoutPrimary>
      );
  }

  renderImportantIcon(){
    if(this.props.totalImportantMessageNotRead > 0){
      return (
        <Image
          style={{width: this.state.image.new_notif_icon.width, height: this.state.image.new_notif_icon.height}}
          source={this.state.image.new_notif_icon.url}
        />
      )
    }else{
      return null
    }
  }

  renderX(){
    let contentList = this.state.showContent == CONSTANT.NOTIFICATION_TAB_TYPE.FIRST ? this.props.notificationList : this.props.messageList;
    
    if(contentList.length == 0){
      return(
        <NoData
          showContent={this.state.showContent}
          image={this.state.image}
        />
      )
    }else{
      return(
        <FlatList 
          showsVerticalScrollIndicator={false}
          extraData={this.state}
          data={contentList}
          keyExtractor={(item, index) => index+''}
          maxToRenderPerBatch={10}
          renderItem={(item) => { return (
            <Notification
              notification_id={item.item.id}
              notification_type_id={item.item.notificationtype.id}
              title={item.item.title}
              message={item.item.header}
              notificationDate={item.item.created_at}
            />
          );}}
        />
      )
    }
  }

  _onChangeTab(key, data){

    const CustomLayoutLinear = {
        duration: 200,
        create: {
            type: LayoutAnimation.Types.linear,
            property: LayoutAnimation.Properties.opacity
        },
        update: {
            type: LayoutAnimation.Types.linear
        },
    };

    LayoutAnimation.configureNext(CustomLayoutLinear);

    if(key == 0){ //firstTab
      this.setState({ 
        showContent: CONSTANT.NOTIFICATION_TAB_TYPE.FIRST,
        contentList: this.props.notificationList
      });
    }else{ // secondTab
      this.setState({ 
        showContent: CONSTANT.NOTIFICATION_TAB_TYPE.SECOND,
        contentList: this.props.messageList
      });
    }
  }

}

class NoData extends Component{
  render(){
    let message = '';
    let img_url = ''

    if(this.props.showContent == CONSTANT.NOTIFICATION_TAB_TYPE.FIRST){ // notifikasi
      message = 'Ups, Belum ada notifikasi yang diterima'
      img_url = this.props.image.empty_notification_icon.url
    }else{
      message = 'Ups, Belum ada pesan yang diterima'
      img_url = this.props.image.empty_message_icon.url
    }

    return(
      <View style={styles.NoData_container}>

        <View style={styles.NoData_content}>
          <View style={styles.NoData_info1}>
              <Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_HEADER} textStyle={{textAlign: 'center', fontFamily: CONSTANT.TEXT_FAMILY.MAIN}}>{message}</Text>
          </View>

          <Image
              style={{width: this.props.image.empty_notification_icon.width, height: this.props.image.empty_notification_icon.height}}
              source={img_url}
          />
        </View>
      </View>
    )
  }
}


function mapStateToProps(state, ownProps) {
  return {
    layoutHeight:state.rootReducer.layout.height,
    userRole:state.rootReducer.user.role,
    notificationList: state.rootReducer.root.notificationList,
    messageList: state.rootReducer.root.messageList,
    totalImportantMessageNotRead:state.rootReducer.root.totalImportantMessageNotRead,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationList);

const styles = StyleSheet.create({
  NoData_container: {
    flex: 1
  },
  NoData_content: {
    marginTop: Scaling.moderateScale(40),
    alignItems: 'center'
  },
  NoData_info1:{
    marginBottom: Scaling.moderateScale(10)
  }
})

