import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Text,
        Image,
        ActivityIndicator,
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from './../../../layout';
import {TextWrapper, Button, Form, TabBar, OrderStatus} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class SearchList extends Component{
  constructor(props) {
      super(props);
      this.state = {
        searchResult:[],
      };
  }
  
  _onChangeText(value){
    ///this.props.actionOrder.search(value, 'hash');
  }

  _renderTab(){
    if (this.props.userRole == 'courier') {
      return(
        <TabCourier/>
      );
    }
    else if (this.props.userRole == 'admin-outlet') {
      return(
        <TabAdminOutlet/>
      );
    }
  }

  _renderContent(){
    let {searchOrderLoadingStatus} = this.props;

    if (searchOrderLoadingStatus) {
      return (
        <LoadingContent/>
      );
    }
    else{
      return(
        <FlatList
          data={this.props.searchOrderResult}
          keyExtractor={(item, index) => {
              return index.toString();
            }
          }
          renderItem={(data) => { 
            return (
              <SearchContent 
                  item={data.item}
              />
            );
          }}
          ListEmptyComponent={() => {return (<EmptyContent />)}}
        /> 
      );
    }
  }

  render(){
      return (
        <LayoutPrimary
            showTitle={true}
            showBackButton={true}
            navigator={this.props.navigator}
            titleText={'Cari Data Pesanan'}
          >
            <View
              style={{
                flex:1,
              }}
            >
              
            
              <View style={{
                height:moderateScale(42.5),
                flexDirection:'row'}}
              >
                    <View style={{flex:0.375,}}>
                      <CategoryList
                        {...this.props}
                      />
                    </View>
                    <View style={{flex:0.625,marginLeft:moderateScale(5)}}>
                      <SearchInput
                        onChangeText={this._onChangeText.bind(this)}
                        autoFocus
                      />
                    </View>

              </View>

              <View style={{
                marginVertical:moderateScale(10),
              }}>
                  {this._renderTab()}
              </View>

              <View
                style={{
                  flex:1
                }}
              >
                {this._renderContent()}
              </View>
            </View>
        </LayoutPrimary>
      );
  }
}

class LoadingContent extends Component{
  render(){
    return(
        <View
          style={{
            flex:1,
            alignItems:'center',
            justifyContent:'center',
          }}
        >
          <ActivityIndicator size="large"/>
        </View>
        
    );
  }
}

class TabCourier extends Component{
  render(){
    return(
      <TabBar.TypeB
        item={[
          {
            itemText:'Pengambilan',
          },
          {
            itemText:'Pengantaran',
          },
        ]}
      />
    );
  }
}

class TabAdminOutlet extends Component{
  render(){
    return(
      <TabBar.TypeB
        item={[
          {
            itemText:'Ambil',
          },
          {
            itemText:'Dicuci',
          },
          {
            itemText:'Antar',
          },
          {
            itemText:'Komplain',
          },
        ]}
        tabItemTextType={'smallercontent'}
      />
    );
  }
}

class EmptyContent extends Component{
  constructor(props) {
      super(props);
      this.state = {
        image:{
          hanger_icon:{
            width:0,
            height:0,
            multiplierWidth:0.75,
            multiplierHeight:0.75,
            url:require('./../../../image/icon/hanger_icon.png'),
          },
        },
      }; 
  }

  componentDidMount(){
    this._getImageSize();
  }

  _getImageSize(){
    var image = this.state.image;

    var image_hanger_icon = ResolveAssetSource(this.state.image.hanger_icon.url);

    image.hanger_icon.width = moderateScale(image_hanger_icon.width);
    image.hanger_icon.height = moderateScale(image_hanger_icon.height);
    this.setState({image});
  }

  render(){
    return(
      <View
        style={{flex:1, marginTop:moderateScale(50), alignItems:'center'}}
      >
        <ResponsiveImage
          source={this.state.image.hanger_icon.url}
          initWidth={this.state.image.hanger_icon.width * this.state.image.hanger_icon.multiplierWidth}
          initHeight={this.state.image.hanger_icon.height * this.state.image.hanger_icon.multiplierHeight}
          style={{
            tintColor:CONSTANT.COLOR.GRAY_C,
          }}
        />

        <TextWrapper type={'smallcontent'} style={{color:CONSTANT.COLOR.GRAY_C}}>{'Pesanan Tidak Ditemukan'}</TextWrapper>
      </View>
    );
  }
}

class SearchContent extends Component{

  componentDidMount(){
  }
  render(){
    let {item} = this.props;

    let {order_hash} = item;
    return(
      <TouchableOpacity
        style={{
          flex:1,
          height:Dimensions.get('window').height * 0.2,
          borderRadius:5,
          backgroundColor:'white',
          elevation:3,
          marginBottom:moderateScale(10),
          marginHorizontal:moderateScale(2),
        }}

        onPress={() => {console.log('hai')}}
      >
        <View
          style={{flex:1,
            margin:moderateScale(10)}}
        >
            <View style={{flex:0.25, flexDirection:'row', alignItems:'center'}}>
              <View
                style={{
                  flex:0.6,
                  justifyContent:'center',
                }}
              >
                <TextWrapper>{order_hash}</TextWrapper>
              </View>

              <View
                style={{
                  flex:0.4,
                  justifyContent:'flex-end',
                  alignItems:'flex-end',
                }}
              >
                
              </View>
    
            </View>
            <View style={{flex:0.75}}>
              <View style={{flex:0.3, flexDirection:'row'}}>
                <View style={{flex:0.25}}>
                  <TextWrapper type={'smallercontent'}>Nama</TextWrapper>
                </View>
                <View style={{flex:0.05}}>
                  <TextWrapper type={'smallercontent'}>:</TextWrapper>
                </View>
                <View style={{flex:0.7}}>
                  <TextWrapper type={'smallercontent'}>Konsumen</TextWrapper>
                </View>
              </View>

              <View style={{flex:0.3, flexDirection:'row'}}>
                <View style={{flex:0.25}}>
                  <TextWrapper type={'smallercontent'}>Alamat</TextWrapper>
                </View>
                <View style={{flex:0.05}}>
                  <TextWrapper type={'smallercontent'}>:</TextWrapper>
                </View>
                <View style={{flex:0.7}}>
                  <TextWrapper type={'smallercontent'}>Tempat Konsumen</TextWrapper>
                </View>
              </View>
            </View>
        </View>
        
      </TouchableOpacity>
    );
  }
}

class CategoryList extends Component{
  render(){
    return(
      <Form.DropdownField.TypeA
          label={'Kategori'}
          data={[
            {
              value:'ID Pesanan',
              identifier:CONSTANT.SEARCH_ORDER_CATEGORY.ID,
            },
            {
              value:'Nama',
              identifier:CONSTANT.SEARCH_ORDER_CATEGORY.NAME,
            },
            {
              value:'Alamat',
              identifier:CONSTANT.SEARCH_ORDER_CATEGORY.ADDRESS,
            },
          ]}
          value={'ID Pesanan'}
          containerStyle={{
            backgroundColor:'#499ABF',
            opacity:0.75,
          }}
          inputContainerStyle={{
          }}
          onChangeText={(value, index, data) => {
              let {actionOrder} = this.props;
              actionOrder.setSearchOrderCategory(data[0].identifier);
          }}
        />
    );
  }
}

class SearchInput extends Component{
  constructor(props) {
        super(props);
        this.state = {
            image:{
                search_icon:{
                    width:0,
                    height:0,
                    multiplierWidth:0.1,
                    multiplierHeight:0.1,
                    url:require('./../../../image/icon/search_icon.png'),
                },
            },
        };  
    }

    componentWillMount(){
        this.getImageSize();
    }

    getImageSize(){
        var image = this.state.image;

        var image_search_icon = ResolveAssetSource(this.state.image.search_icon.url);

        image.search_icon.width = image_search_icon.width;
        image.search_icon.height = image_search_icon.height;
        
        this.setState({image});
    }

  onBlur() {
  }

  render(){
    return(
        <Form.InputField.TypeC
          {...this.props}
          label={'Kata Kunci Pencarian'}
          containerStyle={{
            backgroundColor:'#499ABF',
            opacity:0.75,
          }}
          inputContainerStyle={{
            //paddingTop:Dimensions.get('window').height * 0.015,
          }}
          numberOfLines={1}
          value={this.state.value}
          renderAccessory={() =>{
              return(
                  <Image
                    style={{
                      width:moderateScale(20),
                      height:moderateScale(20),
                      tintColor:'white',
                    }}

                  source={this.state.image.search_icon.url}
                  />
              );
            }
          }
        />
    );
  }
}


function mapStateToProps(state, ownProps) {
  return {
    layoutHeight:state.rootReducer.layout.height,
    userRole:state.rootReducer.user.role,
    searchOrderResult:state.rootReducer.order.searchOrderResult,
    searchOrderLoadingStatus:state.rootReducer.order.searchOrderLoadingStatus,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionOrder: bindActionCreators(actions.order, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchList);


