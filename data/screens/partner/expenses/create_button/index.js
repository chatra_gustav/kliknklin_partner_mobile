import React, { Component, PureComponent } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        StyleSheet,
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling, Form} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class CreateButton extends Component{
  static defaultProps = {
    availableStyle: [
      'Calendar',
      'List',
    ],
  }

  constructor(props) {
    super(props);
  
    this.state = {
      selectedStyle:this.props.availableStyle[this.props.filterButtonType],
    };
  }

  componentWillMount(){
  }

  onPress(){
    this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.CreateExpenditure', {
      
    });
  }

  render(){
    return(
      <Button.Standard
        buttonStyle={styles.buttonStyle}
        buttonColor={CONSTANT.COLOR.BLUE}
        buttonSize={CONSTANT.STYLE.BUTTON.EXTRA_MEDIUM}
        onPress={this.onPress.bind(this)}
      >
          <View
            style={styles.leftContent}
          >
              <Icon 
                name="ios-add-circle-outline" 
                size={Scaling.moderateScale(20)} 
                color={CONSTANT.COLOR.WHITE} 
              />
          </View>

          <View
            style={styles.rightContent}
          >
              <Text
                fontOption={CONSTANT.TEXT_OPTION.BOLD_CONTENT}
                textStyle={{
                  color:CONSTANT.COLOR.WHITE,
                }}
              >
                Buat Pengeluaran
              </Text> 
          </View>
      </Button.Standard>
    );
  }
}

const styles = StyleSheet.create({
  buttonStyle: {
    position:'absolute',
    bottom:'2.5%',
    right:'5%',
    flexDirection:'row',
  },
  leftContent:{
    flex:0.25,
    alignItems:'center',
  },
  rightContent:{
    flex:0.75
  }
});


function mapStateToProps(state, ownProps) {
  return {
    filterButtonType:state.rootReducer.order.filterButtonType,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionOrder: bindActionCreators(actions.order, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateButton);

