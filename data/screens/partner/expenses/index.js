import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  Alert,
  Linking,
  PermissionsAndroid,
  TouchableOpacity,
  ScrollView,
  AppState,
  BackHandler,
  FlatList
} from 'react-native';
import ResolveAssetSource from 'resolveAssetSource';
import MapView from 'react-native-maps';
import Polyline from '@mapbox/polyline';
import FusedLocation from 'react-native-fused-location';
import ResponsiveImage from 'react-native-responsive-image';
import Communications from 'react-native-communications';
import Collapsible from 'react-native-collapsible';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Navigation} from 'react-native-navigation';
import { LargeList } from "react-native-largelist-v3";

import {LayoutPrimary} from './../../../layout';
import {TextWrapper, OrderDetail, Container, CustomView, Scaling, NoDataFound} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

//local
import MonthPicker from './month_picker';
import Items from './items';
import CreateButton from './create_button';

class Expenses extends Component<{}> {
  constructor(props) {
    super(props);
  
    this.state = {
      data:this.props.orderData,
      expenseList:this.props.expenseList,
    };
  }

  componentDidMount(){
    this.props.actionExpenses.getExpenseList();
  }

  extractData(list){
    let extractedItem = [];
    const sContent = { items: [] };
    for (let section = 0; section < list.length; ++section){
        sContent.items.push(list[section]);
    }
    extractedItem.push(sContent);
    return extractedItem;
  }

  render() {
    try{
      let content = <NoDataFound 
            title={'Tidak ada data yang ditemukan'}
            message={'Jangan lupa untuk membuat data pengeluaran outlet anda'}
      />;

      if (this.props.expenseList.length > 0) {
        content = <LargeList
            contentStyle={{
              marginTop:Scaling.verticalScale(10),
            }}

            heightForIndexPath={({section, row}) => {
                return Scaling.verticalScale(135);
            }}

            renderFooter={()=><View style={{height:Scaling.verticalScale(200)}}/>}

            data={this.extractData(this.props.expenseList)}

            renderIndexPath={({section, row}) => {
              return <Items
                {...this.props}
                section={section}
                row={row}
              />;
            }}
        />;
      }
      return (
            <LayoutPrimary
              showTitle={true}
              showOrderOptionButton={true}
              showBackButton={true}
              title={'Pengeluaran'}
              navigator={this.props.navigator}
              showTabBar={false}
              contentContainerStyle={{
              }}
            >
              <MonthPicker 
                ref={component => this.monthPicker = component != null ? component.getWrappedInstance() : null}
              />
              
              {content}

              <CreateButton
                {...this.props}
              />
            </LayoutPrimary>
      );
    }
    catch(error){
      console.log(error, 'expenses.index.render()');
      return null;
    }
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    alignItems:'center',
  }
});


function mapStateToProps(state, ownProps) {
	return {
    orderData: state.rootReducer.order.active.data,
    expenseList: state.rootReducer.expenses.expenseList,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    actionExpenses: bindActionCreators(actions.expenses, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Expenses);

