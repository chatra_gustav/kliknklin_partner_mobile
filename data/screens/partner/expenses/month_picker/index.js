import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';

import {TabBar, Scaling, DateFormat, Text} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class MonthPicker extends Component{
  constructor(props) {
    super(props);
  
    this.state = {
      onSubmit:()=>{},
      selectedMonthYear: this.props.expenseSearchDate,
    };
  }

  render(){
    let {selectedMonthYear} = this.state;
    return(
      <View
        style={{
          width:'100%',
          alignItems:'center',
          marginTop:Scaling.verticalScale(10),
        }}
      >
        <TouchableOpacity
          style={{
            width:'50%',
            height:Scaling.verticalScale(40),
            elevation:2,
            backgroundColor:'#fff',
            borderColor:'lightgray',
            borderRadius:Scaling.moderateScale(5),
            flexDirection:'row',
          }}

          onPress={() => {
            this.props.actionNavigator.showLightBox(CONSTANT.ROUTE_TYPE.MONTH_PICKER,{
                
                selectedMonth: DateFormat(this.state.selectedMonthYear, 'shortDate2'),
                
                onPressButtonPrimary: async (selectedDate) => {
                  this.props.actionNavigator.dismissLightBox();
                  
                  this.props.actionExpenses.getExpenseList(new Date(selectedDate));

                  this.setState({ selectedMonthYear : selectedDate, selectedMonth: DateFormat(selectedDate, 'shortDate2')});

                },
                onPressButtonSecondary: () =>{
                  this.props.actionNavigator.dismissLightBox();
                },
            })
          }}
        >

          <View
            style={{
              flex:1,
              alignItems:'center',
            }}
          >
            <View
              style={{

              }}
            >
              <Text
                fontOption={CONSTANT.TEXT_OPTION.SUB_CONTENT}
              >{'Periode'}</Text>
            </View>
              <Text
                fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}
              >{DateFormat(selectedMonthYear, 'monthYear')}</Text>
          </View>


        </TouchableOpacity>
      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    expenseSearchDate: state.rootReducer.expenses.searchDate,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionExpenses: bindActionCreators(actions.expenses, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(MonthPicker);

