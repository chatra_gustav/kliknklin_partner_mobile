import React, { Component, PureComponent } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        StyleSheet,
        Image,
        TextInput,
        } from 'react-native';

import ImagePicker from 'react-native-image-picker';
import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';

import {LayoutPrimary} from 'app/data/layout';
import {Box, Scaling, Form, Button, Text, DateFormat, fontCreator} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class CreateExpenditure extends Component{
  static defaultProps = {
  }

  constructor(props) {
    super(props);
  
    this.state = {
      data:{
        expenditure_category_index:null,
        receipt_date:null,
        info:null,
        receipt_image_location:null,
        amount:null,
      }
    };
  }

  componentWillMount(){
  }

  _getSelectorData(){
    let result = [];
    try{
      let {expenditureCategoryList} = this.props;

      for(let index in expenditureCategoryList){
        let obj = {}
        obj.id = expenditureCategoryList[index].id;
        obj.value = expenditureCategoryList[index].info;

        result.push(obj);
      }
    }
    catch(error){
      console.log(error, 'create_expenditure._getSelectorData')
    }

    return result;
  }

  async onPressButtonCreateExpense(){
    let {actionValidation, actionNavigator, actionExpenses, navigator} = this.props;
    let {data} = this.state;

    let hasError = false;
    let errorMessage = '';

    if (await actionValidation.isEmpty(data.expenditure_category_index)){
      hasError = true;
      errorMessage = 'Kategori Pengeluaran tidak boleh kosong.';
    }
    else if (await actionValidation.isEmpty(data.receipt_date)){
      hasError = true;
      errorMessage = 'Tanggal Nota tidak boleh kosong.';
    }
    else if (await actionValidation.isEmpty(data.receipt_image_location)) {
      hasError = true;
      errorMessage = 'Gambar Nota tidak boleh kosong.';
    }
    else if (await actionValidation.isEmpty(data.amount)){
      hasError = true;
      errorMessage = 'Jumlah tidak boleh kosong.';
    }
    else if (!await actionValidation.isNumberPositive(data.amount)){
      hasError = true;
      errorMessage = 'Jumlah hanya berupa angka dan lebih besar daripada 0.';
    }

    if (hasError) {
      actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
          title:'Perhatian', 
          message: errorMessage,
          buttonPrimaryText: 'Ya', 
          onPressButtonPrimary: () => {
            actionNavigator.dismissLightBox();
          },
      });
    }
    else{
      actionExpenses.createExpense(data, navigator);
    }
  }

  render(){
    let {expenditureCategoryList} = this.props;
    let {data} = this.state;
    return(
      <LayoutPrimary
        showTitle={true}
        showOrderOptionButton={true}
        showBackButton={true}
        title={'Buat Pengeluaran'}
        navigator={this.props.navigator}
        showTabBar={false}
        contentContainerStyle={{
          backgroundColor:CONSTANT.COLOR.WHITE,
        }}
      >
          <View
              style={{
                flex:0.75,
                margin:Scaling.moderateScale(10),
              }}
            >
              <Input
                buttonStyle={{
                  marginTop:Scaling.verticalScale(5),
                }}

                onPress={() => {
                  this.props.actionNavigator.showLightBox('PartnerKliknKlin.ItemSelection', {
                    overrideBackPress:false,
                    data:expenditureCategoryList,
                    categoryToFind:'info',
                    hasAutoComplete:true,
                    title:'Pilih Kategori Pengeluaran',
                    hasTitle:true,
                    hasCloseButton:true,
                    placeholder:'Cari Kategori Disini',
                    onPressButtonPrimary: (index) => {
                      let data = this.state.data;
                      data.expenditure_category_index = index;
                      this.setState({data});
                    },
                  });
                }}
                label = {'Kategori Pengeluaran'}

                value={
                  data.expenditure_category_index != null ? 

                  expenditureCategoryList[data.expenditure_category_index].info

                  :

                  null
                }
              />

              <Input
                buttonStyle={{
                  marginTop:Scaling.verticalScale(25),
                }}

                onPress={() => {
                  this.props.actionNavigator.showLightBox(CONSTANT.ROUTE_TYPE.DATE_PICKER, {
                    overrideBackPress:false,
                    onPressButtonPrimary: (date) => {
                      let data = this.state.data;
                      data.receipt_date = date;
                      this.setState({data});
                    },
                  });
                }}

                label = {'Tanggal Nota'}

                value={
                  data.receipt_date != null ? 

                  DateFormat(new Date(data.receipt_date), 'fullDate')

                  :

                  null
                }
            />

            <Input
                buttonStyle={{
                  marginTop:Scaling.verticalScale(25),
                }}

                onPress={() => {
                  ImagePicker.launchImageLibrary({}, async (response)  => {
                    if (response.didCancel) {
                        console.log('User cancelled image picker');
                    }
                    else if (response.error) {
                        console.log('ImagePicker Error: ', response.error);
                    }   
                    else {
                      let data = this.state.data;
                      data.receipt_image_location = response.uri
                      this.setState({ data });
                    }
                  });
                }}

                label = {'Gambar Nota Pengeluaran'}

                value={
                  data.receipt_image_location != null ? 

                  data.receipt_image_location

                  :

                  null
                }
              />

            <Input
                buttonStyle={{
                  marginTop:Scaling.verticalScale(25),
                }}
                editable
                keyboardType={'number-pad'}
                onChangeText={(text)=>{
                    let data = this.state.data;
                    data.amount = text;

                    this.setState({data});
                }}
                label = {'Jumlah'}
                value={
                  data.amount != null ? 
                  data.amount
                  :
                  null
                }
                hasRightIcon={false}
                hasTextInput
            />
            
            <Input
                buttonStyle={{
                  marginTop:Scaling.verticalScale(25),
                  height:Scaling.verticalScale(55),
                }}
                numberOfLines={3}
                editable
                multiline
                onChangeText={(text)=>{
                    let data = this.state.data;
                    data.info = text;

                    this.setState({data});
                }}
                label = {'Info'}
                value={
                  data.info != null ? 
                  data.info
                  :
                  null
                }
                hasRightIcon={false}
                hasTextInput
                textInputStyle={{
                  textAlignVertical:'top',
                }}
              />

            </View>
            <View
              style={{
                flex:0.25,
                alignItems:'center',
                justifyContent:'center',
              }}
            >
                <Button.Standard
                  buttonSize={CONSTANT.STYLE.BUTTON.LARGE}
                  buttonText={'Buat'}
                  buttonColor={CONSTANT.COLOR.BLUE}
                  onPress={this.onPressButtonCreateExpense.bind(this)}
                />
          </View>
      </LayoutPrimary>
    );
  }
}

class Input extends Component{
  static defaultProps = {
    onPress:()=>{},
    hasRightIcon:true,
    hasTextInput:false,
  }
  
  onPress(){
    this.props.onPress();

    if (this.props.hasTextInput) {
      this.textInput.focus();
    }
  }

  render(){
    let {hasRightIcon} = this.props;

    const styles = Scaling.ScaledSheet.create({
          fontStyle:fontCreator(CONSTANT.TEXT_OPTION.SUB_HEADER),
    });

    return(
      <Button.Standard 
        onPress={this.onPress.bind(this)}
        buttonStyle={[{
          width:'100%',
          height:Scaling.verticalScale(50),
          borderRadius:Scaling.moderateScale(6),
          borderWidth:0,
          borderBottomWidth:Scaling.moderateScale(2),
          borderColor:CONSTANT.COLOR.LIGHT_GRAY,
          alignItems:'flex-start',
          justifyContent:'flex-start',
          paddingHorizontal:Scaling.moderateScale(5),

        },
        this.props.buttonStyle,
        ]}
        buttonColor={'transparent'}
      >
        
        <Text
          fontOption={CONSTANT.TEXT_OPTION.CONTENT}
          textStyle={{
            flex:0.3,
          }}
        >
          {
            this.props.value != null ? 
            this.props.label
            :
            null
          }
        </Text>

        <TextInput
          ref={(component) => this.textInput = component}
          numberOfLines={1}
          editable={false}
          value={this.props.value}
          placeholder={this.props.label}
          style={[styles.fontStyle, {
            flex:0.7,
            width:'95%',
            padding:0,
            color:CONSTANT.COLOR.BLACK,
          },
          this.props.textInputStyle,
          ]}
          {...this.props}
        />

        {
          hasRightIcon ? 
            <Image
              style={{
                width: Scaling.scale(22.5), 
                height: Scaling.verticalScale(22.5),
                right:Scaling.scale(5),
                top:'40%',
                position:'absolute'
              }}
              source={CONSTANT.IMAGE_URL.BOTTOM_ARROW_ICON}
            />
          :
            null
        }
        
      </Button.Standard>
    );
  }
}

const styles = StyleSheet.create({

});


function mapStateToProps(state, ownProps) {
  return {
    expenditureCategoryList:state.rootReducer.masterData.expenditureCategoryList,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionOrder: bindActionCreators(actions.order, dispatch),
    actionExpenses: bindActionCreators(actions.expenses, dispatch),
    actionValidation: bindActionCreators(actions.validation, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateExpenditure);

