import React, { Component, PureComponent } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        StyleSheet,
        Image
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from 'app/data/layout';
import {TextWrapper, Text, Button, Form, TabBar, OrderStatus, DateFormat, Scaling, toCurrency} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";


class Items extends PureComponent{
  componentDidMount(){
  }
  render(){
    try{
      let data = this.props.expenseList[this.props.row];
      let createdAt = new Date(data.created_at.split(' ')[0]);
      let day =  DateFormat(new Date(data.receipt_date), 'dayNameShort');
      let date = new Date(data.receipt_date).getDate();
      let expenditureInfo = data.expenditure_category.info;
      let amount = data.amount;
      let createdBy = data.user.name;
      let info = data.info;
      let receiptImageUrl = data.receipt_image_url;
      let expenditureID = data.id;

      return(
        <View
          style={{
            height:Scaling.verticalScale(120),
            backgroundColor:CONSTANT.COLOR.WHITE,
            marginBottom:Scaling.verticalScale(15),
            flexDirection:'row',
            elevation:2,
          }}
        >
          <View
            style={{
              marginVertical:Scaling.verticalScale(15),
              borderRightWidth:Scaling.moderateScale(2),
              borderColor:CONSTANT.COLOR.LIGHT_GRAY,
              flex:0.2,
              alignItems:'center',
              justifyContent:'center',
            }}
          >
            <Text
              fontOption={CONSTANT.TEXT_OPTION.SUMMARY_NUMBER_BOX_DASHBOARD}
            >{date}</Text>
            <Text
              fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR}
            >{day}
            </Text>
          </View>

          <View
            style={{
              flex:0.8,
              marginVertical:Scaling.verticalScale(15),
              marginHorizontal:Scaling.scale(10),
            }}
          >
            <Text
              fontOption={CONSTANT.TEXT_OPTION.HEADER}
            >{expenditureInfo + ' - ' + toCurrency(amount, 'Rp ')}
            </Text>

            <Text
              fontOption={CONSTANT.TEXT_OPTION.CONTENT}
              textStyle={{
                marginTop:Scaling.verticalScale(1),
              }}
            >{'Dibuat oleh : ' + createdBy}
            </Text>

            <Text
              fontOption={CONSTANT.TEXT_OPTION.CONTENT}
              textStyle={{
                marginTop:Scaling.verticalScale(1),
              }}
            >{'Dibuat pada : ' + DateFormat(createdAt, 'fullDate')}
            </Text>
                      
            <TouchableOpacity style={{
              flexDirection:'row',
              marginTop:Scaling.verticalScale(1),
            }}
              onPress={() => {
                  this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.WEBVIEW, {
                      title: expenditureInfo + ' - ' + toCurrency(amount, 'Rp '),
                      url: receiptImageUrl,
                  });
              }}
            >
                <Text fontOption={CONSTANT.TEXT_OPTION.CONTENT} textStyle={{
                  color:CONSTANT.COLOR.LIGHT_BLUE
                }}>
                  {'Lihat Gambar Nota'}
                </Text>

                <Image
                  style={{
                    tintColor:CONSTANT.COLOR.LIGHT_BLUE,
                    width: Scaling.scale(12), 
                    height: Scaling.verticalScale(12),
                    marginLeft:Scaling.moderateScale(5.5),
                    marginTop:Scaling.verticalScale(3),
                  }}
                  source={CONSTANT.IMAGE_URL.BOTTOM_ARROW_ICON}
                />
            </TouchableOpacity>

            <Text
              fontOption={CONSTANT.TEXT_OPTION.CONTENT}
              textStyle={{
                marginTop:Scaling.verticalScale(1),
              }}
            >{info}
            </Text>

            <Form.DropdownField.TypeB
                ref={(component) => {this.dropdown_field = component}}
                label={''}
                data={[
                  {value:'Hapus'},
                ]}
                value={''}
                containerStyle={{
                    position:'absolute',
                    right:0,
                    top:0,
                    width:Scaling.scale(35), 
                    height:Scaling.verticalScale(30),
                    alignItems:'flex-start',
                    justifyContent:'center',
                }}
                baseColor={'transparent'}
                dropdownOffset={{
                  top:Scaling.moderateScale(32),
                  left:Scaling.moderateScale(-100),
                }}
                pickerStyle={{
                  width:Scaling.moderateScale(125),
                }}

                onChangeText={this.onChangeText.bind(this, expenditureID, createdAt)}

                renderAccessory={() =>{
                    return(
                          <Image
                            style={{
                              width: Scaling.scale(25), 
                              height: Scaling.verticalScale(20),
                            }}
                            source={CONSTANT.IMAGE_URL.OPTION_ICON}
                          />
                      );
                    }
                }
            />
          </View>
        </View>
      );
    }
    catch(error){
        console.log(error, 'expenses.items');
        return null;
    }
  }

  onChangeText(expenditureID, createdAt, text){
    if (text.toLowerCase() == 'hapus') {
      createdAt = new Date(createdAt);
      let limitDate = new Date(createdAt.setMonth(createdAt.getMonth()+1));
      if (new Date().getTime() > limitDate.getTime()) {
        this.props.actionRoot.showAlertMessage(
          'Hanya bisa dihapus sebelum : \n' + DateFormat(limitDate,'fullDate') + '\n (30 hari setelah dibuat)',
          'Tidak bisa dihapus', 
        );
      }
      else{
        this.props.actionExpenses.deleteExpense(expenditureID);
      }
    }
  }
}

function mapStateToProps(state, ownProps) {
  return {
    expenseList: state.rootReducer.expenses.expenseList,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionExpenses: bindActionCreators(actions.expenses, dispatch),
    actionRoot: bindActionCreators(actions.root, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Items);
