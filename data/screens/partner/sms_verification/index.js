import React, { Component } from 'react';
import { 	
        Platform,
        Modal, 
        View,
        Dimensions,
        Alert,
        Image,
        TouchableHighlight,
        ActivityIndicator,
        ScrollView,
  		} from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import TimerMixin from 'react-timer-mixin';

import {LayoutPrimary} from './../../../layout';
import {TextWrapper, Text, Container, Form, Button} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class SMSVerification extends Component {
    constructor(props){
        super(props);
        this.state = {
            firstCode:'',
            secondCode:'',
            thirdCode:'',
            fourthCode:'',
            errorMessage:'',
            image:{
                sms_icon:{
                    width:0,
                    height:0,
                    multiplierWidth:0.4,
                    multiplierHeight:0.4,
                    url:require('./../../../image/sms_verification/sms_icon.png'),
                }
            },
            timer:this.baseTimeTimer,
            isTimerRunning:false,
        };
    }

    componentWillMount(){
        this._getImageSize();

        if (this.props.smsVerificationSent && this.props.smsVerificationTimer < CONSTANT.SMS_VERIFICATION.BASE_TIME_TIMER) {
            this._runTimer();   
        }
    }

    _sendMobileVerificationCode(){
        this.props.actionAuthentication.sendMobileVerificationCode()
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {

            })
    }

    _verifyMobileVerificationCode(code){
        this.props.actionAuthentication.verifyMobileVerificationCode(code)
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {

            })
    }

    componentWillUnmount() {
        TimerMixin.clearInterval(this.intervalCheckTimer);
    }

    _getImageSize(){
        var image = this.state.image;

        var image_sms_icon = ResolveAssetSource(this.state.image.sms_icon.url);

        image.sms_icon.width = image_sms_icon.width;
        image.sms_icon.height = image_sms_icon.height;
        
        this.setState({image});
    }

    _onPressSendSMSActivation(){
        let {actionRoot, smsVerificationTimer} = this.props;
        
        actionRoot.setSMSVerificationSent(true);

        this._runTimer();

        this._sendMobileVerificationCode();
    }

    _runTimer(){
        let {smsVerificationTimer, actionRoot} = this.props;
        let timer = smsVerificationTimer;

        this.intervalCheckTimer = TimerMixin.setInterval(() => {
            timer -= 1;

            if (timer <= 0) {
                actionRoot.setSMSVerificationTimer(CONSTANT.SMS_VERIFICATION.BASE_TIME_TIMER);

                TimerMixin.clearInterval(this.intervalCheckTimer);
            }
            else{
                actionRoot.setSMSVerificationTimer(timer);
            }
        }, 1000);
    }

    _onPressSubmitCode(){
        let {firstCode, secondCode, thirdCode, fourthCode} = this.state
        if (this._validateCode()) {
            //this.props.actionNavigator.resetTo(this.props.navigator, 'PartnerKliknKlin.ShipmentSchedule');
            console.log(this.state);
            this._verifyMobileVerificationCode(firstCode+secondCode+thirdCode+fourthCode);
        }
        else{
            this.setState({errorMessage:'Kode tidak boleh kosong'});  
        }
    }

    _validateCode(){
        let result = true;

        if (!this._isEmptyCode()) {
            result = false;
        }

        return result;
    }

    _isEmptyCode(){
        if (this.state.firstCode == '' || this.state.secondCode == '' ||
            this.state.thirdCode == '' || this.state.fourthCode == '') {
            return false;
        }
        return true;
    }

    renderSendSmsVerificationCode(){
        let {smsVerificationTimer} = this.props;
        if (smsVerificationTimer != CONSTANT.SMS_VERIFICATION.BASE_TIME_TIMER) {
            return (
                <View>
                    <TextWrapper type={'base'} style={{fontWeight:'bold', color:'#6C6C6C'}}>
                        Kirim ulang kode pada {this.props.smsVerificationTimer}
                    </TextWrapper>
                </View>
            );
        }
        else{
            return (
                <Form.LinkField.TypeA
                    label={'Kirim Ulang Kode'}
                    type={'base'}
                    fontWeight={'Bold'}
                    containerStyle={{alignItems:'center'}}
                    labelStyle={{color:CONSTANT.COLOR.BLUE_A}}
                    onPress={()=>{
                        this._onPressSendSMSActivation();
                    }}
                />
            );
        }
    }

    getContent(){
        let {userMobilePhone, smsVerificationSent} = this.props;

        if (smsVerificationSent) {
            return(
                <View style={{flex:1,}}>
                    <View style={{alignItems:'center'}}>
                        <TextWrapper type='content' style={{fontWeight:'bold', color:'#6C6C6C'}}>
                            Masukan Kode
                        </TextWrapper>

                        <TextWrapper type='base' style={{textAlign:'center', color:'#6C6C6C'}}>
                            {'Kami telah mengirimkan sebuah kode melalui sms ke nomor '} 
                            <TextWrapper fontWeight={'Medium'} type='base' style={{textAlign:'center', color:'#6C6C6C'}}>{userMobilePhone}</TextWrapper> 
                        </TextWrapper>
                    </View>

                    <View
                        style={{flexDirection:'row', width:'100%', justifyContent:'center'}}
                    >
                        <Form.InputField.TypeA
                            ref={component => this.firstCode = component}
                            keyboardType={'numeric'}
                            containerStyle={{width:Dimensions.get('window').width * 0.1, margin:5}}
                            inputStyle={{
                                width:Dimensions.get('window').width * 0.1,
                                fontSize:24,
                                fontWeight:'bold',
                                color:'#6C6C6C'
                            }}
                            maxLength={1}
                            onChangeText={(text) => {
                                this.setState({firstCode:text});
                                this.secondCode.focus();
                            }}
                        />

                        <Form.InputField.TypeA
                            ref={component => this.secondCode = component}
                            keyboardType={'numeric'}
                            containerStyle={{width:Dimensions.get('window').width * 0.1, margin:5}}
                            inputStyle={{
                                width:Dimensions.get('window').width * 0.1,
                                fontSize:24,
                                fontWeight:'bold',
                                color:'#6C6C6C'
                            }}
                            maxLength={1}
                            onChangeText={(text) => {
                                this.setState({secondCode:text});
                                this.thirdCode.focus();
                            }}
                        />

                        <Form.InputField.TypeA
                            ref={component => this.thirdCode = component}
                            keyboardType={'numeric'}
                            containerStyle={{width:Dimensions.get('window').width * 0.1, margin:5}}
                            inputStyle={{
                                width:Dimensions.get('window').width * 0.1,
                                fontSize:24,
                                fontWeight:'bold',
                                color:'#6C6C6C'
                            }}
                            maxLength={1}
                            onChangeText={(text) => {
                                this.setState({thirdCode:text});
                                this.fourthCode.focus();
                            }}
                        />

                        <Form.InputField.TypeA
                            ref={component => this.fourthCode = component}
                            keyboardType={'numeric'}
                            containerStyle={{width:Dimensions.get('window').width * 0.1, margin:5}}
                            inputStyle={{
                                width:Dimensions.get('window').width * 0.1,
                                fontSize:24,
                                fontWeight:'bold',
                                color:'#6C6C6C'
                            }}
                            maxLength={1}
                            onChangeText={(text) => {
                                this.setState({fourthCode:text});
                                this.fourthCode.blur();
                            }}
                        />
                    </View>
                    
                    <Form.ErrorField.TypeA
                        errorText={this.state.errorMessage}
                    />


                    <View style={{ marginHorizontal:10, marginBottom:10, alignItems:'center'}}>
                        <TextWrapper type='base' style={{color:'#6C6C6C', textAlign:'center'}}>
                            Tidak menerima kode ? 
                        </TextWrapper>

                        {this.renderSendSmsVerificationCode()}
                    </View>

                    <Button.Standard
                        buttonText={'Kirim'}
                        buttonSize={CONSTANT.STYLE.BUTTON.LARGE}
                        buttonColor={CONSTANT.COLOR.GREEN}
                        fontOption={CONSTANT.TEXT_OPTION.HEADER}
                        onPress={this._onPressSubmitCode.bind(this)}
                        buttonStyle={{
                            position:'absolute',
                            bottom:'15%',
                            left:'14%',
                        }}
                    />
                </View>
            );
        }
        return(
            <View style={{
                    flex:1,
                    alignItems:'center'
            }}>
                <Text
                    fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}
                    textStyle={{
                        textAlign:'center',
                    }}
                >
                    Tekan tombol kirim untuk melanjutkan   
                </Text>

                <Text
                    fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}
                    textStyle={{
                        textAlign:'center',
                    }}
                >
                    Kami akan mengirimkan sebuah sms berisi kode ke {userMobilePhone}
                </Text>

                <Button.Standard
                    buttonText={'Kirim SMS'}
                    buttonSize={CONSTANT.STYLE.BUTTON.LARGE}
                    buttonColor={CONSTANT.COLOR.BLUE}
                    fontOption={CONSTANT.TEXT_OPTION.HEADER}
                    onPress={this._onPressSendSMSActivation.bind(this)}
                    buttonStyle={{
                        top:'10%',
                    }}
                />
            </View>
        );
    }

    render(){
        let {userMobilePhone} = this.props;

        return(
            <LayoutPrimary
                {...this.props}
                showTitle={true}
                showTabBar={true}
                title={'Verifikasi Nomer HP'}
            >
                <Container.TypeA
                    contentStyle={{
                        minHeight:Dimensions.get('window').height * 0.75,
                        marginBottom:Dimensions.get('window').height * 0.1,
                        marginTop:this.props.layoutHeight * 0.25,
                        alignItems:'center',
                    }}
                >
                    <ResponsiveImage
                            source={this.state.image.sms_icon.url}
                            initWidth={this.state.image.sms_icon.width * this.state.image.sms_icon.multiplierWidth}
                            initHeight={this.state.image.sms_icon.height * this.state.image.sms_icon.multiplierHeight}
                            style={{margin:20}}
                    />

                    {this.getContent()}
                </Container.TypeA>
            </LayoutPrimary>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        errorMessage:state.rootReducer.message.error,
        layoutHeight:state.rootReducer.layout.height,
        userMobilePhone: state.rootReducer.user.data.mobilePhone,
        smsVerificationTimer: state.rootReducer.root.smsVerificationTimer,
        smsVerificationSent: state.rootReducer.root.smsVerificationSent,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actionAuthentication: bindActionCreators(actions.authentication, dispatch),
        actionRoot: bindActionCreators(actions.root, dispatch),
        actionNavigator: bindActionCreators(actions.navigator, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SMSVerification);


