import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity, Linking, ScrollView, Platform, BackHandler} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from 'app/data/layout';
import {Text, DateFormat, ShipmentScheduleListCourier, OutletSelector, Label, Scaling} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

//local
import SearchBar from './search_bar';
import OrderList from './order_list';

class SearchOrder extends Component {
	constructor(props) {
        super(props);
        this.state = {
        	tabBarItemList: ['Semua'],
        	selectedTab: 'Semua'
        };  
    }

	componentWillMount(){
		this.props.actionOrder.setSearchOrderResult([]);
	}
	
    componentDidUpdate(prevProps, prevState){
    	try{
			let {searchOrderResult} = this.props;

			if (JSON.stringify(prevProps.searchOrderResult) != JSON.stringify(searchOrderResult) ) {
				this.setState({ tabBarItemList: ['Semua'] });
			}
		}
		catch(error){

		}
    }

    setSelectedTab(selectedTab){
    	this.setState({ selectedTab });
    }

	render(){
		try{
			let {summaryOrder} = this.props;
			return(
				<LayoutPrimary
					showOutletSelector={false}
					showLogo={false}
					showSearchButton={false}
					showNotificationButton={false}
					showTabBar={false}
					showNavBar={false}
					navigator={this.props.navigator}
				>

					<SearchBar 
						navigator={this.props.navigator}
						searchingType={this.props.searchingType}
					/>

					<TabBar
						itemList={this.state.tabBarItemList}
						searchOrderResult={this.props.searchOrderResult}
						selectedTab={this.state.selectedTab}
						setSelectedTab={this.setSelectedTab.bind(this)}
					/>

					<OrderList
						navigator={this.props.navigator}
						selectedTab={this.state.selectedTab}
					/>
			      	
			  	</LayoutPrimary>
			);
		}
		catch(error){
			console.log(error, 'dashboard.index');
			return null;
		}
		
	}
}

class TabBar extends Component{

	setItem(){

    	let ItemList = this.props.itemList;

    	this.props.searchOrderResult.map((result) => {
    		
    		let order_status_laundry_id = result.orderlaundry.order_status_laundry_id;
    		
    		if(order_status_laundry_id == CONSTANT.ORDER_STATUS.WAIT_PICKUP){
    			ItemList = this.addItem(ItemList, 'Penjemputan');
    		}else if(order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_LAUNDRY){
    			ItemList = this.addItem(ItemList, 'Proses Cuci');
    		}else if(order_status_laundry_id == CONSTANT.ORDER_STATUS.DONE_LAUNDRY){
    			ItemList = this.addItem(ItemList, 'Pengantaran');
    		}else if(order_status_laundry_id == CONSTANT.ORDER_STATUS.APPLIED_COMPLAIN){
    			ItemList = this.addItem(ItemList, 'Komplain');
    		}else if(order_status_laundry_id == CONSTANT.ORDER_STATUS.CANCEL){
    			ItemList = this.addItem(ItemList, 'Batal');
    		}else if(order_status_laundry_id == CONSTANT.ORDER_STATUS.DONE_ORDER){
    			ItemList = this.addItem(ItemList, 'Pesanan Selesai');
    		}else if(order_status_laundry_id == CONSTANT.ORDER_STATUS.CANNOT_FIND_PARTNER){
    			ItemList = this.addItem(ItemList, 'Ditolak');
    		}
    	})
    	
    	return ItemList;

    }

    addItem(ItemList, actionTab){
    	if(!ItemList.includes(actionTab)){
    		ItemList.push(actionTab);

    		return ItemList;
    	}else{
    		return ItemList;
    	}
    }

    onPressItem(item){
    	this.props.setSelectedTab(item);
    }

	render(){

		if(this.props.searchOrderResult.length == 0){ // user belum melakukan pencarian
			return null
		}

		let x = this.setItem();
		
		return(
			<View style={styles.tabBarContainer}>
				<ScrollView 
					horizontal={true}
					showsHorizontalScrollIndicator={false}
				>
					{x.map((item, index) => {
						let backgroundColor = CONSTANT.COLOR.LIGHT_GRAY;

						if(item == this.props.selectedTab){
							backgroundColor = 'lightblue';
						}

						return(
							<TouchableOpacity 
								key={index} 
								style={[styles.tabBar, {backgroundColor: backgroundColor}]}
								onPress={() => this.onPressItem(item)}
							>
								<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_ROLE} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{item}</Text>
							</TouchableOpacity>
						)
					})}
				</ScrollView>
			</View>
		)
	}
}

function mapStateToProps(state, ownProps) {
	return {
		userRole:state.rootReducer.user.role,
		searchOrderResult:state.rootReducer.order.searchOrderResult,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
    	actionOrder: bindActionCreators(actions.order, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchOrder);

const styles = StyleSheet.create({
	tabBarContainer:{
		flexDirection: 'row', 
		alignItems: 'center',
		paddingLeft:Scaling.scale(CONSTANT.STYLE.TAB_BAR.MAIN_DASHBOARD.PADDING_LEFT),
	    paddingRight:Scaling.scale(CONSTANT.STYLE.TAB_BAR.MAIN_DASHBOARD.PADDING_RIGHT),
	    paddingVertical: Scaling.verticalScale(15)
	},
	tabBar: {
		paddingVertical: Scaling.verticalScale(5),
		paddingHorizontal: Scaling.moderateScale(10),
		borderRadius: 8,
		justifyContent: 'center',
		marginRight: Scaling.moderateScale(15)
	}
})
