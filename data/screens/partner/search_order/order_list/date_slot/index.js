import React, { Component, PureComponent } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        StyleSheet,
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';

import {TextWrapper, Text, Button, Form, TabBar, OrderStatus, DateFormat, Scaling} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class DateSlot extends PureComponent{
  static defaultProps = {
    content:() => {
      return(<View/>)
    },
  }

  render(){
    let {item, shipment, content, type} = this.props;
    return(
      <View 
        style={DateSlotStyle.container} 
      >
        <View style={DateSlotStyle.titleSection}>
            <Text fontOption={CONSTANT.TEXT_OPTION.HEADER} textStyle={DateSlotStyle.DateSlotText}>
              {DateFormat(new Date(type && type == 'createdAt' ? item.createdAt : item.date), 'fullDate')}
            </Text>
        </View>

        <View style={DateSlotStyle.contentSection}>
            <FlatList
              data={item.data}
              keyExtractor={(item, index) => index+''}
              renderItem={({item, index}) => { 
                return content(item, index);
              }}
            />
        </View>
        
        <View style={DateSlotStyle.floating}>
          <View style={DateSlotStyle.circle} />
          <View style={[DateSlotStyle.line, {
            height:(Scaling.verticalScale(200) * item.data.length) + (Scaling.verticalScale(10) * item.data.length - 1),
          }]} />
        </View>
      </View>
    );
  }
}

const DateSlotStyle = StyleSheet.create({
  container: {
    flex: 1,
    marginTop:Scaling.verticalScale(20),
  },
  titleSection:{
    height:Scaling.verticalScale(40),
    left:'2.5%',
    marginLeft:Scaling.scale(5),
  },
  contentSection:{
    justifyContent:'center',
  },
  line:{
    borderRightWidth:Scaling.moderateScale(2),
    borderColor:CONSTANT.COLOR.GRAY,
    top:Scaling.verticalScale(-5),
    zIndex:10,
  },
  circle:{
    width:Scaling.scale(10),
    height:Scaling.scale(10),
    borderRadius:Scaling.moderateScale(50),
    backgroundColor:CONSTANT.COLOR.GRAY,
    zIndex:10,
  },
  floating:{
    position:'absolute',
    left:'5.5%',
    alignItems:'center',
    height:'100%',
    top:Scaling.verticalScale(25),
  },
  DateSlotText:{
    top:moderateScale(-3),
  }
});

function mapStateToProps(state, ownProps) {
  return {
    inProgressOrderList:state.rootReducer.order.inProgressOrderList,
    selectedOrderDate:state.rootReducer.order.selectedOrderDate,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionOrder: bindActionCreators(actions.order, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DateSlot);


