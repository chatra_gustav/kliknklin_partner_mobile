import React, { Component, PureComponent } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        StyleSheet,
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from 'app/data/layout';
import {TextWrapper, Text, Button, Form, TabBar, OrderStatus, DateFormat, Scaling, NoActiveOutlet, toCurrency} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

//local
import TimeSlot from './../time_slot';
import DateSlot from './../date_slot';
import BaseItem from './../base_item';
import NoOrderContent from './../no_order_content';


class ResultContent extends PureComponent{
  static defaultProps = {
    data: [],
  }

  componentWillMount(){
    this.props.actionOrder.setOrderListTypeTab(1);
  }

  getContent(){
    let {orderListTypeTab, latePickupOrderList, dataList, dataLateList, userOutlet} = this.props;
    let {indexActiveOutlet} = userOutlet;
    

    if (indexActiveOutlet == null) {
      return(
        <NoActiveOutlet {...this.props}/>
      );
    }
    else{
      if (orderListTypeTab == 1) {
        return(
          <View
            style={{
              flex:1,
            }}
          >
            {dataList.length == 0 ? 
              <NoOrderContent />
                :
              <FlatList
                  showsVerticalScrollIndicator={false}
                  contentContainerStyle={{
                    paddingBottom:Scaling.verticalScale(200),
                  }}
                  initialNumToRender={4}
                  data={dataList}
                  keyExtractor={(item, index) => index+''}
                  renderItem={({item, index}) => {
                    return <DateSlot {...this.props} item={item} 
                        content={(items, key)=>{
                          return(
                              <Items
                                {...this.props}
                                item={items}
                                key={key}
                              />
                          )
                        }}
                        key={index}
                    />;
                  }}
              /> 
            }
            
          </View>
        );
      }
    }
  }


  render(){
    let {latePickupOrderList} = this.props;
    return(
      <View
        style={{flex:1}}
      >
          <View
            style={{
              flex:1,
            }}
          >
            {this.getContent()}
          </View>
      </View>
    );
  }
}

class Items extends PureComponent{
  componentDidMount(){
  }
  render(){
    try{
      let {item} = this.props;
      let {payment, id, customer, orderlaundry, order_hash, pricedetail, orderitem, online_flag} = item;

      let {user} = customer;
      let {name} = user;

      let {orderstatuslaundry} = orderlaundry;
      let orderStatusLaundryID = orderstatuslaundry.id;

      let shipment = item.selectedShipment;
      let {shipmentschedule, shipment_status_id, internalshipment} = shipment;
     
      let {location_address, shipment_time_info, shipment_date } = shipmentschedule;
  
      let userCourier = null;

      if (internalshipment) {
        let {courier} = internalshipment;
        let {partner} = courier;
        userCourier = partner.ktp_name;
      }
      
      let data = 
        [
            {
              category:'Pelanggan',
              content:name,
            },
            {
              category:'Kurir',
              content:userCourier ? userCourier : 'Ambil Sendiri',
            },
         ];
      
      if (online_flag) {
        data = [
            {
              category:'Pelanggan',
              content:name,
            },
            {
              category:'Tempat',
              content:location_address,
              props:{
                numberOfLines:3,
              }
            },
            {
              category:'Kurir',
              content:userCourier ? userCourier : 'Ambil Sendiri',
            },
        ];
      }

      return(
        <BaseItem
          navigator={this.props.navigator}
          item={item}
          customerName={name}
          logOrderPrice={pricedetail}
          orderItem={orderitem}
          orderHash={order_hash}
          orderStatusLaundryID={orderStatusLaundryID}
          shipmentStatus={shipment_status_id}
          onlineFlag={online_flag}
          data={data}
          submitButton={() => {
            if (orderstatuslaundry.id == 3) {
              if (online_flag) {
                return(
                  <Button.Standard
                      buttonSize={CONSTANT.STYLE.BUTTON.THICKER_EXTRA_MEDIUM}
                      buttonText={'SELESAI DICUCI'}
                      onPress={() => {
                        this.props.actionOrder.submitFinishLaundry(this.props.navigator, id);
                      }}
                  />
                );
              }
              return(
                  <Button.Standard
                      buttonSize={CONSTANT.STYLE.BUTTON.THICKER_EXTRA_MEDIUM}
                      buttonColor={CONSTANT.COLOR.ORANGE}
                      buttonText={'DIAMBIL PELANGGAN'}
                      onPress={() => {
                        if (!payment.pay_early_flag) {
                          this.props.actionPOS.showPaymentBox('pay-later', (value) => {
                            this.props.actionOrder.submitFinishLaundry(this.props.navigator, id, value - pricedetail.full_price );
                          }, 
                          'Jumlah yang harus dibayarkan : ' + toCurrency(pricedetail.full_price, 'Rp '),
                          pricedetail.full_price);
                        }else{
                          this.props.actionOrder.submitFinishLaundry(this.props.navigator, id);
                        }
                      }}
                  />
                );
              }
          }}
        />
      );
    }
    catch(error){
        console.log(error, 'orderlist.in_progress_content');
        return null;
    }
  }
}

function mapStateToProps(state, ownProps) {
  return {
    pickupOrderList:state.rootReducer.order.pickupOrderList,
    latePickupOrderList:state.rootReducer.order.latePickupOrderList,
    selectedOrderDate:state.rootReducer.order.selectedOrderDate,
    orderListTypeTab:state.rootReducer.order.orderListTypeTab,
    userOutlet:state.rootReducer.user.outlet,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionOrder: bindActionCreators(actions.order, dispatch),
    actionPOS: bindActionCreators(actions.pos, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ResultContent);

