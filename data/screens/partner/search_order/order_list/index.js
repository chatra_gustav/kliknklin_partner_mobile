import React, { Component, PureComponent } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Text,
        StyleSheet,
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from 'app/data/layout';
import {TextWrapper, Button, Form, TabBar, OrderStatus, DateFormat, toCurrency} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";


//local
import ResultContent from './result_content';

class OrderList extends Component{
	constructor(props){
		super(props);
	}

	render(){
		try{
          	return (
				this.getContentComponent()
          	);
      	}
      	catch(error){
        	console.log(error);
        	return null;
      	}
	}

	getContentComponent(item){
	    let {type, searchOrderResult} = this.props;

	    // if (this.props.type == 'pick-up') {
	    //   	return (
	    //     	<ResultContent
	    //       		dataList={this.extractData(searchOrderResult, 'time')}
	    //       		dataLateList={[]}
	    //       		{...this.props}
	    //     	/>
	    //   	);
	    // }else{
	    // 	console.log('nulll');
	    // 	return null
	    // }
	    return (
        	<ResultContent
          		dataList={this.extractData(searchOrderResult, 'time')}
          		dataLateList={[]}
          		{...this.props}
        	/>
      	);
	}

	extractData(data, filterType = 'time'){
        let displayedData = [];
        let tempResult = [];

        if(this.props.selectedTab == 'Semua'){
            displayedData = data;
        }else if(this.props.selectedTab == 'Penjemputan'){
            for(let i in data){
                if(data[i].orderlaundry.order_status_laundry_id == CONSTANT.ORDER_STATUS.WAIT_PICKUP){
                    displayedData.push(data[i]);
                }
            }
        }else if(this.props.selectedTab == 'Proses Cuci'){
            for(let i in data){
                if(data[i].orderlaundry.order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_LAUNDRY){
                    displayedData.push(data[i]);
                }
            }
        }else if(this.props.selectedTab == 'Pengantaran'){
            for(let i in data){
                if(data[i].orderlaundry.order_status_laundry_id == CONSTANT.ORDER_STATUS.DONE_LAUNDRY){
                    displayedData.push(data[i]);
                }
            }
        }else if(this.props.selectedTab == 'Komplain'){
            for(let i in data){
                if(data[i].orderlaundry.order_status_laundry_id == CONSTANT.ORDER_STATUS.APPLIED_COMPLAIN){
                    displayedData.push(data[i]);
                }
            }
        }else if(this.props.selectedTab == 'Batal'){
            for(let i in data){
                if(data[i].orderlaundry.order_status_laundry_id == CONSTANT.ORDER_STATUS.CANCEL){
                    displayedData.push(data[i]);
                }
            }
        }else if(this.props.selectedTab == 'Pesanan Selesai'){
            for(let i in data){
                if(data[i].orderlaundry.order_status_laundry_id == CONSTANT.ORDER_STATUS.DONE_ORDER){
                    displayedData.push(data[i]);
                }
            }
        }else if(this.props.selectedTab == 'Ditolak'){
            for(let i in data){
                if(data[i].orderlaundry.order_status_laundry_id == CONSTANT.ORDER_STATUS.CANNOT_FIND_PARTNER){
                    displayedData.push(data[i]);
                }
            }
        }

      for(let i in displayedData){

        let {orderlaundry} = displayedData[i];
        let {order_status_laundry_id} = orderlaundry;
        let shipmentType = '';

        if(order_status_laundry_id == CONSTANT.ORDER_STATUS.WAIT_OUTLET_CONFIRMATION || order_status_laundry_id == CONSTANT.ORDER_STATUS.WAIT_PICKUP || 
          order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_PICKUP || order_status_laundry_id == CONSTANT.ORDER_STATUS.DONE_PICKUP || 
          order_status_laundry_id == CONSTANT.ORDER_STATUS.CHECKOUT ){

          shipmentType = CONSTANT.SHIPMENT_TYPE.PICKUP;
        }else{
          shipmentType = CONSTANT.SHIPMENT_TYPE.DELIVERY;
        }

          let shipment = this.getShipment(displayedData[i], shipmentType);
          let {shipmentschedule, shipment_status_id, internalshipment} = shipment;
          let {shipment_date, time} = shipmentschedule;

          let shipmentDate = new Date(shipment_date);
          shipmentDate = new Date(shipmentDate.toDateString())
          
          displayedData[i].selectedShipment = shipment;
          tempResult.push(displayedData[i]);
      }

      let result = [];

      for(let i in tempResult){
          let obj = {};
          let shipment = tempResult[i].selectedShipment;
          let shipmentTimeInfo = shipment.shipmentschedule.shipment_time_info;
          let shipmentDate = shipment.shipmentschedule.shipment_date;
          let createdAt = tempResult[i].created_at.split(' ')[0];
          
          if (result.length == 0) {
            obj.time = shipmentTimeInfo;
            obj.date = shipmentDate;
            obj.createdAt = createdAt;
            obj.data = [];
            obj.data.push(tempResult[i]);
            result.push(obj);
          }
          else{
            let isExist = false;
            
            for(let j in result){
                if (filterType == 'time' && result[j].time == shipmentTimeInfo) {
                  isExist = true;
                  result[j].data.push(tempResult[i]);
                }
                else if(filterType == 'date' && result[j].date == shipmentDate){
                  isExist = true;
                  result[j].data.push(tempResult[i]);
                }
            }

            if (!isExist) {
                obj.time = shipmentTimeInfo;
                obj.date = shipmentDate;
                obj.data = [];
                obj.data.push(tempResult[i]);
                result.push(obj);
            }
          }
      }
    
      if (filterType == 'time') {
          result.sort((a,b) => {
              let shipmentA = a.time;
              shipmentA = shipmentA.split('-')[0].split(':')[0];
              let shipmentB = b.time;
              shipmentB = shipmentB.split('-')[0].split(':')[0];
              return shipmentA - shipmentB;
          })
      }
      else if(filterType == 'date'){
          result.sort((a,b) => {
            let shipmentA = a.date;
            let shipmentB = b.date;
              return new Date(shipmentA).getTime() - new Date(shipmentB).getTime();
          })
      }
      else if (filterType == 'createdDate') {
          result.sort((a,b) => {
              let shipmentA = a.createdAt;
              let shipmentB = b.createdAt;
              return new Date(shipmentA).getTime() - new Date(shipmentB).getTime();
          })
      }

      return result;
  }


	getShipment(item, type = CONSTANT.SHIPMENT_TYPE.DELIVERY){
    	let {shipments} = item;
    	for(let i in shipments){
      		let {shipmenttype} = shipments[i];

      		if (shipmenttype.id == type) {
        		return shipments[i];
      		}
    	}
  	}
}

function mapStateToProps(state, ownProps) {
  return {
    userRole:state.rootReducer.user.role,
    searchOrderResult:state.rootReducer.order.searchOrderResult,
  };
}

function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderList);
