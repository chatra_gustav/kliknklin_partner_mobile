import React, { Component, PureComponent } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        StyleSheet,
        Image,
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';

import {TextWrapper, Text, Button, Form, TabBar, OrderStatus, DateFormat, Scaling} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";


class BaseItem extends PureComponent{
  static defaultProps = {
    orderItem:[],
    data: [],
    submitButton: () => {},
    shipmentStatus:1,
    orderStatusLaundryID:1,
    orderHash:'',
    type:'order',
    problemDetails:'',
    complaintStatusID:1,
  }
  onPress(item = this.props.item){

    let {type} = this.props;

    if (type == 'complaint'){
      this.props.actionComplaint.setComplaintMessageList(this.props.item.complaint.messages);
      this.props.actionOrder.setOrderActive(this.props.item, CONSTANT.ORDER_ACTIVE_TYPE.COMPLAINT);
      this.props.actionOrder.setShipmentActivePickup(null);
      this.props.actionOrder.setShipmentActiveDelivery(null); 

      this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.OrderContent', {})
    }else if(type == 'order'){
      this.props.actionOrder.setOrderActive(this.props.item);
      this.props.actionOrder.setShipmentActivePickup(this.props.item);
      this.props.actionOrder.setShipmentActiveDelivery(this.props.item);

      this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.OrderContent', {})
    }
    else if (type == 'incoming-pos') {
      this.props.actionOrder.setOrderActive(this.props.item.order);
      this.props.actionOrder.setShipmentActiveDelivery(this.props.item.order);
      this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.OrderContent', {});
    }

  }
  getContent() {
    let {orderHash, customerName,data, submitButton, type, problemDetails, orderStatusLaundryID, complaintStatusID, shipmentStatus, logOrderPrice, orderItem} = this.props;
      
    if (type == 'incoming-online' || type == 'incoming-pos') {
      return(
        <IncomingContent {...this.props}/>
      )
    }
    else if (type == 'order' || type == 'complaint') {
      return(
        <RegularContent {...this.props}/>
      );
    }
  }
  render(){
    let {orderHash, customerName,data, submitButton, type, problemDetails, orderStatusLaundryID, complaintStatusID, shipmentStatus, logOrderPrice, orderItem} = this.props;
   
    try{
      return(
        <Button.TypeC
            onPress={this.onPress.bind(this)}
            containerStyle={{
              marginBottom:Scaling.verticalScale(25),
              paddingLeft:'8%',
            }}
        >
              {this.getContent()}
        </Button.TypeC>
      );
    }
    catch(error){
        console.log(error, 'orderlist.base_item.render');
        return null;
    }
  }
}

class RegularContent extends PureComponent{
  render(){
    let {onlineFlag, orderHash, customerName,data, submitButton, type, problemDetails, orderStatusLaundryID, complaintStatusID, shipmentStatus, logOrderPrice, orderItem} = this.props;
    
    return(
      <View>
            <View
                style={{
                  position:'absolute',
                  right:5,
                  top:5,
                  //width:moderateScale(25),
                  //height:moderateScale(25),
                  borderRadius:moderateScale(25/2),
                  backgroundColor:CONSTANT.COLOR.BLUE_A,
                  alignItems:'center',
                  justifyContent:'center',
                }}
            >
              {
                type == 'order' ? 
                  <OrderStatus
                      orderStatus={orderStatusLaundryID}
                      shipmentStatus={shipmentStatus}
                      onlineFlag={onlineFlag != undefined ? onlineFlag : true}
                  />
                  :
                  <OrderStatus
                      complaintStatus={complaintStatusID}
                  />
              }
            </View>

              <View style={{flex:0.20,}}>
                  <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>
                    {orderHash}
                  </Text>
              </View>
              <View style={{flex:0.55, paddingTop:Scaling.verticalScale(15)}}>
                  {data.map((item, key) => 
                      <View key={key} style={content.itemRow}>
                        <View style={content.itemRowTitle}>
                          <Text {...item.props} fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>
                            {item.category}
                          </Text>
                        </View>

                        <View style={content.itemRowColon}>
                          <Text {...item.props} fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>
                            {':'}
                          </Text>
                        </View>

                        <View style={content.itemRowContent}>
                          <Text {...item.props} fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>
                            {item.content}
                          </Text>
                        </View>
                      </View>
                  )}
                    
                    {
                      orderItem.length != 0 ? 
                        <TouchableOpacity style={[content.itemRow, {
                          height:Scaling.verticalScale(30),
                          alignItems:'center',
                          flexDirection:'row',
                        }]}
                          onPress={() => {
                            if (type == 'order') {
                                this.props.actionNavigator.showLightBox('PartnerKliknKlin.BoxItemDetail', {
                                  overrideBackPress:false,
                                  item:orderItem,
                                  logOrderPrice,
                                  customerName
                                });
                            }
                            else if (type == 'complaint') {
                                this.props.actionNavigator.showLightBox('PartnerKliknKlin.BoxComplaintItemDetail', {
                                  overrideBackPress:false,
                                  item:orderItem,
                                  problemDetails:problemDetails,
                                });
                            }
                          }}
                        >
                            <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER} textStyle={{
                              color:CONSTANT.COLOR.LIGHT_BLUE
                            }}>
                              {type == 'order' ? 'Lihat Pesanan' : 'Lihat Keluhan'}
                            </Text>

                            <Image
                              style={{
                                tintColor:CONSTANT.COLOR.LIGHT_BLUE,
                                width: Scaling.scale(15), 
                                height: Scaling.verticalScale(15),
                                marginLeft:Scaling.moderateScale(5.5),
                                marginTop:Scaling.verticalScale(4),
                              }}
                              source={CONSTANT.IMAGE_URL.BOTTOM_ARROW_ICON}
                            />
                        </TouchableOpacity>
                        :
                        null
                    }
              </View>

              <View
                style={{
                  flex:0.3,
                  alignItems:'center',
                }}
              >
                {submitButton()}
              </View>
      </View>
    );
  }
}

class IncomingContent extends PureComponent{

  getSubmitButton(){

  }
  render(){
    try{
      let {isOnlineOrder, deliveryDate, deliveryTime, pickupDate, pickupTime, orderHash, customerName,data, submitButton, type, problemDetails, orderStatusLaundryID, complaintStatusID, shipmentStatus, logOrderPrice, orderItem} = this.props;
      return(
        <View
          style={{
            flex:1,
          }}
        >
              <View
                style={{
                  position:'absolute',
                  right:5,
                  top:5,
                  //width:moderateScale(25),
                  //height:moderateScale(25),
                  borderRadius:moderateScale(25/2),
                  backgroundColor:CONSTANT.COLOR.BLUE_A,
                  alignItems:'center',
                  justifyContent:'center'
                }}
            >
              <OrderStatus
                  orderStatus={orderStatusLaundryID}
              />
            </View>
              <View style={{flex:0.20}}>
                  <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>
                    {orderHash}
                  </Text>
                  <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>
                      {DateFormat(new Date(isOnlineOrder ? pickupDate : deliveryDate), 'fullDate')}
                  </Text>

                  <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>
                      {isOnlineOrder ? pickupTime : deliveryTime}
                  </Text>
              </View>
              <View style={{flex:0.55}}>
                  {data.map((item, key) => 
                      <View key={key} style={content.itemRow}>
                        <View style={content.itemRowTitle}>
                          <Text {...item.props} fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>
                            {item.category}
                          </Text>
                        </View>

                        <View style={content.itemRowColon}>
                          <Text {...item.props} fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>
                            {':'}
                          </Text>
                        </View>

                        <View style={content.itemRowContent}>
                          <Text {...item.props} fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>
                            {item.content}
                          </Text>
                        </View>
                      </View>
                  )}
                    
                    {
                      orderItem.length != 0 ? 
                        <TouchableOpacity style={[content.itemRow, {
                          height:Scaling.verticalScale(30),
                          alignItems:'center',
                          flexDirection:'row'
                        }]}
                          onPress={() => {
                            if (type == 'order' || type == 'incoming-pos' || type == 'incoming-online') {
                                this.props.actionNavigator.showLightBox('PartnerKliknKlin.BoxItemDetail', {
                                  overrideBackPress:false,
                                  item:orderItem,
                                  logOrderPrice,
                                  customerName
                                });
                            }
                            else if (type == 'complaint') {
                                this.props.actionNavigator.showLightBox('PartnerKliknKlin.BoxComplaintItemDetail', {
                                  overrideBackPress:false,
                                  item:orderItem,
                                  problemDetails:problemDetails,
                                });
                            }
                          }}
                        >
                            <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER} textStyle={{
                              color:CONSTANT.COLOR.LIGHT_BLUE
                            }}>
                              {type == 'order' || type == 'incoming-pos' || type == 'incoming-online' ? 'Lihat Pesanan' : 'Lihat Keluhan'}
                            </Text>

                            <Image
                              style={{
                                tintColor:CONSTANT.COLOR.LIGHT_BLUE,
                                width: Scaling.scale(15), 
                                height: Scaling.verticalScale(15),
                                marginLeft:Scaling.moderateScale(5.5),
                                marginTop:Scaling.verticalScale(4),
                              }}
                              source={CONSTANT.IMAGE_URL.BOTTOM_ARROW_ICON}
                            />
                        </TouchableOpacity>
                        :
                        null
                    }
              </View>

              <View
                style={{
                  flex:0.3,
                }}
              >
                {submitButton()}
              </View>

        </View>
      );
    }
    catch(error){
        console.log(error, 'orderlist.base_item.render');
        return null;
    }
  }
}

const styleInProgress = StyleSheet.create({
  itemRowTitle:{flex:0.35},
});

const content = StyleSheet.create({
  titleRow:{
    flexDirection:'row', 
    alignItems:'center',
  },
  titleRowImage:{
    flex:0.1,
  },
  titleRowContent:{
    flex:0.4,
  },
  titleRowRightContent:{
    flex:0.5,
    alignItems:'flex-end',
  },
  itemRow:{
    marginTop:Scaling.verticalScale(3),
    flexDirection:'row',
  },
  itemRowTitle:{
    flex:0.25,
  },
  itemRowColon:{
    flex:0.025
  },
  itemRowContent:{
    flex:0.725,
  },
  itemDetail:{
    paddingHorizontal:Dimensions.get('window').width * 0.025,
    paddingVertical:Dimensions.get('window').height * 0.01,
    borderColor:'#F0F0F0',
  },
});

function mapStateToProps(state, ownProps) {
  return {
    selectedOrderDate:state.rootReducer.order.selectedOrderDate,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionOrder: bindActionCreators(actions.order, dispatch),
    actionComplaint: bindActionCreators(actions.complaint, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BaseItem);

