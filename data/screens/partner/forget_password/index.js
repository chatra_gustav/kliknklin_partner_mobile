import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableHighlight, Linking, Platform} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ResolveAssetSource from 'resolveAssetSource';
import SInfo from 'react-native-sensitive-info';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

//custom component js class import
import {TextWrapper, Form, Button, Text, Scaling} from './../../../components';

//layout
import {LayoutPrimary} from './../../../layout';

import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class ForgetPassword extends Component {
	constructor(props) {
        super(props);
        this.state = {
        	image:{
        		email_icon:{
        			width:0,
					height:0,
					multiplierWidth:0.4,
					multiplierHeight:0.4,
					url:require('./../../../image/forget_password/email_icon.png'),
        		}
        	},
        	data:{
        		email:'',
        	},
        	errorMessage:' ',
        }; 
    }

	componentDidMount(){
		this._getImageSize();
	}

	_getImageSize(){
		var image = this.state.image;

		var image_email_icon = ResolveAssetSource(this.state.image.email_icon.url);

		image.email_icon.width = Scaling.moderateScale(image_email_icon.width);
		image.email_icon.height = Scaling.moderateScale(image_email_icon.height);

      	this.setState({image});
	}

	_validate(){
		let error = '';
		
		error = this._validateEmpty();

		if (error == '') {
			error = this._validateEmail();
		}
		
		return error;
	}

	_validateEmpty(data = this.state.data){
		var result = '';
		
		if (data.email.trim() == '') {
			result = STRING.ERROR.LOGIN.EMPTY_FIELD;
		}

		return result;
	}

	_validateEmail(data = this.state.data){
		var result = '';

    	var regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;;
    	
    	if(!regexEmail.test(data.email)){
			result = STRING.ERROR.LOGIN.EMAIL_FORMAT;
    	}

    	return result;
    }

    _onChangeEmail(email){
    	var data = this.state.data;
    	data.email = email;
		this.setState({data});
    }

    _onPressSubmitButton(){
    	
    	let errorMessage = this._validate();
		
    	if (errorMessage == '') {
    		this.props.actionAuthentication.forgetPassword(this.state.data.email)
    		.then((response) => {
    			let {status} = response;

				if(status.code != CONSTANT.HTTP_STATUS_CODE.OK){
					this.setState({errorMessage:'Email yang dimasukkan tidak tersedia / salah'});
				}
				else{
					this.setState({isResetEmailSend:true});
				}
    		})
    		.catch((error) => {

    		});
    	}
    	else{
    		this.setState({errorMessage});
    	}
    }

	_renderContent(){
		return(
			<View style={{flex : 1}}>
				<KeyboardAwareScrollView>
					<View style={styles.container}>		
						<View style={[styles.containerHeaderForgetPassword,{height: Scaling.verticalScale(140)}]}>
							<ResponsiveImage
								source={this.state.image.email_icon.url}
								initWidth={this.state.image.email_icon.width * this.state.image.email_icon.multiplierWidth}
								initHeight={this.state.image.email_icon.height * this.state.image.email_icon.multiplierHeight}
							/>
						</View>

						<View style={styles.containerBodyForgetPassword}>

							{this.renderMiddleContent()}

				            <View style={styles.formButton}>
					            <Button.TypeD
			                 		buttonText={(this.state.isResetEmailSend ? STRING.SCREEN.FORGET_PASSWORD.SEND_EMAIL_AGAIN : STRING.SCREEN.FORGET_PASSWORD.SEND_EMAIL)}
			                      	buttonColor={CONSTANT.COLOR.BLUE}
			                      	buttonTextStyle={[{color: CONSTANT.COLOR.WHITE, fontSize: Scaling.moderateScale(13), fontFamily: CONSTANT.TEXT_FAMILY.MAIN, fontWeight: CONSTANT.TEXT_WEIGHT.MEDIUM.weight}]}
			                      	buttonStyle={styles.buttonStyle}
			                      	onPress={this._onPressSubmitButton.bind(this)}
			                	/>
			                </View>
						</View>
					</View>
				</KeyboardAwareScrollView>	

				<View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-end', paddingBottom: Scaling.verticalScale(5)}}>
					<Text fontOption={CONSTANT.TEXT_OPTION.CONTENT_SMALLER} textStyle={{color: CONSTANT.COLOR.LIGHT_GRAY}}>Partner App Ver 2.0</Text>
				</View>
			</View>
		)
	}

	renderMiddleContent(){
		if(this.state.isResetEmailSend){
			return(
				<View style={{ alignItems: 'center', justifyContent: 'center'}}>
					<Text textStyle={{color: CONSTANT.COLOR.BLACK, fontFamily: CONSTANT.TEXT_FAMILY.MAIN, fontSize: Scaling.moderateScale(13)}}>{STRING.SCREEN.FORGET_PASSWORD.MIDDLE_TEXT_1}</Text>
					<View style={{flexDirection: 'row', marginBottom: Scaling.verticalScale(15)}}>
						<Text textStyle={{color: CONSTANT.COLOR.BLACK, fontFamily: CONSTANT.TEXT_FAMILY.MAIN, fontSize: Scaling.moderateScale(13)}}>{STRING.SCREEN.FORGET_PASSWORD.MIDDLE_TEXT_2}</Text>
						<Text textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE, fontFamily: CONSTANT.TEXT_FAMILY.MAIN, fontSize: Scaling.moderateScale(13)}}>  {this.state.data.email}</Text>
					</View>

					<Text textStyle={{color: CONSTANT.COLOR.BLACK, fontFamily: CONSTANT.TEXT_FAMILY.MAIN, fontSize: Scaling.moderateScale(13)}}>{STRING.SCREEN.FORGET_PASSWORD.MIDDLE_TEXT_3}</Text>
				</View>
			)
		}else{
			return(
				<Form.InputField.TypeE
	                keyboardType={'email-address'}
	                ref={(ref) => this.email_text_field = ref} 
	                label={STRING.SCREEN.FORGET_PASSWORD.SUBTITLE}
	                onChangeText={this._onChangeEmail.bind(this)}
	            />
			)
		}
	}

	_renderSuccessSendEmail(){
		let {actionNavigator, navigator} = this.props;
		let {data} = this.state;
		return(
			<View>
				<View style={[style.containerHeaderForgetPassword, {height:this.props.layoutHeight * 0.7}]}>
					<ResponsiveImage
						source={this.state.image.email_icon.url}
						initWidth={this.state.image.email_icon.width * this.state.image.email_icon.multiplierWidth}
						initHeight={this.state.image.email_icon.height * this.state.image.email_icon.multiplierHeight}
					/>
				</View>

				<View style={style.containerBodyLogin}>
					<TextWrapper fontWeight={'Medium'} type={'smallercontent'} style={{textAlign:'center'}}>
							{'Kami telah mengirimkan tautan ke email ' + data.email +  ' anda. \n Silahkan buka email tersebut untuk proses lebih lanjut.'} 
						</TextWrapper>

						<TextWrapper fontWeight={'Medium'} type={'smallercontent'} style={{textAlign:'center', marginTop: Scaling.moderateScale(15),}}>
							{'Jika kata kunci telah berhasil diubah, '}
						</TextWrapper>

						<View
							style={{
								flexDirection:'row',
								alignItems:'center',
								alignSelf:'center',
							}}
						>
							<TextWrapper fontWeight={'Medium'} type={'smallercontent'} style={{textAlign:'center'}}>
								{'silahkan '}
							</TextWrapper>

							<Form.LinkField.TypeA
						        label={'Login'}
					            type={'smallercontent'}
					            containerStyle={{marginRight:3}}
								labelStyle={{color:CONSTANT.COLOR.BLUE_A}}
								onPress={()=>{
									actionNavigator.resetTo(navigator, 'PartnerKliknKlin.SignIn');
								}}
			                />

			                <TextWrapper fontWeight={'Medium'} type={'smallercontent'} style={{textAlign:'center'}}>
								{'kembali.'}
							</TextWrapper>
						</View>
				</View>
			</View>
		);
	}

	render(){
		return (
			<LayoutPrimary
				containerStyle={{
					backgroundColor:'white',
				}}
				showBackButton={true}
				navigator={this.props.navigator}
				showNavBar={true}
				showTabBar={false}
				showTitle={true}
	            title={'Reset Password'}
				ignoreNavbar
			>
				{this._renderContent()}
			</LayoutPrimary>
		)
	}
}

var styles = StyleSheet.create({
    container: {
        flex:1
    },
    containerHeaderForgetPassword:{
   		alignItems:'center',
   		justifyContent: 'center',
    },
    containerBodyLogin:{
    	flex:1,
    },
	containerTitleBodyLogin:{
		alignItems:'center',
	},
	containerFormBodyLogin:{
		alignItems:'center',
		marginTop:Scaling.moderateScale(10),
		flex:0.6
    },
    containerFooterLogin:{
		justifyContent:'center',
		alignItems:'center',
    },
    containerBodyForgetPassword:{
    	paddingTop: Scaling.moderateScale(20),
    	alignItems: 'center'
    },
    buttonStyle:{
	    borderRadius: Scaling.moderateScale(15),
	    width:Dimensions.get('window').width * 0.87,
	    height: Scaling.verticalScale(40),
	    borderRadius: 10,
	},
	formButton:{
		marginTop: Scaling.verticalScale(45)
	}
});

function mapStateToProps(state, ownProps) {
	return {
		errorMessage:state.rootReducer.message.error,
		layoutHeight:state.rootReducer.layout.height,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionAuthentication: bindActionCreators(actions.authentication, dispatch),
		actionRoot: bindActionCreators(actions.root, dispatch),
		actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgetPassword);