import React, { Component } from 'react';
import Accordion from 'react-native-collapsible/Accordion';
import { View, Text, StyleSheet, ScrollView, Dimensions, Image } from 'react-native';

const SECTIONS = [
  {
    id: '1',
    title: 'Apa itu aplikasi mobile partner kliknklin?',
    content: 'Aplikasi partner adalah aplikasi berbasis android yang dibuat untuk partner kliknklin mendapatkan order online dari customer kliknklin.'
  },
  {
    id: '2',
    title: 'Bagaimana cara mendapatkan aplikasi mobile partner?',
    content: 'Setelah mempunyai akun yang terdaftar pada sistem partner, pemilik atau pegawai login pada website partner.kliknklin.co/login menggunakan akun tersebut dan mengunduh aplikasi mobile pada menu “bantuan”. Setelah didownload lalu aplikasi akan otomatis terinstall pada gadget. \n\n Jika ada update aplikasi, sistem pada aplikasi mobile akan mengarahkan  ke web partner.',
  },
  {
    id: '3',
    title: 'Apakah ada standard tipe handphone untuk dapat menjalankan aplikasi mobile?',
    content: 'Tipe handphone untuk dapat menjalankan aplikasi mobile adalah handphone dengan OS berbasis android 4.1 (jelly bean)',
  },
  {
    id: '4',
    title: 'Bagaimana cara menggunakan aplikasi mobile partner?',
    content: 'Daftarkan outlet dan staff pada website partner, lakukan deposit minimal Rp 100.000. Buka aplikasi mobile, lalu masuk aplikasi menggunakan akun yang telah terdaftar. Lakukan aktifitas pendataan bidding, pengambilan, konfirmasi, dan pengantaran order pada aplikasi.',
  },
  {
    id: '5',
    title: 'Bagaimana cara melakukan bidding order?',
    content: 'Notifikasi order yang masuk terdapat pada menu beranda. Tekan card “Incoming”, setelah masuk pada halaman “Incoming Orders” lalu tekan tombol “Terima” jika ingin mengambil order, dan “Tolak” jika ingin menolak order, lalu lakukan konfirmasi pada popup.',
  },
  {
    id: '6',
    title: 'Bagaimana cara melihat detil order?',
    content: 'Detil order baru dapat terlihat jika daftar order masuk telah diterima oleh pihak partner, setelah itu detil order baru dapat terlihat ketika menekan salah satu list order yang diinginkan.',
  },
  {
    id: '7',
    title: 'Bagaimana cara mencari order yang spesifik?',
    content: 'Untuk setiap menu telah disediakan fungsi pencarian pada posisi atas halaman. Order-order secara spesifik dapat dicari berdasarkan ID Order, Nama Customer, dan Alamat.'
  },
  {
    id: '8',
    title: 'Bagaimana cara menandai order telah diambil?',
    content: 'Notifikasi order yang perlu diambil segera terdapat pada menu beranda. Tekan card “Pickup”, setelah masuk pada menu “Pickup Orders” lalu tekan tombol “Telah Diambil”. Masukan jumlah kilo dan lembar baju yang telah dihitung pada pop-up modal, lalu tekan tombol “Konfirmasi” dan lakukan konfirmasi pada popup. \n\n Catatan jika ingin melihat navigasi alamat penjemputan tekan order list yang diinginkan, pada peta tersedia tekan sign arah yang berwarna biru dikanan bawah peta. Lalu pilih aplikasi maps yang sudah terinstal pada handphone.',
  },
  {
    id: '9',
    title: 'Bagaimana cara menandai order akan selesai dikerjakan?',
    content: 'Notifikasi order yang perlu dikonfirmasi waktu selesainya dengan segera terdapat pada menu beranda. Tekan card “On Progress”, setelah masuk pada menu “On Progress Orders” lalu tekan tombol “Lapor Selesai” dan lakukan konfirmasi pada popup.',
  },
  {
    id: '10',
    title: 'Bagaimana cara menandai order telah diantar?',
    content: 'Notifikasi order diantar segera terdapat pada menu beranda. Tekan card “Delivery”, setelah masuk pada menu “Delivery Orders” lalu tekan tombol “Telah Dikirim” dan lakukan konfirmasi pada popup.',
  },
  {
    id: '11',
    title: 'Bagaimana cara menyampaikan pertanyaan dan keluhan?',
    content: 'Hubungi kami via email di support@kliknklin.co atau whatsapp 0811-1566-664',
  },
];

const FAQStyles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#F5FCFF',
    top:70
  },
  title: {
    textAlign: 'center',
    fontSize: 22,
    fontWeight: '300',
    marginBottom: 20,
  },
  header: {
    marginLeft: 10,
    marginRight:10,
    flex:1,
    borderTopWidth:1,
    borderColor:'lightgray',
    backgroundColor:'white',
    flexDirection:'row',
    minHeight:Dimensions.get('window').height * 0.10,
    alignItems:'center'
  },
  headerText: {
    color:'gray',
    fontSize: 16,
    fontWeight: '500',
  },
  content: {
    flex: 1,
    paddingTop:5,
    padding: 15,
    backgroundColor: 'white',
  },
  contentText:{
    flex:1,
    color:'gray',
    textAlign:'justify',
    fontWeight:'normal'
  },
  active: {
    backgroundColor: 'rgba(255,255,255,1)',
  },
  inactive: {
    backgroundColor: 'rgba(245,252,255,1)',
  },
  selectors: {
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  selector: {
    backgroundColor: '#F5FCFF',
    padding: 10,
  },
  activeSelector: {
    fontWeight: 'bold',
  },
  selectTitle: {
    fontSize: 14,
    fontWeight: '500',
    padding: 10,
  },
});

export class FAQ extends Component {
      constructor(props){
    super(props);
    this.state = {
      layoutHeight:Dimensions.get('window').height * 0.8,
    }
  }

	_renderHeader(section) {
    return (
      <View style={FAQStyles.header}>
        <View style={{flex:0.075, width:Dimensions.get('window').width * 0.075, height:Dimensions.get('window').width * 0.075, borderRadius:5, borderWidth:1, borderColor:'lightgray', alignItems:'center', justifyContent:'center'}}>
          <TextWrapper type='content' style={FAQStyles.headerText}>{section.id}</TextWrapper>
        </View>
        <View style={{flex:0.935}}>
          <TextWrapper type='content' style={[FAQStyles.headerText,{marginLeft:10,}]}>{section.title}</TextWrapper>
        </View>
        </View>
    );
  }

  _renderContent(section) {
    if (section.detail == undefined) {
      return (
        <View style={FAQStyles.content}>
          <TextWrapper type='base' style={FAQStyles.contentText}>{section.content}</TextWrapper>
        </View>
      );
    }
    else{
      return (
        <View style={FAQStyles.content}>
          <TextWrapper type='base' style={FAQStyles.contentText}>{section.content}</TextWrapper>
          <TextWrapper type='base' style={[FAQStyles.contentText, {marginLeft:5, marginTop:5}]}>{section.detail[0]}</TextWrapper>
          <TextWrapper type='base' style={[FAQStyles.contentText, {marginLeft:5, marginTop:5}]}>{section.detail[1]}</TextWrapper>
          <TextWrapper type='base' style={[FAQStyles.contentText, {marginLeft:5, marginTop:5}]}>{section.detail[2]}</TextWrapper>
        </View>
      );
    }
  }

    render(){
        return (
          <View style={{flex:1, backgroundColor:'white'}}>
            	<ScrollView>
                  <Accordion
      		        sections={SECTIONS}
      		        renderHeader={this._renderHeader}
      		        renderContent={this._renderContent}
                  underlayColor='lightgray'
      		        />
    		     </ScrollView>
          </View>
        );
    }
}
