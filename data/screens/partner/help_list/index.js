import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableHighlight,
        ScrollView,
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';
import DeviceInfo from 'react-native-device-info';


import {LayoutPrimary} from './../../../layout';
import {TextWrapper, Button} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class HelpList extends Component {
    constructor(props){
        super(props);
        this.state = {
            image:{
                right_triangle_icon:{
                    width:0,
                    height:0,
                    multiplierWidth:0.065,
                    multiplierHeight:0.065,
                    url:require('./../../../image/layout/right_triangle_icon.png'),
                },
                kliknklin_icon:{
                    width:0,
                    height:0,
                    multiplierWidth:0.25,
                    multiplierHeight:0.25,
                    url:require('./../../../image/layout/kliknklin_icon.png'),
                },
            },
        };
    }

    componentWillMount(){
        this.getImageSize();
    }

    getImageSize(){
        var image = this.state.image;

        var image_right_triangle_icon = ResolveAssetSource(this.state.image.right_triangle_icon.url);

        image.right_triangle_icon.width = image_right_triangle_icon.width;
        image.right_triangle_icon.height = image_right_triangle_icon.height;

        var image_kliknklin_icon = ResolveAssetSource(this.state.image.kliknklin_icon.url);

        image.kliknklin_icon.width = image_kliknklin_icon.width;
        image.kliknklin_icon.height = image_kliknklin_icon.height;

        this.setState({image});
    }

    _onPress(helpType){
        this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.HelpContent', {helpType});
    }

    render(){
        return(
            <LayoutPrimary
                showTitle={true}
                showTabBar={true}
                navigator={this.props.navigator}
                title={'Bantuan'}
                tabKey={this.props.tabKey}
                showNotificationButton={true}
                showSearchButton={true}
            >
                <ScrollView
                    contentContainerStyle={{
                        paddingBottom:moderateScale(75),
                        alignItems:'center',
                    }}
                >  
                    <HelpItem
                        itemText={'Pertanyaan Umum'}
                        image={this.state.image}
                        onPress={() => {
                            this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.WebView', {
                                title:'Pertanyaan Umum',
                                url:CONSTANT.APP_URL.FAQ,
                            })
                        }}
                    />

                    <HelpItem
                        itemText={'Standar Prosedur'}
                        image={this.state.image}
                        onPress={() => {
                            this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.WebView', {
                                title:'Standar Prosedur',
                                url:CONSTANT.APP_URL.STANDARD_OPERATING_PROCEDURE,
                            })
                        }}
                    />

                     <HelpItem
                        itemText={'Panduan Aplikasi'}
                        image={this.state.image}
                        onPress={() => {
                            this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.WebView', {
                                title:'Panduan Aplikasi',
                                url:CONSTANT.APP_URL.USER_GUIDE,
                            })
                        }}
                    />

                    <HelpItem
                        itemText={'Syarat & Ketentuan Konsumen'}
                        image={this.state.image}
                        onPress={() => {
                            this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.WebView', {
                                title:'Syarat dan Ketentuan Konsumen',
                                url:CONSTANT.APP_URL.TERMS_AND_CONDITION_CUSTOMER,
                            })
                        }}
                    />

                    <HelpItem
                        itemText={'Perjanjian'}
                        image={this.state.image}
                        onPress={() => {
                            this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.WebView', {
                                title:'Perjanjian Mitra',
                                url:CONSTANT.APP_URL.AGREEMENT,
                            })
                        }}
                    />

                    <HelpItem
                        itemText={'Kritik dan Saran'}
                        image={this.state.image}
                        onPress={this._onPress.bind(this, 'feedback')}
                    />

                    <HelpItem
                        itemText={'Hubungi Kami'}
                        image={this.state.image}
                        onPress={this._onPress.bind(this, 'contact-us')}
                    />
                </ScrollView>
            </LayoutPrimary>
        );
    }
}

class HelpItem extends Component{
    render(){
        return(
            <Button.TypeB
                onPress={this.props.onPress}
                buttonText={this.props.itemText}
                buttonTextType={'smallcontent'}
                contentContainerStyle={{
                    justifyContent:'flex-start',
                }}
                containerStyle={{
                    height:Dimensions.get('window').height * 0.125,
                }}
                iconRight={
                    <ResponsiveImage
                        source={this.props.image.right_triangle_icon.url}
                        initWidth={this.props.image.right_triangle_icon.width * this.props.image.right_triangle_icon.multiplierWidth}
                        initHeight={this.props.image.right_triangle_icon.height * this.props.image.right_triangle_icon.multiplierHeight}
                        style={{tintColor:CONSTANT.COLOR.GRAY_B}}
                    />
                }
            />
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        layoutHeight:state.rootReducer.layout.height,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actionNavigator: bindActionCreators(actions.navigator, dispatch),
        actionRoot: bindActionCreators(actions.root, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(HelpList);


