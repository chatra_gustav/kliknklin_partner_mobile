import React, { Component } from 'react';
import { View, Text, ScrollView, Dimensions } from 'react-native';

export class WorkFlow extends Component {

    constructor(props){
        super(props);
        this.state = {
        }
    }


    render(){
        return (
           <View style={{flex:1, backgroundColor:'white'}}>
                <ScrollView
                    style={{margin:10}}
                >
                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                        1.
                        </TextWrapper>
                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                            Masuk ke aplikasi
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                            Masukan alamat email dan password yang terdaftar
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:15, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                        2.
                        </TextWrapper>
                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                            Melihat ringkasan order online yang sedang dikerjakan pada menu “Beranda”
                        </TextWrapper>
                    </View>

                   <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Kartu “Incoming” menunjukan jumlah order online yang masuk
                        </TextWrapper>
                    </View>

                   <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                         Kartu “Pickup” menunjukan jumlah order online yang akan diambil dan notifikasinya adalah jumlah order online yang harus segera diambil dalam jangka waktu 2 jam kedepan
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Kartu “On Progress” menunjukan jumlah order online yang akan diselesaikan dan notifikasinya adalah jumlah order online yang harus segera dikonfirmasi selesainya dalam jangka waktu 12 jam sebelum waktu delivery
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Kartu “Delivery” menunjukan jumlah order online yang akan diantar dan notifikasinya adalah jumlah order online yang harus segera diantar dalam jangka waktu 2 jam kedepan
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                        Catatan : jika salah satu kartu ditekan, akan mengarah ke halaman list order per masing-masing judul
                        </TextWrapper>
                    </View>
                    
                    <View style={{marginTop:15, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                        3.
                        </TextWrapper>
                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                            Melakukan pencarian
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>

                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                            Pencarian semua order online berdasarkan ID Order, Nama Customer, atau Alamat Customer pada bar pencarian di halaman “Beranda”
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Pencarian order spesifik dengan membuka halaman order list yang spesifik. Pada halaman tersebut telah disediakan bar pencarian pada posisi atas halaman. Order-order secara spesifik dapat dicari berdasarkan ID Order, Nama Customer, dan Alamat sesuai dengan data yang ada pada tahap order.
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:15, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                        4.
                        </TextWrapper>
                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                            Melakukan bidding order
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                           Notifikasi order yang masuk terdapat pada menu beranda 
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>

                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Tekan card “Incoming”
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Pada halaman “Incoming Orders” lalu tekan tombol “Terima” jika ingin mengambil order, dan “Tolak” jika ingin menolak order, lalu lakukan konfirmasi pada popup.
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                        Catatan :
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:45, fontWeight:'bold'}}/>

                        <TextWrapper type='base' style={{textAlign:'justify', width:15, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                         Sistem akan mencatat barapa banyak order yang diterima atau ditolak. 
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:45, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:15, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                        Order online yang sudah di terima tidak dapat di cancel oleh user. Jika diperlukan untuk mencancel harap menghubungi pihak kliknklin.
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:45, fontWeight:'bold'}}/>

                        <TextWrapper type='base' style={{textAlign:'justify', width:15, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                        Jika notifikasi order masuk dalam 5 menit tidak segera diambil, maka order akan dialihkan ke laundry lain.
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:15, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:15, fontWeight:'bold'}}>
                        5.
                        </TextWrapper>
                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                            Melihat detil order
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Buka salah satu kartu (pickup , on progress, delivery) pada menu “Beranda”
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Detil order pada ke tiga tahap tersebut berbeda sesuai dengan kebutuhannya
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Pada detil order terdapat tombol-tombol untuk dapat melakukan aksi
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Pada detil order untuk tahap pickup dan delivery terdapat alamat yang dapat dilihat rutenya menggunakan gmaps dengan menekan tombol “direction”
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Catatan : order detail baru dapat terlihat jika daftar order masuk telah diterima oleh pihak partner, setelah itu detil order baru dapat terlihat ketika menekan salah satu list order yang diinginkan.
                        </TextWrapper>
                    </View>

                     <View style={{marginTop:15, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                        6.
                        </TextWrapper>
                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                            Menandai status order menjadi telah diambil
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Notifikasi order yang perlu diambil segera terdapat pada menu beranda. 
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Tekan kartu “Pickup”, setelah masuk pada menu “Pickup Orders” lalu tekan tombol “Telah Diambil”. 
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Masukan jumlah kilo dan lembar baju yang telah dihitung pada pop-up modal, lalu tekan tombol “Konfirmasi” dan lakukan konfirmasi pada popup.
                        </TextWrapper>
                    </View>

                     <View style={{marginTop:15, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                        7.
                        </TextWrapper>
                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                            Menandai status order menjadi selesai dikerjakan
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Notifikasi order yang perlu dikonfirmasi waktu selesainya dengan segera terdapat pada menu beranda. 
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Tekan kartu “On Progress”, setelah masuk pada menu “On Progress Orders” lalu tekan tombol “Lapor Selesai” dan lakukan konfirmasi pada popup.
                        </TextWrapper>
                    </View>

                     <View style={{marginTop:15, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                        8.
                        </TextWrapper>
                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                            Menandai status order menjadi telah diantar
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Notifikasi order diantar segera terdapat pada menu beranda. 
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Tekan card “Delivery”, setelah masuk pada menu “Delivery Orders” lalu tekan tombol “Telah Dikirim” dan lakukan konfirmasi pada popup.
                        </TextWrapper>
                    </View>

                     <View style={{marginTop:15, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                        9.
                        </TextWrapper>
                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                            Melakukan Pengaturan
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Melihat akun user yang sedang login pada aplikasi
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Untuk user dengan status manager dapat mengaktifkan dan menonaktifkan outlet pada halaman pengaturan. Outlet yang aktif dapat melakukan bidding order.
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:15, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                        10.
                        </TextWrapper>
                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                            Melihat Bantuan
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Melihat “FAQ” mengenai pertanyaan yang sering ditanyakan untuk aplikasi POS.
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Melihat “Workflow” mengenai alur kerja pemakaian POS.
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Melihat terms and condition yang akan diarahkan ke web.
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:5, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}/>
                        
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                           {'- '}
                        </TextWrapper>

                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                          Merating google playstore untuk aplikasi pos android.
                        </TextWrapper>
                    </View>

                    <View style={{marginTop:15, flexDirection:'row'}}>
                        <TextWrapper type='base' style={{textAlign:'justify', width:25, fontWeight:'bold'}}>
                        11.
                        </TextWrapper>
                        <TextWrapper type='base' style={{flex:1, textAlign:'justify'}}>
                            Keluar Aplikasi dengan menekan tombol “Log-Out”
                        </TextWrapper>
                    </View>

                </ScrollView>
            </View>
        );
    }
}
