import React, { Component } from 'react';
import { 	
        Platform,
        View,
        Dimensions,
        TouchableHighlight,
        WebView,
  		} from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import TimerMixin from 'react-timer-mixin';

import {LayoutPrimary} from './../../../layout';
import {TextWrapper, Button, Container} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as appActions from "./../../../actions";

class HelpContent extends Component {
    constructor(props){
        super(props);
        this.state = {
            image:{
                attendance_icon:{
                    width:0,
                    height:0,
                    multiplierWidth:0.15,
                    multiplierHeight:0.15,
                    url:require('./../../../image/utility/attendance_icon.png'),
                },
                performance_icon:{
                    width:0,
                    height:0,
                    multiplierWidth:0.15,
                    multiplierHeight:0.15,
                    url:require('./../../../image/utility/performance_icon.png'),
                }
            },
        };
    }

    componentWillMount(){
        this.getImageSize();
    }


    componentWillUnmount() {
        TimerMixin.clearInterval(this.intervalCheckTimer);
    }

    getImageSize(){
        var image = this.state.image;

        var image_attendance_icon = ResolveAssetSource(this.state.image.attendance_icon.url);

        image.attendance_icon.width = image_attendance_icon.width;
        image.attendance_icon.height = image_attendance_icon.height;

        var image_performance_icon = ResolveAssetSource(this.state.image.performance_icon.url);

        image.performance_icon.width = image_performance_icon.width;
        image.performance_icon.height = image_performance_icon.height;
        
        this.setState({image});
    }

    render(){
        return(
            <LayoutPrimary
                showTitle={true}
                showBackButton={true}
                navigator={this.props.navigator}
                titleText={'Alur Kerja Kurir'}
                containerChildStyle={{
                    flex:1,
                    alignItems:'center',
                    paddingTop:Dimensions.get('window').height * 0.1,
                }}
            >
            	<Container.TypeA
            		containerStyle={{
            			flex:1,
            		}}
					contentStyle={{
						//flex:1,
						height:Dimensions.get('window').height * 0.85,
					}}
            	>
					<WebView
						startInLoadingState={true}
						renderLoading={() => {
							return (
								<View style={{flex:1, backgroundColor:'red'}}/>
							)
							}
						}
				        source={{uri: 'https://github.com/facebook/react-native'}}
				        style={{flex:1}}
				    />
            	</Container.TypeA>
            </LayoutPrimary>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        errorMessage:state.rootReducer.message.error,
        layoutHeight:state.rootReducer.layout.height,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actionLogin: bindActionCreators(appActions.ActionCreators.login, dispatch),
        actionRoot: bindActionCreators(appActions.ActionCreators.root, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Help);


