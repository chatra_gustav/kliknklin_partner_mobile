import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity, ScrollView, WebView} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import MapView from 'react-native-maps';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Scaling, CustomView} from 'app/data/components';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class CustomWebView extends Component {
	constructor(props){
    	super(props);
    	this.state={
	      	image:{
	        	marker:{
		            width:0,
		            height:0,
		            multiplierWidth:0.4,
		            multiplierHeight:0.4,
		            url:require('app/data/image/setting/marker.png')
	        	}
	      	},
    	}
 	}

  	componentDidMount(){
    	this.getImageSize();
  	}

  	getImageSize(){
	    var image = this.state.image;

	    var marker = ResolveAssetSource(this.state.image.marker.url);

	    image.marker.width = Scaling.moderateScale(marker.width * this.state.image.marker.multiplierWidth);
	    image.marker.height = Scaling.moderateScale(marker.height * this.state.image.marker.multiplierHeight);

	    this.setState({image});
  	}

	render(){
		try{
			return(
				<LayoutPrimary
					showTabBar={false}
					showBackButton={true}
					showTitle={true}
					title={this.props.title}
					navigator={this.props.navigator}
				>
					<WebView
	                    startInLoadingState={true}
	                    renderLoading={() => {
	                            return (
	                                <LoadingContent/>
	                            )
	                        }
	                    }
	                    source={{uri: this.props.uri}}
	                    style={{flex:1}}
	                />
			  	</LayoutPrimary>
			);
		}
		catch(error){
			console.log(error, 'WebView.index');
			return null;
		}
	}
}

class LoadingContent extends Component{

    render(){
        return (
            <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
            	<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME} textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{'Memuat Konten...'}</Text>
            </View>
        )
    }
}

function mapStateToProps(state, ownProps) {
	return {
	};
}

function mapDispatchToProps(dispatch) {
	return {
    	actionNavigator: bindActionCreators(actions.navigator, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomWebView);

const styles = StyleSheet.create({
	
});