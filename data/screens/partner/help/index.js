import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity, Linking, ScrollView, Platform, BackHandler} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import {moderateScale} from 'react-native-size-matters';
import {YouTubeStandaloneAndroid} from 'react-native-youtube'

import {LayoutPrimary} from 'app/data/layout';
import {Text, DateFormat, ShipmentScheduleListCourier, OutletSelector, Label, Scaling, Avatar} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class Help extends Component {
	constructor(props){
    	super(props);
    	this.state={
	      	image:{
	      		youtube:{
		            width:0,
		            height:0,
		            multiplierWidth:0.4,
		            multiplierHeight:0.4,
		            url:require('app/data/image/help/youtube.png')
	        	},
	        	arrow_right_gray:{
		            width:0,
		            height:0,
		            multiplierWidth:0.4,
		            multiplierHeight:0.4,
		            url:require('app/data/image/attendance/arrow_right_gray.png')
	        	}
	      	},
	      	isFullScreen: false
    	}
 	}

  	componentDidMount(){
    	this.getImageSize();
  	}

  	getImageSize(){
	    var image = this.state.image;

	    var arrow_right_gray = ResolveAssetSource(this.state.image.arrow_right_gray.url);

	    image.arrow_right_gray.width = Scaling.moderateScale(arrow_right_gray.width * this.state.image.arrow_right_gray.multiplierWidth);
	    image.arrow_right_gray.height = Scaling.moderateScale(arrow_right_gray.height * this.state.image.arrow_right_gray.multiplierHeight);

	    var youtube = ResolveAssetSource(this.state.image.youtube.url);

	    image.youtube.width = Scaling.moderateScale(youtube.width * this.state.image.youtube.multiplierWidth);
	    image.youtube.height = Scaling.moderateScale(youtube.height * this.state.image.youtube.multiplierHeight);

	    this.setState({image});
  	}

	render(){
		try{
			let {summaryOrder} = this.props;
			return(
				<LayoutPrimary
					showLogo={true}
					showSearchButton={true}
					showNotificationButton={true}
					showTabBar={true}
					navigator={this.props.navigator}
					showTitle={true}
					title={'Bantuan'}
					tabKey={this.props.tabKey}
				>
					<ScrollView showVerticalScrollIndicator={false}>


						<TouchableOpacity onPress={this.playVideo}
						 	style={{ alignSelf: 'stretch', height: Scaling.verticalScale(160), justifyContent: 'center', alignItems: 'center' }}>
							
							<View style={{alignItems: 'center'}}>
								<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME_REGULAR} 
	                			textStyle={{color: CONSTANT.COLOR.BLACK}}>Tutorial Penggunaan Aplikasi</Text>

	                			<View style={{marginTop: Scaling.moderateScale(15)}}>
	                				<Image
										style={{width: this.state.image.youtube.width, height: this.state.image.youtube.height}}
										source={this.state.image.youtube.url}
									/>
	                			</View>
							</View>
						</TouchableOpacity>

				      	<HelpContent
				      		image={this.state.image}
				      		gotoWebView={this.gotoWebView.bind(this)}
				      		webLinks={this.props.webLinks}
				      		{...this.props}
				      	/>
					</ScrollView>
			  	</LayoutPrimary>
			);
		}
		catch(error){
			console.log(error, 'Setting.index');
			return null;
		}
		
	}

	playVideo = () =>{
		let urlID = '';
		let {webLinks} = this.props;
		for (let i in webLinks) {
			if (webLinks[i].id == CONSTANT.WEB_LINKS.YOUTUBE_TUTORIAL.id) {
				urlID = webLinks[i].url;
			}
		}
		YouTubeStandaloneAndroid.playVideo({
		  	apiKey: "AIzaSyDPwS2DVESZvw_UU1dxUIMpg9gA0Cs4oFY",     // Your YouTube Developer API Key
		  	videoId: urlID,     // YouTube video ID
		  	autoplay: true,             // Autoplay the video
		  	startTime: 0,             // Starting point of video (in seconds)
		  	fullscreen: true
		})
	}

	gotoWebView(title, url){
		this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.WEBVIEW, {
			title,
			url,
		})
	}
}

class HelpContent extends Component{
	render(){
		return(
			<View style={styles.outletSettingContainer}>
				<Row
					styles={styles.rowWithUnderline}
					webLinks={CONSTANT.WEB_LINKS.FAQ}
					image={this.props.image}
					_onPress={this._onPress.bind(this)}
				/>

				<Row
					styles={styles.rowWithUnderline}
					webLinks={CONSTANT.WEB_LINKS.AGREEMENT}
					image={this.props.image}
					_onPress={this._onPress.bind(this)}
				/>

				<Row
					styles={styles.rowWithUnderline}
					webLinks={CONSTANT.WEB_LINKS.USER_GUIDE}
					image={this.props.image}
					_onPress={this._onPress.bind(this)}
				/>

				<Row
					styles={styles.rowWithUnderline}
					webLinks={CONSTANT.WEB_LINKS.STANDARD_OPERATING_PROCEDURE_ORDER}
					image={this.props.image}
					_onPress={this._onPress.bind(this)}
				/>

				<Row
					styles={styles.rowWithUnderline}
					webLinks={CONSTANT.WEB_LINKS.STANDARD_OPERATING_PROCEDURE_PROBLEM}
					image={this.props.image}
					_onPress={this._onPress.bind(this)}
				/>

				<Row
					styles={styles.rowWithUnderline}
					webLinks={CONSTANT.WEB_LINKS.STANDARD_OPERATING_PROCEDURE_PICKUP_DELIVERY}
					image={this.props.image}
					_onPress={this._onPress.bind(this)}
				/>

				<Row
					styles={styles.rowWithUnderline}
					webLinks={CONSTANT.WEB_LINKS.FEEDBACK}
					image={this.props.image}
					_onPress={this._onPress.bind(this)}
				/>

				<Row
					styles={styles.rowWithoutUnderline}
					webLinks={CONSTANT.WEB_LINKS.CONTACT_US}
					image={this.props.image}
					_onPress={this._onPress.bind(this)}
				/>

			</View>
		)
	}

	_onPress(webLinks){
		let {id, title, type, route} = webLinks;
		let webLink = this.props.webLinks;
		if (type == 'webview') {
			let url = '';
			for (let i in webLink) {
				if (webLink[i].id == id) {
					url = webLink[i].url;
				}
			}

			this.props.gotoWebView(title, url);
		}
		else if (type == 'push') {
			this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.HelpContent', {helpType:route});
		}
	}
}

class Row extends Component{
	render(){
		
		let {styles, webLinks} = this.props;
		let {title, type, uri, route} = webLinks;
		return(
			<TouchableOpacity style={styles} onPress={() => this.props._onPress(webLinks)}>
				<View style={styles.leftColumn}>
					<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME_REGULAR} 
                	textStyle={{color: CONSTANT.COLOR.BLACK}}>{title}</Text>
				</View>

				<View style={styles.rightColumn}>
					<Image
	                  	style={{width: this.props.image.arrow_right_gray.width, height: this.props.image.arrow_right_gray.height}}
	                  	source={this.props.image.arrow_right_gray.url}
	                />
				</View>
			</TouchableOpacity>
		)
	}
}

function mapStateToProps(state, ownProps) {
	return {
		userData:state.rootReducer.user.data,
		webLinks:state.rootReducer.root.webLinks
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Help);

const styles = StyleSheet.create({
	profileBoxContainer:{
		flex: 1,
		flexDirection: 'row',
		backgroundColor: CONSTANT.COLOR.WHITE,
		paddingHorizontal: Scaling.scale(CONSTANT.STYLE.TAB_BAR.MAIN_DASHBOARD.PADDING_RIGHT),
		paddingVertical: Scaling.verticalScale(15)
	},
	userImg:{
		flex: 0.3,
		alignItems: 'center',
	},
	userInfo:{
		flex: 0.6,
		justifyContent: 'center',
		marginLeft: Scaling.moderateScale(15)
	},
	hitSlop:{
		top: 5,
		left: 5,
		bottom: 5,
		right: 5
	},
	outletSettingContainer:{
		flex: 1,
		backgroundColor: CONSTANT.COLOR.WHITE,
		paddingHorizontal: Scaling.scale(CONSTANT.STYLE.TAB_BAR.MAIN_DASHBOARD.PADDING_RIGHT),
		paddingVertical: Scaling.verticalScale(15),
		marginTop: Scaling.moderateScale(15),
		elevation: 2
	},
	headerOutletSetting:{
		paddingVertical: Scaling.verticalScale(15),
	},
	rowWithUnderline:{
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingVertical: Scaling.moderateScale(15),
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: CONSTANT.COLOR.LIGHTER_GRAY_FONT
	},
	rowWithoutUnderline:{
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingVertical: Scaling.moderateScale(15)
	},
	rightColumnContent:{
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between'
	}
});