import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableHighlight, Linking, Platform, BackHandler, TouchableOpacity, NativeModules, LayoutAnimation} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ResolveAssetSource from 'resolveAssetSource';
import DeviceInfo from 'react-native-device-info';
import SInfo from 'react-native-sensitive-info';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { Switch } from 'react-native-switch';

//custom component js class import
import {TextWrapper, Form, Button, Text, fontMaker, Scaling} from './../../../components';

//layout
import {LayoutPrimary} from './../../../layout';

import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

class SignIn extends Component {
	constructor(props) {
        super(props);
        this.state = {
        	image:{
        		kliknklin_logo:{
        			width:0,
					height:0,
					multiplierWidth:0.4,
					multiplierHeight:0.4,
					url:require('./../../../image/signin/kliknklin_icon.png')
        		},
        		avatar_icon:{
        			width:0,
					height:0,
					multiplierWidth:0.4,
					multiplierHeight:0.4,
					url:require('./../../../image/signin/avatar_icon.png')
        		},
        		owner_icon:{
        			width:0,
					height:0,
					multiplierWidth:0.4,
					multiplierHeight:0.4,
					url:require('./../../../image/signin/owner_icon.png')
        		},
        		outlet_icon:{
        			width:0,
					height:0,
					multiplierWidth:0.4,
					multiplierHeight:0.4,
					url:require('./../../../image/signin/outlet_icon.png')
        		},
        		handshake_icon:{
        			width:0,
					height:0,
					multiplierWidth:0.4,
					multiplierHeight:0.4,
					url:require('./../../../image/signin/handshake_icon.png')
        		},
        		arrow_right_blue:{
		            width:0,
		            height:0,
		            multiplierWidth:0.4,
		            multiplierHeight:0.4,
		            url:require('./../../../image/attendance/arrow_right_blue.png'),
		        }
        	},
        	errorMessage:'',
        	switchValue: false,
        	switchText: 'Perlihatkan Sandi',
        	isReLoginFormVisible: false,
        	visibleBodyContent: 'Login',
        	data:{
        		name:'',
        		email:'',
        		password:'',
				showPassword:false,
        	}
        }; 
        this.navigator = this.props.navigator; 
    }

    componentWillMount(){
    	this._isReloginAvailable();
    }

	componentDidMount(){
		this._getImageSize();
	}

	_getImageSize(){
		var image = this.state.image;

		var image_kliknklin_logo = ResolveAssetSource(this.state.image.kliknklin_logo.url);

		image.kliknklin_logo.width = Scaling.moderateScale(image_kliknklin_logo.width);
		image.kliknklin_logo.height = Scaling.moderateScale(image_kliknklin_logo.height);

		var avatar_icon = ResolveAssetSource(this.state.image.avatar_icon.url);

		image.avatar_icon.width = Scaling.moderateScale(avatar_icon.width);
		image.avatar_icon.height = Scaling.moderateScale(avatar_icon.height);

		var handshake_icon = ResolveAssetSource(this.state.image.handshake_icon.url);

		image.handshake_icon.width = Scaling.moderateScale(handshake_icon.width);
		image.handshake_icon.height = Scaling.moderateScale(handshake_icon.height);

		var outlet_icon = ResolveAssetSource(this.state.image.outlet_icon.url);

		image.outlet_icon.width = Scaling.moderateScale(outlet_icon.width);
		image.outlet_icon.height = Scaling.moderateScale(outlet_icon.height);

		var owner_icon = ResolveAssetSource(this.state.image.owner_icon.url);

		image.owner_icon.width = Scaling.moderateScale(owner_icon.width);
		image.owner_icon.height = Scaling.moderateScale(owner_icon.height);

		var arrow_right_blue = ResolveAssetSource(this.state.image.arrow_right_blue.url);

		image.arrow_right_blue.width = Scaling.moderateScale(arrow_right_blue.width);
		image.arrow_right_blue.height = Scaling.moderateScale(arrow_right_blue.height);

      	this.setState({image});
	}

	_isReloginAvailable(){
		SInfo.getItem('userState', {
			sharedPreferencesName: 'PartnerKliknKlin',
			keychainService: 'PartnerKliknKlin'})
		.then(value => {
			console.log(value);
			    if (value != null) {

			    	var data = this.state.data;

			    	data.userState = JSON.parse(value);

			    	data.name = this._formatName(data.userState.user_name, data.userState.user_email);
			    	data.email = data.userState.user_email;
			    	data.image_url = data.userState.user_image_url;

			    	this.setState({ data, isReLoginFormVisible: true });
			    }
			}
		);
	}

	_formatName(name, email){
		if(name == '' || name == null){
			return email;
		}else{
			let nameArr = name.split(" ");

			if(nameArr.length >= 2){
				return nameArr[0] + " " + nameArr[1]; 
			}else{
				return nameArr[0];
			}
		}
	}

	_validateLogin(){
		var error = '';
		
		error = this._validateEmpty();

		if (error == '') {
			error = this._validateEmail();
		}

		if (error == '') {
			error = this._validateMinimalChar();
		}

		return error;
	}

	_validateEmpty(data = this.state.data){
		var result = '';
		
		if (data.email.trim() == '' || data.password.trim() == '') {
			result = STRING.ERROR.LOGIN.EMPTY_FIELD;
		}

		return result;
	}

	_validateMinimalChar(data = this.state.data){
		var result = '';
		
		if (data.password.length < 6) {
			result = STRING.ERROR.LOGIN.MINIMAL_CHARACTER_PASSWORD;
		}

		return result;
	}

	_validateEmail(data = this.state.data){
		var result = '';

    	var regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;;
    	
    	if(!regexEmail.test(data.email)){
			result = STRING.ERROR.LOGIN.EMAIL_FORMAT;
    	}

    	return result;
    }

    _onChangeEmail(email){
    	var data = this.state.data;
    	data.email = email;
		this.setState({data});
    }

    _onChangePassword(password){
    	var data = this.state.data;
    	data.password = password;
		this.setState({data});
    }

    _onPressForgetPassword(){
		this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.ForgetPassword');
	}

	_onPressSignUp(){
		this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.SignUp', {});
	}

    _onPressShowPassword(){
    	if (!this.state.showPassword) {
    		this.setState({showPassword:true});
		}
    	else {
    		this.setState({showPassword:false});
    	}
    }

    _onPressSubmitButton(){
    	const error = this._validateLogin();

    	if (error == '') {
    		this.props.actionAuthentication.login(this.state.data);
    	}else{
    		this.props.actionRoot.showErrorMessage(error);
    	}
    }

    _onSubmitEditingEmail(){
		this.password_text_field.focus();
    }

    _onSubmitEditingPassword(){
    	this._onPressSubmitButton();
    }

    _changeVisibleContent(activeIndex){
    	if(activeIndex === 0){
    		this._animation();
    		this.setState({ visibleBodyContent: 'Login' })
    	}
    	else{
    		this._animation();
    		this.setState({ visibleBodyContent: 'Pendaftaran kemitraan' })
    	}
    }

    _animation(){
    	const CustomLayoutLinear = {
	        duration: 200,
	        create: {
	            type: LayoutAnimation.Types.linear,
	            property: LayoutAnimation.Properties.opacity
	        },
	        update: {
	            type: LayoutAnimation.Types.linear
	        },
	    };

	    LayoutAnimation.configureNext(CustomLayoutLinear);
    }

    gotoWebView(title, url){
		this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.WEBVIEW, {
			title,
			url,
		})
	}

	_renderContent(){
		return(
			<View style={{flex : 1}}>
				<KeyboardAwareScrollView>
					<View style={styles.container}>		
						<View style={[styles.containerHeaderLogin,{height: Scaling.verticalScale(160)}]}>
							<ResponsiveImage
								source={this.state.image.kliknklin_logo.url}
								initWidth={this.state.image.kliknklin_logo.width * this.state.image.kliknklin_logo.multiplierWidth}
								initHeight={this.state.image.kliknklin_logo.height * this.state.image.kliknklin_logo.multiplierHeight}
							/>
						</View>
						
						<TabBar
							_changeVisibleContent={this._changeVisibleContent.bind(this)}
						/>

						{this.renderBodyContent()}

					</View>
				</KeyboardAwareScrollView>	

				<View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-end', paddingBottom: Scaling.verticalScale(5)}}>
					<Text fontOption={CONSTANT.TEXT_OPTION.CONTENT_SMALLER} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>Partner App Ver {DeviceInfo.getVersion()}</Text>
				</View>
			</View>
		);
	}

	renderFormEmail(){
		if(this.state.isReLoginFormVisible){
			return(
				<View style={{
					alignItems:'center', 
					backgroundColor: CONSTANT.COLOR.WHITE, 
					borderRadius: 10,
					elevation: 2
				}}>
					<View style={{width:Dimensions.get('window').width * 0.87, flexDirection: 'row', paddingHorizontal: Scaling.moderateScale(15), paddingVertical: Scaling.moderateScale(15)}}>
						<View style={{width: '60%', justifyContent:'space-around'}}>
							<Text fontOption={CONSTANT.TEXT_OPTION.CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK, fontSize: Scaling.moderateScale(13)}}>Selamat datang kembali,</Text>

							<Text fontOption={CONSTANT.TEXT_OPTION.BUTTON} textStyle={{color: CONSTANT.COLOR.BLACK, fontSize: Scaling.moderateScale(22), fontWeight: 'bold'}}>{this.state.data.name}</Text>

							<TouchableOpacity 
								hitSlop={styles.hitSlop}
								onPress={() => this.showEmailForm()}>
								<Text fontOption={CONSTANT.TEXT_OPTION.CONTENT_SMALLER} textStyle={{color: CONSTANT.COLOR.BLUE, fontSize: Scaling.moderateScale(13)}}>Bukan Saya</Text>
							</TouchableOpacity>
						</View>

						<View style={{width: '40%', justifyContent: 'center', alignItems: 'flex-end'}}>
							<ResponsiveImage
								source={(this.state.data.image_url == undefined ? this.state.image.avatar_icon.url : {uri:this.state.data.image_url})}
								initWidth={this.state.image.avatar_icon.width * this.state.image.avatar_icon.multiplierWidth}
								initHeight={this.state.image.avatar_icon.height * this.state.image.avatar_icon.multiplierHeight}
								borderRadius={this.state.image.avatar_icon.height * this.state.image.avatar_icon.multiplierHeight / 2}
								resizeMode={'cover'}
							/>
						</View>
					</View>
				</View>
			)
		}else{
			return(
				<Form.InputField.TypeE
	                keyboardType={'email-address'}
	                ref={(ref) => this.email_text_field = ref} 
	                label={STRING.SCREEN.LOGIN.EMAIL_PLACEHOLDER}
	                onChangeText={this._onChangeEmail.bind(this)}
	                onSubmitEditing={this._onSubmitEditingEmail.bind(this)}
	            />
			)
		}
	}

	showEmailForm(){
		const CustomLayoutLinear = {
	        duration: 200,
	        create: {
	            type: LayoutAnimation.Types.linear,
	            property: LayoutAnimation.Properties.opacity
	        },
	        update: {
	            type: LayoutAnimation.Types.linear
	        },
	    };

	    LayoutAnimation.configureNext(CustomLayoutLinear);

		this.setState({isReLoginFormVisible: false})
	}

	changeSwitchValue(){
		if(this.state.switchValue){
			this.state.switchText = 'Perlihatkan Sandi'
			this.state.showPassword = false
		}
		else{
			this.state.switchText = 'Sembunyikan Sandi'
			this.state.showPassword = true
		}

		this.setState({ switchValue: !this.state.switchValue, switchText: this.state.switchText });
	}


	renderBodyContent(){
		if(this.state.visibleBodyContent === 'Login'){
			return(
				this.renderLoginContent()
			)
		}else{
			return(
				this.renderPendaftaranMitraContent()
			)
		}
	}

	renderLoginContent(){
		return(
			<View style={styles.containerBodyLogin}>
				<View style={styles.containerTitleBodyLogin}>
					<TextWrapper type={'smallercontent'} style={{textAlign:'center',color:'red',  width:Dimensions.get('window').width}}>
						{this.state.errorMessage}
					</TextWrapper>
				</View>
				
				<View style={styles.containerFormBodyLogin}>
					{this.renderFormEmail()}

					<View style={{marginTop:Dimensions.get('window').height * 0.035}}>
	                    <Form.InputField.TypeE
	                        keyboardType={'default'}
	                        ref={(ref) => this.password_text_field = ref}
	                        secureTextEntry={!this.state.showPassword}
	                        label={STRING.SCREEN.LOGIN.PASSWORD_PLACEHOLDER}
	                        onChangeText={this._onChangePassword.bind(this)}
							onSubmitEditing={this._onSubmitEditingPassword.bind(this)}
							autoCapitalize={'none'}
	                    />
	                </View>
                </View>

            	<View style={{height: Scaling.verticalScale(60), width: '100%', alignItems: 'center', flexDirection: 'row'}}>
            		<View style={{width: '57%' , alignItems: 'flex-end'}}>
	                	<Switch
						    value={this.state.switchValue}
						    onValueChange={() => this.changeSwitchValue()}
						    disabled={false}
						    activeText={'Sembunyikan Sandi'}
						    inActiveText={'Perlihatkan Sandi'}
						    activeTextStyle={{}}
							inactiveTextStyle={{}}
						    circleSize={Scaling.moderateScale(26)}
						    barHeight={Scaling.moderateScale(24)}
						    circleBorderWidth={0}
						    backgroundActive={CONSTANT.COLOR.GRAY}
						    backgroundInactive={CONSTANT.COLOR.GRAY}
						    circleActiveColor={CONSTANT.COLOR.BLUE}
						    circleInActiveColor={CONSTANT.COLOR.LIGHT_GRAY}
						    changeValueImmediately={true} // if rendering inside circle, change state immediately or wait for animation to complete
						    innerCircleStyle={{ alignItems: "center", justifyContent: "center" }} // style for inner animated circle for what you (may) be rendering inside the circle
						    outerCircleStyle={{}} // style for outer animated circle
						    renderActiveText={false}
						    renderInActiveText={false}
						    switchLeftPx={2} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
						    switchRightPx={2} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
						    switchWidthMultiplier={2} // multipled by the `circleSize` prop to calculate total width of the Switch
						    switchBorderRadius={10}
						/>
					</View>

					<View style={{width: '43%' ,paddingRight: Dimensions.get('window').width * 0.065 + Scaling.scale(5), alignItems: 'flex-end'}}>
						<Text textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT, fontFamily: CONSTANT.TEXT_FAMILY.MAIN, fontSize: Scaling.moderateScale(13)}}>{this.state.switchText}</Text>
					</View>
                </View>

                <View style={{alignItems: 'center'}}>
                	<Button.TypeD
                 		buttonText={'MASUK'}
                      	buttonColor={CONSTANT.COLOR.BLUE}
                      	buttonTextStyle={[{color: CONSTANT.COLOR.WHITE, fontSize: Scaling.moderateScale(13), fontFamily: CONSTANT.TEXT_FAMILY.MAIN_SEMI_BOLD}]}
                      	buttonStyle={styles.buttonStyle}
                      	onPress={this._onPressSubmitButton.bind(this)}
                	/>
                </View>

                <TouchableOpacity 
                	style={{marginTop: Scaling.verticalScale(25), marginLeft: Dimensions.get('window').width * 0.065 + Scaling.scale(5)}} 
                	hitSlop={styles.hitSlop}
                	onPress={() => this._onPressForgetPassword()}>
					<Text textStyle={{color: CONSTANT.COLOR.BLUE, fontFamily: CONSTANT.TEXT_FAMILY.MAIN, fontSize: Scaling.moderateScale(13), fontWeight: CONSTANT.TEXT_WEIGHT.REGULAR.weight}}>Saya Lupa Password</Text>
				</TouchableOpacity>
                
			</View>
		)
	}

	renderPendaftaranMitraContent(){
		return(
			<View style={styles.containerBodyPendaftaran}>
                <MenuButton
                	smallTitle={'Saya ingin mendaftar sebagai'}
                	bigTitle={'Mitra Laundrypreneur'}
                	image={this.state.image.handshake_icon}
                	arrow_icon={this.state.image.arrow_right_blue}
                	webLinks={CONSTANT.WEB_LINKS.REGISTRATION_PRENEUR}
                	_onPress={this._onPress.bind(this)}
                />

                <MenuButton
                	smallTitle={'Saya pemilik outlet Laundry'}
                	bigTitle={'Daftarkan Outlet'}
                	image={this.state.image.outlet_icon}
                	arrow_icon={this.state.image.arrow_right_blue}
                	webLinks={CONSTANT.WEB_LINKS.REGISTRATION_PARTNER}
                	_onPress={this._onPress.bind(this)}
                />

                <MenuButton
                	smallTitle={'Saya belum memiliki outlet'}
                	bigTitle={'Buka Usaha Laundry'}
                	image={this.state.image.owner_icon}
                	arrow_icon={this.state.image.arrow_right_blue}
                	webLinks={CONSTANT.WEB_LINKS.REGISTRATION_INVESTOR}
                	_onPress={this._onPress.bind(this)}
                />
			</View>
		)
	}

	_onPress(webLinks){
		console.log('webLinks:',webLinks);
		let {id, title, type, route} = webLinks;
		let webLink = this.props.webLinks;
		if (type == 'webview') {
			
			let url = '';
			for (let i in webLink) {
				if (webLink[i].id == id) {
					url = webLink[i].url;
				}
			}

			this.gotoWebView(title, url);
		}
		else if (type == 'push') {
			console.log('push:');
			this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.HelpContent', {helpType:route});
		}
	}

	render(){
		return (
			<LayoutPrimary
				{...this.props}
				containerStyle={{
					backgroundColor:'white'
				}}
				showNavBar={false}
				showTabBar={false}
			>
				{this._renderContent()}
			</LayoutPrimary>
		)
	}
}

class TabBar extends Component{
	constructor(props){
		super(props);
		this.state = {
			tabBarItems:['Login', 'Pendaftaran kemitraan'],
			activeIndex: 0
		}
	}

	render(){
		return (
      		<View style={{width: '100%', alignItems: 'center'}}>
        		<View
          			style={[{
            			height: Scaling.verticalScale(30),
			            width: '100%',
			            justifyContent:'center',
			            flexDirection: 'row',
			            backgroundColor: CONSTANT.COLOR.WHITE
		          	}]}
        		>
	          		{this.state.tabBarItems.map((item, key) => {
	              		
	              		return (
	                  		<TouchableOpacity
	                  			key={key}
						        onPress={() => this.onPressTabBar(key)}
						        style={{
						          	flex: 1,
						          	alignItems: 'center'
						        }}
      						>
      							<View style={{flex: 0.8, justifyContent:'center', alignItems:'center'}}>
      								<Text fontOption={CONSTANT.TEXT_OPTION.BUTTON} textStyle={{color: CONSTANT.COLOR.BLACK, fontSize: Scaling.moderateScale(14)}}>{item}</Text>
      							</View>
      							{this.renderActiveTabbarIndicator(key)}
      						</TouchableOpacity>
	              		);
	            	})}
        		</View>
      		</View>
    	);
	}

	renderActiveTabbarIndicator(key){
		if(this.state.activeIndex === key){
			return(
				<View style={styles.activeTabBar}>
      			</View>
			)
		}else{
			return(
				<View style={styles.nonActiveTabBar}>
      			</View>
			)
		}
	}

	onPressTabBar(key){
		if(key !== this.state.activeIndex){
			this.setState({activeIndex: key});
			this.props._changeVisibleContent(key)
		}
	}
}

class MenuButton extends Component{
	constructor(props){
		super(props);
	}

	render(){

		let {webLinks} = this.props;
		let {title, type, uri, route} = webLinks;

		return(
			<TouchableOpacity style={styles.buttonContainer} onPress={() => this.props._onPress(webLinks)}>

				<View style={styles.leftIcon}>
					<ResponsiveImage
						source={this.props.image.url}
						initWidth={this.props.image.width * this.props.image.multiplierWidth}
						initHeight={this.props.image.height * this.props.image.multiplierHeight}
					/>
				</View>

				<View style={styles.middleContainer}>
					<Text textStyle={{color: CONSTANT.COLOR.BLACK, fontFamily: CONSTANT.TEXT_FAMILY.MAIN, fontSize: Scaling.moderateScale(13)}}>{this.props.smallTitle}</Text>
					<Text textStyle={{color: CONSTANT.COLOR.BLACK, fontFamily: CONSTANT.TEXT_FAMILY.MAIN, fontSize: Scaling.moderateScale(20), fontWeight: 'bold'}}>{this.props.bigTitle}</Text>
				</View>

				<View style={styles.rightIcon}>
					<ResponsiveImage
						source={this.props.arrow_icon.url}
						initWidth={this.props.arrow_icon.width * this.props.arrow_icon.multiplierWidth}
						initHeight={this.props.arrow_icon.height * this.props.arrow_icon.multiplierHeight}
					/>
				</View>

			</TouchableOpacity>
		)
	}
}

var styles = StyleSheet.create({
    container: {
        flex:1
    },

    containerHeaderLogin:{
   		alignItems:'center',
   		justifyContent: 'center',
   		backgroundColor: CONSTANT.COLOR.WHITE
    },

    containerBodyLogin:{
    },

	containerTitleBodyLogin:{
		alignItems:'center',
		height:Dimensions.get('window').height * 0.075,
		marginTop:Scaling.moderateScale(7.5),
	},

	containerFormBodyLogin:{
		alignItems:'center'
    },

    containerFooterLogin:{
		justifyContent:'center',
		alignItems:'center',
    },
    activeTabBar:{
    	flex: 0.2,
    	width: '90%',
    	backgroundColor: CONSTANT.COLOR.BLUE,
    	borderRadius: 10
    },
    nonActiveTabBar:{
    	flex: 0.2,
    	width: '90%',
    	backgroundColor: 'transparent',
    	borderRadius: 10
    },
    buttonStyle:{
	    borderRadius:Scaling.moderateScale(15),
	    width:Dimensions.get('window').width * 0.87,
	    height:CONSTANT.STYLE.BUTTON.THICKER_EXTRA_MEDIUM.HEIGHT,
	    borderRadius: 10,
	},
	hitSlop: {
		top: 5,
		left: 5,
		bottom: 5,
		right: 5
	},
	containerBodyPendaftaran:{
		flex: 1,
		alignItems: 'center',
		paddingTop: Scaling.verticalScale(10),
		paddingBottom: Scaling.verticalScale(5)
	},
	buttonContainer:{
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		borderRadius:Scaling.moderateScale(15),
	    width:Dimensions.get('window').width * 0.87,
	    borderRadius: 10,
	    paddingHorizontal: Scaling.scale(10),
	    paddingVertical: Scaling.verticalScale(25),
	    backgroundColor: CONSTANT.COLOR.WHITE,
	    marginTop: Scaling.verticalScale(20),
	    elevation: 2
	},
	leftIcon:{
		flex: 0.22
	},
	middleContainer:{
		flex: 0.7
	},
	rightIcon:{
		flex: 0.08,
		alignItems: 'flex-end'
	}
});

function mapStateToProps(state, ownProps) {
	return {
		layoutHeight:state.rootReducer.layout.height,
		webLinks:state.rootReducer.root.webLinks
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionAuthentication: bindActionCreators(actions.authentication, dispatch),
		actionRoot: bindActionCreators(actions.root, dispatch),
		actionNavigator: bindActionCreators(actions.navigator, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);

