import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import { GooglePlacesAutocomplete } from '../react-native-google-places-autocomplete';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Scaling, Button} from 'app/data/components';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class AutocompleteField extends Component {
	constructor(props){
    	super(props);
    	this.state={
    		image:{
    			clear_icon:{
          			width:0,
          			height:0,
          			multiplierWidth:0.35,
          			multiplierHeight:0.35,
          			url:require('app/data/image/deposit/clear_icon.png')
        		}
    		},
    		region: {},
    		location:{
    			text: null
    		}
    	}
 	}

  	componentDidMount(){
  		this.getImageSize();
  	}

  	getImageSize(){
  		var image = this.state.image;

  		var clear_icon = ResolveAssetSource(this.state.image.clear_icon.url);

	    image.clear_icon.width = clear_icon.width * this.state.image.clear_icon.multiplierWidth;
	    image.clear_icon.height = clear_icon.height * this.state.image.clear_icon.multiplierHeight;

	    this.setState({image});
  	}

	render(){
		try{
			if(this.props.locationText == null){
				return null
			}else{
				return(
					<GooglePlacesAutocomplete
				      	placeholder='Cari lokasi'
				      	minLength={3} // minimum length of text to search
				      	autoFocus={false}
				      	returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
				      	keyboardAppearance={'light'} // Can be left out for default keyboardAppearance https://facebook.github.io/react-native/docs/textinput.html#keyboardappearance
				      	listViewDisplayed='false'    // true/false/undefined
				      	fetchDetails={true}
				      	text={this.props.locationText}
				      	clearText={this.clearText.bind(this)}
				      	renderDescription={row => row.description} // custom description render
				      	onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
				        	let { width, height } = Dimensions.get('window');
							const aspect_ration = width / height;
				        	const latitudeDelta = 0.01;
							const longitudeDelta = latitudeDelta * aspect_ration;

				        	let region = {
								latitude: details.geometry.location.lat,
					  			longitude: details.geometry.location.lng,
					  			latitudeDelta: latitudeDelta,
					  			longitudeDelta: longitudeDelta
							}

							// var location = this.state.location;
							// location.text = details.formatted_address;
							// this.state.location = location;

							this.props.actionUser.outletLocation(region);

				      	}}
				      	
				      	getDefaultValue={() => ''}

				      	query={{
				        	// available options: https://developers.google.com/places/web-service/autocomplete
				        	key: 'AIzaSyBPbiZ_iSwOV0bl4VLOI7Adj73ukTu746Q',
				        	language: 'id', // language of the results
				        	components: 'country:id',
				      		fields:'geometry,formatted_address'
				      	}}
				      	styles={{
				      		container:{
				      		},
				        	textInputContainer: {
				          		width: '100%',
				          		backgroundColor: CONSTANT.COLOR.WHITE,
	    						height: CONSTANT.STYLE.BUTTON.SMALL.HEIGHT * 2.5,
	    						borderWidth: StyleSheet.hairlineWidth,
	    						borderColor: CONSTANT.COLOR.LIGHT_GRAY,
	    						borderRadius: 10,
	    						marginTop: Scaling.verticalScale(15),
	    						elevation: 2
				        	},
				        	description: {
				          		fontWeight: 'normal'
				        	},
				        	predefinedPlacesDescription: {
				          		color: '#1faadb'
				        	},
							textInput: {
								fontFamily: CONSTANT.TEXT_FAMILY.MAIN,
								fontSize: Scaling.moderateScale(13),
								height: CONSTANT.STYLE.BUTTON.SMALL.HEIGHT * 2.5 - 10
							}

				      	}}

				      	currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
				      	currentLocationLabel="Current location"
				      	nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
				      	GooglePlacesDetailsQuery={{
				        	// available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
				        	fields: 'geometry',
				      	}}

				      	filterReverseGeocodingByTypes={['administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
				      	image={this.state.image}
				    />
				);
			}
		}
		catch(error){
			console.log(error, 'setting.changeLocation.autocomplete.index');
			return null;
		}
	}

	clearText(){
		this.props.clearText()
	}

	changeLocation = () => {
		this.props.actionUser.outletLocation(this.state.region);
	}
}

function mapStateToProps(state, ownProps) {
	return {
		outletData:state.rootReducer.user.outlet
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionUser: bindActionCreators(actions.user, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(AutocompleteField);

const styles = StyleSheet.create({
});