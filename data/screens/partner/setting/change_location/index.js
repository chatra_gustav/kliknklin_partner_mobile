import Geocoder from 'react-native-geocoder'
import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity, ScrollView} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import MapView from 'react-native-maps';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Scaling, Button} from 'app/data/components';
import * as CONSTANT from 'app/data/constant';
import AutocompleteField from './autocomplete_field';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class ChangeLocation extends Component {
	constructor(props){
    	super(props);
    	this.state={
	      	image:{
	        	marker:{
		            width:0,
		            height:0,
		            multiplierWidth:0.4,
		            multiplierHeight:0.4,
		            url:require('app/data/image/setting/marker.png')
	        	}
	      	},
	      	region:{
      			latitude: -7.7956,
      			longitude: 110.3695,
      			latitudeDelta: 999999,
      			longitudeDelta: 999999,
    		},
    		locationText: null
    	}
 	}

  	componentDidMount(){
  		this.getInitLocation();
  		this.getImageSize();
  		this.props.actionUser.setOutletLocation();
  	}

  	getImageSize(){
	    var image = this.state.image;

	    var marker = ResolveAssetSource(this.state.image.marker.url);

	    image.marker.width = Scaling.moderateScale(marker.width * this.state.image.marker.multiplierWidth);
	    image.marker.height = Scaling.moderateScale(marker.height * this.state.image.marker.multiplierHeight);

	    this.setState({image});
  	}

  	getInitLocation(){
		let {outletList, indexActiveOutlet} = this.props.outletData;
		let locationText = outletList[indexActiveOutlet].addressName;
		this.setState({locationText})
	}

  	async changeLocation(coord){

		let region = {
			latitude: coord.latitude,
			longitude: coord.longitude,
			latitudeDelta: coord.latitudeDelta,
			longitudeDelta: coord.longitudeDelta
		}


		this.getLocationByCoord(region);
		this.props.actionUser.outletLocation(region);

	}

	async getLocationByCoord(region){
		Geocoder.fallbackToGoogle("AIzaSyBPbiZ_iSwOV0bl4VLOI7Adj73ukTu746Q");
		let res = await Geocoder.geocodePosition({lat: region.latitude, lng: region.longitude});
		
		let response = res[0];
		this.setState({locationText: response.formattedAddress})	
	}

	saveLocation(){
		let {latitude, longitude} = this.props.outletLocation;
		
		params = {
			latitude: latitude,
			longitude: longitude
      	}
      	
      	this.props.actionRoot.changeOutletLocation(params);
		this.props.actionNavigator.pop(this.props.navigator);
	}

	clearText(){
		this.setState({locationText: ''})
	}

	render(){
		try{
			
			return(
				<LayoutPrimary
					showTabBar={false}
					showBackButton={true}
					showTitle={true}
					title={'Ubah Lokasi'}
					navigator={this.props.navigator}
				>
					<View style={{flex: 1}}>
						<MapView
							initialRegion={{
				      			latitude: -7.7956,
				      			longitude: 110.3695,
				      			latitudeDelta: 999999,
				      			longitudeDelta: 999999,
				    		}}
							region={this.props.outletLocation}
							style={styles.map}
							onRegionChangeComplete={(region) => this.changeLocation(region)}>
						</MapView>

						<View style={[styles.markerPosition, {marginBottom: this.state.image.marker.height}]}>
							<TouchableOpacity onPress={() => this.saveLocation()}>
								<Image
									style={{width: this.state.image.marker.width, height: this.state.image.marker.height}}
									source={this.state.image.marker.url}
								/>
							</TouchableOpacity>
						</View>

						<View style={styles.autocompleteFieldStyle}>
							<AutocompleteField
								locationText={this.state.locationText}
								clearText={this.clearText.bind(this)}
							/>
						</View>
					</View>
			  	</LayoutPrimary>
			);
		}
		catch(error){
			console.log(error, 'ChangeLocation.index');
			return null;
		}
	}
}

function mapStateToProps(state, ownProps) {
	return {
		userData:state.rootReducer.user.data,
		outletData:state.rootReducer.user.outlet,
		outletLocation:state.rootReducer.user.outletLocation
	};
}

function mapDispatchToProps(dispatch) {
	return {
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
    	actionUser: bindActionCreators(actions.user, dispatch),
    	actionRoot: bindActionCreators(actions.root, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangeLocation);

const styles = StyleSheet.create({
	map: {
		...StyleSheet.absoluteFillObject,
	},
	button:{
		position: 'absolute',
		left: Dimensions.get('window').width * 0.05,
		bottom: 5
	},
	markerPosition:{
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	autocompleteFieldStyle:{
		position: 'absolute',
		left: 10, 
		right: 10,
	}
});