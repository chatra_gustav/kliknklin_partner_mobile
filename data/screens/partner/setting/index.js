import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity, Linking, ScrollView, Platform, BackHandler} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from 'app/data/layout';
import {Text, DateFormat, ShipmentScheduleListCourier, OutletSelector, Label, Scaling, Avatar} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class Setting extends Component {
	constructor(props){
    	super(props);
    	this.state={
	      	image:{
	        	edit_icon:{
		            width:0,
		            height:0,
		            multiplierWidth:0.4,
		            multiplierHeight:0.4,
		            url:require('app/data/image/attendance/edit_icon.png')
	        	},
	        	arrow_right_blue:{
		            width:0,
		            height:0,
		            multiplierWidth:0.4,
		            multiplierHeight:0.4,
		            url:require('app/data/image/attendance/arrow_right_blue.png')
	        	}
	      	}
    	}
 	}

  	componentDidMount(){
    	this.getImageSize();
  	}

  	getImageSize(){
	    var image = this.state.image;

	    var edit_icon = ResolveAssetSource(this.state.image.edit_icon.url);

	    image.edit_icon.width = Scaling.moderateScale(edit_icon.width * this.state.image.edit_icon.multiplierWidth);
	    image.edit_icon.height = Scaling.moderateScale(edit_icon.height * this.state.image.edit_icon.multiplierHeight);

	    var arrow_right_blue = ResolveAssetSource(this.state.image.arrow_right_blue.url);

	    image.arrow_right_blue.width = Scaling.moderateScale(arrow_right_blue.width * this.state.image.arrow_right_blue.multiplierWidth);
	    image.arrow_right_blue.height = Scaling.moderateScale(arrow_right_blue.height * this.state.image.arrow_right_blue.multiplierHeight);

	    this.setState({image});
  	}

	render(){
		try{
			let {summaryOrder} = this.props;
			return(
				<LayoutPrimary
					showOutletSelector={true}
					showLogo={true}
					showSearchButton={true}
					showNotificationButton={true}
					showTabBar={true}
					navigator={this.props.navigator}
					tabKey={this.props.tabKey}
					//titleText={STRING.SCREEN.DASHBOARD_ADMIN_OUTLET.TITLE}
				>
					<ScrollView showVerticalScrollIndicator={false}>
						<ProfileBox 
				      		image={this.state.image}
				      		userData={this.props.userData}
				      		changePassword={this.changePassword.bind(this)}
				      	/>

				      	<OutletSetting
				      		{...this.props}
				      		image={this.state.image}
				      		actionAuthentication={this.props.actionAuthentication}
				      		actionNavigator={this.props.actionNavigator}
				      		actionRoot={this.props.actionRoot}
				      		navigator={this.props.navigator}
				      		outletData={this.props.outletData}
				      		jadwalBuka={this.state.jadwalBuka}
				      		jadwalTutup={this.state.jadwalTutup}
				      		mdTimes={this.props.mdTimes}
				      		userRole={this.props.userRole}
				      	/>
					</ScrollView>
			  	</LayoutPrimary>
			);
		}
		catch(error){
			console.log(error, 'Setting.index');
			return null;
		}
	}

	changePassword(){
		this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.CHANGE_PASSWORD)
	}
}

class ProfileBox extends Component{
	render(){

		let {userData} = this.props;
		let {name, email, role, mobilePhone} = userData;
		let user_role = role.name;

		return(
			<TouchableOpacity style={styles.profileBoxContainer}
				onPress={this.changePassword}>

				<View style={styles.userImg}>
					<Avatar 
	                  	size={Scaling.scale(CONSTANT.STYLE.ATTENDANCE.AVATAR_SIZE * 1.5)}
	                  	img={this._getUserImage()}
	                />
	            </View>

                <View style={styles.userInfo}>
	                <View style={{marginBottom: Scaling.moderateScale(5)}}>
	                  	<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME} 
	                  	textStyle={{color: CONSTANT.COLOR.BLACK}}>{name}</Text>
	                </View>
	                <View style={{marginBottom: Scaling.moderateScale(5)}}>
	                  	<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_ROLE} 
	                  	textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>{user_role}</Text>
	                </View>
	                <View style={{marginBottom: Scaling.moderateScale(5)}}>
	                	<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME_REGULAR} 
	                	textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{email}</Text>
	                </View>
	                <View style={{marginBottom: Scaling.moderateScale(5)}}>
	                	<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME_REGULAR} 
	                	textStyle={{color: CONSTANT.COLOR.LIGHT_BLACK}}>{mobilePhone}</Text>
	                </View>
                </View>

                <TouchableOpacity onPress={this.changePassword}
                	style={{flex: 0.1, alignItems: 'flex-end'}} hitSlop={styles.hitSlop}>
	                <Image
	                  style={{width: this.props.image.edit_icon.width, height: this.props.image.edit_icon.height}}
	                  source={this.props.image.edit_icon.url}
	                />
	            </TouchableOpacity>
			</TouchableOpacity>
		)
	}

	_getUserImage(){
        let {userData} = this.props;
        let {userImageURL} = userData;

        if(userImageURL == "")
        	return null

        return userImageURL
    }

    changePassword = () => {
    	this.props.changePassword();
    }
}

class OutletSetting extends Component{

	constructor(props){
    	super(props);
    	this.state={
	      	image:{
	        	edit_icon:{
		            width:0,
		            height:0,
		            multiplierWidth:0.4,
		            multiplierHeight:0.4,
		            url:require('app/data/image/attendance/edit_icon.png')
	        	},
	        	arrow_right_blue:{
		            width:0,
		            height:0,
		            multiplierWidth:0.4,
		            multiplierHeight:0.4,
		            url:require('app/data/image/attendance/arrow_right_blue.png')
	        	}
	      	},

			jadwalBuka:'',
	      	jadwalTutup: ''
    	}
 	}

 	goToWebView(title, url, destination){
 		let {indexActiveOutlet} = this.props.outletData;

 		if (indexActiveOutlet != null) {
			this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.WEBVIEW, {
				title,
				url,
				needParameter:true,
				destination,
				method:'POST',
			})
 		}
		else{
			this.props.actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
                title:'Perhatian', 
                message: 'Anda tidak memiliki outlet yang aktif',
                buttonPrimaryText: 'Ya', 
                onPressButtonPrimary: () => {
                    this.props.actionNavigator.dismissLightBox();
                },
            });
		}
 	}

	render(){

		let {outletList, indexActiveOutlet} = this.props.outletData;
		console.log('outlety: ',outletList[indexActiveOutlet]);
		let outlet_location = indexActiveOutlet != null ? outletList[indexActiveOutlet].area3.info : '-';
		let open_time = indexActiveOutlet != null ? this.formatTime(outletList[indexActiveOutlet].open_time.time) : '-';
		let close_time = indexActiveOutlet != null ? this.formatTime(outletList[indexActiveOutlet].close_time.time) :'-';

		return(
			<View style={styles.outletSettingContainer}>
				<View style={styles.headerOutletSetting}>
					<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME} 
	                textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}>Pengaturan Data Outlet</Text>
				</View>

				{this.renderJamBuka(open_time, close_time)}

				<View style={styles.rowWithUnderline}>
					<View style={styles.leftColumn}>
						<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME_REGULAR} 
	                	textStyle={{color: CONSTANT.COLOR.BLACK}}>Lokasi Outlet</Text>
					</View>

					<View style={styles.rightColumn}>
						<View style={styles.rightColumnContent}>

							<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME_REGULAR} 
		                	textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>{outlet_location}</Text>

		                	<Image
			                  	style={{width: this.props.image.arrow_right_blue.width, height: this.props.image.arrow_right_blue.height}}
			                  	source={this.props.image.arrow_right_blue.url}
			                />
		                </View>
					</View>
				</View>

				<TouchableOpacity style={styles.rowWithUnderline} onPress={this.goToWebView.bind(this,'Data Outlet', CONSTANT.APP_URL.MOBILE_WEB, 'outlet')}>
					<View style={styles.leftColumn}>
						<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME_REGULAR} 
	                	textStyle={{color: CONSTANT.COLOR.BLACK}}>Data Outlet</Text>
					</View>

					<View style={styles.rightColumn}>
						<Image
		                  	style={{width: this.props.image.arrow_right_blue.width, height: this.props.image.arrow_right_blue.height}}
		                  	source={this.props.image.arrow_right_blue.url}
		                />
					</View>
				</TouchableOpacity>

				<TouchableOpacity style={styles.rowWithUnderline} onPress={this.goToWebView.bind(this,'Data Karyawan', CONSTANT.APP_URL.MOBILE_WEB, 'employee')}>
					<View style={styles.leftColumn}>
						<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME_REGULAR} 
	                	textStyle={{color: CONSTANT.COLOR.BLACK}}>Data karyawan</Text>
					</View>

					<View style={styles.rightColumn}>
						<Image
		                  	style={{width: this.props.image.arrow_right_blue.width, height: this.props.image.arrow_right_blue.height}}
		                  	source={this.props.image.arrow_right_blue.url}
		                />
					</View>
				</TouchableOpacity>

				<TouchableOpacity style={styles.rowWithUnderline} onPress={this.goToWebView.bind(this,'Data Item', CONSTANT.APP_URL.MOBILE_WEB, 'item')}>
					<View style={styles.leftColumn}>
						<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME_REGULAR} 
	                	textStyle={{color: CONSTANT.COLOR.BLACK}}>Data Item yang Dijual</Text>
					</View>

					<View style={styles.rightColumn}>
						<Image
		                  	style={{width: this.props.image.arrow_right_blue.width, height: this.props.image.arrow_right_blue.height}}
		                  	source={this.props.image.arrow_right_blue.url}
		                />
					</View>
				</TouchableOpacity>

				<TouchableOpacity style={styles.rowWithUnderline} onPress={this.goToWebView.bind(this,'Pengaturan Biaya Pengiriman', CONSTANT.APP_URL.MOBILE_WEB, 'shipment_cost')}>
					<View style={styles.leftColumn}>
						<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME_REGULAR} 
	                	textStyle={{color: CONSTANT.COLOR.BLACK}}>Pengaturan Biaya Pengiriman</Text>
					</View>

					<View style={styles.rightColumn}>
						<Image
		                  	style={{width: this.props.image.arrow_right_blue.width, height: this.props.image.arrow_right_blue.height}}
		                  	source={this.props.image.arrow_right_blue.url}
		                />
					</View>
				</TouchableOpacity>

				<TouchableOpacity style={styles.rowWithoutUnderline} onPress={() => this.props.actionAuthentication.logout()}>
					<View style={styles.leftColumn}>
						<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME_REGULAR} 
	                	textStyle={{color: CONSTANT.COLOR.RED}}>Log out</Text>
					</View>
				</TouchableOpacity>

			</View>
		)
	}

	renderJamBuka(open_time, close_time){
		console.log('haha:',this.props.userRole);
		console.log('hahai:',CONSTANT.USER_ROLE.COURIER);
		let time = open_time == '-' ? '-' : open_time + '-' + close_time;
		if (this.props.userRole == CONSTANT.USER_ROLE.COURIER) {

        	return(
				<View style={styles.rowWithUnderline}>
					<View style={styles.leftColumn}>
						<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME_REGULAR} 
	                	textStyle={{color: CONSTANT.COLOR.BLACK}}>Jam Buka Tutup</Text>
					</View>

					<View style={styles.rightColumn}>
						<View style={styles.rightColumnContent}>

							<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME_REGULAR} 
		                	textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>{time}</Text>

		                	<Image
			                  	style={{width: this.props.image.arrow_right_blue.width, height: this.props.image.arrow_right_blue.height}}
			                  	source={this.props.image.arrow_right_blue.url}
			                />
		                </View>
					</View>
				</View>
			)
      	}else{
      		return(
      			<TouchableOpacity style={styles.rowWithUnderline} onPress={this.setjadwalBuka}>
					<View style={styles.leftColumn}>
						<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME_REGULAR} 
	                	textStyle={{color: CONSTANT.COLOR.BLACK}}>Jam Buka Tutup</Text>
					</View>

					<View style={styles.rightColumn}>
						<View style={styles.rightColumnContent}>

							<Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME_REGULAR} 
		                	textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>{time}</Text>

		                	<Image
			                  	style={{width: this.props.image.arrow_right_blue.width, height: this.props.image.arrow_right_blue.height}}
			                  	source={this.props.image.arrow_right_blue.url}
			                />
		                </View>
					</View>
				</TouchableOpacity>
      		)
      	}
	}

	setjadwalBuka = () => {
		let {indexActiveOutlet} = this.props.outletData;
		if (indexActiveOutlet != null) {
			let data = [];
			for( x in this.props.mdTimes){
				data.push(this.props.mdTimes[x].time);
			}		

			this.props.actionNavigator.showLightBox(CONSTANT.ROUTE_TYPE.WHEEL_PICKER,{
	        	data:data,
	        	title: 'Pilih Jadwal Buka',
	      		onPressButtonPrimary: (index) => {

	      			this.props.actionNavigator.dismissLightBox();
	      			this.state.jadwalBuka = this.formatTime(this.props.mdTimes[index].time);
	      			
	      			let open_operasional_time_id = this.props.mdTimes[index].id;
	      			setTimeout(() => {this.setjadwalTutup(open_operasional_time_id)}, 500);
	      		},

	      		onPressButtonSecondary: () =>{

	      			this.props.actionNavigator.dismissLightBox();
	      		},
	    	})
		}
	}

	setjadwalTutup(open_operasional_time_id){

		let data = [];
		for( x in this.props.mdTimes){
			data.push(this.props.mdTimes[x].time);
		}		

		this.props.actionNavigator.showLightBox(CONSTANT.ROUTE_TYPE.WHEEL_PICKER,{
        	data:data,
        	title: 'Pilih Jadwal Tutup',
      		onPressButtonPrimary: (index) => {

      			setTimeout(() => {
      				this.props.actionNavigator.dismissLightBox();
      				this.setState({jadwalTutup: this.formatTime(this.props.mdTimes[index].time)}, () => {

      					let close_operasional_time_id = this.props.mdTimes[index].id;
      					this.changeOutletOperationTime(open_operasional_time_id, close_operasional_time_id);
      				})
      			}, 500);
				
      			
      		},

      		onPressButtonSecondary: () =>{

      			this.props.actionNavigator.dismissLightBox();
      		},
    	})
	}

	changeOutletOperationTime(open_operasional_time_id, close_operasional_time_id){
		params = {
			open_operasional_time_id: open_operasional_time_id,
			close_operasional_time_id: close_operasional_time_id
      	}
      	
      	this.props.actionRoot.changeOutletOperationTime(params);
	}

	formatTime(time){
		let arr = time.split(':');
		return arr[0]+':'+arr[1];
	}

	changeLocation = () => {
		this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.CHANGE_LOCATION )
	}

}

function mapStateToProps(state, ownProps) {
	return {
		userData:state.rootReducer.user.data,
		outletData:state.rootReducer.user.outlet,
		mdTimes: state.rootReducer.root.mdTimes,
		userRole:state.rootReducer.user.role,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
    	actionAuthentication: bindActionCreators(actions.authentication, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Setting);

const styles = StyleSheet.create({
	profileBoxContainer:{
		flex: 1,
		flexDirection: 'row',
		backgroundColor: CONSTANT.COLOR.WHITE,
		paddingHorizontal: Scaling.scale(CONSTANT.STYLE.TAB_BAR.MAIN_DASHBOARD.PADDING_RIGHT),
		paddingVertical: Scaling.verticalScale(15)
	},
	userImg:{
		flex: 0.3,
		alignItems: 'center',
	},
	userInfo:{
		flex: 0.6,
		justifyContent: 'center',
		marginLeft: Scaling.moderateScale(15)
	},
	hitSlop:{
		top: 5,
		left: 5,
		bottom: 5,
		right: 5
	},
	outletSettingContainer:{
		flex: 1,
		backgroundColor: CONSTANT.COLOR.WHITE,
		paddingHorizontal: Scaling.scale(CONSTANT.STYLE.TAB_BAR.MAIN_DASHBOARD.PADDING_RIGHT),
		paddingVertical: Scaling.verticalScale(15),
		marginTop: Scaling.moderateScale(15),
		elevation: 2
	},
	headerOutletSetting:{
		paddingVertical: Scaling.verticalScale(15),
	},
	rowWithUnderline:{
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingVertical: Scaling.moderateScale(15),
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: CONSTANT.COLOR.LIGHTER_GRAY_FONT
	},
	rowWithoutUnderline:{
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingVertical: Scaling.moderateScale(15)
	},
	rightColumnContent:{
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between'
	}
});