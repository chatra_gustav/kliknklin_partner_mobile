import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity, Linking, ScrollView, Platform, BackHandler} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from 'app/data/layout';
import {Text, DateFormat, Scaling, Form, Button} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class ChangePassword extends Component {
	constructor(props){
    	super(props);
    	this.state={
	      	image:{
	        	show_password:{
		            width:0,
		            height:0,
		            multiplierWidth:0.1,
		            multiplierHeight:0.1,
		            url:require('app/data/image/signin/show_password.png')
	        	},
	        	hide_password:{
		            width:0,
		            height:0,
		            multiplierWidth:0.1,
		            multiplierHeight:0.1,
		            url:require('app/data/image/signin/hide_password.png')
	        	}
	      	},
	      	showCurrentPassword: false,
	      	showNewPassword: false,
	      	showConfirmPassword: false,
	      	currentPassword: "",
	      	password: "",
	      	password_confirmation: ""
    	}
 	}

  	componentDidMount(){
    	this.getImageSize();
  	}

  	getImageSize(){
	    var image = this.state.image;

	    var show_password = ResolveAssetSource(this.state.image.show_password.url);

	    image.show_password.width = Scaling.moderateScale(show_password.width * this.state.image.show_password.multiplierWidth);
	    image.show_password.height = Scaling.moderateScale(show_password.height * this.state.image.show_password.multiplierHeight);

	    var hide_password = ResolveAssetSource(this.state.image.hide_password.url);

	    image.hide_password.width = Scaling.moderateScale(hide_password.width * this.state.image.hide_password.multiplierWidth);
	    image.hide_password.height = Scaling.moderateScale(hide_password.height * this.state.image.hide_password.multiplierHeight);

	    this.setState({image});
  	}

	render(){
		try{
			let {summaryOrder} = this.props;
			return(
				<LayoutPrimary
					showTitle={true}
					title={'Ubah Password'}
					showBackButton={true}
					showTabBar={false}
					navigator={this.props.navigator}
					//titleText={STRING.SCREEN.DASHBOARD_ADMIN_OUTLET.TITLE}
				>
					<View 
						style={{
				          	flex:1,
				          	height:Dimensions.get('window').height * 0.16,
				          	elevation:2,
				          	marginBottom: Scaling.moderateScale(20),
				          	marginHorizontal: Scaling.moderateScale(2),
				          	paddingHorizontal: Dimensions.get('window').width * 0.05
				        }}>
						
						<View style={styles.customer_number}>

          					<View style={styles.textinput}>
            					<Form.InputField.TypeE
            						ref={(ref) => this.currentPassword = ref}
            						keyboardType={'default'}
              						value={this.state.phone_number}
					              	secureTextEntry={!this.state.showCurrentPassword}
					              	onChangeText={this._onChangeCurrentPassword.bind(this)}
					              	placeholder={'Password saat ini'}
					              	placeholderTextColor={CONSTANT.COLOR.LIGHTER_GRAY_FONT}
					              	onSubmitEditing={() => { this.newPassword.focus(); }}
					              	autoCapitalize={'none'}
					              	containerStyle={[
					                	{
					                  		width: Dimensions.get('window').width * 0.63,
					                  		height: Scaling.verticalScale(40),
					                  		justifyContent: 'center',
					                  		paddingTop: Scaling.verticalScale(7)
					                	}
					              	]}
					              	inputContainerStyle={{
					                	justifyContent: 'center'
					              	}}
					              	inputTextStyle={{fontSize: Scaling.moderateScale(15), color: CONSTANT.COLOR.BLACK}}
            					/>
          					</View>

							<TouchableOpacity style={styles.rightIcon} hitSlop={styles.hitSlop} onPress={() => {
								this.setState({showCurrentPassword: !this.state.showCurrentPassword})
							}}>
								{this.rightIcon1()}
							</TouchableOpacity>

        				</View>

        				<View style={styles.customer_number}>

          					<View style={styles.textinput}>
            					<Form.InputField.TypeE
            						ref={(ref) => this.newPassword = ref}
            						keyboardType={'default'}
              						value={this.state.phone_number}
					              	secureTextEntry={!this.state.showNewPassword}
					              	onChangeText={this._onChangePassword.bind(this)}
					              	placeholder={'Password baru'}
					              	placeholderTextColor={CONSTANT.COLOR.LIGHTER_GRAY_FONT}
					              	autoCapitalize={'none'}
					              	onSubmitEditing={() => { this.confirmPassword.focus(); }}
					              	containerStyle={[
					                	{
					                  		width: Dimensions.get('window').width * 0.63,
					                  		height: Scaling.verticalScale(40),
					                  		justifyContent: 'center',
					                  		paddingTop: Scaling.verticalScale(7)
					                	}
					              	]}
					              	inputContainerStyle={{
					                	justifyContent: 'center'
					              	}}
					              	inputTextStyle={{fontSize: Scaling.moderateScale(15), color: CONSTANT.COLOR.BLACK}}
            					/>
          					</View>

							<TouchableOpacity style={styles.rightIcon} hitSlop={styles.hitSlop} onPress={() => {
								this.setState({showNewPassword: !this.state.showNewPassword})
							}}>
								{this.rightIcon2()}
							</TouchableOpacity>

        				</View>

        				<View style={styles.customer_number}>

          					<View style={styles.textinput}>
            					<Form.InputField.TypeE
            						ref={(ref) => this.confirmPassword = ref}
            						keyboardType={'default'}
              						value={this.state.phone_number}
					              	secureTextEntry={!this.state.showConfirmPassword}
					              	onChangeText={this._onChangePasswordConfirmation.bind(this)}
					              	placeholder={'Konfirmasi password baru'}
					              	placeholderTextColor={CONSTANT.COLOR.LIGHTER_GRAY_FONT}
					              	autoCapitalize={'none'}
					              	onSubmitEditing={() => { this._onPressSubmitButton }}
					              	containerStyle={[
					                	{
					                  		width: Dimensions.get('window').width * 0.63,
					                  		height: Scaling.verticalScale(40),
					                  		justifyContent: 'center',
					                  		paddingTop: Scaling.verticalScale(7)
					                	}
					              	]}
					              	inputContainerStyle={{
					                	justifyContent: 'center'
					              	}}
					              	inputTextStyle={{fontSize: Scaling.moderateScale(15), color: CONSTANT.COLOR.BLACK}}
            					/>
          					</View>

							<TouchableOpacity style={styles.rightIcon} hitSlop={styles.hitSlop} onPress={() => {
								this.setState({showConfirmPassword: !this.state.showConfirmPassword})
							}}>
								{this.rightIcon3()}
							</TouchableOpacity>

        				</View>

	                    <View style={styles.formButton}>
				            <Button.TypeD
		                 		buttonText={'SIMPAN'}
		                      	buttonColor={CONSTANT.COLOR.BLUE}
		                      	buttonTextStyle={[{color: CONSTANT.COLOR.WHITE, fontSize: Scaling.moderateScale(13), fontFamily: CONSTANT.TEXT_FAMILY.MAIN, fontWeight: CONSTANT.TEXT_WEIGHT.MEDIUM.weight}]}
		                      	buttonStyle={styles.buttonStyle}
		                      	onPress={this._onPressSubmitButton.bind(this)}
		                	/>
		                </View>
					</View>
			  	</LayoutPrimary>
			);
		}
		catch(error){
			console.log(error, 'Setting.index');
			return null;
		}
	}

	rightIcon1(){
		if(this.state.showCurrentPassword){
			return(
				<Image
					style={{width: this.state.image.hide_password.width, height: this.state.image.hide_password.height}}
					source={this.state.image.hide_password.url}
				/>
			)
		}else{
			return(
				<Image
					style={{width: this.state.image.show_password.width, height: this.state.image.show_password.height}}
					source={this.state.image.show_password.url}
				/>
			)
		}
	}

	rightIcon2(){
		if(this.state.showNewPassword){
			return(
				<Image
					style={{width: this.state.image.hide_password.width, height: this.state.image.hide_password.height}}
					source={this.state.image.hide_password.url}
				/>
			)
		}else{
			return(
				<Image
					style={{width: this.state.image.show_password.width, height: this.state.image.show_password.height}}
					source={this.state.image.show_password.url}
				/>
			)
		}
	}

	rightIcon3(){
		if(this.state.showConfirmPassword){
			return(
				<Image
					style={{width: this.state.image.hide_password.width, height: this.state.image.hide_password.height}}
					source={this.state.image.hide_password.url}
				/>
			)
		}else{
			return(
				<Image
					style={{width: this.state.image.show_password.width, height: this.state.image.show_password.height}}
					source={this.state.image.show_password.url}
				/>
			)
		}
	}

	_onChangeCurrentPassword(currentPassword){
		this.setState({ currentPassword })
	}

	_onChangePassword(password){
		this.setState({ password })
	}

	_onChangePasswordConfirmation(password_confirmation){
		this.setState({ password_confirmation })
	}

	_onPressSubmitButton = () => {
		if(this._validPassword()){

			let params = {
				currentPassword: this.state.currentPassword,
				password: this.state.password,
				password_confirmation: this.state.password_confirmation
			}

			this.props.actionAuth.changePassword(params, this.props.navigator);
		}
	}

	_validPassword(){
		let {currentPassword, password, password_confirmation} = this.state;

		if(currentPassword == ""){
			this.props.actionRoot.showMessage("Mohon isi kolom \"Password saat ini\"", '');
			return false;
		}else if(password == ""){
			this.props.actionRoot.showMessage("Mohon isi kolom \"Password baru\"", '');
			return false;
		}else if(password_confirmation == ""){
			this.props.actionRoot.showMessage("Mohon isi kolom \"Konfirmasi password baru\"", '');
			return false;
		}else if(password !== password_confirmation){
			this.props.actionRoot.showMessage("Mohon samakan isian \"Password baru\" dengan \"Konfirmasi password baru\"", '');
			return false;
		}

		return true;
	}

}

function mapStateToProps(state, ownProps) {
	return {
		userData:state.rootReducer.user.data,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionAuth: bindActionCreators(actions.authentication, dispatch),
		actionRoot: bindActionCreators(actions.root, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);

const styles = StyleSheet.create({
	buttonStyle:{
	    borderRadius: Scaling.moderateScale(15),
	    width: '100%',
	    height: CONSTANT.STYLE.BUTTON.THICKER_EXTRA_MEDIUM.HEIGHT,
	    borderRadius: 10,
	},
	formButton:{
		marginTop: Scaling.verticalScale(45)
	},
	customer_number: {
    	marginTop: Scaling.moderateScale(10),
	    flexDirection: 'row',
	    alignItems: 'center',
	    borderRadius: Scaling.moderateScale(5),
	    paddingHorizontal: Scaling.moderateScale(10),
	    paddingVertical: Scaling.moderateScale(7),
	    borderWidth: StyleSheet.hairlineWidth,
	    borderColor: CONSTANT.COLOR.LIGHTER_GRAY_FONT,
	    width: Dimensions.get('window').width * 0.9,
	    backgroundColor: CONSTANT.COLOR.WHITE
  	},
  	textinput:{
    	paddingLeft: Scaling.moderateScale(10)
  	},
  	rightIcon:{
    	position: 'absolute',
    	right: Scaling.moderateScale(10),
    	alignItems: 'center'
  	}
});