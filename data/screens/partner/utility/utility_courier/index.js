import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableHighlight,
        ScrollView,
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import TimerMixin from 'react-timer-mixin';

import {LayoutPrimary} from './../../../../layout';
import {TextWrapper, Button, Scaling, Text} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

import BoxUtility from '../box_utility';

class UtilityCourier extends Component {
    constructor(props){
        super(props);
        this.state = {
        };
    }

    render(){
        try{
            return(
                <View>
                    <View
                        style={{
                            width:'100%',
                            paddingTop:Scaling.verticalScale(CONSTANT.STYLE.CONTAINER.BOX_DASHBOARD.PADDING_TOP),
                            paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                        }}
                    >
                        <View
                            style={{
                                marginTop: Scaling.moderateScale(10),
                                flexDirection:'row',
                            }}
                        >
                            <View
                                style={{
                                    width:'100%'
                                }}
                            >
                                <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>Featured</Text>
                            </View>

                        </View>
                        
                        <View
                            style={{
                                flexWrap: 'wrap',
                                flexDirection:'row',
                            }}
                        >
                            <BoxUtility title={STRING.SCREEN.UTILITY.MITRA_BELAJAR} pushedScreen={'webview'}  links={CONSTANT.WEB_LINKS.PARTNER_KLIKNKLIN} {...this.props}/>
                            
                            <BoxUtility title={STRING.SCREEN.UTILITY.ABSENSI_KARYAWAN} {...this.props}/>

                        </View>
                    </View>

                    <View
                        style={{
                            width:'100%',
                            paddingTop:Scaling.verticalScale(CONSTANT.STYLE.CONTAINER.BOX_DASHBOARD.PADDING_TOP),
                            paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                        }}
                    >
                        <View
                            style={{
                                marginTop: Scaling.moderateScale(10),
                                flexDirection:'row',
                            }}
                        >
                            <View
                                style={{
                                    width:'100%'
                                }}
                            >
                                <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>Outlet</Text>
                            </View>

                        </View>
                        
                        <View
                            style={{
                                flexWrap: 'wrap',
                                flexDirection:'row',
                            }}
                        >
                            <BoxUtility title={STRING.SCREEN.UTILITY.RATING_REVIEW} pushedScreen={'not_release_yet'} {...this.props}/>

                        </View>
                    </View>
                </View>
            );
        }
        catch(error){
            console.log(error, 'utility.owner');
            return null;
        }
        
    }
}


function mapStateToProps(state, ownProps) {
    return {
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actionNavigator: bindActionCreators(actions.navigator, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(UtilityCourier);

