import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity, Linking, ScrollView, Platform, BackHandler} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from 'app/data/actions';

const multiplierWidth = 0.37;
const multiplierHeight = 0.37;

class BoxUtility extends Component {
	static defaultProps = {
	 	title:'',
	 	disabled:false,
	}

	constructor(props){
        super(props);
        this.state = {
            image:{
                absen_icon:{
                	width:0,
                    height:0,
                    multiplierWidth:multiplierWidth,
                    multiplierHeight:multiplierHeight,
                    url:require('app/data/image/utility/absen_icon.png'),
                },
                analytics_icon:{
                	width:0,
                    height:0,
                    multiplierWidth:multiplierWidth,
                    multiplierHeight:multiplierHeight,
                    url:require('app/data/image/utility/analytics_icon.png'),
                },
                belajar_icon:{
                	width:0,
                    height:0,
                    multiplierWidth:multiplierWidth,
                    multiplierHeight:multiplierHeight,
                    url:require('app/data/image/utility/belajar_icon.png'),
                },
                bpjs_icon:{
                	width:0,
                    height:0,
                    multiplierWidth:multiplierWidth,
                    multiplierHeight:multiplierHeight,
                    url:require('app/data/image/utility/bpjs_icon.png'),
                },
                deposit_icon:{
                	width:0,
                    height:0,
                    multiplierWidth:multiplierWidth,
                    multiplierHeight:multiplierHeight,
                    url:require('app/data/image/utility/deposit_icon.png'),
                },
                explore_icon:{
                	width:0,
                    height:0,
                    multiplierWidth:multiplierWidth,
                    multiplierHeight:multiplierHeight,
                    url:require('app/data/image/utility/explore_icon.png'),
                },
                IOT_icon:{
                	width:0,
                    height:0,
                    multiplierWidth:multiplierWidth,
                    multiplierHeight:multiplierHeight,
                    url:require('app/data/image/utility/IOT_icon.png'),
                },
                membership_icon:{
                	width:0,
                    height:0,
                    multiplierWidth:multiplierWidth,
                    multiplierHeight:multiplierHeight,
                    url:require('app/data/image/utility/membership_icon.png'),
                },
                paket_data_icon:{
                	width:0,
                    height:0,
                    multiplierWidth:multiplierWidth,
                    multiplierHeight:multiplierHeight,
                    url:require('app/data/image/utility/paket_data_icon.png'),
                },
                pengeluaran_icon:{
                	width:0,
                    height:0,
                    multiplierWidth:multiplierWidth,
                    multiplierHeight:multiplierHeight,
                    url:require('app/data/image/utility/pengeluaran_icon.png'),
                },
                pln_icon:{
                	width:0,
                    height:0,
                    multiplierWidth:multiplierWidth,
                    multiplierHeight:multiplierHeight,
                    url:require('app/data/image/utility/pln_icon.png'),
                },
                promo_outlet_icon:{
                	width:0,
                    height:0,
                    multiplierWidth:multiplierWidth,
                    multiplierHeight:multiplierHeight,
                    url:require('app/data/image/utility/promo_outlet_icon.png'),
                },
                pulsa_icon:{
                	width:0,
                    height:0,
                    multiplierWidth:multiplierWidth,
                    multiplierHeight:multiplierHeight,
                    url:require('app/data/image/utility/pulsa_icon.png'),
                },
                rating_icon:{
                	width:0,
                    height:0,
                    multiplierWidth:multiplierWidth,
                    multiplierHeight:multiplierHeight,
                    url:require('app/data/image/utility/rating_icon.png'),
                },
                setoran_icon:{
                	width:0,
                    height:0,
                    multiplierWidth:multiplierWidth,
                    multiplierHeight:multiplierHeight,
                    url:require('app/data/image/utility/setoran_icon.png'),
                },
                mitra_belanja_icon:{
                	width:0,
                    height:0,
                    multiplierWidth:multiplierWidth,
                    multiplierHeight:multiplierHeight,
                    url:require('app/data/image/utility/mitra_belanja_icon.png')
                }
            },
        };
    }

    componentDidMount(){
    	this.getImageSize();
    }

    getImageSize(){
	    var image = this.state.image;

	    var absen_icon = ResolveAssetSource(this.state.image.absen_icon.url);

	    image.absen_icon.width = absen_icon.width * this.state.image.absen_icon.multiplierWidth;
	    image.absen_icon.height = absen_icon.height * this.state.image.absen_icon.multiplierHeight;

	    var analytics_icon = ResolveAssetSource(this.state.image.analytics_icon.url);

	    image.analytics_icon.width = analytics_icon.width * this.state.image.analytics_icon.multiplierWidth;
	    image.analytics_icon.height = analytics_icon.height * this.state.image.analytics_icon.multiplierHeight;

	    var belajar_icon = ResolveAssetSource(this.state.image.belajar_icon.url);

	    image.belajar_icon.width = belajar_icon.width * this.state.image.belajar_icon.multiplierWidth;
	    image.belajar_icon.height = belajar_icon.height * this.state.image.belajar_icon.multiplierHeight;

	    var bpjs_icon = ResolveAssetSource(this.state.image.bpjs_icon.url);

	    image.bpjs_icon.width = bpjs_icon.width * this.state.image.bpjs_icon.multiplierWidth;
	    image.bpjs_icon.height = bpjs_icon.height * this.state.image.bpjs_icon.multiplierHeight;

	    var deposit_icon = ResolveAssetSource(this.state.image.deposit_icon.url);

	    image.deposit_icon.width = deposit_icon.width * this.state.image.deposit_icon.multiplierWidth;
	    image.deposit_icon.height = deposit_icon.height * this.state.image.deposit_icon.multiplierHeight;

	    var explore_icon = ResolveAssetSource(this.state.image.explore_icon.url);

	    image.explore_icon.width = explore_icon.width * this.state.image.explore_icon.multiplierWidth;
	    image.explore_icon.height = explore_icon.height * this.state.image.explore_icon.multiplierHeight;
	    
	    var IOT_icon = ResolveAssetSource(this.state.image.IOT_icon.url);

	    image.IOT_icon.width = IOT_icon.width * this.state.image.IOT_icon.multiplierWidth;
	    image.IOT_icon.height = IOT_icon.height * this.state.image.IOT_icon.multiplierHeight;

	    var membership_icon = ResolveAssetSource(this.state.image.membership_icon.url);

	    image.membership_icon.width = membership_icon.width * this.state.image.membership_icon.multiplierWidth;
	    image.membership_icon.height = membership_icon.height * this.state.image.membership_icon.multiplierHeight;

	    var paket_data_icon = ResolveAssetSource(this.state.image.paket_data_icon.url);

	    image.paket_data_icon.width = paket_data_icon.width * this.state.image.paket_data_icon.multiplierWidth;
	    image.paket_data_icon.height = paket_data_icon.height * this.state.image.paket_data_icon.multiplierHeight;

	    var pengeluaran_icon = ResolveAssetSource(this.state.image.pengeluaran_icon.url);

	    image.pengeluaran_icon.width = pengeluaran_icon.width * this.state.image.pengeluaran_icon.multiplierWidth;
	    image.pengeluaran_icon.height = pengeluaran_icon.height * this.state.image.pengeluaran_icon.multiplierHeight;

	    var pln_icon = ResolveAssetSource(this.state.image.pln_icon.url);

	    image.pln_icon.width = pln_icon.width * this.state.image.pln_icon.multiplierWidth;
	    image.pln_icon.height = pln_icon.height * this.state.image.pln_icon.multiplierHeight;

	    var promo_outlet_icon = ResolveAssetSource(this.state.image.promo_outlet_icon.url);

	    image.promo_outlet_icon.width = promo_outlet_icon.width * this.state.image.promo_outlet_icon.multiplierWidth;
	    image.promo_outlet_icon.height = promo_outlet_icon.height * this.state.image.promo_outlet_icon.multiplierHeight;

	    var pulsa_icon = ResolveAssetSource(this.state.image.pulsa_icon.url);

	    image.pulsa_icon.width = pulsa_icon.width * this.state.image.pulsa_icon.multiplierWidth;
	    image.pulsa_icon.height = pulsa_icon.height * this.state.image.pulsa_icon.multiplierHeight;

	    var rating_icon = ResolveAssetSource(this.state.image.rating_icon.url);

	    image.rating_icon.width = rating_icon.width * this.state.image.rating_icon.multiplierWidth;
	    image.rating_icon.height = rating_icon.height * this.state.image.rating_icon.multiplierHeight;

	    var setoran_icon = ResolveAssetSource(this.state.image.setoran_icon.url);

	    image.setoran_icon.width = setoran_icon.width * this.state.image.setoran_icon.multiplierWidth;
	    image.setoran_icon.height = setoran_icon.height * this.state.image.setoran_icon.multiplierHeight;

	    var mitra_belanja_icon = ResolveAssetSource(this.state.image.setoran_icon.url);
		
		image.mitra_belanja_icon.width = mitra_belanja_icon.width * this.state.image.mitra_belanja_icon.multiplierWidth;
	    image.mitra_belanja_icon.height = mitra_belanja_icon.height * this.state.image.mitra_belanja_icon.multiplierHeight;	    
	    
	    this.setState({image});
	  }

	render(){
		let {title, disabled} = this.props;
		
		try{
			return(
				<View style={styles.container}>
					<TouchableOpacity
						style={{
							width:Scaling.scale(88.8),
							height:Scaling.scale(97.68),
							alignItems:'center',
							justifyContent:'center',
						}}
						onPress={this._onPressBox.bind(this)}
					>
						<Button.Icon
							buttonStyle={{
								flexDirection:'row',
							}}
							onPress={this._onPressBox.bind(this)}
						>
							{this.renderIcon(title)}

						</Button.Icon>

						<View
							style={{
								width:'100%',
								height:Scaling.verticalScale(22.2),
								justifyContent:'center',
								alignItems:'center',
								marginTop: Scaling.moderateScale(5)
							}}
						>
							<Text 
								fontOption={CONSTANT.TEXT_OPTION.ICON_BOX_DASHBOARD}
								textStyle={{color:CONSTANT.COLOR.DARK_GRAY, textAlign: 'center'}}
							>
							{title}</Text>
						</View>
					</TouchableOpacity>
				</View>
			);
		}
		catch(error){
			console.log(error, 'utility.box_utility');
			return null;
		}
		
	}

	renderIcon(title){

		if(title == STRING.SCREEN.UTILITY.MITRA_BELAJAR){
			return(
				<Image
                  style={{width: this.state.image.belajar_icon.width, height: this.state.image.belajar_icon.height}}
                  source={this.state.image.belajar_icon.url}
                />
			)
		}else if( title == STRING.SCREEN.UTILITY.ABSENSI_KARYAWAN){
			return(
				<Image
                  style={{width: this.state.image.absen_icon.width, height: this.state.image.absen_icon.height}}
                  source={this.state.image.absen_icon.url}
                />
			)
		}else if( title == STRING.SCREEN.UTILITY.PENGATURAN_DEPOSIT){
			return(
				<Image
                  style={{width: this.state.image.deposit_icon.width, height: this.state.image.deposit_icon.height}}
                  source={this.state.image.deposit_icon.url}
                />
			)
		}else if( title == STRING.SCREEN.UTILITY.MEMBERSHIP_OUTLET){
			return(
				<Image
                  style={{width: this.state.image.membership_icon.width, height: this.state.image.membership_icon.height}}
                  source={this.state.image.membership_icon.url}
                />
			)
		}else if( title == STRING.SCREEN.UTILITY.PENGELUARAN_OUTLET){
			return(
				<Image
                  style={{width: this.state.image.pengeluaran_icon.width, height: this.state.image.pengeluaran_icon.height}}
                  source={this.state.image.pengeluaran_icon.url}
                />
			)
		}else if( title == STRING.SCREEN.UTILITY.SETORAN_OUTLET){
			return(
				<Image
                  style={{width: this.state.image.setoran_icon.width, height: this.state.image.setoran_icon.height}}
                  source={this.state.image.setoran_icon.url}
                />
			)
		}else if( title == STRING.SCREEN.UTILITY.PROMOSI_OUTLET){
			return(
				<Image
                  style={{width: this.state.image.promo_outlet_icon.width, height: this.state.image.promo_outlet_icon.height}}
                  source={this.state.image.promo_outlet_icon.url}
                />
			)
		}else if( title == STRING.SCREEN.UTILITY.EXPLORE_DATA_OUTLET){
			return(
				<Image
                  style={{width: this.state.image.explore_icon.width, height: this.state.image.explore_icon.height}}
                  source={this.state.image.explore_icon.url}
                />
			)
		}else if( title == STRING.SCREEN.UTILITY.RINGKASAN_PENJUALAN){
			return(
				<Image
                  style={{width: this.state.image.analytics_icon.width, height: this.state.image.analytics_icon.height}}
                  source={this.state.image.analytics_icon.url}
                />
			)
		}else if( title == STRING.SCREEN.UTILITY.RATING_REVIEW){
			return(
				<Image
                  style={{width: this.state.image.rating_icon.width, height: this.state.image.rating_icon.height}}
                  source={this.state.image.rating_icon.url}
                />
			)
		}else if( title == STRING.SCREEN.UTILITY.MESIN_IOT){
			return(
				<Image
                  style={{width: this.state.image.IOT_icon.width, height: this.state.image.IOT_icon.height}}
                  source={this.state.image.IOT_icon.url}
                />
			)
		}else if( title == STRING.SCREEN.UTILITY.MITRA_BELANJA){
			return(
				<Image
                  style={{width: this.state.image.mitra_belanja_icon.width, height: this.state.image.mitra_belanja_icon.height}}
                  source={this.state.image.mitra_belanja_icon.url}
                />
			)
		}
	}

	_onPressBox(){
		console.log(this.props.pushedScreen)
		if(this.props.pushedScreen == 'not_release_yet'){
			this.props.actionRoot.showMessage("Coming Soon", '');
		}
		else if (this.props.pushedScreen == 'no_active_outlet') {
			this.props.actionRoot.showMessage("Anda tidak memiliki outlet", '');
		}
		else if(this.props.pushedScreen == 'webview'){

			let {id, title, type, route} = this.props.links;
			let webLink = this.props.webLinks;
			if (type == 'webview') {
				
				let url = '';
				for (let i in webLink) {
					if (webLink[i].id == id) {
						url = webLink[i].url;
					}
				}
				this.gotoWebView(title, url);
			}
			else if (type == 'push') {
				console.log('push:');
				this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.HelpContent', {helpType:route});
			}
		}
		else{
			this.props.actionNavigator.push(this.props.navigator, this.props.pushedScreen, this.props.screenProps ? this.props.screenProps : {})
		}
	}

	gotoWebView(title, url){
		this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.WEBVIEW, {
			title,
			url,
		})
	}
}

function mapStateToProps(state, ownProps) {
	return {
		layoutHeight:state.rootReducer.layout.height,
		summaryOrder:state.rootReducer.order.summary,
		webLinks:state.rootReducer.root.webLinks
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(BoxUtility);

const styles = StyleSheet.create({
	container:{
		justifyContent: 'center',
		alignItems: 'center',
		width: Dimensions.get('window').width / 4  -10,
		height: Dimensions.get('window').width / 4 -10,
		marginTop: Scaling.moderateScale(10)
	}
})
