import React, { Component } from 'react';
import { 	
        View,
        Dimensions,
        TouchableHighlight,
        ScrollView,
  		} from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import TimerMixin from 'react-timer-mixin';

import {LayoutPrimary} from './../../../layout';
import {TextWrapper, Button} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

//local component
import UtilityAdminOutlet from './utility_admin_outlet';
import UtilityCourier from './utility_courier';
import UtilityOwner from './utility_owner';

class Utility extends Component {
    constructor(props){
        super(props);
        this.state = {
        };
    }

    renderContent(){
        if (this.props.userRole == CONSTANT.USER_ROLE.ADMIN_OUTLET) {
            return(
                <UtilityAdminOutlet
                    {...this.props}
                />
            );
        }
        else if(this.props.userRole == CONSTANT.USER_ROLE.OWNER){
            return(
                <UtilityOwner
                    {...this.props}
                />
            )
        }
        else if (this.props.userRole == 'courier') {
            return(
                <UtilityCourier/>
            );
        }
    }

    render(){
        return(
            <LayoutPrimary
                showOutletSelector={true}
                showTabBar={true}
                showLogo={true}
                showNotificationButton={true}
                showSearchButton={true}
                tabKey={this.props.tabKey}
                navigator={this.props.navigator}
            >
                <ScrollView
                    contentContainerStyle={{
                        paddingBottom:Dimensions.get('window').height * 0.25,
                        width:'100%',
                        alignItems:'center',
                    }}
                >
                    {this.renderContent()}
                </ScrollView>
            </LayoutPrimary>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        layoutHeight:state.rootReducer.layout.height,
        userRole:state.rootReducer.user.role,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actionNavigator: bindActionCreators(actions.navigator, dispatch),
        actionRoot: bindActionCreators(actions.root, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Utility);


