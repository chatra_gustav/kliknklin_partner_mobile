import React, { PureComponent } from 'react';
import {    
        Platform,
        View,
        Dimensions,
        WebView,
        Image,
        StyleSheet,
        TouchableOpacity
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import TimerMixin from 'react-timer-mixin';

import {LayoutPrimary} from 'app/data/layout';
import {TextWrapper, Button, Container,Text, Scaling, DateFormat} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";


class NotificationContent extends PureComponent {
    constructor(props){
        super(props);
        this.state = {
            image:{
                order_icon:{
                    width:0,
                    height:0,
                    multiplierWidth:0.3,
                    multiplierHeight:0.3,
                    url:require('app/data/image/notification/order_icon.png'),
                },
                announcement_icon:{
                    width:0,
                    height:0,
                    multiplierWidth:0.3,
                    multiplierHeight:0.3,
                    url:require('app/data/image/notification/announcement_icon.png'),
                },
                message_icon:{
                    width:0,
                    height:0,
                    multiplierWidth:0.3,
                    multiplierHeight:0.3,
                    url:require('app/data/image/notification/message_icon.png'),
                }
            },
        };
    }

    componentDidMount(){
        this.getImageSize();
    }

    getImageSize(){
        var image = this.state.image;

        let order_icon = ResolveAssetSource(this.state.image.order_icon.url);

        image.order_icon.width = order_icon.width * this.state.image.order_icon.multiplierWidth;
        image.order_icon.height = order_icon.height * this.state.image.order_icon.multiplierHeight;

        let announcement_icon = ResolveAssetSource(this.state.image.announcement_icon.url);

        image.announcement_icon.width = announcement_icon.width * this.state.image.announcement_icon.multiplierWidth;
        image.announcement_icon.height = announcement_icon.height * this.state.image.announcement_icon.multiplierHeight;

        let message_icon = ResolveAssetSource(this.state.image.message_icon.url);

        image.message_icon.width = message_icon.width * this.state.image.message_icon.multiplierWidth;
        image.message_icon.height = message_icon.height * this.state.image.message_icon.multiplierHeight;     
        
        this.setState({image});
    }

    render(){

        let icon_url = this.getIconUrl();
        let backgroundColor = this.getBackgroundColor();
        let date = this.setUpDate();
        let time = this.setUpTime();

        return(
            <TouchableOpacity style={[styles.container, {backgroundColor: backgroundColor}]}
            onPress={this.onPressContent}>
                <View style={styles.title}>
                    <Text fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME} textStyle={{color: CONSTANT.COLOR.BLACK}}>{this.props.title}</Text>
                </View>

                <View style={styles.message}>
                    <Text 
                        fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME_REGULAR} 
                        textStyle={{fontWeight: CONSTANT.TEXT_WEIGHT.THIN.weight, color: CONSTANT.COLOR.LIGHT_BLACK}}>
                            {this.props.message}
                    </Text>
                </View>

                <View style={styles.dateMessage}>
                    <Image
                      style={{width: Scaling.scale(20), height: Scaling.scale(20)}}
                      source={icon_url}
                    />

                    <View style={styles.date_time}>
                        <Text 
                            fontOption={CONSTANT.TEXT_OPTION.ATTENDANCE_USER_NAME_REGULAR} 
                            textStyle={{fontWeight: CONSTANT.TEXT_WEIGHT.THIN.weight, fontSize: Scaling.moderateScale(12), color: CONSTANT.COLOR.LIGHT_BLACK}}>
                                {DateFormat(this.setUpDate(date), 'notificationFormat')} {time}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>     
        );
    }

    getIconUrl(){

        if(this.props.notification_type_id == CONSTANT.NOTIFICATION_TYPE.ORDER){
            return this.state.image.order_icon.url;
        }else if(this.props.notification_type_id == CONSTANT.NOTIFICATION_TYPE.MARKETING || 
        this.props.notification_type_id == CONSTANT.NOTIFICATION_TYPE.ANNOUNCEMENT){
            return this.state.image.announcement_icon.url;
        }else if(this.props.notification_type_id == CONSTANT.NOTIFICATION_TYPE.MESSAGE){
            return this.state.image.message_icon.url;
        }
    }

    getBackgroundColor(){
        if(this.props.isRead){
            return CONSTANT.COLOR.WHITE;
        }else{
            return CONSTANT.COLOR.WHITE;
        }
    }

    setUpDate(){
        let date = this.props.notificationDate;

        let arr = date.split(" ");
        return arr[0];
    }

    setUpTime(){
        let date = this.props.notificationDate;

        let arr = date.split(" ");
        return arr[1];   
    }

    onPressContent = () => {
        let notification_id = this.props.notification_id;
        this.props.actionRoot.updateNotification(notification_id)
    }
}

function mapStateToProps(state, ownProps) {
    return {
        layoutHeight:state.rootReducer.layout.height,
        userRole:state.rootReducer.user.role,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actionRoot: bindActionCreators(actions.root, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationContent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Scaling.moderateScale(20),
    paddingVertical: Scaling.scale(CONSTANT.STYLE.CONTAINER.BOX_DASHBOARD.PADDING_TOP),
    paddingLeft:Scaling.scale(CONSTANT.STYLE.TAB_BAR.MAIN_DASHBOARD.PADDING_LEFT),
    paddingRight:Scaling.scale(CONSTANT.STYLE.TAB_BAR.MAIN_DASHBOARD.PADDING_RIGHT),
    elevation: 2
  },
  title:{
  },
  message:{
    marginVertical: Scaling.moderateScale(10)
  },
  date_time:{
    marginLeft: Scaling.moderateScale(10)
  },
  dateMessage:{
    flexDirection: 'row',
    alignItems: 'center'
  }
})

