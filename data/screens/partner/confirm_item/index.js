import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList,
} from 'react-native';

import {LayoutPrimary} from './../../../layout';
import * as CONSTANT from './../../../constant';
import {TextWrapper, Container, Button, Form, Scaling} from './../../../components';
import * as STRING from './../../../string';

import ResolveAssetSource from 'resolveAssetSource';
import {moderateScale} from 'react-native-size-matters';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class ConfirmItem extends Component {

  _renderFooter(){
    let {mode} = this.props;
    if (mode == 'confirm') {
      return(
          <FooterContent 
            {...this.props}
            {...this.state}
          />
      );
    }
  }

  _getTitle(){
    let {mode} = this.props;
    if (mode == 'confirm') {
        return STRING.SCREEN.CONFIRM_ITEM.CONFIRM_MODE_TITLE;
    }
    else{
      return STRING.SCREEN.CONFIRM_ITEM.READ_MODE_TITLE;
    }
  }
	render() {
    try{
      let {orderData} = this.props;

      let {orderitem, orderlaundry} = orderData;

      return (
        <LayoutPrimary
          {...this.props}

          showTitle={true}
          showBackButton={true}
          showTabBar={false}
          title={this._getTitle()}
        >
              <Container.TypeA
                contentStyle={{
                  minHeight:'100%',
                }}
              >
                  
                  <FlatList
                    data={orderitem}
                    keyExtractor={(item, index) => index+''}
                    renderItem={(items, separator) => { 
                      let {item} = items; 
                      return (<ItemContent item={item}/>);
                    }}
                    contentContainerStyle={{
                      flex:1,
                    }}
                    ListEmptyComponent={() => {return (<EmptyContent />);}}
                    ListHeaderComponent={() => {return (<HeaderContent />);}}
                  /> 
                  
                  {this._renderFooter()}
              </Container.TypeA>
        </LayoutPrimary>
      );
    }
    catch(error){
      console.log(error);
      return null;
    }

	}
}

class HeaderContent extends Component{
  render(){
    return(
      <View style={styleTable.container}>
        <View
          style={styleTable.containerFirstSection}
        >
          <TextWrapper fontWeight={'SemiBold'} type={'smallestcontent'} style={styleTable.headerItemText}>Produk</TextWrapper>
        </View>

        <View
          style={styleTable.containerSecondSection}
        >
          <TextWrapper fontWeight={'SemiBold'} type={'smallestcontent'} style={styleTable.headerItemText}>Jumlah</TextWrapper>
        </View>
        
      </View>
    );
  }
} 

class FooterContent extends Component{
  constructor(props){
    super(props);
    this.state = {
        courierNote:'',
    };
  }

  _onChangeTextCourierNote(courierNote){
    this.setState({courierNote});
  }

  _onPressSubmitButton(){
    try{
      let {actionOrder, navigator} = this.props;
      let {courierNote} = this.state;

      actionOrder.submitConfirmItem(navigator, courierNote);
    }
    catch(error){
      console.log(error);
    }
  }
  render(){
    try{
      
      return(
        <View style={{
          flex:1, 
          alignItems:'center', 
          justifyContent:'flex-end',
          height:Dimensions.get('window').height * 0.075,
        }}>
            <Form.InputField.TypeB
              keyboardType={'visible-password'}
              ref='username_login' 
              placeholder={'Tambahkan catatan bila ada perubahan'}
              topLabel={true}
              topLabelText={'Catatan Tambahan'}
              onChangeText={this._onChangeTextCourierNote.bind(this)}
            />

            <Button.Standard
              buttonSize={CONSTANT.STYLE.BUTTON.LARGE}
              buttonColor={CONSTANT.COLOR.LIGHT_BLUE}
              buttonText={STRING.CHECKOUT_COURIER.BUTTON}
              onPress={this._onPressSubmitButton.bind(this)}
              buttonStyle={{marginTop:Scaling.verticalScale(10)}}
            />

        </View>
      );
    }
    catch(error){
      return null;
    }
  }
}

class ItemContent extends Component{

  render(){
    return (
        <View style={styleTable.container}>

            <View
              style={styleTable.containerFirstSection}
            >
                <ProductContent
                  {...this.props}
                />
            </View>

            <View
              style={styleTable.containerSecondSection}
            >
                <QuantityContent
                  {...this.props}
                />
            </View>
        </View>
    )
  }
}

class EmptyContent extends Component{
  render(){
    return(
      <View/>
    );
  }
}

class ProductContent extends Component{
  render(){
    try{
      let {item} = this.props;

      let {outlet, kliknklin} = item;
     
      let itemName = '';
      let washingServiceInfo = '';
      let timeServiceInfo = '';
      let itemAddedService = [];
      let isOnDemand = false;

      if (outlet) {
          let {outletitemwashingtime, choosenaddedservice} = outlet;
          let {itemwashingtime, itemaddedservice} = outletitemwashingtime;
          let data = itemwashingtime.item;
          let {washingservice, timeservice, item} = itemwashingtime;
          itemName = item.name;
          washingServiceInfo = washingservice.info;
          timeServiceInfo = timeservice.info;
          itemAddedService = choosenaddedservice;
      }
      else if (
        kliknklin
      ) {
        let {itempricearea, choosenaddedservice} = kliknklin;
        let {itemwashingtime, itemaddedservice} = itempricearea;
        let data = itemwashingtime.item;
        let {washingservice, timeservice, item} = itemwashingtime;
        itemName = item.name;
        washingServiceInfo = washingservice.info;
        timeServiceInfo = timeservice.info;
        itemAddedService = choosenaddedservice;

        isOnDemand = true;
      }
    
      return(
        <View>
          <TextWrapper type={'base'} fontWeight={'SemiBold'}>{itemName}</TextWrapper>
          <TextWrapper type={'smallbase'} style={{marginTop:2.5}}>{washingServiceInfo + ' - ' + timeServiceInfo}</TextWrapper>
    
          
          <View
            style={{
              marginLeft:moderateScale(10),
            }}
          >
              <FlatList
                data={itemAddedService}
                keyExtractor={(item, index) => index+''}
                renderItem={(items, separator) => { 
                  let {item} = items; 
                  return (<AddedServiceContent isOnDemand={isOnDemand} item={item}/>);
                }}
                ListHeaderComponent={() => {
                  if (itemAddedService.length > 0) {
                    return (<HeaderAddedServiceContent />);
                  }
                  return null;
                }}
              /> 
          </View>
          
        </View>
      );
    }
    catch(error){
      console.log(error, 'before_checkout.ProductContent')
      return null;
    }
  }
}

class QuantityContent extends Component{
    render(){
      try{
        let {item} = this.props;
        let {outlet, kliknklin} = item;

        let itemQuantity = item.quantity;
        let itemUnitMeasurement = '';

        if (outlet) {
            let {outletitemwashingtime, choosenaddedservice} = outlet;
            let {itemwashingtime, itemaddedservice} = outletitemwashingtime;
            let data = itemwashingtime.item;
            let {washingservice, timeservice, item} = itemwashingtime;
            itemUnitMeasurement = item.unit_measurement;
        }
        else if (
          kliknklin
        ) {
            let {itempricearea, choosenaddedservice} = kliknklin;
            let {itemwashingtime, itemaddedservice} = itempricearea;
            let data = itemwashingtime.item;
            let {washingservice, timeservice, item} = itemwashingtime;
            itemUnitMeasurement = item.unit_measurement;
        }

        return(
             <TextWrapper type={'base'} fontWeight={'SemiBold'}>{itemQuantity + ' ' + itemUnitMeasurement}</TextWrapper>
        );
      }
      catch(error){
        return null;
      }
    }
}

class QuantityContents extends Component{
    render(){
      let {item} = this.props;
      return(
          <View style={{
            flexDirection:'row',
            alignItems:'center',
          }}>
            <View
              style={{
                flex:0.25,
              }}
            >
              <TouchableOpacity
                  style={{
                      width:moderateScale(20),
                      height:moderateScale(20),
                      borderRadius:moderateScale(20/2),
                      backgroundColor:CONSTANT.COLOR.BLUE_A,
                      alignItems:'center',
                      justifyContent:'center'
                  }}
              >

                  <TextWrapper type={'content'} style={{color:'white'}}>{'-'}</TextWrapper>
              </TouchableOpacity>

            </View>
            
            <View
              style={{
                flex:0.5,
                alignItems:'center',
              }}
            >
            <TextWrapper type={'smallercontent'}>{this.props.data.quantity + ' ' + this.props.data.unitName}</TextWrapper>
            </View>

            <View
              style={{
                flex:0.25,
              }}
            >
            <TouchableOpacity
                style={{
                    width:moderateScale(20),
                    height:moderateScale(20),
                    borderRadius:moderateScale(20/2),
                    backgroundColor:CONSTANT.COLOR.BLUE_A,
                    alignItems:'center',
                    justifyContent:'center'
                }}
            >
              <TextWrapper type={'content'} style={{color:'white'}}>{'+'}</TextWrapper>
            </TouchableOpacity>

            </View>

          </View>
      );
    }
}



class AddedServiceContent extends Component{
    render(){
      try{
        let {item, isOnDemand} = this.props;
        let info = '';

        if (isOnDemand) {
          let {kliknklinaddeditem} = item;
          let {itemaddedservice} = kliknklinaddeditem;
          let {mdaddedservice} = itemaddedservice;

          info = mdaddedservice.info;
        }
        else{
          let {outletaddeditem} = item;
          let {detail} = outletaddeditem;
          let {mdaddedservice} = detail;

          info = mdaddedservice.info;
        }

        return(
          <TextWrapper type={'smallbase'} style={{marginTop:0}} >
            {info}
          </TextWrapper>
        );
      }
      catch(error){
        return null;
      }
    }
}


class HeaderAddedServiceContent extends Component{
  render(){
    return(
      <View>
        <TextWrapper type={'smallbase'} fontWeight={'SemiBold'} style={{marginTop:3, color:CONSTANT.COLOR.GRAY_C}}>{'Servis Tambahan'}</TextWrapper>
      </View>
    );
  }
}

class AddItems extends Component{
  render(){
    return(
        <View style={{
          flexDirection:'row',
          justifyContent:'center',
          marginHorizontal:Dimensions.get('window').width * 0.025,
          marginTop:Dimensions.get('window').height * 0.015,
        }}>

        <View
          style={{flex:0.35,alignItems:'center',}}
        >
            <TouchableOpacity
              style={{ 
                backgroundColor:CONSTANT.COLOR.BLUE_A, 
                padding:5,
                borderRadius:moderateScale(5),
                alignItems:'center',
                width:moderateScale(75),
              }}
            >
              <TextWrapper type={'base'} style={{fontWeight:'bold', color:'white'}}>{'Add Item'}</TextWrapper>
            </TouchableOpacity> 
        </View>

        <View
          style={{flex:0.65}}
        >
        </View>
      </View>
    );
  }
}



const styleTable = StyleSheet.create({
  container:{
    flex:1,
    flexDirection:'row',
    marginHorizontal:Dimensions.get('window').width * 0.025,
    marginTop:Dimensions.get('window').height * 0.015,
  },
  containerFirstSection:{
    flex:0.75,
  },
  containerSecondSection:{
    flex:0.25,
  },
  headerItemText:{
    color:CONSTANT.COLOR.GRAY_C,
  },
  contentItemText:{
    color:CONSTANT.COLOR.BLACK_A,
  },
});


const styles = StyleSheet.create({
  textItems:{
    
  },
  textItemsOther:{
    marginTop:2.5,
  },
});

function mapStateToProps(state, ownProps) {
	return {
		orderData:state.rootReducer.order.active.data,
		layoutHeight:state.rootReducer.layout.height,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    actionOrder: bindActionCreators(actions.order, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmItem);

