import React, { Component, PureComponent } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Text,
        StyleSheet,
        BackHandler
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from './../../../layout';
import {TextWrapper, Button, Form, TabBar, OrderStatus, DateFormat, toCurrency} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";


//local
import InProgressContent from './in_progress_content';
import PickupContent from './pickup_content';
import DeliveryContent from './delivery_content';
import IncomingContent from './incoming_content';
import HistoryContent from './history_content';
import ComplaintContent from './complaint_content';

class OrderList extends Component{

  componentDidMount(){
    let {actionOrder} = this.props;
    actionOrder.setSelectedOrderDate(new Date());

    this.loadData();
    

    if (this.props.overrideBackPress) {
      let route = CONSTANT.ROUTE_TYPE.DASHBOARD;
      let prop = {};
      
      this.props.actionRoot.setOverrideBackPress(true, route, prop, 'reset');

      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
        this.props.actionRoot.setOverrideBackPress(false, null, 'reset', {});
        this.props.actionNavigator.resetTo(this.props.navigator, route, prop);
        return true;
      });
    }
  }

  componentWillUnmount() {
    if (this.props.overrideBackPress) {
      this.backHandler.remove();
    }
  }


  loadData(){
    let {type, actionOrder,} = this.props;
    if (type == CONSTANT.ORDER_ACTIVE_TYPE.INCOMING) {
      actionOrder.getIncomingList();
    }
    else if (type == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP) {
      actionOrder.getPickupList();
    }
    else if (type == CONSTANT.ORDER_ACTIVE_TYPE.IN_PROGRESS) {
      actionOrder.getInProgressList();
    }
    else if (type == CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY) {
      actionOrder.getDeliveryList();
    }
    else if (type == CONSTANT.ORDER_ACTIVE_TYPE.COMPLAINT) {
      actionOrder.getComplaintList();
    }
    else if (type == CONSTANT.ORDER_ACTIVE_TYPE.HISTORY) {
      actionOrder.getHistoryList();
    }
  }

  extractData(data, filterType = 'time', shipmentType = CONSTANT.SHIPMENT_TYPE.DELIVERY){
    let {selectedOrderDate} = this.props;
    let tempResult = [];
    
    for(let i in data){
      let shipment = this.getShipment(data[i], shipmentType);
      let {shipmentschedule, shipment_status_id, internalshipment} = shipment;
      let {shipment_date, time} = shipmentschedule;

      selectedOrderDate = new Date(selectedOrderDate.toDateString());
      let shipmentDate = new Date(shipment_date);
      shipmentDate = new Date(shipmentDate.toDateString())
      
      if (filterType == 'time' && shipmentDate.getTime() == selectedOrderDate.getTime()) {
        data[i].selectedShipment = shipment;
        tempResult.push(data[i]);
      }
      else if(filterType == 'date' || filterType == 'createdDate'){
        data[i].selectedShipment = shipment;
        tempResult.push(data[i]);
      }
    }

    let result = [];

    for(let i in tempResult){
      let obj = {};
      let shipment = tempResult[i].selectedShipment;
      let shipmentTimeInfo = shipment.shipmentschedule.shipment_time_info;
      let shipmentDate = shipment.shipmentschedule.shipment_date;
      let createdAt = tempResult[i].created_at.split(' ')[0];
      if (result.length == 0) {
        obj.time = shipmentTimeInfo;
        obj.date = shipmentDate;
        obj.createdAt = createdAt;
        obj.data = [];
        obj.data.push(tempResult[i]);
        result.push(obj);
      }
      else{
        let isExist = false;
        for(let j in result){
          if (filterType == 'time' && result[j].time == shipmentTimeInfo) {
            isExist = true;
            result[j].data.push(tempResult[i]);
          }
          else if(filterType == 'date' && result[j].date == shipmentDate){
            isExist = true;
            result[j].data.push(tempResult[i]);
          }
          else if(filterType == 'createdDate' && result[j].createdAt == createdAt){
            isExist = true;
            result[j].data.push(tempResult[i]);
          }
        }

        if (!isExist) {
          obj.time = shipmentTimeInfo;
          obj.date = shipmentDate;
          obj.createdAt = createdAt;
          obj.data = [];
          obj.data.push(tempResult[i]);
          result.push(obj);
        }
      }
    }
    
    if (filterType == 'time') {
        result.sort((a,b) => {
          let shipmentA = a.time;
          shipmentA = shipmentA.split('-')[0].split(':')[0];
          let shipmentB = b.time;
          shipmentB = shipmentB.split('-')[0].split(':')[0];
          return shipmentA - shipmentB;
        })
    }
    else if(filterType == 'date'){
        result.sort((a,b) => {
          let shipmentA = a.date;
          let shipmentB = b.date;
          return new Date(shipmentA).getTime() - new Date(shipmentB).getTime();
        })
    }
    else if (filterType == 'createdDate') {
        result.sort((a,b) => {
          let shipmentA = a.createdAt;
          let shipmentB = b.createdAt;
          return new Date(shipmentA).getTime() - new Date(shipmentB).getTime();
        })
    }

    return result;
    
  }

  extractDataComplaint(data, filterType = 'date'){
    let tempResult = [];
    
    for(let i in data){
      tempResult.push(data[i]);
    }

    let result = [];

    for(let i in tempResult){
      let obj = {};
      let createdAt = tempResult[i].complaint.created_at ? tempResult[i].complaint.created_at.split(' ')[0] : new Date();

      if (result.length == 0) {
        obj.createdAt = createdAt;
        obj.data = [];
        obj.data.push(tempResult[i]);
        result.push(obj);
      }
      else{
        let isExist = false;
        for(let j in result){
          if(filterType == 'date' && result[j].createdAt == createdAt){
            isExist = true;
            result[j].data.push(tempResult[i]);
          }
        }

        if (!isExist) {
          obj.createdAt = createdAt;
          obj.data = [];
          obj.data.push(tempResult[i]);
          result.push(obj);
        }
      }
    }
    
    result.sort((a,b) => {
      let shipmentA = a.createdAt;
      let shipmentB = b.createdAt;
      return new Date(shipmentA).getTime() - new Date(shipmentB).getTime();
    })

    return result;
    
  }

  extractDataIncoming(data, filterType = 'date'){
    let tempResult = [];
    
    for(let i in data){
      tempResult.push(data[i]);
    }

    let result = [];

    for(let i in tempResult){
      let obj = {};
      let date = tempResult[i].pickup_date ? new Date(tempResult[i].pickup_date) : new Date();
      
      if (result.length == 0) {
        obj.date = date;
        obj.data = [];
        obj.data.push(tempResult[i]);
        result.push(obj);
      }
      else{
        let isExist = false;
        for(let j in result){
          if(filterType == 'date' && result[j].date.getTime() == date.getTime()){
            isExist = true;
            result[j].data.push(tempResult[i]);
          }
        }

        if (!isExist) {
          obj.date = date;
          obj.data = [];
          obj.data.push(tempResult[i]);
          result.push(obj);
        }
      }
    }
    
    result.sort((a,b) => {
      let shipmentA = a.date;
      let shipmentB = b.date;
      return new Date(shipmentA).getTime() - new Date(shipmentB).getTime();
    });


    return result;   
  }

  getTitle(){
    if (this.props.type == CONSTANT.ORDER_ACTIVE_TYPE.INCOMING) {
      return 'Pesanan Baru';
    }
    else if (this.props.type == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP) {
      return 'Penjemputan';
    }
    else if (this.props.type == CONSTANT.ORDER_ACTIVE_TYPE.IN_PROGRESS) {
      return 'Proses Cuci';
    }
    else if (this.props.type == CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY) {
      return 'Pengantaran';
    }
    else if (this.props.type == CONSTANT.ORDER_ACTIVE_TYPE.COMPLAINT) {
      return 'Komplain';
    }
    else if (this.props.type == CONSTANT.ORDER_ACTIVE_TYPE.HISTORY) {
      return 'Riwayat Pesanan'
    }
  }


  getShipment(item, type = CONSTANT.SHIPMENT_TYPE.DELIVERY){
    let {shipments} = item;
    
    for(let i in shipments){
      let {shipmenttype} = shipments[i];

      if (shipmenttype.id == type) {
        return shipments[i];
      }
    }
  }

  getContentComponent(item){
    let {type, historyOrderList, pickupOrderList, latePickupOrderList, inProgressOrderList, lateInProgressOrderList, deliveryOrderList, lateDeliveryOrderList, incomingOrderList, complaintOrderList} = this.props;

    if (this.props.type == CONSTANT.ORDER_ACTIVE_TYPE.INCOMING) {
      return (
        <IncomingContent
          onlineList={this.extractDataIncoming(incomingOrderList.online)}
          posList={this.extractDataIncoming(incomingOrderList.pos)}
          {...this.props}
        />
      );
    }
    else if (this.props.type == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP) {
      return (
        <PickupContent
          dataList={this.extractData(pickupOrderList, 'time', CONSTANT.SHIPMENT_TYPE.PICKUP)}
          dataLateList={this
            .extractData(latePickupOrderList, 'date', CONSTANT.SHIPMENT_TYPE.PICKUP)}
          {...this.props}
        />
      );
    }
    else if (this.props.type == CONSTANT.ORDER_ACTIVE_TYPE.IN_PROGRESS) {

      return (
        <InProgressContent
          dataList={this.extractData(inProgressOrderList)}
          dataLateList={this.extractData(lateInProgressOrderList, 'date')}
          {...this.props}
        />
      );
    }
    else if (this.props.type == CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY) {
      
      return (
        <DeliveryContent
          dataList={this.extractData(deliveryOrderList)}
          dataLateList={this.extractData(lateDeliveryOrderList, 'date')}
          {...this.props}
        />
      );
    }
    else if (this.props.type == CONSTANT.ORDER_ACTIVE_TYPE.COMPLAINT) {
      return (
        <ComplaintContent
          onlineList={this.extractDataComplaint(complaintOrderList.online)}
          posList={this.extractDataComplaint(complaintOrderList.pos)}
          {...this.props}
          getShipment={this.getShipment.bind(this)}
        />
      );
    }
    else if (this.props.type == CONSTANT.ORDER_ACTIVE_TYPE.HISTORY) {
      return(
        <HistoryContent
          onlineList={this.extractData(historyOrderList.online,'createdDate')}
          posList={this.extractData(historyOrderList.pos,'createdDate')}
          {...this.props}
        />
      );
    }
  }

  render(){
      try{
          return (
            <LayoutPrimary
                showTitle={true}
                showBackButton={true}
                showNotificationButton={true}
                showSearchButton={true}
                searchingType={this.props.type}
                showTabBar={false}
                navigator={this.props.navigator}
                title={this.getTitle()}
              >
                
                  {this.getContentComponent()}
            </LayoutPrimary>
          );
      }
      catch(error){
        console.log(error);
        return null;
      }
      
  }
}

class IncomingContents extends PureComponent{

  _getCourier(courier){
    let result = [];
    for(let index in courier){
      let obj = {};
      obj.id = index;
      obj.info = courier[index];

      result.push(obj);
    }
    return result;
  }

  _onPressAcceptIncoming(){
      actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeC', {
        containerStyle:{
            height:Dimensions.get('window').height * 0.6,
          },
          data:{
            titleText:'Pilih Alasan Penolakan',
            type:'reject-reason',
            buttonPrimaryText:'Tolak',
            onPressButtonPrimary:(rejectReasonID, rejectReasonInfo) =>{
              if (!rejectReasonID) {
                alert('Pilih kurir terlebih dahulu')
              }
              else{
                // actionNavigator.dismissLightBox();
              
                // setTimeout(() => {
                //   actionOrder.setIncomingResponse(false, null, order_id , rejectReasonID);
                // },500);
              }
              
            }
          },
      });
  }

  render(){
    try{
      let {item, actionNavigator, actionOrder} = this.props;
      
      let {order_id, order_status, order_status_laundry, pickup_date, pickup_time, pickup_location, price, payment_method, courier} = item;
    return(
      <Box>
            <View style={{flex:0.25, flexDirection:'row'}}>
              <View
                style={{
                  justifyContent:'center',
                  flex:0.5,
                }}
              >
                <TextWrapper type={'base'}>{DateFormat(new Date(pickup_date), 'fullDate')}</TextWrapper>
                <TextWrapper type={'base'}>{ pickup_time}</TextWrapper>
              </View>

              <View
                style={{
                  flex:0.5,
                  alignItems:'flex-end',
                }}
              >
                <TextWrapper type={'base'}>{payment_method + ' - ' + toCurrency(price, 'Rp ')}</TextWrapper>
              </View>
            </View>
            <View style={{flex:0.5, justifyContent:'center'}}>
                <TextWrapper type={'smallercontent'} style={{textAlign:'center'}}>{pickup_location}</TextWrapper>
            </View>
             <View style={{flex:0.25, justifyContent:'space-evenly', flexDirection:'row'}}>
                <Button.TypeA
                  buttonText={'Tolak'}
                  buttonColor={CONSTANT.COLOR.WHITE_A}
                  containerStyle={{
                    height:Dimensions.get('window').height * 0.05,
                  }}
                  buttonStyle={{
                    height:Dimensions.get('window').height * 0.04,
                    width:Dimensions.get('window').width * 0.3,
                    borderColor:CONSTANT.COLOR.GRAY_B,
                    borderWidth:2,
                  }}
                  buttonTextStyle={{
                    color:CONSTANT.COLOR.GRAY_B,
                  }}
                  onPress={() => {
                    actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeC', {
                    containerStyle:{
                        height:Dimensions.get('window').height * 0.6,
                      },
                      data:{
                        titleText:'Pilih Alasan Penolakan',
                        type:'reject-reason',
                        buttonPrimaryText:'Pilih',
                        onPressButtonPrimary:(rejectReasonID, rejectReasonInfo) =>{

                          actionNavigator.dismissLightBox();
                          
                          setTimeout(() => {
                            actionOrder.setIncomingResponse(false, null, order_id , rejectReasonID);
                          },500);
                        }
                      },
                    });
                  }}
                />
                
                <Button.TypeA
                  buttonText={'Terima'}
                  buttonColor={CONSTANT.COLOR.BLUE_A}
                  containerStyle={{
                    height:Dimensions.get('window').height * 0.05,
                  }}
                  buttonStyle={{
                    height:Dimensions.get('window').height * 0.04,
                    width:Dimensions.get('window').width * 0.3,
                  }}
                  
                  onPress={() => {
                    actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeC', {
                    containerStyle:{
                        height:Dimensions.get('window').height * 0.6,
                      },
                      data:{
                        titleText:'Pilih Kurir',
                        type:'custom',
                        itemList:this._getCourier(courier),
                        buttonPrimaryText:'Pilih',
                        onPressButtonPrimary:(courierID, courierInfo) =>{

                          actionNavigator.dismissLightBox();
                          
                          setTimeout(() => {
                            actionOrder.setIncomingResponse(true, courierID, order_id , null);
                          },500);
                        }
                      },
                    });
                  }}
                />

            </View>

      </Box>
    );
    }
    catch(error){
      return null;
    }
  }
}

class PickupContents extends PureComponent{

  onPress(item = this.props.item){
      this.props.actionOrder.setOrderActive(item);
      this.props.actionOrder.setShipmentActivePickup(item);
      this.props.actionOrder.setShipmentActiveDelivery(item);
      this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.OrderContent', {})
  }

  getShipmentPickup(){
    let {item} = this.props;

    let {shipments} = item;

    for(let i in shipments){
      let {shipmenttype} = shipments[i];

      if (shipmenttype.id == CONSTANT.SHIPMENT_TYPE.PICKUP) {
        return shipments[i];
      }
    }
  }

  render(){
    try{
    let {item} = this.props;
    let {customer, orderlaundry, order_hash} = item;

    let {user} = customer;
    let {name} = user;

    let {orderstatuslaundry} = orderlaundry;
    let orderStatusLaundryID = orderstatuslaundry.id;

    let shipment = this.getShipmentPickup();
    let {shipmentschedule, shipment_status_id} = shipment;
    let {location_address, shipment_time_info, shipment_date} = shipmentschedule;

    return(
      <View
      >
        <Box
          onPress={this.onPress.bind(this)}
        >
              <View
                  style={{
                    position:'absolute',
                    right:-5,
                    top:-5,
                    //width:moderateScale(25),
                    //height:moderateScale(25),
                    borderRadius:moderateScale(25/2),
                    backgroundColor:CONSTANT.COLOR.BLUE_A,
                    alignItems:'center',
                    justifyContent:'center'
                  }}
              >

                <OrderStatus
                    orderStatus={orderStatusLaundryID}
                    shipmentStatus={shipment_status_id}
                />

              </View>
              <View style={{flex:0.35, flexDirection:'row', alignItems:'center'}}>
                <View
                  style={{
                    justifyContent:'center',
                  }}
                >
                  <TextWrapper type={'base'}>{order_hash}</TextWrapper>
                  <TextWrapper type={'base'}>{DateFormat(shipment_date, 'fullDate')}</TextWrapper>
                  <TextWrapper type={'base'}>{shipment_time_info}</TextWrapper>

                </View>
              </View>
              <View style={{flex:0.65, marginBottom:moderateScale(10),justifyContent:'center'}}>
                  <View style={styleContent.itemRow}>
                      <View style={[styleContent.itemRowTitle, {flex:0.20}]}>
                        <TextWrapper type={'base'}>
                        Nama
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowColon}>
                        <TextWrapper type={'base'}>
                        :
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowContent}>
                        <TextWrapper type={'base'}>
                        {name}
                        </TextWrapper>
                      </View>
                    </View>

                    <View style={styleContent.itemRow}>
                      <View style={[styleContent.itemRowTitle, {flex:0.20}]}>
                        <TextWrapper type={'base'}>
                        Tempat
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowColon}>
                        <TextWrapper type={'base'}>
                        :
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowContent}>
                        <TextWrapper type={'base'}>
                        {location_address}
                        </TextWrapper>
                      </View>
                    </View>
              </View>

        </Box>
      </View>
    );
    }
    catch(error){
        console.log(error, 'order_list.PickupContent');
        return null;
    }
  }
}

class DeliveryContents extends PureComponent{
  onPress(item = this.props.item){
      this.props.actionOrder.setOrderActive(item);
      this.props.actionOrder.setShipmentActivePickup(item);
      this.props.actionOrder.setShipmentActiveDelivery(item);
      this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.OrderContent', {})
  }

  getShipmentDelivery(){
    let {item} = this.props;

    let {shipments} = item;

    for(let i in shipments){
      let {shipmenttype} = shipments[i];

      if (shipmenttype.id == CONSTANT.SHIPMENT_TYPE.DELIVERY) {
        return shipments[i];
      }
    }
  }

  render(){
    try{
    let {item} = this.props;
    
    let {customer, orderlaundry, order_hash} = item;

    let {user} = customer;
    let {name} = user;

    let {orderstatuslaundry} = orderlaundry;
    let orderStatusLaundryID = orderstatuslaundry.id;

    let shipment = this.getShipmentDelivery();
    let {shipmentschedule, shipment_status_id} = shipment;
    let {location_address, shipment_time_info, shipment_date} = shipmentschedule;

    return(
      <View
      >
        <Box
          onPress={this.onPress.bind(this)}
        >
              <View
                  style={{
                    position:'absolute',
                    right:-5,
                    top:-5,
                    //width:moderateScale(25),
                    //height:moderateScale(25),
                    borderRadius:moderateScale(25/2),
                    backgroundColor:CONSTANT.COLOR.BLUE_A,
                    alignItems:'center',
                    justifyContent:'center'
                  }}
              >

                <OrderStatus
                    orderStatus={orderStatusLaundryID}
                    shipmentStatus={shipment_status_id}
                />

              </View>
              <View style={{flex:0.35, flexDirection:'row', alignItems:'center'}}>
                <View
                  style={{
                    justifyContent:'center',
                  }}
                >
                  <TextWrapper type={'base'}>{order_hash}</TextWrapper>
                  <TextWrapper type={'base'}>{DateFormat(shipment_date, 'fullDate')}</TextWrapper>
                  <TextWrapper type={'base'}>{shipment_time_info}</TextWrapper>
                  
                </View>
              </View>
              <View style={{flex:0.65, marginBottom:moderateScale(10),justifyContent:'center'}}>
                  <View style={styleContent.itemRow}>
                      <View style={[styleContent.itemRowTitle, {flex:0.20}]}>
                        <TextWrapper type={'base'}>
                        Nama
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowColon}>
                        <TextWrapper type={'base'}>
                        :
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowContent}>
                        <TextWrapper type={'base'}>
                        {name}
                        </TextWrapper>
                      </View>
                    </View>

                    <View style={styleContent.itemRow}>
                      <View style={[styleContent.itemRowTitle, {flex:0.20}]}>
                        <TextWrapper type={'base'}>
                        Tempat
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowColon}>
                        <TextWrapper type={'base'}>
                        :
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowContent}>
                        <TextWrapper type={'base'}>
                        {location_address}
                        </TextWrapper>
                      </View>
                    </View>
              </View>

        </Box>
      </View>
    );
    }
    catch(error){
        console.log(error, 'order_list.PickupContent');
        return null;
    }
  }
}

class ComplaintContents extends PureComponent{

  onPress(){
      try{
        let {actionOrder, actionComplaint, item} = this.props;
        actionComplaint.setComplaintMessageList(item.complaint.messages);
        this.props.actionOrder.setOrderActive(item, CONSTANT.ORDER_ACTIVE_TYPE.COMPLAINT);
        this.props.actionOrder.setShipmentActivePickup(null);
        this.props.actionOrder.setShipmentActiveDelivery(null);
        this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.OrderContent', {});
      }
      catch(error){
        console.log('--error order list complaint onpress');
      }
  }

  renderImportantSign(){
    try{
        let {item, actionComplaint} = this.props;
        let {customer, complaint, order_hash} = item;

        if (item.important) {
              return(
                <View
                  style={{
                    //position:'absolute',
                    right:moderateScale(10),
                    //top:-5,
                    width:moderateScale(25),
                    height:moderateScale(25),
                    borderRadius:moderateScale(25/2),
                    backgroundColor:CONSTANT.COLOR.YELLOW_A,
                    alignItems:'center',
                    justifyContent:'center'
                  }}
                >
                  <TextWrapper type={'content'} style={{color:'white'}} fontWeight={'SemiBold'}>!</TextWrapper>
                </View>
              );
            }
    }
    catch(error){

    }
  }

  render(){
    try{
        let {item} = this.props;
        let {customer, complaint, order_hash} = item;
          return(
            <View
            >
            <Box
              onPress={this.onPress.bind(this)}
            >
                <View
                  style={{
                    position:'absolute',
                    right:-5,
                    top:-5,
                    //width:moderateScale(25),
                    //height:moderateScale(25),
                    borderRadius:moderateScale(25/2),
                    alignItems:'center',
                    justifyContent:'center',
                    flexDirection:'row',
                  }}
                >

                {this.renderImportantSign()}

                <OrderStatus
                    complaintStatus={complaint.complaint_status_id}
                />

              </View>
              <View style={{flex:0.25, flexDirection:'row', alignItems:'center'}}>
                <View
                  style={{
                    justifyContent:'center',
                  }}
                >
                  <TextWrapper type={'base'}>{order_hash}</TextWrapper>
                  
                </View>
              </View>
              <View style={{flex:0.75, marginBottom:moderateScale(10),justifyContent:'center'}}>
                  <View style={styleContent.itemRow}>
                      <View style={[styleContent.itemRowTitle, {flex:0.20}]}>
                        <TextWrapper type={'base'}>
                        Nama
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowColon}>
                        <TextWrapper type={'base'}>
                        :
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowContent}>
                        <TextWrapper type={'base'}>
                        {customer.user.name}
                        </TextWrapper>
                      </View>
                    </View>

                    <View style={styleContent.itemRow}>
                      <View style={[styleContent.itemRowTitle, {flex:0.20}]}>
                        <TextWrapper type={'base'}>
                        Keluhan
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowColon}>
                        <TextWrapper type={'base'}>
                        :
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowContent}>
                        <TextWrapper type={'base'}>
                        {complaint.complaintreason.info}
                        </TextWrapper>
                      </View>
                    </View>
              </View>

            </Box>
            </View>
          )
    }
    catch(error){
      console.log(error, '--catch orderlist-complaintcontent');
      return null;
    }
  }
}

class Box extends PureComponent{

  render(){
    return(
      <TouchableOpacity
        {...this.props}
        style={{
          flex:1,
          minHeight:Dimensions.get('window').height * 0.2,
          borderRadius:5,
          backgroundColor:'white',
          elevation:3,
          marginBottom:moderateScale(10),
          marginHorizontal:moderateScale(2),
        }}

        onPress={() => {this.props.onPress ? this.props.onPress() : () => {}}}
      >
        <View
          style={{flex:1,
            margin:moderateScale(10)}}
        >
            {this.props.children}
        </View>
        
      </TouchableOpacity>
    );
  }
}


const styleContent = StyleSheet.create({
  titleRow:{
    flexDirection:'row', 
    alignItems:'center',
  },
  titleRowImage:{
    flex:0.1,
  },
  titleRowContent:{
    flex:0.4,
  },
  titleRowRightContent:{
    flex:0.5,
    alignItems:'flex-end',
  },
  itemRow:{
    marginTop:moderateScale(3),
    flexDirection:'row',
  },
  itemRowTitle:{
    flex:0.25,
  },
  itemRowColon:{
    flex:0.04,
  },
  itemRowContent:{
    flex:0.71,
    flexDirection:'row',
    alignItems:'center',
  },
  itemDetail:{
    paddingHorizontal:Dimensions.get('window').width * 0.025,
    paddingVertical:Dimensions.get('window').height * 0.01,
    borderColor:'#F0F0F0',
  },
});


function mapStateToProps(state, ownProps) {
  return {
    userRole:state.rootReducer.user.role,
    historyOrderList:state.rootReducer.order.historyOrderList,
    pickupOrderList:state.rootReducer.order.pickupOrderList,
    latePickupOrderList:state.rootReducer.order.latePickupOrderList,
    inProgressOrderList:state.rootReducer.order.inProgressOrderList,
    lateInProgressOrderList:state.rootReducer.order.lateInProgressOrderList,
    deliveryOrderList:state.rootReducer.order.deliveryOrderList,
    lateDeliveryOrderList:state.rootReducer.order.lateDeliveryOrderList,
    incomingOrderList:state.rootReducer.order.incomingOrderList,
    complaintOrderList:state.rootReducer.order.complaintOrderList,
    selectedOrderDate:state.rootReducer.order.selectedOrderDate,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionOrder: bindActionCreators(actions.order, dispatch),
    actionComplaint: bindActionCreators(actions.complaint, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderList);


