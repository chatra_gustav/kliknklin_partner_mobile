import React, { Component, PureComponent } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        StyleSheet,
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from './../../../../layout';
import {TextWrapper, Text, Button, Form, TabBar, OrderStatus, DateFormat, Scaling, toCurrency} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

//local
import * as style from './../style';
import OrderDateTab from './../order_date_tab';
import TimeSlot from './../time_slot';
import DateSlot from './../date_slot';
import BaseItem from './../base_item';
import OrderTypeTab from './../order_type_tab';
import NoOrderContent from './../no_order_content';


class InProgressContent extends PureComponent{
  static defaultProps = {
    data: [],
  }

  componentWillMount(){
    this.props.actionOrder.setOrderListTypeTab(1);
  }

  getContent(){
    let {orderListTypeTab, lateInProgressOrderList, dataList, dataLateList} = this.props;
    if (orderListTypeTab == 1) {

      return(
        <View
          style={{
            flex:1,
          }}
        >
          <OrderDateTab/>
          
          
          {dataList.length == 0 ? 
            <NoOrderContent />
              :
            <FlatList
                contentContainerStyle={{
                  paddingBottom:Scaling.verticalScale(200),
                }}
                initialNumToRender={4}
                data={dataList}
                keyExtractor={(item, index) => index+''}
                renderItem={({item, index}) => {
                  return <TimeSlot {...this.props} item={item} 
                      content={(items, key)=>{
                        return(
                            <Items
                              {...this.props}
                              item={items}
                              key={key}
                            />
                        )
                      }}
                      key={index}
                  />;
                }}
            /> 
          }
          
        </View>
      );
    }
    else if (orderListTypeTab == 0) {
      if (dataLateList.length != 0) {
        return(
          <FlatList
              contentContainerStyle={{
                  paddingBottom:Scaling.verticalScale(150),
              }}
              initialNumToRender={3}
              data={dataLateList}
              keyExtractor={(item, index) => index+''}
              renderItem={({item, index}) => {
                return <DateSlot {...this.props} item={item} 
                    content={(items, key)=>{
                      return(
                          <Items
                            {...this.props}
                            item={items}
                            key={key}
                          />
                      )
                    }}
                    key={index}
                />;
              }}
          /> 
        );
      }
      else{
        return(
          <NoOrderContent/>
        )
      }
    }
  }

  render(){
    let {lateInProgressOrderList} = this.props;
    return(
      <View
        style={{flex:1}}
      >
          <OrderTypeTab
            showImportant={lateInProgressOrderList.length == 0 ? false : true}
          />
          
          <View
            style={{
              flex:1,
            }}
          >
            {this.getContent()}
          </View>
      </View>
    );
  }
}

class Items extends PureComponent{
  componentDidMount(){
  }
  render(){
    try{
      let {item} = this.props;

      let {appliedpromo, online_flag, payment, id, customer, orderlaundry, order_hash, orderitem, pricedetail, logorderprice} = item;
      let {user} = customer;
      let {name} = user;

      let {orderstatuslaundry, expected_duration} = orderlaundry;
      let orderStatusLaundryID = orderstatuslaundry.id;

      let shipment = item.selectedShipment;
      let {shipmentschedule, shipment_status_id, internalshipment} = shipment;
      let userCourier = null;

      if (internalshipment) {
        let {courier} = internalshipment;
        let {partner} = courier;
        userCourier = partner.ktp_name;
      }
      
      let {location_address, shipment_time_info, shipment_date, time} = shipmentschedule;
      let {info} = time;
      let timeDelivery2 = info.split('-')[0];
      let timeDelivery = timeDelivery2.split(':');

      let deliveryDate = new Date(shipment_date);

      deliveryDate.setHours(timeDelivery[0]);
      deliveryDate.setMinutes(timeDelivery[1]);

      let reportDate = new Date(deliveryDate);

      reportDate.setHours(timeDelivery[0] - 6);
      reportDate.setMinutes(timeDelivery[1]);

      return(
        <BaseItem
          navigator={this.props.navigator}
          item={item}
          customerName={name}
          logOrderPrice={pricedetail}
          orderItem={orderitem}
          orderHash={order_hash}
          orderStatusLaundryID={orderStatusLaundryID}
          onlineFlag={online_flag}
          shipmentStatus={shipment_status_id}
          promo={appliedpromo}
          data={[
            {
              category:'Pelanggan',
              content:name,
            },
            {
              category:'Kurir Antar',
              content:userCourier ? userCourier : 'Ambil Sendiri',
            },
            {
              category:'Status Bayar',
              content:payment.payment_status_id == CONSTANT.PAYMENT_STATUS.PAID ? 'Lunas' : 'Belum Dibayar'
            },
          ]}
          submitButton={() => {
            if (online_flag) {
              return(
                <Button.Standard
                    buttonSize={CONSTANT.STYLE.BUTTON.THICKER_EXTRA_MEDIUM}
                    buttonText={'SELESAI DICUCI'}
                    onPress={() => {
                      this.props.actionOrder.submitFinishLaundry(this.props.navigator, id);
                    }}
                />
              );
            }
            return(
                <Button.Standard
                    buttonSize={CONSTANT.STYLE.BUTTON.THICKER_EXTRA_MEDIUM}
                    buttonColor={CONSTANT.COLOR.ORANGE}
                    buttonText={'DIAMBIL PELANGGAN'}
                    onPress={() => {
                      if (!payment.pay_early_flag) {
                        this.props.actionPOS.showPaymentBox('pay-later', (value) => {
                          this.props.actionOrder.submitFinishLaundry(this.props.navigator, id, value - pricedetail.full_price, 'paid-order' );
                        }, 
                        'Jumlah yang harus dibayarkan : ' + toCurrency(pricedetail.full_price, 'Rp '),
                        pricedetail.full_price);
                      }else{
                        this.props.actionOrder.submitFinishLaundry(this.props.navigator, id, -1, 'taken-order');
                      }
                    }}
                />
              );
            
          }}
        />
      );
    }
    catch(error){
        console.log(error, 'orderlist.in_progress_content');
        return null;
    }
  }
}

function mapStateToProps(state, ownProps) {
  return {
    inProgressOrderList:state.rootReducer.order.inProgressOrderList,
    lateInProgressOrderList:state.rootReducer.order.lateInProgressOrderList,
    selectedOrderDate:state.rootReducer.order.selectedOrderDate,
    orderListTypeTab:state.rootReducer.order.orderListTypeTab,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionOrder: bindActionCreators(actions.order, dispatch),
    actionPOS: bindActionCreators(actions.pos, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(InProgressContent);

