import React, { Component, PureComponent } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Text,
        StyleSheet,
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from './../../../layout';
import {TextWrapper, Button, Form, TabBar, OrderStatus, DateFormat, toCurrency} from './../../../components';
import * as STRING from './../../../string';
import * as CONSTANT from './../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";


//local
import InProgressContent from './in_progress_content';

class OrderList extends Component{
  componentDidMount(){  
    this.loadData();
  }

  loadData(){
    let {type, actionOrder} = this.props;

    if (type == 'incoming') {
      actionOrder.getIncomingList();
    }
    else if (type == 'pick-up') {
      actionOrder.getPickupList();
    }
    else if (type == 'on-progress') {
      actionOrder.getInProgressList();
    }
    else if (type == 'delivery') {
      actionOrder.getDeliveryList();
    }
    else if (type == 'complaint') {
      actionOrder.getComplaintList();
    }
  }



  getData(){
    let {type, pickupOrderList, inProgressOrderList, deliveryOrderList, incomingOrderList, complaintOrderList} = this.props;

    if (type == 'incoming') {
      return incomingOrderList;
    }
    else if (type == 'pick-up') {
      return pickupOrderList;
    }
    else if (type == 'on-progress') {
      return inProgressOrderList;
    }
    else if (type == 'delivery') {
      return deliveryOrderList;
    }
    else if (type == 'complaint') {
      return complaintOrderList;
    }
    
    return [];
  }

  getTitle(){
    if (this.props.type == 'incoming') {
      return 'Tawaran Baru';
    }
    else if (this.props.type == 'pick-up') {
      return 'Akan diambil';
    }
    else if (this.props.type == 'on-progress') {
      return 'Sedang Berlangsung';
    }
    else if (this.props.type == 'delivery') {
      return 'Akan diantar';
    }
    else if (this.props.type == 'complaint') {
      return 'Komplain';
    }
  }

  getContentComponent(item){
    if (this.props.type == 'incoming') {
      return (
        <IncomingContent
          item={item}
          {...this.props}
        />
      );
    }
    else if (this.props.type == 'pick-up') {
      return (
        <PickupContent
          item={item}
          {...this.props}
        />
      );
    }
    else if (this.props.type == 'on-progress') {
      return (
        <InProgressContent
          item={item}
          {...this.props}
        />
      );
    }
    else if (this.props.type == 'delivery') {
      return (
        <DeliveryContent
          item={item}
          {...this.props}
        />
      );
    }
    else if (this.props.type == 'complaint') {
      return (
        <ComplaintContent
          item={item}
          {...this.props}
        />
      );
    }
  }

  render(){
      try{
          return (
            <LayoutPrimary
                showTitle={true}
                showBackButton={true}
                showNotificationButton={true}
                showSearchButton={true}
                navigator={this.props.navigator}
                titleText={this.getTitle()}
              >
                
                  <FlatList
                      data={this.getData()}
                      keyExtractor={(item, index) => index+''}
                      renderItem={(items) => {
                        let {item} = items;
                        return this.getContentComponent(item);
                      }}
                  /> 
            </LayoutPrimary>
          );
      }
      catch(error){
        console.log(error);
        return null;
      }
      
  }
}

class IncomingContent extends PureComponent{

  _getCourier(courier){
    let result = [];
    for(let index in courier){
      let obj = {};
      obj.id = index;
      obj.info = courier[index];

      result.push(obj);
      console.log(result);
    }
    return result;
  }

  _onPressAcceptIncoming(){
      actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeC', {
        containerStyle:{
            height:Dimensions.get('window').height * 0.6,
          },
          data:{
            titleText:'Pilih Alasan Penolakan',
            type:'reject-reason',
            buttonPrimaryText:'Tolak',
            onPressButtonPrimary:(rejectReasonID, rejectReasonInfo) =>{
              if (!rejectReasonID) {
                alert('Pilih kurir terlebih dahulu')
              }
              else{
                // actionNavigator.dismissLightBox();
              
                // setTimeout(() => {
                //   actionOrder.setIncomingResponse(false, null, order_id , rejectReasonID);
                // },500);
              }
              
            }
          },
      });
  }

  render(){
    try{
      let {item, actionNavigator, actionOrder} = this.props;
      
      let {order_id, order_status, order_status_laundry, pickup_date, pickup_time, pickup_location, price, payment_method, courier} = item;
    return(
      <Box>
            <View style={{flex:0.25, flexDirection:'row'}}>
              <View
                style={{
                  justifyContent:'center',
                  flex:0.5,
                }}
              >
                <TextWrapper type={'base'}>{DateFormat(new Date(pickup_date), 'fullDate')}</TextWrapper>
                <TextWrapper type={'base'}>{ pickup_time}</TextWrapper>
              </View>

              <View
                style={{
                  flex:0.5,
                  alignItems:'flex-end',
                }}
              >
                <TextWrapper type={'base'}>{payment_method + ' - ' + toCurrency(price, 'Rp ')}</TextWrapper>
              </View>
            </View>
            <View style={{flex:0.5, justifyContent:'center'}}>
                <TextWrapper type={'smallercontent'} style={{textAlign:'center'}}>{pickup_location}</TextWrapper>
            </View>
             <View style={{flex:0.25, justifyContent:'space-evenly', flexDirection:'row'}}>
                <Button.TypeA
                  buttonText={'Tolak'}
                  buttonColor={CONSTANT.COLOR.WHITE_A}
                  containerStyle={{
                    height:Dimensions.get('window').height * 0.05,
                  }}
                  buttonStyle={{
                    height:Dimensions.get('window').height * 0.04,
                    width:Dimensions.get('window').width * 0.3,
                    borderColor:CONSTANT.COLOR.GRAY_B,
                    borderWidth:2,
                  }}
                  buttonTextStyle={{
                    color:CONSTANT.COLOR.GRAY_B,
                  }}
                  onPress={() => {
                    actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeC', {
                    containerStyle:{
                        height:Dimensions.get('window').height * 0.6,
                      },
                      data:{
                        titleText:'Pilih Alasan Penolakan',
                        type:'reject-reason',
                        buttonPrimaryText:'Pilih',
                        onPressButtonPrimary:(rejectReasonID, rejectReasonInfo) =>{

                          actionNavigator.dismissLightBox();
                          
                          setTimeout(() => {
                            actionOrder.setIncomingResponse(false, null, order_id , rejectReasonID);
                          },500);
                        }
                      },
                    });
                  }}
                />
                
                <Button.TypeA
                  buttonText={'Terima'}
                  buttonColor={CONSTANT.COLOR.BLUE_A}
                  containerStyle={{
                    height:Dimensions.get('window').height * 0.05,
                  }}
                  buttonStyle={{
                    height:Dimensions.get('window').height * 0.04,
                    width:Dimensions.get('window').width * 0.3,
                  }}
                  
                  onPress={() => {
                    actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeC', {
                    containerStyle:{
                        height:Dimensions.get('window').height * 0.6,
                      },
                      data:{
                        titleText:'Pilih Kurir',
                        type:'custom',
                        itemList:this._getCourier(courier),
                        buttonPrimaryText:'Pilih',
                        onPressButtonPrimary:(courierID, courierInfo) =>{

                          actionNavigator.dismissLightBox();
                          
                          setTimeout(() => {
                            actionOrder.setIncomingResponse(true, courierID, order_id , null);
                          },500);
                        }
                      },
                    });
                  }}
                />

            </View>

      </Box>
    );
    }
    catch(error){
      return null;
    }
  }
}

class PickupContent extends PureComponent{

  onPress(item = this.props.item){
      this.props.actionOrder.setOrderActive(item);
      this.props.actionOrder.setShipmentActivePickup(item);
      this.props.actionOrder.setShipmentActiveDelivery(item);
      this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.OrderContent', {})
  }

  getShipmentPickup(){
    let {item} = this.props;

    let {shipments} = item;

    for(let i in shipments){
      let {shipmenttype} = shipments[i];

      if (shipmenttype.id == CONSTANT.SHIPMENT_TYPE.PICKUP) {
        return shipments[i];
      }
    }
  }

  render(){
    try{
    let {item} = this.props;
    let {customer, orderlaundry, order_hash} = item;

    let {user} = customer;
    let {name} = user;

    let {orderstatuslaundry} = orderlaundry;
    let orderStatusLaundryID = orderstatuslaundry.id;

    let shipment = this.getShipmentPickup();
    let {shipmentschedule, shipment_status_id} = shipment;
    let {location_address, shipment_time_info, shipment_date} = shipmentschedule;

    return(
      <View
      >
        <Box
          onPress={this.onPress.bind(this)}
        >
              <View
                  style={{
                    position:'absolute',
                    right:-5,
                    top:-5,
                    //width:moderateScale(25),
                    //height:moderateScale(25),
                    borderRadius:moderateScale(25/2),
                    backgroundColor:CONSTANT.COLOR.BLUE_A,
                    alignItems:'center',
                    justifyContent:'center'
                  }}
              >

                <OrderStatus
                    orderStatus={orderStatusLaundryID}
                    shipmentStatus={shipment_status_id}
                />

              </View>
              <View style={{flex:0.35, flexDirection:'row', alignItems:'center'}}>
                <View
                  style={{
                    justifyContent:'center',
                  }}
                >
                  <TextWrapper type={'base'}>{order_hash}</TextWrapper>
                  <TextWrapper type={'base'}>{DateFormat(shipment_date, 'fullDate')}</TextWrapper>
                  <TextWrapper type={'base'}>{shipment_time_info}</TextWrapper>

                </View>
              </View>
              <View style={{flex:0.65, marginBottom:moderateScale(10),justifyContent:'center'}}>
                  <View style={styleContent.itemRow}>
                      <View style={[styleContent.itemRowTitle, {flex:0.20}]}>
                        <TextWrapper type={'base'}>
                        Nama
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowColon}>
                        <TextWrapper type={'base'}>
                        :
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowContent}>
                        <TextWrapper type={'base'}>
                        {name}
                        </TextWrapper>
                      </View>
                    </View>

                    <View style={styleContent.itemRow}>
                      <View style={[styleContent.itemRowTitle, {flex:0.20}]}>
                        <TextWrapper type={'base'}>
                        Tempat
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowColon}>
                        <TextWrapper type={'base'}>
                        :
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowContent}>
                        <TextWrapper type={'base'}>
                        {location_address}
                        </TextWrapper>
                      </View>
                    </View>
              </View>

        </Box>
      </View>
    );
    }
    catch(error){
        console.log(error, 'order_list.PickupContent');
        return null;
    }
  }
}

class DeliveryContent extends PureComponent{
  onPress(item = this.props.item){
      this.props.actionOrder.setOrderActive(item);
      this.props.actionOrder.setShipmentActivePickup(item);
      this.props.actionOrder.setShipmentActiveDelivery(item);
      this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.OrderContent', {})
  }

  getShipmentDelivery(){
    let {item} = this.props;

    let {shipments} = item;

    for(let i in shipments){
      let {shipmenttype} = shipments[i];

      if (shipmenttype.id == CONSTANT.SHIPMENT_TYPE.DELIVERY) {
        return shipments[i];
      }
    }
  }

  render(){
    try{
    let {item} = this.props;
    
    let {customer, orderlaundry, order_hash} = item;

    let {user} = customer;
    let {name} = user;

    let {orderstatuslaundry} = orderlaundry;
    let orderStatusLaundryID = orderstatuslaundry.id;

    let shipment = this.getShipmentDelivery();
    let {shipmentschedule, shipment_status_id} = shipment;
    let {location_address, shipment_time_info, shipment_date} = shipmentschedule;

    return(
      <View
      >
        <Box
          onPress={this.onPress.bind(this)}
        >
              <View
                  style={{
                    position:'absolute',
                    right:-5,
                    top:-5,
                    //width:moderateScale(25),
                    //height:moderateScale(25),
                    borderRadius:moderateScale(25/2),
                    backgroundColor:CONSTANT.COLOR.BLUE_A,
                    alignItems:'center',
                    justifyContent:'center'
                  }}
              >

                <OrderStatus
                    orderStatus={orderStatusLaundryID}
                    shipmentStatus={shipment_status_id}
                />

              </View>
              <View style={{flex:0.35, flexDirection:'row', alignItems:'center'}}>
                <View
                  style={{
                    justifyContent:'center',
                  }}
                >
                  <TextWrapper type={'base'}>{order_hash}</TextWrapper>
                  <TextWrapper type={'base'}>{DateFormat(shipment_date, 'fullDate')}</TextWrapper>
                  <TextWrapper type={'base'}>{shipment_time_info}</TextWrapper>
                  
                </View>
              </View>
              <View style={{flex:0.65, marginBottom:moderateScale(10),justifyContent:'center'}}>
                  <View style={styleContent.itemRow}>
                      <View style={[styleContent.itemRowTitle, {flex:0.20}]}>
                        <TextWrapper type={'base'}>
                        Nama
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowColon}>
                        <TextWrapper type={'base'}>
                        :
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowContent}>
                        <TextWrapper type={'base'}>
                        {name}
                        </TextWrapper>
                      </View>
                    </View>

                    <View style={styleContent.itemRow}>
                      <View style={[styleContent.itemRowTitle, {flex:0.20}]}>
                        <TextWrapper type={'base'}>
                        Tempat
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowColon}>
                        <TextWrapper type={'base'}>
                        :
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowContent}>
                        <TextWrapper type={'base'}>
                        {location_address}
                        </TextWrapper>
                      </View>
                    </View>
              </View>

        </Box>
      </View>
    );
    }
    catch(error){
        console.log(error, 'order_list.PickupContent');
        return null;
    }
  }
}

class ComplaintContent extends PureComponent{

  onPress(){
      try{
        let {actionOrder, actionComplaint, item} = this.props;
        actionComplaint.setComplaintMessageList(item.complaint.messages);
        this.props.actionOrder.setOrderActive(item, CONSTANT.ORDER_ACTIVE_TYPE.COMPLAINT);
        this.props.actionOrder.setShipmentActivePickup(null);
        this.props.actionOrder.setShipmentActiveDelivery(null);
        this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.OrderContent', {});
      }
      catch(error){
        console.log('--error order list complaint onpress');
      }
  }

  renderImportantSign(){
    try{
        let {item, actionComplaint} = this.props;
        let {customer, complaint, order_hash} = item;
        
        if (item.important) {
              return(
                <View
                  style={{
                    //position:'absolute',
                    right:moderateScale(10),
                    //top:-5,
                    width:moderateScale(25),
                    height:moderateScale(25),
                    borderRadius:moderateScale(25/2),
                    backgroundColor:CONSTANT.COLOR.YELLOW_A,
                    alignItems:'center',
                    justifyContent:'center'
                  }}
                >
                  <TextWrapper type={'content'} style={{color:'white'}} fontWeight={'SemiBold'}>!</TextWrapper>
                </View>
              );
            }
    }
    catch(error){

    }
  }

  render(){
    try{
        let {item} = this.props;
        let {customer, complaint, order_hash} = item;
          return(
            <View
            >
            <Box
              onPress={this.onPress.bind(this)}
            >
                <View
                  style={{
                    position:'absolute',
                    right:-5,
                    top:-5,
                    //width:moderateScale(25),
                    //height:moderateScale(25),
                    borderRadius:moderateScale(25/2),
                    alignItems:'center',
                    justifyContent:'center',
                    flexDirection:'row',
                  }}
                >

                {this.renderImportantSign()}

                <OrderStatus
                    complaintStatus={complaint.complaint_status_id}
                />

              </View>
              <View style={{flex:0.25, flexDirection:'row', alignItems:'center'}}>
                <View
                  style={{
                    justifyContent:'center',
                  }}
                >
                  <TextWrapper type={'base'}>{order_hash}</TextWrapper>
                  
                </View>
              </View>
              <View style={{flex:0.75, marginBottom:moderateScale(10),justifyContent:'center'}}>
                  <View style={styleContent.itemRow}>
                      <View style={[styleContent.itemRowTitle, {flex:0.20}]}>
                        <TextWrapper type={'base'}>
                        Nama
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowColon}>
                        <TextWrapper type={'base'}>
                        :
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowContent}>
                        <TextWrapper type={'base'}>
                        {customer.user.name}
                        </TextWrapper>
                      </View>
                    </View>

                    <View style={styleContent.itemRow}>
                      <View style={[styleContent.itemRowTitle, {flex:0.20}]}>
                        <TextWrapper type={'base'}>
                        Keluhan
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowColon}>
                        <TextWrapper type={'base'}>
                        :
                        </TextWrapper>
                      </View>

                      <View style={styleContent.itemRowContent}>
                        <TextWrapper type={'base'}>
                        {complaint.complaintreason.info}
                        </TextWrapper>
                      </View>
                    </View>
              </View>

            </Box>
            </View>
          )
    }
    catch(error){
      console.log(error, '--catch orderlist-complaintcontent');
      return null;
    }
  }
}

class Box extends PureComponent{

  render(){
    return(
      <TouchableOpacity
        {...this.props}
        style={{
          flex:1,
          minHeight:Dimensions.get('window').height * 0.2,
          borderRadius:5,
          backgroundColor:'white',
          elevation:3,
          marginBottom:moderateScale(10),
          marginHorizontal:moderateScale(2),
        }}

        onPress={() => {this.props.onPress ? this.props.onPress() : () => {}}}
      >
        <View
          style={{flex:1,
            margin:moderateScale(10)}}
        >
            {this.props.children}
        </View>
        
      </TouchableOpacity>
    );
  }
}


const styleContent = StyleSheet.create({
  titleRow:{
    flexDirection:'row', 
    alignItems:'center',
  },
  titleRowImage:{
    flex:0.1,
  },
  titleRowContent:{
    flex:0.4,
  },
  titleRowRightContent:{
    flex:0.5,
    alignItems:'flex-end',
  },
  itemRow:{
    marginTop:moderateScale(3),
    flexDirection:'row',
  },
  itemRowTitle:{
    flex:0.25,
  },
  itemRowColon:{
    flex:0.04,
  },
  itemRowContent:{
    flex:0.71,
    flexDirection:'row',
    alignItems:'center',
  },
  itemDetail:{
    paddingHorizontal:Dimensions.get('window').width * 0.025,
    paddingVertical:Dimensions.get('window').height * 0.01,
    borderColor:'#F0F0F0',
  },
});


function mapStateToProps(state, ownProps) {
  return {
    layoutHeight:state.rootReducer.layout.height,
    userRole:state.rootReducer.user.role,
    pickupOrderList:state.rootReducer.order.pickupOrderList,
    inProgressOrderList:state.rootReducer.order.inProgressOrderList,
    deliveryOrderList:state.rootReducer.order.deliveryOrderList,
    incomingOrderList:state.rootReducer.order.incomingOrderList,
    complaintOrderList:state.rootReducer.order.complaintOrderList,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionOrder: bindActionCreators(actions.order, dispatch),
    actionComplaint: bindActionCreators(actions.complaint, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderList);


