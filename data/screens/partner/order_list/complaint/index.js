import React, { Component, PureComponent } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Text,
        StyleSheet,
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from './../../../../layout';
import {TextWrapper, Button, Form, TabBar, OrderStatus, DateFormat} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

//local
import * as style from './../style';


class Complaint extends PureComponent{

  onPress(){
      try{
        let {actionOrder, item} = this.props;
        actionOrder.getComplaintMessageList(item.complaint.id);
        this.props.actionOrder.setOrderActive(this.props.item);
        this.props.actionOrder.setShipmentActivePickup(this.props.item);
        this.props.actionOrder.setShipmentActiveDelivery(this.props.item);
        this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.OrderContent', {})
      }
      catch(error){
        console.log('--error order list complaint onpress');
      }
  }

  getShipmentDelivery(){
    let {item} = this.props;

    let {shipments} = item;

    for(let i in shipments){
      let {shipmenttype} = shipments[i];

      if (shipmenttype.id == CONSTANT.SHIPMENT_TYPE.DELIVERY) {
        return shipments[i];
      }
    }
  }

  getShipmentPickup(){
    let {item} = this.props;

    let {shipments} = item;

    for(let i in shipments){
      let {shipmenttype} = shipments[i];

      if (shipmenttype.id == CONSTANT.SHIPMENT_TYPE.PICKUP) {
        return shipments[i];
      }
    }
  }

  render(){
    try{
      let {item} = this.props;
      
      let {customer, orderlaundry, order_hash} = item;

      let {user} = customer;
      let {name} = user;

      let {orderstatuslaundry, expected_duration} = orderlaundry;
      let orderStatusLaundryID = orderstatuslaundry.id;

      let shipment = this.getShipmentDelivery();
      let {shipmentschedule, shipment_status_id, internalshipment} = shipment;
      let {courier} = internalshipment;
      let {partner} = courier;
      let userCourier = partner.user;
      let {location_address, shipment_time_info, shipment_date, time} = shipmentschedule;
      let {info} = time;
      let timeDelivery2 = info.split('-')[0];
      let timeDelivery = timeDelivery2.split(':');

      let deliveryDate = new Date(shipment_date);

      deliveryDate.setHours(timeDelivery[0]);
      deliveryDate.setMinutes(timeDelivery[1]);

      let reportDate = new Date(deliveryDate);

      reportDate.setHours(timeDelivery[0] - 6);
      reportDate.setMinutes(timeDelivery[1]);

      return(
        <View
        >

        <Button.TypeC
            onPress={this.onPress.bind(this)}
        >
              <View
                  style={{
                    position:'absolute',
                    right:-5,
                    top:-5,
                    //width:moderateScale(25),
                    //height:moderateScale(25),
                    borderRadius:moderateScale(25/2),
                    backgroundColor:CONSTANT.COLOR.BLUE_A,
                    alignItems:'center',
                    justifyContent:'center'
                  }}
              >
                <OrderStatus
                    orderStatus={orderStatusLaundryID}
                    shipmentStatus={shipment_status_id}
                />
              </View>

              <View style={{flex:0.2, flexDirection:'row', alignItems:'center'}}>
                <View
                  style={{
                    justifyContent:'center',
                  }}
                >
                  <TextWrapper type={'base'}>{order_hash}</TextWrapper>
                  
                </View>
              </View>
              <View style={{flex:0.8}}>
                  <View style={style.content.itemRow}>
                      <View style={[style.content.itemRowTitle, styleInProgress.itemRowTitle]}>
                      <TextWrapper type={'base'}>
                        Pelanggan
                        </TextWrapper>
                      </View>

                      <View style={style.content.itemRowColon}>
                        <TextWrapper type={'base'}>
                        :
                        </TextWrapper>
                      </View>

                      <View style={style.content.itemRowContent}>
                        <TextWrapper type={'base'}>
                        {name}
                        </TextWrapper>
                      </View>
                    </View>

                    <View style={style.content.itemRow}>
                    <View style={[style.content.itemRowTitle, styleInProgress.itemRowTitle]}>
                        <TextWrapper type={'base'}>
                        Estimasi Selesai
                        </TextWrapper>
                      </View>

                      <View style={style.content.itemRowColon}>
                        <TextWrapper type={'base'}>
                        :
                        </TextWrapper>
                      </View>

                      <View style={style.content.itemRowContent}>
                        <TextWrapper type={'base'}>
                        {

                          DateFormat(reportDate, 'fullDateTime2')
                        }
                        </TextWrapper>
                      </View>
                    </View>

                    <View style={style.content.itemRow}>
                      <View style={[style.content.itemRowTitle, styleInProgress.itemRowTitle]}>
                        <TextWrapper type={'base'}>
                        Waktu Antar
                        </TextWrapper>
                      </View>

                      <View style={style.content.itemRowColon}>
                        <TextWrapper type={'base'}>
                        :
                        </TextWrapper>
                      </View>

                      <View style={[style.content.itemRowContent]}>
                      <TextWrapper type={'base'}>
                        {DateFormat(new Date(deliveryDate), 'fullDateTime2')}
                        </TextWrapper>
                      </View>
                    </View>

                    <View style={style.content.itemRow}>
                      <View style={[style.content.itemRowTitle, styleInProgress.itemRowTitle]}>
                      <TextWrapper type={'base'}>
                        Kurir Antar
                        </TextWrapper>
                      </View>

                      <View style={style.content.itemRowColon}>
                        <TextWrapper type={'base'}>
                        :
                        </TextWrapper>
                      </View>

                      <View style={style.content.itemRowContent}>
                        <TextWrapper type={'base'}>
                          {partner.ktp_name}
                        </TextWrapper>
                      </View>
                    </View>
              </View>

        </Button.TypeC>

        </View>
      );
    }
    catch(error){
        console.log(error);
        return null;
    }
  }
}

const styleInProgress = StyleSheet.create({
	itemRowTitle:{flex:0.35},
});

function mapStateToProps(state, ownProps) {
  return {
    layoutHeight:state.rootReducer.layout.height,
    userRole:state.rootReducer.user.role,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionOrder: bindActionCreators(actions.order, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Complaint);

