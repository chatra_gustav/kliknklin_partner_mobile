import React, { Component, PureComponent } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        StyleSheet,
        Image,
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from './../../../../layout';
import {TextWrapper, Text, Button, Form, TabBar, OrderStatus, DateFormat, Scaling} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

//local
import * as style from './../style';
import OrderDateTab from './../order_date_tab';
import TimeSlot from './../time_slot';
import DateSlot from './../date_slot';
import BaseItem from './../base_item';
import OrderTypeTab from './../order_type_tab';


class NoOrderContent extends PureComponent{
  static defaultProps = {
    data: [],
  }

  render(){
    return(
      <View
        style={{
          flex:1,
          alignItems:'center',
        }}
      >
          <Text
            textStyle={{
              textAlign:'center',
              marginTop:Scaling.verticalScale(50),
            }}
            fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}
          >{'Tidak ada pesanan yang ditemukan, \n jangan lupa untuk terus \n meningkatkan kualitas layanan anda'} </Text>
          <Image
              source={CONSTANT.IMAGE_URL.NO_ORDER_FOUND}
              style={{
                width:Scaling.scale(300),
                height:Scaling.verticalScale(300),
                resizeMode:'contain',
              }}
          /> 
      </View>
    );
  }
}


function mapStateToProps(state, ownProps) {
  return {
    selectedOrderDate:state.rootReducer.order.selectedOrderDate,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NoOrderContent);

