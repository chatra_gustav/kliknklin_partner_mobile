import React, { Component, PureComponent } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        StyleSheet,
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from './../../../../layout';
import {TextWrapper, Text, Button, Form, TabBar, OrderStatus, DateFormat, Scaling} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

//local
import * as style from './../style';
import OrderDateTab from './../order_date_tab';
import TimeSlot from './../time_slot';
import DateSlot from './../date_slot';
import BaseItem from './../base_item';
import OrderTypeTab from './../order_type_tab';
import OrderDateRange from './../order_date_range';
import NoOrderContent from './../no_order_content';


class HistoryContent extends PureComponent{
  static defaultProps = {
    data: [],
  }

  componentWillMount(){
    this.props.actionOrder.setOrderListTypeTab(0);
  }

    getContent(){
    let {orderListTypeTab, onlineList, posList} = this.props;

    if (orderListTypeTab == 0 && onlineList.length != 0) {
      return(
        <FlatList
            contentContainerStyle={{
              paddingBottom:Scaling.verticalScale(300),
            }}
            initialNumToRender={4}
            data={onlineList}
            keyExtractor={(item, index) => index+''}
            renderItem={({item, index}) => {
              return <DateSlot {...this.props} item={item} 
                  type={'createdAt'}
                  content={(items, key)=>{
                    return(
                        <Items
                          {...this.props}
                          item={items}
                          key={key}
                        />
                    )
                  }}
                  key={index}
              />;
            }}
        /> 
      );
    }
    else if (orderListTypeTab == 1 && posList != 0) {
      return(
        <FlatList
            contentContainerStyle={{
                paddingBottom:Scaling.verticalScale(150),
            }}
            initialNumToRender={3}
            data={posList}
            keyExtractor={(item, index) => index+''}
            renderItem={({item, index}) => {
              return <DateSlot {...this.props} item={item} 
                  type={'createdAt'}
                  content={(items, key)=>{
                    return(
                        <Items
                          {...this.props}
                          item={items}
                          key={key}
                        />
                    )
                  }}
                  key={index}
              />;
            }}
        /> 
      );
    }
    else{
      return(
        <NoOrderContent />
      );
    }
  }

  render(){
    let {lateDeliveryOrderList} = this.props;
    return(
      <View>
          <OrderDateRange />

          <OrderTypeTab
            showImportant={lateDeliveryOrderList.length == 0 ? false : true}
            item={
              [
                {
                  itemText:'Online',
                },
                {
                  itemText:'POS',
                },
              ]
            }
          />

          {this.getContent()}
      </View>
    );
  }
}

class Items extends PureComponent{
  componentDidMount(){
  }
  render(){
    try{
      let {item} = this.props;
      let {customer, orderlaundry, order_hash, pricedetail, orderitem} = item;

      let {user} = customer;
      let {name} = user;

      let {orderstatuslaundry} = orderlaundry;
      let orderStatusLaundryID = orderstatuslaundry.id;

      let shipment = item.selectedShipment;
      let {shipmentschedule, shipment_status_id, internalshipment} = shipment;
      let {location_address, shipment_time_info, shipment_date} = shipmentschedule;
      
      let userCourier = null;

      if (internalshipment) {
        let {courier} = internalshipment;
        let {partner} = courier;
        userCourier = partner.ktp_name;
      }

      return(
        <BaseItem
          navigator={this.props.navigator}
          item={item}
          customerName={name}
          logOrderPrice={pricedetail}
          orderItem={orderitem}
          orderHash={order_hash}
          orderStatusLaundryID={orderStatusLaundryID}
          shipmentStatus={shipment_status_id}
          data={[
            {
              category:'Pelanggan',
              content:name,
            },
            {
              category:'Kurir Antar',
              content:userCourier ? userCourier : 'Ambil Sendiri',
            },
          ]}
        />
      );
    }
    catch(error){
        console.log(error, 'orderlist.in_progress_content');
        return null;
    }
  }
}

function mapStateToProps(state, ownProps) {
  return {
    deliveryOrderList:state.rootReducer.order.deliveryOrderList,
    lateDeliveryOrderList:state.rootReducer.order.lateDeliveryOrderList,
    selectedOrderDate:state.rootReducer.order.selectedOrderDate,
    orderListTypeTab:state.rootReducer.order.orderListTypeTab,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionOrder: bindActionCreators(actions.order, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HistoryContent);

