import React, { Component, PureComponent } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        StyleSheet,
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from './../../../../layout';
import {TextWrapper, Text, Button, Form, TabBar, OrderStatus, DateFormat, Scaling, toCurrency} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

//local
import * as style from './../style';
import OrderDateTab from './../order_date_tab';
import TimeSlot from './../time_slot';
import DateSlot from './../date_slot';
import BaseItem from './../base_item';
import OrderTypeTab from './../order_type_tab';
import NoOrderContent from './../no_order_content';


class IncomingContent extends PureComponent{
  static defaultProps = {
    data: [],
  }

  componentWillMount(){
    this.props.actionOrder.setOrderListTypeTab(this.props.tabBarIndex ? this.props.tabBarIndex : 0);
  }

    getContent(){
    let {orderListTypeTab, onlineList, posList} = this.props;

    if (orderListTypeTab == 1) {
      return(
          posList.length == 0 ? 
            <NoOrderContent />
            :
            <FlatList
              contentContainerStyle={{
                paddingBottom:Scaling.verticalScale(300),
                paddingTop:Scaling.verticalScale(20),
              }}
              initialNumToRender={4}
              data={posList[0].data}
              keyExtractor={(item, index) => index+''}
              renderItem={({item, index}) => {
                return(
                    <Items
                      {...this.props}
                      item={item}
                      key={index}
                      type={'incoming-pos'}
                    />
                )
              }}
          /> 
        
      );
    }
    else if (orderListTypeTab == 0) {
      console.log(onlineList)
      return(
        onlineList.length == 0 ?
        <NoOrderContent/>
          :
        <FlatList
            contentContainerStyle={{
                paddingBottom:Scaling.verticalScale(150),
            }}
            initialNumToRender={3}
            data={onlineList}
            keyExtractor={(item, index) => index+''}
            renderItem={({item, index}) => {
              return <DateSlot {...this.props} item={item} 
                  content={(items, key)=>{
                    return(
                        <Items
                          {...this.props}
                          item={items}
                          key={key}
                          type={'incoming-online'}
                        />
                    )
                  }}
                  key={index}
              />;
            }}
        /> 
      );
    }
  }

  render(){
    return(
      <View>
          <OrderTypeTab
            item={
              [
                {
                  itemText:'Online',
                },
                {
                  itemText:'POS',
                },
              ]
            }
          />

          {this.getContent()}
      </View>
    );
  }
}

class Items extends PureComponent{
  componentDidMount(){
  }
  _getCourier(courier){
    let result = [];
    for(let index in courier){
      let obj = {};
      obj.id = index;
      obj.info = courier[index];

      result.push(obj);
    }
    return result;
  }
  render(){
    try{
      let {item, type, rejectOrderReasonList} = this.props;
      let {order, pickup_location, online_flag,price, order_id, courier, customer, order_laundry_status_id, price_detail,order_item, order_hash, pickup_date, pickup_time, delivery_date, delivery_time} = item;
      let {user} = customer;

      let locationAddress;

      if (online_flag) {
        locationAddress = pickup_location;
      }

      let name = user.name;
      let orderStatusLaundryID = online_flag ? order_laundry_status_id : 15;
      let deliveryDate=delivery_date ? delivery_date : new Date();
      let deliveryTime=delivery_time ? delivery_time : '00:00-01:00';
      let pickupDate=pickup_date? pickup_date : new Date();
      let pickupTime=pickup_time? pickup_time : '00:00-01:00';

      let courierList = this._getCourier(courier);

      let dataPOS = [
            {
              category:'Pelanggan',
              content:name,
            },
            {
              category:'Biaya Pesanan',
              content:toCurrency(price, 'Rp'),
            }
      ];
      let dataOnline = [
            {
              category:'Biaya Pesanan',
              content:toCurrency(price, 'Rp'),
            },
            {
              category:'Tempat',
              content:locationAddress,
            },
      ]
      let data = online_flag ? dataOnline : dataPOS;

      return(
        <BaseItem
          type={type}
          navigator={this.props.navigator}
          item={item}
          customerName={name}
          logOrderPrice={price_detail}
          isOnlineOrder={online_flag}
          orderItem={order_item}
          orderHash={order_hash ? order_hash : '-'}
          orderStatusLaundryID={orderStatusLaundryID}
          deliveryDate={deliveryDate}
          deliveryTime={deliveryTime}
          pickupDate={pickupDate}
          pickupTime={pickupTime}
          locationAddress={locationAddress}
          promo={order.appliedpromo}
          //shipmentStatus={shipment_status_id}
          data={data}
          submitButton={() => {
            if (online_flag) {
              return(
                <View
                  style={{
                    flexDirection:'row',
                    width:'100%',
                    justifyContent:'space-evenly',
                    //backgroundColor:'red'
                  }}
                >
                  <Button.Standard
                      buttonSize={CONSTANT.STYLE.BUTTON.THICKER_EXTRA_MEDIUM}
                      buttonText={'SEDANG PENUH'}
                      buttonColor={CONSTANT.COLOR.GRAY}
                      onPress={() => {
                        this.props.actionNavigator.showLightBox('PartnerKliknKlin.ItemSelection', {
                          data:rejectOrderReasonList,
                          categoryToFind:'info',
                          titleBar:true,
                          title:'Pilih Alasan Penolakan',
                          onPressButtonPrimary:(selectedItemIndex) =>{
                            this.props.actionNavigator.dismissLightBox();
                                                
                            setTimeout(() => {
                              this.props.actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeA',{
                                  title:'konfirmasi', 
                                  message: 'Akan dibatalkan dengan alasan : \n' + rejectOrderReasonList[selectedItemIndex].info, 
                                  buttonPrimaryText: 'ya', 
                                  buttonSecondaryText: 'batal', 
                                  onPressButtonPrimary: () => {
                                    this.props.actionNavigator.dismissLightBox();

                                    setTimeout(() => {
                                      this.props.actionOrder.setIncomingResponse(false, null, order_id , rejectOrderReasonList[selectedItemIndex].id);
                                    }, 350)
                                  }, 
                                  onPressButtonSecondary: () =>{
                                    this.props.actionNavigator.dismissLightBox();
                                  },
                              });
                            },350);
                          }
                        });
                      }}
                  />

                  <Button.Standard
                      buttonSize={CONSTANT.STYLE.BUTTON.THICKER_EXTRA_MEDIUM}
                      buttonText={'TERIMA'}
                      buttonColor={CONSTANT.COLOR.LIGHT_BLUE}
                      onPress={() => {
                        this.props.actionNavigator.showLightBox('PartnerKliknKlin.ItemSelection', {
                          data:courierList,
                          categoryToFind:'info',
                          titleBar:true,
                          title:'Pilih Kurir',
                          onPressButtonPrimary:(selectedItemIndex) => {
                            this.props.actionNavigator.dismissLightBox();
                              
                            setTimeout(() => {
                              this.props.actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeA',{
                                  title:'konfirmasi', 
                                  message: 'Kurir yang dipilih : \n' + courierList[selectedItemIndex].info, 
                                  buttonPrimaryText: 'ya', 
                                  buttonSecondaryText: 'batal', 
                                  onPressButtonPrimary: () => {
                                    this.props.actionNavigator.dismissLightBox();
                                    
                                    setTimeout(() => {
                                      this.props.actionOrder.setIncomingResponse(true, courierList[selectedItemIndex].id, order_id , null);
                                    }, 350)
                                  }, 
                                  onPressButtonSecondary: () =>{
                                    this.props.actionNavigator.dismissLightBox();
                                  },
                              });
                            },350);
                          }
                        });
                      }}
                  />
                </View>
              );
            }
          }}
        />
      );
    }
    catch(error){
        console.log(error, 'orderlist.incoming_content');
        return null;
    }
  }
}

function mapStateToProps(state, ownProps) {
  return {
    deliveryOrderList:state.rootReducer.order.deliveryOrderList,
    lateDeliveryOrderList:state.rootReducer.order.lateDeliveryOrderList,
    selectedOrderDate:state.rootReducer.order.selectedOrderDate,
    orderListTypeTab:state.rootReducer.order.orderListTypeTab,
    rejectOrderReasonList:state.rootReducer.masterData.rejectOrderReasonList,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionOrder: bindActionCreators(actions.order, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(IncomingContent);

