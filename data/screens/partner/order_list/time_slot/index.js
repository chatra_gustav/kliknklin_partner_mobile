import React, { Component, PureComponent } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        StyleSheet,
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';

import {TextWrapper, Text, Button, Form, TabBar, OrderStatus, DateFormat, Scaling} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class TimeSlot extends Component{
  static defaultProps = {
    content:() => {
      return(<View/>)
    },
  }

  _getTimeInfo(){
    let {item} = this.props;

    let {order_time_info} = item;

    let timeInfo = order_time_info.split('-');
    if (timeInfo.length == 2) {
      return timeInfo[0] + ' - ' + timeInfo[1];
    }
    return order_time_info;
  }

  render(){
    let {item, content} = this.props;

    let {order_time_info, logschedule, shipment_id} = item;
    return(
      <View 
        style={timeSlotStyle.container} 
      >
        <View style={timeSlotStyle.titleSection}>
            <Text fontOption={CONSTANT.TEXT_OPTION.HEADER} textStyle={timeSlotStyle.timeSlotText}>
              {item.time}
            </Text>
        </View>

        <View style={timeSlotStyle.contentSection}>
            <FlatList
              data={item.data}
              keyExtractor={(item, index) => index+''}
              renderItem={({item, index}) => { 
                return content(item, index);
              }}
            />
        </View>
        
        <View style={timeSlotStyle.floating}>
          <View style={timeSlotStyle.circle} />
          <View style={[timeSlotStyle.line, {
            height:(Scaling.verticalScale(200) * item.data.length) + (Scaling.verticalScale(10) * item.data.length - 1),
          }]} />
        </View>
      </View>
    );
  }
}

const timeSlotStyle = StyleSheet.create({
  container: {
    flex: 1,
    marginTop:Scaling.verticalScale(20),
  },
  titleSection:{
    height:Scaling.verticalScale(40),
    left:'2.5%',
    marginLeft:Scaling.scale(5),
  },
  contentSection:{
    justifyContent:'center',
  },
  line:{
    borderRightWidth:Scaling.moderateScale(2),
    borderColor:CONSTANT.COLOR.GRAY,
    zIndex:10,
  },
  circle:{
    width:Scaling.scale(10),
    height:Scaling.scale(10),
    borderRadius:Scaling.moderateScale(50),
    backgroundColor:CONSTANT.COLOR.GRAY,
    zIndex:10,
  },
  floating:{
    position:'absolute',
    left:'5.5%',
    alignItems:'center',
    height:'100%',
    top:Scaling.verticalScale(25),
  },
  timeSlotText:{
    top:moderateScale(-3),
  }
});

function mapStateToProps(state, ownProps) {
  return {
    inProgressOrderList:state.rootReducer.order.inProgressOrderList,
    selectedOrderDate:state.rootReducer.order.selectedOrderDate,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionOrder: bindActionCreators(actions.order, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TimeSlot);


