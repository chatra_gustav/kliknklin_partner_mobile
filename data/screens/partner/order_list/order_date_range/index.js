import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';

import {TabBar, Scaling, DateFormat, Text} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class OrderDateRange extends Component{
  constructor(props) {
    super(props);
  
    this.state = {
    };
  }

  render(){
    let {fromDate, toDate} = this.props.historyOrderDate;
    return(
      <View
        style={{
          width:'100%',
          alignItems:'center',
          marginTop:Scaling.verticalScale(10),
        }}
      >
        <TouchableOpacity
          style={{
            width:'80%',
            height:Scaling.verticalScale(40),
            elevation:2,
            backgroundColor:'#fff',
            borderColor:'lightgray',
            borderRadius:Scaling.moderateScale(5),
            flexDirection:'row',
          }}

          onPress={() => {
            this.props.actionNavigator.showLightBox(CONSTANT.ROUTE_TYPE.DATE_RANGE_PICKER,{
        
                onPressButtonPrimary: async (fromDate, toDate) => {
                  this.props.actionNavigator.dismissLightBox();
                  
                  this.props.actionOrder.setHistoryOrderDate(fromDate, toDate);
                },
                onPressButtonSecondary: () =>{
                  this.props.actionNavigator.dismissLightBox();
                },
            })
          }}
        >

          <View
            style={{
              flex:0.5,
              alignItems:'center',
            }}
          >
            <View
              style={{

              }}
            >
              <Text
                fontOption={CONSTANT.TEXT_OPTION.SUB_CONTENT}
              >{'Dari Tanggal'}</Text>
            </View>
              <Text
                fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}
              >{DateFormat(fromDate, 'fullDate')}</Text>
          </View>

          <View
            style={{
              height:'100%',
              justifyContent:'center',
            }}
          >
            <View
              style={{
                width:Scaling.scale(10),
                borderColor:CONSTANT.COLOR.GRAY,
                borderWidth:Scaling.moderateScale(0.5),
              }}
            />
          </View>
          

          <View
            style={{
              flex:0.5,
              alignItems:'center',
            }}
          >
            <View
              style={{

              }}
            >
              <Text
                fontOption={CONSTANT.TEXT_OPTION.SUB_CONTENT}
              >{'Sampai Tanggal'}</Text>
            </View>
              <Text
                fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}
              >{DateFormat(toDate, 'fullDate')}</Text>
          </View>

        </TouchableOpacity>
      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    orderListTypeTab: state.rootReducer.order.orderListTypeTab,
    historyOrderDate: state.rootReducer.order.historyOrderDate,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionOrder: bindActionCreators(actions.order, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderDateRange);