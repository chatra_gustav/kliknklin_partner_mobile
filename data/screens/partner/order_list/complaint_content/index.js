import React, { Component, PureComponent } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        StyleSheet,
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from './../../../../layout';
import {TextWrapper, Text, Button, Form, TabBar, OrderStatus, DateFormat, Scaling} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

//local
import * as style from './../style';
import OrderDateTab from './../order_date_tab';
import TimeSlot from './../time_slot';
import DateSlot from './../date_slot';
import BaseItem from './../base_item';
import OrderTypeTab from './../order_type_tab';
import OrderDateRange from './../order_date_range';
import NoOrderContent from './../no_order_content';


class ComplaintContent extends PureComponent{
  static defaultProps = {
    onlineList:[],
    posList:[],
  }

  componentWillMount(){
    this.props.actionOrder.setOrderListTypeTab(0);
  }

    getContent(){
    let {orderListTypeTab, onlineList, posList} = this.props;

    if (orderListTypeTab == 0 && onlineList.length != 0) {
      return(
        <FlatList
            contentContainerStyle={{
              paddingBottom:Scaling.verticalScale(300),
            }}
            initialNumToRender={4}
            data={onlineList}
            keyExtractor={(item, index) => index+''}
            renderItem={({item, index}) => {
              return <DateSlot {...this.props} item={item} 
                  type={'createdAt'}
                  content={(items, key)=>{
                    return(
                        <Items
                          {...this.props}
                          item={items}
                          key={key}
                        />
                    )
                  }}
                  key={index}
              />;
            }}
        /> 
      );
    }
    else if (orderListTypeTab == 1 && posList != 0) {
      return(
        <FlatList
            contentContainerStyle={{
                paddingBottom:Scaling.verticalScale(150),
            }}
            initialNumToRender={3}
            data={posList}
            keyExtractor={(item, index) => index+''}
            renderItem={({item, index}) => {
              return <DateSlot {...this.props} item={item} 
                  type={'createdAt'}
                  content={(items, key)=>{
                    return(
                        <Items
                          {...this.props}
                          item={items}
                          key={key}
                        />
                    )
                  }}
                  key={index}
              />;
            }}
        /> 
      );
    }
    else{
      return(
        <NoOrderContent />
      );
    }
  }

  render(){
    let {lateDeliveryOrderList} = this.props;
    return(
      <View>
          <OrderTypeTab
            showImportant={lateDeliveryOrderList.length == 0 ? false : true}
            item={
              [
                {
                  itemText:'Online',
                },
                {
                  itemText:'POS',
                },
              ]
            }
          />

          {this.getContent()}
      </View>
    );
  }
}

class Items extends PureComponent{
  componentDidMount(){
  }
  render(){
    try{
      let {item} = this.props;
      let {customer, complaint, order_hash} = item;
      let {user} = customer;
      let {complaintreason, items, complaint_status_id, complaint_details} = complaint;
      
      let name = user.name;
      return(
        <BaseItem
          type={'complaint'}
          navigator={this.props.navigator}
          item={item}
          customerName={name}
          orderItem={items}
          orderHash={order_hash}
          complaintStatusID={complaint_status_id}
          problemDetails={complaint_details}
          data={[
            {
              category:'Pelanggan',
              content:name,
            },
            {
              category:'Keluhan',
              content:complaintreason.info,
            },
          ]}
        />
      );
    }
    catch(error){
        console.log(error, 'orderlist.in_progress_content');
        return null;
    }
  }
}

function mapStateToProps(state, ownProps) {
  return {
    deliveryOrderList:state.rootReducer.order.deliveryOrderList,
    lateDeliveryOrderList:state.rootReducer.order.lateDeliveryOrderList,
    selectedOrderDate:state.rootReducer.order.selectedOrderDate,
    orderListTypeTab:state.rootReducer.order.orderListTypeTab,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionOrder: bindActionCreators(actions.order, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ComplaintContent);

