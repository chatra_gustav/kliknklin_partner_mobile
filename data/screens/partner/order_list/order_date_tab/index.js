import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image
} from 'react-native';

import {TextWrapper, DateFormat, TabBar, Scaling} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from "./../../../../actions";

class OrderDateTab extends Component{
  constructor(props) {
    super(props);
  
    this.state = {
      startDate:new Date(),
      currentTab:0,
      maxLeftTab:1,
      minLeftTab:-1,
      scheduleDateList:[],
    };
  }
  componentDidMount(){
    this.getListOfScheduleDate();
  }


  componentDidUpdate(prevProps, prevState){
    if (prevProps.orderListData != this.props.orderListData) {
      this.getListOfScheduleDate()
    }
  }

  setStartDate(){

  }

  onPressLeftArrow(){
    let {actionOrder} = this.props;
    let {currentTab, minLeftTab} = this.state;
    if (currentTab > minLeftTab) {
      this.setState({currentTab: currentTab - 1}, () => {
        let date = new Date()
        let nextDate = new Date(date.setDate(date.getDate() + (7 * this.state.currentTab)));
        this._dateTab.onPress(0, nextDate);
        this.getListOfScheduleDate(nextDate);
      });
    }
  }

  onPressRightArrow(){
    let {actionOrder} = this.props;
    let {currentTab, maxLeftTab} = this.state;
    if (currentTab < maxLeftTab) {
      this.setState({currentTab: currentTab + 1}, () => {
        let date = new Date()
        let nextDate = new Date(date.setDate(date.getDate() + (7 * this.state.currentTab)));
        this._dateTab.onPress(0, nextDate);
        this.getListOfScheduleDate(nextDate);
      });
    }
  }

  getListOfScheduleDate(startDate = new Date()){
      var scheduleDateList = [];
      let data = this.props.orderListData;

      for (var i = 0; i < 7; i++) {
        let date = new Date(startDate);
        date = new Date(date.setDate(date.getDate() + i));
        
        let isImportant = false;
        for(let j in data){
          let tabDate = new Date(data[j].date).toDateString();
          let dataDate = new Date(date).toDateString();
          if (new Date(tabDate).getTime() == new Date(dataDate).getTime()) {
            isImportant = true;
          }
        }

        scheduleDateList.push({
          date,
          isImportant
        });
      }
      
      this.setState({scheduleDateList});
  }

  onPress(key, dates){
    try{
      let {actionOrder} = this.props;
      actionOrder.setSelectedOrderDate(dates);
    }
    catch(error){
      console.log(error);
    }
  }

  _renderLeftArrow(){

  }

  render(){
    let renderLeftArrow = this.state.currentTab > this.state.minLeftTab ? true : false;
    let renderRightArrow = this.state.currentTab < this.state.maxLeftTab ? true : false;
    return(
      <View
        style={{
          justifyContent:'center',
          flexDirection:'row',
        }}
      >
        <TouchableOpacity
          style={{
            flex:1,
            justifyContent:'center',
            alignItems:'center',
          }}
          disabled={!renderLeftArrow}
          onPress={this.onPressLeftArrow.bind(this)}
        >
          {renderLeftArrow ?
            <Image
                source={CONSTANT.IMAGE_URL.LEFT_ARROW_ICON}
                style={[{
                  width:Scaling.scale(30),
                  height:Scaling.verticalScale(30),
                  resizeMode:'contain',
                  tintColor:CONSTANT.COLOR.GRAY,
                },
                ]}
            />
            :null
          }
           
        </TouchableOpacity>

        <TabBar.TypeC
          ref={connectedComponent => this._dateTab = connectedComponent != null ? connectedComponent.getWrappedInstance() : null}
          item={this.state.scheduleDateList}
          containerStyle={this.props.containerStyle}
          onPress={this.onPress.bind(this)}
          containerStyle={{
            width:'80%',
            marginTop:Scaling.verticalScale(10),
          }}
        />

        <TouchableOpacity
          style={{
            flex:1,
            justifyContent:'center',
            alignItems:'center',
          }}
          disabled={!renderRightArrow}
          onPress={this.onPressRightArrow.bind(this)}
        >
          {renderRightArrow ?
            <Image
                source={CONSTANT.IMAGE_URL.RIGHT_ARROW_ICON}
                style={[{
                  width:Scaling.scale(30),
                  height:Scaling.verticalScale(30),
                  resizeMode:'contain',
                  tintColor:CONSTANT.COLOR.GRAY,
                },
                ]}
            />
            :null
          }
           
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  dateScheduleTextActive: {
    color:'#61CDFF',
  },
  eachDateIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  eachDateIndicatorActive: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    borderBottomWidth: 2,
    borderBottomColor: '#61CDFF', 
    borderStyle: 'dotted'
  },
});

function mapStateToProps(state, ownProps) {
  return {
    orderListData:state.rootReducer.order.orderListData,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionOrder: bindActionCreators(Actions.order, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderDateTab);