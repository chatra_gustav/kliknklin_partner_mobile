import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image
} from 'react-native';

import {TabBar, Scaling} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class OrderTypeTab extends Component{
  constructor(props) {
    super(props);
  
    this.state = {
      currentTabKey:1,
    };
  }

  render(){
    let {showImportant, actionOrder, orderListTypeTab} = this.props;
    let rightItem = null;

    if (showImportant) {
      rightItem=(
          <Image
              source={CONSTANT.IMAGE_URL.IMPORTANT_ICON}
              style={{
                width:Scaling.scale(50 * 0.444),
                height:Scaling.verticalScale(50 * 0.444),
                resizeMode:'contain',
              }}
          /> 
      );
    }
    return(
      <TabBar.TypeB
        {...this.props}
        item={!this.props.item ? 
          [
            {
              itemText:'Telat',
              rightItem:rightItem,
            },
            {
              itemText:'Semua',
            },
          ]
            :
          this.props.item
        }
        currentTabKey={orderListTypeTab}
        onPress={(key)=> {
          actionOrder.setOrderListTypeTab(key);
        }}
        containerStyle={{
          marginTop:Scaling.verticalScale(10)
        }}
      />
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    orderListTypeTab: state.rootReducer.order.orderListTypeTab,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionOrder: bindActionCreators(actions.order, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderTypeTab);