import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Text,
        StyleSheet
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import ResponsiveImage from 'react-native-responsive-image';
import {moderateScale} from 'react-native-size-matters';
import ImagePicker from 'react-native-image-picker';

import {LayoutPrimary} from 'app/data/layout';
import {TextWrapper, Button, Form, TabBar, OrderStatus, Avatar, Scaling} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class InternetOfThings extends Component{

  constructor(props){
    super(props);
    this.state={
    }
  }

  componentDidMount(){
  }



  render(){
      return (
        <LayoutPrimary
            showTitle={true}
            showBackButton={true}
            showOutletSelector={true}
            showSearchButton={true}
            navigator={this.props.navigator}
            titleText={'Internet of Things'}
            contentContainerStyle={{
            }}
          >
        </LayoutPrimary>
      );
  }

}

function mapStateToProps(state, ownProps) {
  return {
    userRole:state.rootReducer.user.role,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(InternetOfThings);

const styles = StyleSheet.create({
  header:{
    height:Scaling.verticalScale(CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.HEIGHT),
    width: '100%',
    backgroundColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BACKGROUND_COLOR,
    borderBottomWidth:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_BOTTOM_WIDTH,
    borderColor:CONSTANT.STYLE.NAV_BAR.MAIN_DASHBOARD.BORDER_COLOR,
    flexDirection:'row'
  },
  attendanceHeader:{
    flex: 1,
    flexDirection: 'row', 
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: Dimensions.get('window').width * 0.025
  }
})

