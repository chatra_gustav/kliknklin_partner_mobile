import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Image,
        StyleSheet,
        NativeModules,
        LayoutAnimation
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling, DateFormat, toCurrency, Form} from 'app/data/components';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

const { UIManager } = NativeModules;
UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

const searchingSuccess = 'Ok';
const searchingOnprogress = 'belum ada konsumen';
const searchingFailed = 'Failed';

class OrderMembership extends Component{

  constructor(props){
    super(props);
    this.state={
      image:{
        info_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/deposit/info_icon.png')
        },
        phone_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/membership/phone_icon.png')
        },
        success_icon:{
          width:0,
          height:0,
          multiplierWidth:0.3,
          multiplierHeight:0.3,
          url:require('app/data/image/membership/success_icon.png')
        },
        cancel_icon:{
          width:0,
          height:0,
          multiplierWidth:0.3,
          multiplierHeight:0.3,
          url:require('app/data/image/membership/cancel_icon.png')
        },
        search_icon:{
          width:0,
          height:0,
          multiplierWidth:0.3,
          multiplierHeight:0.3,
          url:require('app/data/image/searchbar/search_icon.png')
        },
        clear_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/deposit/clear_icon.png')
        }
      },
      membershipCustomer: null,
      searchingStatus: searchingOnprogress,
      errMessage: null,
      successMessage: searchingOnprogress
    }
  }

  componentDidMount(){
    this.getImageSize();
  }

  getImageSize(){
    var image = this.state.image;

    var info_icon = ResolveAssetSource(this.state.image.info_icon.url);

    image.info_icon.width = info_icon.width * this.state.image.info_icon.multiplierWidth;
    image.info_icon.height = info_icon.height * this.state.image.info_icon.multiplierHeight;

    var phone_icon = ResolveAssetSource(this.state.image.phone_icon.url);

    image.phone_icon.width = phone_icon.width * this.state.image.phone_icon.multiplierWidth;
    image.phone_icon.height = phone_icon.height * this.state.image.phone_icon.multiplierHeight;

    var cancel_icon = ResolveAssetSource(this.state.image.cancel_icon.url);

    image.cancel_icon.width = cancel_icon.width * this.state.image.cancel_icon.multiplierWidth;
    image.cancel_icon.height = cancel_icon.height * this.state.image.cancel_icon.multiplierHeight;

    var search_icon = ResolveAssetSource(this.state.image.search_icon.url);

    image.search_icon.width = search_icon.width * this.state.image.search_icon.multiplierWidth;
    image.search_icon.height = search_icon.height * this.state.image.search_icon.multiplierHeight;

    var success_icon = ResolveAssetSource(this.state.image.success_icon.url);

    image.success_icon.width = success_icon.width * this.state.image.success_icon.multiplierWidth;
    image.success_icon.height = success_icon.height * this.state.image.success_icon.multiplierHeight;
    
    var clear_icon = ResolveAssetSource(this.state.image.clear_icon.url);

    image.clear_icon.width = clear_icon.width * this.state.image.clear_icon.multiplierWidth;
    image.clear_icon.height = clear_icon.height * this.state.image.clear_icon.multiplierHeight;
    
    this.setState({image});
  }

  render(){
      return (
        <LayoutPrimary
            showTabBar={false}
            showTitle={true}
            showBackButton={true}
            showOutletSelector={false}
            showSearchButton={false}
            title={'Beli Membership'}
            navigator={this.props.navigator}
            contentContainerStyle={{
            }}
          >
            <View
              style={{
                flex:1
              }}
            >

              <Package
                image={this.state.image}
                package={this.props.package}
                actionNavigator={this.props.actionNavigator}
              />

              <Customer
                ref={(ref) => this.customer = ref}
                image={this.state.image}
                searchCustomer={this.searchCustomer.bind(this)}
                membershipCustomer={this.state.membershipCustomer}
                searchingStatus={this.state.searchingStatus}
                resetSearchingStatus={this.resetSearchingStatus.bind(this)}
                successMessage={this.state.successMessage}
                errMessage={this.state.errMessage}
              />

              <DetilPembayaran
                package={this.props.package}
                buyPackage={this.buyPackage.bind(this)}
                membershipCustomer={this.state.membershipCustomer}
                actionRoot={this.props.actionRoot}
              />

            </View>
        </LayoutPrimary>
      );
  }

  async searchCustomer(type, phone_number){
    try{

      await this.props.actionMembership.searchCustomer(type, phone_number);

      if(this.props.searchedCustomer.length == 1){

        this.props.actionNavigator.dismissLightBox();
        await this.props.actionMembership.setMembershipCustomer(this.props.searchedCustomer[0]);

        this.setState({ 
          membershipCustomer: this.props.membershipCustomer,
          searchingStatus: searchingSuccess,
          successMessage: this.props.membershipCustomer.name+' - '+this.props.membershipCustomer.email,
          errMessage: null
        }, () => {
          this.customer.setPhoneNumber(this.state.membershipCustomer.mobile_phone);
        });

      }else if(this.props.searchedCustomer.length > 1){

        let customerList = this.props.searchedCustomer;
        let isFound = false;
        let customer = "";

        for( x in customerList ){
          if(customerList[x].mobile_phone == phone_number){
            customer = customerList[x];
            isFound = true;
            break;
          }
        }
        
        if(isFound){
          this.props.actionNavigator.dismissLightBox();
          await this.props.actionMembership.setMembershipCustomer(customer);

          this.setState({ 
            membershipCustomer: this.props.membershipCustomer,
            searchingStatus: searchingSuccess,
            successMessage: this.props.membershipCustomer.name+' - '+this.props.membershipCustomer.email,
            errMessage: null
          }, () => {
            this.customer.setPhoneNumber(this.state.membershipCustomer.mobile_phone);
          });
        }else{
          this.setState({ 
            membershipCustomer: this.props.membershipCustomer,
            searchingStatus: searchingFailed,
            successMessage: null,
            errMessage: 'Konsumen tidak ditemukan'
          });
        }

        // this.props.actionNavigator.showLightBox('PartnerKliknKlin.CustomerSelection', {
        //   data: this.props.searchedCustomer,
        //   onPressButtonPrimary: async (customer) => {
            
        //     this.props.actionNavigator.dismissLightBox();
        //     await this.props.actionMembership.setMembershipCustomer(customer);

        //     this.setState({ 
        //       membershipCustomer: this.props.membershipCustomer,
        //       searchingStatus: searchingSuccess,
        //       successMessage: this.props.membershipCustomer.name+' - '+this.props.membershipCustomer.email,
        //       errMessage: null
        //     }, () => {
        //       this.customer.setPhoneNumber(this.state.membershipCustomer.mobile_phone);
        //     });

        //   },
        //   onPressButtonSecondary: () =>{
        //       this.props.actionNavigator.dismissLightBox();
        //   },
        // });
      }
    }catch(err){

      this.setState({ 
        membershipCustomer: this.props.membershipCustomer,
        searchingStatus: searchingFailed,
        successMessage: null,
        errMessage: err
      });
    }
  }

  async buyPackage(){

    let customer = this.state.membershipCustomer;
    let _package = this.props.package;

    this.props.actionMembership.membershipApply(customer, _package, this.props.navigator);
  }

  resetSearchingStatus(){
    this.setState({ searchingStatus: searchingOnprogress, errMessage: null, successMessage: searchingOnprogress, membershipCustomer: null });
  }
}

class Package extends Component{
  render(){

    let item = this.props.package;

    return(
      <View style={styles.packageContainer}>
        <View style={styles.leftColumn}>
          <View style={styles.packageName}>
            <Text fontOption={CONSTANT.TEXT_OPTION.HEADER} textStyle={{color: CONSTANT.COLOR.BLACK}}>{item.package_name}</Text>
          </View>

          <View style={styles.packagePrice}>
            <Text fontOption={CONSTANT.TEXT_OPTION.HEADER} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>{toCurrency(item.package_price, 'Rp ')}</Text>
          </View>

          <View style={styles.packageDetail}>
            <View style={styles.packageDetailRow}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT}>{item.total_voucher} Voucher</Text>
            </View>
            <View style={styles.packageDetailRow}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT}>Diskon </Text>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>{item.max_discount_percentage}% </Text>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT}>Maks {toCurrency(item.max_discount_value, 'Rp ')}</Text>
              </View>
            </View>
            <View style={styles.packageDetailRow}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT}></Text>
            </View>
          </View>
        </View>

        <View style={styles.rightColumn}>
          <TouchableOpacity style={styles.tnc} hitSlop={styles.hitSlop}
            onPress={() =>
                    this.props.actionNavigator.showLightBox("PartnerKliknKlin.BoxMembershipTnC", {
                        overrideBackPress: false,
                        termsAndConditions: item.terms_and_conditions
                    })
                }
          >
            <View style={styles.tnc_content}>
              <Image
                style={{width: this.props.image.info_icon.width, height: this.props.image.info_icon.height}}
                source={this.props.image.info_icon.url}
              />

              <View style={styles.tnclabel}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>Syarat & Ketentuan</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

class Customer extends Component{

  constructor(props){
    super(props);
    this.state = {
      phone_number: ''
    }
  }

  render(){
    return(
      <View style={styles.customerContainer}>
        
        <View style={styles.packageName}>
          <Text fontOption={CONSTANT.TEXT_OPTION.HEADER} textStyle={{color: CONSTANT.COLOR.BLACK}}>Konsumen</Text>
        </View>

        <View style={styles.customer_number}>

          <View style={styles.leftIcon}>
            <Image
              style={{width: this.props.image.phone_icon.width, height: this.props.image.phone_icon.height}}
              source={this.props.image.phone_icon.url}
            />
          </View>

          <View style={styles.textinput}>
            <Form.InputField.TypeE
              value={this.state.phone_number}
              keyboardType={'numeric'}
              ref={(ref) => this.phone_number = ref}
              onChangeText={this._onChange.bind(this)}
              placeholder={'Cari nomor konsumen'}
              placeholderTextColor={CONSTANT.COLOR.LIGHTER_GRAY_FONT}
              containerStyle={[
                {
                  width: Dimensions.get('window').width * 0.63,
                  height: Scaling.verticalScale(40),
                  justifyContent: 'center',
                  paddingTop: Scaling.verticalScale(7)
                }
              ]}
              inputContainerStyle={{
                justifyContent: 'center'
              }}
              inputTextStyle={{fontSize: Scaling.moderateScale(15), color: CONSTANT.COLOR.BLACK}}
            />
          </View>
          
          {this.rightIcon()}

        </View>

        <View style={styles.message}>
          {this.message()}
        </View>

      </View>
    )
  }

  message(){

    if(this.props.errMessage !== null){
      return(
        <Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} textStyle={{color: CONSTANT.COLOR.RED, fontWeight: CONSTANT.TEXT_WEIGHT.REGULAR.weight}}>{this.props.errMessage}</Text>
      )
    }else{
      return(
        <Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT, fontWeight: CONSTANT.TEXT_WEIGHT.REGULAR.weight}}>{this.props.successMessage}</Text>
      )
    }
  }

  rightIcon(){
    if(this.props.searchingStatus == searchingOnprogress){
      return(
        <TouchableOpacity style={styles.rightIcon} hitSlop={styles.hitSlop} onPress={this.searchCustomer}> 
          <Image
            style={{width: this.props.image.search_icon.width, height: this.props.image.search_icon.height}}
            source={this.props.image.search_icon.url}
          />
        </TouchableOpacity>
      )
    }else if(this.props.searchingStatus == searchingFailed){
      return(
        <TouchableOpacity style={styles.rightIcon} hitSlop={styles.hitSlop} onPress={this.clearPhoneNumber}> 
          <Image
            style={{width: this.props.image.clear_icon.width, height: this.props.image.clear_icon.height}}
            source={this.props.image.clear_icon.url}
          />
        </TouchableOpacity>
      )
    }else{
      return(
        <TouchableOpacity style={styles.rightIcon} hitSlop={styles.hitSlop} onPress={this.searchCustomer}> 
          <Image
            style={{width: this.props.image.success_icon.width, height: this.props.image.success_icon.height}}
            source={this.props.image.success_icon.url}
          />
        </TouchableOpacity>
      )
    }
  }

  _onChange = (phone_number) => {
    this.setState({ phone_number });
    this.props.resetSearchingStatus();
  }

  clearPhoneNumber = () => {
    this.phone_number.clear();
    this.state.phone_number = '';
    this.props.resetSearchingStatus();
  }

  searchCustomer = () => {
    this.props.searchCustomer('phone', this.state.phone_number)
  }

  setPhoneNumber = (phone_number) => {
    this.setState({ phone_number });
  }

  getPhonenumber(){
    return this.state.phone_number;
  }
}

class DetilPembayaran extends Component{

  constructor(props){
    super(props);
    this.state = {
    }
  }

  render(){

    let item = this.props.package;

    return(
      <View style={styles.pembayaranContainer}>
        <Text fontOption={CONSTANT.TEXT_OPTION.HEADER} textStyle={{color: CONSTANT.COLOR.BLACK, fontFamily: CONSTANT.TEXT_FAMILY.MAIN_SEMI_BOLD}}>Detail Pembayaran</Text>

        <View style={styles.totalPrice}>
          <View style={styles.leftColumn}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>{item.package_name}</Text>
          </View>

          <View style={styles.rightColumn}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>{toCurrency(item.package_price, 'Rp ')}</Text>
          </View>
        </View>

        <View style={styles.subTotalPrice}>
          <View style={styles.leftColumn}>
            <View style={[styles.priceRow, {marginLeft: Scaling.moderateScale(Dimensions.get('window').width * 0.25)}]}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>Subtotal</Text>
            </View>

            <View style={[styles.priceRow, {marginLeft: Scaling.moderateScale(Dimensions.get('window').width * 0.25)}]}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>{''}</Text>
            </View>

            <View style={[styles.priceRow, {marginLeft: Scaling.moderateScale(Dimensions.get('window').width * 0.25)}]}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>Total Bayar</Text>
            </View>
          </View>

          <View style={styles.rightColumn}>
            <View style={styles.priceRow}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>{toCurrency(item.package_price, 'Rp ')}</Text>
            </View>

            <View style={styles.priceRow}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT}>{''}</Text>
            </View>

            <View style={styles.priceRow}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{color: CONSTANT.COLOR.BLACK}}>{toCurrency(item.package_price, 'Rp ')}</Text>
            </View>
          </View>
        </View>

        <View style={styles.button}>
          <Button.TypeD
            buttonText={'Beli Sekarang'}
            buttonColor={CONSTANT.COLOR.GREEN}
            buttonTextStyle={[styles.buttonTextStyle]}
            buttonStyle={styles.buttonStyle}
            onPress={this.buyPackage}
          />
        </View>
      </View>
    )
  }

  buyPackage = () => {
    if(this.props.membershipCustomer !== null)
      this.props.buyPackage();
    else
      this.props.actionRoot.showMessage("Tidak ada konsumen yang dipilih", 'Gagal Membeli Membership');
  }

}

function mapStateToProps(state, ownProps) {
  return {
    searchedCustomer: state.rootReducer.membership.searchedCustomer,
    membershipCustomer: state.rootReducer.membership.membershipCustomer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionMembership: bindActionCreators(actions.membership, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderMembership);

const styles = StyleSheet.create({
  packageContainer:{
    flexDirection: 'row',
    marginBottom: Scaling.moderateScale(20),
    marginHorizontal: Scaling.moderateScale(2),
    paddingTop: Dimensions.get('window').width * 0.025,
    paddingBottom: Dimensions.get('window').width * 0.025,
    paddingLeft: Dimensions.get('window').width * 0.05,
    paddingRight: Dimensions.get('window').width * 0.05,
    backgroundColor: CONSTANT.COLOR.WHITE,
    elevation: 2
  },
  customerContainer:{
    marginBottom: Scaling.moderateScale(20),
    marginHorizontal: Scaling.moderateScale(2),
    paddingTop: Dimensions.get('window').width * 0.03,
    paddingBottom: Dimensions.get('window').width * 0.05,
    paddingLeft: Dimensions.get('window').width * 0.05,
    paddingRight: Dimensions.get('window').width * 0.05,
    backgroundColor: CONSTANT.COLOR.WHITE,
    elevation: 2
  },
  pembayaranContainer:{
    flex: 1,
    marginHorizontal: Scaling.moderateScale(2),
    paddingTop: Dimensions.get('window').width * 0.03,
    paddingLeft: Dimensions.get('window').width * 0.05,
    paddingRight: Dimensions.get('window').width * 0.05,
    backgroundColor: CONSTANT.COLOR.WHITE,
    elevation: 2
  },
  leftColumn:{
    width: '60%'
  },
  packageName:{

  },
  packagePrice:{
    marginVertical: Scaling.moderateScale(10)
  },
  packageDetail:{

  },
  packageDetailRow:{
    marginVertical: Scaling.moderateScale(2)
  },
  rightColumn: {
    width: '40%'
  },
  tnc:{
    flex: 0.5,
    alignItems: 'center'
  },
  tnc_content:{
    flexDirection: 'row',
    alignItems: 'center'
  },
  tnclabel:{
    marginLeft: Scaling.moderateScale(5)
  },
  hitSlop: {
    top: 5, 
    bottom: 5, 
    left: 5, 
    right: 5
  },
  priceRow:{
    marginVertical:Scaling.moderateScale(5)
  },
  totalPrice: {
    flexDirection: 'row', 
    alignItems: 'center',
    paddingBottom: Scaling.moderateScale(20),
    marginTop: Scaling.moderateScale(15),
    marginBottom: Scaling.moderateScale(15),
    borderBottomWidth: 1,
    borderColor: CONSTANT.COLOR.LIGHT_GRAY
  },
  subTotalPrice:{
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center'
  },
  button:{
    position: 'absolute',
    left: Dimensions.get('window').width * 0.05,
    bottom: Scaling.moderateScale(20)
  },
  buttonTextStyle:{
    color: CONSTANT.COLOR.WHITE, 
    fontSize: Scaling.moderateScale(17), 
    fontFamily: CONSTANT.TEXT_FAMILY.MAIN_SEMI_BOLD, 
  },
  buttonStyle:{
    borderRadius: Scaling.moderateScale(10),
    width:Dimensions.get('window').width * 0.9,
    height:CONSTANT.STYLE.BUTTON.LARGE.HEIGHT,
    alignItems: 'center'
  },
  customer_number: {
    marginTop: Scaling.moderateScale(10),
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: Scaling.moderateScale(5),
    paddingHorizontal: Scaling.moderateScale(10),
    paddingVertical: Scaling.moderateScale(7),
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: CONSTANT.COLOR.LIGHTER_GRAY_FONT,
    width: Dimensions.get('window').width * 0.9
  },
  message:{
    marginTop: Scaling.moderateScale(10)
  },
  leftIcon:{
    alignItems: 'center'
  },
  textinput:{
    paddingLeft: Scaling.moderateScale(10)
  },
  rightIcon:{
    position: 'absolute',
    right: Scaling.moderateScale(10),
    alignItems: 'center'
  }
})

