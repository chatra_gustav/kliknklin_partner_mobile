import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Image,
        StyleSheet,
        NativeModules,
        LayoutAnimation
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Form, Button, Scaling, DateFormat, toCurrency} from 'app/data/components';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

class EditMembership extends Component{

  constructor(props){
    super(props);
    this.state={
      image:{
        calendar_icon:{
          width:0,
          height:0,
          multiplierWidth:0.3,
          multiplierHeight:0.3,
          url:require('app/data/image/membership/calendar_icon.png'),
        }
      },
      outlet_id: '',
      promo_id: '',
      package_name: '',
      package_price: '',
      total_voucher: '',
      voucher_price: '',
      voucher_percentage: '',
      start_date_time: '',
      end_date_time: '',
      terms_and_conditions: '',
      membership_validity_period: '',
      membership_package_id: '',
      duration: ''

    }
  }

  componentDidMount(){
    this.getImageSize();
    this.setOutletID();
    this.setPackageData();
  }

  setPackageData(){
    let {outlet_id, promo_id, package_id, package_name, package_price, total_voucher, max_discount_value, max_discount_percentage, start_date_time, end_date_time, terms_and_conditions, duration} = this.props.package;

    this.state.outlet_id = outlet_id;
    this.state.promo_id = promo_id;
    this.state.package_name = package_name;
    this.state.package_price = package_price.toString();
    this.state.total_voucher = total_voucher.toString();
    this.state.voucher_price = max_discount_value.toString();
    this.state.voucher_percentage = max_discount_percentage.toString();
    this.state.start_date_time = start_date_time;
    this.state.end_date_time = end_date_time;
    this.state.membership_validity_period = start_date_time+' - '+end_date_time;
    this.state.membership_package_id = package_id;
    this.state.duration = duration.toString();
    this.setState({ terms_and_conditions });
  }

  getImageSize(){
    var image = this.state.image;

    var calendar_icon = ResolveAssetSource(this.state.image.calendar_icon.url);

    image.calendar_icon.width = calendar_icon.width * this.state.image.calendar_icon.multiplierWidth;
    image.calendar_icon.height = calendar_icon.height * this.state.image.calendar_icon.multiplierHeight;

    this.setState({image});
  }

  setOutletID(){

    let {userOutlet} = this.props;
    let {outletList, indexActiveOutlet} = userOutlet;

    this.setState({ outlet_id : outletList[indexActiveOutlet].id }, () => console.log('outlet_id: ',this.state.outlet_id));
  }

  _getSelectorData(){
    let result = [];
    try{
      let {userOutlet} = this.props;
      let {outletList} = userOutlet;

      for(let index in outletList){
        let obj = {}
        obj.id = outletList[index].id;
        obj.value = outletList[index].name;

        result.push(obj);
      }

      result.push({
        id:123,
        value:'hello my friend we meet again, till dung dung dung dung dung',
      })
    }
    catch(error){
      console.log(error, 'outlet_selector._getSelectorData')
    }
    return result;
  }

  _onChangeOutlet(value, index, data){
    this.setState({ outlet_id: data[index].id });
  }

  render(){
    let {userOutlet} = this.props;

    return (
      <LayoutPrimary
          showTabBar={false}
          showTitle={true}
          title={'Edit Membership Outlet'}
          showBackButton={true}
          showOutletSelector={false}
          showSearchButton={false}
          navigator={this.props.navigator}
          contentContainerStyle={{
            backgroundColor: CONSTANT.COLOR.WHITE
          }}
        >
          <View
            style={styles.container}
          >
            <KeyboardAwareScrollView 
              showsVerticalScrollIndicator={false}
              enableAutomaticScroll={true}
            >

              <CustomTextField
                title={'Pilih Outlet'}
                child={
                  <Form.DropdownField.MainDashboard
                    {...this.props}
                    data={this._getSelectorData()}
                    value={this._getSelectorData()[userOutlet.indexActiveOutlet].value}
                    onChangeText={this._onChangeOutlet.bind(this)}
                    width={'90%'}
                    height={Scaling.moderateScale(50)}
                    editable={false}
                  />
                }
              />

              <CustomTextField
                title={'Nama Paket Membership'}
                child={
                  <Form.InputField.TypeE
                    value={this.state.package_name}
                    keyboardType={'default'}
                    ref={(ref) => this.package_name_ref = ref}
                    onChangeText={this._onChangeName.bind(this)}
                    placeholder={'Contoh: Paket Silver'}
                    placeholderTextColor={CONSTANT.COLOR.LIGHTER_GRAY_FONT}
                    containerStyle={[
                      {
                        width:'85%',
                        height:Scaling.moderateScale(50)
                      }
                    ]}
                    inputContainerStyle={{
                      alignItems: 'center'
                    }}
                    inputTextStyle={{fontSize: Scaling.moderateScale(14), color: CONSTANT.COLOR.BLACK}}
                  />
                }
              />

              <CustomTextField
                title={'Durasi Membership (bulan)'}
                child={
                  <Form.InputField.TypeE
                    editable={false}
                    value={this.state.duration}
                    keyboardType={'numeric'}
                    ref={(ref) => this.package_duration_ref = ref}
                    placeholder={'Maks: 12'}
                    placeholderTextColor={CONSTANT.COLOR.LIGHTER_GRAY_FONT}
                    containerStyle={[
                      {
                        width:'85%',
                        height:Scaling.moderateScale(50)
                      }
                    ]}
                    inputContainerStyle={{
                      alignItems: 'center'
                    }}
                    inputTextStyle={{fontSize: Scaling.moderateScale(14), color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}
                    returnKeyType = { "next" }
                    onSubmitEditing={() => { this.package_price_ref.focus(); }}
                    blurOnSubmit={false}
                  />
                }
              />

              <CustomTextField
                title={'Harga Paket Membership'}
                child={
                  <Form.InputField.TypeE
                    keyboardType={'numeric'}
                    value={this.state.package_price}
                    ref={(ref) => this.package_price_ref = ref}
                    onChangeText={this._onChangePrice.bind(this)}
                    placeholder={'Contoh: 150000'}
                    placeholderTextColor={CONSTANT.COLOR.LIGHTER_GRAY_FONT}
                    containerStyle={[
                      {
                        width:'85%',
                        height:Scaling.moderateScale(50)
                      }
                    ]}
                    inputContainerStyle={{
                      alignItems: 'center'
                    }}
                    inputTextStyle={{fontSize: Scaling.moderateScale(14), color: CONSTANT.COLOR.BLACK}}
                  />
                }
              />

              <CustomTextField
                title={'Jumlah Voucher'}
                child={
                  <Form.InputField.TypeE
                    value={this.state.total_voucher}
                    keyboardType={'numeric'}
                    ref={(ref) => this.total_voucher_ref = ref}
                    onChangeText={this._onChangeTotalVoucher.bind(this)}
                    placeholder={'Contoh: 10'}
                    placeholderTextColor={CONSTANT.COLOR.LIGHTER_GRAY_FONT}
                    containerStyle={[
                      {
                        width:'85%',
                        height:Scaling.moderateScale(50)
                      }
                    ]}
                    inputContainerStyle={{
                      alignItems: 'center'
                    }}
                    inputTextStyle={{fontSize: Scaling.moderateScale(14), color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}
                    editable={false}
                  />
                }
              />

              <CustomTextField
                title={'Maks Diskon Per Satuan Voucher'}
                child={
                  <Form.InputField.TypeE
                    value={this.state.voucher_price}
                    keyboardType={'numeric'}
                    ref={(ref) => this.voucher_price_ref = ref}
                    onChangeText={this._onChangeVocherPrice.bind(this)}
                    placeholder={'Contoh: 10000'}
                    placeholderTextColor={CONSTANT.COLOR.LIGHTER_GRAY_FONT}
                    containerStyle={[
                      {
                        width:'85%',
                        height:Scaling.moderateScale(50)
                      }
                    ]}
                    inputContainerStyle={{
                      alignItems: 'center'
                    }}
                    inputTextStyle={{fontSize: Scaling.moderateScale(14), color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}
                    editable={false}
                  />
                }
              />

              <CustomTextField
                title={'Maks % Potongan Per Satuan Voucher'}
                child={
                  <Form.InputField.TypeE
                    value={this.state.voucher_percentage}
                    keyboardType={'numeric'}
                    ref={(ref) => this.voucher_percentage_ref = ref}
                    onChangeText={this._onChangeVoucherPercentage.bind(this)}
                    placeholder={'Contoh: 50'}
                    placeholderTextColor={CONSTANT.COLOR.LIGHTER_GRAY_FONT}
                    containerStyle={[
                      {
                        width:'85%',
                        height:Scaling.moderateScale(50)
                      }
                    ]}
                    inputContainerStyle={{
                      alignItems: 'center'
                    }}
                    inputTextStyle={{fontSize: Scaling.moderateScale(14), color: CONSTANT.COLOR.LIGHTER_GRAY_FONT}}
                    editable={false}
                  />
                }
              />
              
              <CustomTextField
                title={'Syarat & Ketentuan Diskon'}
                child={
                  <View style={{paddingTop: Scaling.moderateScale(5)}}>
                    <Form.InputField.TypeE
                      value={this.state.terms_and_conditions}
                      multiline={true}
                      numberOfLines={3}
                      keyboardType={'default'}
                      ref={(ref) => this.terms_and_conditions_ref = ref}
                      onChangeText={this._onChangeTnc.bind(this)}
                      placeholder={'Contoh: \n1. Diskon 50% maksimal Rp 50.000\n2. Berlaku untuk konsumen outlet LaundryKlin'}
                      placeholderTextColor={CONSTANT.COLOR.LIGHTER_GRAY_FONT}
                      containerStyle={[
                        {
                          width:'100%',
                        }
                      ]}
                      inputContainerStyle={{
                        alignItems: 'center',
                        height: 120
                      }}
                      inputTextStyle={{fontSize: Scaling.moderateScale(14), color: CONSTANT.COLOR.LIGHTER_GRAY_FONT, height: 100}}
                      editable={false}
                    />
                  </View>
                }
                height={{height: 120}}
              />
              
              <View style={styles.button}>
                <Button.TypeD
                  buttonText={'Edit Paket'}
                  buttonColor={CONSTANT.COLOR.GREEN}
                  buttonTextStyle={[styles.buttonTextStyle]}
                  buttonStyle={styles.buttonStyle}
                  onPress={this.onButtonPress}
                />
              </View>

            </KeyboardAwareScrollView>  
          </View>
      </LayoutPrimary>
    );
  }

  _onChangeName(package_name){
    this.setState({package_name});
  }

  _onChangePrice(package_price){
    this.setState({package_price});
  }

  _onChangeTotalVoucher(total_voucher){
    this.setState({total_voucher});
  }

  _onChangeVocherPrice(voucher_price){
    this.setState({voucher_price});
  }

  _onChangeVoucherPercentage(voucher_percentage){
    this.setState({voucher_percentage});
  }

  _onChangeTnc(terms_and_conditions){
    this.setState({terms_and_conditions})
  }

  onButtonPress = async () => {

    if(this.isAllTextfieldFilled()){
      let params = {    
        membership_package_id: this.state.membership_package_id,
        package_name: this.state.package_name,
        package_price: this.state.package_price
      }

      await this.props.actionMembership.membershipUpdate(params, this.props.navigator, () => {this.props.refreshAfterCreatePackage() });
    }else{
      this.props.actionRoot.showMessage('Anda belum mengisi kolom nama paket atau harga paket', 'Tidak berhasil');
    }
  }

  isAllTextfieldFilled(){
    return (this.state.membership_package_id !== '' && this.state.package_name !== '') && (this.state.package_price !== '' )
  }
}

class CustomTextField extends Component{
  render(){
    return(
      <View style={[styles.containerTextfield]}>
        <View style={styles.title}>
          <Text fontOption={CONSTANT.TEXT_OPTION.SEE_STATISTICS_HEADER} textStyle={{color: CONSTANT.COLOR.LIGHTER_GRAY_FONT, fontWeight: CONSTANT.TEXT_WEIGHT.REGULAR.weight}}>{this.props.title}</Text>
        </View>

        <View style={[styles.child, this.props.height]}>
          <View style={{flex: 0.85}}>
            {this.props.child}
          </View>

          <View style={{flex: 0.15, alignItems: 'center'}}>
            {this.renderIcon()}
          </View>
        </View>
      </View>
    )
  }

  renderIcon(){
    if(this.props.icon !== undefined){
      return(
        this.props.icon
      )
    }
    return null
  }
}

function mapStateToProps(state, ownProps) {
  return {
    userOutlet:state.rootReducer.user.outlet,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionMembership: bindActionCreators(actions.membership, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditMembership);

const styles = StyleSheet.create({
  container:{
    paddingTop: Dimensions.get('window').width * 0.025,
    paddingBottom: Dimensions.get('window').width * 0.025,
    paddingLeft: Dimensions.get('window').width * 0.05,
    paddingRight: Dimensions.get('window').width * 0.05,
  },
  title:{
    paddingVertical: Scaling.moderateScale(5)
  },
  child:{
    flex:1,
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: CONSTANT.COLOR.LIGHTER_GRAY_FONT,
    borderRadius: Scaling.moderateScale(7),
    height: 50,
    paddingHorizontal: Scaling.moderateScale(10),
    justifyContent: 'center'
  },
  containerTextfield:{
    marginVertical: Scaling.moderateScale(5)
  },
  button:{
    marginTop: Scaling.moderateScale(20)
  },
  buttonStyle:{
    borderRadius: Scaling.moderateScale(10),
    width:Dimensions.get('window').width * 0.9,
    height:CONSTANT.STYLE.BUTTON.LARGE.HEIGHT,
    alignItems: 'center'
  },
  buttonTextStyle:{
    color: CONSTANT.COLOR.WHITE, 
    fontSize: Scaling.moderateScale(17), 
    fontFamily: CONSTANT.TEXT_FAMILY.MAIN_SEMI_BOLD
  }
})