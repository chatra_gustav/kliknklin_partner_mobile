import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity, Linking, Platform} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import SInfo from 'react-native-sensitive-info';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

//custom component js class import
import {TextWrapper, Form, Button, Text, Scaling} from 'app/data/components';

//layout
import {LayoutPrimary} from 'app/data/layout';

import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class OrderMembershipSuccess extends Component {
	constructor(props) {
        super(props);
        this.state = {
        	image:{
        		card_icon:{
        			width:0,
					height:0,
					multiplierWidth:0.4,
					multiplierHeight:0.4,
					url:require('app/data/image/membership/card_icon.png'),
        		},
        		cancel_icon:{
        			width:0,
					height:0,
					multiplierWidth:0.4,
					multiplierHeight:0.4,
					url:require('app/data/image/membership/cancel_icon.png'),
        		}
        	},
        	data:{
        		email:'',
        	},
        	errorMessage:' ',
        }; 
    }

	componentDidMount(){
		this._getImageSize();
	}

	_getImageSize(){
		var image = this.state.image;

		var card_icon = ResolveAssetSource(this.state.image.card_icon.url);

		image.card_icon.width = Scaling.moderateScale(card_icon.width * this.state.image.card_icon.multiplierWidth);
		image.card_icon.height = Scaling.moderateScale(card_icon.height * this.state.image.card_icon.multiplierHeight);

		var cancel_icon = ResolveAssetSource(this.state.image.cancel_icon.url);

		image.cancel_icon.width = Scaling.moderateScale(cancel_icon.width * this.state.image.cancel_icon.multiplierWidth);
		image.cancel_icon.height = Scaling.moderateScale(cancel_icon.height * this.state.image.cancel_icon.multiplierHeight);

      	this.setState({image});
	}

	render(){
		return (
			<LayoutPrimary
				containerStyle={{
					backgroundColor:CONSTANT.COLOR.LIGHT_GRAY,
				}}
				showBackButton={false}
				navigator={this.props.navigator}
				showNavBar={false}
				showTabBar={false}
			>
				{this._renderContent()}
			</LayoutPrimary>
		)
	}

	_renderContent(){
		return(
			<View style={styles.container}>
				<View style={styles.header}>
					<TouchableOpacity onPress={() => {
							this.props.actionNavigator.pop(this.props.navigator)
							this.props.actionNavigator.pop(this.props.navigator)
						}}>
						<Image
		              		style={{width: this.state.image.cancel_icon.width, height: this.state.image.cancel_icon.height}}
		              		source={this.state.image.cancel_icon.url}
		            	/>
					</TouchableOpacity>
				</View>

				<View style={styles.content}>
					<View style={styles.info1}>
			          	<Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} 
			          		textStyle={{
			          			textAlign: 'center', color: CONSTANT.COLOR.LIGHT_BLACK, 
			          			fontWeight: CONSTANT.TEXT_WEIGHT.REGULAR.weight
			          		}}>
			          		Membership Customer Berhasil Terbuat
			          	</Text>
			        </View>

					<Image
	              		style={{width: this.state.image.card_icon.width, height: this.state.image.card_icon.height}}
	              		source={this.state.image.card_icon.url}
	            	/>

	            	<View style={styles.info2}>
			          	<Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} 
			          		textStyle={{
			          			textAlign: 'center', 
			          			color: CONSTANT.COLOR.LIGHT_BLACK
			          		}}>
			          		Untuk memastikan membership telah masuk
			          	</Text>
			          	<Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} 
			          		textStyle={{
			          			textAlign: 'center', 
			          			color: CONSTANT.COLOR.LIGHT_BLACK
			          		}}>
			          		customer dapat mengecek fitur voucher pada
			          	</Text>
			          	<Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} 
			          		textStyle={{
			          			textAlign: 'center', 
			          			color: CONSTANT.COLOR.LIGHT_BLACK
			          		}}>
			          		aplikasi kliknklin
			          	</Text>
			        </View>
	            </View>

            	<View style={styles.button}>
            		<Button.TypeD
			            buttonText={'Cetak Receipt'}
			            buttonColor={CONSTANT.COLOR.LIGHT_BLUE}
			            buttonTextStyle={[{color: CONSTANT.COLOR.WHITE, fontSize: Scaling.moderateScale(17), fontFamily: CONSTANT.TEXT_FAMILY.MAIN_LIGHT, fontWeight: CONSTANT.TEXT_WEIGHT.MEDIUM.weight}]}
			            buttonStyle={styles.buttonStyle}
			            onPress={this.onButtonPress}
			        />
            	</View>
			</View>
		)
	}

	onButtonPress = () => {

		let params = {
			outlet: this.props.outlet,
    		customer: this.props.customer,
    		membershipPackage: this.props.membershipPackage,
    		transaction_hash: this.props.transaction_hash
		}

		this.props.actionBluetoothDevices.cetak(params, false, true)
	}
}

function mapStateToProps(state, ownProps) {
	return {
		errorMessage:state.rootReducer.message.error,
		layoutHeight:state.rootReducer.layout.height,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionAuthentication: bindActionCreators(actions.authentication, dispatch),
		actionRoot: bindActionCreators(actions.root, dispatch),
		actionNavigator: bindActionCreators(actions.navigator, dispatch),
		actionBluetoothDevices: bindActionCreators(actions.bluetoothDevice, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderMembershipSuccess);

var styles = StyleSheet.create({
    container: {
    	flex: 1
    },
    header:{
    	alignItems:'flex-end',
    	padding: Scaling.moderateScale(20)
    },
    content: {
    	marginTop: Scaling.moderateScale(40),
    	alignItems: 'center'
    },
    info1:{
    	marginBottom: Scaling.moderateScale(10)
    },
    info2:{
    	marginTop: Scaling.moderateScale(10)
    },
    button:{
    	marginTop: Scaling.moderateScale(40)
    },
    buttonStyle:{
	    borderRadius: Scaling.moderateScale(10),
	    width:Dimensions.get('window').width * 0.9,
	    height:CONSTANT.STYLE.BUTTON.LARGE.HEIGHT,
	    alignItems: 'center'
	}
});

