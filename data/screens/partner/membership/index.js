import React, { Component } from 'react';
import {    
        View,
        Dimensions,
        TouchableOpacity,
        ScrollView,
        FlatList,
        Image,
        StyleSheet,
        NativeModules,
        LayoutAnimation
        } from 'react-native';

import ResolveAssetSource from 'resolveAssetSource';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling, DateFormat, toCurrency} from 'app/data/components';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

class Membership extends Component{

  constructor(props){
    super(props);
    this.state={
      image:{
        info_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/deposit/info_icon.png'),
        },
        card_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/membership/card_icon.png'),
        },
        add_icon:{
          width:0,
          height:0,
          multiplierWidth:0.4,
          multiplierHeight:0.4,
          url:require('app/data/image/membership/add_icon.png'),
        },
        edit_icon:{
            width:0,
            height:0,
            multiplierWidth:0.35,
            multiplierHeight:0.35,
            url:require('app/data/image/attendance/edit_icon.png')
        }
      }
    }
  }

  componentDidMount(){
    this.getMembershipPackage();
    this.getImageSize();
  }

  async getMembershipPackage(){
    await this.props.actionMembership.getMembershipPackage();
  }

  getImageSize(){
    var image = this.state.image;

    var info_icon = ResolveAssetSource(this.state.image.info_icon.url);

    image.info_icon.width = info_icon.width * this.state.image.info_icon.multiplierWidth;
    image.info_icon.height = info_icon.height * this.state.image.info_icon.multiplierHeight;
    
    var card_icon = ResolveAssetSource(this.state.image.card_icon.url);

    image.card_icon.width = Scaling.moderateScale(card_icon.width * this.state.image.card_icon.multiplierWidth);
    image.card_icon.height = Scaling.moderateScale(card_icon.height * this.state.image.card_icon.multiplierHeight);

    var add_icon = ResolveAssetSource(this.state.image.add_icon.url);

    image.add_icon.width = Scaling.moderateScale(add_icon.width * this.state.image.add_icon.multiplierWidth);
    image.add_icon.height = Scaling.moderateScale(add_icon.height * this.state.image.add_icon.multiplierHeight);

    var edit_icon = ResolveAssetSource(this.state.image.edit_icon.url);

    image.edit_icon.width = Scaling.moderateScale(edit_icon.width * this.state.image.edit_icon.multiplierWidth);
    image.edit_icon.height = Scaling.moderateScale(edit_icon.height * this.state.image.edit_icon.multiplierHeight);

    this.setState({image});
  }

  render(){
      return (
        <LayoutPrimary
            showTabBar={false}
            showTitle={false}
            showBackButton={true}
            showOutletSelector={true}
            showSearchButton={false}
            showCustomButton={true}
            customButton={this.addIcon()}
            navigator={this.props.navigator}
            contentContainerStyle={{
            }}
          >
            <View
              style={{
                flex:1
              }}
            >

              {this.renderContent()}

            </View>
        </LayoutPrimary>
      );
  }

  addIcon(){
    return(
      <TouchableOpacity onPress={this.createPackage.bind(this)}>
        <Image
          style={{width: this.state.image.add_icon.width, height: this.state.image.add_icon.height}}
          source={this.state.image.add_icon.url}
        />
      </TouchableOpacity>
    )
  }

  renderContent(){
    if(this.props.membershipPackages.length == 0){
      return(
        <NoPackage
          image={this.state.image}
          createPackage={this.createPackage.bind(this)}
        />
      )
    }else{
      return(
        <FlatList 
          showsVerticalScrollIndicator={false}
          extraData={this.state}
          data={this.props.membershipPackages}
          keyExtractor={(item, index) => index+''}
          renderItem={(item) => { return (
            <Package
              item={item.item}
              image={this.state.image}
              buyAPackage={this.buyAPackage.bind(this)}
              editPackage={this.editPackage.bind(this)}
              actionNavigator={this.props.actionNavigator}
            />
          );}}
        />
      )
    }
  }

  buyAPackage = (selectedPackage) => {
    this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.ORDER_MEMBERSHIP, {
      package: selectedPackage
    })
  }

  async refreshAfterCreatePackage(action){
    await this.props.actionMembership.getMembershipPackage();
    this.props.actionRoot.showMessage("Membership berhasil "+action, 'Sukses');
  }

  createPackage = () => {
    this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.CREATE_MEMBERSHIP, {
      refreshAfterCreatePackage: () => this.refreshAfterCreatePackage('dibuat')
    });
  }

  editPackage = (selectedPackage) => {
    this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.EDIT_MEMBERSHIP, {
      package: selectedPackage,
      refreshAfterCreatePackage: () => this.refreshAfterCreatePackage('diubah')
    })
  }
}

class Package extends Component{
  render(){

    let item = this.props.item;
    
    return(
      <View style={styles.packageContainer}>
        <View style={styles.leftColumn}>
          <View style={styles.packageName}>
            <View style={{marginRight: Scaling.moderateScale(18)}}>
              <Text fontOption={CONSTANT.TEXT_OPTION.HEADER} textStyle={{color: CONSTANT.COLOR.BLACK}}>{item.package_name}</Text>
            </View>

            <TouchableOpacity onPress={this.editPackage}>
                <Image
                  style={{width: this.props.image.edit_icon.width, height: this.props.image.edit_icon.height}}
                  source={this.props.image.edit_icon.url}
                />
              </TouchableOpacity>
          </View>

          <View style={styles.packagePrice}>
            <Text fontOption={CONSTANT.TEXT_OPTION.HEADER} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>{toCurrency(item.package_price, 'Rp ')}</Text>
          </View>

          <View style={styles.packageDetail}>
            <View style={styles.packageDetailRow}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT}>{item.total_voucher} Voucher</Text>
            </View>
            <View style={styles.packageDetailRow}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT}>Diskon </Text>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>{item.max_discount_percentage}% </Text>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT}>Maks {toCurrency(item.max_discount_value, 'Rp ')}</Text>
              </View>
            </View>
            <View style={styles.packageDetailRow}>
              <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT}></Text>
            </View>
          </View>
        </View>

        <View style={styles.rightColumn}>
          <TouchableOpacity style={styles.tnc} hitSlop={styles.hitSlop}
            onPress={() =>
                this.props.actionNavigator.showLightBox("PartnerKliknKlin.BoxMembershipTnC", {
                    overrideBackPress: false,
                    termsAndConditions: item.terms_and_conditions
                })
            }
          >
            <View style={styles.tnc_content}>
              <Image
                style={{width: this.props.image.info_icon.width, height: this.props.image.info_icon.height}}
                source={this.props.image.info_icon.url}
              />

              <View style={styles.tnclabel}>
                <Text fontOption={CONSTANT.TEXT_OPTION.LARGER_CONTENT} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>Syarat & Ketentuan</Text>
              </View>
            </View>
          </TouchableOpacity>

          <View style={styles.button}>
            <Button.TypeD
              buttonText={'Beli Member'}
              buttonColor={CONSTANT.COLOR.ORANGE}
              buttonTextStyle={[styles.buttonTextStyle]}
              buttonStyle={styles.buttonStyle}
              onPress={this.buyAPackage}
            />
          </View>
        </View>
      </View>
    )
  }

  buyAPackage = () => {
    this.props.buyAPackage(this.props.item);
  }

  editPackage = () => {
    this.props.editPackage(this.props.item);
  }
}

class NoPackage extends Component{
  render(){
    return(
      <View style={styles.NoPackage_container}>

        <View style={styles.NoPackage_content}>
          <View style={styles.NoPackage_info1}>
              <Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} textStyle={{textAlign: 'center', color: CONSTANT.COLOR.LIGHT_BLACK}}>Ups, Outlet ini belum memiliki Paket</Text>
              <Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} textStyle={{textAlign: 'center', color: CONSTANT.COLOR.LIGHT_BLACK}}>Membership apapun</Text>
          </View>

          <Image
              style={{width: this.props.image.card_icon.width, height: this.props.image.card_icon.height}}
              source={this.props.image.card_icon.url}
          />

          <View style={styles.NoPackage_info2}>
            <Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} textStyle={{textAlign: 'center', color: CONSTANT.COLOR.LIGHT_BLACK}}>Membership outlet bertujuan untuk mempertahankan</Text>
            <Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} textStyle={{textAlign: 'center', color: CONSTANT.COLOR.LIGHT_BLACK}}>customer mengorder pada suatu outlet</Text>
          </View>
        </View>

        <View style={styles.NoPackage_button}>
          <Button.TypeD
            buttonText={'Buat Paket Membership'}
            buttonColor={CONSTANT.COLOR.LIGHT_BLUE}
            buttonTextStyle={[{color: CONSTANT.COLOR.WHITE, fontSize: Scaling.moderateScale(17), fontFamily: CONSTANT.TEXT_FAMILY.MAIN_LIGHT, fontWeight: CONSTANT.TEXT_WEIGHT.MEDIUM.weight}]}
            buttonStyle={styles.NoPackage_buttonStyle}
            onPress={this.onButtonPress}
          />
        </View>

        <View style={styles.moreInfo}>
          <Image
            style={{width: this.props.image.info_icon.width, height: this.props.image.info_icon.height}}
            source={this.props.image.info_icon.url}
          />

          <TouchableOpacity style={{marginLeft: Scaling.moderateScale(10)}}>
            <Text fontOption={CONSTANT.TEXT_OPTION.LARGEST_CONTENT} textStyle={{textAlign: 'center', color: CONSTANT.COLOR.LIGHT_BLUE, fontWeight: CONSTANT.TEXT_WEIGHT.REGULAR.weight}}>informasi lebih lanjut</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  onButtonPress = () => {
    this.props.createPackage()
  }
}

function mapStateToProps(state, ownProps) {
  return {
    membershipPackages: state.rootReducer.membership.membershipPackages
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actionRoot: bindActionCreators(actions.root, dispatch),
    actionNavigator: bindActionCreators(actions.navigator, dispatch),
    actionMembership: bindActionCreators(actions.membership, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Membership);

const styles = StyleSheet.create({
  packageContainer:{
    flexDirection: 'row',
    marginBottom: Scaling.moderateScale(20),
    marginHorizontal: Scaling.moderateScale(2),
    paddingTop: Dimensions.get('window').width * 0.025,
    paddingBottom: Dimensions.get('window').width * 0.025,
    paddingLeft: Dimensions.get('window').width * 0.05,
    paddingRight: Dimensions.get('window').width * 0.05,
    backgroundColor: CONSTANT.COLOR.WHITE,
    elevation: 2
  },
  leftColumn:{
    flex: 0.6
  },
  packageName:{
    flexDirection: 'row',
    alignItems: 'center'
  },
  packagePrice:{
    marginVertical: Scaling.moderateScale(10)
  },
  packageDetail:{

  },
  packageDetailRow:{
    marginVertical: Scaling.moderateScale(2)
  },
  rightColumn: {
    flex: 0.4
  },
  tnc:{
    flex: 0.5,
    alignItems: 'center'
  },
  tnc_content:{
    flexDirection: 'row',
    alignItems: 'center'
  },
  tnclabel:{
    marginLeft: Scaling.moderateScale(5)
  },
  button:{
    flex: 0.5,
    top: 5, 
    bottom: 5, 
    left: 5, 
    right: 5
  },
  buttonTextStyle:{
    color: CONSTANT.COLOR.WHITE,
    fontSize: Scaling.moderateScale(12), 
    fontFamily: CONSTANT.TEXT_FAMILY.MAIN_SEMI_BOLD, 
  },
  buttonStyle:{
    borderRadius: Scaling.moderateScale(10),
    width:Dimensions.get('window').width * 0.25,
    height:CONSTANT.STYLE.BUTTON.SMALL.HEIGHT,
    alignItems: 'center'
  },
  hitSlop: {
    top: 5, 
    bottom: 5, 
    left: 5, 
    right: 5
  },
  NoPackage_container: {
    flex: 1,
    paddingLeft: Dimensions.get('window').width * 0.05,
    paddingRight: Dimensions.get('window').width * 0.05,
  },
  NoPackage_content: {
    marginTop: Scaling.moderateScale(40),
    alignItems: 'center'
  },
  NoPackage_info1:{
    marginBottom: Scaling.moderateScale(10)
  },
  NoPackage_info2:{
    marginTop: Scaling.moderateScale(10)
  },
  NoPackage_button:{
    marginTop: Scaling.moderateScale(40)
  },
  NoPackage_buttonStyle:{
    borderRadius: Scaling.moderateScale(10),
    width:Dimensions.get('window').width * 0.9,
    height:CONSTANT.STYLE.BUTTON.LARGE.HEIGHT,
    alignItems: 'center'
  },
  moreInfo:{
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: Scaling.moderateScale(30)
  }
})

