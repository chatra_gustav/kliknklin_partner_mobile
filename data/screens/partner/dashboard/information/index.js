import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity, Linking, ScrollView, Platform, BackHandler} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import Carousel, {ParallaxImage} from 'react-native-snap-carousel';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from 'app/data/layout';
import {Text, DateFormat, ShipmentScheduleListCourier, OutletSelector, Label, Scaling} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";


class Information extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentIndex: 0,
        };  
    }
    
    componentWillMount(){
        this.startFeaturedSlider();
    }

    componentWillUnmount(){
        clearInterval(this.autoFeaturedSlider);
    }

    startFeaturedSlider = () => { 
        this.autoFeaturedSlider = setInterval(() => { 
            if(true){ 
                if(this.state.currentIndex==this.props.informationList.length - 1) 
                    this.setState({currentIndex: 0}); 
                else
                    this.setState({currentIndex: this.state.currentIndex + 1}); 
            } 
        }, 4000); 
    }

    _renderItem ({itemIndex, currentIndex, item, animatedValue }) {
        let even = (currentIndex + 1) % 2 === 0;
        return (
           <TouchableOpacity
                activeOpacity={1}
                style={{
                height: Scaling.verticalScale(CONSTANT.STYLE.IMAGE.INFORMATION.HEIGHT),
                paddingLeft:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                }}
                onPress={() => { 
                    this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.WebView', {
                        title:item.title,
                        url:item.description,
                    })
                }}
            >
                    <View style={[styles.imageContainer]}>
                        <Image
                          source={{ uri: item.carousel_image_url }}
                          style={{
                            width:Scaling.scale(CONSTANT.STYLE.IMAGE.INFORMATION.WIDTH),
                            height:Scaling.verticalScale(CONSTANT.STYLE.IMAGE.INFORMATION.HEIGHT),
                            borderRadius:Scaling.scale(CONSTANT.STYLE.IMAGE.INFORMATION.RADIUS),
                            resizeMode:'cover'
                          }}
                        />
                    </View>
            </TouchableOpacity>
        );
    }

    render(){
        try{
            let {informationList} = this.props;
            if (informationList.length != 0) {
                const { width } = Dimensions.get('window');
                const contentOffset = 0;

                return(
                    <View
                        style={{
                            width:'100%',
                            height:Scaling.verticalScale(500 * 0.444),
                            backgroundColor:'white',
                            marginTop:Scaling.verticalScale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_TOP),
                            paddingHorizontal:CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT,
                        }}
                    >
                        <View
                            style={{
                                height:'20%',
                                justifyContent:'center',
                            }}
                        >
                            <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>Informasi</Text>
                        </View>

                        <View
                            style={{
                                height:'80%',
                                alignItems:'center',
                            }}
                        >
                            <Carousel
                              ref={(c) => { this._carousel = c; }}
                              data={informationList}
                              renderItem={this._renderItem.bind(this)}
                              sliderWidth={Dimensions.get('window').width}
                              sliderHeight={Scaling.verticalScale(CONSTANT.STYLE.IMAGE.INFORMATION.HEIGHT)}
                              itemWidth={Scaling.scale(CONSTANT.STYLE.IMAGE.INFORMATION.WIDTH + 30)}
                              layout={'default'}
                              activeSlideAlignment={'start'}
                              inactiveSlideScale={1}
                              inactiveSlideOpacity={1}
                              autoplay
                              autoplayDelay={3000}
                              autoplayInterval={5000}
                              loop
                            />
                        </View>
                    </View>
                );
            }
            return null;
        }
        catch(error){
            console.log(error, 'dashboard.information.index');
            return null;
        }
        
    }
}

function mapStateToProps(state, ownProps) {
    return {
        userRole:state.rootReducer.user.role,
        informationList:state.rootReducer.root.informationList,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actionRoot: bindActionCreators(actions.root, dispatch),
        actionNavigator: bindActionCreators(actions.navigator, dispatch),
    };
}

const styles = StyleSheet.create({
    slideInnerContainer: {
        paddingBottom: 18 // needed for shadow
    },
    imageContainerEven: {
        backgroundColor: CONSTANT.COLOR.BLACK,
    },
    // image's border radius is buggy on iOS; let's hack it!
    radiusMaskEven: {
        backgroundColor: CONSTANT.COLOR.BLACK,
    },
    textContainerEven: {
        backgroundColor: CONSTANT.COLOR.BLACK,
    },
    title: {
        color: CONSTANT.COLOR.BLACK,
        fontSize: 13,
        fontWeight: 'bold',
        letterSpacing: 0.5
    },
    titleEven: {
        color: 'white'
    },
    subtitle: {
        marginTop: 6,
        color: CONSTANT.COLOR.GRAY,
        fontSize: 12,
        fontStyle: 'italic'
    },
    subtitleEven: {
        color: 'rgba(255, 255, 255, 0.7)'
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Information);

