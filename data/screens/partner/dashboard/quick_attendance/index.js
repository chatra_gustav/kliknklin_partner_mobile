import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity, Linking, ScrollView, Platform, BackHandler} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import Carousel, {ParallaxImage} from 'react-native-snap-carousel';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";



class QuickAttendance extends Component {
	constructor(props) {
        super(props);
        this.state = {
        };  
    }
	
    _onPress(){
        let {indexActiveOutlet} = this.props.userOutlet;

        if(indexActiveOutlet != null)
            this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.ATTENDANCE);
        else{
            this.props.actionRoot.showMessage("Anda tidak memiliki outlet", '');
        }
    }


	render(){
		try{
            if (this.props.userRole != CONSTANT.USER_ROLE.COURIER) {
                return(
                    <View
                        style={{
                            width:'100%',
                            height:Scaling.verticalScale(150 * 0.444),
                            marginTop:Scaling.verticalScale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_TOP),
                        }}
                    >
                        <View
                            style={{
                                flex:1,
                                backgroundColor:CONSTANT.COLOR.ORANGE,
                                marginHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                                borderRadius:7,
                                flexDirection:'row',
                            }}
                        >
                            <View
                                style={{
                                    width:'65%',
                                    justifyContent:'center',
                                    paddingLeft:Scaling.verticalScale(25 * 0.444),
                                }}
                            >
                                <Text
                                    fontOption={CONSTANT.TEXT_OPTION.BUTTON}
                                    textStyle={{
                                        color:CONSTANT.COLOR.WHITE,
                                    }}
                                >
                                    Jangan lupa absen yaa
                                </Text>
                                
                            </View>

                            <View
                                style={{
                                    width:'35%',
                                    justifyContent:'center',
                                }}
                            >
                                <Button.Standard
                                    buttonText={'OKAY'}
                                    buttonColor={CONSTANT.COLOR.ORANGE}
                                    buttonBorderColor={CONSTANT.COLOR.WHITE}
                                    onPress={this._onPress.bind(this)}
                                />
                            </View>
                            
                        </View>

                    </View>
                );
            }
            return null;
		}
		catch(error){
			console.log(error, 'dashboard.quick_attendance.index');
			return null;
		}
		
	}
}

function mapStateToProps(state, ownProps) {
	return {
		userRole:state.rootReducer.user.role,
        userOutlet:state.rootReducer.user.outlet,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

const styles = StyleSheet.create({
});

export default connect(mapStateToProps, mapDispatchToProps)(QuickAttendance);

