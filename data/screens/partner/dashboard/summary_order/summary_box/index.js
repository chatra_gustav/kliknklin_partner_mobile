import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity, Linking, ScrollView, Platform, BackHandler} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from 'app/data/actions';

class SummaryBox extends Component {
	static defaultProps = {
	 	title:'Judul',
	 	summaryNumber:0,
	 	onPress:()=>{},
	}

	constructor(props) {
        super(props);
        this.state = {
        };  
    }

    renderAddSymbol(isRender){
    	if (!isRender) {
    		return null;
    	}

    	return(
    		<Text
				fontOption={CONSTANT.TEXT_OPTION.ADD_SYMBOL_BOX_DASHBOARD}
    		>
    		+
    		</Text>
    	)
    }

	render(){
		let showAddSymbol = false;
		let {summaryNumber, title} = this.props;
		
		if (summaryNumber > 99) {
			summaryNumber = 99;
			showAddSymbol = true;
		}

		try{
			return(
				<TouchableOpacity
					style={{
						width:Scaling.scale(88.8),
						height:Scaling.scale(97.68),
						alignItems:'center',
						justifyContent:'center',
					}}
					onPress={this.props.onPress.bind(this)}
				>
					<Button.Icon
						buttonStyle={{
							flexDirection:'row',
						}}
						onPress={this.props.onPress.bind(this)}
					>
						<Text 
							fontOption={CONSTANT.TEXT_OPTION.SUMMARY_NUMBER_BOX_DASHBOARD}
						>
							{summaryNumber}
						</Text>

						{this.renderAddSymbol(showAddSymbol)}

						{
							this.props.isLate ? 

							<Image
								source={CONSTANT.IMAGE_URL.IMPORTANT_ICON}
								style={{
									width:Scaling.scale(25),
									height:Scaling.scale(25),
									resizeMode:'contain',
									position:'absolute',
									top:'-15%',
									right:'-15%',
								}}
							/>
							:
							null
						}
						
					</Button.Icon>

					<View
						style={{
							width:'100%',
							height:Scaling.verticalScale(22.2),
							justifyContent:'center',
							alignItems:'center',
						}}
					>
						<Text 
							fontOption={CONSTANT.TEXT_OPTION.ICON_BOX_DASHBOARD}
							textStyle={{color:CONSTANT.COLOR.DARK_GRAY}}
						>
						{title}</Text>
					</View>
				</TouchableOpacity>
			);
		}
		catch(error){
			console.log(error, 'dashboard.summary_box');
			return null;
		}
		
	}
}

function mapStateToProps(state, ownProps) {
	return {
		layoutHeight:state.rootReducer.layout.height,
		summaryOrder:state.rootReducer.order.summary,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SummaryBox);

