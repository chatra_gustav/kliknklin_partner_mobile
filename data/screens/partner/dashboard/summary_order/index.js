import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity, Linking, ScrollView, Platform, BackHandler} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';

import {LayoutPrimary} from './../../../../layout';
import {Text, Button, DateFormat, ShipmentScheduleListCourier, OutletSelector, Label, Scaling} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

//local
import SummaryBox from './summary_box';

class SummaryOrder extends Component {
	constructor(props) {
        super(props);
        this.state = {
        };  
    }

    componentWillMount(){
    	this.props.navigator.setOnNavigatorEvent((event) => {
	      if (event.id == 'backPress') {
	        //BackHandler.exitApp();
	      }
	    });
    }

	onPress(type){
		this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.OrderList', {type});
	}

	getContent(){
		if (this.props.userRole == CONSTANT.USER_ROLE.COURIER) {
			return this.getContentCourier();
		}
		else{
			return this.getContentOwner();
		}
	}

	getContentCourier(){
		let {summaryOrder} = this.props;
		try{
			return(
				<View>
					<View
						style={{
							width:'100%',
							height:Scaling.verticalScale(CONSTANT.STYLE.CONTAINER.BOX_DASHBOARD.HEIGHT),
							backgroundColor:CONSTANT.STYLE.CONTAINER.BOX_DASHBOARD.BACKGROUND_COLOR,
							paddingTop:Scaling.verticalScale(CONSTANT.STYLE.CONTAINER.BOX_DASHBOARD.PADDING_TOP),
							paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
						}}
					>
						<View
							style={{
								height:'22.5%',
								flexDirection:'row',
							}}
						>
							<View
								style={{
									width:'55%',
									justifyContent:'center',
								}}
							>
								<Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>Status Cucian</Text>
							</View>
							
							<View
								style={{
									width:'45%',
									justifyContent:'center',
									alignItems:'flex-end',
								}}
							>
								
							</View>
						</View>
						
						<View
							style={{
								height:'77.5%',
								alignItems:'center',
								flexDirection:'row',
							}}
						>
							

							<SummaryBox 
								summaryNumber={summaryOrder.pickup} 
								isLate={summaryOrder.pickupLate > 0 ? true : false}
								title={'Penjemputan'} 
								{...this.props}
								onPress={() =>{
									this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.ORDER_LIST, {
										type:CONSTANT.ORDER_ACTIVE_TYPE.PICKUP
									})
								}}
							/>



							<SummaryBox 
								summaryNumber={summaryOrder.delivery} 
								isLate={summaryOrder.deliveryLate > 0 ? true : false}
								title={'Pengantaran'} 
								{...this.props}
								onPress={() =>{
									this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.ORDER_LIST, {
										type:CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY
									})
								}}
							/>
							
						</View>
					</View>
				</View>
			);
		}
		catch(error){
			console.log(error, 'dashboard.summary_order');
			return null;
		}
	}

	getContentOwner(){
		let {summaryOrder} = this.props;
		try{
			return(
				<View>
					<View
						style={{
							width:'100%',
							height:Scaling.verticalScale(CONSTANT.STYLE.CONTAINER.BOX_DASHBOARD.HEIGHT),
							backgroundColor:CONSTANT.STYLE.CONTAINER.BOX_DASHBOARD.BACKGROUND_COLOR,
							paddingTop:Scaling.verticalScale(CONSTANT.STYLE.CONTAINER.BOX_DASHBOARD.PADDING_TOP),
							paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
						}}
					>
						<View
							style={{
								height:'22.5%',
								flexDirection:'row',
							}}
						>
							<View
								style={{
									width:'55%',
									justifyContent:'center',
								}}
							>
								<Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>Status Cucian</Text>
							</View>
							
							<View
								style={{
									width:'45%',
									justifyContent:'center',
									alignItems:'flex-end',
								}}
							>
								{
									summaryOrder.complaint != 0 ? 

									<Button.Standard 
										buttonSize={CONSTANT.STYLE.BUTTON.EXTRA_MEDIUM}
										buttonColor={CONSTANT.COLOR.RED}
										buttonText={summaryOrder.complaint + ' Keluhan Pelanggan!'}
										onPress={() =>{
										this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.ORDER_LIST, {
											type:CONSTANT.ORDER_ACTIVE_TYPE.COMPLAINT
										})
									}}
									/>
									:
									null
								}
								
							</View>
						</View>
						
						<View
							style={{
								height:'77.5%',
								justifyContent:'space-between',
								alignItems:'center',
								flexDirection:'row',
							}}
						>
							
							<SummaryBox 
								summaryNumber={summaryOrder.incoming} 
								title={'Pesanan Baru'} 
								{...this.props}
								onPress={() =>{
									this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.ORDER_LIST, {
										type:CONSTANT.ORDER_ACTIVE_TYPE.INCOMING
									})
								}}
							/>

							<SummaryBox 
								summaryNumber={summaryOrder.pickup} 
								isLate={summaryOrder.pickupLate > 0 ? true : false}
								title={'Penjemputan'} 
								{...this.props}
								onPress={() =>{
									this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.ORDER_LIST, {
										type:CONSTANT.ORDER_ACTIVE_TYPE.PICKUP
									})
								}}
							/>

							<SummaryBox 
								summaryNumber={summaryOrder.inProgress} 
								isLate={summaryOrder.inProgressLate > 0 ? true : false}
								title={'Proses Cuci'} 
								{...this.props}
								onPress={() =>{
									this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.ORDER_LIST, {
										type:CONSTANT.ORDER_ACTIVE_TYPE.IN_PROGRESS
									})
								}}
							/>

							<SummaryBox 
								summaryNumber={summaryOrder.delivery} 
								isLate={summaryOrder.deliveryLate > 0 ? true : false}
								title={'Pengantaran'} 
								{...this.props}
								onPress={() =>{
									this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.ORDER_LIST, {
										type:CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY
									})
								}}
							/>
							
						</View>
					</View>

					<HistoryButton {...this.props} />
				</View>
			);
		}
		catch(error){
			console.log(error, 'dashboard.summary_order');
			return null;
		}
	}

	render(){
		return this.getContent();	
	}
}

class HistoryButton extends Component{
	render(){
		return(
			<TouchableOpacity
				style={{
					width:'100%',
					height:Scaling.verticalScale(100 * 0.444),
					paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
					backgroundColor:CONSTANT.COLOR.BLUE_STATUS_BACKGROUND,
					flexDirection:'row',
					alignItems:'center',
				}}

				onPress={() => {
					this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.ORDER_LIST, {
						type:CONSTANT.ORDER_ACTIVE_TYPE.HISTORY
					})
				}}
			>
				<Image
		            source={CONSTANT.IMAGE_URL.HISTORY_ICON}
		            style={{
		              width:Scaling.scale(CONSTANT.STYLE.IMAGE.NAVBAR_ICON.WIDTH),
		              height:Scaling.verticalScale(CONSTANT.STYLE.IMAGE.NAVBAR_ICON.HEIGHT),
		              resizeMode:'contain',
		              marginRight:Scaling.scale(10)
		            }}
		        /> 

				<Text 
					fontOption={CONSTANT.TEXT_OPTION.BUTTON}
					textStyle={{color:CONSTANT.COLOR.BLUE_STATUS_TEXT}}
				>
				Lihat Riwayat Pesanan
				</Text>

				<Image
		            source={CONSTANT.IMAGE_URL.RIGHT_ARROW_ICON}
		            style={{
		            	tintColor:CONSTANT.COLOR.BLUE_STATUS_TEXT,
			            width:Scaling.scale(CONSTANT.STYLE.IMAGE.NAVBAR_ICON.WIDTH),
			            height:Scaling.verticalScale(CONSTANT.STYLE.IMAGE.NAVBAR_ICON.HEIGHT),
			            resizeMode:'contain',
			            marginRight:Scaling.scale(10),
			            position:'absolute',
			            right:0,
		            }}
		        /> 
			</TouchableOpacity>
		);
	}
}


function mapStateToProps(state, ownProps) {
	return {
		userRole:state.rootReducer.user.role,
		summaryOrder:state.rootReducer.order.summary,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SummaryOrder);

