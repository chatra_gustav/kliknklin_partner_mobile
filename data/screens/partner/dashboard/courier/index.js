import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';

import {LayoutPrimary} from './../../../../layout';
import {TextWrapper, DateFormat, ShipmentScheduleListCourier} from './../../../../components';
import * as STRING from './../../../../string';

import ResolveAssetSource from 'resolveAssetSource';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from "./../../../../actions";

class DashboardCourier extends Component {

  componentWillMount(){
    
  }

	_renderContent(){
		return (
			<View
				style={{
					flex:1,
          marginVertical:Dimensions.get('window').height * 0.08,
				}}
			>
				<View style={{height:this.props.layoutHeight - Dimensions.get('window').height * 0.17}}>
		      		<AssignmentTodayCourier/>
		      	</View>

		      	
		      	<View style={{flex:1,}}>

					<ShipmentScheduleListCourier 
              navigator={this.props.navigator}
		      />

		      	</View>
	      	</View>
		);
	}

	render() {
	return (
		<LayoutPrimary
			showTitle={true}
			showSearchButton={true}
			showNotificationButton={true}
      showTabBar={true}
      tabKey={this.props.tabKey}
      navigator={this.props.navigator}
			titleText={STRING.SCREEN.DASHBOARD_COURIER.TITLE}
		>
	      	{this._renderContent()}
	  	</LayoutPrimary>
	);

	}

}

class AssignmentTodayCourier extends Component {
  render () {
    return (
      <View style={styles.containerHeaderLogin}>
      		<View style={styles.eachStatIndicator}>
            	<TextWrapper type={'smalltitle'} style={{ color: '#FFFFFF', fontWeight:'bold'}}>{'16'}</TextWrapper>
	          	<TextWrapper type={'smallercontent'} style={{ color: '#FFFFFF'}}>{STRING.SCREEN.DASHBOARD_COURIER.AMOUNT_PICKUP_TODAY_LABEL}</TextWrapper>
	        </View>
	        <View style={styles.eachStatIndicator}>
	          	<TextWrapper type={'smalltitle'} style={{ color: '#FFFFFF', fontWeight:'bold'}}>{'8'}</TextWrapper>
	          	<TextWrapper type={'smallercontent'} style={{ color: '#FFFFFF'}}>{STRING.SCREEN.DASHBOARD_COURIER.AMOUNT_DELIVERY_TODAY_LABEL}</TextWrapper>
	        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  containerHeaderLogin:{
  	flex:1,
	flexDirection:'row',
  },
  paddingOnTop: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height * 0.085,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'yellow',
  },
  statIndicator: {
    flex: 1,
    width: Dimensions.get('window').width * 0.85,
    height: Dimensions.get('window').height * 0.10,
    flexDirection: 'row',
    marginVertical: Dimensions.get('window').height * 0.01,
    // backgroundColor: 'grey'
  },
  eachStatIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  dateScheduleTextActive: {
	color:'#61CDFF',
  },
  dateIndicatorWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: Dimensions.get('window').height * 0.02,
    // backgroundColor: 'blue',
  },
  dateIndicator: {
    flex: 0.15,
    width: Dimensions.get('window').width * 0.85,
    height: Dimensions.get('window').height * 0.10,
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    marginVertical: Dimensions.get('window').height * 0.01,
    elevation: 1,
    justifyContent:'center',
  },
  eachDateIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  eachDateIndicatorActive: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    borderBottomWidth: 2,
    borderBottomColor: '#61CDFF', 
    borderStyle: 'dotted'
  },
  shipmentListWrapper: {
    flex: 1,
    width: Dimensions.get('window').width * 0.90,
    height: Dimensions.get('window').height * 0.53,
  },
  timeLineWrapper: {
    flex: 1,
    flexDirection: 'row',
  },
  timeLine: {
    flex: 1,
    flexDirection: 'row',
    // borderRightWidth: 1,
    // borderRightColor: 'grey',
  },
  insideTL: {
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
  },
  dataLine: {
    flex: 9,
    borderLeftWidth: 1,
    borderLeftColor: '#E8E8E8',
    paddingHorizontal: Dimensions.get('window').width * 0.01,
    paddingBottom: Dimensions.get('window').height * 0.015,
    marginTop: Dimensions.get('window').height * 0.015,
  },
  shipmentContainerPickup: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingVertical: Dimensions.get('window').height * 0.02,
    borderRadius: 5,
    marginHorizontal: Dimensions.get('window').width * 0.02,
    borderLeftWidth: 5,
    borderLeftColor: '#61CDFF',
    marginVertical: Dimensions.get('window').height * 0.01,
    elevation: 2,
  },
  shipmentContainerDelivery: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingVertical: Dimensions.get('window').height * 0.02,
    borderRadius: 5,
    marginHorizontal: Dimensions.get('window').width * 0.02,
    borderLeftWidth: 5,
    borderLeftColor: '#AEC93A',
    // borderLeftColor: '#A1D39A',
    marginVertical: Dimensions.get('window').height * 0.01,
    elevation: 2,
  },
  shipment: {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: Dimensions.get('window').width * 0.02,
  },
  addressIndicator: {
    flex: 3,
    justifyContent: 'flex-start',
    alignItems: 'flex-start', 
    marginRight: Dimensions.get('window').width * 0.02,
  },
  statusIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center', 
    // borderColor: 'green',
    borderWidth: 1,
    height: Dimensions.get('window').height * 0.03,
  }

});

function mapStateToProps(state, ownProps) {
	return {
		errorMessage:state.rootReducer.message.error,
		layoutHeight:state.rootReducer.layout.height,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionLogin: bindActionCreators(Actions.login, dispatch),
		actionRoot: bindActionCreators(Actions.root, dispatch),
    actionNavigator: bindActionCreators(Actions.navigator, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(DashboardCourier);

