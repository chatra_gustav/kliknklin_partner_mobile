import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity, Linking, ScrollView, Platform, BackHandler} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import Carousel, {ParallaxImage} from 'react-native-snap-carousel';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";



class QuickOldPosOrder extends Component {
	constructor(props) {
        super(props);
        this.state = {
        };  
    }
	
    _onPress(){
        this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.ATTENDANCE);
    }


	render(){
		try{
            if (this.props.userRole != CONSTANT.USER_ROLE.COURIER) {
                return(
                    <View
                        style={{
                            width:'100%',
                            height:Scaling.verticalScale(150 * 0.444),
                            marginTop:Scaling.verticalScale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_TOP),
                        }}
                    >
                        <TouchableOpacity
                            style={{
                                flex:1,
                                backgroundColor:CONSTANT.COLOR.WHITE,
                                marginHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                                borderColor:CONSTANT.COLOR.LIGHT_GRAY,
                                borderWidth:Scaling.moderateScale(1),
                                borderRadius:7,
                                flexDirection:'row',
                            }}

                            onPress={()=>{
                                this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.POS_OLD_VERSION)
                            }}
                        >
                            <View
                                style={{
                                    width:'80%',
                                    justifyContent:'center',
                                    paddingLeft:Scaling.verticalScale(25 * 0.444),
                                }}
                            >
                                <Text
                                    fontOption={CONSTANT.TEXT_OPTION.BUTTON}
                                    textStyle={{
                                        color:CONSTANT.COLOR.BLUE_STATUS_TEXT,
                                    }}
                                >
                                  Cari Transaksi Pada POS Lama
                                </Text>
                                
                            </View>

                            <View
                                style={{
                                    width:'20%',
                                    justifyContent:'center',
                                }}
                            >
                                <Image
                                    source={CONSTANT.IMAGE_URL.MENU_BURGER_ICON}
                                    style={{
                                      width:Scaling.scale(50),
                                      height:Scaling.verticalScale(50),
                                      resizeMode:'contain',
                                    }}
                                /> 
                            </View>
                            
                        </TouchableOpacity>

                    </View>
                );
            }
            return null;
		}
		catch(error){
			console.log(error, 'dashboard.quick_attendance.index');
			return null;
		}
		
	}
}

function mapStateToProps(state, ownProps) {
	return {
		userRole:state.rootReducer.user.role,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

const styles = StyleSheet.create({
});

export default connect(mapStateToProps, mapDispatchToProps)(QuickOldPosOrder);

