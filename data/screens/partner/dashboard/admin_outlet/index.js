import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity, Linking, ScrollView, Platform, BackHandler} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from './../../../../layout';
import {TextWrapper, DateFormat, ShipmentScheduleListCourier, OutletSelector, Label} from './../../../../components';
import * as STRING from './../../../../string';
import * as CONSTANT from './../../../../constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

class DashboardAdminOutlet extends Component {
	constructor(props) {
        super(props);
        this.state = {
        };  
    }

    componentWillMount(){
    	this.props.navigator.setOnNavigatorEvent((event) => {
	      if (event.id == 'backPress') {
	        //BackHandler.exitApp();
	      }
	    });
    }

	onPress(type){
		this.props.actionNavigator.push(this.props.navigator, 'PartnerKliknKlin.OrderList', {type});
	}

	render(){
		try{
			let {summaryOrder} = this.props;
			return(
				<LayoutPrimary
					showOutletSelector={true}
					showLogo={true}
					showSearchButton={true}
					showNotificationButton={true}
					showTabBar={true}
					navigator={this.props.navigator}
					//titleText={STRING.SCREEN.DASHBOARD_ADMIN_OUTLET.TITLE}
				>
					<View
						style={{
							flex:0.125,
							alignItems:'center',
						}}
					>
						<OutletSelector />
					</View>
					<View
						style={{
							flex:0.25,
							flexDirection:'row',
						}}
					>
						<View
							style={{flex:0.5, alignItems:'flex-start', }}
						>
							<Box
								boxTitle={summaryOrder.incoming}
								boxSubTitle={'Baru'}
								onPress={this.onPress.bind(this, 'incoming')}
							/>
						</View>

						<View
							style={{flex:0.5, alignItems:'flex-end',}}
						>
							<Box
								boxTitle={summaryOrder.pickup}
								boxSubTitle={'Ambil'}
								late={summaryOrder.pickupLate}
								onPress={this.onPress.bind(this, 'pick-up')}
							/>
						</View>
					</View>

					<View
						style={{
							flex:0.25,
							flexDirection:'row',

						}}
					>
						<View
							style={{flex:0.5, alignItems:'flex-start', }}
						>
							<Box
								boxTitle={summaryOrder.inProgress}
								boxSubTitle={'Cuci'}
								important={0}
								late={summaryOrder.inProgressLate}
								onPress={this.onPress.bind(this, 'on-progress')}
							/>
							

							
						</View>

						<View
							style={{flex:0.5, alignItems:'flex-end',}}
						>
							<Box
								boxTitle={summaryOrder.delivery}
								boxSubTitle={'Antar'}
								late={summaryOrder.deliveryLate}
								onPress={this.onPress.bind(this, 'delivery')}
							/>
						</View>
					</View>

					<View
						style={{
							flex:0.25,
							flexDirection:'row',
						}}
					>
						<View
							style={{flex:1,}}
						>
							<LongBox
								boxTitle={summaryOrder.complaint}
								boxSubTitle={'Komplain'}
								isImportant={summaryOrder.isComplaintImportant}
								onPress={this.onPress.bind(this, 'complaint')}
							/>
						</View>
					</View>
			      	
			  	</LayoutPrimary>
			);
		}
		catch(error){
			console.log(error, 'dashboard.admin_outlet');
			return null;
		}
		
	}
}

class Box extends Component{

	renderImportantLabels(){
		if (this.props.important) {
			return(
				<Labels 
					data={this.props.important}
					backgroundColor={'#FFC300'}
				/>
			);
		}
	}

	renderLateLabels(){
		if (this.props.late) {
			return(
				<Labels 
					data={this.props.late}
					backgroundColor={'red'}
				/>
			);
		}
	}

	render(){
		return(
			<View
				style={{
					//justifyContent:'center',
				}}
			>
				
			
				<TouchableOpacity
					{...this.props}
					style={{
						//alignSelf:'stretch',
						width:moderateScale(140),
						height:moderateScale(115),
						elevation:3,
						borderRadius:moderateScale(5),
						backgroundColor:'white',
					}}
				>
					<View
						style={{flex:0.7, alignItems:'center', justifyContent:'flex-end'}}
					>
						<TextWrapper fontWeight={'SemiBold'} type={'gigatitle'}>{this.props.boxTitle}</TextWrapper>
					</View>

					<View
						style={{flex:0.3,alignItems:'center'}}
					>
						<TextWrapper fontWeight={'Medium'} type={'smallcontent'} style={{color:CONSTANT.COLOR.BLUE_A}}>{this.props.boxSubTitle}</TextWrapper>
					</View>
				</TouchableOpacity>

				<View style={{
						position:'absolute',
						top:moderateScale(-5),
						right:moderateScale(-10),
						flexDirection:'row',
						elevation:5,
				}}>
					{this.renderImportantLabels()}
					{this.renderLateLabels()}
				</View>
			</View>
		);
	}
}

class LongBox extends Component{

	renderImportantLabels(){
		if (this.props.important) {
			return(
				<Labels 
					data={this.props.important}
					backgroundColor={'#FFC300'}
				/>
			);
		}
	}

	renderLateLabels(){
		if (this.props.late) {
			return(
				<Labels 
					data={this.props.late}
					backgroundColor={'red'}
				/>
			);
		}
	}

	renderImportantLabel(){
		if (this.props.isImportant) {
			return(
				<Label.TypeImportant/>
			);
		}
	}



	render(){
		return(
				<TouchableOpacity
					{...this.props}
					style={{
						alignSelf:'stretch',
						height:moderateScale(125),
						elevation:3,
						borderRadius:moderateScale(5),
						backgroundColor:'white',
					}}
				>
					<View
						style={{flex:0.7, alignItems:'center', justifyContent:'flex-end'}}
					>
						<TextWrapper fontWeight={'SemiBold'} type={'gigatitle'}>{this.props.boxTitle}</TextWrapper>
					</View>

					<View
						style={{flex:0.3,alignItems:'center'}}
					>
						<TextWrapper fontWeight={'Medium'} type={'smallcontent'} style={{color:CONSTANT.COLOR.BLUE_A}}>{this.props.boxSubTitle}</TextWrapper>
					</View>

					<View style={{
						position:'absolute',
						top:moderateScale(0),
						right:moderateScale(0),
						flexDirection:'row',
						elevation:5,
				}}>
					{this.renderImportantLabel()}
					{this.renderImportantLabels()}
					{this.renderLateLabels()}
				</View>
				</TouchableOpacity>

				
		);
	}
}

class Labels extends Component{
	render(){
		return(
			<View style={{
				width:moderateScale(30),
				height:moderateScale(30),
				borderRadius:moderateScale(30/2),
				elevation:2,
				backgroundColor:this.props.backgroundColor,
				alignItems:'center',
				justifyContent:'center',
			}}>
				<TextWrapper type={'smallercontent'} style={{color:'white'}}>{this.props.data}</TextWrapper>
			</View>
		);
	}
}

function mapStateToProps(state, ownProps) {
	return {
		layoutHeight:state.rootReducer.layout.height,
		summaryOrder:state.rootReducer.order.summary,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(DashboardAdminOutlet);

