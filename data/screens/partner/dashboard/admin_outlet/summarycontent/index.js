import React, { Component } from 'react';
import {View, Dimensions, TouchableOpacity, Image, ScrollView} from 'react-native';

//custom component js class import
import {TextWrapper} from './../../../../../components/text_wrapper';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';

export class SummaryContent extends Component {
	constructor(props) {
        super(props);
        this.state = {
        	image:{
				summary_box:{
					width:0,
					height:0,
					multiplierWidth:0.555,
					multiplierHeight:0.555,
					url:require('./../../../../../image/dashboard/box_icon.png'),
				},
				summary_box_large:{
					width:0,
					height:0,
					multiplierWidth:0.565,
					multiplierHeight:0.565,
					url:require('./../../../../../image/dashboard/box_icon_large.png'),
				},
				grey_notif_box_icon:{
					width:0,
					height:0,
					multiplierWidth:0.65,
					multiplierHeight:0.65,
					url:require('./../../../../../image/dashboard/grey_notif_box_icon.png'),
				},
				red_notif_box_icon:{
					width:0,
					height:0,
					multiplierWidth:0.65,
					multiplierHeight:0.65,
					url:require('./../../../../../image/dashboard/red_notif_box_icon.png'),
				}
        	},
        };  
	}

	componentDidMount(){
		this._getImageSize();
	}

	componentDidUpdate(prevProps, prevState){
	}

	_getImageSize(){
		var image = this.state.image;

		var image_summary_box = ResolveAssetSource(this.state.image.summary_box.url);

		image.summary_box.width = image_summary_box.width;
		image.summary_box.height = image_summary_box.height;

		var image_summary_box_large = ResolveAssetSource(this.state.image.summary_box_large.url);

		image.summary_box_large.width = image_summary_box_large.width;
		image.summary_box_large.height = image_summary_box_large.height;

		var image_grey_notif_box_icon = ResolveAssetSource(this.state.image.grey_notif_box_icon.url);

		image.grey_notif_box_icon.width = image_grey_notif_box_icon.width;
		image.grey_notif_box_icon.height = image_grey_notif_box_icon.height;

		var image_red_notif_box_icon = ResolveAssetSource(this.state.image.red_notif_box_icon.url);

		image.red_notif_box_icon.width = image_red_notif_box_icon.width;
		image.red_notif_box_icon.height = image_red_notif_box_icon.height;

      	this.setState({image});
	}

	_renderContent(){
		if (this.props._getDataSummary()) {
			if (this.props._getDataUserState().role == 'partner') {
				return(
					<View style={{flex:0.5}}>	
						<View style={{flex:0.5, flexDirection:'row'}}>
							<View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
								<SummaryBox
									navigator={this.props.navigator}
									image={this.state.image}
									data={this.props._getDataSummary()}
									type={'pickup'}
									boxType={'large'}
								/>
							</View>
						</View>
						<View style={{flex:0.5, flexDirection:'row', marginTop:Dimensions.get('window').height * 0.015}}>
							<View style={{flex:0.5, justifyContent:'center', alignItems:'center'}}>
								<SummaryBox
									navigator={this.props.navigator}
									image={this.state.image}
									data={this.props._getDataSummary()}
									type={'onprogress'}
								/>
							</View>
							<View style={{flex:0.5, justifyContent:'center', alignItems:'center',}}>
								<SummaryBox
									navigator={this.props.navigator}
									image={this.state.image}
									data={this.props._getDataSummary()}
									type={'delivery'}
								/>
							</View>
						</View>
					</View>
				);
			}
			else if (this.props._getDataUserState().role == 'staff') {
				var isAdminOutlet = false;
				var isCourier = false;
				for (var index in this.props._getDataUserState().user.staff.staffposition) {
					if(this.props._getDataUserState().user.staff.staffposition[index].id == 1){
						isAdminOutlet = true;
					}

					if(this.props._getDataUserState().user.staff.staffposition[index].id == 2){
						isCourier = true;
					}
				}


				if (isAdminOutlet) {
					return (
						<View style={{flex:0.5,}}>	
							<View style={{flex:0.5, flexDirection:'row'}}>
								<View style={{flex:0.5, justifyContent:'center', alignItems:'center'}}>
									<SummaryBox
										navigator={this.props.navigator}
										image={this.state.image}
										data={this.props._getDataSummaryIncoming()}
										type={'incoming'}
									/>
								</View>
								<View style={{flex:0.5, justifyContent:'center', alignItems:'center'}}>
									<SummaryBox
										navigator={this.props.navigator}
										image={this.state.image}
										data={this.props._getDataSummary()}
										type={'pickup'}
									/>
								</View>
							</View>
							<View style={{flex:0.5, flexDirection:'row', marginTop:Dimensions.get('window').height * 0.015}}>
								<View style={{flex:0.5, justifyContent:'center', alignItems:'center'}}>
									<SummaryBox
										navigator={this.props.navigator}
										image={this.state.image}
										data={this.props._getDataSummary()}
										type={'onprogress'}
									/>
								</View>
								<View style={{flex:0.5, justifyContent:'center', alignItems:'center'}}>
									<SummaryBox
										navigator={this.props.navigator}
										image={this.state.image}
										data={this.props._getDataSummary()}
										type={'delivery'}
									/>
								</View>
							</View>
						</View>
					);
				}

				if (isCourier) {
					return(
						<View style={{flex:0.5}}>	
							<View style={{flex:0.5, flexDirection:'row'}}>
								<View style={{flex:1, justifyContent:'center', alignItems:'flex-start', left:7.5}}>
									<SummaryBox
										navigator={this.props.navigator}
										image={this.state.image}
										data={this.props._getDataSummary()}
										type={'pickup'}
										boxType={'large'}
									/>
								</View>
							</View>
							<View style={{flex:0.5, flexDirection:'row', marginTop:5}}>
								<View style={{flex:1, justifyContent:'center', alignItems:'flex-start', left:7.5}}>
									<SummaryBox
										navigator={this.props.navigator}
										image={this.state.image}
										data={this.props._getDataSummary()}
										type={'delivery'}
										boxType={'large'}
									/>
								</View>
							</View>
						</View>
					);
				}
			}
		}
		return null;
	}

	render(){
		return(
			<ScrollView style={{flex:1, backgroundColor:'#F2F2F2', paddingTop:10}}>
				
				<View style={{flex:0.5}}>
					{this._renderContent()}
				</View>
		</ScrollView>
		);
	}
}

class SummaryBox extends Component {
	constructor(props) {
        super(props);
        this.state = {
        	image:this.props.image ? this.props.image : null,
        	data: this.props.data ? this.props.data : null,
        	type:this.props.type ? this.props.type : 'pickup',
        	boxType: this.props.boxType ? this.props.boxType : 'small',
        	string:{
        		title:'',
        	},
        	leftOffset:0,
        };  
	}

	componentWillMount(){
		this._configSummaryBox();
	}

	componentDidUpdate(prevProps, prevState){
	}

	_configSummaryBox(){
		this._configTitle();
	}

	_configTitle(){
		var string = this.state.string;
		string.title = this.state.type.charAt(0).toUpperCase() + this.state.type.slice(1);
		
		if (this.state.type == 'onprogress') {
			string.title = 'On Progress';
		}

		this.setState({string});
	}

	_onPressSummaryBox(){
		var title = this.state.string.title;

		if (this.state.type == 'onprogress') {
			title = 'On Progress';
		}

		this.props.navigator.push({
			id:'OrderList',
			contentType:this.state.type,
			title:'Order ' + title,
		})
	}

	_getMainContent(){
		if (this.state.type == 'incoming') {
			return parseInt(this.props.data);
		}
		else{
			if (Object.keys(this.state.data).length > 0) {
				if (this.state.type == 'pickup') {
					return parseInt(this.props.data.order.pickup) + parseInt(this.props.data.order.pickup_late);
				}
				else if (this.state.type == 'onprogress') {
					return parseInt(this.props.data.order.onprogress) + parseInt(this.props.data.order.onprogress_late);
				}
				else if (this.state.type == 'delivery') {
					return parseInt(this.props.data.order.delivery) + parseInt(this.props.data.order.delivery_late);
				}
			}
		}
		
		return '0';
	}

	_countLateContent(){
		var summary = -1;
		
		if (this.state.type != 'incoming') {
			if (Object.keys(this.props.data).length > 0) {
				if (this.state.type == 'pickup') {
					summary = this.props.data.order.pickup_late;
				}
				else if (this.state.type == 'onprogress') {
					summary = this.props.data.order.onprogress_late;
				}
				else if (this.state.type == 'delivery'){
					summary = this.props.data.order.delivery_late; 
				}
			}
		}
		
		return summary;
	}

	_countImportantContent(){
		var summary = -1;

		if (this.state.type != 'incoming') {
			if (Object.keys(this.props.data).length > 0) {
				if (this.state.type == 'pickup') {
				summary = this.props.data.order.pickup_important;
				}
				else if (this.state.type == 'onprogress') {
					summary = this.props.data.order.onprogress_important;
				}
				else if (this.state.type == 'delivery'){
					summary = this.props.data.order.delivery_important; 
				}
			}
		}
		

		return summary;
	}

	_getLateContent(){
		var summary = this._countLateContent();

		if (summary > 0) {
			if (this.state.type == 'pickup' || this.state.type == 'onprogress' || this.state.type == 'delivery') {
				return(
					<View style={{margin:1, borderColor:'gray',  borderRadius:2.5, alignItems:'center',justifyContent:'center', width:Dimensions.get('window').width * 0.065, height:Dimensions.get('window').width * 0.065, backgroundColor:'#ed1c24'}}>
						<TextWrapper fontSize={7} style={{fontWeight:'bold', color:'white', backgroundColor:'rgba(0,0,0,0)'}}>
							{summary}
						</TextWrapper>
					</View>
				);
			}
		}
		
		return null;	
	}

	_getImportantContent(){
		var summary = this._countImportantContent();

		
		if (summary > 0) {
			if (this.state.type == 'pickup' || this.state.type == 'onprogress' || this.state.type == 'delivery') {
				return(
					<View style={{margin:1, borderColor:'gray', borderRadius:2.5, alignItems:'center',justifyContent:'center', width:Dimensions.get('window').width * 0.065, height:Dimensions.get('window').width * 0.065, backgroundColor:'#e79e2d'}}>
						<TextWrapper fontSize={7} style={{fontWeight:'bold', color:'white', backgroundColor:'rgba(0,0,0,0)'}}>
							{summary}
						</TextWrapper>
					</View>
				);
			}
		}
		
		return null;
	}

	_renderContent(){
		if (this.state.boxType == 'small') {
			return this._renderSmallBox();
		}
		else if (this.state.boxType == 'large') {
			return this._renderLargeBox();
		}
	}

	_renderSmallBox(){
		return(
			<ResponsiveImage
					source={this.state.image.summary_box.url}
					capInsets={{top:5,left:5,bottom:5,right:5}}
					initWidth={this.state.image.summary_box.width * this.state.image.summary_box.multiplierWidth}
					initHeight={this.state.image.summary_box.height * this.state.image.summary_box.multiplierHeight}
				>
				<View style={{flex:1}}>
					<View style={{flex:0.2, justifyContent:'flex-end',flexDirection:'row',right:4, top:2}}>
						{this._getImportantContent()}
						{this._getLateContent()}
					</View>

					<View style={{flex:0.8}}>
						<View style={{flex:0.55, alignItems:'center'}}>
							<TextWrapper fontSize={50} style={{bottom:10, color:'darkslategray', backgroundColor:'rgba(0,0,0,0)', fontWeight:'bold'}}>
								{this._getMainContent()}
							</TextWrapper>
						</View>
						<View style={{flex:0.45, alignItems:'center'}}>
							<TextWrapper type={'subtitle'} style={{color:'#61CDFF', backgroundColor:'rgba(0,0,0,0)',fontWeight:'bold'}}>
								{this.state.string.title}
							</TextWrapper>
						</View>
					</View>
				</View>
			</ResponsiveImage>
		)
	}

	_renderLargeBox(){
		return(
			<ResponsiveImage
					source={this.state.image.summary_box_large.url}
					capInsets={{top:5,left:5,bottom:5,right:5}}
					initWidth={this.state.image.summary_box_large.width * this.state.image.summary_box_large.multiplierWidth}
					initHeight={this.state.image.summary_box_large.height * this.state.image.summary_box_large.multiplierHeight}
				>
				<View style={{flex:1}}>
					<View style={{flex:0.2, justifyContent:'flex-end',flexDirection:'row',right:5, top:2}}>
						{this._getImportantContent()}
						{this._getLateContent()}
					</View>

					<View style={{flex:0.8}}>
						<View style={{flex:0.55, alignItems:'center'}}>
							<TextWrapper fontSize={50} style={{bottom:10, color:'darkslategray', backgroundColor:'rgba(0,0,0,0)', fontWeight:'bold'}}>
								{this._getMainContent()}
							</TextWrapper>
						</View>
						<View style={{flex:0.45, alignItems:'center'}}>
							<TextWrapper type={'subtitle'} style={{color:'#61CDFF', backgroundColor:'rgba(0,0,0,0)',fontWeight:'bold'}}>
								{this.state.string.title}
							</TextWrapper>
						</View>
					</View>
				</View>
			</ResponsiveImage>
		);
	}

	render(){
		return(
			<TouchableOpacity
				onPress={this._onPressSummaryBox.bind(this)}
			>
				{this._renderContent()}
			</TouchableOpacity>
		);
	}
}
