import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity, Linking, ScrollView, Platform, BackHandler, NativeModules, ToastAndroid} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from 'app/data/layout';
import {Text, DateFormat, ShipmentScheduleListCourier, OutletSelector, Label} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

//local
import SummaryOrder from './summary_order';
import Information from './information';
import QuickAttendance from './quick_attendance';
import QuickOldPOSOrder from './quick_old_pos_order';
import NoActiveOutlet from './no_active_outlet';

class Dashboard extends Component {
	constructor(props) {
        super(props);
        this.state = {
        };  
    }

    componentDidMount(){
    }

	getContent(){
		let {userRole} = this.props;

		if (userRole == CONSTANT.USER_ROLE.ADMIN_OUTLET) {

		}

		return <AdminOutletContent {...this.props} />
	}

	render(){
		try{
			let {summaryOrder} = this.props;
			return(
				<LayoutPrimary
					showOutletSelector={true}
					showLogo={true}
					showSearchButton={true}
					showNotificationButton={true}
					showTabBar={true}
					navigator={this.props.navigator}
					//titleText={STRING.SCREEN.DASHBOARD_ADMIN_OUTLET.TITLE}
				>
					<ScrollView
						showsVerticalScrollIndicator={false}
					>
						{this.getContent()}
					</ScrollView>
			  	</LayoutPrimary>
			);
		}
		catch(error){
			console.log(error, 'dashboard.index');
			return null;
		}
		
	}
}

class AdminOutletContent extends Component{
	/*
		<TouchableOpacity style={{marginVertical: 10}} onPress={() => this.props.actionBluetoothDevices.cetak()}>
            		<Text fontOption={CONSTANT.TEXT_OPTION.HEADER_REGULAR} textStyle={{color: CONSTANT.COLOR.LIGHT_BLUE}}>{'POS Sistem Lama'}</Text>
          		</TouchableOpacity>
	*/
	render(){
		let {indexActiveOutlet} = this.props.userOutlet;

		return(
			<View>
				{
					indexActiveOutlet != null ? 
					<SummaryOrder {...this.props} />	
					:
					<NoActiveOutlet {...this.props} />
				}

				<QuickOldPOSOrder {...this.props} />
				
				<QuickAttendance {...this.props} />
				
				<Information {...this.props} />
			</View>
		);
	}
}



function mapStateToProps(state, ownProps) {
	return {
		userRole:state.rootReducer.user.role,
		userOutlet:state.rootReducer.user.outlet,
		orderCreated:state.rootReducer.pos.orderCreated
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
    	actionPOS: bindActionCreators(actions.pos, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

