import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';

import {LayoutPrimary} from './../../../../layout';
import * as CONSTANT from './../../../../constant';
import {TextWrapper, Container, Button, Form} from './../../../../components';
import * as STRING from './../../../../string';

import ResolveAssetSource from 'resolveAssetSource';
import {moderateScale} from 'react-native-size-matters';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

class CheckoutAdminOutlet extends Component {

  componentDidMount(){
  }

	render() {
	return (
		<LayoutPrimary
			showTitle={true}
      showBackButton={true}
      navigator={this.props.navigator}
			titleText={STRING.CHECKOUT_COURIER.TITLE}
		>
        <View
          style={{
            flex:1,
            alignItems:'center',
            marginTop:this.props.layoutHeight * 0.25,
          }}
        >
	      	<Container.TypeA
            contentStyle={{
              minHeight:Dimensions.get('window').height * 0.85,
              marginBottom: Dimensions.get('window').height * 0.1,
            }}
          >

              <View
                style={{minHeight:Dimensions.get('window').height * 0.5}}
              >

                <HeaderItems/>

                <Items 
                  orderItems={this.props.orderDetailData.orderItems}
                />

                <AddItems/>

              </View>



                <View style={{flex:1, alignItems:'center', justifyContent:'flex-end', marginVertical:10,}}>
                    
                    <View style={{
                      alignSelf:'stretch',
                      marginHorizontal:moderateScale(15),
                    }}>
                    <TextWrapper fontWeight={'SemiBold'} type={'smallercontent'}>
                      {'Info Kurir'}
                    </TextWrapper>
                    </View>
                    <View
                      style={{
                        borderRadius:5,
                        borderColor:'gray',
                        borderWidth:1.5,
                        flex:1,
                        alignSelf:'stretch',
                        marginHorizontal:moderateScale(15),
                      }}
                    >
                      <View
                        style={{
                          margin:moderateScale(15),
                        }}
                      >
                          <TextWrapper type={'base'}>{'Tambah Jaket 1'}</TextWrapper>
                      </View>
                    </View>
                    
                    <Button.TypeA
                      buttonColor={CONSTANT.COLOR.BLUE_A}
                      buttonText={STRING.CHECKOUT_COURIER.BUTTON}
                      onPress={this.props.actionCheckoutCourier.submitCheckoutCourier.bind((this.props.navigator))}
                      containerStyle={{height:Dimensions.get('window').height * 0.055, marginTop:Dimensions.get('window').height * 0.05}}
                    />

                </View>
              
          </Container.TypeA>
        </View>
	  </LayoutPrimary>
	);

	}

}

class AddItems extends Component{
  render(){
    return(
        <View style={{
          flexDirection:'row',
          justifyContent:'center',
          marginHorizontal:Dimensions.get('window').width * 0.025,
          marginTop:Dimensions.get('window').height * 0.015,
        }}>

        <View
          style={{flex:0.35,alignItems:'center',}}
        >
            <TouchableOpacity
              style={{ 
                backgroundColor:CONSTANT.COLOR.BLUE_A, 
                padding:5,
                borderRadius:moderateScale(5),
                alignItems:'center',
                width:moderateScale(75),
              }}
            >
              <TextWrapper type={'base'} style={{fontWeight:'bold', color:'white'}}>{'Add Item'}</TextWrapper>
            </TouchableOpacity> 
        </View>

        <View
          style={{flex:0.65}}
        >
        </View>
      </View>
    );
  }
}

class HeaderItems extends Component{
  render(){
    return(
      <View style={{
        flexDirection:'row',
        justifyContent:'center',
        marginHorizontal:Dimensions.get('window').width * 0.025,
        marginTop:Dimensions.get('window').height * 0.015,
      }}>

        <View
          style={{flex:0.35}}
        >
          <TextWrapper type={'base'} style={{fontWeight:'bold'}}>Qty</TextWrapper>
        </View>
        <View
          style={{flex:0.65}}
        >
          <TextWrapper type={'base'} style={{fontWeight:'bold'}}>Produk</TextWrapper>
        </View>
      </View>
    );
  }
} 

class Items extends Component{
  renderAddedService(addedService){
    return addedService.map((data, key) => {
      return (
        <TextWrapper type={'smallbase'} style={{marginTop:2.5}} key={key}>{'+' + data.serviceName}</TextWrapper>
      )
    })
  }

  itemList(){
    return this.props.orderItems.map((data, key) => {
      return (
        <View style={{
          flexDirection:'row',
          justifyContent:'center',
          marginHorizontal:Dimensions.get('window').width * 0.025,
          marginTop:Dimensions.get('window').height * 0.015,
        }}
        key={key}
        >

            <View
              style={{flex:0.35}}
            >
              <Quantity
                data={data}
              />
            </View>
            <View
              style={{flex:0.65}}
            >
              <TextWrapper type={'base'} style={{fontWeight:'bold'}}>{data.itemName}</TextWrapper>
              <TextWrapper type={'smallbase'} style={{marginTop:2.5}}>{data.washingMethodName}</TextWrapper>

              {this.renderAddedService(data.addedService)}
            </View>
        </View>
      )
    })
  }

  render(){
    return (
        <View>
          {this.itemList()}
        </View>
    )
  }
}

class Quantity extends Component{
    render(){
      return(
          <View style={{
            flexDirection:'row',
            alignItems:'center',
          }}>
            <View
              style={{
                flex:0.25,
              }}
            >
              <TouchableOpacity
                  style={{
                      width:moderateScale(20),
                      height:moderateScale(20),
                      borderRadius:moderateScale(20/2),
                      backgroundColor:CONSTANT.COLOR.BLUE_A,
                      alignItems:'center',
                      justifyContent:'center'
                  }}
              >

                  <TextWrapper type={'content'} style={{color:'white'}}>{'-'}</TextWrapper>
              </TouchableOpacity>

            </View>
            
            <View
              style={{
                flex:0.5,
                alignItems:'center',
              }}
            >
            <TextWrapper type={'smallercontent'}>{this.props.data.quantity + ' ' + this.props.data.unitName}</TextWrapper>
            </View>

            <View
              style={{
                flex:0.25,
              }}
            >
            <TouchableOpacity
                style={{
                    width:moderateScale(20),
                    height:moderateScale(20),
                    borderRadius:moderateScale(20/2),
                    backgroundColor:CONSTANT.COLOR.BLUE_A,
                    alignItems:'center',
                    justifyContent:'center'
                }}
            >
              <TextWrapper type={'content'} style={{color:'white'}}>{'+'}</TextWrapper>
            </TouchableOpacity>

            </View>

          </View>
      );
    }
}


const styles = StyleSheet.create({
  textItems:{
    
  },
  textItemsOther:{
    marginTop:2.5,
  },
});

function mapStateToProps(state, ownProps) {
	return {
		orderDetailData:state.rootReducer.order.detail,
		layoutHeight:state.rootReducer.layout.height,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionCheckoutCourier: bindActionCreators(actions.checkoutCourier, dispatch),
		actionRoot: bindActionCreators(actions.root, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutAdminOutlet);

