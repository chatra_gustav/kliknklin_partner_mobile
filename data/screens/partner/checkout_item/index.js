import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList,
} from 'react-native';

import {LayoutPrimary} from './../../../layout';
import * as CONSTANT from './../../../constant';
import {TextWrapper, Container, Button, Form, toCurrency, Scaling} from './../../../components';
import * as STRING from './../../../string';

import ResolveAssetSource from 'resolveAssetSource';
import {moderateScale} from 'react-native-size-matters';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../actions";

class CheckoutItem extends Component {
  
  constructor(props) {
    super(props);
  
    this.state = {
      checkoutItemList: [],
    }
  }

  componentDidMount(){
    this.props.actionOrder.getCheckoutItemList();

    this._updateCheckoutItem();
  }

  _renderFooter(){
    return(
          <FooterContent 
            {...this.props}
            {...this.state}
          />
      );
  }

  _getTitle(){
    let {mode} = this.props;
    if (mode == 'confirm') {
        return STRING.SCREEN.CONFIRM_ITEM.CONFIRM_MODE_TITLE;
    }
    else{
      return STRING.SCREEN.CONFIRM_ITEM.READ_MODE_TITLE;
    }
  }

  _onPressSubmitButton(){
    try{
      let {actionOrder, navigator} = this.props;

      actionOrder.submitCheckoutItem(navigator);
    }
    catch(error){
      console.log(error);
    }
  }

  _updateCheckoutItem(){
    try{
      let {orderData, actionOrder} = this.props;
      let {orderitem, kliknklin_order_flag} = orderData;

      for(let index in orderitem){
        let {id, item_price, quantity, item_duration, kliknklin, outlet} = orderitem[index];

        let kliknklin_item_price_area_id = kliknklin ? kliknklin.kliknklin_item_price_area_id : null;
        let outlet_item_washing_time_id = outlet ? outlet.outlet_item_washing_time_id : null;
        let list_added_services = null;

        actionOrder.updateCheckoutItem(id, item_price, quantity, quantity, item_duration, kliknklin_item_price_area_id, outlet_item_washing_time_id, list_added_services);
      }

    }
    catch(error){
      console.log(error, 'checkout_item._updateCheckoutItem');
    }
  }

	render() {
    try{
      let {orderData, actionOrder} = this.props;

      let {orderitem, orderlaundry} = orderData;

      return (
        <LayoutPrimary
          {...this.props}

          showTitle={true}
          showBackButton={true}
          showTabBar={false}
          title={this._getTitle()}
        >
          <ScrollView>
            <Container.TypeA
                containerStyle={{
                  minHeight:Dimensions.get('window').height * 0.4,
                }}
              >
                  
                  <FlatList
                    data={orderitem}
                    keyExtractor={(item, index) => index+''}
                    renderItem={(items, separator) => { 
                      let {item} = items; 
                      return (
                        <ItemContent 
                          item={item}
                          actionOrder={actionOrder}
                        />);
                    }}
                    ListEmptyComponent={() => {return (<EmptyContent />);}}
                    ListHeaderComponent={() => {return (<HeaderContent />);}}
                  /> 
                  
              </Container.TypeA>

              <DurationContent orderlaundry={orderlaundry}/>

              <CourierNoteContent {...this.props}/>    
              
              <View
                style={{
                  width:'100%',
                  alignItems:'center',
                }}
              >
                <Button.Standard
                  buttonSize={CONSTANT.STYLE.BUTTON.LARGE}
                  buttonColor={CONSTANT.COLOR.LIGHT_BLUE}
                  fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}
                  buttonText={'Terima Laundry'}
                  onPress={this._onPressSubmitButton.bind(this)}
                  buttonStyle={{marginTop:Scaling.verticalScale(10)}}
                />
              </View>
       
          </ScrollView>
        </LayoutPrimary>
      );
    }
    catch(error){
      console.log(error);
      return null;
    }

  }
}

class DurationContent extends Component{
  render(){
    try {
        let data = ['asd'];
        let {orderlaundry} = this.props;
        let {timeservice, time_service_id} = orderlaundry;
        let info = ''
        if (timeservice) {
          info = timeservice.info;
        }

        return(
          <View style={{
            flex:1,
            marginTop:moderateScale(10),
            height:Dimensions.get('window').height * 0.125,
            marginHorizontal:Dimensions.get('window').width * 0.025,
          }}>
            <View
              style={{flex:0.3,
                justifyContent:'flex-end'
              }}
            >
              <TextWrapper type={'smallbase'} fontWeight={'SemiBold'}>Durasi</TextWrapper>
            </View>
            
            <View
              style={{flex:0.7, justifyContent:'center'}}
            >
            <Container.TypeA
                containerStyle={{
                  minHeight:0,
                  height:Dimensions.get('window').height * 0.075,
                }}
                contentStyle={{
                  flex:1,
                  flexDirection:'row',
                  alignItems:'center',
                  padding:0,
                }}
            >
              <FlatList
                data={data}
                keyExtractor={(item, index) => index+''}
                renderItem={(items, separator) => { 
                  let {item} = items; 
                  return (
                    <TouchableOpacity
                      style={{
                        flex:1,
                        height:moderateScale(25),
                        flexDirection:'row',
                        alignItems:'center',
                      }}
                    >
                    <View
                      style={{
                          flex:0.15,
                          justifyContent:'center',
                      }}
                    >
                      <View
                        style={{
                          borderRadius:moderateScale(30),
                          borderWidth:1,
                          borderColor:CONSTANT.COLOR.BLUE_A,
                          backgroundColor:CONSTANT.COLOR.BLUE_A,
                          width:moderateScale(15),
                          height:moderateScale(15),
                        }}
                      />
                      
                    </View>
                     <View
                        style={{
                          flex:0.5,
                          justifyContent:'center',
                        }}
                      >
                        <TextWrapper type={'smallercontent'}>{info}</TextWrapper>
                      </View>
                      <View
                        style={{
                          flex:0.35,
                          justifyContent:'center',
                        }}
                      >
                        <TextWrapper type={'smallercontent'}>{orderlaundry.expected_duration +' Jam'}</TextWrapper>
                      </View>
                    </TouchableOpacity>
                  );
                }}
                contentContainerStyle={{
                  //flex:1,
                }}
                //ListEmptyComponent={() => {return (<EmptyContent />);}}
                //ListHeaderComponent={() => {return (<HeaderContent />);}}
              /> 
            </Container.TypeA>
            </View>
          </View>
        );
    }
    catch(error){
      return null;
    }
  }
}

class CourierNoteContent extends Component{
  _getCourierNotes(){
    let result = 'Tidak ada catatan tambahan'
    let {shipmentActivePickup} = this.props;
    let {internalshipment} = shipmentActivePickup;
    let {courier_notes} = internalshipment;
    
    if (courier_notes) {
      result = courier_notes;
    }

    return result;
  }

  render(){
    try {
        return(
          <View style={{
            flex:1,
            marginTop:moderateScale(10),
            height:Dimensions.get('window').height * 0.2,
            marginHorizontal:Dimensions.get('window').width * 0.025,
          }}>
            <View
              style={{flex:0.25,
                justifyContent:'flex-end'
              }}
            >
              <TextWrapper type={'smallbase'} fontWeight={'SemiBold'}>Catatan Kurir</TextWrapper>
            </View>
            
            <View
              style={{flex:0.75, justifyContent:'center'}}
            >

            <Container.TypeA
                containerStyle={{
                  minHeight:0,
                  height:Dimensions.get('window').height * 0.125,
                }}
                contentStyle={{
                  flex:1,
                  flexDirection:'row',
                  alignItems:'center',
                  padding:0,
                }}
            >
              <View
                style={{
                  flex:1,
                }}
              >
                <TextWrapper type={'smallercontent'}>{this._getCourierNotes()}</TextWrapper>
              </View>
            </Container.TypeA>
            </View>
          </View>
        );
    }
    catch(error){
      console.log(error);
      return null;
    }
  }
}

class HeaderContent extends Component{
  render(){
    return(
      <View style={styleTable.container}>
        <View
          style={styleTable.containerFirstSection}
        >
          <TextWrapper fontWeight={'SemiBold'} type={'smallestcontent'} style={styleTable.headerItemText}>Jumlah</TextWrapper>
        </View>

        <View
          style={styleTable.containerSecondSection}
        >
          <TextWrapper fontWeight={'SemiBold'} type={'smallestcontent'} style={styleTable.headerItemText}>Produk</TextWrapper>
        </View>
        
        <View
          style={styleTable.containerThirdSection}
        >
          <TextWrapper fontWeight={'SemiBold'} type={'smallestcontent'} style={styleTable.headerItemText}>Harga</TextWrapper>
        </View>
      </View>
    );
  }
} 

class FooterContent extends Component{
  constructor(props){
    super(props);
    this.state = {
        courierNote:'',
    };
  }

  _onChangeTextCourierNote(courierNote){
    this.setState({courierNote});
  }

  _onPressSubmitButton(){
    try{
      let {actionOrder, navigator} = this.props;
      let {courierNote} = this.state;

      actionOrder.submitCheckoutItem(navigator, courierNote);
    }
    catch(error){
      console.log(error);
    }
  }
  render(){
    try{
      
      return(
        <View style={{
          flex:1, 
          alignItems:'center', 
          justifyContent:'flex-end',
          height:Dimensions.get('window').height * 0.075,
        }}>
            <Form.InputField.TypeB
              keyboardType={'visible-password'}
              ref='username_login' 
              placeholder={'Tambahkan catatan bila ada perubahan'}
              topLabel={true}
              topLabelText={'Catatan Tambahan'}
              onChangeText={this._onChangeTextCourierNote.bind(this)}
            />

            <Button.TypeA
              buttonColor={CONSTANT.COLOR.BLUE_A}
              buttonText={STRING.CHECKOUT_COURIER.BUTTON}
              onPress={this._onPressSubmitButton.bind(this)}
              containerStyle={{
                marginTop:moderateScale(10),
              }}
            />

        </View>
      );
    }
    catch(error){
      return null;
    }
  }
}

class ItemContent extends Component{
  constructor(props) {
    super(props);
  
    this.state = {
      itemQuantity: 0,
    }
  }

  
  componentDidMount(){
    try{
        let {item, actionOrder} = this.props;
        let {outlet, id, item_price, quantity, item_duration} = item;

        this._setItemQuantity(quantity);
    }
    catch(error){

    }
  }

  _setItemQuantity(amount){
    this.setState({itemQuantity:amount});
  }

  render(){
    return (
        <View style={styleTable.container}>

            <View
              style={styleTable.containerFirstSection}
            >
                <QuantityContent
                  {...this.props}
                  {...this.state}
                  _setItemQuantity={this._setItemQuantity.bind(this)}
                />   
            </View>

            <View
              style={styleTable.containerSecondSection}
            >
                <ProductContent
                  {...this.props}
                />
            </View>

            <View
              style={styleTable.containerThirdSection}
            >
                <PriceContent
                  {...this.props}
                  {...this.state}
                />
            </View>
        </View>
    )
  }
}

class EmptyContent extends Component{
  render(){
    return(
      <View/>
    );
  }
}

class ProductContent extends Component{
  render(){
    try{
      let {item} = this.props;

      let {outlet, kliknklin} = item;
     
      let itemName = '';
      let washingServiceInfo = '';
      let timeServiceInfo = '';
      let itemAddedService = [];
      let isOnDemand = false;

      if (outlet) {
          let {outletitemwashingtime, choosenaddedservice} = outlet;
          let {itemwashingtime, itemaddedservice} = outletitemwashingtime;
          let data = itemwashingtime.item;
          let {washingservice, timeservice, item} = itemwashingtime;
          itemName = item.name;
          washingServiceInfo = washingservice.info;
          timeServiceInfo = timeservice.info;
          itemAddedService = choosenaddedservice;
      }
      else if (
        kliknklin
      ) {
        let {itempricearea, choosenaddedservice} = kliknklin;
        let {itemwashingtime, itemaddedservice} = itempricearea;
        let data = itemwashingtime.item;
        let {washingservice, timeservice, item} = itemwashingtime;
        itemName = item.name;
        washingServiceInfo = washingservice.info;
        timeServiceInfo = timeservice.info;
        itemAddedService = choosenaddedservice;

        isOnDemand = true;
      }
    
      return(
        <View>
          <TextWrapper type={'base'} fontWeight={'SemiBold'}>{itemName}</TextWrapper>
          <TextWrapper type={'smallbase'} style={{marginTop:2.5}}>{washingServiceInfo + ' - ' + timeServiceInfo}</TextWrapper>
    
          
          <View
            style={{
              marginLeft:moderateScale(10),
            }}
          >
              <FlatList
                data={itemAddedService}
                keyExtractor={(item, index) => index+''}
                renderItem={(items, separator) => { 
                  let {item} = items; 
                  return (<AddedServiceContent isOnDemand={isOnDemand} item={item}/>);
                }}
                ListHeaderComponent={() => {
                  if (itemAddedService.length > 0) {
                    return (<HeaderAddedServiceContent />);
                  }
                  return null;
                }}
              /> 
          </View>
          
        </View>
      );
    }
    catch(error){
      console.log(error, 'before_checkout.ProductContent')
      return null;
    }
  }
}

class QuantityContent extends Component{
    render(){
      try{
        let {item, actionOrder, _setItemQuantity, itemQuantity} = this.props;
        let {id, item_price, quantity, item_duration, kliknklin, outlet} = item;
        let itemUnitMeasurement = '';
        let itemMaxQuantity = 5;

        let kliknklin_item_price_area_id = kliknklin ? kliknklin.kliknklin_item_price_area_id : null;
        let outlet_item_washing_time_id = outlet ? outlet.outlet_item_washing_time_id : null;
        let list_added_services = null;

        if (outlet) {
            let {outletitemwashingtime, choosenaddedservice} = outlet;
            let {itemwashingtime, itemaddedservice} = outletitemwashingtime;
            let data = itemwashingtime.item;
            let {washingservice, timeservice, item} = itemwashingtime;
            itemUnitMeasurement = item.unit_measurement;
            itemMaxQuantity = item.measurement.max_quantity;
        }
        else if (
          kliknklin
        ) {
            let {itempricearea, choosenaddedservice} = kliknklin;
            let {itemwashingtime, itemaddedservice} = itempricearea;
            let data = itemwashingtime.item;
            let {washingservice, timeservice, item} = itemwashingtime;
            itemUnitMeasurement = item.unit_measurement;
            itemMaxQuantity = item.measurement.max_quantity;
        }

        return(
          <View
            style={{
              flexDirection:'row',
              flex:1,
            }}
          >
            <View
              style={{
                flex:0.25,
                justifyContent:'center',
              }}
            >
              <TouchableOpacity
                  style={{
                      width:moderateScale(20),
                      height:moderateScale(20),
                      borderRadius:moderateScale(20/2),
                      backgroundColor:CONSTANT.COLOR.BLUE_A,
                      alignItems:'center',
                      justifyContent:'center'
                  }}

                  onPress={() => {
                    let updatedQuantity = itemQuantity - 1;

                    if (updatedQuantity >= 0) {
                        _setItemQuantity(updatedQuantity);

                        actionOrder.updateCheckoutItem(id, item_price, itemQuantity, updatedQuantity, item_duration, kliknklin_item_price_area_id, outlet_item_washing_time_id, list_added_services);
                    }
                  }}
              >
                  <TextWrapper type={'content'} style={{color:'white'}}>{'-'}</TextWrapper>
              </TouchableOpacity>
            </View>

            <View
              style={{
                flex:0.5,
                alignItems:'center',
                justifyContent:'center',
              }}
            >
              <View
                style={{
                  borderWidth:1,
                  borderRadius:moderateScale(20),
                  width:moderateScale(45),
                  height:moderateScale(25),
                  borderColor:CONSTANT.COLOR.GRAY_B,
                  alignItems:'center',
                  justifyContent:'center',
                }}
              >
                <TextWrapper type={'base'} fontWeight={'SemiBold'}>{itemQuantity + ' ' + itemUnitMeasurement}</TextWrapper>
            
              </View>
              
            </View>

            <View
              style={{
                flex:0.25,
                alignItems:'center',
                justifyContent:'center',
              }}
            >
              <TouchableOpacity
                  style={{
                      width:moderateScale(20),
                      height:moderateScale(20),
                      borderRadius:moderateScale(20/2),
                      backgroundColor:CONSTANT.COLOR.BLUE_A,
                      alignItems:'center',
                      justifyContent:'center'
                  }}

                  onPress={() => {
                    let updatedQuantity = itemQuantity + 1;

                    if (updatedQuantity <= itemMaxQuantity) {
                        _setItemQuantity(updatedQuantity);

                        actionOrder.updateCheckoutItem(id, item_price, itemQuantity, updatedQuantity, item_duration, kliknklin_item_price_area_id, outlet_item_washing_time_id, list_added_services);
                    }
                  }}
              >
                  <TextWrapper type={'content'} style={{color:'white'}}>{'+'}</TextWrapper>
              </TouchableOpacity>
            </View>
              

              

              
          </View>
        );
      }
      catch(error){
        console.log(error, 'checkout_item.QuantityContent')
        return null;
      }
    }
}

class PriceContent extends Component{
  render(){
    try{
      let {item, itemQuantity} = this.props;
      let {kliknklin, outlet} = item;
      let itemPrice = '';

      if (outlet) {
          let {outletitemwashingtime} = outlet;
          let {price} = outletitemwashingtime;
          itemPrice = price;
      }
      else if (
        kliknklin
      ) {
          let {itempricearea, choosenaddedservice} = kliknklin;
          let {price} = itempricearea;
          itemPrice = price;
      }

      return(
        <View>
          <TextWrapper type={'base'} fontWeight={'SemiBold'}>{toCurrency(itemPrice * itemQuantity, 'Rp ')}</TextWrapper>
    
        </View>
      );
    }
    catch(error){
      return null;
    }
  }
}

class QuantityContents extends Component{
    render(){
      let {item} = this.props;
      return(
          <View style={{
            flexDirection:'row',
            alignItems:'center',
          }}>
            <View
              style={{
                flex:0.25,
              }}
            >
              <TouchableOpacity
                  style={{
                      width:moderateScale(20),
                      height:moderateScale(20),
                      borderRadius:moderateScale(20/2),
                      backgroundColor:CONSTANT.COLOR.BLUE_A,
                      alignItems:'center',
                      justifyContent:'center'
                  }}
              >

                  <TextWrapper type={'content'} style={{color:'white'}}>{'-'}</TextWrapper>
              </TouchableOpacity>

            </View>
            
            <View
              style={{
                flex:0.5,
                alignItems:'center',
              }}
            >
            <TextWrapper type={'smallercontent'}>{this.props.data.quantity + ' ' + this.props.data.unitName}</TextWrapper>
            </View>

            <View
              style={{
                flex:0.25,
              }}
            >
            <TouchableOpacity
                style={{
                    width:moderateScale(20),
                    height:moderateScale(20),
                    borderRadius:moderateScale(20/2),
                    backgroundColor:CONSTANT.COLOR.BLUE_A,
                    alignItems:'center',
                    justifyContent:'center'
                }}
            >
              <TextWrapper type={'content'} style={{color:'white'}}>{'+'}</TextWrapper>
            </TouchableOpacity>

            </View>

          </View>
      );
    }
}

class AddedServiceContent extends Component{
    render(){
      try{
        let {item, isOnDemand} = this.props;
        let info = '';

        if (isOnDemand) {
          let {kliknklinaddeditem} = item;
          let {itemaddedservice} = kliknklinaddeditem;
          let {mdaddedservice} = itemaddedservice;

          info = mdaddedservice.info;
        }
        else{
          let {outletaddeditem} = item;
          let {detail} = outletaddeditem;
          let {mdaddedservice} = detail;

          info = mdaddedservice.info;
        }

        return(
          <TextWrapper type={'smallbase'} style={{marginTop:0}} >
            {info}
          </TextWrapper>
        );
      }
      catch(error){
        return null;
      }
    }
}

class HeaderAddedServiceContent extends Component{
  render(){
    return(
      <View>
        <TextWrapper type={'smallbase'} fontWeight={'SemiBold'} style={{marginTop:3, color:CONSTANT.COLOR.GRAY_C}}>{'Servis Tambahan'}</TextWrapper>
      </View>
    );
  }
}

class AddItems extends Component{
  render(){
    return(
        <View style={{
          flexDirection:'row',
          justifyContent:'center',
          marginHorizontal:Dimensions.get('window').width * 0.025,
          marginTop:Dimensions.get('window').height * 0.015,
        }}>

        <View
          style={{flex:0.35,alignItems:'center',}}
        >
            <TouchableOpacity
              style={{ 
                backgroundColor:CONSTANT.COLOR.BLUE_A, 
                padding:5,
                borderRadius:moderateScale(5),
                alignItems:'center',
                width:moderateScale(75),
              }}
            >
              <TextWrapper type={'base'} style={{fontWeight:'bold', color:'white'}}>{'Add Item'}</TextWrapper>
            </TouchableOpacity> 
        </View>

        <View
          style={{flex:0.65}}
        >
        </View>
      </View>
    );
  }
}



const styleTable = StyleSheet.create({
  container:{
    flex:1,
    flexDirection:'row',
  },
  containerFirstSection:{
    flex:0.3,
    alignItems:'center',
    justifyContent:'center',
  },
  containerSecondSection:{
    flex:0.5,
  },
  containerThirdSection:{
    flex:0.2
  },
  headerItemText:{
    color:CONSTANT.COLOR.GRAY_C,
  },
  contentItemText:{
    color:CONSTANT.COLOR.BLACK_A,
  },
});


const styles = StyleSheet.create({
  textItems:{
    
  },
  textItemsOther:{
    marginTop:2.5,
  },
});

function mapStateToProps(state, ownProps) {
	return {
    orderData:state.rootReducer.order.active.data,
    shipmentActivePickup: state.rootReducer.order.shipmentActivePickup,
	};
}

function mapDispatchToProps(dispatch) {
	return {
    actionOrder: bindActionCreators(actions.order, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutItem);

