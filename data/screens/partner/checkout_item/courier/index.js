import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';

import {LayoutPrimary} from './../../../../layout';
import * as CONSTANT from './../../../../constant';
import {TextWrapper, Container, Button, Form} from './../../../../components';
import * as STRING from './../../../../string';

import ResolveAssetSource from 'resolveAssetSource';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "./../../../../actions";

class CheckoutCourier extends Component {

  componentDidMount(){
  }

	render() {
	return (
		<LayoutPrimary
			showTitle={true}
      showBackButton={true}
      navigator={this.props.navigator}
			titleText={STRING.CHECKOUT_COURIER.TITLE}
		>
        <View
          style={{
            flex:1,
            alignItems:'center',
            marginTop:this.props.layoutHeight * 0.25,
          }}
        >
	      	<Container.TypeA
            contentStyle={{
              minHeight:Dimensions.get('window').height * 0.85,
              marginBottom: Dimensions.get('window').height * 0.1,
            }}
          >

              <View
                style={{minHeight:Dimensions.get('window').height * 0.5}}
              >

                <HeaderItems/>

                <Items 
                  orderItems={this.props.orderDetailData.orderItems}
                />

              </View>

                <View style={{flex:1, alignItems:'center', justifyContent:'flex-end', marginVertical:10,}}>

                    <Form.InputField.TypeB
                      keyboardType={'default'}
                      ref='username_login' 
                      placeholder={'Tambahkan catatan bila ada perubahan'}
                      topLabel={true}
                      topLabelText={'Catatan Tambahan'}
                      //onChangeText={this._onChangeEmail.bind(this)}
                    />

                    <Button.TypeA
                      buttonColor={CONSTANT.COLOR.BLUE_A}
                      buttonText={STRING.CHECKOUT_COURIER.BUTTON}
                      onPress={this.props.actionCheckoutCourier.submitCheckoutCourier.bind((this.props.navigator))}
                      containerStyle={{height:Dimensions.get('window').height * 0.075, marginTop:Dimensions.get('window').height * 0.05}}
                    />

                </View>
              
          </Container.TypeA>
        </View>
	  </LayoutPrimary>
	);

	}

}

class HeaderItems extends Component{
  render(){
    return(
      <View style={{
        flexDirection:'row',
        justifyContent:'center',
        marginHorizontal:Dimensions.get('window').width * 0.025,
        marginTop:Dimensions.get('window').height * 0.015,
      }}>

        <View
          style={{flex:0.25}}
        >
          <TextWrapper type={'base'} style={{fontWeight:'bold'}}>Qty</TextWrapper>
        </View>
        <View
          style={{flex:0.75}}
        >
          <TextWrapper type={'base'} style={{fontWeight:'bold'}}>Produk</TextWrapper>
        </View>
      </View>
    );
  }
} 

class Items extends Component{
  renderAddedService(addedService){
    return addedService.map((data, key) => {
      return (
        <TextWrapper type={'smallbase'} style={{marginTop:2.5}} key={key}>{'+' + data.serviceName}</TextWrapper>
      )
    })
  }

  itemList(){
    return this.props.orderItems.map((data, key) => {
      return (
        <View style={{
          flexDirection:'row',
          justifyContent:'center',
          marginHorizontal:Dimensions.get('window').width * 0.025,
          marginTop:Dimensions.get('window').height * 0.015,
        }}
        key={key}
        >

            <View
              style={{flex:0.25}}
            >
              <TextWrapper type={'base'}>{data.quantity + ' ' + data.unitName}</TextWrapper>
            </View>
            <View
              style={{flex:0.75}}
            >
              <TextWrapper type={'base'} style={{fontWeight:'bold'}}>{data.itemName}</TextWrapper>
              <TextWrapper type={'smallbase'} style={{marginTop:2.5}}>{data.washingMethodName}</TextWrapper>

              {this.renderAddedService(data.addedService)}
            </View>
        </View>
      )
    })
  }

  render(){
    return (
        <View>
          {this.itemList()}
        </View>
    )
  }
}


const styles = StyleSheet.create({
  textItems:{
    
  },
  textItemsOther:{
    marginTop:2.5,
  },
});

function mapStateToProps(state, ownProps) {
	return {
		orderDetailData:state.rootReducer.order.detail,
		layoutHeight:state.rootReducer.layout.height,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionCheckoutCourier: bindActionCreators(actions.checkoutCourier, dispatch),
		actionRoot: bindActionCreators(actions.root, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutCourier);

