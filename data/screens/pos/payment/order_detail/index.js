import React, { Component } from 'react';
import {View, Image, StyleSheet, Dimensions, TouchableOpacity, TouchableWithoutFeedback} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

//node js class import

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling, toCurrency, Form,} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

//local
import Container from './../container';

class OrderDetail extends Component{

    getTotalPrice(){
        
    }

    render(){
        try{
            let {posSelectedPromo, posSelectedItem, posTotalPrice, posSelectedCustomer, posSelectedDuration} = this.props;
            
            let promoDetail = null;
            let discountPromo = 0;
            let orderTotalPrice = posTotalPrice;
            
            if (posSelectedPromo && posSelectedPromo.valid) {
                discountPromo = posSelectedPromo.discount_price * -1;
                orderTotalPrice += discountPromo;

                let leftComponent = (
                    <View
                        style={{
                            flexDirection:'row',
                        }}
                    >
                        <View
                            style={{
                                flex:0.175,
                            }}
                        >
                            <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>{'1 pcs'}</Text>
                        </View>

                        <View
                            style={{
                                flex:0.825,
                                justifyContent:'center',
                            }}
                        >
                            <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>{posSelectedPromo.promo_source}</Text>
                        </View>
                    </View>
                );

                let rightComponent = (
                    <View
                        style={{
                            flexDirection:'row',
                        }}
                    >
                        <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>{toCurrency(-1 * posSelectedPromo.discount_price, 'Rp' )}</Text>
                    </View>
                );

                promoDetail = (
                    <Items
                        {...this.props} 
                        //key={item.outletItemWashingTimeID} 
                        leftComponent={leftComponent}
                        rightComponent={rightComponent}
                    />
                );
            }

            return(
                    <Container
                        containerStyle={{
                            flex:1,
                        }}
                    >   
                        <View
                            style={{

                            }}
                        >
                            <KeyboardAwareScrollView
                                contentContainerStyle={{
                                    paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                                }}
                            >
                                <Items
                                    {...this.props} 
                                    containerStyle={{
                                        marginBottom:Scaling.verticalScale(5),
                                        marginTop:Scaling.verticalScale(54 * 0.444),
                                    }}
                                    leftComponent= {
                                        <View style={{width:'100%', flexDirection:'row'}}>
                                            <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>
                                                {(posSelectedCustomer ? posSelectedCustomer.name : null)}
                                            </Text>
                                        </View>
                                    }
                                    rightComponent={<Text fontOption={CONSTANT.TEXT_OPTION.CONTENT}>{'Durasi : ' + posSelectedDuration + ' Jam'}</Text>}
                                />

                                {posSelectedItem.map((item) => {
                                    let leftComponent = (
                                        <View
                                            style={{
                                                flexDirection:'row',
                                            }}
                                        >
                                            <View
                                                style={{
                                                    flex:0.175,
                                                }}
                                            >
                                                <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>{item.quantity + ' ' + item.unitName}</Text>
                                            </View>

                                            <View
                                                style={{
                                                    flex:0.825,
                                                    justifyContent:'center',
                                                }}
                                            >
                                                <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>{item.name}</Text>
                                            </View>
                                        </View>
                                    );

                                    let rightComponent = (
                                        <View
                                            style={{
                                                flexDirection:'row',
                                            }}
                                        >
                                            <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>{toCurrency(item.selectedOutletItemWashingTime.price * item.quantity , 'Rp' )}</Text>
                                        </View>
                                    );

                                    return(
                                        <Items
                                            {...this.props} 
                                            key={item.outletItemWashingTimeID} 
                                            leftComponent={leftComponent}
                                            rightComponent={rightComponent}
                                        />
                                    )
                                })}


                                {promoDetail}
                                    
                            </KeyboardAwareScrollView>
                        </View>

                        <View
                            style={{
                                //position:'absolute',
                                //bottom:0,
                                width:'100%',
                                height:Scaling.verticalScale(120),
                                paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                                marginVertical:Scaling.verticalScale(10),
                            }}
                        >
                            <View
                                style={{
                                    width:'100%',
                                    marginBottom:Scaling.verticalScale(10),
                                    borderBottomWidth:Scaling.moderateScale(1),
                                    borderColor:CONSTANT.COLOR.GRAY,
                                }}
                            />

                            <Items
                                {...this.props} 
                                leftComponent= {
                                    <View style={{width:'100%', flexDirection:'row'}}>
                                        <View style={{width:'35%'}} />
                                            
                                        <View
                                            style={{width:'65%'}}
                                        >
                                            <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>
                                                {'Total Biaya Cucian'}
                                            </Text>
                                        </View>
                                    </View>
                                }
                                rightComponent={<Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>{toCurrency(posTotalPrice , 'Rp' )}</Text>}
                            />

                            <Items
                                {...this.props} 
                                leftComponent= {
                                    <View style={{width:'100%', flexDirection:'row'}}>
                                        <View style={{width:'35%'}} />
                                            
                                        <View
                                            style={{width:'65%'}}
                                        >
                                            <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>
                                                {'Pengiriman - Ambil Sendiri'}
                                            </Text>
                                        </View>
                                    </View>
                                }
                                rightComponent={<Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>{toCurrency(0 , 'Rp' )}</Text>}
                            />

                            <Items
                                {...this.props} 
                                leftComponent= {
                                    <View style={{width:'100%', flexDirection:'row'}}>
                                        <View style={{width:'35%'}} />
                                            
                                        <View
                                            style={{width:'65%'}}
                                        >
                                            <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>
                                                {'Total Diskon'}
                                            </Text>
                                        </View>
                                    </View>
                                }
                                rightComponent={<Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}>{toCurrency(discountPromo , 'Rp' )}</Text>}
                            />

                            <Items
                                {...this.props} 
                                leftComponent= {
                                    <View style={{width:'100%', flexDirection:'row'}}>
                                        <View style={{width:'35%'}} />
                                            
                                        <View
                                            style={{width:'65%'}}
                                        >
                                            <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>
                                                {'TOTAL BAYAR'}
                                            </Text>
                                        </View>
                                    </View>
                                }
                                rightComponent={<Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>{toCurrency(orderTotalPrice , 'Rp' )}</Text>}
                            />
                        </View>
                    </Container>
            );
        }
        catch(error) {
            console.log(error);
            return null;
        }
    }
}

class Items extends Component{
    static defaultProps = {
      leftComponent: null,
      rightComponent: null,
      containerStyle: {},
    }
    render(){
        try{
            let {leftComponent, rightComponent, containerStyle} = this.props;
            return (
                <View
                    style={[{
                        minHeight:Scaling.verticalScale(50 * 0.444),
                        width:'100%',
                        marginTop:Scaling.verticalScale(5 * 0.444),
                        flexDirection:'row',
                    }, containerStyle]}
                >
                    <View
                        style={{
                            flex:0.725
                        }}
                    >
                        {leftComponent}
                    </View>

                    <View
                        style={{
                            flex:0.275
                        }}
                    >
                        {rightComponent}
                    </View>
                </View>
            );
        }
        catch(error){
            console.log(error);
            return null;
        }
    }
}


function mapStateToProps(state, ownProps) {
    return {
        userRole:state.rootReducer.user.role,
        posSelectedItem:state.rootReducer.pos.selectedItem,
        posAvailableDuration:state.rootReducer.pos.availableDuration,
        posTotalPrice:state.rootReducer.pos.totalPrice,
        posSelectedDuration:state.rootReducer.pos.selectedDuration,
        posSelectedCustomer:state.rootReducer.pos.selectedCustomer,
        posSelectedPromo:state.rootReducer.pos.selectedPromo,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actionPOS: bindActionCreators(actions.pos, dispatch),
        actionRoot: bindActionCreators(actions.root, dispatch),
        actionNavigator: bindActionCreators(actions.navigator, dispatch),
    };
}

const styles = StyleSheet.create({
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetail);
