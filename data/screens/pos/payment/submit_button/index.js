import React, { Component } from 'react';
import {View, StyleSheet, imensions, ScrollView, Platform, TouchableOpacity} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import Carousel, {ParallaxImage} from 'react-native-snap-carousel';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling, toCurrency} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class SubmitButton extends Component {
    static defaultProps = {
      isHide: false,
    }
	constructor(props) {
        super(props);
        this.state = {
            disabled:false,
        };  
    }

    _onPressSubmitButton(){
        this.setState({disabled:true}, async () => {
            const result = await this.props.actionPOS.submitOrderPOS(this.props.navigator); 

            if (!result) {
                this.setState({disabled:false});
            }
        });
    }
	
	render(){
        try{
            let {posSelectedItem, posTotalPrice, isHide} = this.props;
            
            if (isHide && posSelectedItem.length > 0) {
                return(
                    <View
                        style={{
                            width:'100%',
                            height:Scaling.verticalScale(200 * 0.444),
                            alignItems:'center',
                            justifyContent:'center',
                            //position:'absolute',
                            //bottom:0,
                            paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                        }}
                    >
                        <Button.Standard
                            buttonStyle={{
                                width:'100%',
                            }}

                            buttonSize={CONSTANT.STYLE.BUTTON.EXTRA_LARGE}
                            buttonColor={CONSTANT.COLOR.GREEN}
                            fontOption={CONSTANT.TEXT_OPTION.HEADER}
                            buttonText={'Buat Pesanan'}
                            onPress={this._onPressSubmitButton.bind(this)}
                            disabled={this.state.disabled}
                        />
                    </View>
                );
            }
            return null;
            
        }
        catch(error){
            console.log(error, 'pos.payment.submit_button');
            return null;
        }
		
	}
}

function mapStateToProps(state, ownProps) {
	return {
		userRole:state.rootReducer.user.role,
        posSelectedItem:state.rootReducer.pos.selectedItem,
        posTotalPrice:state.rootReducer.pos.totalPrice,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
        actionPOS: bindActionCreators(actions.pos, dispatch)
	};
}

const styles = StyleSheet.create({
});

export default connect(mapStateToProps, mapDispatchToProps)(SubmitButton);

