import React, { Component } from 'react';
import {View, Keyboard, BackHandler, StyleSheet, Dimensions, ScrollView, Platform, TouchableOpacity, backHandler, Image} from 'react-native';

//node js class import
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling, Avatar, Form, toCurrency} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

import Container from './../container';

class MembershipDetail extends Component {
	constructor(props) {
        super(props);
        this.state = {
        	promo_code:this.props.posSelectedPromo != null ? this.props.posSelectedPromo.promo_code : '',
        	textColor:'',
        	textFontOption:'',
        	isValidated:true,
        	isKeyboardShown:false,
        };
    }


    componentWillMount(){
		this.configFont();

		if (this.props.posSelectedPromo != null) {
			this.props.actionPOS.validatePromo(this.state.promo_code, this.props.posTotalPrice);
		}
	}

    async _onPressValidatePromo(){
    	let {actionValidation} = this.props;
		let {promo_code} = this.state;
    	let isValidated = true;
    	let info = '';

    	if (await actionValidation.isEmpty(promo_code)) {
			info = 'Kode Promo tidak boleh kosong';
			isValidated = false;
    	}

		this.setState({isValidated, info}, async () => {
			if (isValidated) {
	    		await this.props.actionPOS.validatePromo(this.state.promo_code, this.props.posTotalPrice)
				this.configFont();
	    	}
		});
		
    }

    getDetailComponent(){
		let {posSelectedPromo} = this.props;

		let {isValidated} = this.state;
		if (posSelectedPromo == null && isValidated) {
			return(
				<Button.Standard
					buttonSize={CONSTANT.STYLE.BUTTON.MEDIUM}
					buttonText={'Validasi'}
					buttonColor={CONSTANT.COLOR.ORANGE}
					onPress={this._onPressValidatePromo.bind(this)}
				/>
			);
		}
		else{
			let info = '';
			
			let priceInfo = null;

			let color = CONSTANT.COLOR.RED_STATUS_TEXT;

			if (isValidated) {
				if (posSelectedPromo.valid) {
					color = CONSTANT.COLOR.GREEN_STATUS_TEXT;

					priceInfo = (
						<Text
							fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}
							textStyle={{
								color:posSelectedPromo.valid ? CONSTANT.COLOR.GREEN_STATUS_TEXT : CONSTANT.COLOR.RED_STATUS_TEXT,
								textAlign:'center'
							}}
						>
							{'Potongan Sejumlah : ' + toCurrency(posSelectedPromo.discount_price, 'Rp ')}
						</Text>
					);
				}

				info = posSelectedPromo.info;
			}
			else{
				info = this.state.info;
			}

			return(
				<View>
					<Text
						fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}
						textStyle={{
							color:color,
						}}
					>
						{info}
					</Text>

					{priceInfo}
					
				</View>
			);
		}
    }

    configFont(){
    	let {posSelectedPromo} = this.props;

        let textColor = CONSTANT.COLOR.BLACK;
		let textFontOption = CONSTANT.TEXT_OPTION.HEADER_REGULAR;

		if (posSelectedPromo) {
			if (posSelectedPromo.valid) {
				textColor = CONSTANT.COLOR.DARKER_GRAY;
			}
			else{
				textColor = CONSTANT.COLOR.BLACK;
			}
		}
		else{
			textColor = CONSTANT.COLOR.BLACK;
		}

    	this.setState({textColor, textFontOption});
    }

	
	render(){
        try{
            let {posSelectedPayment, posSelectedPromo} = this.props;
            let {promo_code, textColor, textFontOption} = this.state;

            return(
                <Container
                    containerStyle={{
                        height:Scaling.verticalScale(100),
                       	paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                       	alignItems:'center',
                       	marginBottom:Scaling.verticalScale(10),
                    }}
                >
                	<View
						style={{
							flexDirection:'row',
							width:'100%',
						}}
                	>
                		<Form.InputField.TypeA
							containerStyle={{
								marginTop:Scaling.verticalScale(15),
							}}
							inputTextStyle={{
								textAlign:'left'
							}}
							label={'Kode Membership / Promo Outlet'}
							onChangeText={(value) => {
								this.setState({promo_code:value});
							}}
							onFocus={() => {
								this.setState({isValidated:true}, () => {
									this.props.actionPOS.setSelectedPromo(null);
								});
							}}
							value={this.state.promo_code}
							disabled={posSelectedPromo == null? false : posSelectedPromo.valid}
							inputTextStyle={{color:textColor, textAlign:'left'}}
							textFontOption={textFontOption}
						/>

                		{
                			posSelectedPromo != null && posSelectedPromo.valid ?
	                			<Button.Cancel 
									containerStyle={{
										position:'absolute',
										right:'-2.5%',
										top:'20%'
									}}
									onPress={async () => {
										await this.props.actionPOS.setSelectedPromo(null);
										this.setState({promo_code:''}, async () => {
											this.configFont();
										});

									}}
								/>
								:
								null
                		}
                	</View>

					{this.getDetailComponent()}

                </Container>
            );
        }
        catch(error){
            console.log(error, 'pos.payment.index');
            return null;
        }
		
	}
}

function mapStateToProps(state, ownProps) {
	return {
        posSelectedItem:state.rootReducer.pos.selectedItem,
        posSelectedPayment:state.rootReducer.pos.selectedPayment,
        posAvailableDuration:state.rootReducer.pos.availableDuration,
        posSelectedDuration:state.rootReducer.pos.selectedDuration,
        posSelectedCustomer:state.rootReducer.pos.selectedCustomer,
        posSelectedPromo:state.rootReducer.pos.selectedPromo,
        posTotalPrice:state.rootReducer.pos.totalPrice,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
        actionPOS: bindActionCreators(actions.pos, dispatch),
        actionValidation: bindActionCreators(actions.validation, dispatch),
	};
}

const styles = StyleSheet.create({
});


export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(MembershipDetail);

