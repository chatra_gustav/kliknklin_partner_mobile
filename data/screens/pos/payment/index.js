import React, { Component } from 'react';
import {View, Keyboard, StyleSheet, Dimensions, ScrollView, Platform, TouchableOpacity, backHandler, Image} from 'react-native';

//node js class import
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling, Avatar, Form} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

//local
import SubmitButton from './submit_button';
import OrderDetail from './order_detail';
import Container from './container';
import PaymentMethodDetail from './payment_method_detail';
import MembershipDetail from './membership_detail';
import PaymentCategory from './payment_category';

class POSPayment extends Component {
	constructor(props) {
        super(props);
        this.state = {
            isKeyboardShown:false,
        };
    }

    componentDidMount(){

    }

    componentWillMount(){
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this,true));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidShow.bind(this, false));
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow (isShown) {
        if (!isShown) {
            this.membershipDetailComponent._onPressValidatePromo();
        }

        this.setState({isKeyboardShown:isShown});
    }

	
	render(){
        try{
            let {posSelectedPayment} = this.props;
            return(
                    <LayoutPrimary
                        showTitle={true}
                        showTabBar={false}
                        showBackButton={true}
                        navigator={this.props.navigator}
                        title={'Pembayaran'}
                    >   
                        <KeyboardAwareScrollView
                            contentContainerStyle={{
                                paddingBottom:Scaling.verticalScale(120),
                            }}
                        >
                            <PaymentCategory {...this.props} />

                            <PaymentMethodDetail {...this.props}/>

                            <MembershipDetail 
                                ref={connectedComponent => this.membershipDetailComponent = connectedComponent != null ? connectedComponent.getWrappedInstance() : null}
                                {...this.props}
                            />

                            <OrderDetail />
                        </KeyboardAwareScrollView>

                        <SubmitButton {...this.props} isHide={!this.state.isKeyboardShown}/>
                    </LayoutPrimary>
            );
        }
        catch(error){
            console.log(error, 'pos.payment.index');
            return null;
        }
		
	}
}

function mapStateToProps(state, ownProps) {
	return {
        posSelectedItem:state.rootReducer.pos.selectedItem,
        posSelectedPayment:state.rootReducer.pos.selectedPayment,
        posAvailableDuration:state.rootReducer.pos.availableDuration,
        posSelectedDuration:state.rootReducer.pos.selectedDuration,
        posSelectedCustomer:state.rootReducer.pos.selectedCustomer
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
        actionPOS: bindActionCreators(actions.pos, dispatch),
	};
}

const styles = StyleSheet.create({
});

export default connect(mapStateToProps, mapDispatchToProps)(POSPayment);

