import React, { Component } from 'react';
import {View, StyleSheet, Dimensions, ScrollView, Platform, TouchableOpacity, backHandler, Image} from 'react-native';

//node js class import
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling, Avatar, Form} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

//local
import Container from './../container';

class PaymentCategory extends Component{
    static defaultProps = {
        onPress:() => {},
    }

    constructor(props) {
      super(props);
    
      this.state = {
        payNow:false,
      };
    }

    componentWillMount(){
        this.config();
    }

    config(){
        this._onPressPayNowStatus(false);
    }

    _onPressPayNowStatus(isPayNow){
        let {posSelectedPayment, actionPOS} = this.props;
        posSelectedPayment.isPayNow = isPayNow;
        actionPOS.setSelectedPayment(posSelectedPayment);

        this.setState({payNow:isPayNow});
    }

    render(){
        let {actionPOS, actionNavigator, posSelectedPayment} = this.props;
        let {payNow} = this.state;
        return(
            <View
                style={{
                    flex:1,
                    flexDirection:'row',
                    justifyContent:'space-between',
                    alignItems:'center',
                    paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                    paddingVertical:Scaling.verticalScale(20),
                }}
            >
                <Button.Standard
                    buttonSize={CONSTANT.STYLE.BUTTON.THICKER_EXTRA_MEDIUM}
                    buttonColor={payNow ? CONSTANT.COLOR.LIGHT_BLUE : CONSTANT.COLOR.WHITE}
                    buttonTextStyle={{
                        color:payNow ? CONSTANT.COLOR.WHITE : CONSTANT.COLOR.LIGHT_BLUE,
                    }}
                    buttonStyle={{
                        borderColor:CONSTANT.COLOR.LIGHT_BLUE,
                        borderWidth:Scaling.moderateScale(1),
                    }}
                    fontOption={CONSTANT.TEXT_OPTION.BUTTON}
                    buttonText={'Bayar Sekarang'}
                    onPress={this._onPressPayNowStatus.bind(this, true)}
                />   

                <Button.Standard
                    buttonSize={CONSTANT.STYLE.BUTTON.THICKER_EXTRA_MEDIUM}
                    buttonColor={!payNow ? CONSTANT.COLOR.LIGHT_BLUE : CONSTANT.COLOR.WHITE}
                    buttonTextStyle={{
                        color:!payNow ? CONSTANT.COLOR.WHITE : CONSTANT.COLOR.LIGHT_BLUE,
                    }}
                    buttonStyle={{
                        borderColor:CONSTANT.COLOR.LIGHT_BLUE,
                        borderWidth:Scaling.moderateScale(1),
                    }}
                    fontOption={CONSTANT.TEXT_OPTION.BUTTON}
                    buttonText={'Bayar Saat Diambil'}
                    onPress={this._onPressPayNowStatus.bind(this, false)}
                />
            </View>
           
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        posSelectedItem:state.rootReducer.pos.selectedItem,
        posSelectedPayment:state.rootReducer.pos.selectedPayment,
        posAvailableDuration:state.rootReducer.pos.availableDuration,
        posSelectedDuration:state.rootReducer.pos.selectedDuration,
        posCustomer:state.rootReducer.pos.customer
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actionRoot: bindActionCreators(actions.root, dispatch),
        actionNavigator: bindActionCreators(actions.navigator, dispatch),
        actionPOS: bindActionCreators(actions.pos, dispatch),
    };
}

const styles = StyleSheet.create({
});

export default connect(mapStateToProps, mapDispatchToProps)(PaymentCategory);


