import React, { Component } from 'react';
import {View, StyleSheet, Dimensions, ScrollView, Platform, TouchableOpacity, backHandler, Image} from 'react-native';

//node js class import
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling, Avatar, Form} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

//local
import Container from './../container';

class PaymentMethodDetail extends Component{
    static defaultProps = {
        onPress:() => {},
    }

    constructor(props) {
      super(props);
    
      this.state = {
        payNow:false,
      };
    }

    componentWillMount(){
        this.config();
    }

    config(){
        this._onPressPayNowStatus(false);
    }

    _renderDetail(){
        let {posSelectedPayment} = this.props;
        if (posSelectedPayment.id == 1) {
            return(
                <View style={{
                    marginTop:Scaling.verticalScale(54 * 0.444),
                    height:Scaling.verticalScale(80),
                    width:'100%',
                    backgroundColor:CONSTANT.COLOR.WHITE,
                    paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                    alignItems:'center',
                    justifyContent:'center',
                    borderBottomWidth:Scaling.moderateScale(1),
                    borderTopWidth:Scaling.moderateScale(1),
                    borderColor:CONSTANT.COLOR.LIGHT_GRAY,
                }}>
                    <Form.InputField.SemiRound
                        containerStyle={{
                            width:'80%',
                        }}
                        placeholder={'Masukan nominal pembayaran'}
                        leftIcon={() => {
                            return(
                                <Image
                                    source={CONSTANT.IMAGE_URL.KLIKNKLIN_ICON}
                                    style={{
                                      tintColor: CONSTANT.COLOR.BLUE,
                                      width:Scaling.scale(60 * 0.444),
                                      height:Scaling.verticalScale(60 * 0.444),
                                      resizeMode:'contain',
                                    }}
                                /> 
                            );
                        }}
                    />
                </View>
            );
        }
    }

    _onPressPayNowStatus(isPayNow){
        let {posSelectedPayment, actionPOS} = this.props;
        posSelectedPayment.isPayNow = isPayNow;
        actionPOS.setSelectedPayment(posSelectedPayment);

        this.setState({payNow:isPayNow});
    }

    render(){
        let {actionPOS, actionNavigator, posSelectedPayment} = this.props;
        let {payNow} = this.state;
        return(
            <Container
                containerStyle={{
                    height:Scaling.verticalScale(100),
                    width:'100%',
                    borderColor:CONSTANT.COLOR.LIGHT_GRAY,
                    borderWidth:Scaling.moderateScale(1),
                    paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                    marginBottom:Scaling.verticalScale(10),
                }}
            >

                <TouchableOpacity
                    onPress={async () => {
                        let data = await actionPOS.getPaymentMethodList()
                        actionNavigator.showLightBox('PartnerKliknKlin.ItemSelection', {
                            data,
                            categoryToFind:'category',
                            onPressButtonPrimary:(index) =>{
                                let selectedPayment = JSON.parse(JSON.stringify(data));
                                selectedPayment[index].isPayNow = this.state.payNow;
                                actionPOS.setSelectedPayment(selectedPayment[index]);
                            }
                          });
                    }}
                    style={{
                        flex:1,

                        flexDirection:'row'
                    }}
                >
                     <View
                        style={{
                            flex:0.5,
                            justifyContent:'center',
                        }}
                    >
                        <Text 
                            fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}
                            textStyle={{
                                color:CONSTANT.COLOR.GRAY
                            }}
                        >
                        {'Metode Pembayaran'}
                        </Text>
                    </View>

                    <View
                        style={{
                            flex:0.5,
                            alignItems:'center',
                            justifyContent:'flex-end',
                            flexDirection:'row',
                        }}
                    >
                        <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER} textStyle={{color:CONSTANT.COLOR.LIGHT_BLUE}}>
                        {posSelectedPayment.category}
                        </Text>

                        <Image
                            source={CONSTANT.IMAGE_URL.RIGHT_ARROW_ICON}
                            style={[{
                              width:Scaling.scale(30 * 0.444),
                              height:Scaling.verticalScale(30 * 0.444),
                              marginLeft:Scaling.scale(5),
                              resizeMode:'contain',
                              tintColor:CONSTANT.COLOR.LIGHT_BLUE,
                            },
                            ]}
                        /> 
                        
                    </View>
                </TouchableOpacity>
             </Container>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        posSelectedItem:state.rootReducer.pos.selectedItem,
        posSelectedPayment:state.rootReducer.pos.selectedPayment,
        posAvailableDuration:state.rootReducer.pos.availableDuration,
        posSelectedDuration:state.rootReducer.pos.selectedDuration,
        posCustomer:state.rootReducer.pos.customer
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actionRoot: bindActionCreators(actions.root, dispatch),
        actionNavigator: bindActionCreators(actions.navigator, dispatch),
        actionPOS: bindActionCreators(actions.pos, dispatch),
    };
}

const styles = StyleSheet.create({
});

export default connect(mapStateToProps, mapDispatchToProps)(PaymentMethodDetail);


