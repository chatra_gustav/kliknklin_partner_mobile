import React, { Component } from 'react';
import {View, StyleSheet, Keyboard, ScrollView, Platform, TouchableOpacity} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import Carousel, {ParallaxImage} from 'react-native-snap-carousel';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling, toCurrency} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class SubmitButton extends Component {
	constructor(props) {
        super(props);
        this.state = {
            isKeyboardShown:false,
        };  
    }

    componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
          'keyboardDidShow',
          this._keyboardDidShow.bind(this),
        );
        this.keyboardDidHideListener = Keyboard.addListener(
          'keyboardDidHide',
          this._keyboardDidHide.bind(this),
        );
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow() {
        this.setState({isKeyboardShown:true});
    }

    _keyboardDidHide() {
        this.setState({isKeyboardShown:false});
    }
    
    async _onPressPayment(index){
        let {posSelectedItem, posSelectedCustomer, posSelectedDuration, actionNavigator, actionPOS, navigator} = this.props;

        let notification = null;

        if (!posSelectedItem) {
            notification = 'Produk tidak ada yang dipilih';
        }
        else if(!posSelectedCustomer){
            notification = 'Pelanggan tidak ada yang dipilih';
        }
        else if(!posSelectedDuration){
            notification = 'Durasi tidak ada yang dipilih';
        }
        else{
            let paymentMethodList = await actionPOS.getPaymentMethodList();
            actionPOS.setSelectedPayment(paymentMethodList[index]);

            actionNavigator.push(navigator, CONSTANT.ROUTE_TYPE.POS_PAYMENT);
        }

        if (notification) {
            actionNavigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
                title:'Peringatan', 
                message: notification,
                buttonPrimaryText: 'Ya', 
                onPressButtonPrimary: () => {
                    actionNavigator.dismissLightBox();
                },
            });
        }
    }

	
	render(){
        try{
            let {posSelectedItem, posTotalPrice} = this.props;
            
            if (posSelectedItem.length > 0 && !this.state.isKeyboardShown) {
                return(
                    <View
                        style={{
                            width:'100%',
                            height:Scaling.verticalScale(280 * 0.444),
                            position:'absolute',
                            backgroundColor:CONSTANT.COLOR.WHITE,
                            borderTopWidth:Scaling.moderateScale(1),
                            borderColor:CONSTANT.COLOR.LIGHT_GRAY,
                            bottom:0,
                            paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                        }}
                    >
                        <View
                            style={{
                                flex:0.3,
                                justifyContent:'center',
                            }}
                        >
                            <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>{'TOTAL BAYAR : ' + toCurrency(this.props.posTotalPrice, 'Rp')}</Text>
                        </View>

                        <View
                            style={{
                                flex:0.7,
                                flexDirection:'row',
                                justifyContent:'space-evenly',
                                alignItems:'center',
                            }}
                        >
                            <Button.Standard
                                buttonStyle={{
                                    width:'100%',
                                }}
                                
                                fontOption={CONSTANT.TEXT_OPTION.HEADER}
                                buttonSize={CONSTANT.STYLE.BUTTON.EXTRA_LARGE}
                                buttonColor={CONSTANT.COLOR.ORANGE}
                                buttonText={'Lanjut Ke Pembayaran'}
                                onPress={this._onPressPayment.bind(this, 0)}
                            />
                        </View>
                    </View>
                );
            }
            return null;
            
        }
        catch(error){
            console.log(error, 'pos.item_selection.submit_button');
            return null;
        }
		
	}
}

function mapStateToProps(state, ownProps) {
	return {
		userRole:state.rootReducer.user.role,
        posSelectedItem:state.rootReducer.pos.selectedItem,
        posSelectedDuration:state.rootReducer.pos.selectedDuration,
        posTotalPrice:state.rootReducer.pos.totalPrice,
        posSelectedCustomer:state.rootReducer.pos.selectedCustomer
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
        actionPOS: bindActionCreators(actions.pos, dispatch),
	};
}

const styles = StyleSheet.create({
});

export default connect(mapStateToProps, mapDispatchToProps)(SubmitButton);

