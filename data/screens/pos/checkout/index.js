import React, { Component } from 'react';
import {View, StyleSheet, imensions, ScrollView, Platform, TouchableOpacity, backHandler, Image} from 'react-native';

//node js class import
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling, Avatar} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

//local
import Container from './container';
import SubmitButton from './submit_button';
import ItemSelection from './item_selection';
import ItemDetail from './item_detail';

class POSCheckout extends Component {
	constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidUpdate(prevProps, prevState){
        if (prevProps.posSelectedItem.length != this.props.posSelectedItem.length) {
            if (this.props.posSelectedItem.length == 0) {
                this.props.actionNavigator.pop(this.props.navigator);
            }
        }
    }

    componentDidMount(){
    }

    scrollTo(sectionIndex){
        try{
            this.itemListComponent.scrollTo(sectionIndex);
        }
        catch(error){
        }
    }
	
	render(){
        try{
            let {summaryOrder} = this.props;
            return(
                    <LayoutPrimary
                        showTitle={true}
                        showTabBar={false}
                        showBackButton={true}
                        navigator={this.props.navigator}
                        title={'Checkout'}
                    >   
                        <KeyboardAwareScrollView
                            contentContainerStyle={{
                                paddingBottom:'27%',
                            }}
                        >
                            <CustomerDetail {...this.props}/>

                            <ItemDetail {...this.props}/>

                            <TimeServiceDetail {...this.props}/>

                            <DeliveryMethodDetail {...this.props}/>

                        </KeyboardAwareScrollView>
                        
                        <SubmitButton {...this.props} />
                    </LayoutPrimary>
            );
        }
        catch(error){
            console.log(error, 'pos.item_selection.index');
            return null;
        }
		
	}
}

class CustomerDetail extends Component{
    _getCustomerImage(){
        let {posSelectedCustomer} = this.props;
        let imageSource = null;
        let imageSize;
        if (posSelectedCustomer && posSelectedCustomer.user_image_url) {
            imageSource = posSelectedCustomer.user_image_url;
            imageSize = Scaling.scale(225 * 0.444);
        }
        else{
            imageSize = Scaling.scale(250 * 0.444);
        }

        return(
            <Avatar
                img={imageSource}
                size={imageSize}
                containerStyle={{
                    justifyContent:'center',
                }}
            />
        );
    }

    _getContent(){
        let{posSelectedCustomer} = this.props;

        if (posSelectedCustomer) {
            return(
                <View>
                    <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER} textStyle={{marginBottom:Scaling.scale(5)}}>{posSelectedCustomer.name}</Text>

                    <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER} textStyle={{marginBottom:Scaling.scale(5)}}>{posSelectedCustomer.mobile_phone}</Text>

                    <Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>{posSelectedCustomer.email}</Text>
                </View>
            );
        }
        else{
            return(
                <Button.Standard
                    buttonText={'Pilih Pelanggan'}
                    buttonColor={CONSTANT.COLOR.ORANGE}
                    buttonSize={CONSTANT.STYLE.BUTTON.EXTRA_MEDIUM}
                    onPress={() => {
                        this.props.actionNavigator.push(
                        this.props.navigator,
                        'PartnerKliknKlin.SelectCustomer', 
                        {
                            isDismissAllowed:false,
                        }
                      )
                    }}
                />
            );
        }
    }

    _renderEditButton(){
        try{
            let {posSelectedCustomer} = this.props;

            if (posSelectedCustomer) {
                return(
                    <TouchableOpacity
                        style={{
                            position:'absolute',
                            right:'2%',
                            top:'3%',
                            width:Scaling.scale(50),
                            height:Scaling.verticalScale(50),
                            alignItems:'flex-end',
                        }}

                        onPress={() => {
                            this.props.actionNavigator.push(
                                this.props.navigator,
                                'PartnerKliknKlin.SelectCustomer', 
                                {
                                    isDismissAllowed:false,
                                }
                              )
                            // this.props.actionNavigator.showLightBox(
                            // CONSTANT.ROUTE_TYPE.BOX_SEARCH_CUSTOMER, 
                            // {
                            //     searchResult:posSelectedCustomer.customer_id ? 'found' : 'not-found',
                            //     name:posSelectedCustomer.name,
                            //     mobilePhone:posSelectedCustomer.mobile_phone,
                            //     extractedMobilePhone:posSelectedCustomer.extractedMobilePhone,
                            //     email:posSelectedCustomer.email,
                            //     customerData:posSelectedCustomer,
                            //     notification:'',
                            //     isDismissAllowed:false,
                            // });
                        }}
                    >
                        <Image
                            source={CONSTANT.IMAGE_URL.EDIT_ICON}
                            style={{
                                width:Scaling.scale(25),
                                height:Scaling.verticalScale(25),
                            }}
                        />
                    </TouchableOpacity>
                );
            }
        }
        catch(error){
            console.log(error, 'pos.checkout._renderEditButton')
        }
    }

    _renderContent(){
        return(
            <View
                style={{
                    flex:1,
                    marginHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                    flexDirection:'row',
                }}
            >
                <View
                    style={{
                        flex:0.325,
                        justifyContent:'center',
                    }}
                >
                    {this._getCustomerImage()}
                </View>

                <View
                    style={{
                        flex:0.675,
                        justifyContent:'center',
                    }}
                >
                    {this._getContent()}

                    {this._renderEditButton()}
                </View>   
            </View>
        );
    }

    render(){
        return(
            <Container
                containerStyle={{
                    height:Scaling.verticalScale(300 * 0.444),
                }}
            >
                {this._renderContent()}
            </Container>
        );
    }
}

class TimeServiceDetail extends Component{
    render(){
        return(
            <ItemSelection
                title={'Durasi Pengerjaan'}
                content={this.props.posSelectedDuration + ' Jam'}
                onPress={() => {
                    this.props.actionNavigator.showLightBox('PartnerKliknKlin.ItemSelectionColumnBar', {
                        data:JSON.parse(JSON.stringify(this.props.posAvailableDuration)).durationList, // butuh deep clone
                        title:'Durasi Pengerjaan',
                        isItemDisabled:(value) => this.props.posAvailableDuration.durationReferral > value,
                        onPressButtonPrimary:(selectedValue) =>{
                            let posSelectedItem = JSON.parse(JSON.stringify(this.props.posSelectedItem));

                            let counter = Number.MAX_SAFE_INTEGER;

                            for (let i in posSelectedItem) {
                                let outletItemWashingTime = posSelectedItem[i].item.outletItemWashingTime;
                                let selectedOutletItemWashingTime = posSelectedItem[i].selectedOutletItemWashingTime;
                                let data = {};

                                for(let j in outletItemWashingTime){
                                    if (selectedOutletItemWashingTime.washingServiceID == outletItemWashingTime[j].washingServiceID 
                                        && selectedValue.duration >= outletItemWashingTime[j].duration
                                        && selectedValue.duration - outletItemWashingTime[j].duration < counter
                                        ) {
                                        counter = selectedValue.duration - outletItemWashingTime[j].duration;
                                        data = outletItemWashingTime[j];
                                    }

                                }

                                posSelectedItem[i].outletItemWashingTimeID = data.id;
                                posSelectedItem[i].selectedOutletItemWashingTime = data;
                                posSelectedItem[i].price = data.price;

                                counter = Number.MAX_SAFE_INTEGER;
                            }

                            this.props.actionPos.calculateTotalPrice(posSelectedItem);
                            this.props.actionPos.setSelectedItem(posSelectedItem);
                            this.props.actionPos.setSelectedDuration(selectedValue.duration);
                            this.props.actionNavigator.dismissLightBox();
                        }
                    });
                }}
            />
        );
    }
}

class DeliveryMethodDetail extends Component{
    render(){
        return(
            <ItemSelection
                title={'Pengiriman'}
                content={'Ambil Sendiri'}
            />
        );
    }
}

function mapStateToProps(state, ownProps) {
	return {
		userRole:state.rootReducer.user.role,
        posSelectedItem:state.rootReducer.pos.selectedItem,
        posAvailableDuration:state.rootReducer.pos.availableDuration,
        posSelectedDuration:state.rootReducer.pos.selectedDuration,
        posSelectedCustomer:state.rootReducer.pos.selectedCustomer
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
        actionPos: bindActionCreators(actions.pos, dispatch),
	};
}

const styles = StyleSheet.create({
});

export default connect(mapStateToProps, mapDispatchToProps)(POSCheckout);

