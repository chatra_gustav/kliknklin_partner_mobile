import React, { Component } from 'react';
import {View, StyleSheet, TouchableOpacity, Image} from 'react-native';

//node js class import

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

//local
import Container from './../container';

class ItemSelection extends Component{
    static defaultProps = {
      title:'',
      content:'',
      onPress:()=>{},
    }
    render(){
        return(
            <TouchableOpacity
                onPress={this.props.onPress.bind(this)}
            >
                <Container
                    containerStyle={{
                        flexDirection:'row',
                        paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT)
                    }}
                >
                    <View
                        style={{
                            flex:0.5,
                            justifyContent:'center',
                        }}
                    >
                        <Text 
                            fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER}
                            textStyle={{
                                color:CONSTANT.COLOR.GRAY
                            }}
                        >
                        {this.props.title}
                        </Text>
                    </View>

                    <View
                        style={{
                            flex:0.5,
                            alignItems:'center',
                            justifyContent:'flex-end',
                            flexDirection:'row',
                        }}
                    >
                        <Text fontOption={CONSTANT.TEXT_OPTION.BOLD_SUB_HEADER} textStyle={{color:CONSTANT.COLOR.LIGHT_BLUE}}>
                        {this.props.content}
                        </Text>

                        <Image
                            source={CONSTANT.IMAGE_URL.RIGHT_ARROW_ICON}
                            style={[{
                              width:Scaling.scale(30 * 0.444),
                              height:Scaling.verticalScale(30 * 0.444),
                              marginLeft:Scaling.scale(5),
                              resizeMode:'contain',
                              tintColor:CONSTANT.COLOR.LIGHT_BLUE,
                            },
                            ]}
                        /> 
                    </View>
                    
                </Container>
            </TouchableOpacity>
        );
    }
}


function mapStateToProps(state, ownProps) {
	return {
		userRole:state.rootReducer.user.role,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

const styles = StyleSheet.create({
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemSelection);

