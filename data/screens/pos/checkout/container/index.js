import React, { Component } from 'react';
import {View, StyleSheet, Platform, TouchableOpacity} from 'react-native';

//node js class import
import ResolveAssetSource from 'resolveAssetSource';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

//local

class Container extends Component{
    static defaultProps = {
      containerStyle:{},
    }
    render(){
        return(
            <View
                style={[{
                    width:'100%',
                    minHeight:Scaling.verticalScale(250 * 0.444),
                    backgroundColor:'white',
                    borderBottomWidth:Scaling.moderateScale(1),
                    borderTopWidth:Scaling.moderateScale(1),
                    borderColor:CONSTANT.COLOR.LIGHT_GRAY,
                    marginBottom:Scaling.verticalScale(40 * 0.444),
                },
                this.props.containerStyle
                ]}
            >
                {this.props.children}
            </View>
        );
    }
}

function mapStateToProps(state, ownProps) {
	return {
		userRole:state.rootReducer.user.role,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

const styles = StyleSheet.create({
});

export default connect(mapStateToProps, mapDispatchToProps)(Container);

