import React, { Component } from 'react';
import {View, Image, StyleSheet, Dimensions, TouchableOpacity, TouchableWithoutFeedback} from 'react-native';

//node js class import

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling, toCurrency, Form} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

//local
import Container from './../container';

class ItemDetail extends Component{
    render(){
        try{
            let items = this.props.posSelectedItem;
            
            return(
                    <Container
                        containerStyle={{
                            
                        }}
                    >   
                        {items.map((item) => <Items {...this.props} key={item.outletItemWashingTimeID} data={item} />)}
                        
                        <View
                            style={{
                                width:'100%',
                                alignItems:'center',
                            }}
                        >
                            <Button.Standard 
                                buttonStyle={{
                                    marginTop:Scaling.verticalScale(60 * 0.444),
                                    marginBottom:Scaling.verticalScale(40 * 0.444),
                                }}
                                buttonSize={CONSTANT.STYLE.BUTTON.EXTRA_MEDIUM}
                                buttonText={'Tambah Item'}
                                onPress={() => {
                                    this.props.actionNavigator.pop(this.props.navigator);
                                }}
                            />
                        </View>

                        
                    </Container>
            );
        }
        catch(error) {
            return null;
        }
    }
}

class Items extends Component{
    constructor(props) {
      super(props);
    
      this.state = {
        note:this.props.data.note,
      };
    }
    _onPressChooseItemButton(){
        let {data, posSelectedItem, posAvailableDuration, actionPOS} = this.props;
        let {item} = data;
        let prevSelectedOutletWashingTime = data.selectedOutletItemWashingTime;
        this.props.actionPOS.showWashingServiceSelection(item, async (selectedOutletItemWashingTime) => {
            for(let i in posSelectedItem){
                if (posSelectedItem[i].itemID == data.itemID) {
                    posSelectedItem = JSON.parse(JSON.stringify(posSelectedItem));
                    posSelectedItem[i].selectedOutletItemWashingTime = selectedOutletItemWashingTime;
                    posSelectedItem[i].price = selectedOutletItemWashingTime.price;
                    posSelectedItem[i].outletItemWashingTimeID = selectedOutletItemWashingTime.id;
                     
                    let newAvailableDuration = await actionPOS.calculateAvailableDuration(posAvailableDuration, item, prevSelectedOutletWashingTime, 'substract');
                    newAvailableDuration = await actionPOS.calculateAvailableDuration(newAvailableDuration, item, selectedOutletItemWashingTime, 'add');
                    actionPOS.setAvailableDuration(newAvailableDuration);
                    actionPOS.setSelectedDuration(newAvailableDuration.durationList[0].duration);
                    actionPOS.calculateTotalPrice(posSelectedItem);
                    actionPOS.setSelectedItem(posSelectedItem);
                }
            }
        });
    }

    _onPressSubstractButton(){
        this.setState({count:this.state.count - 1}, () =>{
            this.props.actionPOS.substractSelectedItem(this.props.item, this.state.selectedOutletItemWashingTime, this.state.count);

            if (this.state.count == 0) {
                this.config();
            }
        });
    }
    
    _renderDurationAvailibility(){
        let {posAvailableDuration, posSelectedItem, data} = this.props;
        let {selectedOutletItemWashingTime, item} = data;
        let {outletItemWashingTime} = item;
        
        let allDurationList = [];

        for(let i in outletItemWashingTime){
            if (outletItemWashingTime[i].washingServiceID == selectedOutletItemWashingTime.washingServiceID) {
                    allDurationList.push(outletItemWashingTime[i].duration);
                }
        }

        allDurationList.sort((a,b) => {
            return b - a;
        });

        return(
            allDurationList.map((data, index) => {
                let backgroundColor = 'transparent';

                if (selectedOutletItemWashingTime.duration == data) {
                    backgroundColor = CONSTANT.COLOR.GREEN;
                }
                else if(this.props.posAvailableDuration.durationReferral <= data){
                    backgroundColor = CONSTANT.COLOR.BLUE;
                }
                else{
                    backgroundColor = CONSTANT.COLOR.GRAY;
                }

                return(
                    <View
                        key={index}
                        style={{
                            width:Scaling.scale(70 * 0.444),
                            height:Scaling.scale(70 * 0.444),
                            borderRadius:Scaling.moderateScale(30),
                            marginRight:Scaling.scale(25 * 0.444),
                            backgroundColor:backgroundColor,
                            alignItems:'center',
                            justifyContent:'center',
                        }}
                    >
                        <Text fontOption={CONSTANT.TEXT_OPTION.SUB_CONTENT_BOLD} textStyle={{color:CONSTANT.COLOR.WHITE, margin:0, padding:0}}>{data}</Text>
                    </View>
                );
            })
        )
    }

    render(){
        try{
            let {data} = this.props;
            let source;
            let imageExist;

            if (data.selectedOutletItemWashingTime.item.item_image_url == "") {
                source = CONSTANT.IMAGE_URL.KLIKNKLIN_ICON;
                imageExist = false;
            }
            else{
                source = {uri:data.selectedOutletItemWashingTime.item.item_image_url};
                imageExist = true;
            }

            return (
                <View
                    style={{
                        marginTop:Scaling.verticalScale(40 * 0.444),
                        marginHorizontal:Scaling.scale(54 * 0.444),
                    }}
                >
                    <View
                        style={{
                            height:Scaling.verticalScale(350 * 0.444),
                            flexDirection:'row',
                        }}
                    >
                        <View
                            style={{
                                flex:0.325,
                                alignItems:'center',
                            }}
                        >
                            <Image
                                source={source}
                                style={[{
                                  width:Scaling.scale(270 * 0.444),
                                  height:Scaling.verticalScale(190 * 0.444),
                                  marginBottom:Scaling.verticalScale(15 * 0.444),
                                  resizeMode:'contain',
                                },
                                imageExist == false ? {tintColor:CONSTANT.COLOR.BLUE}: {},
                                ]}
                            /> 

                            <View
                                style={{
                                    flex:1,
                                    flexDirection:'row',
                                }}
                            >

                                <TouchableOpacity
                                    style={{
                                        width:Scaling.scale(80 * 0.444),
                                        height:Scaling.verticalScale(80 * 0.444),
                                        borderRadius:Scaling.moderateScale(10),
                                        backgroundColor:CONSTANT.COLOR.GREEN, 
                                        justifyContent:'center', 
                                        alignItems:'center',
                                    }}
                                    onPress={() => {
                                        this.props.actionPOS.substractSelectedItem(data.item, data.selectedOutletItemWashingTime, data.quantity);
                                    }}
                                >
                                    <Image
                                        source={CONSTANT.IMAGE_URL.MINUS_ICON}
                                        style={{
                                            width:Scaling.scale(20),
                                            height:Scaling.scale(20),
                                            resizeMode:'contain',
                                        }}
                                    />
                                </TouchableOpacity>
                                
                                <View
                                    style={{
                                        justifyContent:'center',
                                        alignItems:'center',
                                        height:Scaling.verticalScale(80 * 0.444),
                                        width:Scaling.scale(100 * 0.444),
                                    }}
                                >
                                    <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>{data.quantity}</Text>
                                </View>

                                <TouchableOpacity
                                    style={{
                                        width:Scaling.scale(80 * 0.444),
                                        height:Scaling.verticalScale(80 * 0.444),
                                        borderRadius:Scaling.moderateScale(10),
                                        backgroundColor:CONSTANT.COLOR.GREEN, 
                                        justifyContent:'center', 
                                        alignItems:'center',
                                    }}
                                    onPress={() => {
                                        this.props.actionPOS.addSelectedItem(data.item, data.selectedOutletItemWashingTime, data.quantity);
                                    }}
                                >
                                    <Image
                                        source={CONSTANT.IMAGE_URL.PLUS_ICON}
                                        style={{
                                            width:Scaling.scale(20),
                                            height:Scaling.scale(20),
                                            resizeMode:'contain',
                                        }}
                                    />
                                </TouchableOpacity>
                                
                            </View>
                            
                        </View>
                        
                        <View
                            style={{
                                flex:0.675,
                            }}
                        >
                            <View
                                style={{
                                    flex:0.25,
                                }}
                            >
                                <Text fontOption={CONSTANT.TEXT_OPTION.HEADER} >{data.name}</Text>
                            </View>

                            <View
                                style={{
                                    flex:0.15,
                                    justifyContent:'center',
                                }}
                            >
                                <TouchableOpacity
                                    onPress={this._onPressChooseItemButton.bind(this)}
                                    style={{
                                        flexDirection:'row',
                                    }}
                                >
                                    <Text fontOption={CONSTANT.TEXT_OPTION.CONTENT} textStyle={{color:CONSTANT.COLOR.LIGHT_BLUE}}>{data.selectedOutletItemWashingTime.washingServiceInfo}</Text>

                                    <Image
                                      style={{
                                        tintColor:CONSTANT.COLOR.LIGHT_BLUE,
                                        width: Scaling.scale(15), 
                                        height: Scaling.verticalScale(15),
                                        marginLeft:Scaling.moderateScale(2.5),
                                        marginTop:Scaling.verticalScale(3),
                                      }}
                                      source={CONSTANT.IMAGE_URL.BOTTOM_ARROW_ICON}
                                    />
                                </TouchableOpacity>
                            </View>

                            <View
                                style={{
                                    flex:0.2,
                                }}
                            >
                                <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>{toCurrency(data.price, 'Rp') + ' / ' + data.unitName}</Text>
                            </View>

                            <View
                                style={{
                                    flex:0.45,
                                }}
                            >
                                <Text numberOfLines={6} fontOption={CONSTANT.TEXT_OPTION.SUB_CONTENT}>{data.description}</Text>
                            </View>
                        </View>
                    </View>
                    <View
                        style={{
                            //height:Scaling.verticalScale(200 * 0.444),
                        }}
                    >
                        <View
                            style={{
                                height:Scaling.verticalScale(60 * 0.444),
                                flex:0.3,
                                flexDirection:'row',
                                alignItems:'center',
                            }}
                        >
                            <View
                                style={{
                                    flex:0.325
                                }}
                            >
                                <Text fontOption={CONSTANT.TEXT_OPTION.CONTENT}>
                                    {'Durasi tersedia (Jam)'}
                                </Text>
                            </View>

                            <View
                                style={{
                                    flex:0.675,
                                    flexDirection:'row',
                                }}
                            >
                                {this._renderDurationAvailibility()}
                            </View>

                        </View>

                        <View
                            style={{
                            }}
                        >

                        <Form.InputField.Underline
                            ref={(ref) => this.notes = ref} 
                            placeholder={'Buat Catatan'}
                            placeholderColor={CONSTANT.COLOR.LIGHT_GRAY}
                            fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}
                            inputTextStyle={{
                                textAlign:'left',
                            }}
                            multiline={true}
                            leftIcon={() => {
                                return(
                                    <Image
                                        source={CONSTANT.IMAGE_URL.NOTE_ICON}
                                        style={{
                                          width:Scaling.scale(60 * 0.444),
                                          height:Scaling.verticalScale(60 * 0.444),
                                          resizeMode:'contain',
                                        }}
                                    /> 
                                );
                            }}
                            onChangeText={(text) => {
                                this.setState({note:text}, () => {
                                    this.props.actionPOS.setItemNote(text, data.itemID);
                                });
                            }}
                            value={this.state.note}
                        />
                        
                        </View>
                        
                    </View>
                </View>
            );
        }
        catch(error){
            console.log(error);
            return null;
        }
    }
}


function mapStateToProps(state, ownProps) {
    return {
        userRole:state.rootReducer.user.role,
        posSelectedItem:state.rootReducer.pos.selectedItem,
        posAvailableDuration:state.rootReducer.pos.availableDuration,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actionPOS: bindActionCreators(actions.pos, dispatch),
        actionRoot: bindActionCreators(actions.root, dispatch),
        actionNavigator: bindActionCreators(actions.navigator, dispatch),
    };
}

const styles = StyleSheet.create({
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemDetail);
