import React, { Component } from 'react';
import {View, StyleSheet, imensions, ScrollView, Platform, TouchableOpacity} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import Carousel, {ParallaxImage} from 'react-native-snap-carousel';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

//local
import ItemFilter from './item_filter';
import ItemList from './item_list';
import SubmitButton from './submit_button';
import AutoScrollButton from './auto_scroll_button';

class POSItemSelection extends Component {
	constructor(props) {
        super(props);
        this.state = {
        };  
    }

    scrollTo(sectionIndex){
        try{
            this.itemListComponent.scrollTo(sectionIndex);
        }
        catch(error){
        }
    }
	
	render(){
        try{
            let {summaryOrder, userOutlet} = this.props;
            let {indexActiveOutlet} = userOutlet;
            return(
                <LayoutPrimary
                    showOutletSelector={true}
                    showResetPOSButton={indexActiveOutlet != null ? true : false}
                    showTabBar={false}
                    showBackButton={true}
                    navigator={this.props.navigator}
                >   

                    <ItemList 
                        ref={connectedComponent => this.itemListComponent = connectedComponent != null ? connectedComponent.getWrappedInstance() : null}
                        {...this.props}
                    />

                    <AutoScrollButton 
                        scrollTo={this.scrollTo.bind(this)}
                    />

                    <SubmitButton {...this.props} />

                </LayoutPrimary>
            );
        }
        catch(error){
            console.log(error, 'pos.item_selection.index');
            return null;
        }
		
	}
}

function mapStateToProps(state, ownProps) {
	return {
		userRole:state.rootReducer.user.role,
        userOutlet:state.rootReducer.user.outlet,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

const styles = StyleSheet.create({
});

export default connect(mapStateToProps, mapDispatchToProps)(POSItemSelection);

