import React, { Component } from 'react';
import {View, Image, StyleSheet, Dimensions, ScrollView, Platform, TouchableOpacity} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import Carousel, {ParallaxImage} from 'react-native-snap-carousel';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Form ,Scaling} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class ItemFilter extends Component{

    _onChangeText(){

    }

    render(){
        try{
            return(
                <View
                    style={{
                        width:'100%',
                        height:Scaling.verticalScale(100 * 0.444),
                        paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                        backgroundColor:'white',
                        borderBottomWidth:moderateScale(1),
                        borderColor:CONSTANT.COLOR.LIGHT_GRAY,
                        flexDirection:'row',
                    }}
                >
                
                    <FilterSelector 
                        {...this.props}
                        label={'Tipe Cuci'}
                        data={[
                            {
                                id:123,
                                value:'hello',
                            },
                            {
                                id:123,
                                value:'my',
                            },
                            {
                                id:123,
                                value:'this is a long word yeah ok ok ok ok ok ok ok',
                            },
                        ]}
                        value={'heiho'}
                        leftIcon={() => {
                            return(
                                <Image
                                    source={CONSTANT.IMAGE_URL.KLIKNKLIN_ICON}
                                    style={{
                                        tintColor: this.props.isActive ? CONSTANT.COLOR.BLUE : CONSTANT.COLOR.GRAY,
                                        width:Scaling.scale(CONSTANT.STYLE.IMAGE.ICON.WIDTH),
                                        height:Scaling.verticalScale(CONSTANT.STYLE.IMAGE.ICON.HEIGHT),
                                        resizeMode:'contain'
                                    }}
                                />
                            );
                        }}
                    />

                    <View
                        style={{
                            height:'100%',
                            justifyContent:'center',
                            alignItems:'center',
                            width:Scaling.scale(90 * 0.444),
                        }}
                    >
                        <View
                            style={{
                                height:'60%',
                                borderLeftWidth:1,
                                borderColor:CONSTANT.COLOR.LIGHT_GRAY,
                            }}
                        />

                    </View>

                    <FilterSelector
                        {...this.props}
                        label={'Tipe Waktu'}
                        data={[
                            {
                                id:123,
                                value:'Regular',
                            },
                            {
                                id:123,
                                value:'Express 3 jam',
                            },
                            {
                                id:123,
                                value:'Express 12 jam',
                            },
                        ]}
                        value={'Express 12 jam'}
                        leftIcon={() => {
                            return(
                                <Image
                                    source={CONSTANT.IMAGE_URL.KLIKNKLIN_ICON}
                                    style={{
                                        tintColor: this.props.isActive ? CONSTANT.COLOR.BLUE : CONSTANT.COLOR.GRAY,
                                        width:Scaling.scale(CONSTANT.STYLE.IMAGE.ICON.WIDTH),
                                        height:Scaling.verticalScale(CONSTANT.STYLE.IMAGE.ICON.HEIGHT),
                                        resizeMode:'contain'
                                    }}
                                />
                            );
                        }}
                    />
                
             </View>
            );
        }
        catch(error){
            console.log(error, 'pos.item_selection.item_filter');
            return null;
        }
    }
}

class FilterSelector extends Component{
    static defaultProps = {
      data :[],
      value: '',
      onChangeText:() => {},
      label:'',
    }

    render(){
        return(
            <View
                style={{
                    flex:1,
                }}
            >
               <Form.DropdownField.MainDashboard
                    {...this.props}
                    label={this.props.label}
                    labelContainerStyle={{
                        top:Scaling.verticalScale(5),
                    }}
                    data={this.props.data}
                    value={this.props.value}
                    onChangeText={this.props.onChangeText}
                    width={'75%'}
                />
            </View>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        userRole:state.rootReducer.user.role,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actionRoot: bindActionCreators(actions.root, dispatch),
        actionNavigator: bindActionCreators(actions.navigator, dispatch),
    };
}

const styles = StyleSheet.create({
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemFilter);

