import React, { Component } from 'react';
import {View, Image, StyleSheet, imensions, ScrollView, Platform, TouchableOpacity} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import Carousel, {ParallaxImage} from 'react-native-snap-carousel';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling, toCurrency} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class SubmitButton extends Component {
	constructor(props) {
        super(props);
        this.state = {
        };  
    }
	
	render(){
        try{
            let {posSelectedItem, posTotalPrice} = this.props;
            
            if (posSelectedItem.length > 0) {
                return(
                    <View
                        style={{
                            width:'100%',
                            height:Scaling.verticalScale(200 * 0.444),
                            justifyContent:'center',
                            alignItems:'center',
                            position:'absolute',
                            bottom:0,
                            paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                        }}
                    >
                        <Button.Standard
                            buttonStyle={{
                                width:'100%',
                                paddingHorizontal:Scaling.scale(25 * 0.444),
                                paddingVertical: Scaling.verticalScale(25 * 0.444),
                                justifyContent:'flex-start',
                                alignItems:'flex-start',
                            }}
        
                            buttonSize={CONSTANT.STYLE.BUTTON.EXTRA_LARGE}
                            buttonColor={CONSTANT.COLOR.BLUE}
                            onPress={() => {
                                this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.POS_CHECKOUT);
                            }}
                        >
                            <View
                                style={{
                                    width:'100%',
                                    height:'40%',
                                }}
                            >
                                <Text fontOption={CONSTANT.TEXT_OPTION.BUTTON} textStyle={{
                                    color:CONSTANT.COLOR.WHITE,
                                }}>TOTAL</Text>
                            </View>
                            <View
                                style={{
                                    width:'100%',
                                    height:'60%',
                                }}
                            >
                                <Text fontOption={CONSTANT.TEXT_OPTION.BUTTON_LARGE} textStyle={{
                                    color:CONSTANT.COLOR.WHITE,
                                }}>{posSelectedItem.length  +' Barang : ' + toCurrency(posTotalPrice, 'Rp ')}</Text>
                            </View>
                            
                            <Image
                              style={{
                                width: Scaling.scale(25), 
                                height: Scaling.verticalScale(25),
                                position:'absolute',
                                right:'5%',
                                top:'47.5%',
                                tintColor:CONSTANT.COLOR.WHITE,
                            }}
                              source={CONSTANT.IMAGE_URL.RIGHT_ARROW_ICON}
                            />
                        </Button.Standard>
                    </View>
                );
            }
            return null;
            
        }
        catch(error){
            console.log(error, 'pos.item_selection.submit_button');
            return null;
        }
		
	}
}

function mapStateToProps(state, ownProps) {
	return {
		userRole:state.rootReducer.user.role,
        posSelectedItem:state.rootReducer.pos.selectedItem,
        posTotalPrice:state.rootReducer.pos.totalPrice,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

const styles = StyleSheet.create({
});

export default connect(mapStateToProps, mapDispatchToProps)(SubmitButton);

