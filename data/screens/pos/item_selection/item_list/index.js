import React, { Component, PureComponent } from 'react';
import {View, Image, FlatList, SectionList, StyleSheet, Dimensions, ScrollView, Platform, TouchableOpacity} from 'react-native';
import sectionListGetItemLayout from 'react-native-section-list-get-item-layout'
//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import { LargeList } from "react-native-largelist-v3";


import {LayoutPrimary} from 'app/data/layout';
import {Text, Form ,Scaling, toCurrency, Button, TextTicker, NoActiveOutlet} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from 'app/data/actions';

//lokal
import NoItem from './../no_item';

class ItemList extends Component{
    constructor(props) {
      super(props);
    
      this.state = {
        item:[],
        extractedItem:[],
        isFinishLoad:false,
      };
    }

    componentWillMount(){
        this.config();
    }

    componentDidUpdate(prevProps, prevState){
        if (prevProps.posOutletItem != this.props.posOutletItem) {
            this.config();
        }
    }

    async config(){
        let item = JSON.parse(JSON.stringify(this.props.posOutletItem));

        let newData = [];
        let rowData = [];
        let counter = 0;
        const maxCounter = 3;

        for(let i in item){
            for(let j in item[i].data){
                rowData.push(item[i].data[j]);
                counter++;

                if (counter == maxCounter) {
                    newData.push(rowData);
                    rowData = [];
                    counter = 0;
                }
            } 
            
            if (counter < maxCounter && counter > 0) {
                newData.push(rowData);
                rowData = [];
                counter = 0;
            }
            item[i].data = newData;
            newData = [];
        }


        const extractedItem = [];

        for (let section = 0; section < item.length; ++section) {
            const sContent = { items: [] };
            for(let index in item[section].data){
                sContent.items.push(item[section].data[index]);
            }
            extractedItem.push(sContent);
        }

        this.setState({
            item,
            extractedItem,
            isFinishLoad:true,
        }, () => {
            this.props.actionPOS.setExtractedOutletItem(this.state.extractedItem);
        });

    }

    scrollTo(sectionIndex){
        this._rootScroll.scrollToIndexPath(
            {section:sectionIndex, row:-1},
            true, 
        );
    }

    render(){
        try{
            if (this.props.userOutlet.indexActiveOutlet == null) {
                return(
                    <NoActiveOutlet {...this.props}/>
                ) 
            }
            else if (this.props.posOutletItem.length == 0) {
                return(
                    <NoItem {...this.props}/>
                );
            }
            return(
                <LargeList
                    ref={(components) => {this._rootScroll = components}}
                    style={{
                        paddingHorizontal:CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT,
                    }}
                    data={this.state.extractedItem}
                    renderSection={this._renderSection}
                    renderFooter={() => {
                        return(
                            <View
                                style={{
                                    height:Dimensions.get('window').height * 0.6,
                                }}
                            />
                        );
                    }}
                    heightForIndexPath={({section, row}) => {
                        if (row == 0) {
                            return Scaling.verticalScale((555 + 90 + 60 + 50) * 0.444);
                        }

                        return Scaling.verticalScale((555 + 40) * 0.444);
                    }}
                    renderIndexPath={this._renderIndexPath}
                />
            );
        }
        catch(error){
            console.log(error, 'pos.item_selection.item_filter');
            return null;
        }
    }

    _renderSection = (section: number) => {
        return (
          <RowHeader category={this.state.item[section].category} />
        );
      };

      _renderIndexPath = ({ section: section, row: row }) => {
        return (
          <SectionItem {...this.props} {...this.state} data={this.state.item} item={this.state.item[section].data[row]} indexRow={row} indexSection={section}/>
        );
      };
}


class SectionItem extends PureComponent{
    static defaultProps = {

    }
        
    render(){
        return(
            <View
                style={{
                    marginTop:this.props.indexRow == 0 ? Scaling.verticalScale(150 * 0.444) : Scaling.verticalScale(40 * 0.444),
                    width:'100%',
                    flexDirection:'row',
                }}
            >
                {this.props.item.map((item, index) => <Item {...this.props} item={item} indexItem={index} key={index} />)}
            </View>
             
        );
    }
}

class RowHeader extends PureComponent{
    static defaultProps = {

    }
        
    render(){
        return(
            <View
                style={{
                    width:'100%',
                    
                    height:Scaling.verticalScale(150 * 0.444),
                     backgroundColor:CONSTANT.COLOR.LIGHTER_GRAY,
                }}
            >
                <View
                    style={{
                        height:Scaling.verticalScale(60 * 0.444),
                        backgroundColor:CONSTANT.COLOR.LIGHTER_GRAY,
                    }}
                />

                <Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>
                    {this.props.category}
                </Text>

            </View>
        );
    }
}

class Item extends PureComponent{
    static defaultProps = {

    }

    constructor(props) {
      super(props);
      this.state = {
        posSelectedItem:null,
        selectedOutletItemWashingTime:[],
        selectedItem:[],
        selectedItemIndex:-1,
      };
    }

    async componentDidUpdate(prevProps, prevState){
        if (prevProps.posSelectedItem != this.props.posSelectedItem || 
            prevProps.item != this.props.item) {
            let data = this.props.item;
            let result = await this.props.actionPOS.findSelectedItemData(data.id);

            this.setState({
               posSelectedItem:result ? result : null,
               selectedOutletItemWashingTime: result ? result.selectedOutletItemWashingTime : prevState.selectedOutletItemWashingTime,
            });

            if (!result) {
                this.config();   
            }
        }
    }

    findItemIndex(IDtoFind){
        let {posSelectedItem} = this.props;
        for(let i in posSelectedItem){
            if (posSelectedItem[i].id == IDtoFind) {
                return i;
            }
        }
        return -1;
    }

    async componentWillMount(){
        let data = this.props.item;
        let result = await this.props.actionPOS.findSelectedItemData(data.id);

        this.setState({
           posSelectedItem:result ? result : null,
           selectedOutletItemWashingTime: result ? result.selectedOutletItemWashingTime : [],
           selectedItemIndex:this.findItemIndex(data.id),
        }, () => {
            let selectedItem = this.state.selectedItemIndex == -1 ? [] : this.props.posSelectedItem[this.state.selectedItemIndex];

            this.setState({selectedItem});
        });
            
        if (!result) {
            this.config();   
        }
    }

    config(){
        let baseList = CONSTANT.BASE_ITEM_CATEGORY_POS.LIST;
        let itemList = this.props.item.outletItemWashingTime
        let result = [];

        for(let i in itemList){
            for(let j in baseList){
                if (baseList[j].washingServiceID == itemList[i].washingServiceID &&
                        baseList[j].timeServiceID == itemList[i].timeServiceID
                    ) {
                    result = itemList[i];
                    break;
                }
            }
        }

        if (result.length == 0) {
            let lowestPrice = Number.MAX_SAFE_INTEGER;
            for(let i in itemList){
                if (itemList[i].price < lowestPrice) {
                    lowestPrice = itemList[i].price;
                    result = itemList[i];
                }
            }
        }
        this.setState({selectedOutletItemWashingTime:result});
    }

    _onPressChooseItemButton(){
        this.props.actionPOS.showWashingServiceSelection(this.props.item, this._onPressAddButton.bind(this));
    }

    async _onPressAddButton(selectedOutletItemWashingTime){
        let {posSelectedItem} = this.state;
        let count = !posSelectedItem ? 1 : posSelectedItem.quantity + 1;

        await this.props.actionPOS.addSelectedItem(this.props.item, selectedOutletItemWashingTime, count);
        this.setState({posSelectedItem:await this.props.actionPOS.findSelectedItemData(this.props.item.id), selectedOutletItemWashingTime});
    }

    async _onPressSubstractButton(){
        let {posSelectedItem} = this.state;
        let count = !posSelectedItem ? 0 : posSelectedItem.quantity - 1;

        await this.props.actionPOS.substractSelectedItem(this.props.item, this.state.selectedOutletItemWashingTime, count);

        if (count == 0) {
            this.config();
        }
    }

    _renderButton(){
        let {posSelectedItem} = this.state;

        if (!posSelectedItem || posSelectedItem.quantity == 0) {
            return(
                <TouchableOpacity
                    style={{height:Scaling.verticalScale(100 * 0.444), backgroundColor:CONSTANT.COLOR.GREEN, justifyContent:'center', alignItems:"center"}}
                    onPress={this._onPressChooseItemButton.bind(this)}
                >
                    <Text fontOption={CONSTANT.TEXT_OPTION.BUTTON} textStyle={{color:CONSTANT.COLOR.WHITE}}>Pilih</Text>
                </TouchableOpacity>
            );
        }
        else{
            return(
                <View
                    style={{
                        height:Scaling.verticalScale(100 * 0.444),
                        flexDirection:'row', 
                    }}
                >
                    <TouchableOpacity
                        style={{
                            flex:0.5,
                            backgroundColor:CONSTANT.COLOR.GREEN, 
                            justifyContent:'center', 
                            alignItems:'center',
                            borderRightWidth:Scaling.moderateScale(1),
                            borderColor:CONSTANT.COLOR.WHITE,
                        }}
                        onPress={this._onPressSubstractButton.bind(this)}
                    >
                        <Image
                            source={CONSTANT.IMAGE_URL.MINUS_ICON}
                            style={{
                                width:Scaling.scale(20),
                                height:Scaling.scale(20),
                                resizeMode:'contain',
                            }}
                        />
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{
                            flex:0.5,
                            backgroundColor:CONSTANT.COLOR.GREEN, 
                            justifyContent:'center', 
                            alignItems:'center'}}
                        onPress={this._onPressAddButton.bind(this, this.state.selectedOutletItemWashingTime)}
                    >
                        <Image
                            source={CONSTANT.IMAGE_URL.PLUS_ICON}
                            style={{
                                width:Scaling.scale(20),
                                height:Scaling.scale(20),
                                resizeMode:'contain',
                            }}
                        />
                    </TouchableOpacity>
                </View>
            );
        }
    }

    _renderMainContent(){
        let {posSelectedItem, selectedOutletItemWashingTime} = this.state;
        let source;
        let imageExist;
        if (selectedOutletItemWashingTime.length == 0 || selectedOutletItemWashingTime.item.item_image_url == "") {
            source = CONSTANT.IMAGE_URL.KLIKNKLIN_ICON;
            imageExist = false;
        }
        else{
            source = {uri:selectedOutletItemWashingTime.item.item_image_url};
            imageExist = true;
        }


        if (!posSelectedItem || posSelectedItem.quantity == 0) {
            return(
                <Image
                    source={source}
                    style={[{
                      width:Scaling.scale(270 * 0.444),
                      height:Scaling.verticalScale(200 * 0.444),
                      resizeMode:'contain'
                    },
                     imageExist == false ? {tintColor:CONSTANT.COLOR.BLUE}: {},
                    ]}
                /> 
            );
        }
        else{
            return(
                <Text fontOption={CONSTANT.TEXT_OPTION.NUMBER_ITEM_SELECTION}>{!posSelectedItem ? 0 : posSelectedItem.quantity}</Text>
            );
        }
    }

    render(){
        let data = this.props.item;
        let {posSelectedItem, selectedOutletItemWashingTime} = this.state;

        return(
            <View
                style={{
                    height:Scaling.verticalScale(555 * 0.444),
                    width:Scaling.scale(300 * 0.444),
                    marginRight:this.props.indexItem < 2 ? Scaling.scale(18 * 0.444) : 0,
                    backgroundColor:CONSTANT.COLOR.WHITE,
                    borderColor:CONSTANT.COLOR.LIGHT_GRAY,
                    borderWidth:Scaling.moderateScale(1),
                }}
            > 
                <View
                    style={{
                        height:Scaling.verticalScale(360 * 0.444), 
                        justifyContent:'center', 
                        alignItems:'center',
                        paddingHorizontal:Scaling.scale(15 * 0.444),
                    }}
                >
                    <View
                        style={{
                            height:Scaling.verticalScale(80 * 0.444), 
                            justifyContent:'center', 
                            alignItems:'center',
                        }}
                    >
                      <Text fontOption={CONSTANT.TEXT_OPTION.CONTENT}
                        textStyle={{textAlign:'center'}}
                      >{data.name}
                      </Text>
                    </View>

                    <View
                        style={{
                            height:Scaling.verticalScale(50 * 0.444),
                            justifyContent:'center', 
                            alignItems:'center',
                        }}
                    >
                        <Text fontOption={CONSTANT.TEXT_OPTION.SUB_CONTENT}
                            textStyle={{
                                color:CONSTANT.COLOR.GRAY,
                                textAlign:'center',
                            }}
                        >
                            {selectedOutletItemWashingTime.washingServiceInfo}
                        </Text>
                    </View>

                    <View
                        style={{height:Scaling.verticalScale(200 * 0.444), justifyContent:'center', alignItems:'center' }}
                    >
                        {this._renderMainContent()}
                    </View>

                </View>

                 <View
                    style={{
                        height:Scaling.verticalScale(100 * 0.444), 
                        justifyContent:'center', 
                        alignItems:'center',
                        paddingHorizontal:Scaling.scale(15 * 0.444)
                }}
                >
                    <Text fontOption={CONSTANT.TEXT_OPTION.CONTENT}>{toCurrency(selectedOutletItemWashingTime.price, 'Rp') + ' / ' + data.unitName}</Text>
                </View>

                {this._renderButton()}

            </View>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        userRole:state.rootReducer.user.role,
        userOutlet:state.rootReducer.user.outlet,
        posSelectedItem:state.rootReducer.pos.selectedItem,
        posOutletItem:state.rootReducer.pos.outletItem,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actionRoot: bindActionCreators(actions.root, dispatch),
        actionNavigator: bindActionCreators(actions.navigator, dispatch),
        actionPOS: bindActionCreators(actions.pos, dispatch),
    };
}



export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(ItemList);

