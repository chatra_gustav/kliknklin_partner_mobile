import React, { Component } from 'react';
import {View, StyleSheet, Dimensions, ScrollView, Platform, TouchableOpacity, Image} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';
import Carousel, {ParallaxImage} from 'react-native-snap-carousel';
import {moderateScale} from 'react-native-size-matters';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, Scaling, toCurrency} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";



class AutoScrollButton extends Component {
    constructor(props) {
      super(props);
    
      this.state = {
      };
    }

    componentDidMount(){
    }

    _onPress(){

    }
	
	render(){
        try{
            let {indexActiveOutlet} = this.props.userOutlet;
            if (indexActiveOutlet != null) {
                return(
                    <TouchableOpacity
                        style={{width:'10%',
                        height:'10%',
                        justifyContent:'center',
                        alignItems:'center',
                        position:'absolute',
                        bottom:Scaling.verticalScale(180 * 0.444),
                        right:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                        paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
                        }}
                        onPress={() => {
                            this.props.actionNavigator.showLightBox('PartnerKliknKlin.ItemSelection', {
                            data:this.props.posOutletItem,
                            categoryToFind:'category',
                            buttonPrimaryText:'Tolak',
                            onPressButtonPrimary:(scrollToIndex) =>{
                              this.props.scrollTo(scrollToIndex);
                            }
                          });
                        }}
                    >
                        <Image
                            source={CONSTANT.IMAGE_URL.AUTO_SCROLL_ICON}
                            style={{
                              width:Scaling.scale(CONSTANT.STYLE.IMAGE.NAVBAR_ICON.WIDTH * 2.5),
                              height:Scaling.verticalScale(CONSTANT.STYLE.IMAGE.NAVBAR_ICON.HEIGHT * 2.5),
                              resizeMode:'contain'
                            }}
                        /> 
                        
                    </TouchableOpacity>
                )
            }
            return null;
        }
        catch(error){
            console.log(error, 'pos.item_selection.submit_button');
            return null;
        }
		
	}
}

function mapStateToProps(state, ownProps) {
	return {
		userRole:state.rootReducer.user.role,
        userOutlet:state.rootReducer.user.outlet,
        posSelectedItem:state.rootReducer.pos.selectedItem,
        posTotalPrice:state.rootReducer.pos.totalPrice,
        posExtractedOutletItem:state.rootReducer.pos.extractedOutletItem,
        posOutletItem:state.rootReducer.pos.outletItem,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
        actionPOS: bindActionCreators(actions.pos, dispatch),
	};
}

const styles = StyleSheet.create({
});

export default connect(mapStateToProps, mapDispatchToProps)(AutoScrollButton);

