import React, { Component } from 'react';
import {View, StyleSheet, Image, Dimensions, TouchableOpacity, Linking, ScrollView, Platform, BackHandler} from 'react-native';

//node js class import
import ResponsiveImage from 'react-native-responsive-image';
import ResolveAssetSource from 'resolveAssetSource';

import {LayoutPrimary} from 'app/data/layout';
import {Text, Button, DateFormat, ShipmentScheduleListCourier, OutletSelector, Label, Scaling} from 'app/data/components';
import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';

//redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from "app/data/actions";

class NoItem extends Component {
	constructor(props) {
        super(props);
        this.state = {
        };  
    }

    componentWillMount(){
    }

    goToWebView(title, url, destination){
 		this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.WEBVIEW, {
			title,
			url,
			needParameter:true,
			destination,
			method:'POST',
		})
 	}

	render(){
		try{
			return(
				<View
						style={{
							flex:1,
							width:'100%',
							height:'100%',
							backgroundColor:CONSTANT.STYLE.CONTAINER.BOX_DASHBOARD.BACKGROUND_COLOR,
							paddingTop:Scaling.verticalScale(CONSTANT.STYLE.CONTAINER.BOX_DASHBOARD.PADDING_TOP),
							paddingHorizontal:Scaling.scale(CONSTANT.STYLE.CONTAINER.BASE.MARGIN_LEFT),
						}}
					>
						<View
							style={{
								flex:0.25,
								alignItems:'center',
								justifyContent:'flex-end',
							}}
						>
							<Text fontOption={CONSTANT.TEXT_OPTION.HEADER}>Anda tidak memiliki produk laundry</Text>
						</View>

						<View
							style={{
								flex:0.35,
								alignItems:'center',
								justifyContent:'center',
							}}
						>
							<Image
					            source={CONSTANT.IMAGE_URL.NO_ITEM_ICON}
					            style={{
						            width:Scaling.scale(225),
						            height:Scaling.verticalScale(225),
						            resizeMode:'contain',
					            }}
					        /> 
						</View>

						<View
							style={{
								flex:0.4,
								alignItems:'center',
							}}
						>
							<Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>
								Untuk pengaturan lebih lanjut silahkan
							</Text>
							<TouchableOpacity
								onPress={this.goToWebView.bind(this,'Data Item', CONSTANT.APP_URL.MOBILE_WEB, 'item')}
								style={{
									marginBottom:Scaling.scale(10),
								}}
							>
								<Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER} 
									textStyle={{
										color:CONSTANT.COLOR.LIGHT_BLUE
									}}
								>Klik Disini</Text>
							</TouchableOpacity>
							<Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER}>
								Atau info lebih lanjut silahkan hubungi 
							</Text>
							<TouchableOpacity
								onPress={() => {
									this.props.actionNavigator.push(this.props.navigator, CONSTANT.ROUTE_TYPE.HELP_CONTENT,{
										helpType:'contact-us'
									})
								}}
							>
								<Text fontOption={CONSTANT.TEXT_OPTION.SUB_HEADER} 
									textStyle={{
										color:CONSTANT.COLOR.LIGHT_BLUE
									}}
								>Tim Kemitraan Kliknklin</Text>
							</TouchableOpacity>
						</View>
						
						
					</View>
			);
		}
		catch(error){
			console.log(error, 'dashboard.summary_order');
			return null;
		}	
	}
}



function mapStateToProps(state, ownProps) {
	return {
		userRole:state.rootReducer.user.role,
		summaryOrder:state.rootReducer.order.summary,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actionRoot: bindActionCreators(actions.root, dispatch),
    	actionNavigator: bindActionCreators(actions.navigator, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(NoItem);

