import SignIn from './partner/sign_in';
import ForgetPassword from './partner/forget_password';
import SMSVerification from './partner/sms_verification';
import HelpList from './partner/help_list';
import HelpContent from './partner/help_content';
import Setting from './partner/setting';
import ChangePassword from './partner/setting/change_password';
import ChangeLocation from './partner/setting/change_location';
import SearchList from './partner/search_list';
import NotificationList from './partner/notification_list';
import Invoice from './partner/invoice';
import OrderList from './partner/order_list';
import Attendance from './partner/attendance';
import AttendanceStatistics from './partner/attendance/attendance_statistics';
import AttendanceDetail from './partner/attendance/attendance_detail';
import SignUp from './partner/sign_up';
import ShipmentSchedule from './partner/shipment_schedule';
import Utility from './partner/utility';
import ConfirmItem from './partner/confirm_item';
import CheckoutItem from './partner/checkout_item';
import ItemDetail from './partner/item_detail';
import BlankPage from './partner/blank_page';
import Deposit from './partner/deposit';
import Cashflow from './partner/deposit/cashflow';
import Membership from './partner/membership';
import OrderMembership from './partner/membership/order_membership';
import OrderMembershipSuccess from './partner/membership/order_success';
import CreateMembership from './partner/membership/create_membership';
import EditMembership from './partner/membership/edit_membership';
import SearchOrder from './partner/search_order';
import Help from './partner/help';
import WebViewScreen from './partner/web_view';
import PosOldVersion from './partner/pos_old_version';
import SearchTransaction from './partner/pos_old_version/search_transaction';
import PaymentPosOldVersion from './partner/pos_old_version/payment_screen';
import Expenses from './partner/expenses';
import CreateExpenditure from './partner/expenses/create_expenditure';

import InternetOfThings from './partner/internet_of_things';
import Dashboard from './partner/dashboard';
import DashboardAdminOutlet from './partner/dashboard/admin_outlet';
import CheckoutAdminOutlet from './partner/checkout_item/admin_outlet';


//pos
import POSItemSelection from './pos/item_selection';
import POSCheckout from './pos/checkout';
import POSPayment from './pos/payment';

//tools
import IotMachine from './partner/iot_machine'
import IotMachineDetail from './partner/iot_machine/iot_machine_detail'
import IotMachineSetting from './partner/iot_machine/iot_machine_setting'

//kurir
import DashboardCourier from './partner/dashboard/courier';
import OrderContent from './partner/order_content';
import CheckoutCourier from './partner/checkout_item/courier';

import {SearchResult} from './partner/search/searchresult';
import {SearchBar} from './partner/search/searchbar';
import {OrderDetail} from './partner/order/detail';

//order list
import {LIncoming} from './partner/order/list/incoming';
import {LPickup} from './partner/order/list/pickup';
import {LOnProgress} from './partner/order/list/onprogress';
import {LDelivery} from './partner/order/list/delivery';

//order detail
import {DPickup} from './partner/order/detail/pickup';
import {DOnProgress} from './partner/order/detail/onprogress';
import {DDelivery} from './partner/order/detail/delivery';

//help
import {FAQ} from './partner/help_list/faq';
import {WorkFlow} from './partner/help_list/workflow';

//order general
import CPickup from './partner/order/general/confirm_pickup_order';
import CIncoming from './partner/order/general/confirm_incoming_order';
import COnProgress from './partner/order/general/confirm_onprogress_order';
import CDelivery from './partner/order/general/confirm_delivery_order';

import RIncoming from './partner/order/general/reject_incoming_order';

import ButtonConfirmListPickup from './partner/order/general/button_confirm_list_pickup_order';
import ButtonConfirmListIncoming from './partner/order/general/button_confirm_list_incoming_order';
import ButtonConfirmListOnProgress from './partner/order/general/button_confirm_list_onprogress_order';
import ButtonConfirmListDelivery from './partner/order/general/button_confirm_list_delivery_order';

import ButtonRejectListIncoming from './partner/order/general/button_reject_list_incoming_order';

var OrderListContent = {
	Incoming:LIncoming,
	Pickup:LPickup,
	OnProgress:LOnProgress,
	Delivery:LDelivery,
};

var OrderDetailContent = {
	Pickup:DPickup,
	OnProgress:DOnProgress,
	Delivery:DDelivery,
};

var ConfirmOrder = {
	Pickup:CPickup,
	Incoming:CIncoming,
	OnProgress:COnProgress,
	Delivery:CDelivery,
};

var RejectOrder = {
	Incoming:RIncoming,
};

var ButtonConfirmList = {
	Pickup:ButtonConfirmListPickup,
	Incoming:ButtonConfirmListIncoming,
	OnProgress:ButtonConfirmListOnProgress,
	Delivery:ButtonConfirmListDelivery,
};

var ButtonRejectList = {
	Incoming:ButtonRejectListIncoming,
};



export {
	SignIn,
	ForgetPassword,
	HelpList,
	HelpContent,
	SMSVerification,
	SearchList,
	NotificationList,
	Invoice,
	OrderList,
	Attendance,
	AttendanceStatistics,
	AttendanceDetail,
	SignUp,
	ShipmentSchedule,
	OrderContent,
	Utility,
	ConfirmItem,
	ItemDetail,
	CheckoutItem,
	BlankPage,
	Deposit,
	Cashflow,
	InternetOfThings,
	IotMachine,
	IotMachineDetail,
	IotMachineSetting,
	Membership,
	OrderMembership,
	OrderMembershipSuccess,
	CreateMembership,
	EditMembership,
	SearchOrder,
	Help,
	WebViewScreen,
	PosOldVersion,
	SearchTransaction,
	PaymentPosOldVersion,
	Expenses,
	CreateExpenditure,
	
	Dashboard,
	DashboardAdminOutlet,
	CheckoutAdminOutlet,

	DashboardCourier,
	CheckoutCourier,
	
	//pos
	POSItemSelection,
	POSCheckout,
	POSPayment,

	ButtonConfirmList,
	ButtonRejectList,
	SearchResult,
	OrderListContent,
	ConfirmOrder,
	RejectOrder,
	OrderDetail,
	OrderDetailContent,
	Setting,
	ChangePassword,
	ChangeLocation,
	SearchBar,
};