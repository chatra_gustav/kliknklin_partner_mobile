import React from 'react';
import { 	
        Platform,
  		} from 'react-native';

import * as fetch from './../fetch';

class Order {

	_searchOrder(keyword, searchCategory, outletID, role, accessToken, searchingType){
		return fetch.postFetch(
			'/order/search',
			{
				keyword:keyword,
				by:searchCategory,
				outlet_id:outletID,
				role:role,
				type: searchingType,
			},
			accessToken,
		);
	};

	_incomingResponse(isAccepted, courierID, outletID, orderID, rejectReasonID, accessToken){
		return fetch.postFetch(
			'/order/incomingresponse',
			{
				answer:isAccepted,
				courier_id:courierID,
				outlet_id:outletID,
				order_id:orderID,
				reject_reason_id: rejectReasonID,
			},
			accessToken,
		);
	};

	_onLaunder(orderID, outletID, orderItem, accessToken){
		return fetch.postFetch(
			'/order/onlaunder',
			{
				outlet_id:outletID,
				order_id:orderID,
				order_items:orderItem,
			},
			accessToken,
		);
	}

	_readyToBeDelivered(orderID, outletID,  accessToken){
		return fetch.postFetch(
			'/order/ready',
			{
				outlet_id:outletID,
				order_id:orderID,
			},
			accessToken,
		);
	}

	_getCheckoutItemList(orderID, accessToken){
		return fetch.postFetch(
			'/orderitem/show',
			{
				order_id:orderID,
			},
			accessToken,
		);
	}

	_startShipment(shipmentID, userLocation, accessToken){
		let locationParameter = '';
		if (userLocation) {
			locationParameter = '?latitude=' + userLocation.latitude + '&longitude=' + userLocation.longitude;
		}

		return fetch.getFetch(
			'/shipment/start/' + shipmentID + locationParameter,
			accessToken,
		);
	}

	_finishShipment(shipmentID, userLocation, courierNote = '', accessToken){
		let bodyParameter = {
			notes:courierNote,
		};

		if (userLocation) {
			bodyParameter = {
				notes:courierNote,
				latitude:userLocation.latitude,
				longitude:userLocation.longitude,
			};
		}

		return fetch.postFetch(
			'/shipment/finish/' + shipmentID,
			bodyParameter,
			accessToken,
		);
	}

	_setDelayShipment(shipmentID, delayReasonID, notes, accessToken){
		return fetch.postFetch(
			'/shipment/delay/' + shipmentID,
			{
				delay_reason_id:delayReasonID,
				notes:notes,
			},
			accessToken,
		);
	}

	_getOrderContent(order_id, accessToken){
		return fetch.getFetch(
			'/order/detail/' + order_id ,
			accessToken,
		);
	}

	_getShipmentSchedule(date, accessToken){
		return fetch.getFetch(
			'/schedule/courier/' + date ,
			accessToken,
		);
	}

	_getSummaryOrder(outletID, accessToken){
		return fetch.getFetch(
			'/order/summary/' + outletID,
			accessToken,
		);
	}

	_getHistoryList(outletID, fromDate, toDate, accessToken){
		return fetch.getFetch(
			'/order/list/history/' + outletID + '/' + fromDate + '/' + toDate,
			accessToken,
		);
	}

	_getIncomingList(outletID, accessToken){
		return fetch.getFetch(
			'/order/list/incoming/' + outletID,
			accessToken,
		);
	}

	_getPickupList(outletID, accessToken){
		return fetch.getFetch(
			'/order/list/pickup/' + outletID,
			accessToken,
		);
	}

	_getInProgressList(outletID, accessToken){
		console.log(outletID);
		console.log(accessToken);
		return fetch.getFetch(
			'/order/list/onprogress/' + outletID,
			accessToken,
		);
	}

	_getDeliveryList(outletID, accessToken){
		return fetch.getFetch(
			'/order/list/delivery/' + outletID,
			accessToken,
		);
	}

	_getComplaintList(outletID, accessToken){
		return fetch.getFetch(
			'/complaint/list/' + outletID,
			accessToken,
		);
	}

	_getComplaintMessageList(complaintID, accessToken){
		return fetch.getFetch(
			'/complaint/message/' + complaintID,
			accessToken,
		);
	}

	_getSummaryOrder(outletID, accessToken){
		return fetch.getFetch(
			'/order/summary/' + outletID,
			accessToken,
		);
	}

	_fetchSummaryIncomingOrder(user){
		return fetch('https://partner.kliknklin.co/api/v1/orders/summary/incoming/' + user.laundry_id, {
		  method: 'GET',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json',
		    'Access-Token': user.access_token,
		  }
		})
		.then(
			(response) => response.json()
		)
		.then(
			(responseData) => {
				return responseData;
			}
		)
		.catch();
	}
	

	_fetchPickupOrder(user){
		return fetch('https://partner.kliknklin.co/api/v1/orders/pickup/list/' + user.laundry_id, {
		  method: 'GET',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json',
		    'Access-Token': user.access_token,
		  }
		})
		.then(
			(response) => response.json()
		)
		.then(
			(responseData) => {
				console.log(responseData, 'anjing lha');
				return responseData;
			}
		)
		.catch();
	}

	_fetchIncomingOrder(user){
		return fetch('https://partner.kliknklin.co/api/v1/orders/incoming/list/' + user.laundry_id, {
		  method: 'GET',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json',
		    'Access-Token': user.access_token,
		  }
		})
		.then(
			(response) => response.json()
		)
		.then(
			(responseData) => {
				return responseData;
			}
		)
		.catch();
	}

	_submitIncomingOrder(user){
		return fetch('https://partner.kliknklin.co/api/v1/orders/incoming/response/'+user.laundry_id, {
		  method: 'POST',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json',
		    'Access-Token': user.access_token,
		  },
		  body: JSON.stringify({
		    action:user.order_action,
		    order_id:user.order_id,
		    courier_id:user.order_action == 'accept' ? user.courier_id : null,
		  })
		})
		.then(
			(response) => response.json()
		)
		.then(
			(responseData) => {
				return responseData;
			}
		)
		.catch();
	}

	_submitPickupOrder(user){
		return fetch('https://partner.kliknklin.co/api/v1/orders/pickup/checkout/'+user.laundry_id, {
		  method: 'POST',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json',
		    'Access-Token': user.access_token,
		  },
		  body: JSON.stringify({
		    order_id:user.order_id,
		    weight:user.item_weight,
		    pieces:user.pieces,
		  })
		})
		.then(
			(response) => response.json()
		)
		.then(
			(responseData) => {
				return responseData;
			}
		)
		.catch();
	}

	_submitOnProgressOrder(user){
		return fetch('https://partner.kliknklin.co/api/v1/orders/onprogress/ready/'+user.laundry_id, {
		  method: 'POST',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json',
		    'Access-Token': user.access_token,
		  },
		  body: JSON.stringify({
		    order_id:user.order_id,
		  })
		})
		.then(
			(response) => response.json()
		)
		.then(
			(responseData) => {
				return responseData;
			}
		)
		.catch();
	}

	_submitDeliveryOrder(user){
		return fetch('https://partner.kliknklin.co/api/v1/orders/delivery/delivered/'+user.laundry_id, {
		  method: 'POST',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json',
		    'Access-Token': user.access_token,
		  },
		  body: JSON.stringify({
		    order_id:user.order_id,
		  })
		})
		.then(
			(response) => response.json()
		)
		.then(
			(responseData) => {
				return responseData;
			}
		)
		.catch();
	}


	_fetchOnProgressOrder(user){
		return fetch('https://partner.kliknklin.co/api/v1/orders/onprogress/list/' + user.laundry_id, {
		  method: 'GET',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json',
		    'Access-Token': user.access_token,
		  }
		})
		.then(
			(response) => response.json()
		)
		.then(
			(responseData) => {
				return responseData;
			}
		)
		.catch();
	}

	_fetchDeliveryOrder(user){
		return fetch('https://partner.kliknklin.co/api/v1/orders/delivery/list/' + user.laundry_id, {
		  method: 'GET',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json',
		    'Access-Token': user.access_token,
		  }
		})
		.then(
			(response) => response.json()
		)
		.then(
			(responseData) => {
				return responseData;
			}
		)
		.catch();
	}

	_searchAllOrder(user){
		return fetch('https://partner.kliknklin.co/api/v1/orders/search/'+user.laundry_id+'/'+user.search_by+'/'+user.search_query, {
			  method: 'GET',
			  headers: {
			    'Accept': 'application/json',
			    'Content-Type': 'application/json',
			    'Access-Token': user.access_token,
			  }
			})
			.then(
				(response) => response.json()
			)
			.then(
				(responseData) => {
					return responseData;
				}
			)
			.catch();
		}

	_sendEmail(params, accessToken){
		return fetch.postFetch(
			'/order/email/invoice',
			{
				order_id:params.order_id,
				subtotalprice: params.subtotalprice,
				expresscharge: params.expresscharge,
				discount: params.discount,
				totalprice: params.totalprice
			},
			accessToken,
		);
	}
}

module.exports = {Order:new Order()};