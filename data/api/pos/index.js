import React from 'react';
import {Platform,} from 'react-native';
import * as fetch from './../fetch';

class POS {
	_getOutletItem(outletID, accessToken){
		return fetch.getFetch(
			'/item/list/'+outletID,
			accessToken,
		);
	};

	_getCustomerOutlet(outletID, accessToken){
		return fetch.getFetch(
			'/customer/outlet/'+outletID,
			accessToken,
		);
	};

	_validatePromo(promo_code, customer_id, accessToken){
		return fetch.postFetch(
			'/promo/validate',
			{
				promo_code:promo_code,
				customer_id:customer_id,
			},
			accessToken,
		);
	}

	_createOrder(data, accessToken){
		return fetch.postFetch(
			'/order/store',
			{
				customer:JSON.stringify(data.customer),
				order:JSON.stringify(data.order),
				order_items:JSON.stringify(data.order_items),
				payment:JSON.stringify(data.payment),
				promo:JSON.stringify(data.promo),
			},
			accessToken,
		);
	};
	
	_updatePayment(order_id, accessToken){
		return fetch.postFetch(
			'/order/payment/change',
			{
				order_id:order_id,
			},
			accessToken,
		);
	};
}

module.exports = {POS: new POS()};