import React from 'react';
import { 	
        Platform,
  		} from 'react-native';

import DeviceInfo from 'react-native-device-info';

import * as fetch from './../fetch';

class Authentication {
	_login(user, pushNotifToken){
		//origin login blom
		return fetch.postFetch(
			'/login' , 
			{
				'email': user.email,
			    'password': user.password,
				'origin_login':'partner_mobile-'+Platform.OS + '-' + DeviceInfo.getVersion(),
			    'registrationtoken': pushNotifToken,
			},
		);
	}

	_forgetPassword(email){
		return fetch.postFetch(
			'/password/reset',
			{
				email,
			}

		);
	}

	_loginAccessToken(user, pushNotifToken){
		return fetch.postFetch(
			'/login/token/access' , 
			{	
				'origin_login':'partner_mobile-'+Platform.OS + '-' + DeviceInfo.getVersion(),
				'registrationtoken': pushNotifToken,
			},
			user.access_token,
		);
	}

	_loginRefreshToken(user, pushNotifToken){
		return fetch.postFetch(
			'/login/token/refresh' , 
			{
				'refresh_token':user.refresh_token,
				'origin_login':'partner_mobile-'+Platform.OS + '-' + DeviceInfo.getVersion(),
				'registrationtoken': pushNotifToken,
			},
		);
	}

	_sendMobileVerificationCode(user){
		let {accessToken} = user;
		return fetch.getFetch(
			'/sms/activate/resend' ,
			accessToken,
		);
	}

	_verifyMobileVerificationCode(token_otp){
		return fetch.postFetch(
			'/sms/activate/verify' , 
			{
				token_otp,
			},
		);
	}

	_logout(access_token, pushNotifToken){
		console.log(pushNotifToken);
		return fetch.postFetch(
			'/logout' , 
			{
			    'registrationtoken': pushNotifToken,
			},
			access_token,
		);
	}

	_fetchProfile(user){
		return fetch('https://partner.kliknklin.co/api/v1/profile/fetch', {
		  method: 'GET',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json',
		    'Access-Token': user.access_token,
		  },
		})
		.then(
			(response) => response.json()
		)
		.then(
			(response) => {
				return response;
			}
		)
		.catch();
	}

	_makeRandom(amount) {
	  var text = "";
	  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+-={[}]:\,?";

	  for (var i = 0; i < amount; i++)
	    text += possible.charAt(Math.floor(Math.random() * possible.length));

	  return text;
	}

	_partnerKnKEncrypt(word){
		var encrypted = '';
		
		encrypted = word.split("").reverse().join("");


		encrypted = this._makeRandom(6) + encrypted + this._makeRandom(4);
		return encrypted;
	}

	_partnerKnKDecrypt(word){
		var decrypted = word.substring(6, word.length - 4);
		
		decrypted = decrypted.split("").reverse().join("");

		return decrypted;
	}

	_changePassword(params, access_token){
		return fetch.postFetch(
			'/password/change' , 
			{
			    'current-password': params.currentPassword,
			    'password': params.password,
			    'password_confirmation': params.password_confirmation
			},
			access_token,
		);
	}
}

module.exports = {Authentication: new Authentication()};