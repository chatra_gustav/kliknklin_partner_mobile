import React from 'react';
import { 	
        Platform,
  		} from 'react-native';

import * as fetch from './../fetch';

class Complaint{
	setItemSolution(complaint_id, complaint_item_solution_id, complaint_item_id, accessToken){
		return fetch.postFetch(
			'/complaint/solution/suggest',
			{
				complaint_id,
				complaint_item_solution_id,
				complaint_item_id,
			},
			accessToken,
		);
	}

	sendComplaintMessage(complaint_id, user_id, message, accessToken){
		return fetch.postFetch(
			'/complaint/response/add',
			{
				complaint_id,
				user_id,
				message,
			},
			accessToken,
		);
	}

	markComplaintSolved(complaint_id, accessToken){
		return fetch.postFetch(
			'/complaint/marksolved',
			{
				complaint_id,
			},
			accessToken,
		);
	}

	_startShipment(shipmentID, accessToken){
		return fetch.getFetch(
			'/shipment/start/' + shipmentID,
			accessToken,
		);
	}
}



module.exports = {Complaint: new Complaint()};