import React from 'react';
import { 	
        Platform,
  		} from 'react-native';

import * as fetch from './../fetch';

class Membership {

	searchCustomer(type, search, accessToken){

		return fetch.postFetch(
			'/search/customer' , 
			{
				'type': type,
			    'search': search,
			},
			accessToken,
		);
	}

	getMembershipPackage(outlet_id, accessToken){
		return fetch.getFetch(
			'/membership/list/'+ outlet_id,
			accessToken,
		);
	}

	membershipStore(params, accessToken){

		return fetch.postFetch(
			'/membership/store' , 
			{
				outlet_id: params.outlet_id,
			    package_name: params.package_name,
			    package_price: params.package_price,
			    total_voucher: params.total_voucher,
			    voucher_price: params.voucher_price,
			    voucher_percentage: params.voucher_percentage,
			    terms_and_conditions: params.terms_and_conditions,
			    duration: params.duration
			},
			accessToken,
		);
	}

	membershipApply(customer_id, outlet_id, membership_package_id, accessToken){

		return fetch.postFetch(
			'/membership/apply' , 
			{
				customer_id: customer_id,
			    outlet_id: outlet_id,
			    membership_package_id: membership_package_id,
			},
			accessToken,
		);
	}

	membershipUpdate(params, accessToken){

		return fetch.postFetch(
			'/membership/update' , 
			{
				membership_package_id: params.membership_package_id,
			    package_name: params.package_name,
			    package_price: params.package_price,
			},
			accessToken,
		);
	}

}

module.exports = {Membership:new Membership()};