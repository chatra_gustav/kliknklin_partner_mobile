import React from 'react';
import {Platform,} from 'react-native';
import * as fetch from './../fetch';

class Customer {
	_createCustomerOutlet(name, mobile_phone, email, outlet_id, accessToken, type = 'new'){
		return fetch.postFetch(
			'/customer/create',
			{
				name,
				mobile_phone,
				email,
				outlet_id,
				from:type,
			},
			accessToken,
		);
	}

	_searchCustomer(mobile_phone, accessToken, type = 'phone'){
		return fetch.postFetch(
			'/search/customer',
			{
				search:mobile_phone,
				type,
			},
			accessToken,
		);
	}
}

module.exports = {Customer: new Customer()};