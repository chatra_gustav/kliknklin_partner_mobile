  import * as CONSTANT from './../constant';

  function getHost() {
    return CONSTANT.APP_URL.API;
  }

  function getHost2() {
    return 'http://pos.kliknklin.co/api/v1/external';
  }

  function secondTimeout(){
    return(
      {
        get:15000,
        post:5000,
        put:10000,
        delete:10000,
      }
    ) 
  }

  function fetchs(ms, promise) {
    return new Promise(function(resolve, reject) {
      setTimeout(function() {
        let error = new Error(CONSTANT.ERROR_TYPE.TIMEOUT);
        error.message = CONSTANT.ERROR_TYPE.TIMEOUT;
        reject(error);
      }, ms)
      promise.then(resolve, reject)
    })
  }

  function header(access_token) {
    if (access_token) {
      return {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'dataType': 'json',
        'Access-Token' : access_token,
      }
    }
    return {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'dataType': 'json',
    }
  }

  function xhr (route, params, verb, access_token, host) {
    var url = `${host}${route}`;
    let options = Object.assign({ method: verb }, params ? { body: JSON.stringify(params) } : null );
    options.headers = header(access_token);
    
    return fetch(url, options).then( response => {
      let json = response.json();
      if (response.ok) {
        return json;
      }
      else{
        return json.then(err => {throw err});
      }
    }).then( json => json );
  }

  export function getFetch2(route, access_token = null, host = getHost2()) { // pos sistem lama
    return fetchs(secondTimeout().get, xhr(route, null, 'GET', access_token, host));
  }

  export function getFetch(route, access_token = null, host = getHost()) {
    return fetchs(secondTimeout().get, xhr(route, null, 'GET', access_token, host));
  }

  export function putFetch(route, params, access_token = null, host = getHost()) {
    return fetchs(secondTimeout().put, xhr(route, params, 'PUT', access_token, host));
  }

  export function postFetch(route, params, access_token = null, host = getHost()) {
    return fetchs(secondTimeout().post, xhr(route, params, 'POST', access_token, host));
  }

  export function postFetch2(route, params, access_token = null, host = getHost2()) {
    return fetchs(secondTimeout().post, xhr(route, params, 'POST', access_token, host));
  }

  export function deleteFetch(route, params, access_token = null, host = getHost()) {
    return fetchs(secondTimeout().delete, xhr(route, params, 'DELETE', access_token, host));
  }