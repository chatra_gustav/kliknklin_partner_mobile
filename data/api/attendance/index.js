import React from 'react';
import { 	
        Platform,
  		} from 'react-native';

import * as fetch from './../fetch';

class Attendance {

	_getStaff(outletID, date, accessToken){

		return fetch.getFetch(
			'/staff/list/' + outletID + '/' + date,
			accessToken,
		);
	}

	_staffIn(outletID ,staff, accessToken, userLocation){
		let bodyData = {
				outlet_id:outletID,
		    	user_id:staff.user_id,
		    	photo_url_in:staff.photo_uri,
		};

		if (userLocation) {
			bodyData.latitude = userLocation.latitude;
			bodyData.longitude = userLocation.longitude;
		}
		return fetch.postFetch(
			'/staff/in',
			bodyData,
			accessToken,
		);
	};

	_staffOut(outletID, staff, accessToken, userLocation){
		let bodyData = {
				outlet_id:outletID,
		    	user_id:staff.user_id,
		    	photo_url_in:staff.photo_uri,
		};

		if (userLocation) {
			bodyData.latitude = userLocation.latitude;
			bodyData.longitude = userLocation.longitude;
		}

		return fetch.postFetch(
			'/staff/out',
			bodyData,
			accessToken,
		);
	}

	_getStaffStatistics(outletID, date, accessToken){
		return fetch.getFetch(
			'/staff/working/' + outletID + '/' + date,
			accessToken,
		);
	}

	
}

module.exports = {Attendance:new Attendance()};