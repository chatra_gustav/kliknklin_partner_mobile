import React from 'react';
import { 	
        Platform,
  		} from 'react-native';
import DeviceInfo from 'react-native-device-info';

import * as fetch from './../fetch';

class Root {
	_checkUpdate(){
		return fetch.getFetch(
			'/app/update/partner_mobile/' + Platform.OS + '/' + DeviceInfo.getVersion()
		);
	}

	_submitFeedback(accessToken, feedback, feedbackType = 7, feedbackImageURL = null){
		return fetch.postFetch(
			'/feedbacks/store',
			{
				feedback: feedback,
				feedback_type: feedbackType,
				image_url:feedbackImageURL,
			},
			accessToken
		);
	}

	_getMDExpenditureCategory(){
		return fetch.getFetch(
			'/md/expenditurecategory'
		);
	}

	_getMDDelayReason(){
		return fetch.getFetch(
			'/md/delayreasons'
		);
	}

	_getMDRejectReason(){
		return fetch.getFetch(
			'/md/rejectreasonpartner'
		);
	}

	_getMDComplaintItemSolution(){
		return fetch.getFetch(
			'/md/complaintitemsolution'
		);
	}

	_getNotification(accessToken){
		return fetch.getFetch(
			'/notifications/get',
			accessToken,
		);
	}

	_getInformation(accessToken){
		return fetch.getFetch(
			'/general/carousel',
			accessToken,
		);
	}
	
	_updateNotification(notification_id ,accessToken){

		return fetch.postFetch(
			'/notifications/update',
			{
				notification_id:notification_id,
			},
			accessToken,
		);
	}



	_getDownloadLink(user){
		return fetch('https://partner.kliknklin.co/api/v1/generate/download', {
		  method: 'GET',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json',
		    'Access-Token': user.access_token,
		  },
		})
		.then(
			(response) => response.json()
		)
		.then(
			(responseData) => {
				return responseData;
			}
		)
		.catch();
	}

	_getMaxItemPieces(user){
		return fetch('https://semeru.kliknklin.co/api/mobile/maxitempieces', {
		  method: 'GET',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json',
		    'Access-Token': user.access_token,
		  },
		})
		.then(
			(response) => response.json()
		)
		.then(
			(responseData) => {
				return responseData;
			}
		)
		.catch();
	}

	_getCourierList(user){
		return fetch('https://partner.kliknklin.co/api/v1/couriers/list/' + user.laundry_id, {
		  method: 'GET',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json',
		    'Access-Token': user.access_token,
		  },
		})
		.then(
			(response) => response.json()
		)
		.then(
			(responseData) => {
				return responseData;
			}
		)
		.catch();
	}

	_getWebLinks(){
		return fetch.getFetch(
			'/links'
		);
	}

	_getMdTIme(){
		return fetch.getFetch(
			'/general/md/time'
		);
	}

	changeOutletOperationTime(outlet_id, params, accessToken){
		return fetch.postFetch(
			'/outlet/change',
			{
				type:'time',
				outlet_id: outlet_id,
				open_operasional_time_id: params.open_operasional_time_id,
				close_operasional_time_id: params.close_operasional_time_id
			},
			accessToken,
		);
	}

	changeOutletLocation(outlet_id, params, accessToken){
		return fetch.postFetch(
			'/outlet/change',
			{
				type:'location',
				outlet_id: outlet_id,
				latitude: params.latitude,
				longitude: params.longitude
			},
			accessToken,
		);
	}
}

module.exports = {Root:new Root()};