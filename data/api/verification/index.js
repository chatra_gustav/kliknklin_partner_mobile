import React from 'react';
import { 	
        Platform,
  		} from 'react-native';

class Verification {
	_sendCodeMobile(user){
		return fetch('https://partner.kliknklin.co/api/v1/sms/verification/send', {
		  method: 'GET',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json',
		    'Access-Token': user.access_token,
		  }
		})
		.then(
			(response) => response.json()
		)
		.then(
			(response) => {
				return response;
			}
		)
		.catch();
	}

	_submitCodeMobile(user){
		return fetch('https://partner.kliknklin.co/api/v1/user/mobile/activate/'+ user.mobile_verification_code, {
		  method: 'GET',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json',
		    'Access-Token': user.access_token,
		  }
		})
		.then(
			(response) => response.json()
		)
		.then(
			(response) => {
				return response;
			}
		)
		.catch();	
	}
}

module.exports = {Verification: new Verification()};