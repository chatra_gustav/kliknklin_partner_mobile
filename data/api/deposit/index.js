import React from 'react';
import { 	
        Platform,
  		} from 'react-native';

import * as fetch from './../fetch';

class Deposit {

	getSummary(accessToken){
		return fetch.getFetch(
			'/balancedetail',
			accessToken,
		);
	}

	cashflowTransaction(startDate, endDate, accessToken){

		return fetch.getFetch(
			'/cashflow/'+ startDate +'/'+ endDate,
			accessToken,
		);
	}

	wdrRequest(accessToken){
		return fetch.getFetch(
			'/balance/waiting',
			accessToken,
		);
	}

	topUpBalance(owner_bank_detail_id, kliknklin_bank_detail_id, owner_account_number, amount, payment_image_url, accessToken){
		
		return fetch.postFetch(
			'/balance/topup',
			{
				owner_bank_detail_id: owner_bank_detail_id,
				kliknklin_bank_detail_id: kliknklin_bank_detail_id,
				owner_account_number: owner_account_number,
				amount: amount,
				payment_image_url: payment_image_url
			},
			accessToken,
		);
	}

	withdrawBalance(owner_bank_detail_id, kliknklin_bank_detail_id, owner_account_number, amount, accessToken){
		
		return fetch.postFetch(
			'/balance/withdraw',
			{
				owner_bank_detail_id: owner_bank_detail_id,
				kliknklin_bank_detail_id: kliknklin_bank_detail_id,
				owner_account_number: owner_account_number,
				amount: amount
			},
			accessToken,
		);
	}

	partner(accessToken){
		return fetch.getFetch(
			'/partner',
			accessToken,
		);
	}

	knkBank(){
		return fetch.getFetch(
			'/md/knkbank',
		);
	}

}

module.exports = {Deposit:new Deposit()};