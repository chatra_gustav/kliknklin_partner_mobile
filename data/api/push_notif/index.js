import React from 'react';
import { 	
        Platform,
  		} from 'react-native';

class PushNotif {
	_saveToken(user){
		return fetch('https://partner.kliknklin.co/api/v1/notification/savetoken', {
		  method: 'POST',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json',
		    'Access-Token': user.access_token,
		  },
		  body: JSON.stringify({
		    'registrationtoken': user.registration_token,
		  })
		})
		.then(
			(response) => response.json()
		)
		.then(
			(response) => {
				return response;
			}
		)
		.catch();
	}
}

module.exports = {PushNotif: new PushNotif()};