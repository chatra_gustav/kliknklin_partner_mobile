import React from 'react';
import * as fetch from './../fetch';

class PosOldVersion {

	_getTransaction(outletID){
		return fetch.getFetch2(
			'/transactions/' + outletID
		);
	}

	_updateTransaction(params){
		return fetch.postFetch2(
			'/transaction/status/update',
			{
				transaction_id:params.transaction_id,
		    	payment_status_id:params.payment_status_id,
		    	transaction_status_id:params.transaction_status_id
			},
		);
	}
}

module.exports = {PosOldVersion:new PosOldVersion()};