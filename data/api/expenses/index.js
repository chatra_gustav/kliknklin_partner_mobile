import React from 'react';
import { 	
        Platform,
  		} from 'react-native';

import * as fetch from './../fetch';

class Expenses {

	_createExpense(expenditure_category_id, user_id, outlet_id, amount, receipt_date, receipt_image_url, info, accessToken){
		return fetch.postFetch(
			'/expenditure',
			{
				expenditure_category_id,
				user_id,
				outlet_id,
				amount,
				receipt_date,
				receipt_image_url,
				info,
			},
			accessToken,
		);
	}

	_getExpenseList(outlet_id, month, accessToken){
		return fetch.getFetch(
			'/expenditure/' + outlet_id + '/' + month,
			accessToken,
		);
	}

	_deleteExpense(id, accessToken){
		return fetch.deleteFetch(
			'/expenditure/' + id,
			accessToken,
		);
	}
}

module.exports = {Expenses:new Expenses()};