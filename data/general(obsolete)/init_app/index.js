import React, { Component } from 'react';
import { AppState, TouchableOpacity, View, Platform, Keyboard, BackHandler, Alert, Linking, AsyncStorage, PermissionsAndroid, Dimensions, NetInfo} from 'react-native';
import {Navigator} from 'react-native-deprecated-custom-components';

//node js class import
import Drawer from 'react-native-drawer';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SInfo from 'react-native-sensitive-info';
import RNFetchBlob from 'react-native-fetch-blob';
import DeviceInfo from 'react-native-device-info';
import TimerMixin from 'react-timer-mixin';

//api
import {Authentication, Order, General,PushNotif as PushNotifAPI} from './../../route/api';

import {PushNotif, ConnectionError} from './../../route/general';

//feature js class import
import {
	SignIn,
	Dashboard,
	SearchResult,
	OrderList,
	OrderDetail,
	Setting,
	Help,
	HelpContent,
} from './../../route/feature';

//general js class import
import {Menu} from './../../route/general';

//custom component js class import
import {TextWrapper, Modal} from './../../route/custom_component';

import {Navigation} from 'react-native-navigation';


export class InitApp extends Component {
	constructor(props) {
        super(props);
        this.state = {
			isNavBarHeaderVisible:false,
			data:{
				loginState:{},
				userState:{},
				outlet:{
					listName: ['Sedang memuat...'],
					listID: [],
					activeID : 0,
				},
				courier:{
					listName : [''],
					listID: [],
				},
				summary:{},
				summaryincoming:0,
				secondReloadSummary:600000,
				secondReloadSummaryIncoming:60000,
			},
			load:{
				isConfigStart:{
					app:false,
					login:false,
					update:false,
					profile:false,
					pushNotif:false,
					outlet:false,
					courier:false,
					summary:false,
					summaryIncoming:false,
					intervalSummary:false,
					maxItemPieces:false,
				},
				isConfigFinish:{
					app:false,
					login:false,
					update:false,
					profile:false,
					pushNotif:false,
					outlet:false,
					courier:false,
					summary:false,
					summaryIncoming:false,
					intervalSummary:false,
					maxItemPieces:false,
				},
				isConfigSuccess:{
					app:false,
					login:false,
					update:false,
					profile:false,
					pushNotif:false,
					outlet:false,
					courier:false,
					summary:false,
					summaryIncoming:false,
					intervalSummary:false,
					maxItemPieces:false,
				},
				isShouldReloadSummary:false,
				isShouldReloadSummaryIncoming:false,
			},
			timeout:{
				login:5000,
				update:5000,
				profile:5000,
				courier:5000,
				summary:5000,
				summaryIncoming:5000,
				maxItemPieces:5000,
			},
			modal:{
                isProgressLoadingVisible:false,
                typeProgressModal:'standard',
                loadingWord:'memuat data yang diperlukan...'
            },
			string:{
				error:{
					reloginFailed:'Mohon maaf, anda harus login ulang untuk masuk kedalam sistem.',
					profileUnauthorized:'Anda tidak diberi wewenang untuk masuk kedalam sistem, hubungi pemilik laundry / kliknklin untuk info lebih lanjut',
					unknown:'Terdapat kesalahan ! coba login ulang atau hubungi kami untuk info lebih lanjut'
				}
			},
			url:{
				update:'https://partner.kliknklin.co/download/apk',
			}
        };  
	}

	componentWillMount(){
		this._setLoginState(null, true, () => {});
	}

	componentDidMount(){
		this._configApp();
	}

	componentWillUnmount(){
		if (this._pushnotif) {
			this._pushnotif._removeListenerPushNotif();
		}
	}

	componentDidUpdate(prevProps, prevState){
		this._listenerConfig(prevProps, prevState);
	}

	async _startApp(){
		if (Object.keys(this.state.data.loginState).length == 0) {
			Navigation.startSingleScreenApp({
                screen: {
                    screen: 'PartnerKliknKlin.SignIn', 
                    title: 'Welcome', 
                    navigatorStyle: {navBarHidden:true}, 
                    navigatorButtons: {} 
                    },
				animationType:'none',
				passProps:{
					_setConfigLoginDone : this._setConfigLoginDone.bind(this),
				}
            });
		}
		else{
			const role = await AsyncStorage.getItem('partnerkliknklin_role');
			if (role == 'courier'){
			   console.log(role);
			}
			// Navigation.startTabBasedApp({
   //              tabs: [
   //              {
   //                  label: 'Home',
   //                  screen: 'ReactNativeReduxExample.HomeTab',
   //                  icon: require('./img/checkmark.png'),
   //                  selectedIcon: require('./img/checkmark.png'),
   //                  title: 'Hey',
   //                  overrideBackPress: false, //this can be turned to true for android
   //                  navigatorStyle: {}
   //              },
   //              {
   //                  label: 'Search',
   //                  screen: 'ReactNativeReduxExample.SearchTab',
   //                  icon: require('./img/checkmark.png'),
   //                  selectedIcon: require('./img/checkmark.png'),
   //                  title: 'Hey',
   //                  navigatorStyle: {}
                    
   //              }
               
   //              ],
   //          });
		}
	}

	_listenerConfig(prevProps, prevState){
		var isConfigStart = this.state.load.isConfigStart;
		var isConfigFinish = this.state.load.isConfigFinish;
		var isConfigSuccess = this.state.load.isConfigSuccess;

		console.log('super hai')
		if (!isConfigSuccess.app) {
			if (!isConfigStart.login) {
				isConfigStart.login = true;
				this._configLoginState();
			}
			if (isConfigSuccess.login) {
				if (isConfigSuccess.update && !isConfigFinish.profile) {
					isConfigFinish.profile = true;

					this._configUserProfile(this.state.data.loginState);
				}
			}

			// if (isConfigSuccess.login) {

			// 	if (!isConfigFinish.update) {
			// 		console.log('1');
			// 		isConfigFinish.update = true;

			// 		this._showModal(true, 'memeriksa update...', () => {
			// 			this._getNavigator().resetTo({id:'Dashboard', title:'Dashboard'});
			// 			this._configCheckUpdate(this.state.data.loginState);
			// 		});	
			// 	}


			// 	if (!isConfigFinish.pushNotif) {
			// 		isConfigFinish.pushNotif = true;

			// 		this._configPushNotif(this.state.data.loginState);
			// 	}			
			// }
			
			// if (isConfigSuccess.update && !isConfigFinish.profile) {
			// 	console.log('2');
			// 	isConfigFinish.profile = true;

			// 	this._showModal(true, 'memuat data yang diperlukan...', () => {
			// 		this._configUserProfile(this.state.data.loginState);
			// 	});
			// }

			// if (isConfigSuccess.profile && !isConfigFinish.outlet) {
			// 	console.log('3');
			// 	isConfigFinish.outlet = true;

			// 	this._configOutlet(this.state.data.userState);
			// }

			// if (isConfigSuccess.outlet) {
			// 	if (!isConfigFinish.courier) {
			// 		console.log('4');
			// 		isConfigFinish.courier = true;

			// 		this._configCourier(this.state.data.loginState);
			// 	}

			// 	if (!isConfigFinish.summary) {
			// 		console.log('5');
			// 		isConfigFinish.summary = true;

			// 		this._configSummaryOrder(this.state.data.loginState, true);
			// 	}

			// 	if (!isConfigFinish.summaryIncoming) {
			// 		console.log('6');
			// 		isConfigFinish.summaryIncoming = true;

			// 		this._configSummaryIncoming(this.state.data.loginState, true);
			// 	}

			// 	if (!isConfigFinish.maxItemPieces) {
			// 		console.log('7');
			// 		isConfigFinish.maxItemPieces = true;

			// 		this._configMaxItemPieces(this.state.data.loginState);
			// 	}
			//}

			if (
				//belom login
				(isConfigFinish.login && !isConfigSuccess.login) || 
					//udah login
					(
						isConfigSuccess.login && 
						isConfigSuccess.profile
					)
				) {
				
				this.props.navigator.dismissModal({
				  animationType: 'slide-down'
				});

				//this._runIntervalSummary();
				
				isConfigFinish.app = true;

				this._startApp();
			}
		}
	}

	_configApp(){
		this._configBackHandler();

		this.forceUpdate();
	}

	_runIntervalSummary(){
		this.reload_summary = TimerMixin.setInterval(() =>{
			this._setShouldReloadSummary(true);
		}, this.state.data.secondReloadSummary);

		this.reload_summary_incoming = TimerMixin.setInterval(() =>{
			this._setShouldReloadSummaryIncoming(true);
		}, this.state.data.secondReloadSummaryIncoming);
	}

	async _downloadUpdate(){
		//android 6.0 bisa nolak ijin download, kalo nolak download jadi pake linking, sementok2nya kasih tau linknya download manual

		const android = RNFetchBlob.android;

		var granted = false;

		try {
			const askPermitWrite = await PermissionsAndroid.requestMultiple([
		      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
		      PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
		      ],
		      {
		        'title': 'Aplikasi butuh ijin',
		        'message': 'untuk bisa update aplikasi tekan allow / ijinkan',
		      }
		    )

		    if (askPermitWrite['android.permission.READ_EXTERNAL_STORAGE'] === 'granted' && askPermitWrite['android.permission.WRITE_EXTERNAL_STORAGE'] === 'granted') {
		      granted = true;
		    }
			    
	  	} catch (err) {
	    	console.warn(err)
	    }
		

	    if (!granted) {
			RNFetchBlob
			  .config({
			    addAndroidDownloads : {
		            useDownloadManager : true,
		            title : 'Kliknklin Partner.apk',
		            description : 'An APK that will be installed',
						mime : 'application/vnd.android.package-archive',
						mediaScannable : true,
						notification : true,
		        }
			  })
			  .fetch('GET', this.state.url.update)
			  .then((res) => {
			  	android.actionViewIntent(res.path(), 'application/vnd.android.package-archive')
			  })
			  .catch((errorMessage) => {
			  		alert('download dibatalkan atau check koneksi anda untuk melanjutkan');
					this._configCheckUpdate();
			  })
		}
		else{
			Linking.canOpenURL(this.state.url.update).then(supported => {
			  if (!supported) {
			  	alert('percobaan mendownload update gagal, \n download secara manual pada ' + this.state.url.update);
			  } else {
			    return Linking.openURL(this.state.url.update);
			  }
			}).catch(err => console.error('An error occurred', err));

			this.checkupdate_interval = TimerMixin.setInterval(() => {
				this._configCheckUpdate(this.state.data.loginState);
			}, 10000);
		}
	}

	_configCheckUpdate(loginState){
		if (this.checkupdate_interval) {
			TimerMixin.clearInterval(this.checkupdate_interval);
		}

		var load = this.state.load;
		
		this._fetch(this.state.timeout.update, General._checkUpdate())
			.then((response) => {
				if (response.success == 'latest') {
					load.isConfigSuccess.update = true;
					this.setState({load});
				}
				else if (response.success == 'newversionavailable') {
					Alert.alert(
					  'Versi baru telah tersedia',
					  'Ingin mengupdatenya sekarang?',
					  [
					    {text: 'Update', 
					    	onPress: () => {
					    		this._downloadUpdate();
					    	}
						},
					    {text: 'Lain kali', 
					    	onPress: () => {
					    		load.isConfigSuccess.update = true;
					    		this.setState({load});
					    	}, 
					    	style: 'cancel'
						},
					  ],
					  { cancelable: false }
					)
					
				}
				else if (response.success == 'forceupdate') {
					Alert.alert(
					  'Versi baru telah tersedia',
					  'Anda harus update aplikasi untuk melanjutkan',
					  [
					    {text: 'Update', 
					    	onPress: () => {
					    		this._downloadUpdate();
					    	}
					    },
					  ],
					  { cancelable: false }
					)
				}
			})
			.catch((error) => {
				this._showApiErrorRetry([this._configCheckUpdate.bind(this), this._configPushNotif.bind(this)]);
			});
	}

	_configBackHandler(){
		var self = this;
		BackHandler.addEventListener('hardwareBackPress', function() {
		 	var navigator = self._getNavigator();
		 	if (navigator.getCurrentRoutes().length > 1) {
		 		self._getNavigator().pop();
		 	}
		 	else{
		 		Alert.alert(
                    'Konfirmasi',
                    'Anda yakin untuk keluar dari kliknklin ?',
                    [
                        {text:'tidak'},
                        {text:'ya', onPress: () => {
                            BackHandler.exitApp();
                        } },
                    ]
                );
		 	}
		 	return true;
		});
	}

	_configLoginState(){
		var load = this.state.load;

		SInfo.getItem('loginState', {
			sharedPreferencesName: 'PartnerKliknKlin',
			keychainService: 'PartnerKliknKlin'})
		.then(value => {
			console.log(value);
			    if (value != null) {
			    	this._showModal(true, 'mencoba masuk...', () => {
						var data = this.state.data;
				    	data.loginState = JSON.parse(value);
						
						data.loginState.access_token = Authentication._partnerKnKDecrypt(data.loginState.access_token);
						data.loginState.refresh_token = Authentication._partnerKnKDecrypt(data.loginState.refresh_token);

				    	this._setLoginState(data.loginState, false, () => {});
						
						this._fetch(this.state.timeout.login, Authentication._loginToken(data.loginState))
							.then((responseLogin) => {
								if (responseLogin.success != 'yeah') {
									this._fetch(this.state.timeout.login, Authentication._refreshToken(data.loginState))
										.then((responseRefresh) => {
											if (responseRefresh.success != 'yeah') {
												var modal = this.state.modal;
												modal.isProgressLoadingVisible = false;

												this.setState({modal} , () => {
													alert(this.state.string.error.reloginFailed);
													
													this._setLoginState(null, false, () => {});
										        });
											}
											else{
												this._setLoginState(responseRefresh.data, true, () => {
													load.isConfigFinish.login = true;
													load.isConfigSuccess.login = true;
													this.setState({load});
												});
											}
										})
										.catch((error) => {
											this._showApiErrorRetry([this._configLoginState.bind(this)]);
										});
									
								}
								else{
									load.isConfigFinish.login = true;
									load.isConfigSuccess.login = true;
									this.setState({load});
								}
							})
							.catch((error) => {
								this._showApiErrorRetry([this._configLoginState.bind(this)]);
							});
			    	})
			    }
			    else{
			    	load.isConfigFinish.login = true;
			    	load.isConfigSuccess.login = false;
					this.setState({load});
			    }
			}
		);
	}

	_configUserProfile(loginState){
		var load = this.state.load;

		this._fetch(this.state.timeout.profile, Authentication._fetchProfile(loginState))
			.then(async (response) => {
				console.log(response);
				if (response.success == 'yeah') {
					if (response.data.role == 'partner') {
						await AsyncStorage.setItem('partnerkliknklin_role', 'owner');
					}
					else if (response.data.role == 'staff') {
						await AsyncStorage.setItem('partnerkliknklin_role', 'staff');
					}
					else{
						await AsyncStorage.setItem('partnerkliknklin_role', 'unknown');
					}

					this._setUserState(response.data);

					load.isConfigSuccess.profile = true;
					this.setState({load});
				}
				else{
					if (responseProfile.error == 'unauthorized') {
						this._showErrorConfig('unauthorized');
					}
					else{
						this._showErrorConfig('unknown');
					}

					this._setLoginState(null, true, () => {});
				}
			})
			.catch((error) => {
				this._showApiErrorRetry([this._configUserProfile.bind(this, loginState)]);
			})
	}

	_configOutlet(userState){
		var load = this.state.load;
    	var profile = userState;
		if (profile) {
			var data = this.state.data;

			if (profile.role == 'partner') {
				var result = [];
				var resultID = [];
				for (var index in profile.user.partner.outlet) {
					result.push(profile.user.partner.outlet[index].outlet_name);
					resultID.push(profile.user.partner.outlet[index].id);
				}

				if (data.loginState.outlet == undefined) {
					data.loginState.outlet = {};
					data.loginState.outlet.activeID = resultID[0];

					this._setLoginState(data.loginState, true, () => {});
				}
				else{
					data.outlet.activeID = data.loginState.outlet.activeID; 
				}

				data.outlet.listName = result;
				data.outlet.listID = resultID;
				
				load.isConfigSuccess.outlet = true;

				this.setState({data, load});

			}
			else if (profile.role == 'staff') {
				var result = [];
				var resultID = [];
				
				if (Array.isArray(profile.user.staff.outlet)) {
					for (var index in profile.user.staff.outlet) {
						result.push(profile.user.staff.outlet[index].outlet_name);
						resultID.push(profile.user.staff.outlet[index].id);
					}
				}
				else{
					result.push(profile.user.staff.outlet.outlet_name);
					resultID.push(profile.user.staff.outlet.id);
				}
				

				if (data.loginState.outlet == undefined) {
					data.loginState.outlet = {};
					data.loginState.outlet.activeID = resultID[0];

					this._setLoginState(data.loginState, true, () => {});
				}
				else{
					data.outlet.activeID = resultID[0]; 
					data.loginState.outlet.activeID = resultID[0];

					this._setLoginState(data.loginState, true, () => {});
				}
				
				data.outlet.listName = result;
				data.outlet.listID = resultID;
				
				load.isConfigSuccess.outlet = true;

				this.setState({data, load});
			}
		}
    }

    _configCourier(loginState){
    	var load = this.state.load;

		loginState.laundry_id = loginState.outlet.activeID;
		this._fetch(this.state.timeout.courier, General._getCourierList(loginState))
			.then((response) => {
				console.log(response);
				if (response.success == 'yeah') {
					var data = this.state.data;
					
					var resultListName = [];
					var resultListID = [];

					for(var index in response.data) {
						resultListName.push(response.data[index].ktp_name);
						resultListID.push(response.data[index].id);
					}

					data.courier.listName = resultListName;
					data.courier.listID = resultListID;
					
					load.isConfigSuccess.courier = true;
					this.setState({data, load});
				}
				else{
					this._showErrorConfig('unknown');

					this._setLoginState(null, true, () => {});
				}
			})
			.catch((error) => {
				this._showApiErrorRetry([this._configCourier.bind(this, loginState), this._configSummaryOrder.bind(this, loginState), this._configSummaryIncoming.bind(this, loginState)]);
			});
	}

	_configMaxItemPieces(loginState){
		this._fetch(this.state.timeout.maxItemPieces, General._getMaxItemPieces(loginState))
			.then((response) => {
				if (response.success == 'yeah') {
					var data = this.state.data;
					//data.summary.order = response.data;

					load.isConfigSuccess.maxItemPieces = true;
					this.setState({data, load});
				}
			})
			.catch((error) => {
				this._showApiErrorRetry([this._configCourier.bind(this, loginState), this._configSummaryOrder.bind(this, loginState, true), this._configSummaryIncoming.bind(this, loginState, true)]);
			});
	}

    _configSummaryOrder(loginState, isShowError){
    	var load = this.state.load;

		loginState.laundry_id = loginState.outlet.activeID;
		this._fetch(this.state.timeout.courier, Order._fetchSummaryOrder(loginState))
			.then((response) => {
				if (response.success == 'yeah') {
					var data = this.state.data;
					data.summary.order = response.data;

					load.isConfigSuccess.summary = true;
					this.setState({data, load});
				}
			})
			.catch((error) => {
				if (isShowError) {
					this._showApiErrorRetry([this._configCourier.bind(this, loginState), this._configSummaryOrder.bind(this, loginState, true), this._configSummaryIncoming.bind(this, loginState, true)]);
				}
				else{
					this._configSummaryOrder(loginState, false);
				}
			});
	}

	_configSummaryIncoming(loginState, isShowError){
		var load = this.state.load;

		loginState.laundry_id = loginState.outlet.activeID;

		this._fetch(this.state.timeout.courier, Order._fetchSummaryIncomingOrder(loginState))
			.then((response) => {
				if (response.success == 'yeah') {
					var data = this.state.data;
					data.summaryincoming = response.data;
					load.isConfigSuccess.summaryIncoming = true;

					this.setState({data, load});
				}
				
			})
			.catch((error) => {
				if (isShowError) {
					this._showApiErrorRetry([this._configCourier.bind(this, loginState), this._configSummaryOrder.bind(this, loginState, true), this._configSummaryIncoming.bind(this, loginState, true)]);
				}
				else{
					this._configSummaryIncoming(loginState, false);
				}
			});
	}

	_configPushNotif() {
		this._fetch(5000, this._pushnotif._configPushNotif())
			.then((responseConfigPushNotif) => {
				if (responseConfigPushNotif.success == 'yeah') {
					var user = JSON.parse(JSON.stringify(this.state.data.loginState));
					user.registration_token = responseConfigPushNotif.token;

					this._fetch(5000, PushNotifAPI._saveToken(user))
				      	.then((responseSaveToken) => {
							if (responseSaveToken.success == 'yeah') {
								console.log('yeha');
								this._setRegistrationToken(responseConfigPushNotif.token);
							}
							else{
								throw 'error';
							}
				      	})
				      	.catch((error) => {
				      		console.log(error);
				        	this._showApiErrorRetry([this._configCheckUpdate.bind(this), this._configPushNotif.bind(this)]);
				    	});
				}
				else{
					throw 'error';
				}
			})
			.catch((error) => {
				console.log(error);
				this._showApiErrorRetry([this._configCheckUpdate.bind(this), this._configPushNotif.bind(this)]);
			});
	}

	_setConfigLoginDone(loginState){
		// this._setLoginState(loginState, true, () => {});

		var load = this.state.load;
		load.isConfigFinish.login = true;
		load.isConfigSuccess.login = true;
		//this.setState({load});
	}

	_fetch(ms, promise) {
	  return new Promise(function(resolve, reject) {
	    setTimeout(function() {
	      reject(new Error("timeout"))
	    }, ms)
	    promise.then(resolve, reject)
	  })
	}

	_showApiErrorRetry(callback = []){
		this._showModal(false, this.state.modal.loadingWord, () => {
			TimerMixin.setTimeout(() => {
				Alert.alert(
				  'Terdapat kesalahan!',
				  'Check koneksi internet anda',
				  [
				    {text: 'Coba Lagi', 
				    	onPress: () => {
				    		this._showModal(true, this.state.modal.loadingWord, () => {
				    			for(var index in callback){
									if (callback) callback[index]();
								}
				    			
				    		})
				    	}
					},
				  ],
				  { cancelable: false }
				)
			}, 250)
		})
	}

	_showApiErrorRetryCancel(callback = () => {}){
		this._showModal(false, this.state.modal.loadingWord, () => {
			TimerMixin.setTimeout(() => {
				Alert.alert(
				  'Terdapat kesalahan!',
				  'Check koneksi internet anda',
				  [
					{text: 'Batalkan',
						onPress: () => {

				    	}
					},
					{text: 'Coba Lagi', 
				    	onPress: () => {
				    		this._showModal(true, this.state.modal.loadingWord, () => {
				    			if (callback) callback();
				    		})
				    	}
					},
				  ],
				  { cancelable: false }
				)
			}, 250)
		})
	}

	_resetApp(){
		var load = this.state.load;
		load.isConfigFinish.login = false;
		load.isConfigFinish.update = false;
		load.isConfigFinish.profile = false;
		load.isConfigFinish.outlet = false;
		load.isConfigFinish.courier = false;
		load.isConfigFinish.summary = false;
		load.isConfigFinish.summaryIncoming = false;
		load.isConfigFinish.app = false;

		load.isConfigSuccess.login = false;
		load.isConfigSuccess.update = false;
		load.isConfigSuccess.profile = false;
		load.isConfigSuccess.outlet = false;
		load.isConfigSuccess.courier = false;
		load.isConfigSuccess.summary = false;
		load.isConfigSuccess.summaryIncoming = false;
		load.isConfigFinish.app = false;

		var data = this.state.data;

		data.loginState = {};
		data.userState = {};
		data.outlet = {
					listName: ['Sedang memuat...'],
					listID: [],
					activeID : 0,
				};
		data.courier = {
					listName : [''],
					listID: [],
				};
		data.summary = {};
		data.summaryincoming = 0;

		if (this.reload_summary) {
			TimerMixin.clearInterval(this.reload_summary);
			this.reload_summary = null;
		}
		if (this.reload_summary_incoming) {
			TimerMixin.clearInterval(this.reload_summary_incoming);
			this.reload_summary_incoming = null;
		}

		this.setState({load,data});
	}

	_setRegistrationToken(registration_token, callback = () => {}){
		var loginState = this.state.data.loginState;
		loginState.registration_token = registration_token;

		this._setLoginState(loginState, true, () => {
			var load = this.state.load;
			load.isConfigSuccess.pushNotif = true;
			this.setState({load});
		});
	}
	
	_setShouldReloadSummary(isReload){
		if (this.state.load.isConfigFinish.app) {
			if (Object.keys(this.state.data.loginState).length != 0) {
				if (this.state.data.loginState.outlet != undefined) {
					this._configSummaryOrder(this.state.data.loginState, false);
				}
			}
		}
	}

	_setShouldReloadSummaryIncoming(isReload){
		if (this.state.load.isConfigFinish.app) {
			if (Object.keys(this.state.data.loginState).length != 0) {
				if (this.state.data.loginState.outlet != undefined) {
					this._configSummaryIncoming(this.state.data.loginState, false);
				}
			}
		}
	}

	_showErrorConfig(errorName){
		if (errorName == 'unauthorized') {
			alert(this.state.string.error.profileUnauthorized);
		}
		else{
			alert(this.state.string.error.unknown);
		}
	}

	_showModal(isShow, modalText, callback = () => {} ){
		var modal = this.state.modal;
		modal.isProgressLoadingVisible = isShow;
		modal.loadingWord = modalText;
		
		this.setState({modal}, () => {
			callback();
		});
	}

	_setNavBarHeaderVisible(value){
		this.setState({isNavBarHeaderVisible:value});
	}

	_setActiveOutlet(activeID){
		var data = this.state.data;
		data.outlet.activeID = activeID;
		data.loginState.outlet.activeID = activeID;

		var modal = this.state.modal;
		modal.isProgressLoadingVisible = true;
		modal.loadingWord = 'Memuat data...';

		this._setLoginState(data.loginState, true, () => {});

		var load = this.state.load;
		load.isConfigFinish.app = false;

		load.isConfigFinish.courier = false;
		load.isConfigFinish.summary = false;
		load.isConfigFinish.summaryIncoming = false;

		load.isConfigSuccess.courier = false;
		load.isConfigSuccess.summary = false;
		load.isConfigSuccess.summaryIncoming = false;

		this.setState({data, modal, load});
	}

	_setLoginState(loginState, isSaveToLocalDB = false, callback = () => {}){
		if (loginState != null && Object.keys(loginState).length != 0) {
			var data = this.state.data;
			data.loginState = loginState;

			this.setState({data}, () => {
				callback();
			});

			if (isSaveToLocalDB) {
				var saveLoginState = JSON.parse(JSON.stringify(loginState));
				saveLoginState.access_token = Authentication._partnerKnKEncrypt(saveLoginState.access_token);
				saveLoginState.refresh_token = Authentication._partnerKnKEncrypt(saveLoginState.refresh_token);

				SInfo.setItem('loginState', JSON.stringify(saveLoginState), {
					sharedPreferencesName: 'PartnerKliknKlin',
					keychainService: 'PartnerKliknKlin'
				});
			}
		}
		else{
			this._resetApp();

			SInfo.deleteItem('loginState', {
	            sharedPreferencesName: 'PartnerKliknKlin',
	            keychainService: 'PartnerKliknKlin'
	        });
			
	        callback();

	        //this._getNavigator().resetTo({id:'SignIn',title:'Sign In'});
		}
	}

	_setUserState(userState, callback = () => {}){
		var data = this.state.data;
		data.userState = userState;

		this.setState({data}, () => {
			callback();
		});
	}

	_getDataUserState(){
		if (Object.keys(this.state.data.userState).length == 0) {
			return null;
		}
		return this.state.data.userState;
	}

	_getDataLoginState(){
		if (Object.keys(this.state.data.loginState).length == 0) {
			return null;
		}
		return this.state.data.loginState;
	}

	_getDataOutlet(){
		if (Object.keys(this.state.data.outlet).length == 0) {
			return null;
		}
		return this.state.data.outlet;
	}

	_getDataSummary(){
		if (Object.keys(this.state.data.summary).length == 0) {
			return null;
		}
		return this.state.data.summary;
	}

	_getDataSummaryIncoming(){
		return this.state.data.summaryincoming;
	}

	_getDataCourier(){
		if (Object.keys(this.state.data.courier).length == 0) {
			return null;
		}
		return this.state.data.courier;
	}

	_getDrawer(){
		return this._drawer;
	}

	_getNavigator(){
		if (this._navigatorknk) {
			return this._navigatorknk._getNavigator();
		}
		return null;
	}

	render(){
		return (
			<View style={{flex:1, backgroundColor:'white'}}/>
		)
	}

			/* 
            
        		<Modal
					isVisible={this.state.modal.isProgressLoadingVisible}
					loadingWord={this.state.modal.loadingWord}
					type={this.state.modal.typeProgressModal}
				/>

				<PushNotif
					ref={(ref) => this._pushnotif = ref}
					navigator={this._getNavigator.bind(this)}
					_setShouldReloadSummary={this._setShouldReloadSummary.bind(this)}
					_setShouldReloadSummaryIncoming={this._setShouldReloadSummaryIncoming.bind(this)}
					_showApiErrorRetryCancel={this._showApiErrorRetryCancel.bind(this)}
					_fetch={this._fetch.bind(this)}
				/>
        	*/
}

// class NavigatorKnK extends Component {
// 	constructor(props) {
//         super(props);
//         this.state = {
//         	initialRoute:{id:''},
//         	isNavBarHeaderVisible:this.props.isNavBarHeaderVisible ? this.props.isNavBarHeaderVisible : false,
//         };
//     }

//     componentDidUpdate(){    }

//     _getNavigator(){
//     	return this._navigator;
//     }

//     static NavigationBarRouteMapper = props => ({
// 			LeftButton(route, navigator, index, navState) {
// 				if (route.id == 'Dashboard' || route.id == 'Setting' || route.id == 'Help') {
// 				    return (
// 				    	<View style={{
// 				    		height:Dimensions.get('window').height * 0.085,
// 				    		marginLeft:10,
// 					        alignItems:'center',
// 					        justifyContent:'center',
// 					    	}}
// 					    >
// 					        <TouchableOpacity
// 					            onPress={() => 
// 					            	{
// 					            		props._drawer().open()
// 					            	}
// 					        	}
// 					        >
// 					            <Icon name='menu' size={35} color={'white'} />
// 				        	</TouchableOpacity>
// 				        </View>
// 				    );
// 				}
// 				else{
// 				    return (
// 				    	<View style={{
// 				    		height:Dimensions.get('window').height * 0.085,
// 				    		marginLeft:10,
// 					        alignItems:'center',
// 					        justifyContent:'center',
// 					    	}}
// 					    >
// 					        <TouchableOpacity
// 					            onPress={() => {
// 					            	navigator.pop();
// 					            	Keyboard.dismiss();
// 					            }}
// 					        >
// 					            <Icon name='chevron-left' size={35} color={'white'} />
// 					        </TouchableOpacity>
// 				        </View>
				        
// 				    );
// 				}
// 			},

// 			RightButton(route, navigator, index, navState) {
// 				return null;
// 			},

// 			Title(route, navigator, index, navState) {
// 				return (
// 				    <View style={{
// 				    	flex:1,
// 				    	top:Dimensions.get('window').height * 0.005,
// 				        alignItems:'center',
// 				        justifyContent:'center',
// 				        alignSelf:'center',marginRight: Navigator.NavigationBar.Styles.Stages.Left.Title.marginLeft || 0}}>
// 				            <TextWrapper type='title' style={{color:'white', fontWeight:'bold'}}>
// 				                {route.title}
// 				            </TextWrapper>
// 				    </View>
// 				);
// 			},
// 	});

// 	render(){
// 		console.log('asd');
// 		return(
// 			<Navigator
// 				ref={(ref) => this._navigator = ref}
//                 configureScene={(route) => Navigator.SceneConfigs.FloatFromLeft}
//                 initialRoute={this.state.initialRoute}
//                 renderScene={(route, navigator) =>this._renderScene(route, navigator)}
//                 onDidFocus={(route) => {
// 					if (route.id == 'SignIn' || route.id == '') {
// 						this.setState({isNavBarHeaderVisible:false});
// 					}
// 					else{
// 						this.setState({isNavBarHeaderVisible:true});
// 					}
//                 }}
//                 navigationBar={this._renderNavBar()}
// 			/>
// 		);
// 	}

// 	_renderNavBar(){
// 		if (this.state.isNavBarHeaderVisible) {
// 			return(
// 				<Navigator.NavigationBar
// 					style={{
// 						backgroundColor: '#61CDFF',
// 				        justifyContent:'center',
// 				        alignItems:'center',
// 				        height:Dimensions.get('window').height * 0.085,
// 					}}
// 					routeMapper={NavigatorKnK.NavigationBarRouteMapper(this.props)}
// 				/>
// 			);
// 		}
// 		return null;
// 	}

// 	_renderLayout(scene, isUseLayout = true){
// 		if (isUseLayout) {
// 			return (
// 				<View style={{flex:1, marginTop:Dimensions.get('window').height *  0.085, left:2.5}}>
// 	   	        	{scene}

// 					<ConnectionError/>
// 				</View>
// 			);
// 		}
// 		return (
// 			<View style={{flex:1}}>
// 	   	        {scene}
// 			</View>
// 		);
// 	}

// 	_renderScene(route, navigator) {
//    	    switch (route.id) {
//    	        case 'SignIn':
//    	        	return this._renderLayout(
//    	        			<SignIn 
//    	        				navigator={navigator}

// 							_setLoginState={this.props._setLoginState} 
// 							_setUserState={this.props._setUserState}
// 							_setConfigLoginDone={this.props._setConfigLoginDone}

// 							_showErrorConfig={this.props._showErrorConfig}
// 							_showModal={this.props._showModal}
// 							_configUserProfile={this.props._configUserProfile}

// 							_fetch={this.props._fetch}
// 							_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
//    	        			/>
//    	        			, false
//    	        	 );
//    	        case 'Dashboard':
//    	        	return this._renderLayout(
// 						<Dashboard 
// 							navigator={navigator}

// 							_getDataLoginState={this.props._getDataLoginState}
// 							_getDataUserState={this.props._getDataUserState}
// 							_getDataOutlet={this.props._getDataOutlet}
// 							_getDataSummary={this.props._getDataSummary}
// 							_getDataSummaryIncoming={this.props._getDataSummaryIncoming}

// 							_setActiveOutlet={this.props._setActiveOutlet} 
// 							_setUserState={this.props._setUserState} 
// 							_setShouldReloadSummary={this.props._setShouldReloadSummary} 
// 							_setShouldReloadSummaryIncoming={this.props._setShouldReloadSummaryIncoming}
// 	        			 /> 
//    	        		);
//    	        case 'SearchResult':
//    	         	return this._renderLayout(
//    	         			<SearchResult 
//  						   	navigator={navigator} 
//  						   	search={route.search} 

//  						   	_getDataLoginState={this.props._getDataLoginState}
						
// 					   		_setShouldReloadSummary={this.props._setShouldReloadSummary}
//  						   	_showModal={this.props._showModal}	

//  						   	_fetch={this.props._fetch}
// 							_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}	   
//    	         			/> 
//    	         	);
//    	        case 'OrderList':
//    	         	return this._renderLayout( 
//    	         			<OrderList 
//      						navigator={navigator}
//      						contentType={route.contentType}

//      						_getDataLoginState={this.props._getDataLoginState}
//      						_getDataCourier={this.props._getDataCourier}

//      						_setShouldReloadSummary={this.props._setShouldReloadSummary}
//      						_setShouldReloadSummaryIncoming={this.props._setShouldReloadSummaryIncoming}
//      						_showModal={this.props._showModal}

// 							_fetch={this.props._fetch}
//      						_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
//      						listType={route.listType}
//    	         			  />
//    	         	);
//    	        case 'OrderDetail':
//    	         	return this._renderLayout(
//    	         			<OrderDetail 
//    	         				navigator={navigator} 
//    	         				contentType={route.contentType} 
//    	         				data={route.data}
//    	         				_afterSubmit={route._afterSubmit}
   	         				
//    	         				_getDataLoginState={this.props._getDataLoginState}
//    	         				_getIsBankTransferPaid={route._getIsBankTransferPaid}

//    	         				_setShouldReloadSummary={this.props._setShouldReloadSummary} 
//    	         				_showModal={this.props._showModal}

//    	         				_fetch={this.props._fetch}
// 							_showApiErrorRetryCancel={this.props._showApiErrorRetryCancel}
//    	         			/>
//    	         	);
//    	        case 'Help':
//    	         	return this._renderLayout(
//    	         			<Help 
//    	         				navigator={navigator}
//    	         			/> 
//    	         	);
//    	        case 'Setting':
//    	         	return this._renderLayout(
// 	   	         		<Setting 
// 	   	         			_getDataOutlet={this.props._getDataOutlet}
// 	   	         			_getDataUserState={this.props._getDataUserState}
// 	   	         	 	/> 
//    	         	);
//    	        case 'FAQ':
//    	         	return this._renderLayout( <HelpContent.FAQ /> );
//    	        case 'AboutApp':
//    	         	return this._renderLayout( <HelpContent.AboutApp /> );
//    	        case 'WorkFlow':
//    	         	return this._renderLayout( <HelpContent.WorkFlow /> );
//         }
//     }
// }