import { Dimensions, DeviceEventEmitter } from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';

import * as actions from 'app/data/actions';
import * as CONSTANT from 'app/data/constant';
import * as types from 'app/data/actions/actionTypes';
import * as API from './../../api';
import {DateFormat, toCurrency} from 'app/data/components';
import {BluetoothManager,BluetoothEscposPrinter} from 'app/data/components/react-native-bluetooth-escpos-printer';

export function cetak(orderData, isOldVersion = false, isMembership = false){
  
  return async function (dispatch, getState){
    let isBluetoothEnabled = await _isBluetoothEnabled();

    if(isBluetoothEnabled){
      
      console.log("scanDevicesNew");
      await BluetoothManager.scanDevicesNew()
      .then(async (bluetoothDevice)=> {
          console.log("connected",bluetoothDevice);
          if(bluetoothDevice){
            dispatch(_print(orderData, isOldVersion, isMembership, bluetoothDevice));
          }

      }, (er)=> {
          console.log("er",er);
          alert('Sambungan bluetooth terputus, Silahkan sambungkan kembali');
      });

    }else{
      await BluetoothManager.enableBluetooth().then( async (r)=>{
        var paired = [];
        if(r && r.length>0){
            for(var i=0;i<r.length;i++){
                try{
                    paired.push(JSON.parse(r[i])); // NEED TO PARSE THE DEVICE INFORMATION
                }catch(e){
                    console.log(JSON.stringify(e))
                }
            }
        }else{
          await BluetoothManager.scanDevicesNew()
          .then(async (bluetoothDevice)=> {
              if(bluetoothDevice){
                dispatch(_print(orderData, isOldVersion, isMembership, bluetoothDevice));
              }

          }, (er)=> {
              console.log("er",er);
              alert('Sambungan bluetooth terputus, Silahkan sambungkan kembali');
          });
        }

        // return paired;
      },(err)=>{
          console.log(JSON.stringify(err))
      });
    }
  }
}

export function sendEmail(orderData){
  return async function (dispatch, getState){

    dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Mengirim Nota..'}));

    let accessToken = await getState().rootReducer.user.data.accessToken;

    let {id, orderitem, pricedetail, customer} = orderData;
    let {user} = customer;
    let subtotalPrice = await _getSubtotalPrice(orderitem);
    let totalPrice = await _getTotalPrice(pricedetail);
    let discount = await _getDiscount(pricedetail);

    let params = {
      order_id: id,
      subtotalprice: subtotalPrice,
      expresscharge: 0,
      discount: discount,
      totalprice: totalPrice
    }

    try{
      if(user.email){
        await API.Order._sendEmail(params, accessToken)
        .then((response) => {

          dispatch(actions.navigator.dismissModal());
          let {status, error, data} = response;
            
          if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
            dispatch(actions.root.showMessage(`Nota telah terkirim ke akun email ${user.email}`, `Nota terkirim`));
          }
          else{
            throw new Error('API ERROR : ' +  status.code);
          }
        });
      }else{
        dispatch(actions.navigator.dismissModal());
        dispatch(actions.root.showMessage(`Konsumen belum mendaftarkan email`, `Nota tidak terkirim`));
      }
    }catch(err){
      console.log(err);
    }
  }
}

async function _isBluetoothEnabled(){

    let result = false;

    await BluetoothManager.isBluetoothEnabled().then((status)=> {
        result = status;
    }, (err)=> {
        alert(err)
    });

    return result;
}

async function scanDevice(){
    let pairedDs = [];
    let foundDs = [];
    let bluetoothDevices = [];
    
    await BluetoothManager.scanDevices()
    .then((s)=> {

        var ss = JSON.parse(s);//JSON string
        console.log(ss);
        pairedDs = ss.paired || []; 
        foundDs = ss.found || [];

        // bluetoothDevices = pairedDs.concat(foundDs)
        bluetoothDevices = foundDs
    }, (er)=> {

        alert('error' + JSON.stringify(er));
    });

    return bluetoothDevices;
}

export function _formatDataForLightBox(bluetoothDevices){
    let result = [];

    bluetoothDevices.map((data, index) => {
        let oBj = {
            id: data.address,
            info: data.name
        }

        result.push(oBj);
    })

    return result;
}

function _print(orderData, isOldVersion, isMembership, bluetoothDevice){
  console.log("isMembership",isMembership);
  return async function (dispatch, getState){

    let prevPrintedData = await getState().rootReducer.bluetoothDevice.prevPrintedData;
    let counter = await getState().rootReducer.bluetoothDevice.counter;

    if(prevPrintedData !== null && orderData.order_hash == prevPrintedData.order_hash){
      dispatch(setCounter(counter++));
    }else{
      counter = 1;
      dispatch(setCounter(1));
    }

    dispatch(setPrevPrintedData(orderData));

    if(isOldVersion){
      dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Mencetak Nota..'}));
      
      _printTextOldVersion(orderData);
      
      setTimeout(() => {
        dispatch(actions.navigator.dismissModal());
      },3000)
    }else if(isMembership){
      dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Mencetak Nota..'}));
      console.log("cetak membershipPackage");
      _printMembershipReceipt(orderData, counter, bluetoothDevice);

      setTimeout(() => {
        dispatch(actions.navigator.dismissModal());
      },3000)

    }else{
      dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Mencetak Nota..'}));
      
      _printNota(orderData, counter, bluetoothDevice);

      setTimeout(() => {
        dispatch(actions.navigator.dismissModal());
      },3000)
    }
  }
}

async function _printNota(orderData, counter, bluetoothDevice){

  let right_column_section_1 = (bluetoothDevice == "Printer001" ? 17 : 27);
  let right_column_section_2 = (bluetoothDevice == "Printer001" ? 19 : 29);
  
  let left_column_section_3 = (bluetoothDevice == "Printer001" ? 6 : 8);
  let center_column_section_3 = (bluetoothDevice == "Printer001" ? 19 : 24);
  let right_column_section_3 = (bluetoothDevice == "Printer001" ? 6 : 9);

  let left_column_section_4 = (bluetoothDevice == "Printer001" ? 4 : 12);
  let centerL_column_section_4 = (bluetoothDevice == "Printer001" ? 18 : 18);
  let centerR_column_section_4 = (bluetoothDevice == "Printer001" ? 2 : 2);
  let right_column_section_4 = (bluetoothDevice == "Printer001" ? 7 : 9);

  let right_column_section_5 = (bluetoothDevice == "Printer001" ? 9 : 19);

  let {outlet, order_hash, customer, orderitem, appliedpromo, pricedetail, payment, orderlaundry} = orderData;
  let orderstatuslaundry = {orderlaundry}
  let status_laundry = await _getLaundryStatus(orderstatuslaundry, order_hash);
  let {user} = customer;
  let cashier = await _getCashier(orderData);
  let deliveryDate = await _getDeliveryDate(orderData);
  let timeservice = await _getTimeServices(orderData);
  let paymentmethod = await _getPaymentMethod(orderData);
  let paymentstatus = await _getPaymentStatus(payment);
  let isDiscount = await _getIsDiscount(appliedpromo);
  let subtotalPrice = await _getSubtotalPrice(orderitem);
  let shippingFee = await _getShippingFee(pricedetail);
  let totalPrice = await _getTotalPrice(pricedetail);
  let discount = await _getDiscount(pricedetail);
  let currDate = await _getCurrDate();
  let promoName = await _getPromoName(appliedpromo, isDiscount);

  await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
  await BluetoothEscposPrinter.setBlob(1);

  let base64PicString = await _getBase64String();
  let printPicOptions = {
      width: 120,
      left: 135
  }
  await BluetoothEscposPrinter.printPic(base64PicString, printPicOptions);
  
  await BluetoothEscposPrinter.printerAlign( BluetoothEscposPrinter.ALIGN.CENTER);
  await  BluetoothEscposPrinter.printText(outlet.name+"\n\r",{
      encoding:'GBK',
      codepage:0,
      widthtimes:0,
      heigthtimes:0,
      fonttype:4
  });

  await  BluetoothEscposPrinter.printText(outlet.address_location+"\n\r",{
      encoding:'GBK',
      codepage:0,
      widthtimes:0,
      heigthtimes:0,
      fonttype:1
  });
  await  BluetoothEscposPrinter.printText("Telp. "+outlet.phone+"\n\r",{
      encoding:'GBK',
      codepage:0,
      widthtimes:0,
      heigthtimes:0,
      fonttype:1
  });

  _space();

  await BluetoothEscposPrinter.printerAlign( BluetoothEscposPrinter.ALIGN.LEFT);

  await  BluetoothEscposPrinter.printText("Id Pesanan: "+order_hash+"\n\r",{
      encoding:'GBK',
      codepage:0,
      widthtimes:0,
      heigthtimes:0,
      fonttype:1
  });

  if(status_laundry){
    await  BluetoothEscposPrinter.printText(status_laundry+"\n\r",{
      encoding:'GBK',
      codepage:0,
      widthtimes:0,
      heigthtimes:0,
      fonttype:1
    });
  }

  _space();

  await BluetoothEscposPrinter.printerAlign( BluetoothEscposPrinter.ALIGN.LEFT);

  await BluetoothEscposPrinter.printColumn([14,right_column_section_1],
      [BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.LEFT],
      ['Cust        : ',user.name+" - "+user.mobile_phone],{
          encoding:'GBK',
          codepage:0,
          widthtimes:0,
          heigthtimes:0,
          fonttype:1
      });

  await BluetoothEscposPrinter.printColumn([14,right_column_section_1],
      [BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.LEFT],
      ['Kasir       : ',cashier],{
          encoding:'GBK',
          codepage:0,
          widthtimes:0,
          heigthtimes:0,
          fonttype:1
      });

  await BluetoothEscposPrinter.printColumn([14,right_column_section_1],
      [BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.LEFT],
      ["Tgl Selesai : ",deliveryDate],{
          encoding:'GBK',
          codepage:0,
          widthtimes:0,
          heigthtimes:0,
          fonttype:1
      });

  _space();

  await BluetoothEscposPrinter.printColumn([12,right_column_section_2],
      [BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.RIGHT],
      [timeservice, paymentmethod+'-'+paymentstatus],{
          encoding:'GBK',
          codepage:0,
          widthtimes:0,
          heigthtimes:0,
          fonttype:1
      });

  if(bluetoothDevice == "Printer001"){
    await  BluetoothEscposPrinter.printText("--------------------------------\n\r",{});
  }else{
    await  BluetoothEscposPrinter.printText("----------------------\n\r",{});
  }

  orderitem.map( async (item_order) => {

   let {quantity, notes, outlet, item_price} = item_order;
   let {outletitemwashingtime} = outlet;
   let {itemwashingtime, price} = outletitemwashingtime;
   let {item, washingservice} = itemwashingtime;
   let {name, unit_measurement} = item;
   let subTotalItemPrice = quantity * item_price;

    BluetoothEscposPrinter.printColumn([left_column_section_3, center_column_section_3, right_column_section_3],
    [BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.RIGHT],
    [quantity.toString()+' '+unit_measurement, name, subTotalItemPrice.toString()],{
      encoding:'GBK',
      codepage:0,
      widthtimes:0,
      heigthtimes:0,
      fonttype:1
    });

    BluetoothEscposPrinter.printColumn([left_column_section_3, center_column_section_3, right_column_section_3],
    [BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.RIGHT],
    ['',washingservice.info, ''],{
        encoding:'GBK',
        codepage:0,
        widthtimes:0,
        heigthtimes:0,
        fonttype:1
    });

    BluetoothEscposPrinter.printColumn([left_column_section_3, center_column_section_3, right_column_section_3],
    [BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.RIGHT],
    ['',notes, ''],{
        encoding:'GBK',
        codepage:0,
        widthtimes:0,
        heigthtimes:0,
        fonttype:1
    });
  })

    _space();

  if(isDiscount){
    BluetoothEscposPrinter.printColumn([left_column_section_3, center_column_section_3, right_column_section_3],
        [BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.RIGHT],
        ['',promoName, discount.toString()],{
            encoding:'GBK',
            codepage:0,
            widthtimes:0,
            heigthtimes:0,
            fonttype:1
        });

    _space();
  }

  if(bluetoothDevice == "Printer001"){
    await  BluetoothEscposPrinter.printText("--------------------------------\n\r",{});
  }else{
    await  BluetoothEscposPrinter.printText("----------------------\n\r",{});
  }

    BluetoothEscposPrinter.printColumn([left_column_section_4, centerL_column_section_4, centerR_column_section_4, right_column_section_4],
        [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
        ['','Biaya Pencucian','Rp', subtotalPrice.toString()],{
            encoding:'GBK',
            codepage:0,
            widthtimes:0,
            heigthtimes:0,
            fonttype:1
        });

    BluetoothEscposPrinter.printColumn([left_column_section_4, centerL_column_section_4, centerR_column_section_4, right_column_section_4],
        [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
        ['','Biaya Pengantaran','Rp', shippingFee.toString()],{
            encoding:'GBK',
            codepage:0,
            widthtimes:0,
            heigthtimes:0,
            fonttype:1
        });

    BluetoothEscposPrinter.printColumn([left_column_section_4, centerL_column_section_4, centerR_column_section_4, right_column_section_4],
      [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
      ['','Discount','Rp', discount.toString()],{
          encoding:'GBK',
          codepage:0,
          widthtimes:0,
          heigthtimes:0,
          fonttype:1
      });

    BluetoothEscposPrinter.printColumn([left_column_section_4, centerL_column_section_4, centerR_column_section_4, right_column_section_4],
        [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
        ['','Total','Rp', totalPrice.toString()],{
            encoding:'GBK',
            codepage:0,
            widthtimes:0,
            heigthtimes:0,
            fonttype:1
        });

    if(bluetoothDevice == "Printer001"){
      await  BluetoothEscposPrinter.printText("--------------------------------\n\r",{});
    }else{
      await  BluetoothEscposPrinter.printText("----------------------\n\r",{});
    }

    _space();

    BluetoothEscposPrinter.printColumn([22,right_column_section_5],
        [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
        ['tgl: '+currDate,'print '+counter],{
            encoding:'GBK',
            codepage:0,
            widthtimes:0,
            heigthtimes:0,
            fonttype:1
        });

    _bottomSpace();

}

async function _printTextOldVersion(orderData){
  let {outlet, transaction_hash, customer, user, itemtransaction, end_time, is_express, payment_status_id, express_charge} = orderData;
  
  let cashier = (user !== null ? user.name : '');
  let deliveryDate = await _getDeliveryDate(null, end_time);
  let timeservice = await _getTimeServices(null, is_express);
  let paymentmethod = 'Cash';
  let paymentstatus = await _getPaymentStatus(null, payment_status_id);
  let subtotalPrice = await _getSubtotalPrice(null, itemtransaction);
  let totalPrice = await _getTotalPrice(null, itemtransaction, express_charge);
  let currDate = await _getCurrDate();

  await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
  await BluetoothEscposPrinter.setBlob(1);

  let base64PicString = await _getBase64String();
  let printPicOptions = {
      width: 120,
      left: 135
  }
  await BluetoothEscposPrinter.printPic(base64PicString, printPicOptions);
  
  _space();

  await BluetoothEscposPrinter.printerAlign( BluetoothEscposPrinter.ALIGN.CENTER);
  await  BluetoothEscposPrinter.printText(outlet.name+"\n\r",{
      encoding:'GBK',
      codepage:0,
      widthtimes:0,
      heigthtimes:0,
      fonttype:4
  });

  await  BluetoothEscposPrinter.printText(outlet.address+"\n\r",{
      encoding:'GBK',
      codepage:0,
      widthtimes:0,
      heigthtimes:0,
      fonttype:1
  });
  await  BluetoothEscposPrinter.printText("Telp. "+outlet.phone+"\n\r",{
      encoding:'GBK',
      codepage:0,
      widthtimes:0,
      heigthtimes:0,
      fonttype:1
  });

  _space();

  await BluetoothEscposPrinter.printerAlign( BluetoothEscposPrinter.ALIGN.LEFT);

  await  BluetoothEscposPrinter.printText("Id Pesanan: "+transaction_hash+"\n\r",{
      encoding:'GBK',
      codepage:0,
      widthtimes:0,
      heigthtimes:0,
      fonttype:4
  });

  _space();

  await BluetoothEscposPrinter.printerAlign( BluetoothEscposPrinter.ALIGN.LEFT);

  await BluetoothEscposPrinter.printColumn([12,30],
      [BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.LEFT],
      ['Cust',': '+customer.name+" - "+customer.mobile_phone],{
          encoding:'GBK',
          codepage:0,
          widthtimes:0,
          heigthtimes:0,
          fonttype:1
      });

  await BluetoothEscposPrinter.printColumn([12,30],
      [BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.LEFT],
      ['Kasir',': '+cashier],{
          encoding:'GBK',
          codepage:0,
          widthtimes:0,
          heigthtimes:0,
          fonttype:1
      });

  await BluetoothEscposPrinter.printColumn([12,30],
      [BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.LEFT],
      ["Tgl Selesai", deliveryDate],{
          encoding:'GBK',
          codepage:0,
          widthtimes:0,
          heigthtimes:0,
          fonttype:1
      });

  _space();

  await BluetoothEscposPrinter.printColumn([13,30],
      [BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.RIGHT],
      [timeservice, paymentmethod+' - '+paymentstatus],{
          encoding:'GBK',
          codepage:0,
          widthtimes:0,
          heigthtimes:0,
          fonttype:1
      });

  await  BluetoothEscposPrinter.printText("--------------------------------\n\r",{});

  itemtransaction.map( async (item_order) => {

    let {total_item, notes, itemvariant, sub_total_price} = item_order;
    let {item} = itemvariant;
    let {name, is_kilo} = item

    let unit_measurement = getUnitMeasurement(is_kilo);

    await BluetoothEscposPrinter.printColumn([8,24,10],
          [BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.RIGHT],
          [total_item.toString()+' '+unit_measurement, name, sub_total_price.toString()],{
              encoding:'GBK',
              codepage:0,
              widthtimes:0,
              heigthtimes:0,
              fonttype:1
          });

    await BluetoothEscposPrinter.printColumn([8,24,10],
        [BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.RIGHT],
        ['',notes, ''],{
            encoding:'GBK',
            codepage:0,
            widthtimes:0,
            heigthtimes:0,
            fonttype:1
        });
  })

    _space();

    await  BluetoothEscposPrinter.printText("--------------------------------\n\r",{});

    await BluetoothEscposPrinter.printColumn([16,13,3,10],
        [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
        ['','Sub Total','Rp', subtotalPrice.toString()],{
            encoding:'GBK',
            codepage:0,
            widthtimes:0,
            heigthtimes:0,
            fonttype:1
        });

    await BluetoothEscposPrinter.printColumn([16,13,3,10],
        [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
        ['','Biaya Pengantaran','Rp', '0'],{
            encoding:'GBK',
            codepage:0,
            widthtimes:0,
            heigthtimes:0,
            fonttype:1
        });

    await BluetoothEscposPrinter.printColumn([16,13,3,10],
        [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
        ['','Total','Rp', totalPrice.toString()],{
            encoding:'GBK',
            codepage:0,
            widthtimes:0,
            heigthtimes:0,
            fonttype:1
        });

    await  BluetoothEscposPrinter.printText("--------------------------------\n\r",{});

    _space();

    await BluetoothEscposPrinter.printColumn([22,20],
        [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
        ['tgl: '+currDate,''],{
            encoding:'GBK',
            codepage:0,
            widthtimes:0,
            heigthtimes:0,
            fonttype:1
        });

    _bottomSpace();
}

async function _printMembershipReceipt(orderData, counter, bluetoothDevice){

  let right_column_section_1 = (bluetoothDevice == "Printer001" ? 20 : 30);
  let right_column_section_2 = (bluetoothDevice == "Printer001" ? 19 : 29);
  
  let left_column_section_3 = (bluetoothDevice == "Printer001" ? 6 : 8);
  let center_column_section_3 = (bluetoothDevice == "Printer001" ? 19 : 24);
  let right_column_section_3 = (bluetoothDevice == "Printer001" ? 6 : 9);

  let left_column_section_4 = (bluetoothDevice == "Printer001" ? 4 : 12);
  let centerL_column_section_4 = (bluetoothDevice == "Printer001" ? 18 : 18);
  let centerR_column_section_4 = (bluetoothDevice == "Printer001" ? 2 : 2);
  let right_column_section_4 = (bluetoothDevice == "Printer001" ? 7 : 9);

  let right_column_section_5 = (bluetoothDevice == "Printer001" ? 9 : 19);

  let currDate = await _getCurrDate();
  let {outlet, customer, membershipPackage, transaction_hash} = orderData;

  await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
  await BluetoothEscposPrinter.setBlob(1);

  let base64PicString = await _getBase64String();
  let printPicOptions = {
      width: 120,
      left: 135
  }
  await BluetoothEscposPrinter.printPic(base64PicString, printPicOptions);
  
  _space();

  await BluetoothEscposPrinter.printerAlign( BluetoothEscposPrinter.ALIGN.CENTER);
  await  BluetoothEscposPrinter.printText(outlet.name+"\n\r",{
      encoding:'GBK',
      codepage:0,
      widthtimes:0,
      heigthtimes:0,
      fonttype:4
  });

  await  BluetoothEscposPrinter.printText(outlet.addressName+"\n\r",{
      encoding:'GBK',
      codepage:0,
      widthtimes:0,
      heigthtimes:0,
      fonttype:1
  });

  _space();

  await BluetoothEscposPrinter.printerAlign( BluetoothEscposPrinter.ALIGN.LEFT);

  await  BluetoothEscposPrinter.printText("Id Pesanan: "+transaction_hash+"\n\r",{
      encoding:'GBK',
      codepage:0,
      widthtimes:0,
      heigthtimes:0,
      fonttype:1
  });

  _space();

  await BluetoothEscposPrinter.printerAlign( BluetoothEscposPrinter.ALIGN.LEFT);

  await BluetoothEscposPrinter.printColumn([11,right_column_section_1],
      [BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.LEFT],
      ['Konsumen : ',customer.name+" - "+customer.mobile_phone],{
          encoding:'GBK',
          codepage:0,
          widthtimes:0,
          heigthtimes:0,
          fonttype:1
      });

  _space();

  await  BluetoothEscposPrinter.printText("--------------------------------\n\r",{});

  BluetoothEscposPrinter.printColumn([left_column_section_3, center_column_section_3, right_column_section_3],
  [BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.LEFT,BluetoothEscposPrinter.ALIGN.RIGHT],
  ['1', membershipPackage.package_name, membershipPackage.package_price.toString()],{
    encoding:'GBK',
    codepage:0,
    widthtimes:0,
    heigthtimes:0,
    fonttype:1
  });

  _space();

  BluetoothEscposPrinter.printText("--------------------------------\n\r",{});

  BluetoothEscposPrinter.printColumn([left_column_section_4, centerL_column_section_4, centerR_column_section_4, right_column_section_4],
      [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
      ['','Sub Total','Rp', membershipPackage.package_price.toString()],{
          encoding:'GBK',
          codepage:0,
          widthtimes:0,
          heigthtimes:0,
          fonttype:1
      });

  BluetoothEscposPrinter.printColumn([left_column_section_4, centerL_column_section_4, centerR_column_section_4, right_column_section_4],
      [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
      ['','Discount','Rp', '0'],{
          encoding:'GBK',
          codepage:0,
          widthtimes:0,
          heigthtimes:0,
          fonttype:1
      });

  BluetoothEscposPrinter.printColumn([left_column_section_4, centerL_column_section_4, centerR_column_section_4, right_column_section_4],
      [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
      ['','Total','Rp', membershipPackage.package_price.toString()],{
          encoding:'GBK',
          codepage:0,
          widthtimes:0,
          heigthtimes:0,
          fonttype:1
      });

  BluetoothEscposPrinter.printText("--------------------------------\n\r",{});

  _space();

  BluetoothEscposPrinter.printColumn([22,right_column_section_5],
      [BluetoothEscposPrinter.ALIGN.LEFT, BluetoothEscposPrinter.ALIGN.RIGHT],
      ['tgl: '+currDate,'print '+counter],{
          encoding:'GBK',
          codepage:0,
          widthtimes:0,
          heigthtimes:0,
          fonttype:1
      });

  _bottomSpace();
}

async function _bottomSpace(){
    await  BluetoothEscposPrinter.printText("\n\r",{});
    await  BluetoothEscposPrinter.printText("\n\r",{});
    await  BluetoothEscposPrinter.printText("\n\r",{});
    await  BluetoothEscposPrinter.printText("\n\r",{});
    await  BluetoothEscposPrinter.printText("\n\r",{});
}

async function _space(){
    await  BluetoothEscposPrinter.printText("\n\r",{});
}

async function _getBase64String(){
    let _base64String = "";
    await RNFetchBlob.fetch('GET', 'https://res.cloudinary.com/knk-customer-test/image/upload/v1547526782/knk_login_d2a8q3.png', {

    })
    .then((res) => {
        let status = res.info().status;

        if(status == 200) {
            // the conversion is done in native code
            _base64String = res.base64()

        } else {
            // handle other status codes
        }
    })
    // Something went wrong:
    .catch((errorMessage, statusCode) => {
        alert(errorMessage);
    })

    return _base64String;
}

async function _getCashier(orderData){
  let {orderlaundry} = orderData;
  let {log} = orderlaundry;
  let cashier  = '';

  log.map((log_order) => {
    if(log_order.order_status_laundry_id == CONSTANT.ORDER_STATUS.IN_PROGRESS_LAUNDRY){
      cashier = log_order.user.name;
    }
  })

  return cashier
}

async function _getDeliveryDate(orderData, end_time = null){

  if(end_time){ // untuk pos sistem lama
    let deliveryDate = "";
    let shipment_date = end_time.split(' ')[0];
    let shipment_date_arr = shipment_date.split('-');

    deliveryDate = shipment_date_arr[2]+'/'+shipment_date_arr[1]+'/'+shipment_date_arr[0];

    return deliveryDate;
  }
  else{
    let deliveryDate = "";
    let {shipments} = orderData;

    shipments.map((shipment) => {
      
      if(shipment.shipment_type_id == CONSTANT.SHIPMENT_TYPE.DELIVERY){
      
        let {shipmentschedule} = shipment;
        let {shipment_date} = shipmentschedule; // 2018-08-25
        let shipment_date_arr = shipment_date.split('-');

        deliveryDate = shipment_date_arr[2]+'/'+shipment_date_arr[1]+'/'+shipment_date_arr[0];
      }
    })
    return deliveryDate;
  }
}

function _getTimeServices(orderData, is_express = null){

  if(is_express !== null){ // untuk pos sistem lama

    if(is_express == 1){
      return 'Express'
    }else{
      return 'Reguler-Time'
    }
    
  }else{
    let {orderlaundry} = orderData;
    return orderlaundry.timeservice.info;
  }
}

async function _getPaymentMethod(orderData){
  let paymentMethod = "";
  let {payment} = orderData;
  let {paymentmethod} = payment;

  return paymentmethod.info;
  // return 'Cash';
}

async function _getPaymentStatus(payment, payment_status_id = null){

  if(payment_status_id !== null){ // untuk pos sistem lama
    let paymentStatusId = Number(payment_status_id);
    let paymentstatus = (paymentStatusId == 1 ? 'Belum Bayar' : 'Sudah Bayar')

    return paymentstatus;
    
  }else{
    let status = "";
    console.log(payment);
    let {pay_early_flag, paymentstatus} = payment;
    let {id} = paymentstatus;

    if(id == 2) { // sudah lunas
      if(pay_early_flag == 1){
        status = 'Lunas Diawal'
      }else{
        status = 'Lunas Diakhir'
      }
    }else{
      if(pay_early_flag == 1){
        status = 'Bayar Diawal'
      }else{
        status = 'Bayar Diakhir'
      }
    }

    return status;
  }
}

async function _getIsDiscount(appliedpromo){
  if(appliedpromo.length == 0)
    return false;
  return true;
}

async function _getTotalPrice(pricedetail, itemtransaction, express_charge){

  if(pricedetail == null){ // untuk pos sistem lama
    let subTotalPrice = await _getSubtotalPrice(null, itemtransaction);
    let expressCharge = await getExpressCharge(express_charge);

    return subTotalPrice + expressCharge;
  }else{
    let {full_price} = pricedetail;
    return full_price;
  }
}

async function _getSubtotalPrice(orderitem, itemtransaction = null){

  if(itemtransaction !== null){ // untuk pos sistem lama
    let subTotalPrice = 0;

    itemtransaction.map((item) => {
      subTotalPrice += getTotalItemPrice(item);
    })

    return subTotalPrice;
  }else{
    let subTotalPrice = 0;

    orderitem.map( (item_order) => {
      let {quantity, item_price} = item_order;
      subTotalPrice += quantity * item_price;
    })

    return subTotalPrice;
  }
}

function getExpressCharge(express_charge){
  let expressCharge = 0;

  if(express_charge){
    expressCharge = express_charge;
  }

  return expressCharge;
}

function getTotalItemPrice(item){
  return item.sub_total_price - (item.sub_total_price * item.itemvariant.discount / 100);
}

async function _getShippingFee(pricedetail){
  let {shipment_fare} = pricedetail;
  return shipment_fare;
}

async function _getDiscount(pricedetail){
  let {marketing_cost_kliknklin, marketing_cost_outlet} = pricedetail;
  return marketing_cost_kliknklin + marketing_cost_outlet;
}

async function _getCurrDate(){
  let currDate = new Date();
  let date = DateFormat(currDate, 'shortDate2');
  let time = DateFormat(currDate, 'simpleTime');

  return date+' '+time;
}

async function _getPromoName(appliedpromo, isDiscount){
  if(isDiscount){
    let {promo} = appliedpromo[0];
    let {promo_name} = promo;

    if(promo_name.includes(":")){
      promo_name = promo_name.split(":")[1];
      if(promo_name.charAt(0) == " "){
        return promo_name.substr(1);
      }
    }

    return promo_name;
  }
  else{
    return '';
  }
}

async function getUnitMeasurement(is_kilo){ // untuk pos sistem lama

  if(is_kilo)
    return 'Kg'

  return ''
}

async function _getLaundryStatus(orderstatuslaundry, order_hash){

  if(isPosOrder(order_hash)){
    if(orderstatuslaundry.id == 3){
      return 'Status Pesanan: Proses Cuci'
    }
    else if(orderstatuslaundry.id == 5 || orderstatuslaundry.id == 9){
      return 'Status Pesanan: Selesai'
    }
    else{
      return '' 
    }
  }else{
    return ''
  }
}


function isPosOrder(order_hash){
  return order_hash.includes('POS');
}

function setConnectedDevice(data){

  return {
      type: types.SET_CONNECTED_DEVICE, 
      data,
  };
}

function setPrevPrintedData(data){

  return {
      type: types.SET_PREV_PRINTED_DATA, 
      data,
  };
}

function setCounter(data){
  return {
      type: types.SET_COUNTER, 
      data,
  };
}