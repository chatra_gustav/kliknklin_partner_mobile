import {
  PermissionsAndroid,
  AppState,
  Linking,
  Platform,
  BackHandler
} from 'react-native';


import * as types from './../actionTypes';
import * as STRING from './../../string';
import * as CONSTANT from './../../constant';
import * as API from './../../api';
import * as actions from './../../actions';

import {Navigation, ScreenVisibilityListener} from 'react-native-navigation';


export function appInitialized(navigator) {
  return async function(dispatch, getState) {
  	console.log('--start app-init')
  	dispatch(actions.pushNotif.configPushNotif(navigator))
  	.then((response) =>{
  		let pushNotifToken = getState().rootReducer.pushNotif.token;
  		if (pushNotifToken) {
			dispatch(actions.authentication.checkLoginState());
			dispatch(getWebLinks())
			dispatch(getMdTime())
	  		dispatch(setupScreenVisibilityListener());
  		}
  	})
  	.catch((error) => {
  		console.log(error);
  		console.log('disana')
  	});
  };
}

export function setupScreenVisibilityListener(){
	return async function(dispatch, getState) {
		new ScreenVisibilityListener({
	  		willDisappear: ({screen, startTime, endTime, commandType}) => {
				//dispatch(actions.layout.setPressBarrierVisibility(true));
				//console.log('willdisapper',screen);
		    },
		    didAppear: async ({screen, startTime, endTime, commandType}) => {
			//set screen state
			dispatch(actions.layout.setScreenState(CONSTANT.SCREEN_STATE.DID_APPEAR));

	      	//avoid user cant press twice of same page
	        dispatch(actions.layout.setPressBarrierVisibility(false));

	        if (screen == CONSTANT.ROUTE_TYPE.POS_ITEM_SELECTION) {
				dispatch(actions.pos.changeDurationToRegular());
			}

	        if (screen == 'PartnerKliknKlin.Dashboard' ||
	        	screen == 'PartnerKliknKlin.SignIn' ||
	        	screen == 'PartnerKliknKlin.ShipmentSchedule') {
	        	setTimeout(() => {
	        		dispatch(checkUpdate());
	        	}, 500);
	        }
			
	        if (screen == 'PartnerKliknKlin.Dashboard' ||
	        	screen == 'PartnerKliknKlin.ShipmentSchedule') {
	        	let {isInitialLoadDataDone} = getState().rootReducer.root;
	        	let {role} = getState().rootReducer.user;

	        	if (!isInitialLoadDataDone) {
	        		dispatch(actions.authentication.loadInitialData(role));
	        	}
	        }

	        //console.log('didappear');
	      },
	      willCommitPreview: () =>{
			//console.log('wcp')
	      },
	      willAppear: ({screen, startTime, endTime, commandType}) =>{
	      },
	      didDisappear:()=>{
	      	dispatch(actions.layout.setScreenState(CONSTANT.SCREEN_STATE.DID_DISAPPEAR));
	      }
	    }).register();
	}
}

export function isScreenDidAppear(){
	return async function(dispatch, getState) {
		return new Promise((resolve, reject) => {
			let interval = setInterval(() => {
				let {screenState} = getState().rootReducer.layout;
				if (screenState == CONSTANT.SCREEN_STATE.DID_APPEAR) {
					resolve(true);
					clearInterval(interval);
				}
			},500);
		});
	}
}

export function searchObjectInList(data, categoryToFind, subCategoryToFind = null, dataToFind){
	return async function(dispatch, getState){
		for(let index in data){
			if (subCategoryToFind) {
				console.log(data[index][categoryToFind], data[index][categoryToFind][subCategoryToFind]);
				if (data[index][categoryToFind][subCategoryToFind] == dataToFind) {
					return data[index];
				}
			}
			else{
				console.log(data[index][categoryToFind]);

				if (data[index][categoryToFind] == dataToFind) {
					return data[index];
				}
			}
			
		}
		
		return [];
	}
}

export function showConnectionError(retryFunction = () => {}, message = 'Periksa Koneksi Internet Anda', title = 'Gangguan Koneksi'){
	return async function(dispatch, getState) {

		await dispatch(isScreenDidAppear())
			.then((response) => {
				if (response == true) {
					setTimeout(() => {
						dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
					    	title,
					    	message,
					    	buttonPrimaryText: 'Coba Lagi',
						    onPressButtonPrimary: () => {
						    	dispatch(actions.navigator.dismissLightBox())
						    	dispatch(retryFunction);
						    },
						    isDismissAllowed:false,
						}));
					}, 200);
				}
				
			})
			.catch((error) => {

			})
	}
}

export function showErrorMessage(message = '', title = 'Terjadi Kesalahan'){

	return async function(dispatch, getState) {
		await dispatch(isScreenDidAppear())
			.then((response) => {
				if (response == true) {
					setTimeout(() => {
						dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
					    	title,
					    	message,
					    	buttonPrimaryText: 'Ya',
						    onPressButtonPrimary: () => {
						    	dispatch(actions.navigator.dismissLightBox());
						    },
						    isDismissAllowed:true,
						}));
					}, 200);
					
				}
				
			})
			.catch((error) => {

			})
	}
}

export function showAlertMessage(message = '', title = '', isDismissAllowed= true, onPressButtonPrimary = () => {}){ // showErrorMessage tanpa fungsi "isScreenDidAppear()"

	return async function(dispatch, getState) {
		setTimeout(() => {
			dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
		    	title,
		    	message,
		    	buttonPrimaryText: 'Ya',
			    onPressButtonPrimary: () => {
			    	onPressButtonPrimary();
			    	
			    	dispatch(actions.navigator.dismissLightBox());
			    },
			    isDismissAllowed:isDismissAllowed,
			}));
		}, 200);
	}
}

export function showMessage(message = '', title = '', isDismissAllowed= true){ // showErrorMessage tanpa fungsi "isScreenDidAppear()"

	return async function(dispatch, getState) {
		setTimeout(() => {
			dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeE',{
		    	title,
		    	message,
		    	buttonPrimaryText: 'Ya',
			    onPressButtonPrimary: () => {
			    	dispatch(actions.navigator.dismissLightBox());
			    },
			    isDismissAllowed:isDismissAllowed,
			}));
		}, 200);
	}
}

export function checkUpdate(){
	return async function(dispatch, getState) {
		return API.Root._checkUpdate()
			.then(async (response) => {
				let {status, error, data} = response;

        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
					let {app} = data;
					if (app == CONSTANT.UPDATE_APP_STATUS.FORCE_UPDATE) {

						dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
					    	title:'Update', 
					    	message: 'Anda harus memperbarui aplikasi anda',
					    	buttonPrimaryText: 'Perbarui',
						    onPressButtonPrimary: () => {
						    	Linking.canOpenURL(CONSTANT.APP_URL.UPDATE_APP_ANDROID).then(supported => {
						            if (supported) {
						                Linking.openURL(CONSTANT.APP_URL.UPDATE_APP_ANDROID);
						            } else {
						                alert('Don\'t know how to open URI: ' + (CONSTANT.APP_URL.UPDATE_APP_ANDROID));
						            }
						            return false;
						        });
						    },
						    isDismissAllowed:false,
						}));
					}
					else if (app == CONSTANT.UPDATE_APP_STATUS.NEW_VERSION_AVAILABLE) {
						dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeA',{
					    	title:'Update', 
					    	message: 'Pembaruan aplikasi telah tersedia',
					    	buttonPrimaryText: 'Perbarui',
					    	buttonSecondaryText :'Lain kali',
						    onPressButtonPrimary: () => {
						    	Linking.canOpenURL(CONSTANT.APP_URL.UPDATE_APP_ANDROID).then(supported => {
						            if (supported) {
						                Linking.openURL(CONSTANT.APP_URL.UPDATE_APP_ANDROID);
						            } else {
						                alert('Don\'t know how to open URI: ' + (CONSTANT.APP_URL.UPDATE_APP_ANDROID));
						            }
						            return false;
						        });
						    },
						    onPressButtonSecondary: () => {
						    	dispatch(actions.navigator.dismissModal());
								dispatch(actions.navigator.dismissLightBox());
						    },
						    isDismissAllowed:false,
						}));
					}
        		}
				else{
					//dispatch(loginRefreshToken(loginData));
				}
	  		})
	  		.catch((error) => {
	  			console.log(error);
				if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED) {
					dispatch(actions.root.showConnectionError(checkUpdate(), 'Gagal memeriksa update, \n periksa internet anda'));
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}
	  		});

	}
}

export function submitFeedback(feedback, navigator){
	return async function(dispatch, getState) {
			dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeA',{
	    	title:'konfirmasi', 
	    	message: 'apakah anda yakin untuk mengirim ini?',
	    	buttonPrimaryText:'ya',
	    	buttonSecondaryText: 'batal', 
		    onPressButtonPrimary: async () => {
				try{
			    	let userData = getState().rootReducer.user.data;
					
					dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));
					
					dispatch(actions.navigator.dismissLightBox());

					dispatch(actions.navigator.pop(navigator));

					API.Root._submitFeedback(userData.accessToken, feedback)
						.then(async response => {
							let {status, error, data} = response;

							if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
								dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
							    	title:'Sukses', 
							    	message: 'Kritik dan saran berhasil dikirim',
							    	buttonPrimaryText: 'Ya', 
								    onPressButtonPrimary: () => {
								    	dispatch(actions.navigator.dismissModal());
										dispatch(actions.navigator.dismissLightBox());
								    },
								}));
			        		}
							else{
								throw new Error('API ERROR : ' + status.code);
							}
						})
						.catch(error => {
							dispatch(actions.navigator.dismissModal());
							console.log(error);
						});
				}
				catch(error){
					console.log(error);
					dispatch(actions.navigator.dismissLightBox());
					setTimeout(() => {
						alert('feedback internal error!');
					}, 
					1000);
				}
				
		    }, 
		    onPressButtonSecondary: () =>{
				dispatch(actions.navigator.dismissLightBox());
		    },
		}));
	}
}

export function getNotification(){
	return async function(dispatch, getState) {
		let {accessToken} = getState().rootReducer.user.data;

		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));
		API.Root._getNotification(accessToken)
		.then((response) => {
			dispatch(actions.navigator.dismissModal());

			let {status, error, data} = response;
    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
				dispatch(setNotificationList(data));
				dispatch(setMessageList(data));
				dispatch(actions.order.getSummaryOrder());

				const messageList = getState().rootReducer.root.messageList;
				
				dispatch(setTotalImportantMessageNotRead(messageList));
    		}
			else{
				throw new Error('API ERROR : ' + status.code);
			}
		})
		.catch((error) => {
			dispatch(actions.navigator.dismissModal());
			console.log(error, 'get notification');
		})
	}
}

export function updateNotification(notification_id){
	return async function(dispatch, getState) {
		let {accessToken} = getState().rootReducer.user.data;
		API.Root._updateNotification(notification_id, accessToken)
			.then((response) => {
				let {status, error, data} = response;
        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
					getNotification();
        		}
				else{
					throw new Error('API ERROR : ' + status.code);
				}
			})
			.catch((error) => {
				console.log(error, 'get notification');
				//alert(error.message);
			})
	}
}

export function getInformationList(){
	return async function(dispatch, getState) {
		let {accessToken} = getState().rootReducer.user.data;
		API.Root._getInformation(accessToken)
			.then((response) => {
				let {status, error, data} = response;
        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
					dispatch(setInformationList(data));
        		}
				else{
					throw new Error('get information api didnt return 200');
				}
			})
			.catch((error) => {
				console.log(error, 'getInformationList');

				if (error == CONSTANT.ERROR_TYPE.TIMEOUT) {
					dispatch(getInformation())
				}
			})
	}
}

export function checkLocationServiceStatus(){
	return async function (dispatch, getState){
		BackgroundGeolocation.checkStatus(async status => {
			const granted = status.authorization == 1 ? true : false;

			if (status.authorization !== BackgroundGeolocation.AUTHORIZED) {
				dispatch(
					showOpenLocationSettingBox(
						'Kliknklin butuh mengakses lokasi anda', 
						'buka pengaturan lokasi sekarang?', 
						() => {
							BackgroundGeolocation.showLocationSettings();
							dispatch(setLocationServicePermission(granted));
						}
					)
				);
			}
			else{
				dispatch(dismissBox());
			}

			dispatch(setLocationServiceStatus(granted));
		});
	}
}

export function getWebLinks(){
	return async function (dispatch, getState){
		
		API.Root._getWebLinks()
		.then((response) => {
			let {status, error, data} = response;
    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
				dispatch(setWebLinks(data));
    		}
			else{
				throw new Error('API ERROR : ' + status.code);
			}
		})
		.catch((error) => {
			console.log(error, 'root.getweblinks');
		})
	}
}

export function getMdTime(){
	return async function (dispatch, getState){

		API.Root._getMdTIme()
		.then((response) => {
			let {status, error, data} = response;
    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
				dispatch(setMdTime(data));
    		}
			else{
				throw new Error('API ERROR : ' + status.code);
			}
		})
		.catch((error) => {
			console.log(error, 'root.getmdtime');
		})
	}
}

export function changeOutletOperationTime(params) {
	return async function (dispatch, getState){

		let {accessToken} = getState().rootReducer.user.data;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;
		let mdTimes = getState().rootReducer.root.mdTimes;

		API.Root.changeOutletOperationTime(outletList[indexActiveOutlet].id, params, accessToken)
		.then((response) => {
			let {status, error, data} = response;

    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {

    			let outlet;

				for(x in outletList){
					if(outletList[x].id == data.id){

						outlet = outletList[x];
						
						outlet.open_time = {
							id: params.open_operasional_time_id,
							time: getTime(mdTimes, params.open_operasional_time_id),
							info: getTimeInfo(mdTimes, params.open_operasional_time_id)
						}

						outlet.close_time = {
							id: params.close_operasional_time_id,
							time: getTime(mdTimes, params.close_operasional_time_id),
							info: getTimeInfo(mdTimes, params.close_operasional_time_id)
						}
					}
				}

				setTimeout(() => {
					
					for(x in outletList){
						if(outletList[x].id == outlet.id){
							outletList[x] = outlet;
						}
					}

					dispatch(actions.user.setUserOutlet(outletList, indexActiveOutlet))
				}, 200);
    		}
			else{
				throw new Error('API ERROR : ' + status.code);
			}
		})
		.catch((error) => {
			console.log(error, 'root.getmdtime');
		})
	}
}

export function changeOutletLocation(params) {
	return async function (dispatch, getState){

		let {accessToken} = getState().rootReducer.user.data;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;

		API.Root.changeOutletLocation(outletList[indexActiveOutlet].id, params, accessToken)
		.then((response) => {
			let {status, error, data} = response;
			console.log('response:',response);

    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
    			let outlet;

				for(x in outletList){
					if(outletList[x].id == data.id){

						outlet = outletList[x];
						outlet.addressName = data.address_location;
						outlet.addressCoordinate = data.geolocation.coordinates;
						outlet.area2 = data.area2;
						outlet.area3 = data.area3;
					}
				}

				setTimeout(() => {
					
					for(x in outletList){
						if(outletList[x].id == outlet.id){
							outletList[x] = outlet;
						}
					}

					dispatch(actions.user.setUserOutlet(outletList, indexActiveOutlet))
				}, 200);
    		}
			else{
				throw new Error('API ERROR : ' + status.code);
			}
		})
		.catch((error) => {
			console.log(error, 'root.getmdtime');
		})
	}
}

export function configBackgroundLocation(){
	return async function (dispatch, getState){
		BackgroundGeolocation.configure({
			desiredAccuracy: 10000,
			stationaryRadius: 25,
			distanceFilter: 25,
			notificationTitle: 'Kliknklin sedang mengambil lokasi anda.',
			notificationText: 'Aktif',
			debug: true,
			startOnBoot: false,
			stopOnTerminate: false,
			locationProvider: BackgroundGeolocation.DISTANCE_FILTER_PROVIDER,
			interval: 10000,
			fastestInterval: 5000,
			activitiesInterval: 10000,
			stopOnStillActivity: false,
			maxLocations:1,
		});

		BackgroundGeolocation.on('location', (location) => {
		// handle your locations here
		// to perform long running operation on iOS
		// you need to create background task
		BackgroundGeolocation.startTask(taskKey => {
		  // execute long running task
		  // eg. ajax post location
		  // IMPORTANT: task has to be ended by endTask
		  BackgroundGeolocation.endTask(taskKey);
		});
		});

		BackgroundGeolocation.on('stationary', (stationaryLocation) => {
		// handle stationary locations here
		//Actions.sendLocation(stationaryLocation);
		});

		BackgroundGeolocation.on('error', (error) => {
		console.log('[ERROR] BackgroundGeolocation error:', error);
		});

		BackgroundGeolocation.on('start', () => {
		console.log('[INFO] BackgroundGeolocation service has been started');
		});

		BackgroundGeolocation.on('stop', () => {
		console.log('[INFO] BackgroundGeolocation service has been stopped');
		});

		BackgroundGeolocation.on('authorization', (status) => {
		});

		BackgroundGeolocation.on('background', () => {
		console.log('[INFO] App is in background');
		});

		BackgroundGeolocation.on('foreground', () => {
			console.log('[INFO] App is in foreground');
		});

		BackgroundGeolocation.stop();
	}
}

//set function
export function setIsInitialLoadDataDone(data){
	return {
	    type: types.IS_INITIAL_LOAD_DATA_DONE, 
	    data,
	};	
}

export function setRoute(route) {
	return {
	    type: types.ROOT_ROUTE, 
	    route,
	};
}

export function setSMSVerificationTimer(timerSecond){
	return {
	    type: types.SMS_VERIFICATION_TIMER, 
	    timerSecond,
	  };
}

export function setSMSVerificationSent(isSent){
	return {
	    type: types.SMS_VERIFICATION_SENT, 
	    isSent,
	  };
}


export function setErrorMessage(message){
	return {
	    type: types.SET_ERROR_MESSAGE, 
	    message,
	};
}

export function setConnectionStatus(connectionStatus){
	return {
	    type: types.CONNECTION_STATUS,
	    connectionStatus,
	};
}

export function setLocationServiceStatus(locationServiceStatus){
	return {
		type: types.LOCATION_SERVICE_STATUS,
		locationServiceStatus,
	}
}

export function setLocationServicePermission(locationServicePermission){
	return {
		type: types.LOCATION_SERVICE_PERMISSION,
		locationServicePermission,
	}
}

export function setLayoutHeight(height) {
  return {
    type: types.LAYOUT_HEIGHT, 
    height,
  };
}

export function setUpdateType(updateType, updateResolveFlag) {
  return {
    type: types.SET_UPDATE_TYPE, 
    updateType,
    updateResolveFlag,
  };
}

export function setOverrideBackPress(isOverride, route, props, routeType = 'reset',) {
	return {
		type: types.OVERRIDE_BACK_PRESS,
		isOverride,
		route,
		routeType,
		props,
	}
}

export function setNotificationList(data) {
	const result = setupNotification(data);
	return {
		type: types.NOTIFICATION_LIST, 
		data:result,
	};
}

export function setMessageList(data){
	const result = setupMessage(data);
	
	return {
	    type: types.MESSAGE_LIST, 
	    data:result,
	};
}

export function setInformationList(data){
	return {
	    type: types.INFORMATION_LIST, 
	    data,
	};
}

export function setTotalImportantMessageNotRead(data){
	const result = countMessageNotRead(data);
	return {
	    type: types.TOTAL_IMPORTANT_MESSAGE_NOT_READ, 
	    data:result,
	};
}

export function setIsLoadScreen(isLoad){
	return {
	    type: types.IS_LOAD_SCREEN, 
	    isLoad,
	};
}

export function setWebLinks(data){
	return {
	    type: types.SET_WEB_LINKS, 
	    data,
	};
}

export function setMdTime(data) {
	return {
		type: types.SET_MD_TIME,
		data,
	}
}

export const ORDER = 1;
export const ACTION = 5;
export const MESSAGE = 3;
export const MARKETING = 2;
export const ANNOUNCEMENT = 4;

function setupMessage(data){
	let result = [];

	for(let i in data){
		let {notification_type_id} = data[i];
		if (notification_type_id == CONSTANT.NOTIFICATION_TYPE.MESSAGE) {
			result.push(data[i]);
		}
	}
	
	return result;
}

function setupNotification(data){
	let result = [];

	for(let i in data){
		let {notification_type_id} = data[i];
		if (notification_type_id == CONSTANT.NOTIFICATION_TYPE.ANNOUNCEMENT||
			notification_type_id == CONSTANT.NOTIFICATION_TYPE.MARKETING || 
			notification_type_id == CONSTANT.NOTIFICATION_TYPE.ORDER || 
			notification_type_id == CONSTANT.NOTIFICATION_TYPE.ACTION
			) {
			result.push(data[i]);
		}
	}

	return result;
}

function countMessageNotRead(data){
	let result = 0;
	for(let i in data){
		let {read_flag} = data[i];
		if (!read_flag) {
			result++;
		}
	}

	return result;
}

function getTime(mdTimes, operasional_time_id){
	let result = '00:00';
	for( x in mdTimes ){
		if(mdTimes[x].id == operasional_time_id){
			result = mdTimes[x].time;
		}
	}

	return result;
}

function getTimeInfo(mdTimes, operasional_time_id){
	let result = '00:00';
	for( x in mdTimes ){
		if(mdTimes[x].id == operasional_time_id){
			result = mdTimes[x].info;
		}
	}
	return result;
}
