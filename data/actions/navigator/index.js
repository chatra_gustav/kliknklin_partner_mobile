import * as actions from './../../actions';
import * as CONSTANT from 'app/data/constant';
import { Navigation } from 'react-native-navigation';

export function push(navigator, route, props = {}){
  return async function(dispatch, getState){
    
    let {pressBarrierVisibility} = getState().rootReducer.layout;

    dispatch(actions.layout.setPressBarrierVisibility(true));
    
    if (!pressBarrierVisibility) {
      let {overrideBackPress} = props;

      if (route == 'PartnerKliknKlin.OrderContent') {
        let {item} = props;
        if (item) {
          actions.order.setOrderActive(item);
        }
      }

      if (!overrideBackPress) {
        overrideBackPress = false;
      }
      else{
        props.overrideBackPress = true;
      }
      navigator.push({
        screen: route, // unique ID registered with Navigation.registerScreen
        passProps: props, // Object that will be passed as props to the pushed screen (optional)
        backButtonTitle: undefined, // override the back button title (optional)
        backButtonHidden: true, // hide the back button altogether (optional)
        navigatorStyle: {
          navBarHidden: true,
        }, // override the navigator style for the pushed screen (optional)
        navigatorButtons: {}, // override the nav buttons for the pushed screen (optional)
        animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the push have different transition animation (optional)
      });
    }
  }
}

export function resetTo(navigator, route, props = {}){
  return async function(dispatch, getState){
      let {pressBarrierVisibility} = getState().rootReducer.layout;

      dispatch(actions.layout.setPressBarrierVisibility(true));

      let overrideBackPress = false;
      if (route == 'PartnerKliknKlin.DashboardAdminOutlet' ||
          route == 'PartnerKliknKlin.SignIn' ||
          route == 'PartnerKliknKlin.Utility' ||
          route == 'PartnerKliknKlin.Setting' ||
          route == 'PartnerKliknKlin.HelpList' ||
          route == 'PartnerKliknKlin.SMSVerification'
        ) {
        overrideBackPress = true;
      }
      
      if (!pressBarrierVisibility) {
        navigator.resetTo({
          screen: route, // unique ID registered with Navigation.registerScreen
          passProps: props, // Object that will be passed as props to the pushed screen (optional)
          backButtonTitle: undefined, // override the back button title (optional)
          backButtonHidden: true, // hide the back button altogether (optional)
          navigatorStyle: {
            navBarHidden: true,
          }, // override the navigator style for the pushed screen (optional)
          navigatorButtons: {}, // override the nav buttons for the pushed screen (optional)
          animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the push have different transition animation (optional)
        });
      }
  }
}


export function pop(navigator){
  return async function(dispatch, getState){
    
    navigator.pop({
      animated: true, // does the pop have transition animation or does it happen immediately (optional)
      animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
      //overrideBackPress: true
    });
  }
}

export function popToRoot(navigator){
  return async function(dispatch, getState){
    dispatch(actions.navigator.dismissLightBox());

    navigator.popToRoot({
      animated: true, // does the popToRoot have transition animation or does it happen immediately (optional)
      animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the popToRoot have different transition animation (optional)
      //overrideBackPress: true
    });
  }
}

export function goToWebView(webLinkData, navigator, props = {}){
  return async function(dispatch, getState){
    let webLinks = getState().rootReducer.root.webLinks;
    let url = '';
    let title = webLinkData.title;
    for (let i in webLinks) {
        if (webLinks[i].id == webLinkData.id) {
          url = webLinks[i].url;
        }
    }
    
    dispatch(push(navigator, CONSTANT.ROUTE_TYPE.WEBVIEW, {
      title,
      url,
    }));
  }
}

export function showInAppNotification(route, props = {}){
  return async function(dispatch, getState){
    Navigation.showInAppNotification({
       screen: route, // unique ID registered with Navigation.registerScreen
       passProps: props, // simple serializable object that will pass as props to the in-app notification (optional)
       autoDismissTimerSec: 9999 // auto dismiss notification in seconds
    });
  }
}


export function showModal(route, props = {}){
  return async function(dispatch, getState){
    let {overrideBackPress} = props;
    
    Navigation.showModal({
      screen: route,
      passProps: props,
      navigatorStyle: {navBarHidden:true},
      animationType: 'none',
      overrideBackPress:overrideBackPress != undefined ? overrideBackPress : true,
    });
  }
}

export function dismissModal(){
  return async function(dispatch,getState){
    Navigation.dismissModal({
      animationType: 'none' // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
    });
  }
}

export function dismissAllModals(){
  return async function(dispatch,getState){
    Navigation.dismissAllModals({
      animationType: 'none' // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
    });
  }
}

export function showLightBox(route, props = {}){
  return async function(dispatch, getState){
    let {isDismissAllowed, overrideBackPress} = props;
    Navigation.showLightBox({
      screen: route,
      passProps: props,
      style: {
        backgroundBlur: 'dark', // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
        //backgroundColor: '#ff000080', // tint color for the background, you can specify alpha here (optional)
        tapBackgroundToDismiss: isDismissAllowed != undefined ? isDismissAllowed : true // dismisses LightBox on background taps (optional)
      },
      overrideBackPress:overrideBackPress != undefined ? overrideBackPress : true,
      adjustSoftInput: 'resize',
    });
  }
}

export function dismissLightBox(){
  return async function(dispatch,getState){
    Navigation.dismissLightBox();
  }
}


export function setRootRoute(route) {
  return {
    type: types.ROOT_ROUTE,
    route,
  };
}

