
import * as actions from './../../actions';

import * as types from './../actionTypes';
import * as STRING from './../../string';
import * as CONSTANT from './../../constant';
import * as API from './../../api';

export function submitStartPickup(navigator){
	return async function(dispatch, getState){
		try{
		let {orderActiveType} = getState().rootReducer.order.active;
		let orderData = getState().rootReducer.order.active.data;

    	let shipmentData = [];
		let word = '';

    	if(orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP){
			shipmentData = getState().rootReducer.order.shipmentActivePickup;
			word = 'pengambilan';
    	}
    	else if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY) {
			shipmentData = getState().rootReducer.order.shipmentActiveDelivery;
			word = 'pengantaran';
    	}

			
	    dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeA',{
	    	title:'konfirmasi', 
	    	message: 'apakah anda yakin untuk memulai '+word+' laundry', 
	    	buttonPrimaryText: 'ya', 
	    	buttonSecondaryText: 'batal', 
		    onPressButtonPrimary: () => {
		    	dispatch(startPickup(navigator, shipmentData))
		    }, 
		    onPressButtonSecondary: () =>{
				dispatch(actions.navigator.dismissLightBox());
		    },
		}));
		}
		catch(error){
			console.log(error);
			dispatch(actions.navigator.dismissLightBox());
			setTimeout(() => {
				alert('start shipment internal error!');
			}, 
			1000);
		}
  	}
}

export function startPickup(navigator, shipmentData){
	return async function(dispatch, getState){
		dispatch(actions.navigator.dismissLightBox());

		setTimeout(async () => {
			let userLocation = getState().rootReducer.user.location;
			let {orderActiveType} = getState().rootReducer.order.active;
			let orderData = getState().rootReducer.order.active.data;
			let userData = getState().rootReducer.user.data;
    		let userRole = getState().rootReducer.user.role;
		
			
			// let locationPermission = await dispatch(actions.location.checkLocationServicePermission(navigator, startPickup(navigator, shipmentData)));

			// if (locationPermission) {
			// 	dispatch(actions.user.getUserLocation(navigator));
			
			// 	let intervalLocation = setInterval(() => {
			// 		userLocation = getState().rootReducer.user.location;
			// 		if (userLocation != null) {
			// 			clearInterval(intervalLocation);

						dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));
			

						API.Order._startShipment(shipmentData.id, userLocation , userData.accessToken)
							.then(async response => {
								let {status, error, data} = response;
								if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
									if (userRole == CONSTANT.USER_ROLE.COURIER) {
										dispatch(getShipmentSchedule())
											.then((responseShipmentSchedule) => {
													let {shipmentScheduleList} = getState().rootReducer.order;
													let newOrderActive = [];

													for(let i in shipmentScheduleList){
														for (let j in shipmentScheduleList[i].logschedule) {
															if (newOrderActive.length == 0 && shipmentScheduleList[i].logschedule[j].order.id == orderData.id) {

																newOrderActive = shipmentScheduleList[i].logschedule[j].order;
															}
														}
													}

													dispatch(setOrderActive(newOrderActive));
													dispatch(setShipmentActivePickup(newOrderActive));
													dispatch(setShipmentActiveDelivery(newOrderActive));

													dispatch(getShipmentSchedule());

													dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
												    	title:'Perubahan Status Pesanan Berhasil', 
												    	message: 'Status pesanan saat ini : ' + data.status.orderlaundry.info,
												    	buttonPrimaryText: 'Ya', 
													    onPressButtonPrimary: () => {
													    	dispatch(actions.navigator.dismissModal());
															dispatch(actions.navigator.dismissLightBox());
													    },
													}));
											})
											.catch((error) => {

											})
									}
									else if (userRole == CONSTANT.USER_ROLE.ADMIN_OUTLET ||
												userRole == CONSTANT.USER_ROLE.OWNER
										) {
										if(orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP){
											dispatch(getPickupList())
												.then((responsePickupList) => {
													let {pickupOrderList} = getState().rootReducer.order;
													let newOrderActive = searchOrderContent(pickupOrderList, orderData.id);
													
													dispatch(setOrderActive(newOrderActive));
													dispatch(setShipmentActivePickup(newOrderActive));
													dispatch(setShipmentActiveDelivery(newOrderActive));

													dispatch(getSummaryOrder());

													dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
												    	title:'Perubahan Status Pesanan Berhasil', 
												    	message: 'Status pesanan saat ini : ' + data.status.orderlaundry.info,
												    	buttonPrimaryText: 'Ya', 
													    onPressButtonPrimary: () => {
													    	dispatch(actions.navigator.dismissModal());
															dispatch(actions.navigator.dismissLightBox());
													    },
													}));
												})
												.catch((error) => {
													console.log(error);
												});
									    	}
									    	else if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY) {
												dispatch(getDeliveryList())
												.then((responseDeliveryList) => {
													let {deliveryOrderList} = getState().rootReducer.order;
													let newOrderActive = searchOrderContent(deliveryOrderList, orderData.id);
													
													dispatch(setOrderActive(newOrderActive));
													dispatch(setShipmentActivePickup(newOrderActive));
													dispatch(setShipmentActiveDelivery(newOrderActive));
													
													dispatch(getSummaryOrder());
													
													dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
												    	title:'Perubahan Status Pesanan Berhasil', 
												    	message: 'Status pesanan saat ini : ' + data.status.orderlaundry.info,
												    	buttonPrimaryText: 'Ya',
													    onPressButtonPrimary: () => {
													    	dispatch(actions.navigator.dismissModal());
															dispatch(actions.navigator.dismissLightBox());
													    },
													}));
												})
												.catch((error) => {

												});
									    	}
									}
									
				        		}
								else{
									throw new Error('API ERROR : ' + status.code);
								}
							})
							.catch(error => {
								dispatch(actions.navigator.dismissModal());
								console.log(error);
							});
						//}
				//}, 500);
			//}
			
		},500)
	}
}



export function submitFinishPickup(navigator){
	return async function(dispatch, getState){
	    dispatch(actions.navigator.push(navigator, 'PartnerKliknKlin.ConfirmItem', {
	    	mode:'confirm',
	    }));
  	}
}

export function submitStartDelivery(navigator){
	return async function(dispatch, getState){
	    dispatch(actions.root.showBoxTypeA('konfirmasi', 'apakah anda yakin untuk memulai pengantaran laundry', 'ya', 'batal', 
		    () => {
				//api call waiting for pickup to go to pickup point
		    }, 
		    () =>{
				dispatch(actions.root.dismissBox());
		    },
		));
  	}
}

export function submitFinishDelivery(navigator){
	return async function(dispatch, getState){
	    dispatch(actions.root.showBoxTypeA('konfirmasi', 'apakah anda yakin untuk menyelesaikan pengantaran laundry', 'ya', 'batal', 
		    () => {
				//api call waiting for pickup to go to pickup point
		    }, 
		    () =>{
				dispatch(actions.root.dismissBox());
		    },
		));
  	}
}

export function submitFinishLaundry(navigator, orderID, change = -1, type= 'order-created'){
	return async function(dispatch, getState){
		let message = 'apakah anda yakin laundry telah selesai?';
		
		if (type == 'paid-order') {
			message = 'apakah anda yakin untuk menyelesaikan pembayaran?';
		}
		else if (type == 'taken-order') {
			message = 'apakah anda yakin pelanggan akan menyelesaikan pesanan?'
		}

	    dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeA',{
	    	title:'konfirmasi', 
	    	message,
	    	buttonPrimaryText:'ya',
	    	buttonSecondaryText: 'batal', 
		    onPressButtonPrimary: async () => {
				try{
					dispatch(actions.navigator.dismissLightBox());

					let {orderActiveType} = getState().rootReducer.order.active;
					let orderData = getState().rootReducer.order.active.data;
					let currentOrderID = orderID ? orderID : orderData.id;

					let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;

			    	let userData = getState().rootReducer.user.data;

					dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));
					

					API.Order._readyToBeDelivered(currentOrderID, outletList[indexActiveOutlet].id, userData.accessToken)
						.then(async response => {
							let {status, error, data} = response;
							if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
								if (data.order.online_flag) {
										dispatch(getDeliveryList(false))
										.then((responseDeliveryList) => {
											dispatch(actions.navigator.dismissAllModals());

											dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
										    	title:'Perubahan Status Pesanan Berhasil', 
										    	message: 'Status pesanan saat ini : ' + data.order_status_laundry,
										    	buttonPrimaryText: 'Ya', 
											    onPressButtonPrimary: () => {
											    	
											    	dispatch(actions.navigator.dismissModal());
													dispatch(actions.navigator.dismissLightBox());
											    },
											}));

											let {deliveryOrderList} = getState().rootReducer.order;
											let newOrderActive = searchOrderContent(deliveryOrderList, currentOrderID);
											
											dispatch(setOrderActive(newOrderActive));
											dispatch(setShipmentActivePickup(newOrderActive));
											dispatch(setShipmentActiveDelivery(newOrderActive));

											dispatch(getSummaryOrder());
										})
										.catch((error) => {
											dispatch(actions.navigator.dismissModal());
										});

										dispatch(getInProgressList(false))
										.then((responseInProgressList) => {
										})
										.catch((error) => {

										});
										
								}
								else{
					
									let orderCreatedData = getState().rootReducer.pos.orderCreated;
							        orderCreatedData.showCreated = true;
							        orderCreatedData.data = {
							            customer:{},
							            order:{},
							            order_items:{},
							            payment:{},
							            change,
							        }

									dispatch(actions.pos.setOrderCreatedData(orderCreatedData));
				                    dispatch(setOrderActive(data.order));
				                    dispatch(setShipmentActiveDelivery(data.order));
				                    
				                    dispatch(actions.navigator.resetTo(navigator, CONSTANT.ROUTE_TYPE.ORDER_CONTENT, {
				                        overrideBackPress:true,
				                        routeType:CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY,
				                    }));

				                    setTimeout(() => {
				                        dispatch(actions.navigator.showLightBox(CONSTANT.ROUTE_TYPE.BOX_ORDER_CREATED, {
				                            change:orderCreatedData.data.change,
				                            type,
				                            overrideBackPress:true,
				                        }));
				                    }, 150)
								}	
								
								
			        		}
							else{
								throw new Error('API ERROR : ' + status.code);
							}
						})
						.catch(error => {
							dispatch(actions.navigator.dismissModal());
							console.log(error);
						});
				}
				catch(error){
					console.log(error);
					dispatch(actions.navigator.dismissAllModals());
					dispatch(actions.navigator.dismissLightBox());
					setTimeout(() => {
						alert('start shipment internal error!');
					}, 
					1000);
				}
				
		    }, 
		    onPressButtonSecondary: () =>{
				dispatch(actions.navigator.dismissLightBox());
		    },
		}));
  	}
}

export function submitInProgressLaundry(navigator){
	return async function(dispatch, getState){
	    dispatch(actions.navigator.push(navigator, 'PartnerKliknKlin.CheckoutItem', {}));
  	}
}

export function submitConfirmItem(navigator, courierNote){
	return async function(dispatch, getState){
		dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeA',{
	    	title:'konfirmasi', 
	    	message: 'apakah anda yakin telah selesai melakukan pengambilan laundry? \n catat semua perubahan yang diberikan pelanggan!', 
	    	buttonPrimaryText: 'ya', 
	    	buttonSecondaryText: 'batal', 
		    onPressButtonPrimary: () => {
				dispatch(confirmItem(navigator, courierNote));
		    }, 
		    onPressButtonSecondary: () =>{
				dispatch(actions.navigator.dismissLightBox());
		    },
		}));
	}
}

export function confirmItem(navigator, courierNote){
	return async function(dispatch, getState){
		try{
			dispatch(actions.navigator.dismissLightBox());

			setTimeout(async () => {
				// let locationPermission = await dispatch(actions.location.checkLocationServicePermission(navigator, confirmItem(navigator, courierNote)));

				// if (locationPermission) {
				// 	let userLocation = await dispatch(actions.user.getUserLocation(navigator));

				// 	if (userLocation) {
						let userLocation = getState().rootReducer.user.location;
						let {orderActiveType} = getState().rootReducer.order.active;
						let orderData = getState().rootReducer.order.active.data;
						let userRole = getState().rootReducer.user.role;
				    	let userData = getState().rootReducer.user.data;
						let shipmentData = [];

				    	if(orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP){
							shipmentData = getState().rootReducer.order.shipmentActivePickup;
				    	}
				    	else if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY) {
							shipmentData = getState().rootReducer.order.shipmentActiveDelivery;
				    	}
						
						dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));
						
						dispatch(actions.navigator.dismissLightBox());
						
						dispatch(actions.navigator.pop(navigator));
						
						API.Order._finishShipment(shipmentData.id, userLocation ,courierNote, userData.accessToken)
							.then(async response => {
								let {status, error, data} = response;

								if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
									if (userRole == CONSTANT.USER_ROLE.COURIER) {
										dispatch(getShipmentSchedule())
											.then((responseShipmentSchedule) => {
													let {shipmentScheduleList} = getState().rootReducer.order;
													let newOrderActive = [];

													for(let i in shipmentScheduleList){
														for (let j in shipmentScheduleList[i].logschedule) {
															if (newOrderActive.length == 0 && shipmentScheduleList[i].logschedule[j].order.id == orderData.id) {

																newOrderActive = shipmentScheduleList[i].logschedule[j].order;
															}
														}
													}

													dispatch(setOrderActive(newOrderActive));
													dispatch(setShipmentActivePickup(newOrderActive));
													dispatch(setShipmentActiveDelivery(newOrderActive));

													dispatch(getShipmentSchedule());

													dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
												    	title:'Perubahan Status Pesanan Berhasil', 
												    	message: 'Status pesanan saat ini : ' + data.status.orderlaundry.info,
												    	buttonPrimaryText: 'Ya', 
													    onPressButtonPrimary: () => {
													    	dispatch(actions.navigator.dismissModal());
															dispatch(actions.navigator.dismissLightBox());
													    },
													}));
											})
											.catch((error) => {

											})
									}
									else if (userRole == CONSTANT.USER_ROLE.ADMIN_OUTLET ||
												userRole == CONSTANT.USER_ROLE.OWNER
										) {
										if(orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP){
											dispatch(getPickupList())
											.then((responsePickupList) => {
												let {pickupOrderList} = getState().rootReducer.order;
												let newOrderActive = searchOrderContent(pickupOrderList, orderData.id);
												
												dispatch(setOrderActive(newOrderActive));
												dispatch(setShipmentActivePickup(newOrderActive));
												dispatch(setShipmentActiveDelivery(newOrderActive));

												dispatch(getSummaryOrder());

												dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
											    	title:'Perubahan Status Pesanan Berhasil', 
											    	message: 'Status pesanan saat ini : ' + data.status.orderlaundry.info,
											    	buttonPrimaryText: 'Ya', 
												    onPressButtonPrimary: () => {
												    	
												    	dispatch(actions.navigator.dismissModal());
														dispatch(actions.navigator.dismissLightBox());
												    },
												}));
											})
											.catch((error) => {

											});
								    	}
								    	else if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY) {
											dispatch(getDeliveryList())
											.then((responsePickupList) => {
												let {deliveryOrderList} = getState().rootReducer.order;
												let newOrderActive = searchOrderContent(deliveryOrderList, orderData.id);
												
												dispatch(setOrderActive(newOrderActive));
												dispatch(setShipmentActivePickup(newOrderActive));
												dispatch(setShipmentActiveDelivery(newOrderActive));
												
												dispatch(getSummaryOrder());
												
												dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
											    	title:'Perubahan Status Pesanan Berhasil', 
											    	message: 'Status pesanan saat ini : ' + data.status.orderlaundry.info,
											    	buttonPrimaryText: 'Ya',
												    onPressButtonPrimary: () => {
												    	dispatch(actions.navigator.dismissModal());
														dispatch(actions.navigator.dismissLightBox());
												    },
												}));
											})
											.catch((error) => {

											});
								    	}
									}
				        		}
								else{
									throw new Error('API ERROR : ' + status.code);
								}
							})
							.catch(error => {
								dispatch(actions.navigator.dismissModal());
								console.log(error);
							});
					//}
					//}

			},500);
		
		}
		catch(error){
			console.log(error);
			dispatch(actions.navigator.dismissLightBox());
			setTimeout(() => {
				alert('start shipment internal error!');
			}, 
			1000);
		}	
	}
}

export function submitCheckoutItem(navigator){
	return async function(dispatch, getState){
		let updatedCheckoutItemList = getState().rootReducer.order.updatedCheckoutItemList;

		dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeA',{
	    	title:'konfirmasi', 
	    	message: 'apakah anda yakin data yang dimasukan benar? \n setelah ini perubahan tidak dapat dilakukan!',
	    	buttonSecondaryText: 'batal', 
	    	buttonPrimaryText: 'ya',
		    onPressButtonPrimary: async () => {
				try{
					let {orderActiveType} = getState().rootReducer.order.active;
					let orderData = getState().rootReducer.order.active.data;
					let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;

			    	let userData = getState().rootReducer.user.data;
					
					let orderItem = getState().rootReducer.order.updatedCheckoutItemList;
					
					dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));
					
					dispatch(actions.navigator.dismissLightBox());
					
					dispatch(actions.navigator.pop(navigator));
					
					API.Order._onLaunder(orderData.id, outletList[indexActiveOutlet].id, JSON.stringify(orderItem), userData.accessToken)
						.then(async response => {
							let {status, error, data} = response;
							if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
								dispatch(getPickupList())
								.then((responsePickupList) => {
								})
								.catch((error) => {

								});

								dispatch(getInProgressList())
								.then((responsePickupList) => {
									let {inProgressOrderList} = getState().rootReducer.order;
									let newOrderActive = searchOrderContent(inProgressOrderList, orderData.id);
									
									dispatch(setOrderActive(newOrderActive));
									dispatch(setShipmentActivePickup(newOrderActive));
									dispatch(setShipmentActiveDelivery(newOrderActive));

									dispatch(getSummaryOrder());

									dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
								    	title:'Perubahan Status Pesanan Berhasil', 
								    	message: 'Status pesanan saat ini : ' + data.order_status_laundry,
								    	buttonPrimaryText: 'Ya', 
									    onPressButtonPrimary: () => {
									    	
									    	dispatch(actions.navigator.dismissModal());
											dispatch(actions.navigator.dismissLightBox());
									    },
									}));
								})
								.catch((error) => {

								});
			        		}
							else{
								throw new Error('API ERROR : ' + status.code);
							}
						})
						.catch(error => {
							dispatch(actions.navigator.dismissModal());
							console.log(error);
						});
				}
				catch(error){
					console.log(error);
					dispatch(actions.navigator.dismissLightBox());
					setTimeout(() => {
						alert('start shipment internal error!');
					}, 
					1000);
				}
				
		    }, 
		    onPressButtonSecondary: () =>{
				dispatch(actions.navigator.dismissLightBox());
		    },
		}));
	}
}

export function submitDelayShipment(navigator, delayReasonID, delayReasonInfo, notes, showDelayReasonList){
	return async function(dispatch, getState){
		dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeA',{
	    	title:'Konfirmasi', 
	    	message: 'Apakah anda yakin untuk menunda perjalanan? \n Alasan : ' + delayReasonInfo ,
	    	buttonPrimaryText: 'Ya', 
		    onPressButtonPrimary: () => {
		    	try{
					let {orderActiveType} = getState().rootReducer.order.active;
					let orderData = getState().rootReducer.order.active.data;

			    	let userData = getState().rootReducer.user.data;
    				let userRole = getState().rootReducer.user.role;

					let shipmentData = [];

			    	if(orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP){
						shipmentData = getState().rootReducer.order.shipmentActivePickup;
			    	}
			    	else if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY) {
						shipmentData = getState().rootReducer.order.shipmentActiveDelivery;
			    	}
					
					dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));
					
					dispatch(actions.navigator.dismissLightBox());

					API.Order._setDelayShipment(shipmentData.id, delayReasonID, notes, userData.accessToken)
						.then(async response => {
							let {status, error, data} = response;

							if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
								if (userRole == CONSTANT.USER_ROLE.COURIER) {
									dispatch(getShipmentSchedule())
										.then((responseShipmentSchedule) => {
												let {shipmentScheduleList} = getState().rootReducer.order;
												let newOrderActive = [];

												for(let i in shipmentScheduleList){
													for (let j in shipmentScheduleList[i].logschedule) {
														if (newOrderActive.length == 0 && shipmentScheduleList[i].logschedule[j].order.id == orderData.id) {

															newOrderActive = shipmentScheduleList[i].logschedule[j].order;
														}
													}
												}

												dispatch(setOrderActive(newOrderActive));
												dispatch(setShipmentActivePickup(newOrderActive));
												dispatch(setShipmentActiveDelivery(newOrderActive));

												dispatch(getShipmentSchedule());

												dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
											    	title:'Perubahan Status Pesanan Berhasil', 
											    	message: 'Status pesanan saat ini : ' + data.status.shipment.info,
											    	buttonPrimaryText: 'Ya', 
												    onPressButtonPrimary: () => {
												    	
												    	dispatch(actions.navigator.dismissModal());
														dispatch(actions.navigator.dismissLightBox());
												    },
												}));
										})
										.catch((error) => {

										})
								}
								else if (userRole == CONSTANT.USER_ROLE.ADMIN_OUTLET ||
											userRole == CONSTANT.USER_ROLE.OWNER
									) {
									if(orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP){
										dispatch(getPickupList())
											.then((responsePickupList) => {
												let {pickupOrderList} = getState().rootReducer.order;
												let newOrderActive = searchOrderContent(pickupOrderList, orderData.id);

												dispatch(setOrderActive(newOrderActive));
												dispatch(setShipmentActivePickup(newOrderActive));
												dispatch(setShipmentActiveDelivery(newOrderActive));

												dispatch(getSummaryOrder());

												dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
											    	title:'Perubahan Status Pesanan Berhasil', 
											    	message: 'Status pesanan saat ini : ' + data.status.shipment.info,
											    	buttonPrimaryText: 'Ya', 
												    onPressButtonPrimary: () => {
												    	
												    	dispatch(actions.navigator.dismissModal());
														dispatch(actions.navigator.dismissLightBox());
												    },
												}));
											})
											.catch((error) => {

											});
							    	}
							    	else if (orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY) {
							    		
										dispatch(getDeliveryList())
											.then((responsePickupList) => {
												let {deliveryOrderList} = getState().rootReducer.order;
												let newOrderActive = searchOrderContent(deliveryOrderList, orderData.id);
												
												dispatch(setOrderActive(newOrderActive));
												dispatch(setShipmentActivePickup(newOrderActive));
												dispatch(setShipmentActiveDelivery(newOrderActive));
												
												dispatch(getSummaryOrder());
												
												dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
											    	title:'Perubahan Status Pesanan Berhasil', 
											    	message: 'Status pesanan saat ini : ' + data.status.shipment.info,
											    	buttonPrimaryText: 'Ya', 
												    onPressButtonPrimary: () => {
												    	dispatch(actions.navigator.dismissModal());
														dispatch(actions.navigator.dismissLightBox());
												    },
												}));
											})
											.catch((error) => {

											});
							    	}
								}
			        		}
							else{
								throw new Error('API ERROR : ' + status.code);
							}
						})
						.catch(error => {
							dispatch(actions.navigator.dismissModal());
							console.log(error);
						});
				}
				catch(error){
					dispatch(actions.navigator.dismissLightBox());
					
					console.log(error);
					setTimeout(() => {
						alert('start shipment internal error!');
					}, 
					1000);
				}
		    },
		    buttonSecondaryText: 'Batal', 
		    onPressButtonSecondary: () => {
		    	dispatch(actions.navigator.dismissLightBox());

		    	setTimeout(() => {
					showDelayReasonList();
		    	},500);
		    	//dispatch(actions.navigator.dismissModal());
				//dispatch(actions.navigator.dismissLightBox());
		    },
		}));
  	}
}

export function search(keyword, category, searchingType){
	return async function(dispatch, getState){
		let userData = getState().rootReducer.user.data;
		let userRole = getState().rootReducer.user.role;
		let attendanceDate = getState().rootReducer.attendance.attendanceDate;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;

		if(indexActiveOutlet != null){
			if (userRole == CONSTANT.USER_ROLE.COURIER) {
				userRole = 'Kurir';
			}else if (userRole == CONSTANT.USER_ROLE.OWNER) {
				userRole = 'Owner';
			}else if (userRole == CONSTANT.USER_ROLE.ADMIN_OUTLET) {
				userRole = 'Admin Outlet';
			}

			dispatch(setSearchOrderLoadingStatus(true));
			dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));

			API.Order._searchOrder(keyword, category, outletList[indexActiveOutlet].id, userRole, userData.accessToken, searchingType)
			.then((response) => {
				dispatch(setSearchOrderLoadingStatus(false));
				dispatch(actions.navigator.dismissAllModals());

				let {status, error, data} = response;

        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
        			
        			dispatch(setSearchOrderResult(data));
        		}
				else{
					throw new Error('API ERROR : ' + status.code);
				}
			})
			.catch((error) => {
				console.log(error)
				dispatch(setSearchOrderLoadingStatus(false));
				dispatch(actions.navigator.dismissAllModals());

				setTimeout(
        			() => alert(error.message)
        		,200)
			})
		}
	}
}


export function getShipmentSchedule(){

	return async function(dispatch, getState){
		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));
					
		let {accessToken} = getState().rootReducer.user.data;

		let {shipmentScheduleDate} = getState().rootReducer.order;

		await API.Order._getShipmentSchedule(shipmentScheduleDate, accessToken)
			.then(async (response) => {
				let {status, error, data} = response;

				dispatch(actions.navigator.dismissAllModals());
				
        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
        			
        			await dispatch(setShipmentScheduleList(data));
        			dispatch(setAssignmentTodayCourier(data));
        		}
				else{
					throw new Error('API ERROR : ' + status.code);
				}
			})
			.catch((error) => {
				console.log(error, '--getShipmentSchedule');

				dispatch(actions.navigator.dismissModal());

        		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
					dispatch(actions.root.showConnectionError(getShipmentSchedule()));
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}
			});
	}
}

export function getHistoryList(fromDate, toDate){
	return async function(dispatch, getState){
		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));

		let {accessToken} = getState().rootReducer.user.data;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;
		
		if (!fromDate && !toDate) {
			let historyDate = getState().rootReducer.order.historyOrderDate;
			fromDate = historyDate.fromDate;
			toDate = historyDate.toDate;
		}

		await API.Order._getHistoryList(outletList[indexActiveOutlet].id, fromDate, toDate, accessToken)
			.then((response) => {
				let {status, error, data} = response;
				dispatch(actions.navigator.dismissAllModals());
        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
        			dispatch(setHistoryOrderList(data));
        		}
				else{
					throw new Error('API ERROR : ' + status.code);
				}
			})
			.catch((error) => {
				console.log(error);
				dispatch(actions.navigator.dismissModal());

        		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
					dispatch(actions.root.showConnectionError(getHistoryList()));
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}
			});
	}
}

export function getComplaintInfo(complaintItems, complaintStatus){
	return async function(dispatch, getState){
		let message = '';
	    let isImportant = false;

	    //need-solution, 'rejected-solution', 'solution-accepted', 'new-message', 'solution-need-customer-response',
	    let messageType = 'need-solution';

		if (complaintStatus == CONSTANT.COMPLAINT_STATUS.IN_PROGRESS) {
			
			let counterComplaintItemAccepted = 0;
		    let counterComplaintItemNeedCustomerResponse = 0;
		    for(let i in complaintItems){
		    	console.log(complaintItems[i].complaint_item_status_id);
		      if (complaintItems[i].complaint_item_status_id == CONSTANT.COMPLAINT_ITEM_STATUS.IN_PROGRESS) {
		        counterComplaintItemAccepted++;
		      }

		      if (complaintItems[i].complaint_item_status_id == CONSTANT.COMPLAINT_ITEM_STATUS.SOLUTION_APPLIED) {
		        counterComplaintItemNeedCustomerResponse++;
		      }

		      if (complaintItems[i].complaint_item_status_id == CONSTANT.COMPLAINT_ITEM_STATUS.REJECTED) {
		        messageType = 'rejected-solution';
		      }
		    }
			console.log(counterComplaintItemAccepted , counterComplaintItemNeedCustomerResponse, complaintItems.length)
		    if (counterComplaintItemAccepted == complaintItems.length) {
		      messageType = 'solution-accepted';
		    }
		    else if (counterComplaintItemNeedCustomerResponse == complaintItems.length) {
		      messageType = 'solution-need-customer-response';
		    }

		    if (messageType == 'need-solution') {
		      message = 'Berikan solusi terhadap keluhan ini!';
		      isImportant = true;
		    }
		    else if (messageType == 'rejected-solution') {
		      message = 'Solusi ada yang ditolak, berikan solusi yang baru!';
		      isImportant = true;
		    }
		    else if (messageType == 'solution-need-customer-response') {
		      message = 'Menunggu respon pelanggan terhadap solusi.';
		      isImportant = false;
		    }
		    else if (messageType == 'solution-accepted') {
		      message = 'Solusi diterima, segera selesaikan keluhan!';
		      isImportant = true;
		    }
		}
		else if (complaintStatus == CONSTANT.COMPLAINT_STATUS.SOLVED) {
			message = 'Keluhan telah diselesaikan.';
		    isImportant = false;
		}

	    return {
	      message,
	      isImportant,
	    }
	}
}

export function setHistoryOrderDate(fromDate, toDate){
	return async function(dispatch, getState){
		dispatch(_setHistoryOrderDate(fromDate, toDate));

		dispatch(getHistoryList(fromDate, toDate));
	}
}

export function getIncomingList(){
	return async function(dispatch, getState){
		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));

		let {accessToken} = getState().rootReducer.user.data;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;

		API.Order._getIncomingList(outletList[indexActiveOutlet].id, accessToken)
			.then((response) => {
				let {status, error, data} = response;
				dispatch(actions.navigator.dismissAllModals());
				
        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
        			
        			dispatch(setIncomingOrderList(data));
        		}
				else{
					throw new Error('API ERROR : ' + status.code);
				}
			})
			.catch((error) => {
				console.log(error);
				dispatch(actions.navigator.dismissModal());

        		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
					dispatch(actions.root.showConnectionError(getIncomingList()));
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}
			});
	}
}

export function getPickupList(){
	return async function(dispatch, getState){
		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));

		let {accessToken} = getState().rootReducer.user.data;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;

		
		await API.Order._getPickupList(outletList[indexActiveOutlet].id, accessToken)
			.then(async (response) => {
				let {status, error, data} = response;
				dispatch(actions.navigator.dismissAllModals());
				
        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
        			dispatch(setOrderListDataMapping(data, 'pickup'));
        			dispatch(setPickupOrderList(data));
        			dispatch(setLatePickupOrderList(data));
        		}
				else{
					throw new Error('API ERROR : ' + status.code);
				}
			})
			.catch((error) => {
				console.log(error);
				dispatch(actions.navigator.dismissModal());

        		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
					dispatch(actions.root.showConnectionError(getPickupList()));
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}
			});
	}
}

export function getInProgressList(isModalDismiss = true){
	return async function(dispatch, getState){
		if (isModalDismiss) {
			dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));
		}

		let {accessToken} = getState().rootReducer.user.data;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;

		await API.Order._getInProgressList(outletList[indexActiveOutlet].id, accessToken)
			.then((response) => {
				let {status, error, data} = response;
				if (isModalDismiss) {
					dispatch(actions.navigator.dismissAllModals());
				}
				
        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
        			dispatch(setOrderListDataMapping(data));
        			dispatch(setInProgressOrderList(data));
        			dispatch(setLateInProgressOrderList(data));
        		}
				else{
					throw new Error('API ERROR : ' + status.code);
				}
			})
			.catch((error) => {
				console.log(error);
				dispatch(actions.navigator.dismissModal());

        		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
					dispatch(actions.root.showConnectionError(getInProgressList()));
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}
			});
	}
}

export function getDeliveryList(isModalDismiss = true){
	return async function(dispatch, getState){
		if (isModalDismiss) {
			dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));
		}
		
		let {accessToken} = getState().rootReducer.user.data;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;

		await API.Order._getDeliveryList(outletList[indexActiveOutlet].id, accessToken)
			.then((response) => {
				let {status, error, data} = response;

				if (isModalDismiss) {
					dispatch(actions.navigator.dismissAllModals());
				}
				
        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
        			
        			dispatch(setOrderListDataMapping(data));
        			dispatch(setDeliveryOrderList(data));
        			dispatch(setLateDeliveryOrderList(data));
        		}
				else{
					throw new Error('API ERROR : ' + status.code);
				}
			})
			.catch((error) => {
				console.log(error);
				dispatch(actions.navigator.dismissModal());

        		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
					dispatch(actions.root.showConnectionError(getDeliveryList()));
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}
			});
	}
}

export function goToOrderDetail(data = null, navigator){
	return async function(dispatch, getState){
		if (data) {
			await dispatch(setOrderActive(data));
			await dispatch(setShipmentActivePickup(data))
        	await dispatch(setShipmentActiveDelivery(data));
		}
        
		let orderActiveType = getState().rootReducer.order.active.orderActiveType;
        dispatch(actions.navigator.resetTo(navigator, CONSTANT.ROUTE_TYPE.ORDER_CONTENT, {
            overrideBackPress:true,
            route:CONSTANT.ROUTE_TYPE.ORDER_LIST,
            routeType:orderActiveType,
        }));

		setTimeout(() => {
        	dispatch(actions.navigator.dismissAllModals());
		}, 100);
	}
}

export function getOrderContent(order_id, navigator){
	return async function(dispatch, getState){
		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));

		let {accessToken} = getState().rootReducer.user.data;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;
		await API.Order._getOrderContent(order_id, accessToken)
			.then((response) => {
				let {status, error, data} = response;
        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
        			
        			dispatch(goToOrderDetail(data, navigator));
        		}
				else{
					throw new Error('API ERROR : ' + status.code);
				}
			})
			.catch((error) => {
				console.log(error);
				dispatch(actions.navigator.dismissModal());

        		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
					dispatch(actions.root.showConnectionError(getComplaintList()));
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}
			});
	}
}

export function getComplaintList(){
	return async function(dispatch, getState){
		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));

		let {accessToken} = getState().rootReducer.user.data;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;

		await API.Order._getComplaintList(outletList[indexActiveOutlet].id, accessToken)
			.then((response) => {
				let {status, error, data} = response;
				dispatch(actions.navigator.dismissAllModals());
				
        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
        			
        			dispatch(setComplaintOrderList(data));
        		}
				else{
					throw new Error('API ERROR : ' + status.code);
				}
			})
			.catch((error) => {
				console.log(error);
				dispatch(actions.navigator.dismissModal());

        		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
					dispatch(actions.root.showConnectionError(getComplaintList()));
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}
			});
	}
}

export function getComplaintMessageList(complaintID){
	return async function(dispatch, getState){

		let {accessToken} = getState().rootReducer.user.data;
		
		await API.Order._getComplaintMessageList(complaintID, accessToken)
			.then((response) => {
				let {status, error, data} = response;
				console.log(response);
        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
        			
        			dispatch(setComplaintMessageList(data));
        		}
				else{
					throw new Error('API ERROR : ' + status.code);
				}
			})
			.catch((error) => {
				dispatch(actions.navigator.dismissModal());

        		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
					dispatch(actions.root.showConnectionError(getComplaintMessageList(complaintID)));
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}
			});
	}
}

export function getSummaryOrder(){
	return async function(dispatch, getState){
		let {accessToken} = getState().rootReducer.user.data;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;
		if (indexActiveOutlet != null) {
			console.log('dimulai cerita');
			API.Order._getSummaryOrder(outletList[indexActiveOutlet].id, accessToken)
			.then(async (response) => {
				console.log(response);
				let {status, error, data} = response;
				dispatch(actions.navigator.dismissAllModals());
        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
        			let {incoming, pickup, notifexist, onprogress, delivery, complaint, pickuplate, onprogresslate, deliverylate, isnewmessageorsolutioncomplaint} = data;
        			dispatch(setSummaryOrder(incoming, pickup, onprogress, delivery, complaint, pickuplate, onprogresslate, deliverylate, isnewmessageorsolutioncomplaint, notifexist));
        		}
				else{
					throw new Error('API ERROR : ' + status.code);
				}
			})
			.catch((error) => {
				console.log(error, error.message, 'getSummaryOrder');
				dispatch(actions.navigator.dismissModal());

    			if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED) {
					//dispatch(getSummaryOrder());
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}
			});
		}
	}
}


export function setShipmentDate(date){
	return async function(dispatch, getState){
		dispatch(setShipmentScheduleDate(date));

		dispatch(getShipmentSchedule(date));
	}
}

function searchOrderContent(data, orderID){
	for(let index in data){
		if (data[index].id == orderID) {
			return data[index];
		}
	}

	return [];
}

export function getCheckoutItemList(){
	return async function(dispatch, getState){
		//dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));

		let {accessToken} = getState().rootReducer.user.data;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;
		let {data} = getState().rootReducer.order.active;
		let orderID = data.id;
		
		API.Order._getCheckoutItemList(orderID, accessToken)
			.then(async (response) => {
				let {status, error, data} = response;
				dispatch(actions.navigator.dismissAllModals());

        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
        			//dispatch(setExtractedCheckoutItemList(data));
        			dispatch(setCheckoutItemList(data));
        		}
				else{
					throw new Error('API ERROR : ' + status.code);
				}
			})
			.catch((error) => {
				console.log(error);
				dispatch(actions.navigator.dismissModal());

        		setTimeout(
        			() => alert(error)
        		,200);
			});
	}
}

export function setIncomingResponse(isAccepted, courierID = null, orderID, rejectReasonID){
	return async function(dispatch, getState){
		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));

		let {accessToken} = getState().rootReducer.user.data;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;
		
		//console.log(isAccepted, courierID, outletList[indexActiveOutlet].id, orderID, rejectReasonID, accessToken);

		API.Order._incomingResponse(isAccepted, courierID, outletList[indexActiveOutlet].id, orderID, rejectReasonID, accessToken)
			.then(async (response) => {
				
				let {status, error, data} = response;
				dispatch(actions.navigator.dismissAllModals());

        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {

        			dispatch(getSummaryOrder());
        			dispatch(getIncomingList());

        			if (isAccepted) {
						dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
					    	title:'Pesan', 
					    	message: 'Pesanan Berhasil Diterima',
					    	buttonPrimaryText: 'Ya', 
						    onPressButtonPrimary: () => {
						    	dispatch(actions.navigator.dismissModal());
								dispatch(actions.navigator.dismissLightBox());
						    },
						}));
        			}
        			else{
						dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
					    	title:'Pesan', 
					    	message: 'Penolakan Pesanan Berhasil',
					    	buttonPrimaryText: 'Ya', 
						    onPressButtonPrimary: () => {
						    	dispatch(actions.navigator.dismissModal());
								dispatch(actions.navigator.dismissLightBox());
						    },
						}));
        			}

        			
        			//let {pickup, onprogress, delivery, complaint, pickuplate, onprogresslate, deliverylate} = data;
        			//dispatch(setSummaryOrder(pickup, onprogress, delivery, complaint, pickuplate, onprogresslate, deliverylate));
        		}
				else{
					dispatch(getSummaryOrder());
        			dispatch(getIncomingList());
					alert('Gagal diterima! Order telah dibatalkan / diambil oleh outlet lain')
				}
			})
			.catch((error) => {
				console.log(error, 'action.order.setIncomingResponse');
				dispatch(actions.navigator.dismissModal());

        		setTimeout(
        			() => alert(error)
        		,200);
			});
	}
}

//set function
export function setOrderActive(data, orderActiveType = null){

	if (!orderActiveType) {
		orderActiveType = CONSTANT.ORDER_ACTIVE_TYPE.DEFAULT;

		try{
			let {orderlaundry, complaint} = data;
			let orderStatus = orderlaundry.order_status_laundry_id;

			if (orderStatus == CONSTANT.ORDER_STATUS.WAIT_PICKUP ||
				orderStatus == CONSTANT.ORDER_STATUS.IN_PROGRESS_PICKUP ||
				orderStatus == CONSTANT.ORDER_STATUS.DONE_PICKUP
			) {
				orderActiveType = CONSTANT.ORDER_ACTIVE_TYPE.PICKUP;
			}
			else if ( orderStatus == CONSTANT.ORDER_STATUS.IN_PROGRESS_LAUNDRY
				) {
				orderActiveType = CONSTANT.ORDER_ACTIVE_TYPE.IN_PROGRESS;
			}
			else if (orderStatus == CONSTANT.ORDER_STATUS.DONE_LAUNDRY || 
						orderStatus == CONSTANT.ORDER_STATUS.IN_PROGRESS_DELIVERY ||
						orderStatus == CONSTANT.ORDER_STATUS.DONE_DELIVERY ||
						orderStatus == CONSTANT.ORDER_STATUS.AFTER_SUBMIT_POS
				) {
				orderActiveType = CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY;
			}
		}
		catch(error){
			console.log(error, 'setOrderActive');
		}	
	}
		
	return {
	    type: types.ORDER_ACTIVE, 
	    data,
	    orderActiveType,
	};	
}

export function setCheckoutItemList(data){
	data = getShipment('pickup', data);

	return {
	    type: types.CHECKOUT_ITEM_LIST, 
	    data,
	};	
}

export function getAvailableDuration(){
	return async function(dispatch, getState){
		let orderData = getState().rootReducer.order.active.data;
		let {orderitem} = orderData;
	}
}

export function updateCheckoutItem(orderItemID, itemPrice, currentQuantity, updatedQuantity, itemDuration, kliknklinItemPriceAreaID = null, outletItemWashingTimeID = null, listAddedServices = null){
	return async function(dispatch, getState){
		let updatedCheckoutItemList = getState().rootReducer.order.updatedCheckoutItemList;

		let index = -1;

		for(let indexList in updatedCheckoutItemList){
			if (updatedCheckoutItemList[indexList].id == orderItemID) {
				index = indexList;
			}
		}

		if (index == -1) {
			let objOrderItem = {
				id:orderItemID,
				item_price:itemPrice,
				item_duration:itemDuration,
				item_quantity:currentQuantity,
				kliknklin_item_price_area_id:kliknklinItemPriceAreaID,
				outlet_item_washing_time_id:outletItemWashingTimeID,
				list_added_services:listAddedServices,
				action:'no action',
			};
			
			updatedCheckoutItemList.push(objOrderItem);
		}
		else{
			if (updatedQuantity == 0) {
				updatedCheckoutItemList[index] = {
					id:orderItemID,
					item_price:itemPrice,
					item_duration:itemDuration,
					item_quantity:updatedQuantity,
					kliknklin_item_price_area_id:kliknklinItemPriceAreaID,
					outlet_item_washing_time_id:outletItemWashingTimeID,
					list_added_services:listAddedServices,
					action:'delete',
				};
			}
			else if (currentQuantity != updatedQuantity) {
				updatedCheckoutItemList[index] = {
					id:orderItemID,
					item_price:itemPrice,
					item_duration:itemDuration,
					item_quantity:updatedQuantity,
					kliknklin_item_price_area_id:kliknklinItemPriceAreaID,
					outlet_item_washing_time_id:outletItemWashingTimeID,
					list_added_services:listAddedServices,
					action:'update',
				};
				
			}
		}

		dispatch(setUpdatedCheckoutItemList(updatedCheckoutItemList));
	}
}

function getIndexList(itemToSearchList, list){

	for(let indexList in list){
		let isReturnIndex = true;
		for(let indexItemToSearchList in itemToSearchList){
			//console.log(indexList, list[indexList][itemToSearchList[indexItemToSearchList].categoryToSearch], itemToSearchList[indexItemToSearchList].itemToSearch);
			if (list[indexList][itemToSearchList[indexItemToSearchList].categoryToSearch] != itemToSearchList[indexItemToSearchList].itemToSearch) {
				isReturnIndex = false;
			}
		}

		if (isReturnIndex) {
			return indexList;
		}
	}

	return -1;
}

export function setExtractedCheckoutItemList(data){
	let categoryList = [];
	let itemList = [];
	let addedItemList = [];
	let washingServiceList = [];
	let timeServiceList = [];
	let addedServiceList = [];
	let relationItemVariantList = [];

	for(let indexData in data){
		let category = data[indexData];
		let categoryID = category.category_id;
		let categoryInfo = category.category;
		let categoryIndex = categoryList.length;

		let objCategory = {};
		objCategory.categoryID = categoryID;
		objCategory.categoryInfo = categoryInfo;

		categoryList.push(objCategory);
		
		let itemData = category.items;

		for(let indexItems in itemData){
			let item = itemData[indexItems];
			let itemID = item.item_id;
			let itemInfo = item.item;
			let itemIndex = itemList.length;

			let objItem = {};
			objItem.categoryIndex = categoryIndex;
			objItem.categoryInfo = categoryInfo;
			objItem.itemInfo = itemInfo;
			objItem.itemID = itemID;

			itemList.push(objItem);

			let variantData = item.variant;

			for(let indexVariant in variantData){
				let variant = variantData[indexVariant];
				let washingServiceID = variant.washingservice.id;
				let washingServiceInfo = variant.washingservice.info;
				let timeServiceID = variant.timeservice.id;
				let timeServiceInfo = variant.timeservice.info;
				let addedServiceData = variant.addedservice;

				let indexWashingService = getIndexList([
					{itemToSearch: washingServiceID, categoryToSearch: 'washingServiceID'},
					{itemToSearch: itemID, categoryToSearch: 'itemID'}
				], washingServiceList);


				let indexTimeService = getIndexList([
					{itemToSearch: timeServiceID, categoryToSearch: 'timeServiceID'},
					{itemToSearch: itemID, categoryToSearch: 'itemID'}
				], timeServiceList);

				let objWashingService = {};
				let objTimeService = {};
				
				if (indexWashingService == -1) {
					objWashingService.itemID = itemID;
					objWashingService.itemInfo = itemInfo;
					objWashingService.washingServiceID = washingServiceID;
					objWashingService.washingServiceInfo = washingServiceInfo;

					washingServiceList.push(objWashingService);
				}

				if (indexTimeService == -1) {

					objTimeService.itemID = itemID;
					objTimeService.itemInfo = itemInfo;
					objTimeService.timeServiceID = timeServiceID;
					objTimeService.timeServiceInfo = timeServiceInfo;

					timeServiceList.push(objTimeService);
				}

				for(let indexAddedService in addedServiceData){
					let addedService = addedServiceData[indexAddedService];
					let addedServiceID = addedService.id;
					let addedServiceInfo = addedService.itemaddedservice.mdaddedservice.info;

					let indexAddedService = getIndexList([
						{itemToSearch: addedServiceID, categoryToSearch: 'addedServiceID'},
						{itemToSearch: itemID, categoryToSearch: 'itemID'}
					], addedServiceList);

					let objAddedService = {};

					if (indexAddedService == -1) {
						objAddedService.itemID = itemID;
						objAddedService.itemInfo = itemInfo;
						objAddedService.addedServiceID = addedServiceID;
						objAddedService.addedServiceInfo = addedServiceInfo;
						
						addedServiceList.push(objAddedService);
					}
				}
			}

		}
	}
	
	return {
	    type: types.EXTRACTED_CHECKOUT_ITEM_LIST, 
	    categoryList,
	    itemList,
	    washingServiceList,
	    timeServiceList,
	    addedItemList,
	    relationItemVariantList,
	};	
}

function setOrderListDataMapping(data,  shipmentType = 'delivery'){
	return async function(dispatch, getState){
	    let result = [];
	    for(let i in data){
	      let shipment = getShipment(shipmentType, data[i]);
	      let shipmentTimeInfo = shipment.shipmentschedule.shipment_time_info;
	      let shipmentDate = shipment.shipmentschedule.shipment_date;
	      let createdAt = data[i].created_at.split(' ')[0];

	      let obj = {};
	      let isExist = false;

	      if (result.length != 0) {}{
	        for(let j in result){
	          if (result[j].date == shipmentDate) {
	            isExist = true;
	            result[j].data.push(data[i]);
	          }
	        }
	      }

	      if (result.length == 0 || !isExist) {
	          obj.time = shipmentTimeInfo;
	          obj.date = shipmentDate;
	          obj.createdAt = createdAt;
	          obj.data = [];
	          obj.data.push(data[i]);
	          result.push(obj);
	      }
	    }
	    result.sort((a,b) => {
	      let shipmentA = a.date;
	      let shipmentB = b.date;
	      return new Date(shipmentA).getTime() - new Date(shipmentB).getTime();
	    })
		
	    dispatch(setOrderListData(result));
	}
  }

export function setUpdatedCheckoutItemList(data){
	return {
	    type: types.UPDATED_CHECKOUT_ITEM_LIST, 
	    data,
	};	
}

export function setShipmentActivePickup(data){
	if (data) {
		data = getShipment('pickup', data);
	}
	
	return {
	    type: types.SHIPMENT_ACTIVE_PICKUP, 
	    data,
	};	
}

export function setShipmentActiveDelivery(data){
	if (data) {
		data = getShipment('delivery', data);
	}

	return {
	    type: types.SHIPMENT_ACTIVE_DELIVERY, 
	    data,
	};	
}

function getShipment(type = 'pickup', data){
    let {shipments} = data;

    for(let i in shipments){
      let {shipmenttype} = shipments[i];

		if (type == 'pickup') {
			if (shipmenttype.id == CONSTANT.SHIPMENT_TYPE.PICKUP) {
		        return shipments[i];
		    }
		}
		else if (type == 'delivery') {
			if (shipmenttype.id == CONSTANT.SHIPMENT_TYPE.DELIVERY) {
		        return shipments[i];
		    }
		}
    }

    return {};
}

export function searchOrderIndex(orderID){
	return async function(dispatch, getState){
		let {data} = getState().rootReducer.order.active;

		for(let index in data){
			if (data[index].id == orderID) {
				return index;
			}
		}

		return -1;
	}
}

export function setOrderLaundryStatus(orderID, newID, newInfo){
	return async function(dispatch, getState){

		let {data} = getState().rootReducer.order.active;
		
		let orderIndex = dispatch(searchOrderIndex(orderID));

		let {orderlaundry} = data[orderIndex];
		let {orderstatuslaundry} = orderlaundry;

		orderlaundry.order_status_laundry_id = newID;

		orderstatuslaundry.id = newID;
		orderstatuslaundry.info = newInfo;

		dispatch(setOrderActive(data));
	}
}

export function setOrderShipmentStatus(orderID, newID, newInfo){
	return async function(dispatch, getState){
		let {data} = getState().rootReducer.order.active;
		
		let orderIndex = dispatch(searchOrderIndex(orderID));

		let {orderlaundry} = data[orderIndex];
		let {orderstatuslaundry} = orderlaundry;

		orderlaundry.order_status_laundry_id = newID;

		orderstatuslaundry.id = newID;
		orderstatuslaundry.info = newInfo;

		dispatch(setOrderActive(data));
	}
}


export function setShipmentScheduleList(data){
	return {
	    type: types.SHIPMENT_SCHEDULE_LIST, 
	    data,
	};	
}

export function setShipmentScheduleDate(dates){
	const year = dates.getFullYear();
	const month = dates.getMonth() + 1;
	const date = dates.getDate();

	const dateChoosen = year +'-' + month + '-' + date;

	return {
	    type: types.SHIPMENT_SCHEDULE_DATE, 
	    date:dateChoosen,
	};	
}

export function setAssignmentTodayCourier(data){
	let {totalPickup, totalDelivery} = countTotalShipmentSchedule(data);
	return {
	    type: types.ASSIGNMENT_TODAY_COURIER, 
	    totalPickup,
	    totalDelivery,
	};	
}

export function setSearchOrderResult(data){
	return {
	    type: types.SEARCH_ORDER_RESULT, 
	    data,
	};
}

export function setSearchOrderLoadingStatus(loadingStatus){
	return {
	    type: types.SEARCH_ORDER_LOADING_STATUS, 
	    loadingStatus,
	};
}

export function setSearchOrderCategory(category){
	return {
	    type: types.SEARCH_ORDER_CATEGORY, 
	    category,
	};
}

export function setIncomingOrderList(data){
	let online = [];
	let pos = [];
	for(let i in data){

		if (data[i].online_flag) {
			online.push(data[i]);
		}
		else{
			pos.push(data[i]);
		}
	}

	return{
		type: types.INCOMING_ORDER_LIST, 
	    online,
	    pos,
	};
}

export function setPickupOrderList(data){
	return{
		type: types.PICKUP_ORDER_LIST, 
	    data,
	};
}

export function setInProgressOrderList(data){
	return{
		type: types.IN_PROGRESS_ORDER_LIST, 
	    data,
	};
}

export function setLateInProgressOrderList(data){
	let result = [];
	try{
		for(let i in data){
			let shipment = getShipment('delivery', data[i]);

			let {shipmentschedule} = shipment;
		    let {shipment_date, time} = shipmentschedule;
		    let {info} = time;
		    //14.00-15.00 -> 14.01 dianggap telat
		    let timeDelivery2 = info.split('-')[0];
		    let timeDelivery = timeDelivery2.split(':');

		    let deliveryDate = new Date(shipment_date);

		    deliveryDate.setHours(timeDelivery[0]);
		    deliveryDate.setMinutes(timeDelivery[1]);

		    let currentDate = new Date();

			if (deliveryDate.getTime() < currentDate.getTime()) {
				result.push(data[i]);
			}
		    // let reportDate = new Date(deliveryDate);

		    // reportDate.setHours(timeDelivery[0] - 6);
		    // reportDate.setMinutes(timeDelivery[1]);
		}
	}
	catch(error){
		console.log(error);
	}

	return{
		type: types.LATE_IN_PROGRESS_ORDER_LIST, 
	    data:result,
	};
}

export function setLatePickupOrderList(data){
	let result = [];
	try{
		for(let i in data){
			let shipment = getShipment('pickup', data[i]);

			let {shipmentschedule} = shipment;
		    let {shipment_date, time} = shipmentschedule;
		    let {info} = time;
		    //14.00-15.00 -> 15.01 dianggap telat
		    let timeDelivery2 = info.split('-')[1];
		    let timeDelivery = timeDelivery2.split(':');

		    let deliveryDate = new Date(shipment_date);

		    deliveryDate.setHours(timeDelivery[0]);
		    deliveryDate.setMinutes(timeDelivery[1]);

		    let currentDate = new Date();
			if (deliveryDate.getTime() < currentDate.getTime()) {
				result.push(data[i]);
			}
		    // let reportDate = new Date(deliveryDate);

		    // reportDate.setHours(timeDelivery[0] - 6);
		    // reportDate.setMinutes(timeDelivery[1]);
		}
	}
	catch(error){
		console.log(error);
	}

	return{
		type: types.LATE_PICKUP_ORDER_LIST, 
	    data:result,
	};
}

export function setLateDeliveryOrderList(data){
	let result = [];
	try{
		for(let i in data){
			let shipment = getShipment('delivery', data[i]);

			let {shipmentschedule} = shipment;
		    let {shipment_date, time} = shipmentschedule;
		    let {info} = time;
		    //14.00-15.00 -> 15.01 dianggap telat
		    let timeDelivery2 = info.split('-')[1];
		    let timeDelivery = timeDelivery2.split(':');

		    let deliveryDate = new Date(shipment_date);

		    deliveryDate.setHours(timeDelivery[0]);
		    deliveryDate.setMinutes(timeDelivery[1]);

		    let currentDate = new Date();

			if (deliveryDate.getTime() < currentDate.getTime()) {
				result.push(data[i]);
			}
		    // let reportDate = new Date(deliveryDate);

		    // reportDate.setHours(timeDelivery[0] - 6);
		    // reportDate.setMinutes(timeDelivery[1]);
		}
	}
	catch(error){
		console.log(error);
	}

	return{
		type: types.LATE_DELIVERY_ORDER_LIST, 
	    data:result,
	};
}

export function setDeliveryOrderList(data){
	return{
		type: types.DELIVERY_ORDER_LIST, 
	    data,
	};
}

export function setSelectedOrderDate(date){
	return{
		type: types.SELECTED_ORDER_DATE, 
	    date,
	};
}

export function setComplaintOrderList(data){
	let online = [];
	let pos = [];

	for(let i in data){
		if (data[i].online_flag) {
			online.push(data[i]);
		}
		else{
			pos.push(data[i]);
		}
	}

	return{
		type: types.COMPLAINT_ORDER_LIST, 
	    online,
	    pos,
	};
}

export function setHistoryOrderList(data){
	let online = [];
	let pos = [];

	for(let i in data){
		if (data[i].online_flag) {
			online.push(data[i]);
		}
		else{
			pos.push(data[i]);
		}
	}

	return{
		type: types.HISTORY_ORDER_LIST, 
	    online,
	    pos,
	};
}

export function _setHistoryOrderDate(fromDate, toDate){
	return{
		type: types.HISTORY_ORDER_DATE, 
	    fromDate,
	    toDate,
	};
}

export function setOrderListData(data){
	return{
		type: types.ORDER_LIST_DATA, 
	    data,
	};
}

export function setComplaintMessageList(data){
	return{
		type: types.COMPLAINT_MESSAGE_LIST, 
	    data,
	};
}

export function setOrderListTypeTab(data){
	return{
		type: types.ORDER_LIST_TYPE_TAB, 
	    data,
	};
}

export function setSummaryOrder(incoming, pickup, inProgress, delivery, complaint, pickupLate, inProgressLate, deliveryLate, isComplaintImportant, isNotificationImportant){
	return{
		type: types.ORDER_SUMMARY, 
		incoming,
	    pickup,
	    inProgress,
	    delivery,
	    complaint,
	    pickupLate,
	    inProgressLate,
	    deliveryLate,
	    isComplaintImportant,
	    isNotificationImportant,
	};
}



function countTotalShipmentSchedule(data){
	let result = {
		totalPickup:0,
		totalDelivery:0,
	};

	for(let i in data){
		let {logschedule} = data[i];
		for(let j in logschedule){
			let {order} = logschedule[j];

			let {shipments} = order;

			let shipmentData = [];
			for(let k in shipments){
				if (shipments[k].id == logschedule[j].shipment_id) {
					shipmentData = shipments[k];
				}
			}

			let {shipment_type_id} = shipmentData;

			if (shipment_type_id == CONSTANT.SHIPMENT_TYPE.PICKUP) {
				result.totalPickup += 1;
			}
			else if (shipment_type_id == CONSTANT.SHIPMENT_TYPE.DELIVERY) {
				result.totalDelivery += 1;
			}
		}
	}
	
	return result;
}

