import * as types from './../actionTypes';

import * as actions from './../../actions';

import * as STRING from './../../string';
import * as CONSTANT from './../../constant';
import * as API from './../../api';
import * as COMPONENT from 'app/data/components';

export function getMembershipPackage(){

	return async function(dispatch, getState){
		
		let userData = getState().rootReducer.user.data;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;

		let packageList = [];

		if (indexActiveOutlet != null) {
			await API.Membership.getMembershipPackage(outletList[indexActiveOutlet].id, userData.accessToken)
			.then(async (response) => {

				let {status, error, data} = response;

	    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
	    			
	    			data.map((obj_data) => {
	    				let obj = {
	    					outlet_id: obj_data.outlet_id,
	    					promo_id: obj_data.promo_id,
	    					package_id: obj_data.id,
	    					package_name: obj_data.package_name,
	    					package_price: obj_data.package_price,
	    					total_voucher: obj_data.total_voucher,
	    					max_discount_value: obj_data.promo.promoorder.max_discount_value,
	    					max_discount_percentage: obj_data.promo.promoorder.max_discount_percentage,
	    					terms_and_conditions: obj_data.promo.terms_and_conditions,
	    					duration: obj_data.duration
	    				}

	    				packageList.push(obj);
	    			})
	    		}
				else{
					throw new Error('API ERROR : ' +  status.code);
				}
			})
			.catch((error) => {
				console.log(error, '--getMembershipPackage');

	    		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
					dispatch(actions.root.showConnectionError());
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}
			});
		}
		await dispatch(setMembershipPackages(packageList));
	}
}

export function searchCustomer(type, search){

	return async function(dispatch, getState){
		
		let userData = getState().rootReducer.user.data;

		await API.Membership.searchCustomer(type, search,  userData.accessToken)
		.then(async (response) => {

			let {status, error, data} = response;
			let searchedCustomer = [];

    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
    			
    			data.map((obj_data) => {
    				if(obj_data.customer !== null){
    					let obj = {
		    				user_id: obj_data.id,
		    				name: obj_data.name,
		    				email: obj_data.email,
		    				mobile_phone: obj_data.mobile_phone,
		    				customer_id: obj_data.customer.id
		    			}

		    			searchedCustomer.push(obj);
    				}
    			})

    			await dispatch(setSearchedCustomer(searchedCustomer));

    		}
			else{
				throw error;
			}
		})
		.catch((error) => {
			console.log(error, '--searchCustomer');

    		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
				dispatch(actions.root.showConnectionError());
			}
			else{
				throw error
			}
		});
	}
}

export function membershipApply(customer, membershipPackage, navigator){

	return async function(dispatch, getState){

		let userData = getState().rootReducer.user.data;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;

		await API.Membership.membershipApply(customer.customer_id, outletList[indexActiveOutlet].id, membershipPackage.package_id, userData.accessToken)
		.then(async (response) => {

			let {status, error, data} = response;
			
    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {

    			dispatch(actions.navigator.push(navigator, CONSTANT.ROUTE_TYPE.ORDER_MEMBERSHIP_SUCCESS, {
    				outlet: outletList[indexActiveOutlet],
    				customer: customer,
    				membershipPackage: membershipPackage,
    				transaction_hash: data.transaction_hash
    			}));
    		}
			else{
				throw new Error('API ERROR : ' +  status.code);
			}
		})
		.catch((error) => {
			console.log(error, '--membershipApply');

    		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
				dispatch(actions.root.showConnectionError());
			}
			else{
				dispatch(actions.root.showErrorMessage(error.message));
			}
		});
	}
}

export function membershipStore(params, navigator, props_function = {}){

	return async function(dispatch, getState){

		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:STRING.LOADING_MODAL.CREATE_MEMBERSHIP}));
		let userData = getState().rootReducer.user.data;

		await API.Membership.membershipStore(params, userData.accessToken)
		.then(async (response) => {

			let {status, error, data} = response;
			console.log('response: ',response);
    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
    			
    			dispatch(actions.navigator.dismissModal());
    			props_function();

    			setTimeout(() => {
    				dispatch(actions.navigator.pop(navigator));
    			}, 500)
    		}
			else{
				throw new Error('API ERROR : ' +  status.code);
			}
		})
		.catch((error) => {
			console.log(error, '--membershipStore');

    		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
				dispatch(actions.root.showConnectionError());
			}
			else{
				dispatch(actions.root.showErrorMessage(error.message));
			}
		});
	}
}

export function membershipUpdate(params, navigator, props_function = {}){

	return async function(dispatch, getState){

		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:STRING.LOADING_MODAL.UPDATE_MEMBERSHIP}));
		let userData = getState().rootReducer.user.data;

		await API.Membership.membershipUpdate(params, userData.accessToken)
		.then(async (response) => {

			let {status, error, data} = response;

			console.log(response)

    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
    			
    			dispatch(actions.navigator.dismissModal());
    			props_function();

    			setTimeout(() => {
    				dispatch(actions.navigator.pop(navigator));
    			}, 500)
    		}
			else{
				throw new Error('API ERROR : ' +  status.code);
			}
		})
		.catch((error) => {
			console.log(error, '--membershipUpdate');

    		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
				dispatch(actions.root.showConnectionError());
			}
			else{
				dispatch(actions.root.showErrorMessage(error.message));
			}
		});
	}
}

export function setSearchedCustomer(data){

	return {
	    type: types.SET_SEARCHED_CUSTOMER, 
	    data,
	};
}

export function setMembershipCustomer(data){

	return {
	    type: types.SET_MEMBERSHIP_CUSTOMER, 
	    data,
	};
}

export function setMembershipPackages(data){

	return {
	    type: types.SET_MEMBERSHIP_PACKAGES,
	    data,
	};
}

function getDate(datetime){
	let x = datetime.split(" ");
	let date = x[0];

	return COMPONENT.DateFormat(date, 'shortDate2');
}