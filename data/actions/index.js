import * as user from "./user";
import * as root from "./root";
import * as authentication from "./authentication";
import * as navigator from "./navigator";
import * as order from "./order";
import * as map from "./map";
import * as location from "./location";
import * as masterData from "./master_data";
import * as layout from "./layout";
import * as pushNotif from "./push_notif";
import * as complaint from "./complaint";
import * as pos from "./pos";
import * as attendance from "./attendance";
import * as uploadImage from "./uploadImage";
import * as deposit from "./deposit";
import * as membership from "./membership";
import * as customer from "./customer";
import * as validation from "./validation";
import * as bluetoothDevice from './bluetooth_device';
import * as posOldVersion from './pos_old_version';
import * as expenses from './expenses';

export {
    user,
    root,
    authentication,
    navigator,
    order,
    map,
    location,
    masterData,
    layout,
    pushNotif,
    complaint,
    pos,
    attendance,
    uploadImage,
    deposit,
    membership,
    customer,
    validation,
    bluetoothDevice,
    posOldVersion,
    expenses,
};
