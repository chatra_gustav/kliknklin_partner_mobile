import * as types from "app/data/actions/actionTypes";

import * as actions from "app/data/actions";

import * as STRING from "app/data/string";
import * as CONSTANT from "app/data/constant";
import { toCurrency } from "app/data/components";
import * as API from "app/data/api";

export function getOutletItemList() {
    return async function(dispatch, getState) {
        let { outletList, indexActiveOutlet } = getState().rootReducer.user.outlet;
        let userData = getState().rootReducer.user.data;

        let result = [];

        if (indexActiveOutlet != null) {
            let outletID = outletList[indexActiveOutlet].id;
            await API.POS._getOutletItem(outletID, userData.accessToken)
                .then(async response => {
                    let { status, error, data } = response;
                    if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
                        result = data;
                    } else {
                        throw new Error("API ERROR : " + status.code);
                    }
                })
                .catch(error => {
                    dispatch(actions.navigator.dismissModal());
                    console.log(error);
                });
        }

        dispatch(setOutletItem(result));

        return result;
    };
}

export function getPaymentMethodList() {
    return async function(dispatch, getState) {
        return [{ id: CONSTANT.PAYMENT_METHOD.CASH, category: "Cash" }];
    };
}

export function validatePromo(promo_code, orderPrice) {
    return async function(dispatch, getState) {
        dispatch(
            actions.navigator.showModal("PartnerKliknKlin.LoadingModal", {
                loadingText: "Melakukan validasi.."
            })
        );

        let customerID = getState().rootReducer.pos.selectedCustomer.customer_id;
        let accessToken = getState().rootReducer.user.data.accessToken;

        API.POS._validatePromo(promo_code, customerID, accessToken)
            .then(response => {
                dispatch(actions.navigator.dismissAllModals());

                let { status, error, data } = response;
                console.log(response, promo_code, customerID);

                if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
                    let selectedPromo = {
                        promo_code: promo_code
                    };

                    if (data.valid) {
                        let discountPrice = 0;

                        if (
                            orderPrice *
                                data.promo.promoorder.max_discount_percentage *
                                0.01 >=
                            data.promo.promoorder.max_discount_value
                        ) {
                            discountPrice = data.promo.promoorder.max_discount_value;
                        } else {
                            discountPrice =
                                orderPrice *
                                data.promo.promoorder.max_discount_percentage *
                                0.01;
                        }

                        selectedPromo.promo_id = data.promo.id;
                        selectedPromo.discount_price = discountPrice;
                        selectedPromo.promo_source = data.promo.promosource.info;
                    }

                    Object.assign(selectedPromo, data);

                    dispatch(setSelectedPromo(selectedPromo));
                } else {
                    throw new Error("API ERROR : " + status.code);
                }
            })
            .catch(error => {
                dispatch(actions.navigator.dismissAllModals());

                console.log(error);
            });
    };
}

export function submitOrderPOS(navigator) {
    return async function(dispatch, getState) {
        let result = false;

        //user
        let { accessToken } = getState().rootReducer.user.data;
        let { origin_id } = getState().rootReducer.user.data;

        //customer
        let customer = getState().rootReducer.pos.selectedCustomer;
        if (customer.id) {
            customer.customer_status = "existing";
            if (!customer.customer_id) {
                customer.customer_id = customer.customer ? customer.customer.id : null;
            }
        } else {
            customer.customer_status = "new";
            customer.origin_id = origin_id;
        }

        let order = {};

        let { outletList, indexActiveOutlet } = getState().rootReducer.user.outlet;

        order.outlet_id = outletList[indexActiveOutlet].id;
        order.area_3_id = outletList[indexActiveOutlet].area3.id;
        order.origin_id = origin_id;

        let payment = getState().rootReducer.pos.selectedPayment;
        payment.payment_method_id = payment.id;
        payment.pay_now = payment.isPayNow;

        let order_items = getState().rootReducer.pos.selectedItem;
        let extractedOrderItems = [];

        for (let i in order_items) {
            let obj = {};
            obj.item_price = order_items[i].price;
            obj.item_duration = order_items[i].selectedOutletItemWashingTime.duration;
            obj.quantity = order_items[i].quantity;
            obj.notes = order_items[i].note ? order_items[i].note : "";
            obj.outlet_item_washing_time_id = order_items[i].outletItemWashingTimeID;

            extractedOrderItems.push(obj);
        }

        order_items = [...extractedOrderItems];

        //promo
        let promo = getState().rootReducer.pos.selectedPromo;

        let obj = getState().rootReducer.pos.orderCreated;
        obj.showCreated = true;
        obj.data = {
            customer,
            order,
            order_items,
            payment,
            promo,
            change: -1
        };

        let { totalPrice } = getState().rootReducer.pos;
        let paidPrice = totalPrice;

        if (promo) {
            paidPrice -= promo.discount_price;
        }

        if (payment.payment_method_id == 1 && payment.pay_now) {
            await dispatch(
                showPaymentBox(
                    "pay-now",
                    async value => {
                        dispatch(
                            actions.navigator.showModal("PartnerKliknKlin.LoadingModal", {
                                loadingText: "Menunggu.."
                            })
                        );

                        obj.data.change = value - paidPrice;

                        result = await dispatch(createOrder(obj, accessToken, navigator));
                    },
                    "Jumlah yang harus dibayarkan : " + toCurrency(paidPrice, "Rp "),
                    paidPrice
                )
            );
        } else {
            result = await dispatch(createOrder(obj, accessToken, navigator));
        }

        return result;
    };
}

export function showPaymentBox(
    type = "pay-later",
    onPress,
    notification = "",
    paidPrice
) {
    return async function(dispatch, getState) {
        await dispatch(
            actions.navigator.showLightBox(CONSTANT.ROUTE_TYPE.BOX_SINGLE_INPUT, {
                title: "Jumlah Uang Pelanggan",
                onPressButtonPrimary: onPress,
                buttonPrimaryText: type == "pay-later" ? "Selesaikan" : "Buat",
                placeholder: "masukkan uang pelanggan",
                notification,
                leftIcon: CONSTANT.IMAGE_URL.PAYMENT_ICON,
                validation:async value => {
                    if(!await dispatch(
                                actions.validation.isNumberPositive(parseInt(value))
                        )){
                        return (
                            "Jumlah hanya berupa angka dan bernilai positif"
                        );
                    }
                    else if (parseInt(value) < parseInt(paidPrice)) {
                        return (
                            "Uang yang dibayarkan tidak bisa kurang dari " +
                            toCurrency(paidPrice, "Rp")
                        );
                    } else {
                        return "";
                    }
                }
            })
        );
    };
}

export function createOrder(orderCreatedData, accessToken, navigator) {
    return async function(dispatch, getState) {
        let result = false;
        console.log(orderCreatedData);
        await API.POS._createOrder(orderCreatedData.data, accessToken)
            .then(response => {
                dispatch(actions.navigator.dismissAllModals());

                let { status, error, data } = response;
                if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
                    result = true;
                    dispatch(actions.customer.getCustomerOutletList());
                    dispatch(setOrderCreatedData(orderCreatedData));

                    dispatch(actions.order.setOrderActive(data));
                    dispatch(actions.order.setShipmentActiveDelivery(data));

                    dispatch(
                        actions.navigator.resetTo(
                            navigator,
                            CONSTANT.ROUTE_TYPE.ORDER_CONTENT,
                            {
                                overrideBackPress: true,
                                routeType: CONSTANT.ORDER_ACTIVE_TYPE.INCOMING
                            }
                        )
                    );

                    setTimeout(() => {
                        dispatch(
                            actions.navigator.showLightBox(
                                CONSTANT.ROUTE_TYPE.BOX_ORDER_CREATED,
                                {
                                    change: orderCreatedData.data.change,
                                    overrideBackPress: true
                                }
                            )
                        );
                    }, 150);
                } else {
                    dispatch(
                        actions.navigator.showLightBox("PartnerKliknKlin.BoxTypeB", {
                            title: "Perhatian",
                            message: error,
                            buttonPrimaryText: "Ya",
                            onPressButtonPrimary: () => {
                                dispatch(actions.navigator.dismissModal());
                                dispatch(actions.navigator.dismissLightBox());
                            }
                        })
                    );

                    let errors = new Error("API ERROR : " + status.code + " | " + error);
                    errors.name = "api";
                    throw errors;
                }
            })
            .catch(error => {
                dispatch(actions.navigator.dismissModal());

                if (error.name != "api") {
                    dispatch(
                        actions.navigator.showLightBox("PartnerKliknKlin.BoxTypeB", {
                            title: "Perhatian",
                            message: "Server Sedang Bermasalah",
                            buttonPrimaryText: "Ya",
                            onPressButtonPrimary: () => {
                                dispatch(actions.navigator.dismissModal());
                                dispatch(actions.navigator.dismissLightBox());
                            }
                        })
                    );
                }

                console.log(error);
            });

        return result;
    };
}

export function testing(navigator) {
    return async function(dispatch, getState) {
        dispatch(
            actions.navigator.push(navigator, CONSTANT.ROUTE_TYPE.ORDER_CONTENT, {
                overrideBackPress: true,
                route: CONSTANT.ROUTE_TYPE.ORDER_LIST,
                routeType: "history"
            })
        );
    };
}

export function updatePayment(orderId, navigator) {
    return async function(dispatch, getState) {
        let userData = getState().rootReducer.user.data;

        dispatch(
            actions.navigator.showModal("PartnerKliknKlin.LoadingModal", {
                loadingText: "Mohon tunggu.."
            })
        );

        API.POS._updatePayment(orderId, userData.accessToken)
            .then(response => {
                dispatch(actions.navigator.dismissModal());

                let { status, error, data } = response;

                if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
                    dispatch(actions.order.setOrderActive(data));
                } else {
                    throw new Error("API ERROR : " + status.code);
                }
            })
            .catch(error => {
                dispatch(actions.navigator.dismissModal());
                console.log(error);
            });
    };
}

export function showWashingServiceSelection(item, onSuccess = () => {}) {
    return async function(dispatch, getState) {
        let data = item.outletItemWashingTime;
        let extractedData = [];
        for (let i in data) {
            let isExist = false;
            let duration = Number.MAX_SAFE_INTEGER;

            for (let j in extractedData) {
                if (extractedData[j].washingServiceID == data[i].washingServiceID) {
                    isExist = true;

                    if (extractedData[j].duration < data[i].duration) {
                        extractedData[j] = data[i];
                    }
                }
            }

            if (!isExist) {
                extractedData.push(data[i]);
            }
        }

        if (extractedData.length == 1) {
            onSuccess(data[0]);
        } else {
            let extractedItem = [];
            const sContent = { items: [] };
            for (let section = 0; section < extractedData.length; ++section) {
                sContent.items.push(extractedData[section]);
            }
            extractedItem.push(sContent);

            dispatch(
                actions.navigator.showLightBox(
                    CONSTANT.ROUTE_TYPE.BOX_ITEM_SELECTION_DETAILED,
                    {
                        data: extractedItem,
                        title: "Pilih Metode Pencucian",
                        itemTitle: item.name,
                        itemDescription: item.description,
                        rowDescriptionCategory: "washingServiceDescription",
                        rowInfoCategory: "washingServiceInfo",
                        rowPriceCategory: "price",
                        isShowConfirmation: false,
                        onPressButtonPrimary: selectedOutletItemWashingTime => {
                            onSuccess(selectedOutletItemWashingTime);
                            //dispatch(addSelectedItem(item, selectedOutletItemWashingTime, 1));
                        },
                        onPressButtonSecondary: () => {
                            dispatch(showWashingServiceSelection(item));
                        }
                    }
                )
            );
        }
    };
}

export function calculateAvailableDuration(
    { durationList, durationReferralList },
    { id, outletItemWashingTime },
    selectedOutletItemWashingTime,
    calculateType
) {
    return async function(dispatch, getState) {
        let result = {};
        let addDurationList = [];
        let substractDurationList = [];

        if (calculateType == "add") {
            let minimumDurationOfItem = Number.MAX_SAFE_INTEGER;

            for (let i in outletItemWashingTime) {
                if (
                    selectedOutletItemWashingTime.washingServiceID ==
                    outletItemWashingTime[i].washingServiceID
                ) {
                    if (outletItemWashingTime[i].duration < minimumDurationOfItem) {
                        minimumDurationOfItem = outletItemWashingTime[i].duration;
                    }

                    let isInsert = true;

                    for (let j in durationList) {
                        if (
                            durationList[j].duration == outletItemWashingTime[i].duration
                        ) {
                            isInsert = false;
                            durationList[j].itemLinked += 1;
                        }
                    }

                    if (isInsert) {
                        let obj = {
                            duration: outletItemWashingTime[i].duration,
                            itemLinked: 1
                        };
                        durationList.push(obj);
                    }
                }
            }

            durationReferralList.push({
                duration: minimumDurationOfItem,
                id
            });

            result.durationList = [...durationList];
            result.durationReferralList = durationReferralList;
        } else if (calculateType == "substract") {
            let minimumDurationOfItem = Number.MAX_SAFE_INTEGER;

            for (let i in outletItemWashingTime) {
                if (
                    outletItemWashingTime[i].washingServiceID ==
                    selectedOutletItemWashingTime.washingServiceID
                ) {
                    if (outletItemWashingTime[i].duration < minimumDurationOfItem) {
                        minimumDurationOfItem = outletItemWashingTime[i].duration;
                    }

                    let indexDeletedDurationList = [];
                    for (let j in durationList) {
                        if (
                            durationList[j].duration == outletItemWashingTime[i].duration
                        ) {
                            let { duration } = durationList[j];

                            durationList[j].itemLinked -= 1;

                            if (durationList[j].itemLinked == 0) {
                                indexDeletedDurationList.push(parseInt(j));
                            }
                        }
                    }

                    durationList = durationList.filter(
                        (value, index) => !indexDeletedDurationList.includes(index)
                    );
                }
            }

            for (let i in durationReferralList) {
                if (durationReferralList[i].id == id) {
                    durationReferralList.splice(i, 1);
                }
            }

            result.durationList = durationList;
            result.durationReferralList = durationReferralList;
        }

        result.durationList.sort((a, b) => {
            return b.duration - a.duration;
        });

        let durationReferral = 0;

        for (let i in result.durationReferralList) {
            if (result.durationReferralList[i].duration > durationReferral) {
                durationReferral = result.durationReferralList[i].duration;
            }
        }

        result.durationReferral = durationReferral;

        return result;
    };
}

export function addSelectedItem(data, selectedOutletItemWashingTime, quantity) {
    return async function(dispatch, getState) {
        //dicari dulu dah ada belum
        //ada ditambah
        //ga ada buat baru
        let item = {};
        item.itemID = data.id;
        item.outletItemWashingTimeID =
            selectedOutletItemWashingTime.outletItemWashingTimeID;
        item.quantity = quantity;
        item.price = selectedOutletItemWashingTime.price;
        item.unitName = data.unitName;
        item.name = data.name;
        item.description = data.description;

        let selectedItemList = JSON.parse(
            JSON.stringify(getState().rootReducer.pos.selectedItem)
        );

        let availableDurationList = getState().rootReducer.pos.availableDuration;

        let isNewItem = true;

        for (let index in selectedItemList) {
            if (
                selectedItemList[index].outletItemWashingTimeID ==
                selectedOutletItemWashingTime.id
            ) {
                selectedItemList[index].quantity++;
                isNewItem = false;
            }
        }

        if (isNewItem) {
            selectedItemList.push({
                itemID: item.itemID,
                outletItemWashingTimeID: selectedOutletItemWashingTime.id,
                quantity: item.quantity,
                price: item.price,
                unitName: item.unitName,
                name: item.name,
                description: item.description,
                item: data,
                itemTags: [],
                selectedOutletItemWashingTime
            });

            dispatch(
                setAvailableDuration(
                    await dispatch(
                        calculateAvailableDuration(
                            availableDurationList,
                            data,
                            selectedOutletItemWashingTime,
                            "add"
                        )
                    )
                )
            );
        }

        let selectedDuration = getState().rootReducer.pos.availableDuration;
        console.log(selectedDuration);
        dispatch(setSelectedDuration(selectedDuration.durationList[0].duration));

        dispatch(calculateTotalPrice(selectedItemList));

        dispatch(setSelectedItem(selectedItemList));
    };
}

export function substractSelectedItem(data, selectedOutletItemWashingTime, quantity) {
    return async function(dispatch, getState) {
        //dicari dulu dah ada belum
        //ada dikurang
        //quantity 0 dihapus

        let selectedItemList = [...getState().rootReducer.pos.selectedItem];
        let availableDurationList = getState().rootReducer.pos.availableDuration;

        let isRemoveItem = true;

        for (let index in selectedItemList) {
            if (
                selectedItemList[index].outletItemWashingTimeID ==
                selectedOutletItemWashingTime.id
            ) {
                selectedItemList[index].quantity--;
                if (selectedItemList[index].quantity == 0) {
                    selectedItemList.splice(index, 1);

                    dispatch(
                        setAvailableDuration(
                            await dispatch(
                                calculateAvailableDuration(
                                    availableDurationList,
                                    data,
                                    selectedOutletItemWashingTime,
                                    "substract"
                                )
                            )
                        )
                    );
                }
            }
        }

        let selectedDuration = getState().rootReducer.pos.availableDuration;

        if (selectedDuration.durationList.length == 0) {
            dispatch(setSelectedDuration(0));
        } else {
            dispatch(setSelectedDuration(selectedDuration.durationList[0].duration));
        }

        dispatch(calculateTotalPrice(selectedItemList));

        dispatch(setSelectedItem(selectedItemList));
    };
}

export function changeDurationToRegular() {
    return async function(dispatch, getState) {
        let selectedItemList = JSON.parse(
            JSON.stringify(getState().rootReducer.pos.selectedItem)
        );
        let selectedDuration = getState().rootReducer.pos.availableDuration;
        let baseList = CONSTANT.BASE_ITEM_CATEGORY_POS.LIST;

        let isNeedToChange = false;

        for (let i in selectedItemList) {
            let { item, selectedOutletItemWashingTime } = selectedItemList[i];
            let { outletItemWashingTime } = item;

            let outletItemWashingTimeLongestDuration = {};
            let longestDuration = 0;

            for (let j in outletItemWashingTime) {
                if (
                    selectedOutletItemWashingTime.washingServiceID ==
                        outletItemWashingTime[j].washingServiceID &&
                    outletItemWashingTime[j].duration > longestDuration
                ) {
                    longestDuration = outletItemWashingTime[j].duration;
                    outletItemWashingTimeLongestDuration = outletItemWashingTime[j];
                }
            }

            if (longestDuration != selectedOutletItemWashingTime.duration) {
                selectedItemList[
                    i
                ].selectedOutletItemWashingTime = outletItemWashingTimeLongestDuration;
                selectedItemList[i].price = outletItemWashingTimeLongestDuration.price;
                selectedItemList[i].outletItemWashingTimeID =
                    outletItemWashingTimeLongestDuration.id;

                isNeedToChange = true;
            }
        }

        if (selectedItemList.length != 0 && isNeedToChange) {
            dispatch(
                actions.navigator.showLightBox("PartnerKliknKlin.BoxTypeB", {
                    title: "Informasi",
                    message: "Waktu produk dikembalikan ke regular time",
                    buttonPrimaryText: "Ya",
                    onPressButtonPrimary: () => {
                        dispatch(actions.navigator.dismissModal());
                        dispatch(actions.navigator.dismissLightBox());
                    }
                })
            );

            dispatch(setSelectedItem(selectedItemList));
            dispatch(calculateTotalPrice(selectedItemList));

            dispatch(setSelectedDuration(selectedDuration.durationList[0].duration));
        }
    };
}

export function resetPOSData() {
    return async function(dispatch, getState) {
        dispatch(setSelectedItem([]));
        dispatch(setSelectedCustomer(null));
        dispatch(setSelectedPayment({}));
        dispatch(setTotalPrice(0));
        dispatch(
            setOrderCreatedData({
                showCreated: false,
                data: {}
            })
        );
        dispatch(setSelectedPromo(null));
        dispatch(
            setAvailableDuration({
                durationList: [],
                durationReferralList: [],
                durationReferral: 0
            })
        );
        dispatch(setSelectedDuration(0));
        dispatch(setSelectedDeliveryMethod(0));
    };
}

export function setItemNote(note, itemID) {
    return async function(dispatch, getState) {
        let selectedItemList = getState().rootReducer.pos.selectedItem;

        for (let i in selectedItemList) {
            if (selectedItemList[i].itemID == itemID) {
                selectedItemList[i].note = note;
            }
        }

        dispatch(setSelectedItem(selectedItemList));
    };
}

export function calculateTotalPrice(selectedItemList) {
    return async function(dispatch, getState) {
        //iterasi,
        //tambah price x quantity
        let totalPrice = 0;

        for (let index in selectedItemList) {
            totalPrice +=
                selectedItemList[index].price * selectedItemList[index].quantity;
        }

        dispatch(setTotalPrice(totalPrice));
    };
}

export function findSelectedItemData(itemID) {
    return async function(dispatch, getState) {
        let selectedItemList = getState().rootReducer.pos.selectedItem.slice();

        for (let i in selectedItemList) {
            if (selectedItemList[i].itemID == itemID) {
                return selectedItemList[i];
            }
        }

        return null;
    };
}

export function setOutletItem(outletItemList) {
    return {
        type: types.POS_OUTLET_ITEM,
        data: outletItemList
    };
}

export function setExtractedOutletItem(extractedItemList) {
    return {
        type: types.POS_EXTRACTED_OUTLET_ITEM,
        data: extractedItemList
    };
}

export function setSelectedItem(selectedItemList) {
    return {
        type: types.POS_SELECTED_ITEM,
        data: selectedItemList
    };
}

export function setSelectedPayment(data) {
    return {
        type: types.POS_SELECTED_PAYMENT,
        data
    };
}

export function setSelectedPromo(data) {
    return {
        type: types.POS_SELECTED_PROMO,
        data
    };
}

export function setSelectedCustomer(customer) {
    return {
        type: types.POS_SELECTED_CUSTOMER,
        data: customer
    };
}

export function setSelectedDuration(duration) {
    return {
        type: types.POS_SELECTED_DURATION,
        data: duration
    };
}

export function setSelectedDeliveryMethod(deliveryMethod) {
    return {
        type: types.POS_SELECTED_DELIVERY_METHOD,
        data: deliveryMethod
    };
}

export function setTotalPrice(totalPrice) {
    return {
        type: types.POS_TOTAL_PRICE,
        totalPrice
    };
}

export function setAvailableDuration(durationList) {
    return {
        type: types.POS_AVAILABLE_DURATION,
        data: durationList
    };
}

export function setOrderCreatedData(data) {
    return {
        type: types.POS_ORDER_CREATED_DATA,
        data
    };
}
