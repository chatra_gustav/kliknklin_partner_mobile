import {
  PermissionsAndroid,
  AppState,
  Platform,
} from 'react-native';


import * as types from './../actionTypes';
import * as STRING from './../../string';
import * as CONSTANT from './../../constant';
import * as API from './../../api';
import * as actions from './../../actions';

//import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';

import type {Notification, RemoteMessage, NotificationOpen} from 'react-native-firebase';
import firebase from 'react-native-firebase';

export function configPushNotif(navigator){
  return async function(dispatch, getState) {
      firebase.notifications().getInitialNotification()
      .then((notificationOpen: NotificationOpen) => {
        if (notificationOpen) { 
          const notification: Notification = notificationOpen.notification;
          dispatch(_handleOnNotificationOpened(notification, navigator));
          firebase.notifications().removeDeliveredNotification(notification.notificationId);
        }
      });

      await firebase.messaging().hasPermission()
      .then(async enabled => {
        if (enabled) {
          await dispatch(_getRegistrationToken())
            .then((response) =>{
                dispatch(displayNotification());
            })
            .catch((error) => {
            });
        } else {
          firebase.messaging().requestPermission()
          .then(() => {
              dispatch(configPushNotif());
          })
          .catch(error => {
            // User has rejected permissions  
          });
        } 
      });
  }
}

export function _onNotificationOpenedListener(navigator){
  return async function(dispatch, getState) {
    this._onNotificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
        const notification: Notification = notificationOpen.notification;
        dispatch(_handleOnNotificationOpened(notification, navigator));
        firebase.notifications().removeDeliveredNotification(notification.notificationId);
    });
  }
}

export function _removeOnNotificationOpenedListener(){
  return async function(dispatch, getState) {
    this._onNotificationOpenedListener();
  }
}

export function _handleOnNotificationOpened(notification, navigator){
  return async function(dispatch, getState) {
    let {email} = getState().rootReducer.user.data;
    let {notificationId, data} = notification;
    if (data.email && data.email == email) {
      if (notificationId == CONSTANT.NOTIFICATION_PAGE.ORDER_CONTENT) { 
        dispatch(actions.order.getOrderContent(data.orderid, navigator)); 
      }
      else if (notificationId == CONSTANT.NOTIFICATION_PAGE.ORDER_INCOMING) {
        dispatch(actions.navigator.resetTo(navigator, CONSTANT.ROUTE_TYPE.ORDER_LIST, {
            overrideBackPress:true,
            type:'incoming',
        }));
      }
    }
  }
}

export function _getRegistrationToken(){
  return async function(dispatch, getState) {
    await firebase.messaging().getToken()
    .then(async fcmToken => {
      if (fcmToken) {
        await dispatch(setPushNotifToken(fcmToken));
      } else {
        await dispatch(configPushNotif());
      } 
    });
  }
}

export function displayNotification(){
  return async function(dispatch, getState) {
    firebase.notifications().onNotification(async (notification: Notification) => {
      console.log(notification);
        let {data} = notification;
        let notification_id = data && data.notification_id ? data.notification_id : 'default-id';
        let title = data && data.title ? data.title : 'Title Text';
        let body = data && data.body ? data.body : 'Body Text';
        let subtitle = data && data.subtitle ? data.subtitle : 'Subtitle Text';

        const displayedNotification = new firebase.notifications.Notification({
          sound: 'default',
          show_in_foreground: true,
        })
        .setNotificationId(notification_id)
        .setTitle(title)
        .setSubtitle(subtitle)
        .setBody(body)
        .android.setSmallIcon('ic_launcher')
        .android.setLargeIcon('ic_launcher')
        .setData(data);

        if (Platform.OS === 'android') {
          const channel = new firebase.notifications.Android.Channel('default-channel', 'Default Channel', firebase.notifications.Android.Importance.High)
            .setDescription('Default Channel');

          firebase.notifications().android.createChannel(channel);

          displayedNotification.android
            .setPriority(firebase.notifications.Android.Priority.High)
            .android.setChannelId('default-channel')
            .android.setVibrate(1000);
        }

        await firebase.notifications().displayNotification(displayedNotification);
    });
  }
}


export function obsoloteconfigPushNotif(){
	return async function(dispatch, getState) {
	    FCM.requestPermissions(); // for iOS
	    
	    var data = {};
	    data.success = 'nope';
	    
	    await FCM.getFCMToken().then((token) => {
        dispatch(setPushNotifToken(token));
	       
	      //buka notif dari dead apps
	      FCM.getInitialNotification().then(notif=>{
	      	//console.log('haha');
          this._reloadDashboard();
          this._receivePushNotif(notif)
	      });
	    }); 

	    this.refreshTokenListener = await FCM.on(FCMEvent.RefreshToken, (token) => {
	        dispatch(setPushNotifToken(token));

	        FCM.getInitialNotification().then(notif=>{
	        	this._reloadDashboard();
	        	this._receivePushNotif(notif)
	        });
	    });

            FCM.on(FCMEvent.Notification, async (notif) => { 
            this._reloadDashboard();
            // there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
            if(notif.local_notification){
              //buka notif dari foreground
              //console.log(notif);
              //console.log('hehe');
              this._receivePushNotif(notif);
              return;
            }
            if(notif.opened_from_tray){
                //buka notif dari background
                //console.log(notif);
                //console.log('huhu');
                this._receivePushNotif(notif);
                return;
            }
            //await someAsyncCall();
            
            if(Platform.OS ==='ios'){
              //optional
              //iOS requires developers to call completionHandler to end notification process. If you do not call it your background remote notifications could be throttled, to read more about it see the above documentation link. 
              //This library handles it for you automatically with default behavior (for remote notification, finish with NoData; for WillPresent, finish depend on "show_in_foreground"). However if you want to return different result, follow the following code to override
              //notif._notificationType is available for iOS platfrom
              switch(notif._notificationType){
                case NotificationType.Remote:
                  notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
                  break;
                case NotificationType.NotificationResponse:
                  notif.finish();
                  break;
                case NotificationType.WillPresent:
                  notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
                  break;
              }
            }

            FCM.presentLocalNotification({
                //id: "UNIQ_ID_STRING",                               // (optional for instant notification)
                title: notif.fcm.title,                     // as FCM payload
                body: notif.fcm.body,                    // as FCM payload (required)
                sound: "default",                                   // as FCM payload
                priority: "high",                                   // as FCM payload
                click_action: notif.fcm.action,                             // as FCM payload
                badge: 10,                                          // as FCM payload IOS only, set 0 to clear badges
                number: 10,                                         // Android only
                //ticker: "My Notification Ticker",                   // Android only
                auto_cancel: true,                                  // Android only (default true)
                large_icon: "ic_launcher",                           // Android only
                icon: "ic_launcher",                                // as FCM payload, you can relace this with custom icon you put in mipmap
                //big_text: "bisa diubah juga",     // Android only
                //sub_text: "bisa diubah",                      // Android only
                //color: "red",                                       // Android only
                vibrate: 300,                                       // Android only default: 300, no vibration if you pass null
                //tag: notif.fcm.tag,                                    // Android only
                //group: "group",                                     // Android only
                //my_custom_data:'my_custom_field_value',             // extra data you want to throw
                lights: true,                                       // Android only, LED blinking (default false)
                show_in_foreground: true                               // notification when app is in foreground (local & remote)
            });   
        });
    };
}

function listenerPushNotif(){
	  
}

export function setPushNotifToken(token){
  return{
    type: types.PUSH_NOTIF_TOKEN,
    token,
  }
}

