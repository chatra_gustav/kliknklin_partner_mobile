import FusedLocation from 'react-native-fused-location';

import * as types from './../actionTypes';

import * as actions from './../../actions';
import * as CONSTANT from './../../constant';
import Geolocation from 'react-native-geolocation-service';
import Permissions from 'react-native-permissions';
import AndroidOpenSettings from 'react-native-android-open-settings';
import {Linking, Alert} from 'react-native';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';

export function checkLocationServicePermission(navigator, callback = () => {}){
	return async function (dispatch, getState){
		let response = false;

		await Permissions.check('location')
			.then(async responseCheck => {
				if (responseCheck == 'authorized') {
					response = true;
				}
				else{
					dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeA',{
				    	title:'Ijinkan KliknKlin mengakses lokasi anda', 
				    	message: 'Kami butuh mengakses lokasi anda untuk melanjutkan', 
				    	isDismissAllowed:false,
				    	buttonPrimaryText: 'ya', 
				    	buttonSecondaryText: 'batal', 
					    onPressButtonPrimary: () => {
					    	dispatch(actions.navigator.dismissLightBox());
							if (responseCheck == 'restricted') {
								AndroidOpenSettings.appDetailsSettings();
							}
							else{
								Permissions.request('location')
								.then(responseRequest => {
									console.log(responseCheck);
									if (responseRequest == 'authorized') {
										dispatch(callback);
										response = true;
									}
									else{
										dispatch(actions.navigator.dismissLightBox());
										dispatch(actions.navigator.popToRoot(navigator));
									}
							    }); 
							}
					    },
					    onPressButtonSecondary: () => {
							dispatch(actions.navigator.dismissLightBox());
							dispatch(actions.navigator.popToRoot(navigator)); 
					    },
					}));
				}
				
		    });
		
		return response; 
	}
}


export function getCurrentLocation(navigator){
	return async function (dispatch, getState){
		let result = null;

		try{

			console.log('start');
			
			await RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({interval: 10000, fastInterval: 5000})
			  .then(async data => {

				let resultLocaton = false;

				const timer = setTimeout(() => {
					if (!resultLocaton) {
						dispatch(getCurrentLocation());
					}
				}, 2000);

			    FusedLocation.setLocationPriority(FusedLocation.Constants.HIGH_ACCURACY);

		        await FusedLocation.getFusedLocation()
		        	.then((response) => {
		        		console.log(response);

		        		resultLocaton = true;

						result = response;

		                let orderActive = getState().rootReducer.order.active;

		        		if (Object.keys(orderActive).length != 0) {
							if (orderActive.orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.PICKUP) {
								let shipmentActivePickup = getState().rootReducer.order.shipmentActivePickup;
								
								try{
									let {coords} = response;
									let {shipmentschedule} = shipmentActivePickup;
									let {geolocation} = shipmentschedule;

									dispatch(actions.map.fetchDirectionGoogleMaps(response.latitude, response.longitude, geolocation.coordinates[1], geolocation.coordinates[0]));
								}
								catch(error){
									console.log(error);
								}
							}
							else if (orderActive.orderActiveType == CONSTANT.ORDER_ACTIVE_TYPE.DELIVERY) {
								let shipmentActiveDelivery = getState().rootReducer.order.shipmentActiveDelivery;
								
								try{
									let {coords} = response;
									let {shipmentschedule} = shipmentActiveDelivery;
									let {geolocation} = shipmentschedule;

									dispatch(actions.map.fetchDirectionGoogleMaps(response.latitude, response.longitude, geolocation.coordinates[1], geolocation.coordinates[0]));
								}
								catch(error){
									console.log(error);
								}
							}
		                }

						dispatch(actions.user.setUserLocation(response));
		        	})
		        	.catch((error) => {
						console.log(error,'elu ya');
		        	});
			  }).catch(err => {
			  	console.log(err, err.code);
			  	if (err.code == 'ERR00') {
			  		dispatch(getCurrentLocation(navigator));
			  	}
			    // The user has not accepted to enable the location services or something went wrong during the process
			    // "err" : { "code" : "ERR00|ERR01|ERR02", "message" : "message"}
			    // codes : 
			    //  - ERR00 : The user has clicked on Cancel button in the popup
			    //  - ERR01 : If the Settings change are unavailable
			    //  - ERR02 : If the popup has failed to open
			  });
		}
	    catch(error){
			console.log(error);
	    }

	    return result;
	}
}

