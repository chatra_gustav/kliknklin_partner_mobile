import * as types from './../actionTypes';

import * as actions from './../../actions';

import * as STRING from './../../string';
import * as CONSTANT from './../../constant';
import * as API from './../../api';

export function setItemSolution(complaintItemSolutionID, complaintItemSolutionInfo, complaintItemID){
	return async function(dispatch, getState){

		let orderActive = getState().rootReducer.order.active;
		
		let userData = getState().rootReducer.user.data;

		dispatch(actions.navigator.dismissLightBox());
    	dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));

		API.Complaint.setItemSolution(orderActive.data.complaint.id, complaintItemSolutionID, complaintItemID, userData.accessToken)
			.then(async (response) => {
				let {status, error, data} = response;
				console.log(response);

	    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
	    			dispatch(actions.order.getComplaintList())
	    				.then(async (response) => {
	    					let {complaintOrderList} = getState().rootReducer.order;
	    					let currentData = null;

	    					for(let i in complaintOrderList.online){
								if (complaintOrderList.online[i].complaint.id == orderActive.data.complaint.id) {
									currentData = complaintOrderList.online[i];
								}
	    					}

	    					for(let i in complaintOrderList.pos){
								if (complaintOrderList.pos[i].complaint.id == orderActive.data.complaint.id) {
									currentData = complaintOrderList.pos[i];
								}
	    					}


	    					if (currentData) {
	    						dispatch(setComplaintMessageList(currentData.complaint.messages));
								dispatch(actions.order.setOrderActive(currentData, CONSTANT.ORDER_ACTIVE_TYPE.COMPLAINT));
	    					}
	    					
	    					dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
						    	title:'Pemberian solusi barang berhasil', 
						    	message: 'Solusi barang saat ini : ' + complaintItemSolutionInfo,
						    	buttonPrimaryText: 'Ya', 
							    onPressButtonPrimary: () => {
							    	
							    	dispatch(actions.navigator.dismissModal());
									dispatch(actions.navigator.dismissLightBox());
							    },
							}));
	    				})
	    				.catch((error) => {
							console.log(error,'inside')
	    				});

			        }
				else{
					throw new Error('API ERROR : ' + status.code);
				}
			})
			.catch((error) => {
				console.log(error);
				dispatch(actions.navigator.dismissAllModals());

	    		setTimeout(
	    			() => alert(error)
	    		,200);
			});
	}
}


export function sendComplaintMessage(messages){
	return async function(dispatch, getState){
		let orderActive = getState().rootReducer.order.active;
		
		let userData = getState().rootReducer.user.data;

		API.Complaint.sendComplaintMessage(orderActive.data.complaint.id, messages.user._id, messages.text, userData.accessToken)
		.then(async (response) => {
			let {status, error, data} = response;
			dispatch(actions.navigator.dismissAllModals());

    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
    			dispatch(getComplaintMessageList(orderActive.data.complaint.id));
    		}
			else{
				console.log(error);
				throw new Error('API ERROR : ' + status.code);
			}
		})
		.catch((error) => {
			console.log(error);
			dispatch(actions.navigator.dismissModal());

    		setTimeout(
    			() => alert(error)
    		,200);
		});
	}
}

export function markComplaintSolved(){
	return async function(dispatch, getState){
		let orderActive = getState().rootReducer.order.active;
		
		let userData = getState().rootReducer.user.data;
		
		dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeA',{
	    	title:'konfirmasi', 
	    	message: 'apakah anda yakin untuk menyelesaikan komplain', 
	    	buttonPrimaryText: 'ya', 
	    	buttonSecondaryText: 'batal', 
		    onPressButtonPrimary: async () => {
				dispatch(actions.navigator.dismissLightBox());

				await API.Complaint.markComplaintSolved(orderActive.data.complaint.id, userData.accessToken)
					.then(async (response) => {
						let {status, error, data} = response;
						console.log(data);
			    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
			    			dispatch(actions.order.getComplaintList())
								.then(async (response) => {
									let {complaintOrderList} = getState().rootReducer.order;
									let currentData = {};
									
									for(let i in complaintOrderList.online){
										if (complaintOrderList.online[i].complaint.id == orderActive.data.complaint.id) {
											currentData = complaintOrderList.online[i];
										}
			    					}

			    					for(let i in complaintOrderList.pos){
										if (complaintOrderList.pos[i].complaint.id == orderActive.data.complaint.id) {
											currentData = complaintOrderList.pos[i];
										}
			    					}

			    					if (currentData) {
			    						await dispatch(setComplaintMessageList(currentData.complaint.messages));
										await dispatch(actions.order.setOrderActive(currentData, CONSTANT.ORDER_ACTIVE_TYPE.COMPLAINT));
			    					}

									let {currentComplaintStatus} = data;

									dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeB',{
								    	title:'Perubahan Status Komplain Berhasil', 
								    	message: 'Status komplain saat ini : ' + currentComplaintStatus.info,
								    	buttonPrimaryText: 'Ya', 
									    onPressButtonPrimary: () => {
									    	dispatch(actions.navigator.dismissModal());
											dispatch(actions.navigator.dismissLightBox());
									    },
									}));
								})
								.catch((error) => {
									console.log(error);
								});
						}
						else{
							console.log(error);
							throw new Error('API ERROR : ' + status.code);
						}
					})
					.catch((error) => {
						dispatch(actions.navigator.dismissModal());

			    		setTimeout(
			    			() => alert(error)
			    		,200);
		});
		    }, 
		    onPressButtonSecondary: () =>{
				dispatch(actions.navigator.dismissLightBox());
		    },
		}));
	}
}

export function getComplaintList(){
	return async function(dispatch, getState){
		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));

		let {accessToken} = getState().rootReducer.user.data;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;

		await API.Order._getComplaintList(outletList[indexActiveOutlet].id, accessToken)
			.then((response) => {
				let {status, error, data} = response;
				dispatch(actions.navigator.dismissAllModals());
				
        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
        			
        			dispatch(setComplaintOrderList(data));
        		}
				else{
					throw new Error('API ERROR : ' + status.code);
				}
			})
			.catch((error) => {
				console.log(error);
				dispatch(actions.navigator.dismissModal());

        		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
					dispatch(actions.root.showConnectionError(getComplaintList()));
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}
			});
	}
}

export function getComplaintMessageList(complaintID){
	return async function(dispatch, getState){
		let result = false;

		let {accessToken} = getState().rootReducer.user.data;
		
		await API.Order._getComplaintMessageList(complaintID, accessToken)
			.then(async (response) => {
				let {status, error, data} = response;
        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
        			dispatch(setComplaintMessageList(data));

        			result = true;
        		}
				else{
					console.log(error, 'getComplaintMessageList');
					throw new Error('API ERROR : ' + status.code);
				}
			})
			.catch((error) => {
				dispatch(actions.navigator.dismissModal());

        		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
					dispatch(actions.root.showConnectionError(getComplaintMessageList(complaintID)));
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}
			});
		return result;
	}
}


export function isSolutionAccepted(){
	return async function(dispatch, getState){
		try{
			let result = true;

			let orderActive = getState().rootReducer.order.active;
			let {data} = orderActive;

			let {complaint} = data;
			let {items} = complaint;

			for (let index in items){
				if (items[index].complaint_item_status_id != CONSTANT.COMPLAINT_ITEM_STATUS.IN_PROGRESS 
					) {
					result = false;
				}
			}	
			
			return result;		
		}
		catch(error){
			console.log(error, 'isSolutionAccepted')
		}
	}
}


export function setComplaintOrderList(data){
	return{
		type: types.COMPLAINT_ORDER_LIST, 
	    data,
	};
}

export function setComplaintMessageList(data){
	return{
		type: types.COMPLAINT_MESSAGE_LIST, 
	    data,
	};
}

export function setShipmentScheduleList(data){
	return {
	    type: types.SHIPMENT_SCHEDULE_LIST, 
	    data,
	};	
}