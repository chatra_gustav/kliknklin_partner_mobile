import * as types from './../actionTypes';
import * as STRING from './../../string';
import * as ActionRoot from './../root';

import * as fetchApi from './../../api/fetch';

import Polyline from '@mapbox/polyline';


export function fetchDirectionGoogleMaps(selfLatitude, selfLongitude, destinationLatitude, destinationLongitude){
	return async function (dispatch, getState){
		let directionCoords = [];
		let centerCoords = {};
		const apiKeyMaps = 'AIzaSyC7TdQ2gY21ils1brRLqVixAHC5gjhne5Q';
		const viewPortLatitudeDelta = 1.3;
		const viewPortLongitudeDelta = 1.3;
		
		//console.log(selfLatitude, selfLongitude, destinationLatitude, destinationLongitude);

		await fetchApi.getFetch(
			'maps/api/directions/json?origin=' + selfLatitude + ',' + selfLongitude + 
				'&destination=' + destinationLatitude + ',' + destinationLongitude + 
				'&key=' + apiKeyMaps +'&avoid=tolls&avoid=highways', 
			null,
			'https://maps.googleapis.com/'
		)
			.then(async (response) => {
				//console.log(response);
				if (response.status == 'OK') {
					let points = response.routes[0].overview_polyline.points;
				    let steps = Polyline.decode(points);

				    for (let i=0; i < steps.length; i++) {
						let tempLocation = {
							latitude : steps[i][0],
							longitude : steps[i][1]
						}

						directionCoords.push(tempLocation);
				    }

				    var northeastBounds = response.routes[0].bounds.northeast;
				    var southwestBounds = response.routes[0].bounds.southwest;

					centerCoords.latitude = (northeastBounds.lat + southwestBounds.lat) / 2;
					centerCoords.longitude = (northeastBounds.lng + southwestBounds.lng) / 2;
					centerCoords.latitudeDelta = Math.abs(northeastBounds.lat - southwestBounds.lat) * viewPortLatitudeDelta;
					centerCoords.longitudeDelta = Math.abs(northeastBounds.lng - southwestBounds.lng) * viewPortLongitudeDelta;
			 		
			 		dispatch(setDirectionCoords(directionCoords));
			 		dispatch(setCenterCoords(centerCoords));
			 		dispatch(setDistance(response.routes[0].legs[0].distance.text));
			 		dispatch(setDuration(response.routes[0].legs[0].duration.text));
				}
				else{
					throw response.status;
				}
				
			})
		 	.catch((error) => {
		 		console.log(error);
		 		if (error == 'timeout') {
					dispatch(fetchDirectionGoogleMaps(selfLatitude, selfLongitude, destinationLatitude, destinationLongitude));
		 		}
			});
	}
}

export function resetCoords(){
	return async function (dispatch, getState){
		dispatch(setDirectionCoords([]));
		dispatch(setCenterCoords(null));
	}
}

export function setDistance(distance){
	return {
		type: types.MAP_DIRECTION_DISTANCE,
		distance,
	}
}

export function setDuration(duration){
	return {
		type: types.MAP_DIRECTION_DURATION,
		duration,
	}
}

export function setDirectionCoords(coords){
	return {
		type: types.MAP_DIRECTION_COORDS,
		coords,
	}
}

export function setCenterCoords(coords){
	return {
		type: types.MAP_CENTER_COORDS,
		coords,
	}
}

function _translateDestination(){
	var destination = this.state.data.pickup_location;
	if (destination.split(' ').length == 2) {
		var check = destination.split(' ')[1];
		check = check.split(',');
		destination = {};
		destination.latitude = check[0];
		destination.longitude = check[1];
	}
	else if (destination.split(',').length == 2) {
		destination = {};
		destination.latitude = destination.split(',')[0];
		destination.longitude = destination.split(',')[1];
	}

	return destination;
}