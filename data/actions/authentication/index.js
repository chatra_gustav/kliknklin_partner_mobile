import SInfo from 'react-native-sensitive-info';

import * as types from './../actionTypes';

import * as Api from './../../api';

import { Navigation } from 'react-native-navigation';

import * as STRING from './../../string';
import * as CONSTANT from './../../constant';

import * as actions from './../../actions';

export function checkLoginState(){
	return async function (dispatch, getState){

		//temp logout
		//dispatch(logout());

		SInfo.getItem('loginState', {
				sharedPreferencesName: 'PartnerKliknKlin',
				keychainService: 'PartnerKliknKlin'})
			.then(async responseLoginState => {
				
				//already login
				if (responseLoginState != null) {
					responseLoginState = await JSON.parse(responseLoginState);
					responseLoginState.access_token = await Api.Authentication._partnerKnKDecrypt(responseLoginState.access_token);
					responseLoginState.refresh_token = await Api.Authentication._partnerKnKDecrypt(responseLoginState.refresh_token);
					
					dispatch(loginAccessToken(responseLoginState));
				}
				//not yet login
				else{
					dispatch(actions.root.setRoute('login'));
					dispatch(actions.root.checkUpdate());
				}
			})
			.catch(error => {
				
		});
	}
}

//local db
export function saveLoginStateToDB(loginState){
	return async function(dispatch, getState){
		loginState.access_token = Api.Authentication._partnerKnKEncrypt(loginState.access_token);
		loginState.refresh_token = Api.Authentication._partnerKnKEncrypt(loginState.refresh_token);

		SInfo.setItem('loginState', JSON.stringify(loginState), {
			sharedPreferencesName: 'PartnerKliknKlin',
			keychainService: 'PartnerKliknKlin'
		});
	}
}

export function saveUserStateToDB(user_name, user_email, user_image_url){
	return async function(dispatch, getState){
		let userState = {};
		
		userState.user_name = user_name;
		userState.user_email = user_email;
		userState.user_image_url = user_image_url;

		SInfo.setItem('userState', JSON.stringify(userState), {
			sharedPreferencesName: 'PartnerKliknKlin',
			keychainService: 'PartnerKliknKlin'
		});
	}
}


export function deleteLoginStateFromDB(){
	return async function(dispatch, getState){
		SInfo.deleteItem('loginState', {
            sharedPreferencesName: 'PartnerKliknKlin',
            keychainService: 'PartnerKliknKlin'
	    });
	}
}

export function resolveLogin(data, existingOauth = null){
	return async function(dispatch, getState){
		try{
			let {user, partner, oauth} = data;

			let {email, name, mobile_phone, user_image_url, email_verified_flag, mobile_phone_verified_flag} = user;

			let {availibility, role, outlet, outlets} = partner;

			let {id} = role;

			let userData = {};
			let userOutlet = [];
			userData.id = user.id;
			userData.name = name;
			userData.email = email;
			userData.mobilePhone = mobile_phone;
			userData.userImageURL = user_image_url;
			userData.emailVerifiedFlag = email_verified_flag;
			userData.mobilePhoneVerifiedFlag = mobile_phone_verified_flag;
			userData.availibility = availibility;
			userData.role = role;
			userData.origin_id = partner.origin_id;
			
			if (existingOauth) {
				userData.accessToken = existingOauth.access_token;
				userData.refreshToken = existingOauth.refresh_token; 

				dispatch(saveLoginStateToDB(existingOauth));
				dispatch(saveUserStateToDB(userData.name, userData.email, userData.userImageURL));
			}
			else{
				let {access_token, refresh_token} = oauth;
				
				userData.accessToken = access_token;
				userData.refreshToken = refresh_token; 

				dispatch(saveLoginStateToDB(oauth));
				dispatch(saveUserStateToDB(userData.name, userData.email, userData.userImageURL));
			}

			if (role.id == CONSTANT.USER_ROLE.OWNER) {
				userData.outlet = outlets;

				for(let index in outlets){
					let {id,name, address_location, outlet_status_id, outlet_type_id, geolocation, area2, area3, open_time, close_time, outletrating, mapping} = outlets[index];
					let {coordinates} = geolocation;
					let {total_order, pickup_rating, delivery_rating, laundry_rating} = outletrating;

					let obj= {}
					obj.id = id;
					obj.name = name;
					obj.addressName = address_location;
					obj.outletStatusID = outlet_status_id;
					obj.outletTypeID = outlet_type_id;
					obj.addressCoordinate = coordinates;
					obj.area2 = area2;
					obj.area3 = area3;
					obj.open_time = open_time;
					obj.close_time = close_time;
					obj.rating = {
						totalOrder:total_order,
						pickupRating:pickup_rating,
						deliveryRating:delivery_rating,
						laundryRating:laundry_rating,
					};
					obj.mapping = mapping;

					userOutlet.push(obj);
				}
			}
			else{
				let obj= {}

				if (outlet != null) {
					let {id,name, address_location, outlet_status_id, outlet_type_id, geolocation, area2, area3, open_time, close_time, outletrating, mapping} = outlet;
					let {coordinates} = geolocation;
					let {total_order, pickup_rating, delivery_rating, laundry_rating} = outletrating;
					
					
					obj.id = id;
					obj.name = name;
					obj.addressName = address_location;
					obj.outletStatusID = outlet_status_id;
					obj.outletTypeID = outlet_type_id;
					obj.addressCoordinate = coordinates;
					obj.area2 = area2;
					obj.area3 = area3;
					obj.open_time = open_time;
					obj.close_time = close_time;
					obj.rating = {
						totalOrder:total_order,
						pickupRating:pickup_rating,
						deliveryRating:delivery_rating,
						laundryRating:laundry_rating,
					};
					obj.mapping = mapping;

					userOutlet.push(obj);
				}
			}

			dispatch(actions.root.setRoute(_getRouteRoot(userData.role.id, userData.mobilePhoneVerifiedFlag)))

			dispatch(actions.user.setUserData(userData));

			let indexSelected = userOutlet.length == 0 ? null : 0;
			dispatch(actions.user.setUserOutlet(userOutlet, indexSelected));
			dispatch(actions.user.setUserRole(id));
			dispatch(loadOutletData());
			dispatch(loadInitialData(userData.role.id))
		}
		catch(error){
			console.log(error,'action.authentication.resolveLogin');
		}
	}
}

export function loadOutletData(){
	return async function(dispatch, getState) {	
		dispatch(actions.pos.getOutletItemList());
		dispatch(actions.customer.getCustomerOutletList());
		dispatch(actions.order.getSummaryOrder());
		dispatch(actions.root.getInformationList());
		dispatch(actions.pos.resetPOSData());
		dispatch(actions.attendance.getStaff());
		dispatch(actions.attendance.getStaffStatistics());
		dispatch(actions.membership.getMembershipPackage());
	}
}


export function loadInitialData(role){
	return async function(dispatch, getState) {	
		dispatch(actions.root.setIsInitialLoadDataDone(true));

		let userData = getState().rootReducer.user.data;

		dispatch(actions.masterData.getRejectOrderReasonList());
		dispatch(actions.masterData.getDelayOrderReasonList());
		dispatch(actions.masterData.getComplaintItemSolution());
		dispatch(actions.masterData.getExpenditureCategoryList());

		if (role == CONSTANT.USER_ROLE.COURIER) {
			dispatch(actions.order.setShipmentScheduleDate(new Date()));
			
			await dispatch(actions.order.getShipmentSchedule());

			let {shipmentScheduleList} = getState().rootReducer.order;

			dispatch(actions.order.setAssignmentTodayCourier(shipmentScheduleList));

			let interval = setInterval(async () => {

				let {route} = getState().rootReducer.root;

				if (route == CONSTANT.ROOT_ROUTE.LOGIN) {
					clearInterval(interval);
				}
				else{
					await dispatch(actions.order.getShipmentSchedule());

					let {shipmentScheduleList} = getState().rootReducer.order;

					dispatch(actions.order.setAssignmentTodayCourier(shipmentScheduleList));
				}
			}, 600000);
		}
		else if (role == CONSTANT.USER_ROLE.ADMIN_OUTLET) {
			dispatch(actions.order.getSummaryOrder());
			
			let interval = setInterval(() => {
				let {route} = getState().rootReducer.root;

				if (route == CONSTANT.ROOT_ROUTE.LOGIN) {
					clearInterval(interval);
				}
				else{
					dispatch(actions.order.getSummaryOrder());
				}
				
			}, 600000);
		}
		else if (role == CONSTANT.USER_ROLE.OWNER) {
			dispatch(actions.order.getSummaryOrder());
			
			let interval = setInterval(() => {
				let {route} = getState().rootReducer.root;
				
				if (route == CONSTANT.ROOT_ROUTE.LOGIN) {
					clearInterval(interval);
				}
				else{
					dispatch(actions.order.getSummaryOrder());
				}
				
			}, 600000);
		}
	}
}

function _getRouteRoot(role, mobilePhoneVerifiedFlag) {
	if (!mobilePhoneVerifiedFlag) {
		return CONSTANT.ROOT_ROUTE.SMS_VERIFICATION;
	}
	else{
		return CONSTANT.ROOT_ROUTE.AFTER_LOGIN;
		if (role == CONSTANT.USER_ROLE.COURIER) {
			return CONSTANT.ROOT_ROUTE.AFTER_LOGIN_COURIER;
		}
		else if (role == CONSTANT.USER_ROLE.ADMIN_OUTLET) {
			return CONSTANT.ROOT_ROUTE.AFTER_LOGIN_ADMIN_OUTLET;
		}
		else if (role == CONSTANT.USER_ROLE.OWNER) {
			return CONSTANT.ROOT_ROUTE.AFTER_LOGIN_OWNER;
		}
	}
}

export function loginAccessToken(loginData){
	return async function(dispatch, getState) {				
		let pushNotifToken = getState().rootReducer.pushNotif.token;
		console.log('--start action/loginAccessToken');
		Api.Authentication._loginAccessToken(loginData, pushNotifToken)
			.then((response) => {
				let {status, error, data} = response;

        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
					dispatch(resolveLogin(data, loginData));
        		}
				else{
					dispatch(loginRefreshToken(loginData));
				}
			})
			.catch((error) => {
				if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED) {
					dispatch(actions.root.setRoute('blank-page'));
					
					dispatch(actions.root.showConnectionError(loginAccessToken(loginData)));
				}

				console.log(error, error.message,'accesstoken');
			});
	}
}

export function loginRefreshToken(loginData){
	return async function(dispatch, getState){
		let pushNotifToken = getState().rootReducer.pushNotif.token;

		console.log('--start action/loginRefreshToken');
		Api.Authentication._loginRefreshToken(loginData, pushNotifToken)
			.then((response) => {
				let {status, error, data} = response;

        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
        			
					dispatch(resolveLogin(data));
        		}
				else{
					dispatch(actions.root.setRoute('login'));

					throw new Error('Mohon untuk login ulang');
				}
			})
			.catch((error) => {
				if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED) {
					dispatch(actions.root.setRoute('blank-page'));
					
					dispatch(actions.root.showConnectionError(loginAccessToken(loginData)));
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}

				console.log(error,'refreshtoken');
				//alert(error.message);
			});
	}
}

export function login(loginData){
	return async function(dispatch, getState) {
		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:STRING.LOADING_MODAL.LOGIN}));

		let pushNotifToken = getState().rootReducer.pushNotif.token;
		
    	Api.Authentication._login(loginData, pushNotifToken)
        	.then((response) => {
        		console.log(response)
        		let {status, error, data} = response;
        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
					dispatch(resolveLogin(data));
        		}
				else{
					throw new Error('username atau password salah');
				}

				dispatch(actions.navigator.dismissModal());
        	})
        	.catch((error) => {
        		dispatch(actions.navigator.dismissModal());

				if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED) {
					dispatch(actions.root.showConnectionError(login(loginData)));
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}

				console.log(error, error.message,'login');
    		});
	}
}

export function forgetPassword(email){
	return async function(dispatch, getState){
		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));

		return Api.Authentication._forgetPassword(email)
        	.then((response) => {
        		let {status, error, data} = response;
				
				dispatch(actions.navigator.dismissModal());

        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
					return response;
        		}
        		else{
        			throw new Error(error.user_error_message);
        		}

        	})
        	.catch((error) => {
        		dispatch(actions.navigator.dismissModal());

        		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED) {
					dispatch(actions.root.showConnectionError(forgetPassword(email)));
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}

				console.log(error, error.message,'forget-password');
    		});
	}
}

export function sendMobileVerificationCode(){
	return async function(dispatch, getState){
		//dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));

		let { data } = getState().rootReducer.user;
		let { smsVerificationTimer} = getState().rootReducer.root;

		if (smsVerificationTimer == CONSTANT.SMS_VERIFICATION.BASE_TIME_TIMER) {
			return Api.Authentication._sendMobileVerificationCode(data)
	        	.then((response) => {
					let {status, error, data} = response;

	        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
	        			
	        			return true;
	        		}
					else{
						let errors = new Error('API ERROR : ' + status.code + ' || ' + error);
	                    errors.name = 'api';
	                    throw errors;
					}

	        	})
	        	.catch((error) => {
	        		console.log(error);
	        		dispatch(actions.navigator.dismissModal());

					if (error.name == 'api') {
	                    setTimeout(
		        			() => alert('Server Kami Sedang Bermasalah')
		        		,200)
	                }
	                else{
	                	setTimeout(
		        			() => alert('Koneksi Anda Bermasalah')
		        		,200)
	                }
	        		
	        		//dispatch(ActionRoot.dismissLoadingModal());

	     //    		const retryFunction = () => {
						// dispatch(login(loginData));
	     //    		}
					
	     //    		if (error == 'timeout') {
	     //    			dispatch(ActionRoot.showErrorBox(STRING.ERROR.TIMEOUT.TITLE, STRING.ERROR.TIMEOUT.CONTENT, retryFunction));
	     //    		}
	     //    		else if (error == 'api') {
	     //    			dispatch(ActionRoot.setErrorMessage(STRING.ERROR.LOGIN));
	     //    		}
	     //    		else{
	     //    			dispatch(ActionRoot.showErrorBox(STRING.ERROR.UNKNOWN.TITLE, STRING.ERROR.UNKNOWN.CONTENT, retryFunction));
	     //    		}
	    		});
		}
	}
}

export function verifyMobileVerificationCode(code){
	return async function(dispatch, getState){
		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));

		return Api.Authentication._verifyMobileVerificationCode(code)
        	.then((response) => {
				let {status, error} = response;
				
				dispatch(actions.navigator.dismissModal());

        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
        			let {data, role} = getState().rootReducer.user;

        			data.mobilePhoneVerifiedFlag = true;

					dispatch(actions.root.setRoute(_getRouteRoot(role, data.mobilePhoneVerifiedFlag)));
        			dispatch(actions.user.setUserData(data));
        		}
				else{
					throw new Error('API ERROR : ' + status.code);
				}
        		
        	})
        	.catch((error) => {
        		dispatch(actions.navigator.dismissModal());

        		setTimeout(
        			() => alert(error)
        		,200)
        		//dispatch(ActionRoot.dismissLoadingModal());

     //    		const retryFunction = () => {
					// dispatch(login(loginData));
     //    		}
				
     //    		if (error == 'timeout') {
     //    			dispatch(ActionRoot.showErrorBox(STRING.ERROR.TIMEOUT.TITLE, STRING.ERROR.TIMEOUT.CONTENT, retryFunction));
     //    		}
     //    		else if (error == 'api') {
     //    			dispatch(ActionRoot.setErrorMessage(STRING.ERROR.LOGIN));
     //    		}
     //    		else{
     //    			dispatch(ActionRoot.showErrorBox(STRING.ERROR.UNKNOWN.TITLE, STRING.ERROR.UNKNOWN.CONTENT, retryFunction));
     //    		}
    		});
	}
}

export function resetReducerData(){
	return async function(dispatch, getState) {
		dispatch(actions.root.setIsInitialLoadDataDone(false));
	}
}

export function logout(){
	return async function(dispatch, getState) {
		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:STRING.LOADING_MODAL.LOGOUT}));

		let pushNotifToken = getState().rootReducer.pushNotif.token;
		let {accessToken} = getState().rootReducer.user.data;
	
		Api.Authentication._logout(accessToken, pushNotifToken)
        	.then((response) => {
        		let {status, error, data} = response;
        		
        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
        			dispatch(resetReducerData());

					SInfo.deleteItem('loginState', {
			            sharedPreferencesName: 'PartnerKliknKlin',
			            keychainService: 'PartnerKliknKlin'
				    });

				    dispatch(actions.root.setRoute('login'));
        		}
				else{
					throw new Error(error.internal_error_message.error);
				}

				dispatch(actions.navigator.dismissModal());
        	})
        	.catch((error) => {
        		dispatch(actions.navigator.dismissModal());

				if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
					dispatch(actions.root.showConnectionError(logout()));
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}
    		});
		
	}
}

export function changePassword(params, navigator) {
	return async function(dispatch, getState) {
		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:"Mohon Tunggu.."}));

		let {accessToken} = getState().rootReducer.user.data;

		Api.Authentication._changePassword(params, accessToken)
		.then((response) => {

			dispatch(actions.navigator.dismissModal());

			let {status, error, data} = response;

			if(status.code == CONSTANT.HTTP_STATUS_CODE.OK && error.length == 0) {
				dispatch(actions.root.showMessage("Password berhasil diubah", 'Berhasil'));
				dispatch(actions.navigator.pop(navigator));
			}else{
				throw new Error(error);
			}
		})
		.catch((error) => {
    		dispatch(actions.navigator.dismissModal());

			if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
				dispatch(actions.root.showConnectionError(logout()));
			}
			else{
				dispatch(actions.root.showErrorMessage(error.message));
			}
		});
			
	}
}

//set function
export function setLoginState(loginState){
	return {
	    type: types.SET_LOGIN_STATE, 
	    loginState,
	};	
}

