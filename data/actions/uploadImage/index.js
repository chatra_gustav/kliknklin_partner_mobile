import * as API from './../../api';
import * as actions from 'app/data/actions';

export function uploadImage(uri){

	return async function(dispatch, getState){
		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText: 'Mohon tunggu...'}));

		let response = await API.UploadImage._uploadImage(uri);
		let {url} = response;

		return url;
	}
}