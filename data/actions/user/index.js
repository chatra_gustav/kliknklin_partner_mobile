import * as types from './../actionTypes';
import * as actions from './../../actions';
import { Dimensions } from 'react-native';

export function getUserLocation(navigator){
	return async function (dispatch, getState){
		let result = null;

		await dispatch(setUserLocation(null));

		let isVisible = await navigator.screenIsCurrentlyVisible();
    	
		while(!isVisible){
	      isVisible = await navigator.screenIsCurrentlyVisible();
	    }


    	if (isVisible) {
			let userLocation = await dispatch(actions.location.getCurrentLocation());
			
			result = userLocation;
			
			await dispatch(setUserLocation(userLocation));
			//check location status
			//get location
		}

		return result;
	}
}

export function setOutletLocation(){
	return async function (dispatch, getState){

		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;

		let coordinate = outletList[indexActiveOutlet].addressCoordinate;
		let latitude = coordinate[0];
		let longitude = coordinate[1];

		let { width, height } = Dimensions.get('window');
		const aspect_ration = width / height;
		const latitudeDelta = 0.01;
		const longitudeDelta = latitudeDelta * aspect_ration;

		let data = {
			latitude: latitude,
			longitude: longitude,
			latitudeDelta: latitudeDelta,
			longitudeDelta: longitudeDelta
		}

		await dispatch(outletLocation(data));
	}
}

export function setUserLocation(location){
	return {
		type: types.USER_LOCATION,
		location,
	}
}


export function setUserRole(role){
	return {
		type: types.USER_ROLE,
		role,
	}
}

export function setUserData(data){
	return{
		type: types.USER_DATA,
		data,
	}
}

export function setUserOutlet(data, indexActiveOutlet){
	return{
		type: types.USER_OUTLET,
		data,
		indexActiveOutlet,
	}
}

export function outletLocation(data){

	return{
		type: types.OUTLET_LOCATION,
		data
	}
}
