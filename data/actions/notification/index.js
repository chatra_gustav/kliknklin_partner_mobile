import {
  PermissionsAndroid,
  AppState,
} from 'react-native';


import * as types from './../actionTypes';
import * as STRING from './../../string';
import * as API from './../../api';
import * as actions from './../../actions';

export function getNotification(){
	return async function(dispatch, getState) {
		let {accessToken} = getState().rootReducer.user.data;
		
		API.Root._getNotification(accessToken)
			.then((response) => {
				console.log(response,'notification');
			})
			.catch((error) => {
				console.log(error, 'get notification');
				//alert(error.message);
			})
	}
}