import * as types from './../actionTypes';

import * as actions from './../../actions';

import * as STRING from './../../string';
import * as CONSTANT from './../../constant';
import * as API from './../../api';
import {toCurrency} from 'app/data/components';

export function getExpenseList(selectedMonth = new Date()){
	return async function(dispatch, getState){
		dispatch(setExpenseSearchDate(selectedMonth));

		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));

		let {accessToken} = getState().rootReducer.user.data;

		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;
		let outletID = outletList[indexActiveOutlet].id;

		let month = new Date(selectedMonth).getMonth() + 1;
		
		await API.Expenses._getExpenseList(outletID, month, accessToken)
			.then((response) => {
				dispatch(actions.navigator.dismissAllModals());

				let {status, error, data} = response;
        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
        			dispatch(setExpenseList(data));
        		}
				else{
					let errors = new Error('API ERROR : ' + status.code + ' || ' + error);
                    errors.name = 'api';
                    throw errors;
				}
			})
			.catch((error) => {
				console.log(error);
				dispatch(actions.navigator.dismissModal());
				
				if (error.name == 'api') {
                    setTimeout(
	        			() => alert('Server Kami Sedang Bermasalah')
	        		,200);
                }
                else{
                	if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
						dispatch(actions.root.showConnectionError(getExpenseList(selectedMonth)));
					}
					else{
						dispatch(actions.root.showErrorMessage(error.message));
					}
                }
			});
	}
}


export function createExpense(data, navigator){
	return async function(dispatch, getState){
		let { expenditureCategoryList } = getState().rootReducer.masterData;

		data.expenditure_category_id = expenditureCategoryList[data.expenditure_category_index].id;
		data.expenditure_category_info = expenditureCategoryList[data.expenditure_category_index].info;

		let {accessToken, id} = getState().rootReducer.user.data;

		data.accessToken = accessToken;
		data.user_id = id;

		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;
		data.outlet_id = outletList[indexActiveOutlet].id;

		dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeA',{
	    	title:'Konfirmasi', 
	    	message: 'apakah anda yakin untuk membuat pengeluaran? \n' + 
	    			data.expenditure_category_info + ' - ' + toCurrency(data.amount, 'Rp '), 
	    	buttonPrimaryText: 'ya', 
	    	buttonSecondaryText: 'batal', 
		    onPressButtonPrimary: async () => {
		    	dispatch(actions.navigator.dismissLightBox());
		    	data.receipt_image_url = await dispatch(actions.uploadImage.uploadImage(data.receipt_image_location));
		    	
		    	dispatch(_createExpense(data, navigator));
		    }, 
		    onPressButtonSecondary: () =>{
				dispatch(actions.navigator.dismissLightBox());
		    },
		}));
	}
}

export function _createExpense(data, navigator){
	return async function(dispatch, getState){
		API.Expenses._createExpense(data.expenditure_category_id, data.user_id, data.outlet_id, data.amount, data.receipt_date, data.receipt_image_url, data.info, data.accessToken)
			.then((response) => {
				let {status, error, data} = response;

				dispatch(actions.navigator.dismissAllModals());
				
        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
        			let {searchDate} = getState().rootReducer.expenses;
        			dispatch(getExpenseList(searchDate));

        			dispatch(actions.root.showAlertMessage('Pengeluaran berhasil dibuat', 'Perhatian', false, () =>{
        				dispatch(actions.navigator.pop(navigator));
        			}));
        		}
				else{
					let errors = new Error('API ERROR : ' + status.code + ' || ' + error);
                    errors.name = 'api';
                    throw errors;
				}
			})
			.catch((error) => {
				console.log(error);
				dispatch(actions.navigator.dismissModal());
				
				if (error.name == 'api') {
                    setTimeout(
	        			() => alert('Server Kami Sedang Bermasalah')
	        		,200);
                }
                else{
                	if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
						//dispatch(actions.root.showConnectionError(getExpenseList()));
					}
					else{
						dispatch(actions.root.showErrorMessage(error.message));
					}
                }
			});
	}
}

export function deleteExpense(id){
	return async function(dispatch, getState){
		let {accessToken} = getState().rootReducer.user.data;
		
		dispatch(actions.navigator.showLightBox('PartnerKliknKlin.BoxTypeA',{
	    	title:'Konfirmasi', 
	    	message: 'apakah anda yakin untuk menghapus pengeluaran?', 
	    	buttonPrimaryText: 'ya', 
	    	buttonSecondaryText: 'batal', 
		    onPressButtonPrimary: async () => {
		    	dispatch(actions.navigator.dismissLightBox());

		    	dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Menunggu..'}));

		    	API.Expenses._deleteExpense(id, accessToken)
					.then((response) => {
						let {status, error, data} = response;

						dispatch(actions.navigator.dismissAllModals());
						
		        		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
		        			let {searchDate} = getState().rootReducer.expenses;
		        			dispatch(getExpenseList(searchDate));

		        			dispatch(actions.root.showAlertMessage('Pengeluaran berhasil dihapus', 'Perhatian', false));
		        		}
						else{
							let errors = new Error('API ERROR : ' + status.code + ' || ' + error);
		                    errors.name = 'api';
		                    throw errors;
						}
					})
					.catch((error) => {
						console.log(error);
						dispatch(actions.navigator.dismissModal());
						
						if (error.name == 'api') {
		                    setTimeout(
			        			() => alert('Server Kami Sedang Bermasalah')
			        		,200);
		                }
		                else{
		                	if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
								//dispatch(actions.root.showConnectionError(getExpenseList()));
							}
							else{
								dispatch(actions.root.showErrorMessage(error.message));
							}
		                }
					});
		    }, 
		    onPressButtonSecondary: () =>{
				dispatch(actions.navigator.dismissLightBox());
		    },
		}));
	}
}



export function setExpenseList(data){

	return {
	    type: types.EXPENSE_LIST, 
	    data,
	};
}

export function setExpenseSearchDate(date){

	return {
	    type: types.EXPENSE_SEARCH_DATE, 
	    date,
	};
}


