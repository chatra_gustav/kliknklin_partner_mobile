import * as actions from './../../actions';
import { Navigation } from 'react-native-navigation';

export function email(itemToCheck){
  return async function(dispatch, getState){
      let result;

      var regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      
      return regexEmail.test(itemToCheck);
  }
}

export function minChar(itemToCheck, minChar){
  return async function(dispatch, getState){
    if (!itemToCheck || itemToCheck.length < minChar) {
      return false;
    }
    return true;
  }
}

export function isNumberPositive(itemToCheck){
  return async function(dispatch, getState){
    itemToCheck = parseInt(itemToCheck);
    return Number.isInteger(itemToCheck) && itemToCheck > 0;
  }
}

export function letter(itemToCheck){
  return async function(dispatch, getState){
      let result;

      var regex = /^[A-Za-z ]+$/;
      return regex.test(itemToCheck);
  }
}

export function isEmpty(itemToCheck){
  return async function(dispatch, getState){
    if (itemToCheck == null || itemToCheck == undefined) {
        return true;
    }
    else{
        if (typeof itemToCheck != 'string') {
          itemToCheck = itemToCheck.toString();
        }
        console.log(itemToCheck.trim());
        return itemToCheck.trim() == '';
    }
  }
}


export function mobilePhone(itemToCheck){
  return async function(dispatch, getState){
    let minCharPhoneNumber = 7;
    let maxCharPhoneNumber = 14
    
    if (/^[0-9]+$/.test(itemToCheck)) {
      if (itemToCheck.length >= minCharPhoneNumber && itemToCheck.length <= maxCharPhoneNumber) {
          if (itemToCheck.substring(0,2) == '08') {
            return true;
          }
        }
    }
    return false;
  }
}