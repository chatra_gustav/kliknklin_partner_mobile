import {
  PermissionsAndroid,
  AppState,
} from 'react-native';


import * as types from './../actionTypes';
import * as STRING from './../../string';
import * as Api from './../../api';
import * as actions from './../../actions';

export function setLayoutHeight(height) {
  return {
    type: types.LAYOUT_HEIGHT, 
    height,
  };
}

export function setScreenState(screenState){
  return {
    type: types.SCREEN_STATE,
    screenState,
  };
}

export function setPressBarrierVisibility(visibility) {
  return {
    type: types.LAYOUT_PRESS_BARRIER_VISIBILITY,
    visibility,
  };
}

