import * as types from './../actionTypes';

import * as actions from './../../actions';

import * as STRING from './../../string';
import * as CONSTANT from './../../constant';
import * as API from './../../api';
import * as COMPONENT from 'app/data/components';

export function getKnkBanks(){
	return async function(dispatch, getState){
		
		let banks = [];	
		let userData = getState().rootReducer.user.data;

		await API.Deposit.knkBank()
		.then(async (response) => {

			let index = 1;

			response.map((bank) => {
				
				if(bank.availability === 1){
					let obj = {
						showIndex: index,
						bank_id: bank.bank_id,
						account_number: bank.account_number,
						account_name: bank.account_name,
						bank_name: bank.bank.bank_name,
						logo_image_url: bank.bank.logo_image_url
					}

					banks.push(obj);
					index++;
				}
			})

			await dispatch(setKnkBank(banks));
    		
		})
		.catch((error) => {
			console.log(error, '--getKnkBank');

    		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
				dispatch(actions.root.showConnectionError());
			}
			else{
				dispatch(actions.root.showErrorMessage(error.message));
			}
		});
	}
}

export function getPartnerData(){
	return async function(dispatch, getState){
		
		let userData = getState().rootReducer.user.data;

		await API.Deposit.partner(userData.accessToken)
		.then(async (response) => {
			console.log(response);
			let {status, error, data} = response;

    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
    			let obj = null
    			if (data.account.bankdetail != null) {
					obj = {
	    				name: data.partner.ktp_name,
	    				account_holder_name: data.account.bankdetail.account_holder_name,
	    				account_number: data.account.bankdetail.account_number,
	    				bank_detail_id: data.account.bankdetail.bank_id,
	    				bank_image_url: data.account.bankdetail.bank.logo_image_url
	    			}
    			}

    			await dispatch(setAccountOwner(obj));
    		}
			else{
				throw new Error('API ERROR : ' +  status.code);
			}
		})
		.catch((error) => {
			console.log(error, '--getPartnerData');

    		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
				dispatch(actions.root.showConnectionError());
			}
			else{
				dispatch(actions.root.showErrorMessage(error.message));
			}
		});
	}
}

export function getSummary(date){

	return async function(dispatch, getState){
		
		let userData = getState().rootReducer.user.data;
		
		await API.Deposit.getSummary(userData.accessToken)
		.then(async (response) => {

			let {status, error, data} = response;

    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {

    			await dispatch(setDepositSummary(data));
    		}
			else{
				throw new Error('API ERROR : ' +  status.code);
			}
		})
		.catch((error) => {
			console.log(error, '--getSummary');

    		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
				dispatch(actions.root.showConnectionError());
			}
			else{
				dispatch(actions.root.showErrorMessage(error.message));
			}
		});
	}
}

export function getCashflowTransaction(startDate, endDate){

	return async function(dispatch, getState){
		
		let userData = getState().rootReducer.user.data;

		await API.Deposit.cashflowTransaction(startDate, endDate, userData.accessToken)
		.then(async (response) => {

			let {status, error, data} = response;
			let cashFlow = [];
    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {

    			data.map((data_obj) => {
    				if((data_obj.category.id == 2 && data_obj.wdr != null) || data_obj.category.id == 1 || data_obj.category.id == 3 || data_obj.category.id == 5 || data_obj.category.id == 6){
    					let params = {
	    					data: data_obj,
	    					category: data_obj.category,
	    					torder: data_obj.torder,
	    					wdr: data_obj.wdr,
	    					sorder: data_obj.sorder,
	    					amount: data_obj.amount,
	    					info: data_obj.info
	    				}

	    				let obj = {
	    					category: params.category.id,
	    					last_balance: COMPONENT.toCurrency(data_obj.last_balance, ""),
	    					transaction_hash: getTransactionHash(params),
		    				is_paid: isPaid(params),
		    				customer_name: getCustomerName(params),
		    				payment_method: getPaymentMethod(params),
		    				full_price: COMPONENT.toCurrency(getFullPrice(params), '+Rp '),
		    				knk_profit: COMPONENT.toCurrency(getKnkProfit(params), ''),
		    				outlet_profit: COMPONENT.toCurrency(getOutletProfit(params), ''),
		    				shipment_fare: COMPONENT.toCurrency(getShipmentFare(params), ''),
		    				marketing_cost_outlet: COMPONENT.toCurrency(getMCO(params), ''),
		    				marketing_cost_kliknklin: COMPONENT.toCurrency(getMCKnk(params), ''),
		    				bank_name: getBankName(params),
	    					account_number: getAccountNumber(params),
		    				debit: COMPONENT.toCurrency(getDebitAmount(params), ''),
		    				credit: COMPONENT.toCurrency(getCreditAmount(params), ''),
		    				balance: getBalance(data_obj.last_balance, params),
		    				is_withdraw: isWithdrawTransaction(params),
		    				amount: params.amount,
		    				info: params.info
		    			}

		    			cashFlow.push(obj);
    				}
    			})

    			await dispatch(setCashflowTransaction(cashFlow));
    		}
			else{
				throw new Error('API ERROR : ' +  status.code);
			}
		})
		.catch((error) => {
			console.log(error, '--getCashflowTransaction');

    		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
				dispatch(actions.root.showConnectionError());
			}
			else{
				dispatch(actions.root.showErrorMessage(error.message));
			}
		});
	}
}

export function getWdrRequest(){

	return async function(dispatch, getState){
		
		let userData = getState().rootReducer.user.data;

		await API.Deposit.wdrRequest(userData.accessToken)
		.then(async (response) => {

			let {status, error, data} = response;
			let wdrRequest = [];
    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
    			data.map((data_obj) => {
    				let obj = {
    					type_id:data_obj.type.id,
    					type: data_obj.type.info,
    					status: data_obj.status.info,
    					amount: getAmount(data_obj.type.id, data_obj.amount),
    					bank_name: data_obj.ownerbank.bank.bank_name,
    					account_number: data_obj.ownerbank.account_number,
    					is_withdraw: isWithdraw(data_obj.type.id),
    					payment_image_url: data_obj.payment_image_url
	    			}

	    			wdrRequest.push(obj);
    			})
    			await dispatch(setWdrRequest(wdrRequest));
    		}
			else{
				throw new Error('API ERROR : ' +  status.code);
			}
		})
		.catch((error) => {
			console.log(error, '--getWdrRequest');

    		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
				dispatch(actions.root.showConnectionError());
			}
			else{
				dispatch(actions.root.showErrorMessage(error.message));
			}
		});
	}
}

export function topUpBalance(kliknklin_bank_detail_id, amount, payment_image_url){

	return async function(dispatch, getState){
		
		let userData = getState().rootReducer.user.data;
		let ownerData = getState().rootReducer.deposit.accountOwner;
		
		await API.Deposit.topUpBalance(ownerData.bank_detail_id, kliknklin_bank_detail_id, ownerData.account_number, amount, payment_image_url, userData.accessToken)
		.then(async (response) => {

			let {status, error, data} = response;

    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
    			dispatch(actions.root.showMessage("Permintaan penambahan deposit anda akan segera diproses", 'Berhasil'));
    		}
			else{
				throw new Error('API ERROR : ' +  status.code);
			}
		})
		.catch((error) => {
			console.log(error, '--getSummary');

    		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
				dispatch(actions.root.showConnectionError());
			}
			else{
				dispatch(actions.root.showMessage(error, 'Terjadi Kesalahan'));
			}
		});
	}
}

export function withdrawBalance(kliknklin_bank_detail_id, amount){

	return async function(dispatch, getState){
		
		let userData = getState().rootReducer.user.data;
		let ownerData = getState().rootReducer.deposit.accountOwner;
		
		await API.Deposit.withdrawBalance(ownerData.bank_detail_id, kliknklin_bank_detail_id, ownerData.account_number, amount, userData.accessToken)
		.then(async (response) => {

			let {status, error, data} = response;

    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {

    			dispatch(actions.root.showMessage("Permintaan penarikan deposit anda akan segera diproses", 'Berhasil'));

    		}
			else{
				throw error;
			}
		})
		.catch((error) => {
			console.log(error, '--withdrawBalance');

    		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
				dispatch(actions.root.showConnectionError());
			}
			else{
				dispatch(actions.root.showMessage(error, 'Terjadi Kesalahan'));
			}
		});
	}
}

export function setAccountOwner(data){
	return {
	    type: types.SET_ACCOUNT_OWNER, 
	    data,
	};	
}

export function setKnkBank(data){
	return {
	    type: types.SET_KLIKNKLIN_BANK,
	    data,
	};	
}

export function setDepositSummary(data){

	return {
	    type: types.SET_DEPOSIT_SUMMARY, 
	    data,
	};
}

export function setCashflowTransaction(data){

	return {
	    type: types.SET_CASHFLOW_TRANSACTION, 
	    data,
	};
}

export function setWdrRequest(data){

	return {
	    type: types.SET_WDR_REQUEST, 
	    data,
	};
}

function getTransactionHash(params){
	let {category} = params;
	let {id} = category;
	if(id === 2)
		return params.data.transaction_hash;
	else if(id === 1)
		return params.torder.sorder.order_hash;
	else if(id === 3 || id === 5 || id === 6) // revert order, complaint charge, admin privileges
		return params.sorder.order_hash;
}

function isPaid(params){
	if(params.category.id === 1){
		if(params.torder.is_paid === 1 || params.torder.is_paid === 0)
			return 'Belum Bayar';
		else if(params.torder.is_paid === 2)
			return 'Sudah Bayar';
		else if(params.torder.is_paid === 3)
			return 'Menunggu Persetujuan';
		else if(params.torder.is_paid === 4)
			return 'cancel';
	}else{
		return ''
	}
}

function getCustomerName(params){
	if(params.category.id === 1)
		return params.torder.sorder.customer.user.name;
	else
		return ''
}

function getPaymentMethod(params){
	if(params.category.id === 1){
		id = params.torder.sorder.payment.payment_method_id;

		if(id === 1)
			return 'Cash'
		else if(id === 2)
			return 'Bank Transfer'
		else if(id === 3)
			return 'Membership'
		else 
			return 'Wallet'
	}else{
		return ''
	}
}

function getFullPrice(params){
	let {category} = params;
	let {id} = category;

	if(id === 2 || id === 3 || id === 5 || id === 6)
		return 0
	else
		return params.torder.sorder.pricedetail.full_price
}

function getKnkProfit(params){
	let {category} = params;
	let {id} = category;

	if(id === 2 || id === 3 || id === 5 || id === 6)
		return 0
	else
		return params.torder.sorder.pricedetail.kliknklin_profit
}

function getOutletProfit(params){
	let {category} = params;
	let {id} = category;

	if(id === 2 || id === 3 || id === 5 || id === 6)
		return 0
	else
		return params.torder.sorder.pricedetail.outlet_profit
}

function getShipmentFare(params){
	let {category} = params;
	let {id} = category;

	if(id === 2 || id === 3 || id === 5 || id === 6)
		return 0
	else
		return params.torder.sorder.pricedetail.shipment_fare
}

function getMCO(params){
	let {category} = params;
	let {id} = category;

	if(id === 2 || id === 3 || id === 5 || id === 6)
		return 0
	else
		return params.torder.sorder.pricedetail.marketing_cost_outlet
}

function getMCKnk(params){
	let {category} = params;
	let {id} = category;

	if(id === 2 || id === 3 || id === 5 || id === 6)
		return 0
	else
		return params.torder.sorder.pricedetail.marketing_cost_kliknklin
}

function getBankName(params){
	if(params.category.id === 2)
		return params.wdr.ownerbank.bank.bank_name;
	else
		return ''
}

function getAccountNumber(params){
	if(params.category.id === 2)
		return params.wdr.ownerbank.account_number;
	else 
		return ''
}

function getDebitAmount(params){
	if(params.category.id === 2){
		if(params.wdr.type.id === 2)
			return params.wdr.amount
		else
			return 0
	}else{
		return 0
	}
}

function getCreditAmount(params){
	if(params.category.id === 2){
		if(params.wdr.type.id === 2)
			return 0
		else
			return params.wdr.amount
	}else{
		return 0
	}
}

function getDeposit(params){
	if(params.category.id === 2){
		if(params.wdr.type.id === 2)
			return COMPONENT.toCurrency(params.wdr.amount, '+Rp ')
		else
			return COMPONENT.toCurrency(params.wdr.amount, '- Rp ')
	}else{
		return getOutletDeposit(params)
	}
}

function getBalance(last_balance, params){
	let balance = 0;
	let payment_method = getPaymentMethod(params);

	let {category, amount} = params;
	let {id} = category;

	if(id == 1 || id == 2){
		if(payment_method === 'Cash'){
			let debit = getDebitAmount(params);
			let credit = getCreditAmount(params);
			let knk_profit = getKnkProfit(params);

			balance = last_balance + debit - credit - knk_profit;
		}
		else if(payment_method === 'Bank Transfer'){
			let debit = getDebitAmount(params);
			let credit = getCreditAmount(params);
			let outlet_profit = getOutletProfit(params);
			let shipment_fare = getShipmentFare(params);

			balance = last_balance + debit - credit + outlet_profit + shipment_fare;
		}else{ 
			//wdrRequest
			let debit = getDebitAmount(params);
			let credit = getCreditAmount(params);
			let outlet_profit = getOutletProfit(params);

			balance = last_balance + debit - credit;
		}
	}else if(id == 3 || id == 5 || id == 6){
		balance = last_balance + amount;
	}

	return COMPONENT.toCurrency(balance, 'Rp ');
}

function isWithdrawTransaction(params){
	if(params.category.id === 2){
		if(params.wdr.type.id === 2){
			return false
		}
		else{
			return true
		}
	}else{
		return false
	}
}

function getAmount(type_id, amount){
	if(type_id === 2)
		return COMPONENT.toCurrency(amount, '+Rp ')
	else
		return COMPONENT.toCurrency(amount, '- Rp ')
}

function isWithdraw(type_id){
	if(type_id === 2)
		return false
	else
		return true
}

function getOutletDeposit(params){
	let deposit = 0;
	let payment_method_id = params.torder.sorder.payment.payment_method_id;

	if(payment_method_id == 1){ // Cash
		deposit = getMCKnk(params) - getKnkProfit(params);

		if(deposit >= 0){
			return COMPONENT.toCurrency(deposit, '+Rp ')
		}else{
			return COMPONENT.toCurrency(deposit, '- Rp ')
		}
	}else{ // Bank Transfer
		deposit = getOutletProfit(params) + getShipmentFare(params) - getMCO(params);

		if(deposit >= 0){
			return COMPONENT.toCurrency(deposit, '+Rp ')
		}else{
			return COMPONENT.toCurrency(deposit, '- Rp ')
		}
	}
}