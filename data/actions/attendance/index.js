import * as types from './../actionTypes';

import * as actions from './../../actions';

import * as STRING from './../../string';
import * as CONSTANT from './../../constant';
import * as API from './../../api';

export function getStaff(){

	return async function(dispatch, getState){

		let userData = getState().rootReducer.user.data;
		let attendanceDate = getState().rootReducer.attendance.attendanceDate;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;
		
		let staffList = [];

		if(indexActiveOutlet !== null){
			await API.Attendance._getStaff(outletList[indexActiveOutlet].id, attendanceDate, userData.accessToken)
			.then(async (response) => {

				let {status, error, data} = response;
	    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
	    			
	    			data.map((obj) => {
	    				let staff_obj = {
	        				user_id: obj.partner.user_id,
	        				user_name: obj.partner.ktp_name,
	        				user_role: obj.partner.role.display_name,
	        				img_url: getImageUrl(obj.partner),
	        				time_in: getDatetimeIn(obj.attendance),
	        				time_out: getDatetimeOut(obj.attendance),
	        				isIn: isUserIn(obj.attendance),
	        				isOut: isUserOut(obj.attendance)
	        			}

	        			staffList.push(staff_obj);
	    			})

	    			await dispatch(setStaffList(staffList));
	    		}
				else{
					throw new Error('API ERROR : ' +  status.code);
				}
			})
			.catch((error) => {
				console.log(error, '--getStaff');

	    		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
					dispatch(actions.root.showConnectionError());
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}
			});
		}else{
			await dispatch(setStaffList(staffList));	
		}
	}
}

export function getStaffStatistics(){

	return async function(dispatch, getState){

		let userData = getState().rootReducer.user.data;
		let attendanceMonth = getState().rootReducer.attendance.attendanceMonth;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;

		let staffList = [];

		if(indexActiveOutlet !== null){
			await API.Attendance._getStaffStatistics(outletList[indexActiveOutlet].id, attendanceMonth, userData.accessToken)
			.then(async (response) => {

				let {status, error, data} = response;

	    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {

	    			data.map((obj) => {
	    				let staff_obj = {
	        				user_id: obj.partner.user_id,
	        				user_name: obj.partner.ktp_name,
	        				user_role: obj.partner.role.display_name,
	        				img_url: getImageUrl(obj.partner),
	        				days_in: (obj.days == undefined || "" ? "" : obj.days),
	        				hours_in: (obj.hours == undefined || "" ? "" : obj.hours),
	        				percentage: (obj.percentage == undefined ? 0 : obj.percentage),
	        				attendance: (obj.attendance == undefined ? [] : obj.attendance)
	        			}

	        			staffList.push(staff_obj);
	    			})

	    			await dispatch(setStaffSummary(staffList));
	    		}
				else{
					throw new Error('API ERROR : ' +  status.code);
				}
			})
			.catch((error) => {
				console.log(error, '--getStaff');

	    		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
					dispatch(actions.root.showConnectionError());
				}
				else{
					dispatch(actions.root.showErrorMessage(error.message));
				}
			});
		}
		else{
			await dispatch(setStaffSummary(staffList));
		}
	}
}

export function staffIn(navigator, staff){

	return async function(dispatch, getState){

		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText: 'Mohon tunggu...'}));

		let userData = getState().rootReducer.user.data;	
		let staffList = getState().rootReducer.attendance.staffList;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;
		let userLocation = getState().rootReducer.user.location;


		await API.Attendance._staffIn(outletList[indexActiveOutlet].id, staff, userData.accessToken, userLocation)
		.then(async (response) => {

			dispatch(actions.navigator.dismissAllModals());
			let {status, error, data} = response;
			
    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {

			    let findData = (p) => {
			      return p.user_id === data.user_id;
			    }

			    dataIndex = staffList.findIndex(findData);

			    staffList[dataIndex].isIn = true;
			    staffList[dataIndex].time_in = getDatetimeIn(data);

    			await dispatch(setStaffList(staffList));
    		}
			else{
				throw new Error('API ERROR : ' +  status.code);
			}
		})
		.catch((error) => {
			console.log(error, '--staffIn');

			dispatch(actions.navigator.dismissModal());

    		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
				dispatch(actions.root.showConnectionError());
			}
			else{
				dispatch(actions.root.showErrorMessage(error.message));
			}
		});
	}
}

export function staffOut(navigator, staff){

	return async function(dispatch, getState){

		dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText: 'Mohon tunggu...'}));

		let userData = getState().rootReducer.user.data;
		let staffList = getState().rootReducer.attendance.staffList;
		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;
		let userLocation = getState().rootReducer.user.location;

		await API.Attendance._staffOut(outletList[indexActiveOutlet].id, staff, userData.accessToken, userLocation)
		.then(async (response) => {

			dispatch(actions.navigator.dismissAllModals());
			let {status, error, data} = response;
			
    		if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {

			    let findData = (p) => {
			      return p.user_id === data.user_id;
			    }

			    dataIndex = staffList.findIndex(findData);

			    staffList[dataIndex].isOut = true;
			    staffList[dataIndex].time_out = getDatetimeOut(data);

    			await dispatch(setStaffList(staffList));
    		}
			else{
				throw new Error('API ERROR : ' +  status.code);
			}
		})
		.catch((error) => {
			console.log(error, '--staffOut');

			dispatch(actions.navigator.dismissModal());

    		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
				dispatch(actions.root.showConnectionError());
			}
			else{
				dispatch(actions.root.showErrorMessage(error.message));
			}
		});
	}
}

function getImageUrl(data_partner){
	if(data_partner.user_image_url === '' || data_partner.user_image_url === null){
		return null
	}else{
		return data_partner.user_image_url;
	}
}

function isEmptyObj(myObject){
	return !Object.keys(myObject).length;
}

function getDatetimeIn(data_attendance){
	if( isEmptyObj(data_attendance) ){
		return ''
	}
	else if(data_attendance.datetime_in === null){
		return ''
	}
	else{
		let arr_datetime = data_attendance.datetime_in.split(" ");
		let time = arr_datetime[1];

		let arr_time = time.split(":");
		return arr_time[0]+':'+arr_time[1];
	}
}

function getDatetimeOut(data_attendance){
	if( isEmptyObj(data_attendance) )
		return ''
	else if(data_attendance.datetime_out === null)
		return ''
	else{

		let arr_datetime = data_attendance.datetime_out.split(" ");
		let time = arr_datetime[1];

		let arr_time = time.split(":");
		return arr_time[0]+':'+arr_time[1];
	}
}

function isUserIn(data_attendance){
	if( isEmptyObj(data_attendance) )
		return false
	else if(data_attendance.datetime_in === null)
		return false
	else
		return true
}

function isUserOut(data_attendance){
	if( isEmptyObj(data_attendance) )
		return false
	else if(data_attendance.datetime_out === null)
		return false
	else
		return true
}

export function setStaffList(data){

	return {
	    type: types.SET_STAFF_LIST, 
	    data,
	};
}

export function setStaffSummary(data){

	return {
	    type: types.SET_STAFF_SUMMARY, 
	    data,
	};
}

export function setAttendanceDate(data){ // untuk menampilkan data staff pertanggal tersebut

	return {
	    type: types.SET_ATTENDANCE_DATE,
	    data,
	};
}

export function setAttendanceMonth(data){ // untuk melihat statistik staff perbulan tersebut

	return {
	    type: types.SET_ATTENDANCE_MONTH,
	    data,
	};
}