import * as types from './../actionTypes';

import * as actions from './../../actions';
import * as CONSTANT from './../../constant';
import * as API from './../../api';


export function getDelayOrderReasonList(){
	return async function (dispatch, getState){
		let result = [];
		await API.Root._getMDDelayReason()
			.then(async response => {
				let {status, error, data} = response;
				
				if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
					dispatch(setDelayOrderReasonList(data));
					result = data;
        		}
				else{
					throw new Error('API ERROR : ' + status.code);
				}
			})
			.catch(error => {
				dispatch(actions.navigator.dismissModal());
				console.log(error, 'actions.root.getMDDelayReason');
			});

		return result;
	}
}

export function getRejectOrderReasonList(){
	return async function (dispatch, getState){
		let result = [];
		await API.Root._getMDRejectReason()
			.then(async response => {
				let {status, error, data} = response;
				
				if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
					dispatch(setRejectOrderReasonList(data));
					result = data;
        		}
				else{
					throw new Error('API ERROR : ' + status.code);
				}
			})
			.catch(error => {
				dispatch(actions.navigator.dismissModal());
				console.log(error, 'actions.root.getMDDelayReason');
			});

		return result;
	}
}

export function getComplaintItemSolution(){
	return async function (dispatch, getState){
		let result = [];
		await API.Root._getMDComplaintItemSolution()
			.then(async response => {
				let {status, error, data} = response;
				if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
					dispatch(setComplaintItemSolutionList(data));
					result = data;
        		}
				else{
					throw new Error('API ERROR : ' + status.code);
				}
			})
			.catch(error => {
				dispatch(actions.navigator.dismissModal());
				console.log(error, 'actions.master_data.getComplaintItemSolution');
			});

		return result;
	}
}

export function getExpenditureCategoryList(){
	return async function (dispatch, getState){
		let result = [];
		await API.Root._getMDExpenditureCategory()
			.then(async response => {
				let {status, error, data} = response;
				if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
					dispatch(setExpenditureCategoryList(data));
					result = data;
        		}
				else{
					let errors = new Error('API ERROR : ' + status.code + ' || ' + error);
                    errors.name = 'api';
                    throw errors;
				}
			})
			.catch(error => {
				console.log(error, 'actions.master_data.getComplaintItemSolution');
				dispatch(actions.navigator.dismissModal());
				
				if (error.name == 'api') {
                }
                else{
                	if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
						dispatch(getExpenditureCategory());
						//dispatch(actions.root.showConnectionError(getExpenseList()));
					}
					else{
						//dispatch(actions.root.showErrorMessage(error.message));
					}
                }
			});

		return result;
	}
}

export function setExpenditureCategoryList(data){
	return {
		type: types.EXPENDITURE_CATEGORY_LIST,
		data,
	}
}

export function setDelayOrderReasonList(reason){
	return {
		type: types.DELAY_ORDER_REASON_LIST,
		reason,
	}
}

export function setRejectOrderReasonList(reason){
	return {
		type: types.REJECT_ORDER_REASON_LIST,
		reason,
	}
}

export function setOutletList(listOutlet){
	return{
		type: types.OUTLET_LIST,
		reason: listOutlet,
	}
}

export function setComplaintItemSolutionList(listSolution){
	return{
		type: types.COMPLAINT_ITEM_SOLUTION_LIST,
		listSolution: listSolution,
	}
}

export function seCourierList(listCourier){
	return{
		type: types.COURIER_LIST,
		reason: listCourier,
	}
}