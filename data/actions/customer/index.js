import * as types from 'app/data/actions/actionTypes';

import * as actions from 'app/data/actions';

import * as STRING from 'app/data/string';
import * as CONSTANT from 'app/data/constant';
import * as API from 'app/data/api';

export function getCustomerOutletList(){
	return async function(dispatch, getState){
        let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;
        let userData = getState().rootReducer.user.data;

        let result = [];

        if (indexActiveOutlet != null) {
            let outletID = outletList[indexActiveOutlet].id;
            await API.POS._getCustomerOutlet(outletID, userData.accessToken)
                .then(async response => {
                    let {status, error, data} = response;
                    if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
                        dispatch(setCustomerOutletList(data));
                    }
                    else{
                        throw new Error('API ERROR : ' + status.code);
                    }
                })
                .catch(error => {
                    console.log(error + ' - actions.customer.getCustomerOutletList');
                });
        }

        return result;
	}
}

export function createCustomerOutlet(name, mobile_phone, email){
    return async function(dispatch, getState){
        let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;
        let userData = getState().rootReducer.user.data;
        let outletID = outletList[indexActiveOutlet].id;

        let result = [];

        dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Membuat akun..'}));

        await API.Customer._createCustomerOutlet(name, mobile_phone, email, outletID, userData.accessToken)
            .then(async response => {
                dispatch(actions.navigator.dismissModal());

                let {status, error, data} = response;
                if (status.code == CONSTANT.HTTP_STATUS_CODE.OK) {
                    result = data;
                }
                else{
                    result = error;
                    throw new Error('API ERROR : ' + status.code);
                }
            })
            .catch(error => {
                dispatch(actions.navigator.dismissModal());
                console.log(error + ' - actions.customer.createCustomerOutlet');
            });

        return result;
    }
}

export function searchCustomerKliknklin(itemToSearch, categoryToSearch){
    return async function(dispatch, getState){
        let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;
        let userData = getState().rootReducer.user.data;
        let outletID = outletList[indexActiveOutlet].id;

        let result = 'not-found';

        await API.Customer._searchCustomer(itemToSearch, userData.accessToken, categoryToSearch)
            .then(async response => {
                let {status, error, data} = response;
                if (status.code == CONSTANT.HTTP_STATUS_CODE.OK || status.code == CONSTANT.HTTP_STATUS_CODE.NOT_FOUND
                    || status.code ==CONSTANT.HTTP_STATUS_CODE.INTERNAL_SERVER_ERROR) {
                    result = data;
                }
                else{
                    let errors = new Error('API ERROR : ' + status.code + ' || ' + error);
                    errors.name = 'api';
                    throw errors;
                }
            })
            .catch(error => {

                if (error.name == 'api') {
                    result = 'error-api';
                }
                else{
                    result = 'error-timeout';   
                }

                console.log(error.message + ' - actions.customer.searchCustomerKliknklin');
            });

        return result;
    }
}

export function searchCustomerByEmail(itemToSearch){
    return async function(dispatch, getState){
        let result = await dispatch(searchCustomerKliknklin(itemToSearch, 'email'));
        if (result.length == 0) {
            return null;
        }
        else{
            for(let i in result){
                if (result[i].email == itemToSearch) {      
                    return result;
                }
            }
        }
    }
}

export function searchCustomerByPhone(itemToSearch){
    return async function(dispatch, getState){
        //cari local customer outlet list, ada gak phone numbernya.
        //kalo ada set data pos customer
        //kalo ga ada search customer di db by phone
        //kalo di db ada set data pos customer
        //kalo ga ada di db register customer dengan phone, name, email
        //data returnya di set data pos customer

        let customerOutlet = getState().rootReducer.customer.outlet;

        let customerData = null;

        let result = 'found';

        //cari di customer outlet
        //kalo ada langsung done
        //kalo ga ada cari di db
        for(let i in customerOutlet){
            if (customerOutlet[i].user.mobile_phone == itemToSearch) {
                customerData = JSON.parse(JSON.stringify(customerOutlet[i].user));
                customerData.customer_id = customerOutlet[i].customer_id;
            }
        }

        if (!customerData) {
            customerData = await dispatch(searchCustomerKliknklin(itemToSearch, 'phone'));
            console.log(customerData);
            if (Array.isArray(customerData)) {
                let newCustomerData = null;

                for(let i in customerData){
                    if (customerData[i].mobile_phone == itemToSearch) {
                        newCustomerData = JSON.parse(JSON.stringify(customerData[i]));
                    }
                }
                
                customerData = newCustomerData;

                if (!newCustomerData) {
                    result = 'not-found'
                }
            }
            else{
                result = customerData;
            }
            
        }
        
        dispatch(setTempCustomerData(customerData))
        
        let obj = {};
        obj.result = result
        obj.customerData = customerData;
        return obj;
    }
}

export function setSearchData(data){
    return {
        type: types.CUSTOMER_SEARCH_DATA, 
        data,
    };  
}

export function setTempCustomerData(data){
    return {
        type: types.CUSTOMER_TEMP_DATA, 
        data,
    };  
}

export function setCustomerOutletList(dataList){
    return {
        type: types.CUSTOMER_OUTLET, 
        data:dataList,
    };  
}


