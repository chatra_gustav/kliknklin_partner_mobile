import * as types from './../actionTypes';

import * as actions from './../../actions';

import * as STRING from './../../string';
import * as CONSTANT from './../../constant';
import * as API from './../../api';

export function getTransaction(){

	return async function(dispatch, getState){

		let {outletList, indexActiveOutlet} = getState().rootReducer.user.outlet;

		let transactionList = [];

		if(indexActiveOutlet != null){
			let outletMapping = outletList[indexActiveOutlet].mapping;

			if(outletMapping){
				dispatch(actions.navigator.showModal('PartnerKliknKlin.LoadingModal', {loadingText:'Mohon tunggu..'}));

				await API.PosOldVersion._getTransaction(outletMapping.outlet_id_old)
				.then(async (response) => {

					dispatch(actions.navigator.dismissModal());

					let {success, data} = response;
					
		    		if (success == 'yeah') {
		    			await dispatch(setTransactionList(data));
		    		}
					else{
						throw new Error('API ERROR : ');
					}
				})
				.catch((error) => {
					console.log(error, '--getTransaction');
					dispatch(actions.navigator.dismissModal());

		    		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
						dispatch(actions.root.showConnectionError());
					}
					else{
						dispatch(actions.root.showErrorMessage(error.message));
					}
				});
			}else{
				await dispatch(setTransactionList([]));
			}
		}else{
			await dispatch(setTransactionList([]));
		}
	}
}

export function doneTransaction(params){
	return async function(dispatch, getState){
		
		let transactionList = getState().rootReducer.posOldVersion.transactionList;

		await API.PosOldVersion._updateTransaction(params)
		.then(async (response) => {

			let {success, data} = response;

    		if (success == 'yeah') {

    			await dispatch(getTransaction());

    			let new_activeTransaction = {};
    			let activeTransaction = getState().rootReducer.posOldVersion.activeTransaction;

    			let findData = (p) => {
			      return p.id === activeTransaction.id;
			    }

			    dataIndex = transactionList.findIndex(findData);

			    new_activeTransaction = {...transactionList[dataIndex]};

			    new_activeTransaction.transaction_status_id = 2;

    			await dispatch(setActiveTransaction(new_activeTransaction));
    		}
			else{
				throw new Error('API ERROR : ');
			}
		})
		.catch((error) => {
			console.log(error, '--doneTransaction');

    		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
				dispatch(actions.root.showConnectionError());
			}
			else{
				dispatch(actions.root.showErrorMessage(error.message));
			}
		});
	}
}

export function payTransaction(params){
	return async function(dispatch, getState){

		let transactionList = getState().rootReducer.posOldVersion.transactionList;
		
		await API.PosOldVersion._updateTransaction(params)
		.then(async (response) => {

			let {success, data} = response;

    		if (success == 'yeah') {

    			if(params.transaction_status_id == 2){
    				await dispatch(getTransaction());
    			}
    			else{
    				
    				await dispatch(setActiveTransaction(data));

    				await dispatch(getTransaction());
    			}
    		}
			else{
				throw new Error('API ERROR : ');
			}
		})
		.catch((error) => {
			console.log(error, '--payTransaction');

    		if (error.message == CONSTANT.ERROR_TYPE.TIMEOUT || error.message == CONSTANT.ERROR_TYPE.NETWORK_FAILED){
				dispatch(actions.root.showConnectionError());
			}
			else{
				dispatch(actions.root.showErrorMessage(error.message));
			}
		});
	}
}

export function configTransactionStatus(transaction_status){

	return async function(dispatch, getState){

		await dispatch(getTransaction());

		let activeTransactionList = [];
		let transactionList = getState().rootReducer.posOldVersion.transactionList;

		if(transaction_status.id == 1){ // semua status transaksi
			activeTransactionList = transactionList;
		}else if(transaction_status.id == 2){ // Belum Dibayar - Sudah Selesai
			activeTransactionList = filterTransactionList(transactionList, 1, 2);
		}else if(transaction_status.id == 3){ // Belum Dibayar - Sedang Dikerjakan
			activeTransactionList = filterTransactionList(transactionList, 1, 1);
		}else if(transaction_status.id == 4){ // Sudah Dibayar - Sedang Dikerjakan
			activeTransactionList = filterTransactionList(transactionList, 2, 1);
		}

		await dispatch(setTransactionList(activeTransactionList));
	}

}

function filterTransactionList(transactionList, status_payment, status_transaction){

	let returnList = [];

	transactionList.map((transaction) => {

		let payment_status_id = Number(transaction.payment_status_id);
		let transaction_status_id = transaction.transaction_status_id;

		if(payment_status_id == status_payment && transaction_status_id == status_transaction){
			returnList.push(transaction);
		}
	})

	return returnList;
}

export function searchTransaction(keyword, category){
	return async function(dispatch, getState){

		await dispatch(getTransaction());

		let activeTransactionList = [];
		let transactionList = getState().rootReducer.posOldVersion.transactionList;

		if(category == CONSTANT.SEARCH_ORDER_CATEGORY.NAME){
			activeTransactionList = transactionList.filter(t => (t.customer.name).includes(keyword));
		}else{ // search by transaction hash
			activeTransactionList = transactionList.filter(t => (t.transaction_hash).includes(keyword));
		}

		await dispatch(setSearchedTransactionList(activeTransactionList));
	}
}

export function setTransactionList(data){

	return {
	    type: types.SET_TRANSACTION_LIST, 
	    data,
	};
}

export function setSearchedTransactionList(data){

	return {
	    type: types.SET_SEARCH_TRANSACTION_LIST, 
	    data,
	};
}

export function setActiveTransaction(data){

	return {
	    type: types.SET_ACTIVE_TRANSACTION, 
	    data,
	};
}